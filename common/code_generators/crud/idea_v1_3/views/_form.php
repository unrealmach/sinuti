<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";

?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form section default-bg">




    <?= "<?php " ?>$form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= "<?= " ?> Html::encode(<?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

            <?php
            foreach ($generator->getColumnNames() as $attribute) {
                if (in_array($attribute, $safeAttributes)) {
                    echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
                }
            }

            ?>
        </div>
    </div>
    <div class="form-group">
        <center>       
            <?= "<?= " ?>Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).<?= $generator->generateString('Create') ?> : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).<?= $generator->generateString('Update') ?>, ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
