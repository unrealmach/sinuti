<?php
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";

?>

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString('Create ' . Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString(Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)))) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-create box box-info">
    <div class="box-header with-border">
        <?=
        " <?php
            \$ar = explode(\" \", (\$this->title));
            \$title = \"\";
            for (\$i = 1; \$i < count(\$ar); \$i++) {
            \$title.=\" \" . \$ar[\$i];}
          ?> \n"
        ?>
        <center>
            <h1 class="box-title"><?= "<?= " ?>Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= "<?= " ?>$this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
