<?php
return [
    'components' => [
         'db_auditoria' => [
            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=localhost;dbname=sinuti_auditoria',
//            'username' => 'root',
//            'password' => '',
            
            'dsn' => 'pgsql:host=localhost;dbname=sinuti_auditoria',
             'username' => 'postgres',
            'password' => 'admin123',
            'charset' => 'utf8',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
//            'dsn' => 'mysql:host=localhost;dbname=sistema_nutrimental_infantil',
//            'username' => 'root',
//            'password' => '',
            'dsn' => 'pgsql:host=localhost;dbname=sistema_nutrimental_infantil',
             'username' => 'postgres',
            'password' => 'admin123',
            'charset' => 'utf8',
        ],
       
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
