<?php
return [
    'language' => 'es',
    'timeZone' => 'America/Lima',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'name' => 'SINUTI',
    'components' => [
//        'SessionTimeOut' => [
//            'class' => 'common\components\SessionTimeOut'
//        ],
        'errorHandler' => [
            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
        ],
        'Util' => [
            'class' => 'common\components\Util'
        ],
        'SendGrid' => [
            'class' => 'common\components\SendGrid',
            'turnOn' => false,
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ]
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'user' => [
            'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'on afterLogin' => function($event) {
            \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
        }],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'audit' => [
            'class' => 'bedezign\yii2\audit\Audit',
            'layout' => 'main',
            'db' => 'db_auditoria',
            'ignoreActions' => ['audit/*', 'debug/*'],
            'maxAge' => 100,
//            'accessIps' => ['127.0.0.1'],
            'compressData' => true,
            'panels' => [
                'audit/request',
                'audit/error',
                'audit/trail',
            ],
        ],
      
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ]
    ],
];