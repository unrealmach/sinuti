DROP procedure IF EXISTS `new_procedure`;

DELIMITER $$
CREATE  PROCEDURE `new_procedure`()
    DETERMINISTIC
    COMMENT 'First SP at Expertdeveloper'
BEGIN

-- START TRANSACTION;
	-- 0 FALSE Significa que ya se deben de finalizar el periodo, cambiar fechas de fin
    -- 1 TRUE Significa que el sistema aun esta en el periodo
    -- Busca si la fecha actual esta entre los dias de inicio y fin del periodo para el periodo activo
    SET @check_cambio_correcto := 
    (SELECT IF(estado =  "ACTIVO" AND NOW() BETWEEN concat(periodo.fecha_inicio,' ','00:00:00' )
									AND concat(periodo.fecha_fin,' ','23:59:59'),1,0) as data FROM  `periodo` );
    select @check_cambio_correcto;
    
    IF @check_cambio_correcto = 0 THEN  
		
	-- cambia los etados y pone fechas fin
    UPDATE  sector_asignado_educador SET  sector_asignado_educador.fecha_fin_actividad =  now()  where sector_asignado_educador.fecha_fin_actividad is NULL; -- triger 2
    UPDATE autoridades_cibv set autoridades_cibv.fecha_fin_actividad = now() where autoridades_cibv.fecha_fin_actividad is NULL; -- trigger 1
	UPDATE asignacion_gastronomo_distrito set asignacion_gastronomo_distrito.fecha_fin_actividad = now() where asignacion_gastronomo_distrito.fecha_fin_actividad is NULL; -- triger 3
    UPDATE asignacion_nutricionista_distrito set asignacion_nutricionista_distrito.fecha_fin_actividad = now() where asignacion_nutricionista_distrito.fecha_fin_actividad is NULL;
       
        
	 UPDATE  `matricula` SET  `matricula_estado` =  'RETIRADO' WHERE  `matricula`.`matricula_estado` ='ACTIVO';
	 UPDATE  `periodo` SET  `estado` =  'INACTIVO' WHERE  `periodo`.`estado` = 'ACTIVO';
        
    END IF;
  -- COMMIT; 
END$$

DELIMITER ;
