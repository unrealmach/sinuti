-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-08-2015 a las 05:56:51
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;

SET AUTOCOMMIT = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_assignment`
--
-- Creación: 07-08-2015 a las 03:06:31
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('sysadmin', '1', NULL),
('sysadmin', '2', 1438918974);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item`
--
-- Creación: 07-08-2015 a las 03:06:31
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, NULL, NULL),
('/admin/*', 2, NULL, NULL, NULL, NULL, NULL),
('/settings/*', 2, NULL, NULL, NULL, 1436137414, 1436137414),
('actualizar_compania', 2, 'Permite actualizar una compania', NULL, NULL, 1436110497, 1436113259),
('actualizar_compania_propia', 2, 'Permite actualizar la compania unicamente al usuario que creo la compania', 'Actualizar_propia_compania', NULL, 1436113347, 1436117532),
('crear_compania', 2, 'Permite crear una compania', NULL, NULL, 1436110471, 1436110471),
('gastronomo', 1, 'Aquella persona encargada del area de alimentacion, platillos, menus, alimentos', NULL, NULL, 1438917143, 1438917143),
('institucion', 1, 'Responsable de la institucion, podrá ingresar infantes, ', NULL, NULL, 1436110396, 1438917261),
('nutricion', 1, 'Aquella entidad o persona que esta encargada del area nutricional infantil en el centro o institución\r\nPermitira Gestionar datos antropometricos, medidas y reportes de estos', NULL, NULL, 1438916919, 1438916919),
('permission_admin', 2, 'Permisos para agregar, cambiar, modificar permisos, roles', NULL, NULL, NULL, NULL),
('supervisor', 1, 'Aquella entidad o persona que supervisara a la institucion en el area de nutricion, salud y alimentacion', NULL, NULL, 1438916981, 1438916981),
('sysadmin', 1, 'Puede hacer cualquier cosa en el sistema', NULL, NULL, NULL, 1438918949);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_item_child`
--
-- Creación: 07-08-2015 a las 03:06:31
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('sysadmin', '/*'),
('sysadmin', '/admin/*'),
('institucion', '/settings/*'),
('institucion', 'actualizar_compania_propia'),
('sysadmin', 'crear_compania'),
('sysadmin', 'gastronomo'),
('sysadmin', 'institucion'),
('sysadmin', 'nutricion'),
('sysadmin', 'permission_admin'),
('sysadmin', 'supervisor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_rule`
--
-- Creación: 07-08-2015 a las 03:06:31
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('Actualizar_propia_compania', 'O:52:"backend\\modules\\rbac\\reglas\\ActualizarPropiaCompania":3:{s:4:"name";s:26:"Actualizar_propia_compania";s:9:"createdAt";i:1436113236;s:9:"updatedAt";i:1436113236;}', 1436113236, 1436113236);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--
-- Creación: 07-08-2015 a las 03:55:37
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'adm5', 'adm5', 'adm5', 'eiuWvmtqm9TO_lUkEd79rqFhCaItkajy', '$2y$13$PPAIf/iunQgUWkmJStEwp.IcpTmn08wDD37BY0Ni.sTYHVAWFTBvC', NULL, 'unrealmach2@hotmail.com', 10, 1435896515, 1435960226),
(2, 'Mauricio Robinson', 'Chamorro Chamorro', 'unrealmach', 'kfA444Wrk5uAZMIrRQGVhYHsV00HGR89', '$2y$13$aVRHzeH2JXDm7OiM8A1SXeQCITYb1NF3eCv44hwSyUE3Bvx8B8Rx2', NULL, 'unrealmach@hotmail.com', 10, 1438918844, 1438918844);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Filtros para la tabla `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
