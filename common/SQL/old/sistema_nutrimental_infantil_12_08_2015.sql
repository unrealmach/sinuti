-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistema_nutrimental_infantil` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistema_nutrimental_infantil` DEFAULT CHARACTER SET latin1 ;
-- -----------------------------------------------------
-- Schema nutricion_infantil
-- -----------------------------------------------------
USE `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`centro_infantil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`centro_infantil` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`centro_infantil` (
  `cen_inf_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cen_inf_nombre` VARCHAR(150) NOT NULL COMMENT '',
  `cen_inf_direccion` VARCHAR(300) NOT NULL COMMENT '',
  `cen_inf_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `cen_inf_correo` VARCHAR(100) NOT NULL COMMENT '',
  `cen_inf_coord_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `cen_inf_coord_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  PRIMARY KEY (`cen_inf_id`)  COMMENT '',
  UNIQUE INDEX `cen_inf_nombre_UNIQUE` (`cen_inf_nombre` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`directiva`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`directiva` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`directiva` (
  `directiva_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `directiva_nombre` VARCHAR(300) NOT NULL COMMENT '',
  `directiva_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `directiva_cargo` ENUM('PRESIDENTE', 'VICEPRESIDENTE', 'TESORERO', 'SECRETARIO') NOT NULL COMMENT '',
  `centro_infantil_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`directiva_id`)  COMMENT '',
  INDEX `fk_directiva_centro_infantil_idx` (`centro_infantil_id` ASC)  COMMENT '',
  CONSTRAINT `fk_directiva_centro_infantil`
    FOREIGN KEY (`centro_infantil_id`)
    REFERENCES `sistema_nutrimental_infantil`.`centro_infantil` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`educador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`educador` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`educador` (
  `educador_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `educador_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `educador_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `educador_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `centro_infantil_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`educador_id`)  COMMENT '',
  INDEX `fk_educadoras_centro_infantil1_idx` (`centro_infantil_id` ASC)  COMMENT '',
  CONSTRAINT `fk_educadoras_centro_infantil1`
    FOREIGN KEY (`centro_infantil_id`)
    REFERENCES `sistema_nutrimental_infantil`.`centro_infantil` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`infante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`infante` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`infante` (
  `infante_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_cedula_o_pasaporte` VARCHAR(45) NOT NULL COMMENT '',
  `infante_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `infante_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `infante_nacionalidad` VARCHAR(50) NOT NULL COMMENT '',
  `infante_genero` ENUM('FEMENINO', 'MASCULINO') NOT NULL DEFAULT 'MASCULINO' COMMENT '',
  `infante_etnia` ENUM('MESTIZO', 'AFROAMERICANO', 'INDIGENA') NOT NULL DEFAULT 'MESTIZO' COMMENT '',
  `infantes_fecha_nacimiento` DATE NOT NULL COMMENT 'preguntar campo fecha nacimientto',
  `infante_represent_documento` VARCHAR(45) NOT NULL COMMENT '',
  `infante_represent_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `infante_represent_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `infante_represent_parentesco` ENUM('MAMA', 'PAPA', 'ABUELO', 'ABUELA', 'HERMANO', 'HERMANA', 'TIO', 'TIA', 'MADRASTRA', 'PADRASTRO', 'REPRESENTANTE_LEGAL') NOT NULL DEFAULT 'MAMA' COMMENT '',
  `infante_direccion_domiciliaria` VARCHAR(300) NOT NULL COMMENT '',
  `infante_represent_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `infante_represent_correo` VARCHAR(100) NULL COMMENT '',
  `infante_fecha_ingreso_cibv` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `infante_fecha_salida_cibv` DATETIME NULL COMMENT '',
  `centro_infantil_id` INT NOT NULL COMMENT '',
  `educador_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`infante_id`)  COMMENT '',
  INDEX `fk_infantes_centro_infantil1_idx` (`centro_infantil_id` ASC)  COMMENT '',
  INDEX `fk_infantes_educadoras1_idx` (`educador_id` ASC)  COMMENT '',
  UNIQUE INDEX `infantes_cedula_UNIQUE` (`infante_cedula_o_pasaporte` ASC)  COMMENT '',
  CONSTRAINT `fk_infantes_centro_infantil1`
    FOREIGN KEY (`centro_infantil_id`)
    REFERENCES `sistema_nutrimental_infantil`.`centro_infantil` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_infantes_educadoras1`
    FOREIGN KEY (`educador_id`)
    REFERENCES `sistema_nutrimental_infantil`.`educador` (`educador_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`asistencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`asistencia` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`asistencia` (
  `asistencia_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `asistencia_hora_ingreso` TIME NOT NULL COMMENT '',
  `asistencia_hora_salida` TIME NOT NULL COMMENT '',
  `asistencia_fecha` DATE NOT NULL COMMENT '',
  `infantes_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`asistencia_id`)  COMMENT '',
  INDEX `fk_asistencia_infantes1_idx` (`infantes_id` ASC)  COMMENT '',
  CONSTRAINT `fk_asistencia_infantes1`
    FOREIGN KEY (`infantes_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_rule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_rule` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_rule` (
  `name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  `updated_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`name`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_item` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_item` (
  `name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `type` INT(11) NOT NULL COMMENT '',
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `rule_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  `updated_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`name`)  COMMENT '',
  INDEX `rule_name` (`rule_name` ASC)  COMMENT '',
  INDEX `idx-auth_item-type` (`type` ASC)  COMMENT '',
  CONSTRAINT `auth_item_ibfk_1`
    FOREIGN KEY (`rule_name`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_rule` (`name`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_assignment` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_assignment` (
  `item_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `user_id` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`item_name`, `user_id`)  COMMENT '',
  CONSTRAINT `auth_assignment_ibfk_1`
    FOREIGN KEY (`item_name`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_item_child`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_item_child` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_item_child` (
  `parent` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `child` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  PRIMARY KEY (`parent`, `child`)  COMMENT '',
  INDEX `child` (`child` ASC)  COMMENT '',
  CONSTRAINT `auth_item_child_ibfk_1`
    FOREIGN KEY (`parent`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2`
    FOREIGN KEY (`child`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`user` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `first_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `last_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `username` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `auth_key` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `password_hash` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `password_reset_token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `status` SMALLINT(6) NOT NULL DEFAULT '10' COMMENT '',
  `created_at` INT(11) NOT NULL COMMENT '',
  `updated_at` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

USE `sistema_nutrimental_infantil`;

DELIMITER $$

USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`cen_inf_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`cen_inf_BEFORE_INSERT` BEFORE INSERT ON `centro_infantil` FOR EACH ROW
BEGIN
SET NEW.cen_inf_coord_nombres = UPPER(NEW.cen_inf_coord_nombres);
SET NEW.cen_inf_coord_apellidos = UPPER(NEW.cen_inf_coord_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`cen_inf_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`cen_inf_BEFORE_UPDATE` BEFORE UPDATE ON `centro_infantil` FOR EACH ROW
BEGIN
SET NEW.cen_inf_coord_nombres = UPPER(NEW.cen_inf_coord_nombres);
SET NEW.cen_inf_coord_apellidos = UPPER(NEW.cen_inf_coord_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`educador_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`educador_BEFORE_INSERT` BEFORE INSERT ON `educador` FOR EACH ROW
BEGIN
SET NEW.educador_nombres = UPPER(NEW.educador_nombres);
SET NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`educador_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`educador_BEFORE_UPDATE` BEFORE UPDATE ON `educador` FOR EACH ROW
BEGIN
SET NEW.educador_nombres = UPPER(NEW.educador_nombres);
SET NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`infante_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`infante_BEFORE_INSERT` BEFORE INSERT ON `infante` FOR EACH ROW
BEGIN
SET NEW.infante_nombres = UPPER(NEW.infante_nombres);
SET NEW.infante_apellidos = UPPER(NEW.infante_apellidos);
SET NEW.infante_represent_apellidos = UPPER(NEW.infante_represent_apellidos);
SET NEW.infante_represent_nombres = UPPER(NEW.infante_represent_nombres);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`infante_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`infante_BEFORE_UPDATE` BEFORE UPDATE ON `infante` FOR EACH ROW
BEGIN
SET NEW.infante_nombres = UPPER(NEW.infante_nombres);
SET NEW.infante_apellidos = UPPER(NEW.infante_apellidos);
SET NEW.infante_represent_apellidos = UPPER(NEW.infante_represent_apellidos);
SET NEW.infante_represent_nombres = UPPER(NEW.infante_represent_nombres);
END
$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
