-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Schema sistema_nutrimental_infantil
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `sistema_nutrimental_infantil` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `sistema_nutrimental_infantil` ;

-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`cibv`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`cibv` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`cibv` (
  `cen_inf_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cen_inf_nombre` VARCHAR(150) NOT NULL COMMENT '',
  `cen_inf_direccion` VARCHAR(300) NOT NULL COMMENT '',
  `cen_inf_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `cen_inf_correo` VARCHAR(100) NOT NULL COMMENT '',
  `cen_inf_activo` ENUM('SI', 'NO') NOT NULL DEFAULT 'SI' COMMENT '',
  PRIMARY KEY (`cen_inf_id`)  COMMENT '',
  UNIQUE INDEX `cen_inf_nombre_UNIQUE` (`cen_inf_nombre` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`periodo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`periodo` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`periodo` (
  `periodo_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `fecha_inicio` DATE NOT NULL COMMENT '',
  `fecha_fin` DATE NOT NULL COMMENT '',
  PRIMARY KEY (`periodo_id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`grupo_comite`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`grupo_comite` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`grupo_comite` (
  `grupo_comite_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `fecha_inicio_actividad` DATE NOT NULL COMMENT '',
  `fecha_fin_actividad` DATE NULL COMMENT '',
  `periodo_id` INT NOT NULL COMMENT '',
  `cen_inf_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`grupo_comite_id`)  COMMENT '',
  INDEX `fk_grupo_comite_periodo1_idx` (`periodo_id` ASC)  COMMENT '',
  INDEX `fk_grupo_comite_cibv1_idx` (`cen_inf_id` ASC)  COMMENT '',
  CONSTRAINT `fk_grupo_comite_periodo1`
    FOREIGN KEY (`periodo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`periodo` (`periodo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grupo_comite_cibv1`
    FOREIGN KEY (`cen_inf_id`)
    REFERENCES `sistema_nutrimental_infantil`.`cibv` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`comite_padres_flia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`comite_padres_flia` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`comite_padres_flia` (
  `comite_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `comite_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `comite_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `comite_cedula_o_pasaporte` VARCHAR(45) NOT NULL COMMENT '',
  `comite_numero_contacto` VARCHAR(15) NOT NULL COMMENT '',
  `grupo_comite_id` INT NOT NULL COMMENT '',
  `cargo` ENUM('PRESIDENTE', 'VICEPRESIDENTE', 'TESORERO', 'SECRETARIO') NOT NULL COMMENT '',
  PRIMARY KEY (`comite_id`)  COMMENT '',
  UNIQUE INDEX `directiva_cedula_UNIQUE` (`comite_cedula_o_pasaporte` ASC)  COMMENT '',
  INDEX `fk_comite_padres_flia_grupo_comite1_idx` (`grupo_comite_id` ASC)  COMMENT '',
  CONSTRAINT `fk_comite_padres_flia_grupo_comite1`
    FOREIGN KEY (`grupo_comite_id`)
    REFERENCES `sistema_nutrimental_infantil`.`grupo_comite` (`grupo_comite_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`educador`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`educador` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`educador` (
  `educador_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `educador_cedula` VARCHAR(45) NOT NULL COMMENT '',
  `educador_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `educador_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `educador_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `fecha_inicio_actividad` DATE NOT NULL COMMENT '',
  `fecha_fin_actividad` DATE NULL COMMENT '',
  PRIMARY KEY (`educador_id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`infante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`infante` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`infante` (
  `infante_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_cedula_o_pasaporte` VARCHAR(45) NOT NULL COMMENT '',
  `infante_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `infante_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `infante_nacionalidad` VARCHAR(50) NOT NULL COMMENT '',
  `infante_genero` ENUM('FEMENINO', 'MASCULINO') NOT NULL DEFAULT 'MASCULINO' COMMENT '',
  `infante_etnia` ENUM('MESTIZO', 'AFROAMERICANO', 'INDIGENA') NOT NULL DEFAULT 'MESTIZO' COMMENT '',
  `infantes_fecha_nacimiento` DATE NOT NULL COMMENT 'preguntar campo fecha nacimientto',
  `infante_represent_documento` VARCHAR(45) NOT NULL COMMENT '',
  `infante_represent_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `infante_represent_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `infante_represent_parentesco` ENUM('MAMA', 'PAPA', 'ABUELO', 'ABUELA', 'HERMANO', 'HERMANA', 'TIO', 'TIA', 'MADRASTRA', 'PADRASTRO', 'REPRESENTANTE_LEGAL') NOT NULL DEFAULT 'MAMA' COMMENT '',
  `infante_direccion_domiciliaria` VARCHAR(300) NOT NULL COMMENT '',
  `infante_represent_telefono` VARCHAR(15) NOT NULL COMMENT '',
  `infante_represent_correo` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`infante_id`)  COMMENT '',
  UNIQUE INDEX `infantes_cedula_UNIQUE` (`infante_cedula_o_pasaporte` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`asistencia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`asistencia` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`asistencia` (
  `asistencia_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `asistencia_hora_ingreso` TIME NOT NULL COMMENT '',
  `asistencia_hora_salida` TIME NOT NULL COMMENT '',
  `asistencia_fecha` DATE NOT NULL COMMENT '',
  `infantes_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`asistencia_id`)  COMMENT '',
  INDEX `fk_asistencia_infantes1_idx` (`infantes_id` ASC)  COMMENT '',
  CONSTRAINT `fk_asistencia_infantes1`
    FOREIGN KEY (`infantes_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_rule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_rule` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_rule` (
  `name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  `updated_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`name`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_item` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_item` (
  `name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `type` INT(11) NOT NULL COMMENT '',
  `description` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `rule_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `data` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  `updated_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`name`)  COMMENT '',
  INDEX `rule_name` (`rule_name` ASC)  COMMENT '',
  INDEX `idx-auth_item-type` (`type` ASC)  COMMENT '',
  CONSTRAINT `auth_item_ibfk_1`
    FOREIGN KEY (`rule_name`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_rule` (`name`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_assignment` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_assignment` (
  `item_name` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `user_id` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `created_at` INT(11) NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`item_name`, `user_id`)  COMMENT '',
  CONSTRAINT `auth_assignment_ibfk_1`
    FOREIGN KEY (`item_name`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`auth_item_child`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`auth_item_child` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`auth_item_child` (
  `parent` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `child` VARCHAR(64) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  PRIMARY KEY (`parent`, `child`)  COMMENT '',
  INDEX `child` (`child` ASC)  COMMENT '',
  CONSTRAINT `auth_item_child_ibfk_1`
    FOREIGN KEY (`parent`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2`
    FOREIGN KEY (`child`)
    REFERENCES `sistema_nutrimental_infantil`.`auth_item` (`name`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`user` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `first_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `last_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `username` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `auth_key` VARCHAR(32) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `password_hash` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `password_reset_token` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL COMMENT '',
  `email` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `status` SMALLINT(6) NOT NULL DEFAULT '10' COMMENT '',
  `created_at` INT(11) NOT NULL COMMENT '',
  `updated_at` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`coordinador_cibv`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`coordinador_cibv` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`coordinador_cibv` (
  `coordinador_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `coordinador_nombres` VARCHAR(150) NOT NULL COMMENT '',
  `coordinador_apellidos` VARCHAR(150) NOT NULL COMMENT '',
  `coordinador_cedula_o_pasaporte` VARCHAR(45) NOT NULL COMMENT '',
  `coordinador_contacto` VARCHAR(15) NOT NULL COMMENT '',
  `coordinador_correo` VARCHAR(150) NOT NULL COMMENT '',
  `fecha_inicio_actividad` DATE NOT NULL COMMENT '',
  `fecha_fin_actividad` DATE NULL COMMENT '',
  PRIMARY KEY (`coordinador_id`)  COMMENT '',
  UNIQUE INDEX `coordinador_cedula_o_pasaporte_UNIQUE` (`coordinador_cedula_o_pasaporte` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`matricula`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`matricula` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`matricula` (
  `matricula_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cen_inf_id` INT NOT NULL COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `periodo_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`matricula_id`)  COMMENT '',
  INDEX `fk_matricula_cibv1_idx` (`cen_inf_id` ASC)  COMMENT '',
  INDEX `fk_matricula_infante1_idx` (`infante_id` ASC)  COMMENT '',
  INDEX `fk_matricula_periodo1_idx` (`periodo_id` ASC)  COMMENT '',
  CONSTRAINT `fk_matricula_cibv1`
    FOREIGN KEY (`cen_inf_id`)
    REFERENCES `sistema_nutrimental_infantil`.`cibv` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matricula_periodo1`
    FOREIGN KEY (`periodo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`periodo` (`periodo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`sector_asignado`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`sector_asignado` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`sector_asignado` (
  `sector_asignado_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cen_inf_id` INT NOT NULL COMMENT '',
  `periodo_id` INT NOT NULL COMMENT '',
  `educador_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`sector_asignado_id`)  COMMENT '',
  INDEX `fk_sector_asignado_cibv1_idx` (`cen_inf_id` ASC)  COMMENT '',
  INDEX `fk_sector_asignado_periodo1_idx` (`periodo_id` ASC)  COMMENT '',
  INDEX `fk_sector_asignado_educador1_idx` (`educador_id` ASC)  COMMENT '',
  CONSTRAINT `fk_sector_asignado_cibv1`
    FOREIGN KEY (`cen_inf_id`)
    REFERENCES `sistema_nutrimental_infantil`.`cibv` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sector_asignado_periodo1`
    FOREIGN KEY (`periodo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`periodo` (`periodo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sector_asignado_educador1`
    FOREIGN KEY (`educador_id`)
    REFERENCES `sistema_nutrimental_infantil`.`educador` (`educador_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`grupo_edad`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`grupo_edad` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`grupo_edad` (
  `grupo_edad_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `grupo_edad_descripcion` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`grupo_edad_id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`salon`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`salon` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`salon` (
  `salon_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `educador_id` INT NOT NULL COMMENT '',
  `periodo_id` INT NOT NULL COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `grupo_edad_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`salon_id`)  COMMENT '',
  INDEX `fk_aula_educador1_idx` (`educador_id` ASC)  COMMENT '',
  INDEX `fk_aula_periodo1_idx` (`periodo_id` ASC)  COMMENT '',
  INDEX `fk_aula_infante1_idx` (`infante_id` ASC)  COMMENT '',
  INDEX `fk_aula_grupo_edad1_idx` (`grupo_edad_id` ASC)  COMMENT '',
  CONSTRAINT `fk_aula_educador1`
    FOREIGN KEY (`educador_id`)
    REFERENCES `sistema_nutrimental_infantil`.`educador` (`educador_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aula_periodo1`
    FOREIGN KEY (`periodo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`periodo` (`periodo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aula_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aula_grupo_edad1`
    FOREIGN KEY (`grupo_edad_id`)
    REFERENCES `sistema_nutrimental_infantil`.`grupo_edad` (`grupo_edad_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`autoridades_cibv`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`autoridades_cibv` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`autoridades_cibv` (
  `autoridades_cibv_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `periodo_id` INT NOT NULL COMMENT '',
  `coordinador_id` INT NOT NULL COMMENT '',
  `cen_inf_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`autoridades_cibv_id`)  COMMENT '',
  INDEX `fk_autoridades_cibv_periodo1_idx` (`periodo_id` ASC)  COMMENT '',
  INDEX `fk_autoridades_cibv_coordinador_cibv1_idx` (`coordinador_id` ASC)  COMMENT '',
  INDEX `fk_autoridades_cibv_cibv1_idx` (`cen_inf_id` ASC)  COMMENT '',
  CONSTRAINT `fk_autoridades_cibv_periodo1`
    FOREIGN KEY (`periodo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`periodo` (`periodo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_autoridades_cibv_coordinador_cibv1`
    FOREIGN KEY (`coordinador_id`)
    REFERENCES `sistema_nutrimental_infantil`.`coordinador_cibv` (`coordinador_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_autoridades_cibv_cibv1`
    FOREIGN KEY (`cen_inf_id`)
    REFERENCES `sistema_nutrimental_infantil`.`cibv` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`asignacion_user_cibv`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`asignacion_user_cibv` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`asignacion_user_cibv` (
  `asignacion_user_cibv_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `user_id` INT NOT NULL COMMENT '',
  `centro_infantil_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`asignacion_user_cibv_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`habito_alimenticio`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`habito_alimenticio` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`habito_alimenticio` (
  `habito_alimenticio_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  PRIMARY KEY (`habito_alimenticio_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`patologia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`patologia` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`patologia` (
  `patologia_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `patologia_pregunta_1` TEXT NOT NULL COMMENT '',
  `patologia_pregunta_2` TEXT NOT NULL COMMENT '',
  `patologia_pregunta_3` TEXT NOT NULL COMMENT '',
  `patologia_pregunta_4` TEXT NOT NULL COMMENT '',
  PRIMARY KEY (`patologia_id`)  COMMENT '',
  INDEX `fk_patologia_infante1_idx` (`infante_id` ASC)  COMMENT '',
  CONSTRAINT `fk_patologia_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`tipo_vacuna`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`tipo_vacuna` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`tipo_vacuna` (
  `tipo_vacuna_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `tipo_vac_enfermedad` VARCHAR(45) NOT NULL COMMENT '',
  `tipo_vac_nombre` VARCHAR(45) NOT NULL COMMENT '',
  `tipo_vac_num_dosis` SMALLINT(4) NOT NULL COMMENT '',
  `tipo_vac_dosis_canti` DECIMAL(4,2) NOT NULL COMMENT '',
  `tipo_vac_dosis_medida` ENUM('ML', 'GOTAS') NOT NULL COMMENT '',
  `tipo_vac_via` ENUM('I.D.', 'V.O.', 'IM', 'VS') NULL COMMENT '',
  `tipo_vac_fecha_aplicacion` DATE NOT NULL COMMENT '',
  PRIMARY KEY (`tipo_vacuna_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`registro_vacuna_infante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`registro_vacuna_infante` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`registro_vacuna_infante` (
  `reg_vac_inf_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `tipo_vacuna_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`reg_vac_inf_id`)  COMMENT '',
  INDEX `fk_registro_vacuna_infante_infante1_idx` (`infante_id` ASC)  COMMENT '',
  INDEX `fk_registro_vacuna_infante_tipo_vacuna1_idx` (`tipo_vacuna_id` ASC)  COMMENT '',
  CONSTRAINT `fk_registro_vacuna_infante_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_vacuna_infante_tipo_vacuna1`
    FOREIGN KEY (`tipo_vacuna_id`)
    REFERENCES `sistema_nutrimental_infantil`.`tipo_vacuna` (`tipo_vacuna_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`dato_antropometrico`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`dato_antropometrico` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`dato_antropometrico` (
  `dat_antro_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `dat_antro_fecha_registro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
  `dat_antro_edad_meses` INT NOT NULL COMMENT '',
  `dat_antro_talla_infante` DECIMAL(5,2) NOT NULL DEFAULT 0 COMMENT '',
  `dat_antro_peso_infante` DECIMAL(5,2) NOT NULL DEFAULT 0 COMMENT '',
  `dat_antro_imc_infante` DECIMAL(5,2) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`dat_antro_id`)  COMMENT '',
  INDEX `fk_dato_antropometrico_infante1_idx` (`infante_id` ASC)  COMMENT '',
  CONSTRAINT `fk_dato_antropometrico_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`m_carta_semanal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`m_carta_semanal` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`m_carta_semanal` (
  `m_c_sem_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `cen_inf_id` INT NOT NULL COMMENT '',
  `m_c_sem_responsable` VARCHAR(45) NOT NULL COMMENT '',
  `m_c_sem_fecha_inicio` DATETIME NOT NULL COMMENT '',
  `m_c_sem_fecha_fin` DATETIME NOT NULL COMMENT '',
  `m_c_sem_nomb_entidad_ejecutora` VARCHAR(100) NOT NULL COMMENT '',
  `m_c_sem_num_personal_comunitario` INT NOT NULL COMMENT '',
  `m_c_sem_capacidad_infantes` INT NOT NULL COMMENT '',
  PRIMARY KEY (`m_c_sem_id`)  COMMENT '',
  INDEX `fk_m_carta_semanal_cibv1_idx` (`cen_inf_id` ASC)  COMMENT '',
  CONSTRAINT `fk_m_carta_semanal_cibv1`
    FOREIGN KEY (`cen_inf_id`)
    REFERENCES `sistema_nutrimental_infantil`.`cibv` (`cen_inf_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`asignacion_infante_c_semanal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`asignacion_infante_c_semanal` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`asignacion_infante_c_semanal` (
  `asig_inf_c_sem_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  `m_carta_semanal_id` INT NOT NULL COMMENT '',
  `asi_inf_c_sem_id_observaciones` TEXT NOT NULL COMMENT '',
  PRIMARY KEY (`asig_inf_c_sem_id`)  COMMENT '',
  INDEX `fk_asignacion_infante_c_semanal_infante1_idx` (`infante_id` ASC)  COMMENT '',
  INDEX `fk_asignacion_infante_c_semanal_m_carta_semanal1_idx` (`m_carta_semanal_id` ASC)  COMMENT '',
  CONSTRAINT `fk_asignacion_infante_c_semanal_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asignacion_infante_c_semanal_m_carta_semanal1`
    FOREIGN KEY (`m_carta_semanal_id`)
    REFERENCES `sistema_nutrimental_infantil`.`m_carta_semanal` (`m_c_sem_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`asignacion_inf_c_semanal_has_infante`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`asignacion_inf_c_semanal_has_infante` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`asignacion_inf_c_semanal_has_infante` (
  `asig_inf_c_sem_id` INT NOT NULL COMMENT '',
  `infante_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`asig_inf_c_sem_id`, `infante_id`)  COMMENT '',
  INDEX `fk_asignacion_inf_c_semanal_has_infante_infante1_idx` (`infante_id` ASC)  COMMENT '',
  INDEX `fk_asignacion_inf_c_semanal_has_infante_asignacion_inf_c_se_idx` (`asig_inf_c_sem_id` ASC)  COMMENT '',
  CONSTRAINT `fk_asignacion_inf_c_semanal_has_infante_asignacion_inf_c_sema1`
    FOREIGN KEY (`asig_inf_c_sem_id`)
    REFERENCES `sistema_nutrimental_infantil`.`asignacion_infante_c_semanal` (`asig_inf_c_sem_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asignacion_inf_c_semanal_has_infante_infante1`
    FOREIGN KEY (`infante_id`)
    REFERENCES `sistema_nutrimental_infantil`.`infante` (`infante_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`categoria` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`categoria` (
  `categoria_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `categoria_nombre` VARCHAR(100) NOT NULL COMMENT '',
  `categoria_descripcion` VARCHAR(255) NOT NULL COMMENT '',
  PRIMARY KEY (`categoria_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`alimento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`alimento` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`alimento` (
  `alimento_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `categoria_id` INT NOT NULL COMMENT '',
  `alimento_nombre` VARCHAR(255) NOT NULL COMMENT '',
  `alimento_descripcion` TEXT NOT NULL COMMENT '',
  PRIMARY KEY (`alimento_id`)  COMMENT '',
  INDEX `fk_alimento_categoria1_idx` (`categoria_id` ASC)  COMMENT '',
  CONSTRAINT `fk_alimento_categoria1`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `sistema_nutrimental_infantil`.`categoria` (`categoria_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`composicion_nutricional`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`composicion_nutricional` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`composicion_nutricional` (
  `com_nut_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `alimento_id` INT NOT NULL COMMENT '',
  `energia_kcal` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `proteinas_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `grasa_total_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `carbohidratos_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `fibra_dietetica_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `calcio_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `fosforo_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `hierro_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `tiamina_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `riboflavina_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `niacina_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `vitamina_c_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `vitamina_a_equiv_retinol_mcg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `acidos_grasos_monoinsaturados_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `acidos_grasos_poliinsaturados_g` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `acidos_grasos_saturados` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `colesterol_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `potasio_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `sodio_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `zinc_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `magnesio_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `vitamina_b6_mg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `vitamina_b12_mcg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  `folato_mcg` DECIMAL(6,2) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`com_nut_id`)  COMMENT '',
  INDEX `fk_composicion_nutricional_alimento1_idx` (`alimento_id` ASC)  COMMENT '',
  CONSTRAINT `fk_composicion_nutricional_alimento1`
    FOREIGN KEY (`alimento_id`)
    REFERENCES `sistema_nutrimental_infantil`.`alimento` (`alimento_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`nutriente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`nutriente` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`nutriente` (
  `nutriente_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nutriente_nombre` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`nutriente_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`tiempo_comida`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`tiempo_comida` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`tiempo_comida` (
  `tiem_com_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `tiem_com_nombre` VARCHAR(45) NOT NULL COMMENT '',
  `tiem_com_hora_inicio` TIME(0) NOT NULL COMMENT '',
  `tiempo_com_hora_fin` TIME(0) NOT NULL COMMENT '',
  PRIMARY KEY (`tiem_com_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`rango_nutriente`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`rango_nutriente` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`rango_nutriente` (
  `ran_nut_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `nutriente_id` INT NOT NULL COMMENT '',
  `tiempo_comida_id` INT NOT NULL COMMENT '',
  `grupo_edad_id` INT NOT NULL COMMENT '',
  `ran_nut_valor_min` DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT '',
  `ran_nut_valor_max` DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`ran_nut_id`)  COMMENT '',
  INDEX `fk_rango_nutriente_nutriente1_idx` (`nutriente_id` ASC)  COMMENT '',
  INDEX `fk_rango_nutriente_tiempo_comida1_idx` (`tiempo_comida_id` ASC)  COMMENT '',
  INDEX `fk_rango_nutriente_grupo_edad1_idx` (`grupo_edad_id` ASC)  COMMENT '',
  CONSTRAINT `fk_rango_nutriente_nutriente1`
    FOREIGN KEY (`nutriente_id`)
    REFERENCES `sistema_nutrimental_infantil`.`nutriente` (`nutriente_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rango_nutriente_tiempo_comida1`
    FOREIGN KEY (`tiempo_comida_id`)
    REFERENCES `sistema_nutrimental_infantil`.`tiempo_comida` (`tiem_com_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rango_nutriente_grupo_edad1`
    FOREIGN KEY (`grupo_edad_id`)
    REFERENCES `sistema_nutrimental_infantil`.`grupo_edad` (`grupo_edad_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`m_prep_carta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`m_prep_carta` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`m_prep_carta` (
  `m_prep_carta_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `m_prep_carta_nombre` VARCHAR(100) NOT NULL COMMENT '',
  `m_prep_carta_descripcion` TEXT NOT NULL COMMENT '',
  PRIMARY KEY (`m_prep_carta_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`tipo_preparacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`tipo_preparacion` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`tipo_preparacion` (
  `tipo_prep_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `tipo_prep_nombre` VARCHAR(45) NOT NULL COMMENT '',
  `tipo_prep_descripcion` VARCHAR(255) NOT NULL COMMENT '',
  PRIMARY KEY (`tipo_prep_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`m_platillo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`m_platillo` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`m_platillo` (
  `m_platillo_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `m_platillo_nombre` TINYTEXT NOT NULL COMMENT '',
  `m_platillo_descripcion` TINYTEXT NOT NULL COMMENT '',
  PRIMARY KEY (`m_platillo_id`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`d_preparacion_carta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`d_preparacion_carta` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`d_preparacion_carta` (
  `d_prep_carta_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `d_prep_carta_cantidad` DECIMAL(10,2) NOT NULL COMMENT '',
  `d_prep_carta_porcion` VARCHAR(45) NOT NULL COMMENT '',
  `m_prep_carta_id` INT NOT NULL COMMENT '',
  `tipo_preparacion_id` INT NOT NULL COMMENT '',
  `grupo_edad_id` INT NOT NULL COMMENT '',
  `alimento_id` INT NULL COMMENT '',
  `m_platillo_id` INT NULL COMMENT '',
  PRIMARY KEY (`d_prep_carta_id`)  COMMENT '',
  INDEX `fk_d_preparacion_carta_m_prep_carta1_idx` (`m_prep_carta_id` ASC)  COMMENT '',
  INDEX `fk_d_preparacion_carta_tipo_preparacion1_idx` (`tipo_preparacion_id` ASC)  COMMENT '',
  INDEX `fk_d_preparacion_carta_grupo_edad1_idx` (`grupo_edad_id` ASC)  COMMENT '',
  INDEX `fk_d_preparacion_carta_alimento1_idx` (`alimento_id` ASC)  COMMENT '',
  INDEX `fk_d_preparacion_carta_m_platillo1_idx` (`m_platillo_id` ASC)  COMMENT '',
  CONSTRAINT `fk_d_preparacion_carta_m_prep_carta1`
    FOREIGN KEY (`m_prep_carta_id`)
    REFERENCES `sistema_nutrimental_infantil`.`m_prep_carta` (`m_prep_carta_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_preparacion_carta_tipo_preparacion1`
    FOREIGN KEY (`tipo_preparacion_id`)
    REFERENCES `sistema_nutrimental_infantil`.`tipo_preparacion` (`tipo_prep_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_preparacion_carta_grupo_edad1`
    FOREIGN KEY (`grupo_edad_id`)
    REFERENCES `sistema_nutrimental_infantil`.`grupo_edad` (`grupo_edad_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_preparacion_carta_alimento1`
    FOREIGN KEY (`alimento_id`)
    REFERENCES `sistema_nutrimental_infantil`.`alimento` (`alimento_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_preparacion_carta_m_platillo1`
    FOREIGN KEY (`m_platillo_id`)
    REFERENCES `sistema_nutrimental_infantil`.`m_platillo` (`m_platillo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`porcion_por_categoria`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`porcion_por_categoria` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`porcion_por_categoria` (
  `porc_cat_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `categoria_id` INT NOT NULL COMMENT '',
  `porc_cat_cantidad_g` DECIMAL(10,2) NOT NULL DEFAULT 100 COMMENT 'La porcion por default de cualquier alimento de cualquier categoria en las tablas de composicion alimenticia son de 100gramos por cada uno de estos.\n',
  `porc_cat_descripcion` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`porc_cat_id`)  COMMENT '',
  INDEX `fk_porcion_por_categoria_categoria1_idx` (`categoria_id` ASC)  COMMENT '',
  CONSTRAINT `fk_porcion_por_categoria_categoria1`
    FOREIGN KEY (`categoria_id`)
    REFERENCES `sistema_nutrimental_infantil`.`categoria` (`categoria_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`porcion_por_alimento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`porcion_por_alimento` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`porcion_por_alimento` (
  `porc_alim_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `alimento_id` INT NOT NULL COMMENT '',
  `porc_alim_cantidad` DECIMAL(10,2) NOT NULL DEFAULT 0 COMMENT '',
  `porc_alim_medida_cacera` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`porc_alim_id`)  COMMENT '',
  INDEX `fk_porcion_por_alimento_alimento1_idx` (`alimento_id` ASC)  COMMENT '',
  CONSTRAINT `fk_porcion_por_alimento_alimento1`
    FOREIGN KEY (`alimento_id`)
    REFERENCES `sistema_nutrimental_infantil`.`alimento` (`alimento_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`d_carta_semanal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`d_carta_semanal` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`d_carta_semanal` (
  `d_c_sem_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `d_c_sem_dia_semana` ENUM('LUNES', 'MARTES', 'MIERCOLES', 'JUEVES', 'VIERNES') NOT NULL COMMENT '',
  `m_carta_semanal_id` INT NOT NULL COMMENT '',
  `m_prep_carta_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`d_c_sem_id`)  COMMENT '',
  INDEX `fk_d_carta_semanal_m_carta_semanal1_idx` (`m_carta_semanal_id` ASC)  COMMENT '',
  INDEX `fk_d_carta_semanal_m_prep_carta1_idx` (`m_prep_carta_id` ASC)  COMMENT '',
  CONSTRAINT `fk_d_carta_semanal_m_carta_semanal1`
    FOREIGN KEY (`m_carta_semanal_id`)
    REFERENCES `sistema_nutrimental_infantil`.`m_carta_semanal` (`m_c_sem_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_carta_semanal_m_prep_carta1`
    FOREIGN KEY (`m_prep_carta_id`)
    REFERENCES `sistema_nutrimental_infantil`.`m_prep_carta` (`m_prep_carta_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`d_platillo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`d_platillo` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`d_platillo` (
  `d_platillo_id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `alimento_id` INT NOT NULL COMMENT '',
  `m_platillo` INT NOT NULL COMMENT '',
  `d_platillo_cantidad_g` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`d_platillo_id`)  COMMENT '',
  INDEX `fk_d_platillo_alimento1_idx` (`alimento_id` ASC)  COMMENT '',
  INDEX `fk_d_platillo_m_platillo1_idx` (`m_platillo` ASC)  COMMENT '',
  CONSTRAINT `fk_d_platillo_alimento1`
    FOREIGN KEY (`alimento_id`)
    REFERENCES `sistema_nutrimental_infantil`.`alimento` (`alimento_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_d_platillo_m_platillo1`
    FOREIGN KEY (`m_platillo`)
    REFERENCES `sistema_nutrimental_infantil`.`m_platillo` (`m_platillo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `sistema_nutrimental_infantil`.`bitacora_consumo_infantil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `sistema_nutrimental_infantil`.`bitacora_consumo_infantil` ;

CREATE TABLE IF NOT EXISTS `sistema_nutrimental_infantil`.`bitacora_consumo_infantil` (
  `b_cons_inf_id` INT NOT NULL COMMENT '',
  `asig_inf_c_sem_id` INT NOT NULL COMMENT '',
  `b_cons_inf_consumo` DECIMAL(3,2) NOT NULL COMMENT '',
  `b_cons_inf_porcentaje` DECIMAL(5,2) NOT NULL COMMENT '',
  `alimento_id` INT NULL COMMENT '',
  `m_platillo_id` INT NULL COMMENT '',
  PRIMARY KEY (`b_cons_inf_id`, `b_cons_inf_consumo`, `b_cons_inf_porcentaje`)  COMMENT '',
  INDEX `fk_consumo_c_sem_inf_asignacion_infante_c_semanal1_idx` (`asig_inf_c_sem_id` ASC)  COMMENT '',
  CONSTRAINT `fk_consumo_c_sem_inf_asignacion_infante_c_semanal1`
    FOREIGN KEY (`asig_inf_c_sem_id`)
    REFERENCES `sistema_nutrimental_infantil`.`asignacion_infante_c_semanal` (`asig_inf_c_sem_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

USE `sistema_nutrimental_infantil`;

DELIMITER $$

USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`comite_padres_flia_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`comite_padres_flia_BEFORE_INSERT` BEFORE INSERT ON `comite_padres_flia` FOR EACH ROW
BEGIN
SET NEW.comite_nombres = UPPER(NEW.comite_nombres);
SET NEW.comite_apellidos = UPPER(NEW.comite_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`comite_padres_flia_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`comite_padres_flia_BEFORE_UPDATE` BEFORE UPDATE ON `comite_padres_flia` FOR EACH ROW
BEGIN
SET NEW.comite_nombres = UPPER(NEW.comite_nombres);
SET NEW.comite_apellidos = UPPER(NEW.comite_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`educador_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`educador_BEFORE_INSERT` BEFORE INSERT ON `educador` FOR EACH ROW
BEGIN
SET NEW.educador_nombres = UPPER(NEW.educador_nombres);
SET NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`educador_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`educador_BEFORE_UPDATE` BEFORE UPDATE ON `educador` FOR EACH ROW
BEGIN
SET NEW.educador_nombres = UPPER(NEW.educador_nombres);
SET NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`infante_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`infante_BEFORE_INSERT` BEFORE INSERT ON `infante` FOR EACH ROW
BEGIN
SET NEW.infante_nombres = UPPER(NEW.infante_nombres);
SET NEW.infante_apellidos = UPPER(NEW.infante_apellidos);
SET NEW.infante_represent_apellidos = UPPER(NEW.infante_represent_apellidos);
SET NEW.infante_represent_nombres = UPPER(NEW.infante_represent_nombres);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`infante_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`infante_BEFORE_UPDATE` BEFORE UPDATE ON `infante` FOR EACH ROW
BEGIN
SET NEW.infante_nombres = UPPER(NEW.infante_nombres);
SET NEW.infante_apellidos = UPPER(NEW.infante_apellidos);
SET NEW.infante_represent_apellidos = UPPER(NEW.infante_represent_apellidos);
SET NEW.infante_represent_nombres = UPPER(NEW.infante_represent_nombres);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`coordinador_cibv_BEFORE_INSERT` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`coordinador_cibv_BEFORE_INSERT` BEFORE INSERT ON `coordinador_cibv` FOR EACH ROW
BEGIN
SET NEW.coordinador_nombres = UPPER(NEW.coordinador_nombres);
SET NEW.coordinador_apellidos = UPPER(NEW.coordinador_apellidos);
END
$$


USE `sistema_nutrimental_infantil`$$
DROP TRIGGER IF EXISTS `sistema_nutrimental_infantil`.`coordinador_cibv_BEFORE_UPDATE` $$
USE `sistema_nutrimental_infantil`$$
CREATE DEFINER = CURRENT_USER TRIGGER `sistema_nutrimental_infantil`.`coordinador_cibv_BEFORE_UPDATE` BEFORE UPDATE ON `coordinador_cibv` FOR EACH ROW
BEGIN
SET NEW.coordinador_nombres = UPPER(NEW.coordinador_nombres);
SET NEW.coordinador_apellidos = UPPER(NEW.coordinador_apellidos);
END
$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
