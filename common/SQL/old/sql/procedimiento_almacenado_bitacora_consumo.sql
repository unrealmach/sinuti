/*
IN educador_id, IN cibv_id
1. instanciar el menu semanal para el cibv y para la semana actual
2. instanciar los infantes asignados para el educador en el periodo actual
3. obtener los menus del dia
4. crear los registros para cada infante 
*/

DROP PROCEDURE IF EXISTS myProcedure;
DROP PROCEDURE IF EXISTS debug;

/** debuger para controlar las variables*/
DELIMITER $$

CREATE PROCEDURE debug(msg VARCHAR(255))
BEGIN
SELECT CONCAT("*** ", msg) AS '*** DEBUG:'; 
END
$$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE myProcedure(
  IN _user_id INT,
  IN _cent_inf_id INT)
BEGIN
DECLARE _currentDay date DEFAULT current_date();
DECLARE _menu_semanal_id INT DEFAULT 0;
DECLARE _fecha_actual_valida INT default 0;


/*compara si el dia actual esta dentro delas fechas de la carta semanal*/
/*compara que esa carta semanal disponible para la fecha sea asignada a un cibv si es una fecha valida devuelve 1, caso contrario 0*/
 select @fecha_consulta_valida:= if(m_c_sem_fecha_inicio IS NULL or m_c_sem_fecha_inicio= '',FALSE,TRUE)
 as fecha_valida
 from m_carta_semanal 
WHERE  (now() BETWEEN  m_carta_semanal.m_c_sem_fecha_inicio  AND  DATE_ADD(m_carta_semanal.m_c_sem_fecha_inicio, INTERVAL 4 DAY) 
AND  m_carta_semanal.cen_inf_id=_cent_inf_id);



IF  @fecha_consulta_valida 
	THEN 
		-- busca el id del usuario q sea de rol educador
        
			select * from alimentos;
    ELSE
			select * from alimentos;
END IF;
    


CALL debug(_currentDay);

	END
$$
DELIMITER ;



Call myProcedure(1,1);

/*
busca al id del usuario si se de rol educador mediante PHP RBAC
*/




select @usuario_valido;

/*compara si el dia actual esta dentro delas fechas de la carta semanal*/
/*compara que esa carta semanal disponible para la fecha sea asignada a un cibv si es una fecha valida devuelve 1, caso contrario 0*/
select @myvar:= if(m_c_sem_fecha_inicio IS NULL or m_c_sem_fecha_inicio= '',FALSE,TRUE)
 as fecha_valida
 from m_carta_semanal 
WHERE  (now() BETWEEN  m_carta_semanal.m_c_sem_fecha_inicio  AND  DATE_ADD(m_carta_semanal.m_c_sem_fecha_inicio, INTERVAL 4 DAY) 
AND  m_carta_semanal.cen_inf_id=1);





/* buscar con el periodo y el id del educador los infantes asignados */

/* obtener el periodo en curso */
select 	* from periodo where periodo.estado = "ACTIVO";

/* obtener el id del educador deaucuerdo al id del usuario --tabla de asignacion: rol institucion-educador*/
select centro_infantil_id from 	asignacion_user_educador_cibv where user_id = 5;

select * from salon where salon.educador_id = 1 and salon.periodo_id=1;

-- obtiene el numero de semana select WEEK(now());
select m_carta_semanal.m_c_sem_fecha_inicio from m_carta_semanal where m_carta_semanal.cen_inf_id=1;

-- obtien los menus del menu semanal del dia en curso
select * from d_carta_semanal where m_carta_semanal_id = 1 and d_c_sem_dia_semana = 
(
SELECT 
     CASE DAYOFWEEK((select now()))
     WHEN 1 THEN 'DOMINGO'
     WHEN 2 THEN 'LUNES'
     WHEN 3 THEN 'MARTES'
     WHEN 4 THEN 'MIERCOLES'
     WHEN 5 THEN 'JUEVES'
     WHEN 6 THEN 'VIERNES'
     WHEN 7 THEN 'SABADO'
     END as fecha
)
 ;

-- generar las filas de asignacion en la tabla de bitacora para los infantes con esos menus de dicho dia





