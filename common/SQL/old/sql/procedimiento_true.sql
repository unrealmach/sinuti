
/*
1. debera buscar al id del usuario si se de rol educador mediante PHP RBAC
2. debera de preguntar si el periodo es valido y enviar el id al procedure
*/

DROP PROCEDURE IF EXISTS myProcedure;

DELIMITER $$
CREATE PROCEDURE myProcedure(
  IN _user_id INT,
  IN _cent_inf_id INT,
  IN _periodo_id INT
  )
BEGIN
DECLARE _menu_semanal_id INT DEFAULT 0;
DECLARE _fecha_actual_valida INT default 0;
 set @fecha_consulta_valida ='FALSE';
 set @educadorid = 0;
 set @listainfantes = null;
 set @listamenus = null;
 
/*compara si el dia actual esta dentro delas fechas de la carta semanal*/
/*compara que esa carta semanal disponible para la fecha sea asignada a un cibv si es una fecha valida devuelve 1, caso contrario 0*/
 select @fecha_consulta_valida:= if(m_c_sem_fecha_inicio IS NULL,'FALSE','TRUE')
 as fecha_valida
 from m_carta_semanal 
WHERE  (now() BETWEEN  m_carta_semanal.m_c_sem_fecha_inicio  AND  DATE_ADD(m_carta_semanal.m_c_sem_fecha_inicio, INTERVAL 4 DAY) 
AND  m_carta_semanal.cen_inf_id=_cent_inf_id);

IF  @fecha_consulta_valida = 'TRUE'
	THEN 
		/* busca si el educador esta registrado con dicho id de usuario */
		select @educadorid := centro_infantil_id  from 	asignacion_user_educador_cibv where user_id = _user_id;
        
			if @educadorid IS NOT NULL
				then 
					/* busca los infantes para ese educador y ese periodo*/
					select @listainfantes := infante_id from salon where salon.educador_id = @educadorid and salon.periodo_id=_periodo_id;
                    
                    /* obtiene los menus del dia*/
                   select @listamenus := d_c_sem_id from d_carta_semanal where m_carta_semanal_id = 1 and d_c_sem_dia_semana = 
							(
							SELECT 
								 CASE DAYOFWEEK((select now()))
								 WHEN 1 THEN 'DOMINGO'
								 WHEN 2 THEN 'LUNES'
								 WHEN 3 THEN 'MARTES'
								 WHEN 4 THEN 'MIERCOLES'
								 WHEN 5 THEN 'JUEVES'
								 WHEN 6 THEN 'VIERNES'
								 WHEN 7 THEN 'SABADO'
								 END as fecha
							)
							 ;
                             
                             if @listainfantes is not null and @listamenus is not null
								then
                                 select @listainfantes as 'listainfantesc';
                                 select @listamenus as 'listamenusc';
                                 select "ya valio" as 'resultadofinal';
                                 /* generar el procedimiento para las filas del detalle de bitacora*/
							 else 
								    call debug(@listainfantes);
                                    call debug(@listamenus);
									select "exepcion, infantes o menus disponibles" as errorD;
							end if;
        
								
                    
				else
					call debug(@educadorid);
					select "exepcion, no existe ese usuario asignado a un educador" as errorD;
			end if;
        
        
		
    ELSE
			call debug(@fecha_consulta_valida);
			select "excepcion de fecha no valida, o no se tiene ingresos de menus semanales para esa semana para ese cibv" as errorD;
END IF;
    



	END
$$
DELIMITER ;


/* use_id, cen_inf_id, periodo_id*/
Call myProcedure(5,1,1);