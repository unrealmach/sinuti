DROP PROCEDURE IF EXISTS Sample1;

DELIMITER $$

CREATE PROCEDURE Sample1()
-- para cada loop se crea un bloque
BLOCK1: begin
    declare v_col1 int;            -- es la variable que contendra el temporal_id de cada row          
    declare no_more_rows1 boolean default FALSE;   -- es la variable que indica el fin el loop
    declare cursor1 cursor for  -- es el curso que sera pasada por el loop
								select m_prep_carta_id from d_carta_semanal where m_carta_semanal_id = 1 and d_c_sem_dia_semana = 
										(
										SELECT 
											 CASE DAYOFWEEK((select now()))
											 WHEN 1 THEN 'DOMINGO'
											 WHEN 2 THEN 'LUNES'
											 WHEN 3 THEN 'MARTES'
											 WHEN 4 THEN 'MIERCOLES'
											 WHEN 5 THEN 'JUEVES'
											 WHEN 6 THEN 'VIERNES'
											 WHEN 7 THEN 'SABADO'
											 END as fecha
										);
    declare continue handler for not found set no_more_rows1 := TRUE;   -- indica si la variable es true entonces se acabo el loop         
    
    open cursor1; -- empieza a recorrer el primer selec con ese cursor
    LOOP1: loop -- inicia el loop
				fetch cursor1 into  v_col1; -- alimenta cada resultado de las filas del cursor a esa variable
               
				if no_more_rows1 then
					close cursor1;
					leave LOOP1;
				end if;
					BLOCK2: begin
						declare v_col2 int;
						declare no_more_rows2 boolean default FALSE;
						declare cursor2 cursor for
												select infante_id from infante;
					   declare continue handler for not found set no_more_rows2 := TRUE;
						
                        open cursor2;
						LOOP2: loop
							fetch cursor2 into  v_col2;
                           
							if no_more_rows2 then
								close cursor2;
								leave LOOP2;
							end if;
                            
                             select v_col1 as 'm_prep_carta_id', v_col2 as 'infante_id';
                             
						end loop LOOP2;
					end BLOCK2;
    end loop LOOP1;
end BLOCK1;
$$
DELIMITER ;

call Sample1();