DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `new_procedure`()
BEGIN

	            SET @check_cambio_correcto := 
    (SELECT IF(estado =  "ACTIVO" AND NOW() BETWEEN concat(periodo.fecha_inicio,' ','00:00:00' )
									AND concat(periodo.fecha_fin,' ','23:59:59'),1,0) as data FROM  `periodo` );
    select @check_cambio_correcto;
    
    IF @check_cambio_correcto = 0 THEN  
		
	    UPDATE  sector_asignado_educador SET  sector_asignado_educador.fecha_fin_actividad =  now()  where sector_asignado_educador.fecha_fin_actividad is NULL; 
	        UPDATE autoridades_cibv set autoridades_cibv.fecha_fin_actividad = now() where autoridades_cibv.fecha_fin_actividad is NULL; 	
	        UPDATE asignacion_gastronomo_distrito set asignacion_gastronomo_distrito.fecha_fin_actividad = now() where asignacion_gastronomo_distrito.fecha_fin_actividad is NULL;    
	         UPDATE asignacion_nutricionista_distrito set asignacion_nutricionista_distrito.fecha_fin_actividad = now() where asignacion_nutricionista_distrito.fecha_fin_actividad is NULL;
       
        
	 UPDATE  `matricula` SET  `matricula_estado` =  'RETIRADO' WHERE  `matricula`.`matricula_estado` ='ACTIVO';
	 UPDATE  `periodo` SET  `estado` =  'INACTIVO' WHERE  `periodo`.`estado` = 'ACTIVO';
        
    END IF;
  END$$

DELIMITER ;