

INSERT INTO `cibv`(`cen_inf_id`, `parroquia_id`, `cen_inf_nombre`, `cen_inf_direccion`, `cen_inf_telefono`, `cen_inf_correo`, `cen_inf_activo`) VALUES
 (1,3, 'MI PRIMER CIBV', 'Ibarra', '0969665851', 'un@d.com', 'SI'),
(2,3, 'CORAZONES FELICES', 'Av. Rafael Sanchez 8-30 y Carlos Emilio Grija', '0986209933', 'lucreciajc2000@hotmail.com', 'SI'),
(3,3, 'CIBV CASITA DE SORPRESAS', 'AV. RAFAEL SANCHEZ 5-92 Y TEODORO GOMEZ', '0985684008', 'revelocarla@yahoo.com', 'SI'),
(4,3, 'CIBV Barrio Central', 'Tena 9-71 y Pelíkano', '062602706', 'xime1963@hotmail.es', 'SI'),
(5,3, 'CIBV YACUCALLE', 'Carlos Emilio Grijalva 10-33 Juana Atabalipa', '062600637', 'janeth.romero@cz.gob.ec', 'SI'),
(6,3, 'LOS RISUEÑOS', 'Santa Rosa del Tejar', '0989055348', 'raquel.ruiz@cz.inclusion.gob.ec', 'SI'),
(7,3, 'JESÚS TE AMA', 'ZUMBA 6-18 Y TENA', '0992388479', 'cartagena.mara@yahoo.com', 'SI'),
(8,3, 'AMAZONAS', 'TEODORO GOMEZ DE LA TORRE Y EUGENIO ESPEJO', '062953069', 'maria.jara@cz.inclusion.gob.ec', 'SI'),
(9,3, 'MIS PEQUEÑOS ANGELITOS', 'Miguel Endara 5-101 y Federico Larrea', '062610790', 'lorenita_pili2011@hotmail.com', 'SI'),
(10,3, 'SIMON BOLIVAR', 'HUERTOS FAMILIARES, CALLE ESMERALDAS Y 13 DE ', '062558558', 'luz.benavides@cz.inclusion.gob.ec', 'SI'),
(11,3, 'CIBV ESTRELLITAS', 'OLMEDO Y MEJIA 3-49', '062603217', 'hiquimene@yahoo.com', 'SI'),
(12,3, 'YUYUCOCHA ', 'JUAN PABLO II Y NARCIZA DE JESUS ', '062650426', 'pa1989@hotmail.es', 'SI'),
(13,3, 'ANGELITOS DE AMOR', 'ISLA STA. MARIA 3-54', '0986108637', 'alba.diaz@cz.inclusion.gob.ec', 'SI'),
(14,3, 'CIBV  Caranqui', 'Los Incas  y Av. Atahualpa', '0980244180', 'chary-martinez@hotmail.com', 'SI'),
(15,3, 'CIBV ANGELITOS NEGROS', 'CHOTA', '0983695427', 'anabelcuasapaz@hotmail.com', 'SI'),
(16,3, 'CIVB CHOCOLATITOS', 'Panamericana Norte via a Pimampiro', '0997693705', 'elena.arce2010@hotmail.com','SI'),
(17,3, 'CARITAS ALEGRES', 'CUBILCHE 2-32 Y SAN PABLO', '0985807289', 'amparo.limaico@cz.inclusion.gob.ec', 'SI'),
(18,3, 'DR. LUIS JARAMILLO PEREZ', 'EUSEBIO BORRERO Y VICENTE ROCAFUERTE ', '062950579', 'anadela.armas@inclusion.gob.ec', 'SI'),
(19,3, 'CIBV LA CANDELARIA', 'ESPINOZA DE LOS MONTEROS 11-28 Y JUANA ATABAL', '0991198564', 'rosmery80@live.com', 'SI'),
(20,3, 'CIBV SAN', ' SECTOR EL MILAGRO CALLE LIMONAL Y CAPULI', '062542163', 'alexadra.anrango@cz.inclusion.gob.ec', 'SI'),
(21,3, 'CIBV RETOÑITOS', 'Av. El Retorno y Río Patate', '0988058574', 'cibvrtos@hotmail.com', 'SI');