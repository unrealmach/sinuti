-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-02-2016 a las 06:02:26
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Truncar tablas antes de insertar `auth_assignment`
--

TRUNCATE TABLE `auth_assignment`;
--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('coordinador', '3', 1438965392),
('coordinador', '6', 1454190963),
('educador', '5', 1453339149),
('gastronomo', '7', 1455313965),
('sysadmin', '1', NULL),
('sysadmin', '2', 1438918974);

--
-- Truncar tablas antes de insertar `auth_item`
--

TRUNCATE TABLE `auth_item`;
--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, NULL, NULL),
('/admin/*', 2, NULL, NULL, NULL, NULL, NULL),
('/asistencia/asistencia/*', 2, NULL, NULL, NULL, 1455307930, 1455307930),
('/cartasemanal/mcartasemanal/*', 2, NULL, NULL, NULL, 1455308421, 1455308421),
('/centroinfantil/cibv/*', 2, NULL, NULL, NULL, 1455307771, 1455307771),
('/centroinfantil/grupocomite/*', 2, NULL, NULL, NULL, 1455307659, 1455307659),
('/composicionnutricional/composicionnutricional/*', 2, NULL, NULL, NULL, 1455308264, 1455308264),
('/composicionnutricional/nutriente/*', 2, NULL, NULL, NULL, 1455308284, 1455308284),
('/composicionnutricional/rangonutriente/*', 2, NULL, NULL, NULL, 1455308301, 1455308301),
('/consumoalimenticio/asignacioninfantecsemanal/*', 2, NULL, NULL, NULL, 1455307060, 1455307060),
('/consumoalimenticio/bitacoraconsumoinfantil/*', 2, NULL, NULL, NULL, 1455496115, 1455496115),
('/individuo/infante/*', 2, NULL, NULL, NULL, 1455308112, 1455308112),
('/individuo/patologia/*', 2, NULL, NULL, NULL, 1455308138, 1455308138),
('/individuo/registrovacunainfante/*', 2, NULL, NULL, NULL, 1455308154, 1455308154),
('/inscripcion/matricula/*', 2, NULL, NULL, NULL, 1455307949, 1455307949),
('/inscripcion/salon/*', 2, NULL, NULL, NULL, 1455307963, 1455307963),
('/metricasindividuo/datoantropometrico/*', 2, NULL, NULL, NULL, 1455308194, 1455308194),
('/metricasindividuo/datoantropometrico/view', 2, NULL, NULL, NULL, 1455385050, 1455385050),
('/metricasindividuo/grupoedad/*', 2, NULL, NULL, NULL, 1455308214, 1455308214),
('/metricasindividuo/tipovacuna/*', 2, NULL, NULL, NULL, 1455308439, 1455308439),
('/nutricion/alimento/*', 2, NULL, NULL, NULL, 1455308238, 1455308238),
('/nutricion/categoria/*', 2, NULL, NULL, NULL, 1455308249, 1455308249),
('/parametrossistema/periodo/*', 2, NULL, NULL, NULL, 1455307793, 1455307793),
('/preparacion_platillo/mplatillo/*', 2, NULL, NULL, NULL, 1455308403, 1455308403),
('/preparacioncarta/mprepcarta/*', 2, NULL, NULL, NULL, 1455308370, 1455308370),
('/preparacioncarta/tiempocomida/*', 2, NULL, NULL, NULL, 1455308314, 1455308314),
('/preparacioncarta/tipopreparacion/*', 2, NULL, NULL, NULL, 1455308335, 1455308335),
('/racionalimenticia/porcionporalimento/*', 2, NULL, NULL, NULL, 1455308386, 1455308386),
('/recursoshumanos/autoridadescibv/*', 2, NULL, NULL, NULL, 1455307838, 1455307838),
('/recursoshumanos/coordinadorcibv/*', 2, NULL, NULL, NULL, 1455308060, 1455308060),
('/recursoshumanos/educador/*', 2, NULL, NULL, NULL, 1455308074, 1455308074),
('/recursoshumanos/sectorasignadoeducador/*', 2, NULL, NULL, NULL, 1455312333, 1455312333),
('actualizar_compania', 2, 'Permite actualizar una compania', NULL, NULL, 1436110497, 1436113259),
('actualizar_compania_propia', 2, 'Permite actualizar la compania unicamente al usuario que creo la compania', 'Actualizar_propia_compania', NULL, 1436113347, 1436117532),
('asignar_infantes_c_semanal', 2, 'Permite que se pueda asignar infantes de un cibv a una carta semanal, para indicar que dicha semana los infantes consumiran los menus indicados en esta', NULL, NULL, 1453940001, 1453940001),
('coordinador', 1, 'Responsable de la institucion, podrá ingresar infantes, ', NULL, NULL, 1436110396, 1453940017),
('coordinador-gad', 1, 'Es el usuario representante del área de desarrollo social del GAD', NULL, NULL, 1454019967, 1454019967),
('crear_compania', 2, 'Permite crear una compania', NULL, NULL, 1436110471, 1436110471),
('educador', 1, 'educador', NULL, NULL, 1453339135, 1453939899),
('gastronomo', 1, 'Aquella persona encargada del area de alimentacion, platillos, menus, alimentos', NULL, NULL, 1438917143, 1438917143),
('nutricion', 1, 'Aquella entidad o persona que esta encargada del area nutricional infantil en el centro o institución\r\nPermitira Gestionar datos antropometricos, medidas y reportes de estos', NULL, NULL, 1438916919, 1438916919),
('permission_admin', 2, 'Permisos para agregar, cambiar, modificar permisos, roles', NULL, NULL, NULL, NULL),
('supervisor', 1, 'Aquella entidad o persona que supervisara a la institucion en el area de nutricion, salud y alimentacion', NULL, NULL, 1438916981, 1438916981),
('sysadmin', 1, 'Puede hacer cualquier cosa en el sistema', NULL, NULL, NULL, 1438918949);

--
-- Truncar tablas antes de insertar `auth_item_child`
--

TRUNCATE TABLE `auth_item_child`;
--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('sysadmin', '/*'),
('sysadmin', '/admin/*'),
('coordinador', '/asistencia/asistencia/*'),
('coordinador', '/cartasemanal/mcartasemanal/*'),
('coordinador', '/centroinfantil/cibv/*'),
('coordinador', '/centroinfantil/grupocomite/*'),
('nutricion', '/composicionnutricional/composicionnutricional/*'),
('nutricion', '/composicionnutricional/nutriente/*'),
('nutricion', '/composicionnutricional/rangonutriente/*'),
('coordinador', '/consumoalimenticio/asignacioninfantecsemanal/*'),
('educador', '/consumoalimenticio/bitacoraconsumoinfantil/*'),
('coordinador', '/individuo/infante/*'),
('educador', '/individuo/patologia/*'),
('educador', '/individuo/registrovacunainfante/*'),
('coordinador', '/inscripcion/matricula/*'),
('coordinador', '/inscripcion/salon/*'),
('educador', '/metricasindividuo/datoantropometrico/*'),
('coordinador', '/metricasindividuo/datoantropometrico/view'),
('coordinador-gad', '/metricasindividuo/datoantropometrico/view'),
('nutricion', '/metricasindividuo/tipovacuna/*'),
('nutricion', '/nutricion/alimento/*'),
('nutricion', '/nutricion/categoria/*'),
('coordinador', '/parametrossistema/periodo/*'),
('gastronomo', '/preparacion_platillo/mplatillo/*'),
('nutricion', '/preparacioncarta/mprepcarta/*'),
('nutricion', '/preparacioncarta/tiempocomida/*'),
('nutricion', '/preparacioncarta/tipopreparacion/*'),
('coordinador-gad', '/recursoshumanos/autoridadescibv/*'),
('coordinador-gad', '/recursoshumanos/coordinadorcibv/*'),
('coordinador', '/recursoshumanos/educador/*'),
('coordinador', '/recursoshumanos/sectorasignadoeducador/*'),
('coordinador', 'asignar_infantes_c_semanal'),
('sysadmin', 'coordinador'),
('sysadmin', 'coordinador-gad'),
('sysadmin', 'crear_compania'),
('sysadmin', 'educador'),
('sysadmin', 'gastronomo'),
('sysadmin', 'nutricion'),
('sysadmin', 'permission_admin'),
('sysadmin', 'supervisor');

--
-- Truncar tablas antes de insertar `auth_rule`
--

TRUNCATE TABLE `auth_rule`;
--
-- Volcado de datos para la tabla `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('Actualizar_propia_compania', 'O:52:"backend\\modules\\rbac\\reglas\\ActualizarPropiaCompania":3:{s:4:"name";s:26:"Actualizar_propia_compania";s:9:"createdAt";i:1436113236;s:9:"updatedAt";i:1436113236;}', 1436113236, 1436113236);

--
-- Truncar tablas antes de insertar `user`
--

TRUNCATE TABLE `user`;
--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'adm5', 'adm5', 'adm5', 'eiuWvmtqm9TO_lUkEd79rqFhCaItkajy', '$2y$13$PPAIf/iunQgUWkmJStEwp.IcpTmn08wDD37BY0Ni.sTYHVAWFTBvC', NULL, 'unrealmach2@hotmail.com', 10, 1435896515, 1435960226),
(2, 'Mauricio Robinson', 'Chamorro Chamorro', 'unrealmach', 'kfA444Wrk5uAZMIrRQGVhYHsV00HGR89', '$2y$13$aVRHzeH2JXDm7OiM8A1SXeQCITYb1NF3eCv44hwSyUE3Bvx8B8Rx2', NULL, 'unrealmach@hotmail.com', 10, 1438918844, 1438918844),
(3, 'coordinador1', 'cordinador1', 'coordinador1', 'ZOe0GHiREM7SL2gF-6QfKHJ_rW-iBig1', '$2y$13$bWI/utKbe1SCm2Yxyz1dF.rqMSvaMno68wzzid5rvKIOsK//WsKte', NULL, 'institucion1@hotmail.com', 10, 1438965380, 1438965380),
(4, 'nutricion1', 'nutricion1', 'nutricion1', 'RtDC8xnapND-sHbwhcKz89rPGb_wTP9w', '$2y$13$dyufqsuuYq5OFCCz2NDvFehtUr8hU6rdhpqrC0kyj07RCKvytKrpq', NULL, 'nutricion@hotmail.com', 10, 1438966565, 1438966565),
(5, 'nombre del educador', 'apellido del educadro', 'educador', 'CLt7sCMpLIJYnUlKthjYyprYoVkN1TKh', '$2y$13$RVQzpVgHMam71H8XTIPxPu12GEMU5haYWuYn5EniRAubTJLGlJA6K', NULL, 'llll@ll.com', 10, 1453339108, 1453339108),
(6, 'Coordinador2', 'CO2', 'coordinador2', 'Dt87JHPwOHDCo5DAKVjQ62P_ujXMfhyl', '$2y$13$J71.c4kLIJN4Cvr7RuDkpOgWGtAN/wiUlJVn1g/bVBNl7IFhr710.', NULL, 'co@co.com', 10, 1454190943, 1454190943),
(7, 'gastronomo1', 'gastronomo1', 'gastronomo1', 'yOiSuBo5Jrq8tWnfw1isf67IDh__wqkB', '$2y$13$1EpEig7AeupHDuWM0wo7kuBMnZhhs2OcUZ4UsXzQ.i.M7G884bjPO', NULL, 'gaa@ll.com', 10, 1455313952, 1455313952);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
