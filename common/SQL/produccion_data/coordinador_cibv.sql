-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2016 a las 21:29:28
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Volcado de datos para la tabla `coordinador_cibv`
--

INSERT INTO `coordinador_cibv` (`coordinador_id`, `coordinador_nombres`, `coordinador_apellidos`, `coordinador_DNI`, `coordinador_contacto`, `coordinador_correo`) VALUES
(1, 'HIPATIA ', 'QUIROZ', '1234567890', '0986209933', 'hipatia_quiroz@mies.com'),
(2, 'LUCRECIA ', 'JÁTIVA', '1234567891', '0991892126', 'lucrecia_jativa@mies.com'),
(3, 'MIRIAM ', 'ROMERO', '1234567892', '0985684008', 'miriam_romero@mies.com'),
(4, 'CARLA ', 'REVELO', '1234567893', '0991198564', 'carla_revelo@mies.com'),
(5, 'ROCÍO ', 'CARLOSAMA', '1234567894', '0980244180', 'rocio_carlosama@mies.com'),
(6, 'ROSARIO ', 'MARTINEZ', '1234567895', '0986108637', 'rosario_martinez@mies.com'),
(7, 'ALBA ', 'DÍAZ', '1234567896', '0992388479', 'alba_dias@mies.com'),
(8, 'MARÍA ', 'CARTAGENA', '1234567897', '0992388479', 'maria_cartagena@mies.com'),
(9, 'LORENA ', 'JUMA', '1234567898', '0997691541', 'lorena_juma@mies.com'),
(10, 'MARIA ', 'BENAVIDES', '1234567899', '0982873968', 'maria_benavides@mies.com'),
(11, 'XIMENA ', 'FREIRE', '1234567881', '0980208297', 'ximena_freire@mies.com'),
(12, 'VERÓNICA ', 'ANRANGO', '1234567882', '0984738037', 'veronica_anrango@mies.com'),
(13, 'JANETH ', 'CANACUÁN', '1234567883', '0981099293', 'janeth_canacuan@mies.com'),
(14, 'MARIA  ', 'CUASAPAZ', '1234567884', '0983695427', 'maria_cuaspaz@mies.com'),
(15, 'ELENE ', 'ARCE', '1234567885', '0997693705', 'elene_arce@mies.com'),
(16, 'AMPARO ', 'LIMAICO', '1234567886', '0985807289', 'amparo_limaico@mies.com'),
(17, 'JOVANA ', 'JARAMILLO', '1234567887', '0988058574', 'jovana_jaramillo@mies.com'),
(18, 'MARILYN ', 'RUIZ', '1234567888', '0989055348', 'marilyn_ruiz@mies.com'),
(19, 'PATRICIA ', 'FLORES ', '1234567889', '062650426', 'patricia_flores@mies.com'),
(20, 'PATRICIA ', 'CASTRO', '1234567880', '0997821028', 'patricia_@mies.com'),
(21, 'BEATRIZ ', 'JARA', '1234567809', '0989199896', 'beatriz_jara@mies.com');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
