-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-03-2016 a las 18:02:05
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Volcado de datos para la tabla `tipo_vacuna`
--

INSERT INTO `tipo_vacuna` (`tipo_vacuna_id`, `tipo_vac_enfermedad`, `tipo_vac_nombre`, `tipo_vac_num_dosis`, `tipo_vac_dosis_canti`, `tipo_vac_dosis_medida`, `tipo_vac_via`) VALUES
(1, 'TUBERCULOSIS', 'BCG', 1, '0.10', 'ML', 'I.D.'),
(2, 'POLIOMIELITIS', 'ANTIPOLIO', 4, '2.00', 'GOTAS', 'V.O.'),
(3, 'DPT+HB+Hib', 'PENTAVALENTE', 3, '0.50', 'ML', 'IM'),
(4, 'HEPATITIS B', 'PENTAVALENTE', 4, '0.50', 'ML', 'IM'),
(5, 'DIARREA RORAVIRUS', 'ROTAVIRUS', 2, '1.00', 'ML', 'V.O.'),
(6, 'NEUMONIA', 'NEUMOCOCO', 3, '0.50', 'ML', 'IM'),
(7, 'SRP', 'TRIPE VIRAL', 3, '0.50', 'ML', 'VS'),
(8, 'FIEBRE AMARILLA', 'FIEBRE AMARILLA', 1, '0.50', 'ML', 'VS');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
