-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-02-2016 a las 17:21:24
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Volcado de datos para la tabla `educador`
--

INSERT INTO `educador` (`educador_id`, `educador_dni`, `educador_nombres`, `educador_apellidos`, `educador_telefono`, `educador_correo`) VALUES
(1, '1003348586', 'MIREYA ELIZABETH', 'AUPAZ TOBAR', '0999999999', 'mireya_aupaz@mies.com'),
(2, '1003149489', 'SONIA MARISOL', 'LIMA MOROCHO', '0999999999', 'sonia_lima@mies.com'),
(3, '1003689311', 'NARCISA DE JESUS', 'BETANCURTH', '0999999999', 'narcisa_betancurth@mies.com'),
(4, '1003008487', 'NANCY YOHANA', 'ROSAS PAEZ', '0999999999', 'nancy_rosas@mies.com'),
(5, '1718026428', 'JANETH ELIZABETH', 'RUALES PACHECO', '0999999999', 'janeth_ruales@mies.com'),
(6, '1001644630', 'GLADYS YOLANDA', 'VALVERDE FLORES', '0999999999', 'gladys_valverde@mies.com'),
(7, '1001933389', 'SHEYLA GANIMEDE', 'BELTRAN PONCE', '0999999999', 'sheyla_beltran@mies.com'),
(8, '1002081048', 'RUTH MARIELA ', 'TELENEMA CRIOLLO', '0999999999', 'ruth_telenema@mies.com'),
(9, '1001779808', 'PATRICIA DEL CONSUELO', 'CARDENAS DIAZ', '0999999999', 'patricia_cardenas@mies.com'),
(10, '1002472783', 'SILVIA JEANETH', 'POZO ALTAMIRANO', '0999999999', 'silvia_pozo@mies.com'),
(11, '1002979357', 'MIRIAM VICTORIA', 'LARA ACOSTA', '0999999999', 'miriam_lara@mies.com'),
(12, '1003339007', 'KARINA VANESA', 'NARVAEZ ORMAZA', '0999999999', 'karinan@mies.com'),
(13, '1004036710', 'DANIELA FERNANDA', 'AUPAZ TOBAR', '0999999999', 'daniela_aupaz@mies.com'),
(14, '1003209994', 'MARTHA ESPERANZA', 'CANACUAN ALDAS', '0999999999', 'martha_canacuan@mies.com'),
(15, '1003214200', 'ANDREA MARIBEL', 'CARVAJAL AYALA', '0999999999', 'andrea_carvajal@mies.com'),
(16, '1002933537', 'JESSICA ELIZABETH', 'ZUÑIGA BURGA', '0999999999', 'jessica_zuniga@mies.com'),
(17, '1001875507', 'AURA ELIZA', 'SALAZAR PULLES', '0999999999', 'aura_salazar@mies.com'),
(18, '0401061619', 'ANA LUCIA', 'CHIRAN QUILISMAL', '0999999999', 'ana_chiran@mies.com'),
(19, '1003082136', 'CECILIA GABRIELA', 'HERRERA ALVEAR', '0999999999', 'cecilia_herrera@mies.com'),
(20, '1002097481', 'MARIA ERMELIDA', 'MINDA LARA', '0999999999', 'maria_minda@mies.com'),
(21, '1002818522', 'MARIA DEL CARMEN', 'NOVOA MINA', '0999999999', 'maria_novoa@mies.com'),
(22, '1001713690', 'MARIA EUGENIA', 'PABON ARMAS', '0999999999', 'maria_pabon@mies.com'),
(23, '1002613881', 'JAELA ROSARIO', 'CONGO CALDERON', '0999999999', 'jaela_congo@mies.com'),
(24, '1002294054', 'MARINA ALEXANDRA', 'JATIVA CASTRO', '0999999999', 'marina_jativa@mies.com'),
(25, '1002520623', 'CECILIA DEL CARMEN', 'SOTO GUERRERO', '0999999999', 'cecilia_soto@mies.com'),
(26, '1003216890', 'MARCIA PATRICIA', 'ROMERO VASQUEZ', '0999999999', 'marcia_romero@mies.com'),
(27, '1002236253', 'ROSA DEL CISNE', 'GUERRERO JOJOA', '0999999999', 'rosa_guerrero@mies.com'),
(28, '1004345086', 'PAMELA ALEJANDRA', 'IBUJES POTOSI', '0999999999', 'pamela_ibujes@mies.com'),
(29, '1003957295', 'KATHERINE DEL PILAR', 'ERAZO TENGANA', '0999999999', 'katherine_erazo@mies.com'),
(30, '1003334206', 'ANGELA BEATRIZ', 'PORTILLA OBANDO', '0999999999', 'angela_portilla@mies.com'),
(31, '1002543625', 'DORIS ADRIANA', 'SANDOVAL PUPIALES', '0999999999', 'doris_sandoval@mies.com'),
(32, '0601949092', 'ENMA MAGDALENA', 'UVIDIA AGUIAR', '0999999999', 'enma_uvidia@mies.com'),
(33, '1003095252', 'NANCY CECIBEL', 'CHUQUIN ALBAN', '0999999999', 'nancy_chuquin@mies.com'),
(34, '1001206422', 'MARIA ELSA', 'HERRERA MOLINA', '0999999999', 'maria_herrera@mies.com'),
(35, '1307932382', 'JENNY MARIBEL', 'VALENCIA MEDRANDA', '0999999999', 'jenny_valencia@mies.com'),
(36, '1003492236', 'SISA ARYNINA', 'VEGA PICUASI', '0999999999', 'sisa_vega@mies.com'),
(37, '1003484068', 'MAYRA ELIZABETH', 'FONTE FARINANGO', '0999999999', 'mayra_fonte@mies.com'),
(38, '1002588331', 'LIGIA ELENA', 'MATANGO JUMA', '0999999999', 'ligia_matango@mies.com'),
(39, '1001977931', 'ROSA LORENA', 'MORALES MONGE', '0999999999', 'rosa_morales@mies.com'),
(40, '1003241989', 'SILVIA PATRICIA', 'ORTIZ VELASTEGUI', '0999999999', 'silvia_ortiz@mies.com'),
(41, '1234567890', 'YADIRA FERNANDA', 'YAPUD CANACUAN', '0999999999', 'yadira_yapud@mies.com'),
(42, '1003703319', 'CARLA DANIELA', 'CHALA ESPINOZA', '0999999999', 'carla_chala@mies.com'),
(43, '1002519526', ' JANETH MARGARITA', 'AGUILAR CEVALLOS', '0999999999', 'janeth_aguilar@mies.com'),
(44, '1002289526', 'GILMA ANATOLIA', 'DE JESUS CHALA', '0999999999', 'gilma_djesus@mies.com'),
(45, '1234567899', 'SILVIA LORENA', 'POMASQUI TITUAÑA', '0999999999', 'silvia_pomasqui@mies.com'),
(46, '1003698170', 'LIDIA ELIZABETH', 'MENDEZ CARCELEN', '0999999999', 'lidia_mendez@mies.com'),
(47, '1001665460', 'MARIA NARCISA', 'VALENCIA MANOSALVAS', '0999999999', 'maria_valencia@mies.com'),
(48, '1003165527', 'VERONICA ELIZABETH', 'SALCEDO SUAREZ', '0999999999', 'veronica_salcedo@mies.com'),
(49, '1234567809', 'KATERINE ROXANA', 'MALDONADO LARA', '0999999999', 'katherine_maldonado@mies.com'),
(50, '1002076816', 'JHIRA INGRITH', 'TERAN CHICAIZA', '0999999999', 'jhira_teran@mies.com'),
(51, '1002927869', 'EVELYN LIZETH', 'MINDA BENAVIDES', '0999999999', 'evelyn_minda@mies.com'),
(52, '1708485337', 'SANDRA VERONICA', 'ANDRADE SIMBAÑA', '0999999999', 'sandra_andrade@mies.com'),
(53, '1310862659', 'LILIANA FERNANDA', 'ESPINOZA RIVADENEIRA', '0999999999', 'liliana_espinoza@mies.com'),
(54, '1001886546', 'CARMITA PATRICIA', 'MENA RIVERA', '0999999999', 'carmita_mena@mies.com'),
(55, '1001724655', 'CARMEN VERONICA', 'VALENZUELA ANDRADE', '0999999999', 'carmen_valenzuela@mies.com'),
(56, '1002255006', 'CARMEN VERONICA', 'CHUGA LOPEZ', '0999999999', 'carmen_chuga@mies.com'),
(57, '1002527446', 'JENNY ANTONIETA', 'LOPEZ AREVALO', '0999999999', 'jenny_lopez@mies.com'),
(58, '1002312278', 'MONICA DEL ROCIO', 'MORENO CHIRIBOGA', '0999999999', 'monica_moreno@mies.com'),
(59, '1004272660', 'NORMA MARIBEL', 'VARGAS TITUAÑA', '0999999999', 'norma_vargas@mies.com'),
(60, '1002240040', 'MIRIAN JEANETH', 'BENALCAZAR PILLAJO', '0999999999', 'mirian_benalcazar@mies.com'),
(61, '0401414966', 'SANDRA PATRICIA', 'CUASQUEN MAYA', '0999999999', 'sandra_cuasquen@mies.com'),
(62, '1002944823', 'SANDRA MARIBEL', 'OÑATE RAMIREZ', '0999999999', 'sandra_onate@mies.com'),
(63, '1002049607', 'ROSA CELINA', 'ORTEGA MUGMAL', '0999999999', 'rosa_ortega@mies.com'),
(64, '401321138', 'FANNY ALEXANDRA', 'ANDRADE LOPEZ', '0999999999', 'fanny_andrade@mies.com'),
(65, '1234567898', 'GERMANIA DEL PILAR', 'PEÑAFIEL HURTADO', '0999999999', 'germania_penafiel@mies.com'),
(66, '1003109426', 'MARIA BELEN', 'HIDALGO TRUJILLO', '0999999999', 'maria_hidalgo@mies.com'),
(67, '1002155735', 'MARIELA PATRICIA', 'LOPEZ PORTILLA', '0999999999', 'mariela_lopez@mies.com'),
(68, '1003702105', 'MARTHA CECILIA', 'ALBAN TUQUERREZ', '0999999999', 'martha_alban@mies.com'),
(69, '1001783115', 'MARIA MAGALI', 'CARCELEN MALDONADO', '0999999999', 'maria_carcelen@mies.com'),
(70, '0400771945', 'GERMANIA ESMERALDA', 'MAFLA LUCERO', '0999999999', 'germania_mafla@mies.com'),
(71, '1754646683', 'MARIA DEL CARMEN CENAIDA', 'ROZO NIÑO', '0999999999', 'maria_rozo@mies.com'),
(72, '1002079984', 'SILVIA LORENA', 'ARCINIEGA MUNOZ', '0999999999', 'silvia_arciniega@mies.com'),
(73, '1002510855', ' MONICA VERENICE', 'CHISCUETH PORTILLA', '0999999999', 'monica_cuiscueth@mies.com'),
(74, '1003985544', 'JHOSELYN LIZBETH', 'ECHEVERRIA BENITEZ', '0999999999', 'jhoselyn_echeverria@mies.com'),
(75, '1709810293', 'LORENA DEL PILAR', 'BENAVIDES OSORIO', '0999999999', 'lorena_benavides@mies.com'),
(76, '1003404660', ' MAYRA MILENA', 'GUEL CAIPE', '0999999999', 'mayra_guel@mies.com'),
(77, '1715104772', 'MARIA TERESA', 'NAZATE', '0999999999', 'maria_nazate@mies.com'),
(78, '1002117735', 'NANCY ESPERANZA', 'PALACIOS HEREDIA', '0999999999', 'nancy_palacios@mies.com'),
(79, '1003326954', 'KARLA SOLEDAD', 'ROSERO CANCHALA', '0999999999', 'karla_rosero@mies.com'),
(80, '1002766309', 'VERONICA PATRICIA', 'SALAZAR TIPAZ', '0999999999', 'veronica_salazar@mies.com'),
(81, '1750440685', 'LILIANA FERNANDA', 'BENAVIDES VALDES', '0999999999', 'liliana_benavides@mies.com'),
(82, '0909810822', 'LOURDES NARCISA', 'BRIONES RENDON', '0999999999', 'lourdes_brion@mies.com'),
(83, '1003232442', 'GABRIELA ROSARIO', 'TORRES MARTINEZ', '0999999999', 'gabriela_torres@mies.com'),
(84, '1234567897', 'SANDRA', 'VELASCO JARRIN', '0999999999', 'sandra_velasco@mies.com'),
(85, '0803083179', 'ELAINE CAROLINA', 'CASTILLO CEDEÑO', '0999999999', 'elaine_castillo@mies.com'),
(86, '1002612677', 'MYRIAN DEL CARMEN', 'HERRERA GUAMAN', '0999999999', 'myrian_herrera@mies.com'),
(87, '1001969946', 'BEKY GUADALUPE', 'JIMENEZ ARMIJOS', '0999999999', 'beky_jimenez@mies.com'),
(88, '1001757135', 'VIVIANA DEL CARMEN', 'MORENO NARVAEZ', '0999999999', 'viviana_moreno@mies.com'),
(89, '1003689310', 'YAJAIRA LIZBETH', 'PEREZ SOTELO', '0999999999', 'yajaira_perez@mies.com'),
(90, '1002771317', 'VERONICA DEL CARMEN', 'CASTRO UTRERAS', '0999999999', 'veronica_castro@mies.com'),
(91, '1002702908', 'CANDIDA MARICELA', 'PEREZ CUASTUMAL', '0999999999', 'candida_perez@mies.com'),
(92, '1002839759', ' ELENA ESTHEFANIA', 'PALACIOS TUSA', '0999999999', 'elena_palacios@mies.com'),
(93, '1001701299', 'CECILIA LUCIA', 'PERUGACHI FLORES', '0999999999', 'cecilia_perugachi@mies.com'),
(94, '0400429130', ' CELIA CRUZ', 'PONCE NAVARRETE', '0999999999', 'celia_ponce@mies.com'),
(95, '1002370219', 'SANDRA ANALI', 'TORRES MORALES', '0999999999', 'sandra_torres@mies.com'),
(96, '1002503579', 'AMANDA ARACELY', ' LOPEZ  YEPEZ', '0999999999', 'amanda_lopez@mies.com');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
