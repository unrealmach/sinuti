-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-02-2016 a las 06:14:53
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Volcado de datos para la tabla `cibv`
--

INSERT INTO `cibv` (`cen_inf_id`, `parroquia_id`, `cen_inf_nombre`, `cen_inf_direccion`, `cen_inf_telefono`, `cen_inf_correo`, `cen_inf_activo`) VALUES
(1, 3, 'Estrellitas', 'Barrio El Sagrario Calle Olmedo 3-51 Entre Borrero Y Mejía', '0999999999', 'estrellitas@cibv.com', 'SI'),
(2, 4, 'Corazones Felices ', 'Yacucalle, Av. Rafael Sánchez 8-30 Y Carlos Emilio Grijalva Esquina Referencia: Frente A La Pista De Bicicletas', '0999999999', 'corazonesfelices@cibv.com', 'SI'),
(3, 4, 'Yacucalle', 'Yacucalle, Calle Carlos Emilio Grijalva 10-33,  Entre Juana Atabalipa  Y Antonio Cordero Referencia: Casa de Aldeas S.O.S', '0999999999', 'yacucalle@cibv.com', 'SI'),
(4, 4, 'Casita de Sorpresas ', 'Yacucalle, Calle Rafael Sánchez 5-92; Entre Teodoro Gómez Y Leoro Franco Referencia: Frente Al Parque De La Familia', '0999999999', 'casitadesorpresas@cibv.com', 'SI'),
(5, 1, 'La Candelaria', 'Caranqui, Av. José Espinoza De Los Monteros 11-28 Entre Juana Atabalipa Y QuiisQuis', '0999999999', 'lacalendaria@cibv.com', 'SI'),
(6, 1, 'Caranqui', 'Caranqui, Calle Los Incas 5-36 Entre  Huiracocha Y Av. Atahualpa', '0999999999', 'caranqui@cibv.com', 'SI'),
(7, 2, 'Angelitos de amor ', 'Azaya, Calle Santa María 3-54 Entre Riobamba Y Ambato  Referencia: Frente A La Casa Del Balón', '0999999999', 'angelitosdeamor@cibv.com', 'SI'),
(8, 2, 'Jesús te ama', 'Azaya, Calle Zumba 6-18 Entre Tena Y Ciudad De Puyo', '0999999999', 'jesusteama@cibv.com', 'SI'),
(9, 4, 'Mis pequeños angelitos', 'Calle Jorge Subia S/N;Referencia: Tras El Cementerio Jardín De Paz', '0999999999', 'mispequesangelitos@cibv.com', 'SI'),
(10, 3, 'Simón Bolívar ', 'Esmeraldas 2-108 Y 13 De Abril', '0999999999', 'simonbolivar@cibv.com', 'SI'),
(11, 2, 'Barrio Central', 'Alpachaca, Calle Tena 9-71 Entre  Pelikano Y Tungurahua', '0999999999', 'barriocentral@cibv.com', 'SI'),
(12, 3, 'SAN El Milagro', 'Barrio El Milagro, Calle El Limonal S/N Entre Capulí Y Aguacate Referencia: Casa Comunal El Milagro', '0999999999', 'san@cibv.com', 'SI'),
(13, 4, 'Caritas Sonrientes', 'Casa Comunal Colinas Del Sur calles ocho de marzo y 18 de marzo', '0999999999', 'caritassonrientes@cibv.com', 'SI'),
(14, 4, 'Angelitos negros ', 'Comunidad De Chota, Referencia: Junto Al Coliseo', '0999999999', 'angelitosnegros@cibv.com', 'SI'),
(15, 6, 'Chocolatitos', 'El Juncal Vía Pimampiro', '0999999999', 'chocolatitos@cibv.com', 'SI'),
(16, 5, 'Caritas Alegres', 'El Priorato, Calle Cubilche 2-32; Referencia: Casa Barrial', '0999999999', 'caritasalegres@cibv.com', 'SI'),
(17, 4, 'Retoñitos', 'Los Ceibos, Av. El Retorno Entre Rio Patate Y Rio Blanco,  Casa Comunal Junto Al Parque De Los Ceibos', '0999999999', 'retoñitos@cibv.com', 'SI'),
(18, 1, 'Risueños', 'Casa Comunal Del Barrio El Tejar,  Referencia: Vía Santa Rosa Del Tejar', '0999999999', 'risueños@cibv.com', 'SI'),
(19, 4, 'Yuyucocha', 'Yuyucocha, Calle Juan Pablo II, Entre Narcisa De Jesús Y Armando Hidrobo Referencia: Junto a La ciudadela De Maestros', '0999999999', 'yuyucocha@cibv.com', 'SI'),
(20, 3, 'Creciendo Felices', 'Calle Ramón Alarcón Entre  Jaime Roldos Aguilera Y Nicolás Hidalgo', '0999999999', 'creciendofelices@cibv.com', 'SI'),
(21, 4, 'Amazonas', 'Av. Teodoro Gómez De La Torre Y Av. Eugenio Espejo', '0999999999', 'amazonas@cibv.com', 'SI');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
