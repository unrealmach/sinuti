
/*
//secuencias de borrado 
*/


/*
borrar alimento
*/

--se eliminan las constraint de las foreingkeys de las tablas del sistema unidas a la tabla de alimentos
ALTER TABLE `composicion_nutricional` DROP FOREIGN KEY `fk_composicion_nutricional_alimento1`; 
ALTER TABLE `d_platillo` DROP FOREIGN KEY `fk_d_platillo_alimento1`;
ALTER TABLE `d_preparacion_carta` DROP FOREIGN KEY `fk_d_preparacion_carta_alimento1`;
ALTER TABLE `porcion_por_alimento` DROP FOREIGN KEY `fk_porcion_por_alimento_alimento1`; 


--una vez eliminado se puede subir denuevo las constraints

ALTER TABLE `composicion_nutricional` ADD CONSTRAINT `fk_composicion_nutricional_alimento1` FOREIGN KEY (`alimento_id`) REFERENCES `sistema_nutrimental_infantil`.`alimento`(`alimento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d_platillo` ADD CONSTRAINT `fk_d_platillo_alimento1` FOREIGN KEY (`alimento_id`) REFERENCES `sistema_nutrimental_infantil`.`alimento`(`alimento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `d_preparacion_carta` ADD CONSTRAINT `fk_d_preparacion_carta_alimento1` FOREIGN KEY (`alimento_id`) REFERENCES `sistema_nutrimental_infantil`.`alimento`(`alimento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `porcion_por_alimento` ADD CONSTRAINT `fk_porcion_por_alimento_alimento1` FOREIGN KEY (`alimento_id`) REFERENCES `sistema_nutrimental_infantil`.`alimento`(`alimento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;