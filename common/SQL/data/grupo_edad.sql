INSERT INTO `grupo_edad` (`grupo_edad_id`,`grupo_edad_descripcion`) VALUES
(1, 'Un año'),
(2, 'Un año y medio'),
(3,  'Dos años'),
(4, 'Dos años y medio'),
(5,  'Tres años'),
(6, 'Tres años y medio'),
(7,  'Cuatro años'),
(8,  'Cuatro años y medio'),
(9, 'Cinco años');