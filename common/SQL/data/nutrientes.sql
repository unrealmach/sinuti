START TRANSACTION;
INSERT INTO `nutriente` (`nutriente_id`, `nutriente_nombre`) VALUES
(3, 'energia_kcal'), 
(4, 'proteinas_g'), 
(5, 'grasa_total_g'), 
(6, 'carbohidratos_g'), 
(7, 'fibra_dietetica_g'), 
(11, 'calcio_mg'), 
(12, 'fosforo_mg'), 
(13, 'hierro_mg'),
(14, 'tiamina_mg'), 
(15, 'riboflavina_mg'), 
(16, 'niacina_mg'), 
(17, 'vitamina_c_mg'),
(18, 'vitamina_a_eqiv_retinol_mcg'), 
(19, 'acidos_grasos_monoinsat_g'), 
(20, 'acidos_grasos_poliinsaturad_g'),
(21, 'acidos_grasos_saturados_g'),
(22, 'colesterol_mg'), 
(23, 'potasio_mg'), 
(24, 'sodio_mg'), 
(25, 'zinc_mg'), 
(26, 'magnesio_mg'), 
(27, 'vitamina_b6_mg'), 
(28, 'vitamina_b12_mcg'), 
(29, 'folato_mcg'); 

Commit