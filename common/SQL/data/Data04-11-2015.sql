-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-11-2015 a las 17:50:16
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_nutrimental_infantil`
--

--
-- Truncar tablas antes de insertar `alimento`
--

TRUNCATE TABLE `alimento`;
--
-- Truncar tablas antes de insertar `asignacion_infante_c_semanal`
--

TRUNCATE TABLE `asignacion_infante_c_semanal`;
--
-- Truncar tablas antes de insertar `asignacion_inf_c_semanal_has_infante`
--

TRUNCATE TABLE `asignacion_inf_c_semanal_has_infante`;
--
-- Truncar tablas antes de insertar `asignacion_user_cibv`
--

TRUNCATE TABLE `asignacion_user_cibv`;
--
-- Volcado de datos para la tabla `asignacion_user_cibv`
--

INSERT INTO `asignacion_user_cibv` (`asignacion_user_cibv_id`, `user_id`, `centro_infantil_id`) VALUES
(1, 5, 1);

--
-- Truncar tablas antes de insertar `asistencia`
--

TRUNCATE TABLE `asistencia`;
--
-- Truncar tablas antes de insertar `auth_assignment`
--

TRUNCATE TABLE `auth_assignment`;
--
-- Volcado de datos para la tabla `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('institucion', '3', 1438965392),
('institucion', '5', 1446234831),
('sysadmin', '1', NULL),
('sysadmin', '2', 1438918974);

--
-- Truncar tablas antes de insertar `auth_item`
--

TRUNCATE TABLE `auth_item`;
--
-- Volcado de datos para la tabla `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*', 2, NULL, NULL, NULL, NULL, NULL),
('/admin/*', 2, NULL, NULL, NULL, NULL, NULL),
('/centro_infantil/*', 2, NULL, NULL, NULL, 1446652647, 1446652647),
('/settings/*', 2, NULL, NULL, NULL, 1436137414, 1436137414),
('Accesos_Usuario_Cibv', 2, 'AccesosUsuarioCibv', 'AccesosUsuarioCibv', NULL, 1446236949, 1446237063),
('actualizar_compania', 2, 'Permite actualizar una compania', NULL, NULL, 1436110497, 1436113259),
('actualizar_compania_propia', 2, 'Permite actualizar la compania unicamente al usuario que creo la compania', 'Actualizar_propia_compania', NULL, 1436113347, 1436117532),
('crear_compania', 2, 'Permite crear una compania', NULL, NULL, 1436110471, 1436110471),
('gastronomo', 1, 'Aquella persona encargada del area de alimentacion, platillos, menus, alimentos', NULL, NULL, 1438917143, 1438917143),
('index_cibv', 2, 'index_cibv', NULL, NULL, 1446237527, 1446237527),
('institucion', 1, 'Responsable de la institucion, podrá ingresar infantes, ', NULL, NULL, 1436110396, 1438917261),
('nutricion', 1, 'Aquella entidad o persona que esta encargada del area nutricional infantil en el centro o institución\r\nPermitira Gestionar datos antropometricos, medidas y reportes de estos', NULL, NULL, 1438916919, 1438916919),
('permission_admin', 2, 'Permisos para agregar, cambiar, modificar permisos, roles', NULL, NULL, NULL, NULL),
('supervisor', 1, 'Aquella entidad o persona que supervisara a la institucion en el area de nutricion, salud y alimentacion', NULL, NULL, 1438916981, 1438916981),
('sysadmin', 1, 'Puede hacer cualquier cosa en el sistema', NULL, NULL, NULL, 1438918949);

--
-- Truncar tablas antes de insertar `auth_item_child`
--

TRUNCATE TABLE `auth_item_child`;
--
-- Volcado de datos para la tabla `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('sysadmin', '/*'),
('institucion', '/admin/*'),
('sysadmin', '/admin/*'),
('institucion', 'Accesos_Usuario_Cibv'),
('sysadmin', 'crear_compania'),
('sysadmin', 'gastronomo'),
('institucion', 'index_cibv'),
('sysadmin', 'institucion'),
('sysadmin', 'nutricion'),
('sysadmin', 'permission_admin'),
('sysadmin', 'supervisor');

--
-- Truncar tablas antes de insertar `auth_rule`
--

TRUNCATE TABLE `auth_rule`;
--
-- Volcado de datos para la tabla `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('AccesosUsuarioCibv', 'O:46:"backend\\modules\\rbac\\reglas\\AccesosUsuarioCibv":3:{s:4:"name";s:18:"AccesosUsuarioCibv";s:9:"createdAt";i:1446236875;s:9:"updatedAt";i:1446236875;}', 1446236875, 1446236875),
('Actualizar_propia_compania', 'O:52:"backend\\modules\\rbac\\reglas\\ActualizarPropiaCompania":3:{s:4:"name";s:26:"Actualizar_propia_compania";s:9:"createdAt";i:1436113236;s:9:"updatedAt";i:1436113236;}', 1436113236, 1436113236);

--
-- Truncar tablas antes de insertar `autoridades_cibv`
--

TRUNCATE TABLE `autoridades_cibv`;
--
-- Truncar tablas antes de insertar `categoria`
--

TRUNCATE TABLE `categoria`;
--
-- Truncar tablas antes de insertar `cibv`
--

TRUNCATE TABLE `cibv`;
--
-- Volcado de datos para la tabla `cibv`
--

INSERT INTO `cibv` (`cen_inf_id`, `cen_inf_nombre`, `cen_inf_direccion`, `cen_inf_telefono`, `cen_inf_correo`, `cen_inf_activo`) VALUES
(1, 'Primer centro infantil', 'Ibarra El sagrario 1111', '098883338', 'dasdas@ll.com', 'SI'),
(2, 'segundo centro infantil', 'tulcan', '0009999', 'tu@loc.com', 'SI');

--
-- Truncar tablas antes de insertar `comite_padres_flia`
--

TRUNCATE TABLE `comite_padres_flia`;
--
-- Truncar tablas antes de insertar `composicion_nutricional`
--

TRUNCATE TABLE `composicion_nutricional`;
--
-- Truncar tablas antes de insertar `contrato`
--

TRUNCATE TABLE `contrato`;
--
-- Truncar tablas antes de insertar `coordinador_cibv`
--

TRUNCATE TABLE `coordinador_cibv`;
--
-- Truncar tablas antes de insertar `dato_antropometrico`
--

TRUNCATE TABLE `dato_antropometrico`;
--
-- Truncar tablas antes de insertar `d_carta_semanal`
--

TRUNCATE TABLE `d_carta_semanal`;
--
-- Truncar tablas antes de insertar `d_preparacion_carta`
--

TRUNCATE TABLE `d_preparacion_carta`;
--
-- Truncar tablas antes de insertar `educador`
--

TRUNCATE TABLE `educador`;
--
-- Volcado de datos para la tabla `educador`
--

INSERT INTO `educador` (`educador_id`, `educador_cedula`, `educador_nombres`, `educador_apellidos`, `educador_telefono`, `fecha_inicio_actividad`, `fecha_fin_actividad`) VALUES
(1, '1003007802', 'AMANDA SSOFÍA', 'MEJÍA ANDRADE', '09993993', '2015-10-01', NULL);

--
-- Truncar tablas antes de insertar `grupo_comite`
--

TRUNCATE TABLE `grupo_comite`;
--
-- Truncar tablas antes de insertar `grupo_edad`
--

TRUNCATE TABLE `grupo_edad`;
--
-- Volcado de datos para la tabla `grupo_edad`
--

INSERT INTO `grupo_edad` (`grupo_edad_id`, `grupo_edad_descripcion`) VALUES
(1, 'De 0 a 1 año');

--
-- Truncar tablas antes de insertar `habito_alimenticio`
--

TRUNCATE TABLE `habito_alimenticio`;
--
-- Truncar tablas antes de insertar `infante`
--

TRUNCATE TABLE `infante`;
--
-- Volcado de datos para la tabla `infante`
--

INSERT INTO `infante` (`infante_id`, `infante_cedula_o_pasaporte`, `infante_nombres`, `infante_apellidos`, `infante_nacionalidad`, `infante_genero`, `infante_etnia`, `infantes_fecha_nacimiento`, `infante_represent_documento`, `infante_represent_nombres`, `infante_represent_apellidos`, `infante_represent_parentesco`, `infante_direccion_domiciliaria`, `infante_represent_telefono`, `infante_represent_correo`) VALUES
(1, '1004042469', 'MAURICIO', 'CHAMORRO', 'Ecuatoriano', 'MASCULINO', 'MESTIZO', '2015-06-25', '0400288775', 'RAUL', 'CHAMORRO', 'PAPA', 'Ibarra', '099993', 'dasdasd@ss.com'),
(2, '1032030303', 'MADONA', 'REALPE', 'Colombiana', 'FEMENINO', 'AFROAMERICANO', '2015-06-10', '099939393', 'MAMA', 'MAMA  APELLIDO', 'MAMA', 'tulca', '0999999', 'ii@ll.com');

--
-- Truncar tablas antes de insertar `matricula`
--

TRUNCATE TABLE `matricula`;
--
-- Volcado de datos para la tabla `matricula`
--

INSERT INTO `matricula` (`matricula_id`, `cen_inf_id`, `infante_id`, `periodo_id`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1);

--
-- Truncar tablas antes de insertar `m_carta_semanal`
--

TRUNCATE TABLE `m_carta_semanal`;
--
-- Truncar tablas antes de insertar `m_prep_carta`
--

TRUNCATE TABLE `m_prep_carta`;
--
-- Truncar tablas antes de insertar `nutriente`
--

TRUNCATE TABLE `nutriente`;
--
-- Truncar tablas antes de insertar `patologia`
--

TRUNCATE TABLE `patologia`;
--
-- Truncar tablas antes de insertar `periodo`
--

TRUNCATE TABLE `periodo`;
--
-- Volcado de datos para la tabla `periodo`
--

INSERT INTO `periodo` (`periodo_id`, `fecha_inicio`, `fecha_fin`) VALUES
(1, '2015-11-01', '2015-12-06');

--
-- Truncar tablas antes de insertar `porcion_por_alimento`
--

TRUNCATE TABLE `porcion_por_alimento`;
--
-- Truncar tablas antes de insertar `porcion_por_categoria`
--

TRUNCATE TABLE `porcion_por_categoria`;
--
-- Truncar tablas antes de insertar `rango_nutriente`
--

TRUNCATE TABLE `rango_nutriente`;
--
-- Truncar tablas antes de insertar `registro_vacuna_infante`
--

TRUNCATE TABLE `registro_vacuna_infante`;
--
-- Truncar tablas antes de insertar `salon`
--

TRUNCATE TABLE `salon`;
--
-- Volcado de datos para la tabla `salon`
--

INSERT INTO `salon` (`salon_id`, `educador_id`, `periodo_id`, `infante_id`, `grupo_edad_id`) VALUES
(1, 1, 1, 1, 1);

--
-- Truncar tablas antes de insertar `tiempo_comida`
--

TRUNCATE TABLE `tiempo_comida`;
--
-- Truncar tablas antes de insertar `tipo_preparacion`
--

TRUNCATE TABLE `tipo_preparacion`;
--
-- Truncar tablas antes de insertar `tipo_vacuna`
--

TRUNCATE TABLE `tipo_vacuna`;
--
-- Truncar tablas antes de insertar `user`
--

TRUNCATE TABLE `user`;
--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'adm5', 'adm5', 'adm5', 'eiuWvmtqm9TO_lUkEd79rqFhCaItkajy', '$2y$13$PPAIf/iunQgUWkmJStEwp.IcpTmn08wDD37BY0Ni.sTYHVAWFTBvC', NULL, 'unrealmach2@hotmail.com', 10, 1435896515, 1435960226),
(2, 'Mauricio Robinson', 'Chamorro Chamorro', 'unrealmach', 'kfA444Wrk5uAZMIrRQGVhYHsV00HGR89', '$2y$13$aVRHzeH2JXDm7OiM8A1SXeQCITYb1NF3eCv44hwSyUE3Bvx8B8Rx2', NULL, 'unrealmach@hotmail.com', 10, 1438918844, 1438918844),
(3, 'institucion1', 'institucion1', 'institucion1', 'ZOe0GHiREM7SL2gF-6QfKHJ_rW-iBig1', '$2y$13$bWI/utKbe1SCm2Yxyz1dF.rqMSvaMno68wzzid5rvKIOsK//WsKte', NULL, 'institucion1@hotmail.com', 10, 1438965380, 1438965380),
(4, 'nutricion1', 'nutricion1', 'nutricion1', 'RtDC8xnapND-sHbwhcKz89rPGb_wTP9w', '$2y$13$dyufqsuuYq5OFCCz2NDvFehtUr8hU6rdhpqrC0kyj07RCKvytKrpq', NULL, 'nutricion@hotmail.com', 10, 1438966565, 1438966565),
(5, 'institucion1', 'institucion1', 'institucion2', 'ai1dif6cdC19DcfvBX-BQgIGrj3kK0iR', '$2y$13$qqj0p3FnoYZhfJMXnEMCqOSYm0bhp.Ek/OQZx6Wu3WesbckLUSkRO', NULL, 'i@user.com', 10, 1446234776, 1446234776);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
