ALTER TABLE salon ADD cen_inf_id INT(11);
ALTER TABLE salon ADD CONSTRAINT fk_salon_cibv FOREIGN KEY (cen_inf_id) 
REFERENCES cibv (cen_inf_id) ON UPDATE RESTRICT ON DELETE CASCADE;

UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 3;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 4;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 5;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 6;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 7;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 8;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 9;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 10;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 11;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 12;
UPDATE salon SET cen_inf_id = 22 WHERE salon_id = 13; 

ALTER TABLE salon MODIFY cen_inf_id INT NOT NULL;

