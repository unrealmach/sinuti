DROP TRIGGER IF EXISTS asignacion_nutricionista_distrito_change_fecha_fin_AFTER_UPDATE ;

DELIMITER $$

CREATE  TRIGGER asignacion_nutricionista_distrito_change_fecha_fin_AFTER_UPDATE 
 AFTER UPDATE ON `asignacion_nutricionista_distrito` 
 FOR EACH ROW
BEGIN
 IF coalesce(old.fecha_fin_actividad,'') <> coalesce(new.fecha_fin_actividad,'') THEN
	SET @asignacion_nutricionista_distrito_id := OLD.asignacion_nutricionista_distrito_id;
	SET @nutricionista_id := OLD.nutricionista_id;
    SET @user_id := (select id from user where email = (select nutricionista_correo from nutricionista where nutricionista_id = @nutricionista_id));
-- autoridades_cibv_change_fecha_fin_AFTER_UPDATEselect @user_id;
    SET @observacion:= 'Registro de fecha fin';
    
    INSERT INTO `asignacion_user_nutricionista_distrito` ( `user_id`, `asignacion_nutricionista_distrito_id`, `fecha`, `observaciones`) 
    VALUES ( @user_id ,@asignacion_nutricionista_distrito_id , now(),@observacion);
    
 END IF;
END$$
DELIMITER ;