
DROP TRIGGER IF EXISTS asignacion_gastronomo_distrito_change_fecha_fin_AFTER_UPDATE ;

DELIMITER $$

CREATE  TRIGGER asignacion_gastronomo_distrito_change_fecha_fin_AFTER_UPDATE 
 AFTER UPDATE ON `asignacion_gastronomo_distrito` 
 FOR EACH ROW
BEGIN
 IF coalesce(old.fecha_fin_actividad,'') <> coalesce(new.fecha_fin_actividad,'') THEN
	SET @asignacion_gastronomo_distrito_id := OLD.asignacion_gastronomo_distrito_id;
	SET @gastronomo_id := OLD.gastronomo_id;
    SET @user_id := (select id from user where email = (select gastronomo_correo from gastronomo where gastronomo_id = @gastronomo_id));
-- autoridades_cibv_change_fecha_fin_AFTER_UPDATEselect @user_id;
    SET @observacion:= 'Registro de fecha fin';
    
    INSERT INTO `asignacion_user_gastronomo_distrito` ( `user_id`, `asignacion_gastronomo_distrito_id`, `fecha`, `observaciones`) 
    VALUES ( @user_id ,@asignacion_gastronomo_distrito_id , now(),@observacion);
    
 END IF;
END$$
DELIMITER ;
