DROP TRIGGER IF EXISTS user_change_email_AFTER_UPDATE ;

DELIMITER $$

CREATE  TRIGGER user_change_email_AFTER_UPDATE 
 AFTER UPDATE ON `user` 
 FOR EACH ROW
BEGIN
	SET @email_old := OLD.email;
    SET @email_new := NEW.email;
    
     UPDATE gastronomo set gastronomo.gastronomo_correo = @email_new  where gastronomo.gastronomo_correo = @email_old;
     UPDATE nutricionista set nutricionista.nutricionista_correo = @email_new  where nutricionista.nutricionista_correo = @email_old;
     UPDATE coordinador_cibv set coordinador_cibv.coordinador_correo = @email_new  where coordinador_cibv.coordinador_correo = @email_old;
     UPDATE educador set educador.educador_correo = @email_new  where educador.educador_correo = @email_old;
     

END$$
DELIMITER ;