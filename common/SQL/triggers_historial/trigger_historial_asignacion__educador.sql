DROP TRIGGER IF EXISTS sector_asignado_educador_change_fecha_fin_AFTER_UPDATE ;

DELIMITER $$

CREATE  TRIGGER sector_asignado_educador_change_fecha_fin_AFTER_UPDATE 
 AFTER UPDATE ON `sector_asignado_educador` 
 FOR EACH ROW
BEGIN
 IF coalesce(old.fecha_fin_actividad,'') <> coalesce(new.fecha_fin_actividad,'') THEN
	SET @sector_asignado_educador_id := OLD.sector_asignado_educador_id;
	SET @educador_id := OLD.educador_id;
    SET @user_id := (select id from user where email = (select educador_correo from educador where educador_id = @educador_id));
-- autoridades_cibv_change_fecha_fin_AFTER_UPDATEselect @user_id;
    SET @observacion:= 'Registro de fecha fin';
    
    INSERT INTO `asignacion_user_educador_cibv` ( `user_id`, `sector_asignado_educador_id`, `fecha`, `observaciones`) 
    VALUES ( @user_id ,@sector_asignado_educador_id , now(),@observacion);
    
 END IF;
END$$
DELIMITER ;