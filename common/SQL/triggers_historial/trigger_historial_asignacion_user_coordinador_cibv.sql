DROP TRIGGER IF EXISTS autoridades_cibv_change_fecha_fin_AFTER_UPDATE ;

DELIMITER $$

CREATE  TRIGGER autoridades_cibv_change_fecha_fin_AFTER_UPDATE 
 AFTER UPDATE ON `autoridades_cibv` 
 FOR EACH ROW
BEGIN
 IF coalesce(old.fecha_fin_actividad,'') <> coalesce(new.fecha_fin_actividad,'') THEN
	SET @autoridad_cibv_id := OLD.autoridades_cibv_id;
	SET @coordinador_id := OLD.coordinador_id;
    SET @user_id := (select id from user where email = (select coordinador_correo from coordinador_cibv where coordinador_id = @coordinador_id));
-- autoridades_cibv_change_fecha_fin_AFTER_UPDATEselect @user_id;
    SET @observacion:= 'Registro de fecha fin';
    
    INSERT INTO `asignacion_user_coordinador_cibv`( `user_id`, `autoridades_cibv_id`, `fecha`, `observaciones`) 
    VALUES ( @user_id ,@autoridad_cibv_id , now(),@observacion);
    
 END IF;
END$$
DELIMITER ;