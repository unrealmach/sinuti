<?php
namespace common\models\recursos;

use Yii;
use yii\helpers\ArrayHelper;

class DynamicFormModel extends \yii\base\Model
{ /**
 * Creates and populates a set of models.
 * @param string $modelClass
 * @param array $multipleModels
 * @return array     */

    public static function createMultiple($modelClass, $multipleModels = [], $id1 = null, $id2 = null)
    {
        $model = new $modelClass;
        $formName = $model->formName();
        $post = Yii::$app->request->post($formName);
        $models = [];
        if (!empty($multipleModels)) {
            if ($id1 == null || $id2 == null) {
                $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            } else {
                $keys = array_keys(ArrayHelper::map($multipleModels, $id1, $id2));
            }
            $multipleModels = array_combine($keys, $multipleModels);
        }
        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if ($id1 == null || $id2 == null) {
                    if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                        $models[] = $multipleModels[$item['id']];
                    } else {
                        $models[] = new $modelClass;
                    }
                }else{
                     if (isset($item[$id1]) && !empty($item[$id1]) && isset($multipleModels[$item[$id1]])) {
                        $models[] = $multipleModels[$item[$id1]];
                    } else {
                        $models[] = new $modelClass;
                    }
                }
            }
        }
        unset($model, $formName, $post);
        return $models;
    }
}
