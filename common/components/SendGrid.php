<?php
namespace common\components;

use Yii;
use yii\base\Component;

/**
 * Utilitario para manejar metodos globales
 */
class SendGrid extends Component
{

    private $templateIds = ['creacion_usuario' => "f8d41f5b-f395-45f7-8e54-7cf1826ae016"];
//    private $apiKey = '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';
    private $apiKey = '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';
    private $emailFrom = 'sinutisoporte@gmail.com';
    public $turnOn = true;
    private $options = [
        'turn_off_ssl_verification' => false,
        'protocol' => 'https',
        'host' => 'api.sendgrid.com',
        'endpoint' => '/api/mail.send.json',
        'port' => '587',
//            'encryption' => 'tls',
        'url' => null,
        'raise_exceptions' => false
    ];

    /**
     * Envia un email con sendgrid
     * @author Mauricio Chamorro
     * @param type $emailTo destino o destino como array [user1@ss.com,user2@ss.com]
     * @param type $subject tema
     * @param type $text texto
     * @param type $htmlBody cuerpo en html
     */
    public function sendEmailSendGrid($emailTo, $subject, $text, $htmlBody, $template_id = null)
    {

        if ($this->turnOn) {
            try {
                $sendgrid = new \SendGrid($this->apiKey);
                $email =  new \SendGrid\Mail\Mail();
                $email->setFrom($this->emailFrom);
                foreach ($emailTo as $destino) {
                    $email->addTo($destino,"New user");
                
                }
                $email->setSubject($subject);

                $email->addContent('text/html',$htmlBody);
                
                if (!is_null($template_id)) {
                    $email->setTemplateId($template_id);
                }
                
                $sendgrid->send($email, $this->options);
            } catch (\SendGrid\Exception $e) {
                Yii::$app->getSession()->setFlash('error', $e->getCode());
                echo $e->getCode();
                foreach ($e->getErrors() as $er) {
                    Yii::$app->getSession()->setFlash('warning', $er);
                    echo $er;
                }
            }

            Yii::$app->getSession()->setFlash('success', "Correcto, email enviado");
        } else {
            Yii::$app->getSession()->setFlash('warning', "Correcto, pero sin envio de email");
        }
    }

    public function getIdTemplate($tipo)
    {

        return $this->templateIds[$tipo];
    }
}

