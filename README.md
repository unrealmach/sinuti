http://yii2.sin.gadi.ec/ ->frontend
http://yii2.sin.gadi.ec/admin -> backend



Activar rewrite_module en los modulos de apache

Se debe de activar la opcion de hosts virtuales para apache, dicho host debe de ser como la siguiente configuración:

<VirtualHost *:80>
 ServerName yii2.sin.gadi.ec
  ServerAlias yii2.sin.gadi.ec 
   DocumentRoot "g:/Temporales/PHP/sinuti" 
     <Directory "g:/Temporales/PHP/sinuti"> 
        AllowOverride All
     </Directory>
 </VirtualHost>


Ademas de que el archivo en hosts debe de tener una direccion apuntando al localhost


127.0.0.1 yii2.sin.gadi.ec

-------------

Requiere instalar extensiones

para php 7.2 -> actualizar a yii 2.0.13
Ojo -> el bower para idea v3, mixit up y slideReveal fue removido de composer, es necesario copiarlos del vendor original