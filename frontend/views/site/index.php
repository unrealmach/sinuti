<?php
/* @var $this yii\web\View */
$this->title = 'Clientes';
use yii\helpers\Html;

?>
<div class="site-index">

    <div class="owl-carousel content-slider-with-controls-autoplay">
    
    <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/1.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/1.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>


        <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/4.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/4.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>

        <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/2.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/2.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>

        <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/3.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/3.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>
        <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/5.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/5.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>
        <div class="overlay-container">
            <center>
                <?= Html::img('@web/img/v2.2/slider-ini-index/6.jpg') ?>
                <a href="<?= Yii::getAlias('@web/img/v2.2/slider-ini-index/6.jpg') ?>" class="popup-img overlay" title=""><i class="fa fa-search-plus"></i></a>
            </center>
        </div>

    </div>
    <div class="space-bottom"></div>


    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">

                <h2 align="justify" >¿En qué consiste? </h2>
                <p align="justify">
                    Es un software web profesional que permite llevar los registros alimentarios de los infantes en un centro de desarrollo integral infantil; crea, supervisa menús y platillos saludables según los estándares del Ministerio de Salud Pública.
</p>
<p align="justify">    
En cuanto a porcentajes de alimentación. Da a conocer la composición nutricional de los ingredientes, preparaciones y planificaciones de alimentación semanal.  También, calcula el el déficit o exceso de nutrientes en la alimentación de un infante en un día en específico.
</p>
<p align="justify">    
Converge los procesos de recopilación, análisis y decisión de la nutrición y salud infantil en una sola herramienta, para mejorar la calidad de vida de los infantes disminuyendo los problemas de nutrición desde una temprana edad.
</p>
<p align="justify">    
Además,Fusiona el manejo del talento humano de su institución con las obligaciones alimentarias de los infantes, asignando funciones específicas a: educadores, coordinadores, supervisores, nutricionistas y gastronomos, todo esto en pro del bienestar y salud  del niño(a).

                </p>

            </div>
            <div class="col-lg-6">
                <h2 align="justify" >Sinuti</h2>

                <p align="justify">Se enmarca directamente con el objetivo 3 “Salud y Bienestar” de los ODS (Objetivos de Desarrollo Sostenible) de la ONU (Organización de las Naciones Unidas) y con el Plan Nacional Toda una Vida 2017-2021 del Ecuador los cuales tienen como objetivo erradicar la desnutrición infantil promoviendo una alimentación nutritiva y saludable.
                </p>
                
            </div>
        </div>

    </div>
</div>
