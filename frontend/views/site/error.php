<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;

?>
<!-- Main content -->
<section class="main-container">

    <div class="container">
        <div class="row">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-6 col-md-offset-3">
                <h1 class="title"><b> Atención:   <?= nl2br(Html::encode($message)) ?> </b></h1>
                <br>

                <p>
                    <?php echo $exception->errorInfo[2] ?>
                </p>
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Regresar'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
            </div>
            <!-- main end -->

        </div>

    </div>




</section>
