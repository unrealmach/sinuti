<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

?>
<section class="main-container light-translucent-bg">
    <div class="container">
        <div class="row">
            <div class="main col-md-8 col-md-offset-2">
                <div class="logo">
                    <a href="/"><img id="logo" src='%baseUrl%/images/logo_red.png' alt="iDea"></a>
                </div>
                <div class="site-slogan">
                    Sistema Nutrimental Infantil
                </div>
                <div class="object-non-visible animated object-visible fadeInDownSmall" data-animation-effect="fadeInDownSmall" data-effect-delay="300">
                    <div class="form-block center-block">
                        <h2 class="title">Ingresar</h2>
                        <hr>
                        <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-horizontal']]); ?>
                        <?=
                            $form->field($model, 'username', ['options' => ['class' => 'form-group has-feedback'],
                                'template' => '{label}<div class="col-sm-8">{input}<i class="fa fa-user form-control-feedback"></i>{hint}{error}</div>'])
                            ->textInput(array('placeholder' => 'Usuario'))
                            ->label(null, ['class' => 'col-sm-3 control-label'])
                        ?>
                        <?=
                            $form->field($model, 'password', ['options' => ['class' => 'form-group has-feedback'],
                                'template' => '{label}<div class="col-sm-8">{input}<i class="fa fa-lock form-control-feedback"></i>{hint}{error}</div>'])
                            ->passwordInput(array('placeholder' => 'Contraseña'))
                            ->label(null, ['class' => 'col-sm-3 control-label'])
                        ?>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8">
                                <?=
                                    $form->field($model, 'rememberMe', ['template' => '<div class="checkbox">{label}{input}hint}{error}</div>'])
                                    ->checkbox()
                                ?>
                                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>
                                <div class="form-group">
                                    <?= Html::submitButton('Log In', ['class' => 'btn btn-group btn-default btn-sm', 'name' => 'login-button']) ?>
                                </div>
                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
