<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\LoginForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

//$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
$model         = new LoginForm();
?>

<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>SINUTI</b></a>
        <h4>Sistema de valoración nutrimental infantil</h4>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Logueate para empezar tu sesión</p>
        <?php
        $form          = ActiveForm::begin(['id' => 'login-form',
                'enableClientValidation' => false,
                'action' => '/user-management/auth/login',
                'class' => 'login-form']);
        ?>
        <div class="form-group has-feedback">
            <?=
                $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('username')])
            ?>
        </div>
        <div class="form-group has-feedback">
            <?=
                $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
            ?>
        </div>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div class="divider"></div>
        <?=
        Html::submitButton('Log in',
            ['class' => 'btn btn-group btn-dark btn-sm', 'name' => 'login-button'])
        ?>
<?php ActiveForm::end(); ?>
    </div>
</div><!-- /.login-box -->
