<?php

use yii\helpers\Html;

?>

<footer id="footer">
    <div class="footer">

        <div class=" text-muted footer-bottom clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="owl-carousel clients">

                            <div class="client" style="    border-radius: 24%;background-color: white">
                                <a href="http://www.inclusion.gob.ec/" target="blank">
                                    <?= Html::img('@web/img/logo_mies.png') ?>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-5 ">
                        <blockquote class="inline">
                            <p class="margin-clear">-Comer es una necesidad, pero comer de forma inteligente es un arte.-La Rochefoucauld.</p>

                        </blockquote>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="subfooter">

            <div class="container"  style="text-align:center; width:100%">
                <div class="row">
                    <div class="col-md-12">
                        <p>Copyright © 2019 All Rights Reserved</p>
                    </div>

                </div>
            </div>
    </div>
</footer>
