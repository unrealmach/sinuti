<?php

use frontend\assets\AppAsset;
use frontend\assets\BehaviorsAsset;
use frontend\assets\FixesAsset;
use frontend\widgets\Alert;
use yii\bootstrap\Modal;
// use frontend\assets\UpdateYiiAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

// UpdateYiiAsset::register($this);
$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;

BehaviorsAsset::register($this);
FixesAsset::register($this);

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language?>">
<head>
    <meta charset="<?=Yii::$app->charset?>">
    <link rel="shortcut icon" type="image/ico" href="<?=Yii::getAlias('@web') . "/img/v2.2/favicon_32x32.ico"?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>-->
    <?=Html::csrfMetaTags()?>
    <title><?=Html::encode($this->title)?></title>
    <?php $this->head()?>
</head>
<body class="front no-trans">
      <?php
Modal::begin([
    'id' => 'mainModal',
    'headerOptions' => [
        'id' => 'mainModalHeader',
        'style' => 'text-align:center',
    ],
    'header' =>
    '<h4 class="modal-title" id="labelMainModalHeader">LOADING... </h4>',
    'footer' => '<button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Close</button>',
    'footerOptions' => [
        'id' => 'mainModalFooter',
    ],
    'size' => 'modal-lg',
    'class'=> 'modal fade',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false], //para que se cierre unicamente con la x
]);

?>
    <!--contenido del modal-->
    <div style=" width:40%" class="center">
         <?=Html::img('@web/img/v2.2/gif-cargando.gif')?>
        </div>
    <!--fin contenido modal-->
    <?php
Modal::end();

?>
    <?php
Modal::begin([
    'id' => 'mainModal2',

    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false], //para que se cierre unicamente con la x
]);

?>
    <!--contenido del modal-->
    <div id="tagLoading" >

    <div style=" width:40%" class="center">
         <?=Html::img('@web/img/v2.2/gif-cargando.gif')?>
    </div>
    </div>
    <!--fin contenido modal-->
    <?php
Modal::end();

?>

<!--Start scroll-->
<div class="scrollToTop"><i class="icon-up-open-big"></i></div>
<!--End scroll-->
<?php $this->beginBody()?>
<div class="page-wrapper ">
    <?=
$this->render('header.php', ['baseUrl' => $baseUrl])

?>
    <!--//cuerpo del body-->
    <section style="padding-top: 10px;padding-bottom: 10px;"
             class="main-container object-non-visible animated object-visible container">


        <?=
Alert::widget([
    'options' => [
        'class' => 'alertMessage flashAlertFloat', 'duration' => 6000]])

?>
        <div id="flashAlert"
             style="display: none; "
             class="flashAlertFloat">
                        <div id="contentFlashAlert" > </div>
        </div>

        <?=
Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])

?>

        <?=
    $content;

?>

    </section>
    <!--fin del cuerpo del body-->


    <?=
$this->render(
    'footer.php'
)

?>
</div>
<?php $this->endBody()?>
</body>
</html>
<?php $this->endPage()?>

