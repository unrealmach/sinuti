<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;
$asset = AppAsset::register($this);
$baseUrl = $asset->baseUrl;
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body >
        <div class="fullscreen-bg"></div>
        <div class="page-wrapper">
            <?php $this->beginBody() ?>
            <?php
            $newContent = str_replace("%baseUrl%", $baseUrl, $content);
            echo $newContent;
            ?>
            <?php $this->endBody() ?>
        </div>
    </body>
</html>
<?php $this->endPage() ?>
