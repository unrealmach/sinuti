<?php
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\UserManagementModule;

?>

<!-- header-top start (Add "dark" class to .header-top in order to enable dark header-top e.g <div class="header-top dark">) -->
<!-- ================ -->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-2 col-sm-6">

                <!-- header-top-first start -->
                <!-- ================ -->
                <div class="header-top-first clearfix">
                    <ul class="social-links clearfix hidden-xs">
                    </ul>
                    <div class="social-links hidden-lg hidden-md hidden-sm">
                        <div class="btn-group dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
                            <ul class="dropdown-menu dropdown-animation">
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- header-top-first end -->

            </div>
            <div class="col-xs-10 col-sm-6">

                <!-- header-top-second start -->
                <!-- ================ -->
                <div id="header-top-second"  class="clearfix">

                    <!-- header top dropdowns start -->
                    <!-- ================ -->
                    <div class="header-top-dropdown">
                        <div class="btn-group dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i>
                                <?php
                                if (Yii::$app->user->isGuest) {
                                    echo "Log in";
                                } else {
                                    $authModel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
                                    if (!is_null($authModel)) {
                                        echo Yii::$app->user->identity->username . " ( " . strtoupper($authModel->item_name) . " )";
                                    } else {
                                        echo Yii::$app->user->identity->username . " ( SIN ASIGNAR )";
                                    }
                                }

                                ?>   
                            </button>
                            <?php if (Yii::$app->user->isGuest) { ?>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                    <li>
                                        <?=
                                        $this->render(
                                            'minicomponents/inlineLogin.php', ['baseUrl' => $baseUrl]
                                        )

                                        ?>
                                    </li>
                                </ul>
                            <?php } else { ?>
                                <ul class="dropdown-menu dropdown-menu-right dropdown-animation">
                                    <li>
                                        <?php
                                        echo GhostMenu::widget([
                                            'encodeLabels' => false,
                                            'activateParents' => true, //permite que los demas roles cambien su password de user
                                            'items' => [
                                                [
                                                    'label' => 'Acciones',
                                                    'items' => [
                                                        ['label' => 'Change own password', 'url' => ['/user-management/auth/change-own-password']],
                                                    ],
                                                ],
                                            ],
                                        ]);

                                        ?>
                                        <a href="/user-management/auth/logout" data-method="post"> Logout ( <?= Yii::$app->user->identity->username ?> ) </a>
                                    </li>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <!--  header top dropdowns end -->

                </div>
                <!-- header-top-second end -->

            </div>
        </div>
    </div>
</div>


<!-- header-top end -->

<!-- header start classes:
    fixed: fixed navigation mode (sticky menu) e.g. <header class="header fixed clearfix">
     dark: dark header version e.g. <header class="header dark clearfix">
================ -->
<header class="header fixed clearfix" style="padding: 5px;">
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <!-- header-left start -->
                <!-- ================ -->
                <div class="header-left clearfix">

                    <!-- logo  -->
                    <div class="logo" style="width: 140px;">

                        <a href="/"><img id="logo" src="<?=  Yii::getAlias('@web/img/v2.2/SINUTI-LOGO-PNG.png') ?>" alt="iDea"></a>
                    </div>

                    <!-- name-and-slogan -->
                    <div class="site-slogan">
                    </div>

                </div>
                <!-- header-left end -->

            </div>
            <div class="col-md-9">

                <!-- header-right start -->
                <!-- ================ -->
                <div class="header-right clearfix" >

                    <!-- main-navigation start -->
                    <!-- ================ -->
                    <div class="main-navigation animated">
                        <?php
                        yii\bootstrap\NavBar::begin(
                            ['innerContainerOptions' => ['class' => 'container-fluid'],]
                        );

                        ?>
                        <?php
                        echo yii\bootstrap\Nav::widget([
                            'options' => [
//                                  'class'=>'nav navbar-nav navbar-right',
                                'class' => 'nav navbar-nav navbar-left',
                            ],
                            'dropDownCaret' => '',
                            'items' => [
                                ['label' => 'Centro Infantil',
                                    'options' => ['class' => 'dropdown mega-menu'],
                                    'items' => [
                                        Yii::$app->user->isGuest ? 'No tiene acceso' :
                                            '<div class="row">
                                                <div class="col-lg-4 col-md-3 hidden-sm">
                                                    <h4>' . Yii::t('app', 'Centro Infantil') . '</h4>
                                                    <p>' . Yii::t('app', 'Realice las gestiones de cada centro') . '</p>
                                                        <img style="width:500px; height:100px !important" src="' . Yii::getAlias("@web/img/iconos_extra/cibv.svg") . '" alt="iDea">
                                                </div>
                                                <div class="col-lg-8 col-md-9">
                                                    <h4>' . Yii::t('app', 'Acciones') . '</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                   ' . ( Yii::$app->user->can('/centroinfantil/cibv/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('centroinfantil/cibv') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Centro Infantil') . '</a></li>' : '') . '
                                                                   ' . ( Yii::$app->user->can('/parametrossistema/periodo/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('parametrossistema/periodo') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Período') . '</a></li>' : '') . '
                                                                   ' . ( Yii::$app->user->can('/centroinfantil/cibv/report-estado-nutricional') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('centroinfantil/cibv/report-estado-nutricional') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Reportes') . '</a></li>' : '') . '
                                                                   
                                                                 </ul>
                                                          </div>
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/inscripcion/matricula/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('inscripcion/matricula') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Matriculas - Infantes') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/consumoalimenticio/asignacioninfantecsemanal/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('consumoalimenticio/asignacioninfantecsemanal') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Planificación Semanal - Infantes') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/consumoalimenticio/bitacoraconsumoinfantil/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('consumoalimenticio/bitacoraconsumoinfantil') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Bitácora de consumo') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                       </div>
                                                  </div>
                                         </div>',
                                    ],
                                ],
                                ['label' => 'Sectores',
                                    'options' => ['class' => 'dropdown mega-menu'],
                                    'items' => [
                                        Yii::$app->user->isGuest ? 'No tiene acceso' :
                                            '<div class="row">
                                                <div class="col-lg-4 col-md-3 hidden-sm">
                                                    <h4>' . Yii::t('app', 'Sectores') . '</h4>
                                                    <p>' . Yii::t('app', 'Realice las matriculas de cada infante y la distribución de salones') . '</p>
                                                              <img style="width:500px; height:100px !important" src="' . Yii::getAlias("@web/img/iconos_extra/asignacion.svg") . '" alt="iDea">
                                                </div>
                                                <div class="col-lg-8 col-md-9">
                                                    <h4>' . Yii::t('app', 'Acciones') . '</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/inscripcion/salon/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('inscripcion/salon') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Salones') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/recursoshumanos/sectorasignadoeducador/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('recursoshumanos/sectorasignadoeducador') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Sector asignado educador') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/centroinfantil/sectorasignadoinfante/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('centroinfantil/sectorasignadoinfante') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Sector asignado infante') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/tecnicoarea/asignaciongastronomodistrito/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('tecnicoarea/asignaciongastronomodistrito') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Distrito asignado gastronomo') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/tecnicoarea/asignacionnutricionistadistrito/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('tecnicoarea/asignacionnutricionistadistrito') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Distrito asignado nutricionista') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                       </div>
                                                  </div>
                                         </div>',
                                    ],
                                ],
                                ['label' => 'RR HH',
                                    'options' => ['class' => 'dropdown mega-menu'],
                                    'items' => [
                                        Yii::$app->user->isGuest ? 'No tiene acceso' :
                                            '<div class="row">
                                                <div class="col-lg-4 col-md-3 hidden-sm">
                                                    <h4>' . Yii::t('app', 'Recursos Humanos') . '</h4>
                                                    <p>' . Yii::t('app', 'Realice las gestiones del personal') . '</p>
                                                         <img style="width:500px; height:100px !important" src="' . Yii::getAlias("@web/img/iconos_extra/recursos.humanos.svg") . '" alt="iDea">
                                                </div>
                                                <div class="col-lg-8 col-md-9">
                                                    <h4>' . Yii::t('app', 'Acciones') . '</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/recursoshumanos/coordinadorcibv/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('recursoshumanos/coordinadorcibv/index') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Coordinadores') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/recursoshumanos/educador/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('recursoshumanos/educador/index') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Educadores') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/recursoshumanos/gastronomo/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('recursoshumanos/gastronomo') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Gastrónomo') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/recursoshumanos/nutricionista/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('recursoshumanos/nutricionista') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Nutricionista') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                       </div>
                                                  </div>
                                         </div>',
                                    ],
                                ],
                                ['label' => 'Individuo',
                                    'options' => ['class' => 'dropdown mega-menu'],
                                    'items' => [
                                        Yii::$app->user->isGuest ? 'No tiene acceso' :
                                            '<div class="row">
                                                <div class="col-lg-4 col-md-3 hidden-sm">
                                                    <h4>' . Yii::t('app', 'Individuos') . '</h4>
                                                    <p>' . Yii::t('app', 'Realice las gestiones de los infantes') . '</p>
                                                        <img style="width:500px; height:100px !important" src="' . Yii::getAlias("@web/img/iconos_extra/antropometria.svg") . '" alt="iDea">
                                                </div>
                                                <div class="col-lg-8 col-md-9">
                                                    <h4>' . Yii::t('app', 'Acciones') . '</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/individuo/infante/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('individuo/infante') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Infantes') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/metricasindividuo/datoantropometrico/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('metricasindividuo/datoantropometrico') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Datos Antropométricas') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/individuo/registrovacunainfante/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('individuo/registrovacunainfante') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Registro de Vacunas') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                       </div>
                                                  </div>
                                         </div>',
                                    ],
                                ],
                                ['label' => 'Nutrición',
                                    'options' => ['class' => 'dropdown mega-menu'],
                                    'items' => [
                                        Yii::$app->user->isGuest ? 'No tiene acceso' :
                                            '<div class="row">
                                                <div class="col-lg-4 col-md-3 hidden-sm">
                                                    <h4>' . Yii::t('app', 'Nutrición') . '</h4>
                                                    <p>' . Yii::t('app', 'Realice la gestion de menús semanales') . '</p>
                                                        <img style="width:500px; height:100px !important" src="' . Yii::getAlias("@web/img/iconos_extra/menu.semana.svg") . '" alt="iDea">
                                                </div>
                                                <div class="col-lg-8 col-md-9">
                                                    <h4>' . Yii::t('app', 'Acciones') . '</h4>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                  ' . ( Yii::$app->user->can('/nutricion/alimento/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('nutricion/alimento') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Alimentos') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/preparacion_platillo/mplatillo/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('preparacion_platillo/mplatillo') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Receta o preparación') . '</a></li>' : '') . '
                                                                 ' . ( Yii::$app->user->can('/preparacioncarta/mprepcarta/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('preparacioncarta/mprepcarta') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Menús') . '</a></li>' : '') . '
                                                                 
                                                                 </ul>
                                                          </div>
                                                        <div class="col-sm-4">
                                                           <div class="divider"></div>
                                                                 <ul class="menu">
                                                                 ' . ( Yii::$app->user->can('/cartasemanal/mcartasemanal/index') ? ' <li><a href="' . Yii::$app->urlManager->createUrl('cartasemanal/mcartasemanal') . '"><i class="icon-right-open"></i>' . Yii::t('app', 'Planificaciones semanales') . '</a></li>' : '') . '
                                                                 </ul>
                                                          </div>
                                                       </div>
                                                  </div>
                                         </div>',
                                    ],
                                ],
                            ],
                        ])

                        ?>
                        <!--fin nav-->
                        <?php yii\bootstrap\NavBar::end();

                        ?>

                        <!--                        navbar end -->

                    </div>
                    <!-- main-navigation end -->

                </div>
                <!-- header-right end -->

            </div>
        </div>
    </div>
</header>
<!-- header end -->

