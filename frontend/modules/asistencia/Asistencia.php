<?php

namespace frontend\modules\asistencia;

class Asistencia extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\asistencia\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
