<?php

namespace frontend\modules\asistencia\controllers;

use Yii;
use backend\modules\asistencia\models\Asistencia;
use backend\modules\asistencia\models\AsistenciaSearch;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\individuo\models\Infante;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AsistenciaController implements the CRUD actions for Asistencia model.
 */
class AsistenciaController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Asistencia models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AsistenciaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single Asistencia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new Asistencia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Asistencia();
        $listInfantes = [];
        $userCordinador = AsignacionUserCoordinadorCibv::find()->deUserId(\Yii::$app->user->id)->one();
        if ($userCordinador) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $userCordinador->autoridades_cibv_id]);
            $periodo = Periodo::find()->periodoActivo()->one();
            $matricula = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, $periodo->periodo_id)->all();
            if (!empty($matricula)) {
                foreach ($matricula as $value) {
                    array_push($listInfantes, Infante::findOne(['infante_id' => $value->infante_id]));
                }
            } else {
                $listInfantes = Infante::find()->all();
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $validateAsistencia = Asistencia::find()->deAsistenciaDiaria($model->infantes_id, $model->asistencia_fecha)->one();
            if (!empty($validateAsistencia)) {
                throw new NotFoundHttpException('El infante ya tiene la asistencia del dia');
            }
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->asistencia_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'listInfantes' => $listInfantes
            ]);
        }
    }

    /**
     * Updates an existing Asistencia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->asistencia_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Asistencia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Asistencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Asistencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Asistencia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
