<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */

$this->title = Yii::t('app', 'Crear Asistencia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
        'listInfantes' => $listInfantes
    ])
    ?>

</div>
