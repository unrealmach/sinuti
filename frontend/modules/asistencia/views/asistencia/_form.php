<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\modules\individuo\models\Infante;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="asistencia-form section default-bg">




    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= Html::encode(Yii::t('app', 'Crear Asistencia')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

            <?=
                $form->field($model, 'asistencia_hora_ingreso', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>']
                )->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                ->label(null, ['class' => 'col-sm-3 control-label'])

            ?>

            <?=
                $form->field($model, 'asistencia_hora_salida', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                ->label(null, ['class' => 'col-sm-3 control-label'])

            ?>

            <?=
                $form->field($model, 'asistencia_fecha', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label(null, ['class' => 'col-sm-3 control-label'])

            ?>

            <?=
            $form->field($model, 'infantes_id', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                'data' => ArrayHelper::map($listInfantes, 'infante_id', 'nombreCompleto', 'infante_dni'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(null, ['class' => 'col-sm-3 control-label']);

            ?>

        </div>
    </div>
    <div class="form-group">
        <center>       
<?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

<?php ActiveForm::end(); ?>

</div>
