<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\asistencia\models\AsistenciaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asistencias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    if ($rol == 'coordinador'):
        ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'infantes_id',
                    'value' => 'infantes.nombreCompleto'
                ],
                'asistencia_fecha',
                'asistencia_hora_ingreso',
                'asistencia_hora_salida',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'educador'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'infantes_id',
                    'value' => 'infantes.nombreCompleto'
                ],
                'asistencia_fecha',
                'asistencia_hora_ingreso',
                'asistencia_hora_salida',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'Admin'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                'asistencia_id',
                ['attribute' => 'infantes_id',
                    'value' => 'infantes.nombreCompleto'
                ],
                'asistencia_fecha',
                'asistencia_hora_ingreso',
                'asistencia_hora_salida',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    <?php endif; ?>

</div>
