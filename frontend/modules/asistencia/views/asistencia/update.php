<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asistencia',
]) . ' ' . $model->asistencia_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asistencia_id, 'url' => ['view', 'id' => $model->asistencia_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asistencia-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
