<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */

$this->title = $model->infantes->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>
    <?php if ($rol == 'coordinador'): ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->asistencia_id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//                        'asistencia_id',
                    'asistencia_fecha',
                    'asistencia_hora_ingreso',
                    'asistencia_hora_salida',
//            'infantes_id',
                ],
            ])
            ?>
        </div>
    </div>
</div>
