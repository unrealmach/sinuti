$(function() {
    $("#mcartasemanal-m_c_sem_fecha_inicio").on('change', function() {
        if ($(this).val() != "") {
            getNumSemana($(this).val());
        }
        ;
    });


});

/**
 * 
 * @param int grupo_platillo_id
 * @param string url
 * @author Sofia Mejia
 * @returns JSON
 */
function llamarlista(grupo_platillo_id, url) {
    grupo_platillo_id = parseInt(grupo_platillo_id);
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {id: grupo_platillo_id
        },
        success: function(data) {
            $('#mplatillo-tipo_receta_id').html('');

            $('#mplatillo-tipo_receta_id').select2({data: data});

        }
    });
}

/**
 * Agrega la fecha fin y el numero de semana a la carta semanal
 * @param string fecha
 * @author Sofia Mejia
 * @returns JSON
 */
function getNumSemana(fecha) {
    var fecha_ini = fecha;
    $.ajax({
        beforeSend: function() {
        },
        url: urlAjaxNumSemana,
        method: 'GET',
        dataType: 'json',
        data: {
            fecha: fecha_ini
        },
        success: function(data) {

//                showModalWithData(data);
            $.each(data, function(key, value) {
                $("#mcartasemanal-m_c_sem_num_semana").val(key);
                $("#mcartasemanal-m_c_sem_fecha_fin").val(value);
            });


        },
        complete: function() {

        }
    });
}