$(function() {
    $("body").on('click', '#btn-calcula-grafica', function() {
        console.log($("#datagrafica").data("serie"));
        $('#datagrafica').highcharts({
            chart: {
                type: 'column',
                zoomType: 'xy',
            },
            title: {
                text: 'Composición nutricional del menu'
            },
//                    subtitle: {
//                        text: 'Source: WorldClimate.com'
//                    },
            xAxis: {
                categories: $("#datagrafica").data("categories"),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: '<b>Cantidad </b>'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<b> Cantidad {point.y:.1f} </b>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'Cantidad actual',
                    data: $("#datagrafica").data("serie")

                }, ]
        });


    });

//valida que los datos esten completos en el paso 1 del wizard
    $("input, select, textarea").on('blur', function() {
        $("#stepwizard_step1_next").css('visibility', 'visible');
    });
    var contadorError = 0;
    $("#stepwizard_step1_next").on('mouseover', function() {
        contadorError = 0;
        $("input, select, textarea").blur();
        $(".help-block").each(function() {
            if ($(this).html().length > 0) {
                contadorError++;
                $("#stepwizard_step1_next").css('visibility', 'hidden');
            }
        })
        if (contadorError > 0) {
            alert("Revise el formulario, datos incompletos o no validos");
        }
    });

});