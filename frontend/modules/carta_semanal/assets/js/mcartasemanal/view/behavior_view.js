$(function() {
    $("#btnCalcular").on("click", function() {
        var numComensales = $("#numeroComensales").val()
        $('#table-ingredientes > tbody  > tr > td[data-cantidad_inicial]').each(function() {
            var nuevoValor = ($(this).data("cantidad_inicial") * numComensales)
            $(this).html("" + nuevoValor);
        });
    });
});