$(function() {

    $("body").on('click','#btnEliminarMenu', function() {
        $.ajax({
            beforeSend: function() {
                showModalLoading();
            },
            url: urlAjaxDeleteCarta,
            method: 'GET',
            success: function(data) {
                showModalWithData(data);
            },
            complete: function() {
            }
        });
    });

    $("body").on('click','#deleteMenu', function() {
        $.ajax({
            beforeSend: function() {
//                showModalLoading();
            },
            url: urlAjaxDeleteCarta,
            method: 'GET',
            data: {dia: $("#select_day").val(),
                fecha: $("#select_tiempo_comida").val(),
            },
            success: function(data) {
                alert("Proceso realizado");
//                showModalWithData(data);
            },
            complete: function() {
            }
        });
    });



});