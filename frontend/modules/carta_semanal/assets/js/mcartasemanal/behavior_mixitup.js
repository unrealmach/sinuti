$(function () {
    $("#stepwizard_step1_next").on('click', function () {
        $('#Container-mix-menu').mixItUp(
            {
                animation: {
                    animateChangeLayout: false,
                    animateResizeContainer: false,
                }
            }
        );
    })

    $("#btnBuscarCarta").on('click', function () {
        loadData();
    });

    $("body").on('click', '#btnVerAllMenus', function () {
        verCartaSemanal();
    });
//  $('#Container-mix-menu').mixItUp();


});

/**
 * Obtiene los resultados de la busqueda deacuerdo al valor ingresado
 * @author Sofia Mejia
 * @returns {undefined}
 */
function loadData() {
    $.ajax({
        beforeSend: function () {
            showModalLoading();
            $('#Container-mix-menu').children('div').remove();
        },
        url: urlAjaxGetCartaSearch,
        method: 'GET',
        data: {
            palabraClave: "%" + $("#buscadorCartas").val() + "%",
        },
        success: function (data) {
            $html = convertHTML(data);
            $('#Container-mix-menu').mixItUp('append', $($html));
        },
        complete: function () {
            closeModal();
        }
    });

}

/**
 * Convierte el arreglo de json a un string que conforman los items
 * del mixitup en formato HTML
 * @param array data
 * @author Sofia Mejia
 * @returns {$items|String}
 */
function convertHTML(data) {
    $items = "";

    $.each(data, function (key, value) {
        itemHtml = generateItemMixItUpCarta(value);
        $items += " " + itemHtml;
    })
    return $items;
}

/**
 * Genera el HTML necesario para un item del mixitup
 * @param model carta
 * @returns {String}
 */
function generateItemMixItUpCarta(carta) {
//    console.log(carta);
    var item = " <div class='mix category-" + carta['tiempo_comida_id'] + "' data-myorder='" + carta['m_prep_carta_id'] + "'>"



        + " <div style='margin-top: 0px; padding: 0px'    margin-bottom: 0px;' class='box-style-1 white-bg object-non-visible animated object-visible fadeInUpSmall'     data-animation-effect='fadeInUpSmall' data-effect-delay='0'>"

                    + "     <h2 style='margin:0px'>"+'"' + carta['m_prep_carta_nombre'] +'"'+ "</h2>"


                        + "<select title='Seleccione el día' id='dias'>"
                        + "<option value='--'> - DÍA - </option>"
                        + "<option value='LUNES'>Lunes</option>"
                        + "<option value='MARTES'>Martes</option>"
                        + "<option value='MIERCOLES'>Miércoles</option>"
                        + "<option value='JUEVES'>Jueves</option>"
                        + "<option value='VIERNES'>Viernes</option>"
                        + "</select>"
        + "<div class='row'>"
             + "<div class='col-sm-12'>"
                 + " <div class='col-sm-6'>"
                    + " <a style='font-size: 1em;     height: 30px' title='Ver detalle' onclick='loadDataIngredientesCarta(this)' class='btn radius btn-info btn-sm carta-ver-ingredientes' type='button' data-carta_id='" + carta['m_prep_carta_id'] + "' data-carta_tiempo_comida ='" + carta['tiempo_comida_id'] + "'>  <i style='font-size: 15px; margin: 0px;' class='glyphicon glyphicon-info-sign' ></i> </a>"
                + "</div>"
                  + "<div class='col-sm-6'>"
                        + "<a style='font-size: 1em;     height: 30px'  title='Agregar a la planificación' onclick='agregarDetalle(this, urlAjaxDPrepcarta)' class='btn radius btn-info btn-sm carta-agregar-menu' type='button' data-carta_id='" + carta['m_prep_carta_id'] + "' data-carta_tiempo_comida ='" + carta['tiempo_comida_id'] + "' >  <i style='font-size: 15px;margin: 0px;' class='glyphicon  glyphicon-thumbs-up' title='Agregar al menu' ></i> </a>"
                 + " </div>"
             + " </div>"
        + " </div>"

        + " </div>"
        + " </div>";
    return item;

}

/**
 * Cargar el carta selccionado
 * @param {type} item
 * @author Sofia Mejia
 * @returns {undefined}
 */
function loadDataIngredientesCarta(item) {
    var carta_id = $(item).data("carta_id");

    $.ajax({
        beforeSend: function () {
            showModalLoading();
//            $('#Container-mix-menu').children('div').remove();
        },
        url: urlAjaxViewAllIngredientes,
        method: 'GET',
        data: {
            carta_id: carta_id,
        },
        success: function (data) {
           
            showModalWithData(data);
//            console.log(data);
//            $html = convertHTML(data);
//            $('#Container-mix-menu').mixItUp('append', $($html));
        },
        complete: function () {
//            closeModal();
        }
    });
}

/**
 * agrega el mprepcarta de acuerdo al tiempo de comida
 * @param {type} item
 * @param {type} url
 * @author Sofia Mejia
 * @returns {undefined}
 */
function agregarDetalle(item, url) {
    if ($(item).siblings("select").val() !== "--") {
        var dia = $(item).parent().parent().parent().siblings("select").val();
        console.log($(item));
        var mCarta = $(item).data("carta_id");
        var tiempoComida = $(item).data("carta_tiempo_comida");
        console.log(dia + " " + mCarta + " " + tiempoComida);
        $.ajax({
            beforeSend: function () {
                showModalLoading();
            },
            url: url,
            method: 'GET',
            dataType: 'json',
            data: {
                cartaid: JSON.stringify(mCarta),
                dia: JSON.stringify(dia),
                tiempoComida: JSON.stringify(tiempoComida)
            },
            success: function (data) {
                showModalWithData(data);

            },
            complete: function () {

            }
        });
    } else {
        alert("Debe de seleccionar un día");
    }
}

/**
 * Permite visualizar la carta semanal
 * @author Sofia Mejia
 * @returns {undefined}
 */
function verCartaSemanal() {
    var dia = 0;
    var mCarta = 0;
    var tiempoComida = 0;
    $.ajax({
        beforeSend: function () {
            showModalLoading2();
        },
        url: urlAjaxDPrepcarta,
        method: 'GET',
        dataType: 'json',
        data: {
            cartaid: JSON.stringify(mCarta),
            dia: JSON.stringify(dia),
            tiempoComida: JSON.stringify(tiempoComida)
        },
        success: function (data) {
            showModalWithData2(data);

        },
        complete: function () {

        }
    });
}
