<?php

namespace frontend\modules\carta_semanal;

class Cartasemanal extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\carta_semanal\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
