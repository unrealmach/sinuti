<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use yii\bootstrap\Tabs;
use \common\models\User;

use \backend\modules\recursos_humanos\models\Nutricionista;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\MCartaSemanal */

$dateIni = new DateTime($model->m_c_sem_fecha_inicio);
$dateFin = new DateTime($model->m_c_sem_fecha_fin);
$this->title = 'Plan de alimentación semanal para el centro infantil "' . $model->cenInf->cen_inf_nombre . '" del ' . $dateIni->format('Y-m-d') . " - " . $dateFin->format('Y-m-d');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Carta Semanal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="plan-view" style="margin: 20px 0;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode($this->title) ?></h1>

        </div>
        <div class="panel-body">

            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>
                    <li><a href="#vtab2" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Detalles del plan</a></li>


                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">

                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                ['attribute' => 'm_c_sem_user_responsable',
                                    'value' => function ($model) {

                                        $nameUser = AsignacionUserCoordinadorCibv::findOne(['user_id' => $model->m_c_sem_user_responsable]);
                                        if (empty($nameUser)) {
                                            $nameUser = User::findOne(['id' => $model->m_c_sem_user_responsable]);
                                            $nameUser = empty($nameUser) ? NULL : Nutricionista::findOne(['nutricionista_correo' => $nameUser->email])->nombreCompleto;
                                        } else {
                                            $nameUser = $nameUser->autoridadCibv->coordinador->nombreCompleto;
                                        }

                                        return $nameUser;
                                    }],


                                'm_c_sem_capacidad_infantes',
                            ],
                        ])
                        ?>
                    </div>
                    <div class="tab-pane fade" id="vtab2">
                        <?=
                        Tabs::widget([
                            'items' => [
                                [
                                    'label' => 'Alimentos y/o preparaciones',
                                    'content' => $this->render("listViews/_alimento_platillo", ['model' => $model]),
                                    'active' => false
                                ],
                                [
                                    'label' => 'Composición Nutricional',
                                    'items' => [
                                        [
                                            'label' => 'Lunes',
                                            'content' => $this->render("listViews/_comp_lunes", ['composicionTotal' => $composLunes]),
                                        ],
                                        [
                                            'label' => 'Martes',
                                            'content' => $this->render("listViews/_comp_martes", ['composicionTotal' => $composMartes]),
                                        ],
                                        [
                                            'label' => 'Miércoles',
                                            'content' => $this->render("listViews/_comp_miercoles", ['composicionTotal' => $composMiercoles]),
                                        ],
                                        [
                                            'label' => 'Jueves',
                                            'content' => $this->render("listViews/_comp_jueves", ['composicionTotal' => $composJueves]),
                                        ],
                                        [
                                            'label' => 'Viernes',
                                            'content' => $this->render("listViews/_comp_viernes", ['composicionTotal' => $composViernes]),
                                        ],
                                    ],
                                ],
                            ],
                        ]);
                        ?>
                        <div id="resumenPrepPlatillo">
                            <?php // echo $this->render("listViews/_alimento_platillo", ['model' => $model]) ?>
                        </div>


                    </div>

                    <p class="container">
                        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                            'onclick' => ' window.history.back();',
                            'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->m_c_sem_id], ['class' => 'btn btn-primary']) ?>
                        <?=
                        Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->m_c_sem_id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Generar Pdf'), ['/cartasemanal/mcartasemanal/make-pdf-carta-semanal', 'carta_semanal_id' => $model->m_c_sem_id], ['class' => 'btn btn-success']) ?>
                    </p>


                </div>


            </div>


        </div>
    </div>

</div>

