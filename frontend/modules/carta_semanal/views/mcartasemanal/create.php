<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\MCartaSemanal */

$this->title = Yii::t('app', 'Crear Plan de Alimentación Semanal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Planes de Alimentación Semanal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mcarta-semanal-create">

    </br>

    <?=
    $this->render('wizard/_form', [
        'model' => $model,
        'd_model' => $d_model,
        'listCibv' => $listCibv,
    ])
    ?>

</div>
