<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use backend\modules\carta_semanal\models\DCartaSemanal;
use backend\modules\carta_semanal\models\recursos\CartaSemanalSession;
use yii\widgets\DetailView;
?>

<div>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
//                    'm_c_sem_user_responsable',
            'm_c_sem_capacidad_infantes',
//            'cenInf.cen_inf_nombre',
            [
                'label' => 'Centro Infantil',
                'value' => $model->cenInf->cen_inf_nombre
            ],
            'm_c_sem_fecha_inicio',
            'm_c_sem_fecha_fin'
        ],
    ])
    ?>
</div>
<?php
$dCartaSemanales = DCartaSemanal::find()->where(['m_carta_semanal_id' => $model->m_c_sem_id])->all();
$manejadorCartaSemanal = new CartaSemanalSession();
if (!empty($dCartaSemanales)) {
    foreach ($dCartaSemanales as $dCartaSemanal) {
        $manejadorCartaSemanal->setCartIdByDayAndTime($dCartaSemanal->d_c_sem_dia_semana, $dCartaSemanal->mPrepCarta->tiempoComida->tiem_com_nombre, $dCartaSemanal->mPrepCarta->m_prep_carta_id);
    }
    echo $manejadorCartaSemanal->createHTMLFromSessionObject();
} else {
    Yii::$app->getSession()->setFlash('error', 'No existen detalles del menu seleccionado ');
}
?>