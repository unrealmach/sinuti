<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\McartasemanalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mcarta-semanal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'm_c_sem_id') ?>

    <?= $form->field($model, 'cen_inf_id') ?>

    <?= $form->field($model, 'm_c_sem_fecha_inicio') ?>

    <?= $form->field($model, 'm_c_sem_fecha_fin') ?>

    <?= $form->field($model, 'm_c_sem_user_responsable') ?>

    <?php // echo $form->field($model, 'm_c_sem_num_semana') ?>

    <?php // echo $form->field($model, 'm_c_sem_capacidad_infantes') ?>

    <?php // echo $form->field($model, 'm_c_sem_observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
