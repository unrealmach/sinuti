<div>
    <?=
    \common\components\fixWidget\HighCharts::widget([
        'clientOptions' => [
            'chart' => [
                'type'=>'column',
                'zoomType' => 'xy',
//                    'renderTo' => "contentSlider"
            ],
            'title' => [
                'text' => 'Composición nutricional menú'
            ],
            'subtitle' => [
                'text' => 'Datos obtenidos de la tabla de alimentos del Ecuador'
            ],
            'xAxis' => [[
            'categories' => array_keys($composicionTotal),
            'crosshair' => true
                ]],
            'yAxis' => [[// Primary yAxis
                'labels' => [
                    'overflow' => 'justify',
                    'style' => [
                        'color' => 'Highcharts.getOptions().colors[2]'
                    ]
            ],
            'title' => [
                'text' => 'Cantidad',

            ],
                ],
            ],
            'tooltip' => [
                'headerFormat'=> '<span style="font-size:10px">{point.key}</span><table>',
                'footerFormat'=> '</table>',
                'shared'=> true,
            ],
            'legend' => [
                'layout' => 'vertical',
                'align' => 'left',
                'x' => 80,
                'verticalAlign' => 'top',
                'y' => 55,
                'floating' => true,
                'enabled' => false,
                'backgroundColor' => "#ffffff !important"
            ],
            'series' => [
                [
                    'name' => 'cantidad',
                    'data' => array_values($composicionTotal),
                    //'type' => 'column'
                ],
            ]
    ]]);
    ?>
</div>
