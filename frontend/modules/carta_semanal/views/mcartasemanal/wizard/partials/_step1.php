<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use backend\modules\preparacion_carta\models\TiempoComida;
use backend\modules\metricas_individuo\models\GrupoEdad;
use yii\helpers\ArrayHelper;
use backend\modules\centro_infantil\models\Cibv;
use kartik\date\DatePicker;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/step1_behavior.js', ['depends' => [frontend\assets\AppAsset::className()]]);
//setTiempoTipoPreparacion
$urlAjaxNumSemana = \Yii::$app->urlManager->createUrl('admin/carta_semanal/mcartasemanal/ajax-num-sem-select');
?>
    <div class="panel panel-default">
        <div class="panel-heading ">
            <div style="text-align:center; width:100%">
                <h2 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Información Básica')) : Html::encode(Yii::t('app', 'Actualizar Información Básica')) ?>
                </h2>
            </div>
        </div>

        <!--inicio maestro-->
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($listCibv, 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
//      TODO validar que solo pueda ver el CIBV asignado al coordinador  'data' => ArrayHelper::map([0 => Cibv::findOne($model->cen_inf_id)], 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Cibv', 'class' => 'col-sm-12',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'm_c_sem_capacidad_infantes', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['type' => 'number', 'min' => 1], array('placeholder' => 'Ingrese ...'))
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nombre_user', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array('readonly' => TRUE))
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($model, 'm_c_sem_fecha_inicio', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(DatePicker::className(), [
//            'name' => 'm_c_sem_fecha_inicio',
                            'options' => ['placeholder' => 'Seleccione un día de la semana'],
                            'readonly' => true,
                            'pluginOptions' => [
                                'daysOfWeekDisabled' => '0,2,3,4,5,6',
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'calendarWeeks' => true,
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'm_c_sem_fecha_fin', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array('readonly' => TRUE))
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'm_c_sem_num_semana', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...', 'readonly' => true))
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?=
                    $form->field($model, 'm_c_sem_observaciones', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6,'style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
            </div>

            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-right']) . Yii::t('app', 'Next'), [

                        'class' => 'btn btn-default next-step',
                        'id' => 'stepwizard_step1_next']) ?>

                </div>


            </div>


        </div>
        <!--fin maestro-->
    </div>
<?php
$script = <<< JS
    $(function(){
     urlAjaxNumSemana = '{$urlAjaxNumSemana}';
        })
  
JS;
$this->registerJs($script);
