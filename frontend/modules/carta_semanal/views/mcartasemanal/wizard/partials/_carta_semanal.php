<table id="carta_semanal" >
    <tr>
        <th id="Des">DESAYUNO</th>
        <th id="DESAYUNO-LUNES"></th>
        <th id="DESAYUNO-MARTES"></th>
        <th id="DESAYUNO-MIERCOLES"></th>
        <th id="DESAYUNO-JUEVES"></th>
        <th id="DESAYUNO-VIERNES"></th>
    </tr>
    <tr variante="DESAYUNO-liquido">
        <th>líquido</th>
        <td id="DESAYUNO-LUNES-liquido"></td>
        <td id="DESAYUNO-MARTES-liquido"></td>
        <td id="DESAYUNO-MIERCOLES-liquido"></td>
        <td id="DESAYUNO-JUEVES-liquido"></td>
        <td id="DESAYUNO-VIERNES-liquido"></td>
    </tr>
    <tr variante="DESAYUNO-solido">
        <th>sólido</th>
        <td id="DESAYUNO-LUNES-solido"></td>
        <td id="DESAYUNO-MARTES-solido"></td>
        <td id="DESAYUNO-MIERCOLES-solido"></td>
        <td id="DESAYUNO-JUEVES-solido"></td>
        <td id="DESAYUNO-VIERNES-solido"></td>
    </tr>
    <tr variante="DESAYUNO-otro">
        <th>otro</th>
        <td id="DESAYUNO-LUNES-otro"></td>
        <td id="DESAYUNO-MARTES-otro"></td>
        <td id="DESAYUNO-MIERCOLES-otro"></td>
        <td id="DESAYUNO-JUEVES-otro"></td>
        <td id="DESAYUNO-VIERNES-otro"></td>
    </tr>
    <tr>
        <th>REFRIGERIO DE LA MAÑANA</th>
        <th id="REFRIGERIO_DE_LA_MAÑANA-LUNES"></th>
        <th id="REFRIGERIO_DE_LA_MAÑANA-MARTES"></th>
        <th id="REFRIGERIO_DE_LA_MAÑANA-MIERCOLES"></th>
        <th id="REFRIGERIO_DE_LA_MAÑANA-JUEVES"></th>
        <th id="REFRIGERIO_DE_LA_MAÑANA-VIERNES"></th>
    </tr>
    <tr 
        variante="REFRIGERIO_DE_LA_MAÑANA-fruta">
        <th>fruta</th>
        <td id="REFRIGERIO_DE_LA_MAÑANA-LUNES-fruta"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-MARTES-fruta"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-MIERCOLES-fruta"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-JUEVES-fruta"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-VIERNES-fruta"></td>
    </tr>
    <tr 
        variante="REFRIGERIO_DE_LA_MAÑANA-liquido">
        <th>liquido</th>
        <td id="REFRIGERIO_DE_LA_MAÑANA-LUNES-liquido"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-MARTES-liquido"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-MIERCOLES-liquido"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-JUEVES-liquido"></td>
        <td id="REFRIGERIO_DE_LA_MAÑANA-VIERNES-liquido"></td>
    </tr>
    <tr>
        <th>ALMUERZO</th>
        <th id="ALMUERZO-LUNES"></th>
        <th id="ALMUERZO-MARTES"></th>
        <th id="ALMUERZO-MIERCOLES"></th>
        <th id="ALMUERZO-JUEVES"></th>
        <th id="ALMUERZO-VIERNES"></th>
    </tr>
    <tr variante="ALMUERZO-sopa">
        <th>sopa</th>
        <td id="ALMUERZO-LUNES-sopa"></td>
        <td id="ALMUERZO-MARTES-sopa"></td>
        <td id="ALMUERZO-MIERCOLES-sopa"></td>
        <td id="ALMUERZO-JUEVES-sopa"></td>
        <td id="ALMUERZO-VIERNES-sopa"></td>
    </tr>
    <tr variante="ALMUERZO-plato-fuerte">
        <th>plato fuerte</th>
        <td id="ALMUERZO-LUNES-plato-fuerte"></td>
        <td id="ALMUERZO-MARTES-plato-fuerte"></td>
        <td id="ALMUERZO-MIERCOLES-plato-fuerte"></td>
        <td id="ALMUERZO-JUEVES-plato-fuerte"></td>
        <td id="ALMUERZO-VIERNES-plato-fuerte"></td>
    </tr>
    <tr variante="ALMUERZO-acompañado">
        <th>acompañado</th>
        <td id="ALMUERZO-LUNES-acompañado"></td>
        <td id="ALMUERZO-MARTES-acompañado"></td>
        <td id="ALMUERZO-MIERCOLES-acompañado"></td>
        <td id="ALMUERZO-JUEVES-acompañado"></td>
        <td id="ALMUERZO-VIERNES-acompañado"></td>
    </tr>
    <tr variante="ALMUERZO-ensalada">
        <th>ensalada</th><td id="ALMUERZO-LUNES-ensalada"></td>
        <td id="ALMUERZO-MARTES-ensalada"></td>
        <td id="ALMUERZO-MIERCOLES-ensalada"></td>
        <td id="ALMUERZO-JUEVES-ensalada"></td>
        <td id="ALMUERZO-VIERNES-ensalada"></td>
    </tr>
    <tr variante="ALMUERZO-jugo">
        <th>jugo</th>
        <td id="ALMUERZO-LUNES-jugo"></td>
        <td id="ALMUERZO-MARTES-jugo"></td>
        <td id="ALMUERZO-MIERCOLES-jugo"></td>
        <td id="ALMUERZO-JUEVES-jugo"></td>
        <td id="ALMUERZO-VIERNES-jugo"></td>
    </tr>
    <tr>
        <th>REFRIGERIO DE LA TARDE</th>
        <th id="REFRIGERIO _DE _LA _TARDE-LUNES"></th>
        <th id="REFRIGERIO _DE _LA _TARDE-MARTES"></th>
        <th id="REFRIGERIO _DE _LA _TARDE-MIERCOLES"></th>
        <th id="REFRIGERIO _DE _LA _TARDE-JUEVES"></th>
        <th id="REFRIGERIO _DE _LA _TARDE-VIERNES"></th>
    </tr>
    <tr variante="REFRIGERIO _DE _LA _TARDE-liquido">
        <th>líquido</th>
        <td id="REFRIGERIO _DE _LA _TARDE-LUNES-liquido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-MARTES-liquido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-MIERCOLES-liquido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-JUEVES-liquido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-VIERNES-liquido"></td>
    </tr>
    <tr variante="REFRIGERIO _DE _LA _TARDE-solido">
        <th>sólido</th>
        <td id="REFRIGERIO _DE _LA _TARDE-LUNES-solido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-MARTES-solido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-MIERCOLES-solido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-JUEVES-solido"></td>
        <td id="REFRIGERIO _DE _LA _TARDE-VIERNES-solido"></td>
    </tr>
</table>

