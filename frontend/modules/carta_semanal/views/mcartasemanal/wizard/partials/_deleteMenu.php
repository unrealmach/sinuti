
<?php
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>


<h4>Seleccione una posición para proceder a vacíar en la preparación semanal de alimentación</h4>

<button id="btnVerAllMenus" type="button" class="btn" > <?= Html::encode(Yii::t('app', 'Ver Planificación Semanal')) ?></button>

<br>

<?php
$data_days = ['LUNES' => 'Lunes', 'MARTES' => 'Martes', 'MIERCOLES' => 'Miércoles', 'JUEVES' => 'Jueves', 'VIERNES' => 'Viernes'];
echo "<label>Seleccione el día: </label>";
echo Select2::widget([
    'id'=>'select_day',
    'name' => 'select_day',
    'data' => $data_days,
//    'options' => ['placeholder' => 'Select a state ...'],
]);

$data_tiempo_comida = ['DESAYUNO' => 'DESAYUNO', 'REFRIGERIO_DE_LA_MAÑANA' => 'REFRIGERIO_DE_LA_MAÑANA',
    'ALMUERZO' => 'ALMUERZO', 'REFRIGERIO_DE_LA_TARDE' => 'REFRIGERIO_DE_LA_TARDE',];

echo "<label>Seleccione el tiempo de comida</label>";
echo Select2::widget([
    'id'=>'select_tiempo_comida',
    'name' => 'select_tiempo_comida',
    'data' => $data_tiempo_comida,
]);

?>

<button id="deleteMenu" type="button" class="btn" > <?= Html::encode(Yii::t('app', 'Vaciar')) ?></button>