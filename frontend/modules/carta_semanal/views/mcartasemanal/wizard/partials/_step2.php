<?php

use frontend\assets\MixitUpAsset;
use yii\helpers\Html;

MixitUpAsset::register($this);
$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behavior_mixitup.js', ['depends' => [frontend\assets\AppAsset::className()]]);
$this->registerJsFile($webPath . '/extraOptions.js', ['depends' => [frontend\assets\AppAsset::className()]]);

$urlAjaxGetCartaSearch = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-get-cartas-search');
$urlAjaxViewAllIngredientes = \Yii::$app->urlManager->createUrl('preparacioncarta/mprepcarta/ajax-view-all-ingredientes');
$urlAjaxDeleteCarta = \Yii::$app->urlManager->createUrl('cartasemanal/mcartasemanal/ajax-del-carta');

if ($model->isNewRecord) {
    $urlAjaxDPrepCarta = \Yii::$app->urlManager->createUrl('admin/carta_semanal/mcartasemanal/ajax-d-prep-carta');
} else {
    $urlAjaxDPrepCarta = \Yii::$app->urlManager->createUrl('admin/carta_semanal/mcartasemanal/ajax-dprep-cu');
}

?>
    <div class="panel panel-default">
        <div class="panel-heading ">
            <div style="text-align:center; width:100%">
                <h2 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Detalles de la planificación')) : Html::encode(Yii::t('app', 'Actualizar Información Básica')) ?>
                </h2>
            </div>
        </div>
        <div class="panel-body">

            <div class="controls">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">


                                <input id="buscadorCartas" class="form-control" type="text" placeholder="Buscar menú"/>
                                <div class="input-group-addon" style="padding: 0px !important; ">
                                    <button type="button" id="btnBuscarCarta" href="" class="btn btn-white btn-sm"
                                       style="margin:0px !important">
                                        <i class="glyphicon glyphicon-search" title="Buscar"></i>
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <label><?= Html::encode(Yii::t('app', 'Extras:')) ?></label>
                        <button id="btnEliminarMenu" type="button"
                                class="radius"><?= Html::encode(Yii::t('app', 'Eliminar menú')) ?></button>
                        <button id="btnVerAllMenus" type="button"
                                class="radius"> <?= Html::encode(Yii::t('app', 'Ver Planificación Semanal')) ?></button>


                    </div>
                </div>

                <div class="row">

                    <div class=" col-sm-12">
                        <label>Ordenar:</label>

                        <button type="button" class=" active sort btn btn-sm radius" data-sort="myorder:asc">Asc</button>
                        <button type="button" class="sort btn btn-sm radius" data-sort="myorder:desc">Desc</button>


                        <label>Filtrar por :</label>
                        <button type="button" class="filter radius btn btn-sm"
                                data-filter="all"> <?= Html::encode(Yii::t('app', 'Todos')) ?></button>
                        <button type="button" class="filter radius btn btn-sm"
                                data-filter=".category-DESAYUNO"> <?= Html::encode(Yii::t('app', 'Desayunos')) ?></button>
                        <button type="button" class="filter radius btn btn-sm"
                                data-filter=".category-ALMUERZO"> <?= Html::encode(Yii::t('app', 'Almuerzos')) ?></button>
                        <button type="button" class="filter radius btn btn-sm"
                                data-filter=".category-REFRIGERIO_DE_LA_MAÑANA"> <?= Html::encode(Yii::t('app', 'R. Media Mañana')) ?></button>
                        <button type="button" class="filter radius btn btn-sm"
                                data-filter=".category-REFRIGERIO_DE_LA_TARDE"> <?= Html::encode(Yii::t('app', 'R. Media Tarde')) ?></button>

                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="Container-mix-menu" style="width: 100%;" class="mix_contanier">
                    <!--            <div class="mix category-desayuno" data-myorder="1">
                                </div>-->
                </div>
            </div>
        </div>

        <div class="container">

            <div class="form-group">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-left']) . Yii::t('app', 'Previous'), [

                    'class' => 'btn btn-default prev-step',
                    'id' => 'stepwizard_step2_prev']) ?>


                <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
            </div>


        </div>
    </div>

<?php
$script = <<< JS
    $(function(){
     urlAjaxGetCartaSearch =  '{$urlAjaxGetCartaSearch}';
     urlAjaxViewAllIngredientes =  '{$urlAjaxViewAllIngredientes}';
     urlAjaxDPrepcarta = '{$urlAjaxDPrepCarta}';
     urlAjaxDeleteCarta = '{$urlAjaxDeleteCarta}';
        })
  
JS;
$this->registerJs($script);
