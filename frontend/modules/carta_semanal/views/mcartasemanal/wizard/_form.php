<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
frontend\assets\HighchartAsset::register($this);

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorModalCarta.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>
</br>
<div class="panel">
    <div class="mplatillo-form ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <center>
                    <h1 style="color: black !important;">
                        <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Crear Nuevo Plan de Alimentación Semanal')) : Html::encode(Yii::t('app', 'Actualizar Plan de Alimentación Semanal')) ?>
                    </h1>
                </center>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => 'form',
                    'options'=>[
                            'style'=>'margin: 0px !important;'
                    ]]); ?>
                <?php
                $wizard_config = [
                    'id' => 'stepwizard',
                    'steps' => [
                        1 => [
                            'title' => 'Información básica',
                            'icon' => 'glyphicon glyphicon-edit',
                            'content' => $this->render('partials/_step1', ['model' => $model, 'form' => $form, 'listCibv' => $listCibv]),
                            'buttons' => [
                                'next' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],

//                                'next' => [
//                                    'title' => Yii::t('app', 'Next'),
//                                    'options' => [
//                                        'class' => 'disabled',
//                                        'hidden' => 'hidden',
//                                    ],
//                                ],
                            ],
                        ],
                        2 => [
                            'title' => 'Detalles de la planificación',
                            'icon' => 'glyphicon glyphicon-th',
                            'content' => $this->render('partials/_step2', ['model' => $model, 'd_model' => $d_model, 'form' => $form]),
                            'buttons' => [
                                'save' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                                'prev' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],

                            ],
                        ],
//                        2 => [
//                            'title' => 'Step 2',
//                            'icon' => 'glyphicon glyphicon-cloud-upload',
//                            'content' => $this->render('partials/_step2', ['model' => $model, 'd_model' => $d_model, 'form' => $form]),
//                            'buttons' => [
//                                'save' => [
//                                    'options' => [
//                                        'class' => 'disabled',
//                                        'hidden' => 'hidden',
//                                    ],
//                                ],
//                                'prev' => [
//                                    'title' => Yii::t('app', 'Previous'),
//                                ],
//                            ],
//                        ],
//                        3 => [
//                            'title' => 'Step 3',
//                            'icon' => 'glyphicon glyphicon-transfer',
//                            'content' => $this->render('partials/_step3', ['model' => $model, 'form' => $form]),
//                        ],
                    ],
//                    'complete_content' => "You are done!", // Optional final screen
                    'start_step' => 1, // Optional, start with a specific step
                ];

                ?>
                <?= drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!--<div class="form-group">-->
            <!--<center>-->       
        <?=
        ''
//    Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) 

        ?>
        <!--</center>-->
        <!--</div>-->



    </div>

</div>
