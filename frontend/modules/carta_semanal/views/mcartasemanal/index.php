<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\recursos_humanos\models\recursos\RrhhModelAux;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\carta_semanal\models\McartasemanalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Plan de Alimentación Semanal ');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="mcarta-semanal-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php if ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                    'm_c_sem_id',
                [
                    'attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'
                ],
                'm_c_sem_fecha_inicio',
                'm_c_sem_fecha_fin',
                [
                    'attribute' => 'm_c_sem_user_responsable',
                    'value' => 'm_c_sem_user_responsable',
                    'filter' => Html::activeDropDownList($searchModel, 'm_c_sem_user_responsable',
                        ArrayHelper::map(RrhhModelAux::getUsersFromRrhhByRoles(['nutricion', 'coordinador']), 
                            'user_id', 'nombres'), ['class' => 'form-control', 'prompt' => 'Seleccione al personal']),
                    'value' => function($model, $key, $index, $widget) {
                        return RrhhModelAux::getNombresTalentHumanoByUser($model->m_c_sem_user_responsable);
                    }
                ],
//                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}, {update}',
                ],
            ],
        ]);

        ?>
    <?php elseif ($rol == 'coordinador' || $rol == 'nutricion'): ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                    'm_c_sem_id',
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                'm_c_sem_fecha_inicio',
                'm_c_sem_fecha_fin',
                    [
                    'attribute' => 'm_c_sem_user_responsable',
                    'value' => 'm_c_sem_user_responsable',
                    'filter' => Html::activeDropDownList($searchModel, 'm_c_sem_user_responsable',
                        ArrayHelper::map(RrhhModelAux::getUsersFromRrhhByRoles(['nutricion', 'coordinador']), 
                            'user_id', 'nombres'), ['class' => 'form-control', 'prompt' => 'Seleccione al personal']),
                    'value' => function($model, $key, $index, $widget) {
                        return RrhhModelAux::getNombresTalentHumanoByUser($model->m_c_sem_user_responsable);
                    }
                ],
                    [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}, {update}',
                ],
            ],
        ]);

        ?>
    <?php elseif ($rol == 'coordinador-gad'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                    'm_c_sem_id',
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                'm_c_sem_fecha_inicio',
                'm_c_sem_fecha_fin',
                 [
                    'attribute' => 'm_c_sem_user_responsable',
                    'value' => 'm_c_sem_user_responsable',
                    'filter' => Html::activeDropDownList($searchModel, 'm_c_sem_user_responsable',
                        ArrayHelper::map(RrhhModelAux::getUsersFromRrhhByRoles(['nutricion', 'coordinador']), 
                            'user_id', 'nombres'), ['class' => 'form-control', 'prompt' => 'Seleccione al personal']),
                    'value' => function($model, $key, $index, $widget) {
                        return RrhhModelAux::getNombresTalentHumanoByUser($model->m_c_sem_user_responsable);
                    }
                ],
                    [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]);

        ?>
    <?php endif; ?>
</div>
