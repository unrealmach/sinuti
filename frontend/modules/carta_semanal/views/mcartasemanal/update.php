<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\MCartaSemanal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Mcarta Semanal',
        ]) . ' ' . $model->m_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Planificaciones Semanales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->m_c_sem_id, 'url' => ['view', 'id' => $model->m_c_sem_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mcarta-semanal-update">

    </br>
    <?=
    $this->render('wizard/_form', [
        'model' => $model,
        'd_model' => $d_model,
        'listCibv' => $listCibv,
    ])
    ?>

</div>
