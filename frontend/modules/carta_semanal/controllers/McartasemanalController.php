<?php

namespace frontend\modules\carta_semanal\controllers;

use Yii;
use backend\modules\carta_semanal\models\MCartaSemanal;
use backend\modules\carta_semanal\models\McartasemanalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\carta_semanal\models\recursos\CartaSemanalSession;
use backend\modules\carta_semanal\models\DCartaSemanal;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\preparacion_carta\models\MPrepCarta;
use backend\modules\preparacion_carta\models\recursos\PreparacionCartaSession;
use kartik\mpdf\Pdf;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use \backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\Nutricionista;

/**
 * McartasemanalController implements the CRUD actions for MCartaSemanal model.
 */
class McartasemanalController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MCartaSemanal models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new McartasemanalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single MCartaSemanal model.
     * carga el html para ver toda la planificacion semanal
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $composLunes = [];
        $composLunesTotal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $composMartes = [];
        $composMartesTotal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $composMiercoles = [];
        $composMiercolesTotal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $composJueves = [];
        $composJuevesTotal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $composViernes = [];
        $composViernesTotal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $d_model = DCartaSemanal::find()->where(['m_carta_semanal_id' => $id])->all();
        foreach ($d_model as $DCartaSem) {
            switch ($DCartaSem->d_c_sem_dia_semana) {
                case 'LUNES':
                    $composLunes[] = $this->composicionTotalDiaria($DCartaSem->m_prep_carta_id);
                    break;
                case 'MARTES':
                    $composMartes[] = $this->composicionTotalDiaria($DCartaSem->m_prep_carta_id);
                    break;
                case 'MIERCOLES':
                    $composMiercoles[] = $this->composicionTotalDiaria($DCartaSem->m_prep_carta_id);
                    break;
                case 'JUEVES':
                    $composJueves[] = $this->composicionTotalDiaria($DCartaSem->m_prep_carta_id);
                    break;
                case 'VIERNES':
                    $composViernes[] = $this->composicionTotalDiaria($DCartaSem->m_prep_carta_id);
                    break;

                default:
                    break;
            }
            if ($DCartaSem->d_c_sem_dia_semana == 'LUNES') {
                
            }
        }
        foreach ($composLunes as $value) {
            foreach ($value as $nut => $cantidad) {
                $composLunesTotal[$nut] += $cantidad;
            }
        }
        foreach ($composMartes as $value) {
            foreach ($value as $nut => $cantidad) {
                $composMartesTotal[$nut] += $cantidad;
            }
        }
        foreach ($composMiercoles as $value) {
            foreach ($value as $nut => $cantidad) {
                $composMiercolesTotal[$nut] += $cantidad;
            }
        }
        foreach ($composJueves as $value) {
            foreach ($value as $nut => $cantidad) {
                $composJuevesTotal[$nut] += $cantidad;
            }
        }
        foreach ($composViernes as $value) {
            foreach ($value as $nut => $cantidad) {
                $composViernesTotal[$nut] += $cantidad;
            }
        }
        $dataProvider = new ActiveDataProvider([
            'query' => DCartaSemanal::find()->where(['m_carta_semanal_id' => $model->m_c_sem_id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('view', [
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'composLunes' => $composLunesTotal,
                    'composMartes' => $composMartesTotal,
                    'composMiercoles' => $composMiercolesTotal,
                    'composJueves' => $composJuevesTotal,
                    'composViernes' => $composViernesTotal,
        ]);
    }

    /**
     * Suma todas las cantidades de los nutrientes de la carta semanal de acuerdo
     * al dia de la semana
     * @param int $m_pre_carta_id del MPrepCarta seleccionado
     * @author Sofia Mejia <amandasofiameia@gmail.com>
     * @return array
     */
    public function composicionTotalDiaria($m_pre_carta_id) {
        $composCero = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0, 'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];
        $compos = $this->composicionDiaria($m_pre_carta_id);
        foreach ($compos as $nut => $cantidad) {
            $composCero[$nut] += $cantidad;
        }
        return $composCero;
    }

    /**
     * Busca la composicion total de la prepcarta seleccionada
     * @param int $m_prep_carta_id del MPrepCarta seleccionado
     * @author Sofia Mejia <amandasofiameia@gmail.com>
     * @return array
     */
    public function composicionDiaria($m_prep_carta_id) {
        $model = MPrepCarta::findOne(['m_prep_carta_id' => $m_prep_carta_id]);
        Yii::$app->session->remove('alimentoPlatilloSeleccionado');
        Yii::$app->session->set('preparacionCartaSession', new PreparacionCartaSession());
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->updateSesion($model);
        return $sesionDetalle->maestro['composicionTotal'];
    }

    /**
     * Creates a new MCartaSemanal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MCartaSemanal();
        $d_model = new DCartaSemanal;
        $d_model1 = [];
        $listCibv = Cibv::find()->all();
        $listDetalle = [];
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $model->m_c_sem_user_responsable = strval(\Yii::$app->user->id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo.');
        }
        if (\Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $validate = $model->validate();
            if (!$validate) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return \yii\widgets\ActiveForm::validate($model);
            }
        }
        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            if (!empty($asignacionCoordinadorCibv)) {
                $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
                if (!empty($autoridadCibv)) {
                    $listCibv = Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
                    
                    $model->nombre_user = $autoridadCibv->coordinador->nombreCompleto;
                } else {
                    throw new NotFoundHttpException('Aun no se ha asignado el coordinador al centro infantil');
                }
            } else {
                throw new NotFoundHttpException('Aun no se registra el coordinador para el centro infantil');
            }
        }
        if ($authAsiignmentMdel->item_name == 'nutricion') {
            $user = \webvimark\modules\UserManagement\models\User::findOne(['id' => \Yii::$app->user->id]);
            if (!empty($user)) {
                $nutricionista = Nutricionista::findOne(['nutricionista_correo' => $user->email]);
                $model->nombre_user = $nutricionista->nombreCompleto;
            } else {
                throw new NotFoundHttpException('No existe un nutricionista activo para realizar esta operación');
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session->get('cartaSemanalSession');
            $dataDetalle = $session->getCartaSemanal();
            $model->m_c_sem_fecha_inicio = $model->m_c_sem_fecha_inicio." 00:00:00";
            $model->m_c_sem_fecha_fin = $model->m_c_sem_fecha_fin." 23:59:59";
            $validate = $model->validate();
            if ($validate == false) {
//                Yii::$app->getSession()->setFlash('error', 'Incoherencia en datos');
            }
            foreach ($dataDetalle as $dia => $tiempo_comida) {
                foreach ($tiempo_comida as $key2 => $mprep_carta_id) {
                    $temp = new DCartaSemanal();
                    if ($mprep_carta_id != 0) {
                        array_push($listDetalle, $mprep_carta_id);
                        $temp->d_c_sem_dia_semana = $dia;
                        $temp->m_prep_carta_id = $mprep_carta_id;
                        array_push($d_model1, $temp);
                    }
                }
            }
            foreach ($d_model1 as $sub_model) {
                $validate = $sub_model->validate() && $validate;
            }
            if (count(array_unique($listDetalle)) <= 0) {
                Yii::$app->getSession()->setFlash('error', 'La planificación debe tener al menos un menú diario');
            } else {
                if ($validate) {
//                 Instancia uan transaccion
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
//                     Valida maestro 
                        if ($flag = $model->save(false)) {
                            foreach ($d_model1 as $detalle) {
//                             agrega el id del maestro a los detalles
                                $detalle->m_carta_semanal_id = $model->m_c_sem_id;
//                              valida la consistencia del detalle
                                if (!($flag = $detalle->save(false))) {
//                            Regresa la transaccion a su estado inicial
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
//                        si el maestro y detalles se validan correctamente
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->m_c_sem_id]);
                        }
                    } catch (Exception $exc) {
                        $transaction->rollBack();
                    }
                } else {
                    var_dump($model->getErrors());
                    foreach ($d_model1 as $key => $value) {
                        var_dump($value->getErrors());
                    }
                }
            }
        }
        Yii::$app->session->set('cartaSemanalSession', new CartaSemanalSession());


        return $this->render('create', [
                    'model' => $model,
                    'd_model' => $d_model,
                    'listCibv' => $listCibv,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->asignacionInfanteCSemanals) {
            throw new NotFoundHttpException('La planificación semanal ya ha sido asignada');
        }
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo.');
        }
        $d_model = $model->dCartaSemanals;

        if (!Yii::$app->request->post()) {
            Yii::$app->session->set('cartaSemanalSession', new CartaSemanalSession());

            $session = Yii::$app->session->get('cartaSemanalSession');

            foreach ($d_model as $value) {
                //poner en util
                switch ($value->mPrepCarta->tiempoComida->tiem_com_nombre) {
                    case 'DESAYUNO': {
//                            $session->setCartUpdateIdByDayAndTime($value->d_c_sem_dia_semana, 'DESAYUNO', $value->d_c_sem_id);
                            $session->setCartIdByDayAndTime($value->d_c_sem_dia_semana, 'DESAYUNO', $value->m_prep_carta_id);
                            break;
                        }
                    case 'REFRIGERIO_DE_LA_MAÑANA': {
//                            $session->setCartUpdateIdByDayAndTime($value->d_c_sem_dia_semana, 'REFRIGERIO_DE_LA_MAÑANA', $value->d_c_sem_id);
                            $session->setCartIdByDayAndTime($value->d_c_sem_dia_semana, 'REFRIGERIO_DE_LA_MAÑANA', $value->m_prep_carta_id);
                            break;
                        }
                    case 'ALMUERZO': {
//                            $session->setCartUpdateIdByDayAndTime($value->d_c_sem_dia_semana, 'ALMUERZO', $value->d_c_sem_id);
                            $session->setCartIdByDayAndTime($value->d_c_sem_dia_semana, 'ALMUERZO', $value->m_prep_carta_id);
                            break;
                        }
                    case 'REFRIGERIO_DE_LA_TARDE': {
//                            $session->setCartUpdateIdByDayAndTime($value->d_c_sem_dia_semana, 'REFRIGERIO_DE_LA_TARDE', $value->d_c_sem_id);
                            $session->setCartIdByDayAndTime($value->d_c_sem_dia_semana, 'REFRIGERIO_DE_LA_TARDE', $value->m_prep_carta_id);
                            break;
                        }
                }
            }
        }
        $d_model1 = [];
        $listCibv = Cibv::find()->all();
        $listDetalle = [];
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $model->m_c_sem_user_responsable = strval(\Yii::$app->user->id);
        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            if (!empty($asignacionCoordinadorCibv)) {
                $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
                if (!empty($autoridadCibv)) {
                    $listCibv = Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
                    $model->nombre_user = $autoridadCibv->coordinador->nombreCompleto;
                } else {
                    throw new NotFoundHttpException('Aun no se ha asignado el coordinador al centro infantil');
                }
            } else {
                throw new NotFoundHttpException('Aun no se registra el coordinador para el centro infantil');
            }
        }
        if ($authAsiignmentMdel->item_name == 'nutricion') {
            $user = \webvimark\modules\UserManagement\models\User::findOne(['id' => \Yii::$app->user->id]);
            if (!empty($user)) {
                $nutricionista = Nutricionista::findOne(['nutricionista_correo' => $user->email]);
                $model->nombre_user = $nutricionista->nombreCompleto;
            } else {
                throw new NotFoundHttpException('No existe un nutricionista activo para realizar esta operación');
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            $detallesParaBorrar = $d_model;


            $session = Yii::$app->session->get('cartaSemanalSession');
            $dataDetalle = $session->getCartaSemanal();
            $validate = $model->validate();
            foreach ($dataDetalle as $dia => $tiempo_comida) {
                foreach ($tiempo_comida as $key2 => $mprep_carta_id) {
                    $temp = new DCartaSemanal();
                    if ($mprep_carta_id != 0) {
                        array_push($listDetalle, $mprep_carta_id);
                        $temp->d_c_sem_dia_semana = $dia;
                        $temp->m_prep_carta_id = $mprep_carta_id;
                        array_push($d_model1, $temp);
                    }
                }
            }
            foreach ($d_model1 as $sub_model) {
                $validate = $sub_model->validate() && $validate;
            }
            if (count(array_unique($listDetalle)) <= 0) {
                Yii::$app->getSession()->setFlash('error', 'La planificación debe tener al menos un menú diario');
            } else {

                $transaction2 = \Yii::$app->db->beginTransaction();
                $paraBorrar = [];
                foreach ($detallesParaBorrar as $key3 => $model3) {
                    $paraBorrar[] = $model3->d_c_sem_id;
                }
                if (DCartaSemanal::deleteAll(['in', 'd_c_sem_id', array_values($paraBorrar)]) > 0) {
                    $transaction2->commit();
                } else {
                    $transaction2->rollBack();
                };



                if ($validate) {
//                 Instancia uan transaccion
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
//                     Valida maestro 
                        if ($flag = $model->save(false)) {
                            foreach ($d_model1 as $detalle) {
//                             agrega el id del maestro a los detalles
                                $detalle->m_carta_semanal_id = $model->m_c_sem_id;
//                              valida la consistencia del detalle
                                if (!($flag = $detalle->save(false))) {
//                            Regresa la transaccion a su estado inicial
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
//                        si el maestro y detalles se validan correctamente
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $model->m_c_sem_id]);
                        }
                    } catch (Exception $exc) {
                        $transaction->rollBack();
                    }
                } else {
                    var_dump($model->getErrors());
                    foreach ($d_model1 as $key => $value) {
                        var_dump($value->getErrors());
                    }
                }
            }
        }
//        Yii::$app->session->set('cartaSemanalSession', new CartaSemanalSession());
        return $this->render('create', [
                    'model' => $model,
                    'd_model' => $d_model,
                    'listCibv' => $listCibv,
        ]);
    }

    /**
     * Deletes an existing MCartaSemanal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MCartaSemanal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MCartaSemanal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MCartaSemanal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Permite generar el PDF de la planificacion semanal
     * @param type $carta_semanal_id
     * @return PDF
     */
    public function actionMakePdfCartaSemanal($carta_semanal_id) {
        $model = $this->findModel($carta_semanal_id);

        $content = $this->renderPartial('reports/_alimento_platillo', [
            'model' => $model,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::DEST_BROWSER,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px} input{ width:100px !important, heigth: 200px !important}; .btn, button{visibility: hidden !important;}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Receta '],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Sinuti Report'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
//      $pdf->Output($pdf->content,'','');
//exit;
        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Elimina una carta del menu semanal via ajax
     * @param string $dia
     * @param string $fecha
     * @author Sofia Mejia <amandasofiameia@gmail.com>
     * @return type
     */
    public function actionAjaxDelCarta($dia = NULL, $fecha = NULL) {
        if (!is_null($dia) && !is_null($fecha)) {
            $this->ajaxDPrepCarta($dia, $fecha);
        }

        return $this->renderAjax("wizard/partials/_deleteMenu");
    }

    /**
     * Ayuda con la eliminaión de la carta
     * A la carta_id le pone 0
     * @param type $dia
     * @param type $fecha
     * @author Sofia Mejia <amandasofiameia@gmail.com>
     */
    private function ajaxDPrepCarta($dia, $fecha) {
        $carta_id = 0;
        $dia = $dia;
        $tiempoComida = $fecha;
        $session = Yii::$app->session->get('cartaSemanalSession');
        $session->setCartIdByDayAndTime($dia, $tiempoComida, $carta_id);
    }

}
