<?php

namespace frontend\modules\centro_infantil\controllers;

use Yii;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\centro_infantil\models\CibvSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\centro_infantil\models\GrupoComite;
use backend\modules\recursos_humanos\models\ComitePadresFlia;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\metricas_individuo\models\DatoAntropometrico;
use \backend\modules\rbac\models\AuthAssignment;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;

/**
 * CibvController implements the CRUD actions for Cibv model.
 */
class CibvController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cibv models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CibvSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authoAssigment = \backend\modules\rbac\models\AuthAssignment::findOne([
            'user_id' => Yii::$app->user->id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'rol' => $authoAssigment->item_name,
        ]);
    }

    /**
     * Displays a single Cibv model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $grupo_comite = GrupoComite::getComiteByPeriodo($id)->one();
        $authoAssigment = \backend\modules\rbac\models\AuthAssignment::findOne([
            'user_id' => Yii::$app->user->id]);
        $dataProvider = [];
        $dataProvider2 = [];
        $grupo_comite_id = 0;
        $autoridad_cibv_id = 0;
        if (!is_null($grupo_comite)) {
            $comite_central = ComitePadresFlia::find()->where(['grupo_comite_id' => $grupo_comite->grupo_comite_id]);
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => $comite_central,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);
            $grupo_comite_id = $grupo_comite->grupo_comite_id;
        }
        $autoridad_cibv = AutoridadesCibv::getAutoridadesByPeriodo($id)->one();
        if (!is_null($autoridad_cibv)) {
            $dataProvider2 = new \yii\data\ActiveDataProvider([
                'query' => AutoridadesCibv::getAutoridadesByPeriodo($id),
                'pagination' => [
                    'pageSize' => 1,
                ],
            ]);
            $autoridad_cibv_id = $autoridad_cibv->autoridades_cibv_id;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'grupo_comite_id' => $grupo_comite_id,
            'comite_central' => $dataProvider,
            'autoridad_cibv_id' => $autoridad_cibv_id,
            'autoridad_cibv' => $dataProvider2,
            'rol' => $authoAssigment->item_name,
        ]);
    }

    /**
     * Creates a new Cibv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cibv();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Creación exitosa');
            return $this->redirect(['view', 'id' => $model->cen_inf_id]);
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Cibv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Creación exitosa');
            return $this->redirect(['view', 'id' => $model->cen_inf_id]);
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Cibv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Eliminación exitosa');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Cibv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cibv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cibv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Permite ver el estado de crecimiento y nutricional de infantes
     * de acuerdo al centro infantil
     * @param int $cibv_id
     * @param string $fecha_consulta Indica la fecha con la cual se realiza la consulta
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return mixed
     */
    public function actionReportEstadoNutricional($cibv_id = null, $fecha_consulta = null)
    {
        $fecha_consulta = is_null($fecha_consulta) ? date("Y-m") : $fecha_consulta;
        $authAsiignmentMdel = AuthAssignment::find()->getRoleByUser();
        $days = Yii::$app->Util->getDaysOfMonth($fecha_consulta);
        $dataDays = current($days) . " - " . end($days);
        $is_admin = true;

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->getOneAsignacionUserCoordinadorCibv();

        if (!empty($asignacionCoordinadorCibv)) {
            $cibv_id= AutoridadesCibv::getCibvIdByAutoridad($asignacionCoordinadorCibv);

        }


        if (is_null($cibv_id)) {
            if ($authAsiignmentMdel->item_name != 'Admin' && $authAsiignmentMdel->item_name != 'coordinador-gad' && $authAsiignmentMdel->item_name != 'nutricion' && $authAsiignmentMdel->item_name != 'sysadmin') {
                throw new NotFoundHttpException('No tiene permisos para estos reportes');
            }
            $cibv_id = Cibv::find()->limit(1)->one();
            if (!isset($cibv_id)) {
                throw new NotFoundHttpException('Aun no se ha ingresado ningun centro infantil al sistema al sistema');
            } else {
                $cibv_id = $cibv_id->cen_inf_id;
            }

        }


        switch ($authAsiignmentMdel->item_name) {
            case 'Admin': $is_admin = true;
                break;
            case 'coordinador-gad': $is_admin = true;
                break;
            case  'nutricion': $is_admin = true;
                break;
            case 'sysadmin': $is_admin = true;
                break;
            default :
                $is_admin = false;
                break;
        }


        $out = $this->getDataToView($cibv_id, $fecha_consulta);
        return $this->render('report/index', [
            'fecha_consulta' => $fecha_consulta,
            'is_admin' => $is_admin,
            'cibv_id' => $cibv_id,
            'data' => $out,
            'dataDays' => $dataDays
        ]);
    }

    /**
     * Permite generar la grafica con los datos del estado nutricional
     * de los infantes de acuerdo al cibv
     * @param int $cibv_id
     * @param string $fecha_consulta Indica la fecha con la cual se realiza la consulta
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    private function getDataToView($cibv_id, $fecha_consulta = null)
    {
        $days = Yii::$app->Util->getDaysOfMonth($fecha_consulta);
        $metricas = Yii::$app->Util->getAllDominioMetricas();


        $data = new DatoAntropometrico();
        $out = $data->getDataToHighchart($cibv_id, $days, $metricas);

        return $out;
    }
}
