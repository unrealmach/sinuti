<?php

namespace frontend\modules\centro_infantil\controllers;

use Yii;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\centro_infantil\models\SectorasignadoinfanteSearch;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\individuo\models\Infante;
use backend\modules\parametros_sistema\models\Periodo;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectorasignadoinfanteController implements the CRUD actions for SectorAsignadoInfante model.
 */
class SectorasignadoinfanteController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SectorAsignadoInfante models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SectorasignadoinfanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authoAssigment = \backend\modules\rbac\models\AuthAssignment::findOne(['user_id' => Yii::$app->user->id]);


        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authoAssigment->item_name,
        ]);
    }

    /**
     * Displays a single SectorAsignadoInfante model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SectorAsignadoInfante model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($sector_asig_id = null) {
        $model = new SectorAsignadoInfante();
        $listInfantes = [];
        $listSectorAsignado = [];
        $periodo = Periodo::find()->periodoActivo()->one();
        if (Empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo. Por favor comuniquese con el coordinador del GAD');
        }
        if ($sector_asig_id != null) {
            $model->sector_asignado_id = $sector_asig_id;
            $sectorAsignado = SectorAsignadoEducador::findOne(['sector_asignado_educador_id' => $sector_asig_id]);
            $matriculas = Matricula::find()->deCentroInfantiIdAndPeriodoId($sectorAsignado->cen_inf_id, $sectorAsignado->periodo_id)->all();

            foreach ($matriculas as $value) {
                $validateAsigInfante = SectorAsignadoInfante::find()->InfanteSalonAsignado($value->infante_id)->one();

                if (empty($validateAsigInfante)) {
                    array_push($listInfantes, Infante::findOne(['infante_id' => $value->infante_id]));
                }else {
                    $listInfantes = Infante::find()->all();
                }
            }
            //TODO: no dbn de salir infants ya asignados a un sector
//            var_dump($listInfantes); die();
            $listSectorAsignado = SectorAsignadoEducador::find()->where(['sector_asignado_educador_id' => $sector_asig_id])->all();
        } else {
            $listInfantes = Infante::find()->all();
            $listSectorAsignado = SectorAsignadoEducador::find()->all();
        }
        if ($model->load(Yii::$app->request->post())) {
            $validateSectorAsig = SectorAsignadoInfante::find()->infanteSalonAsignado($model->infante_id)->one();
            if (!empty($validateSectorAsig))
                throw new NotFoundHttpException('El infante ya tiene salon asignado');
            if ($model->save()) {

                return $this->redirect(['/recursoshumanos/educador/view', 'id' => $sectorAsignado->educador_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'listInfantes' => $listInfantes,
                        'listSectorAsignado' => $listSectorAsignado,
            ]);
        }
    }

    /**
     * Updates an existing SectorAsignadoInfante model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $listInfantes = [];
        $listSectorAsignado = [];
        if ($model->sector_asignado_id) {
            $listInfantes = Infante::find()->where(['infante_id' => $model->infante_id])->all();
            $listSectorAsignado = SectorAsignadoEducador::find()->where(['sector_asignado_educador_id' => $model->sector_asignado_id])->all();
        } else {
            $listInfantes = Infante::find()->all();
            $listSectorAsignado = SectorAsignadoEducador::find()->all();
        }
        if ($model->load(Yii::$app->request->post())) {
//            $validateSectorAsig = SectorAsignadoInfante::find()->infanteSalonAsignado($model->infante_id)->one();
//            if (!empty($validateSectorAsig))
//                throw new NotFoundHttpException('El infante ya tiene salon asignado');
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->sector_asignado_infante_id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'listInfantes' => $listInfantes,
                        'listSectorAsignado' => $listSectorAsignado,
            ]);
        }
    }

    /**
     * Deletes an existing SectorAsignadoInfante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SectorAsignadoInfante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SectorAsignadoInfante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SectorAsignadoInfante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
