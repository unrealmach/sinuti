<?php

namespace frontend\modules\centro_infantil\controllers;

use Yii;
use backend\modules\centro_infantil\models\GrupoComite;
use backend\modules\centro_infantil\models\GrupocomiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\recursos_humanos\models\ComitePadresFlia;
use common\models\recursos\DynamicFormModel;
use yii\data\ArrayDataProvider;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use yii\helpers\ArrayHelper;

/**
 * GrupocomiteController implements the CRUD actions for GrupoComite model.
 */
class GrupocomiteController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GrupoComite models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new GrupocomiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GrupoComite model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        //crea las relaciones con el detalle
        $d_model = $this->findModel($id)->comitePadresFlias;
        $all_data = [];
        foreach ($d_model as $key => $data) {
            $atributos = $data->attributes;
            $atributos['nombre_completo'] = $atributos['comite_nombres'] . ' ' . $atributos['comite_apellidos'];
//            agrega los atributos de cada detalle en un arreglo
            array_push($all_data, $atributos);
        }
//        transforma el arreglo en un dataprovider 
        $provider = new ArrayDataProvider([
            'allModels' => $all_data,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'provider' => $provider,
        ]);
    }

    /**
     * Creates a new GrupoComite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cen_inf_id = null) {
        $model = new GrupoComite();
        $d_model = [new ComitePadresFlia];
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }
        $listCibv = [];
        if ($cen_inf_id != null) {
            $model->cen_inf_id = $cen_inf_id;
            $listCibv = Cibv::find()->where(['cen_inf_id' => $cen_inf_id])->all();
        } else {
            $listCibv = Cibv::find()->all();
        }

        $countDmodel = 0;
        if ($model->load(Yii::$app->request->post())) {
            //crea el objeto de multiples detalles
            $d_model = DynamicFormModel::createMultiple(ComitePadresFlia::className());
            DynamicFormModel::loadMultiple($d_model, \Yii::$app->request->post());

            $validate = $model->validate();
            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;
            foreach ($d_model as $value) {
                $countDmodel = ++$countDmodel;
            }
            if ($countDmodel < 4) {
                Yii::$app->getSession()->setFlash('error', 'El comite debe tener 4 representantes');
            } else {
                if ($validate) {
//                Instancia una transacción
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
//                    Se valida el padre con todos sus atributos, pero aun no esta guardado en la bd
                        if ($flag = $model->save(false)) {
                            foreach ($d_model as $comite) {
//                            agrega el id del padre a cada detalle
                                $comite->grupo_comite_id = $model->grupo_comite_id;
//                            Valida la consistencia del detalle
                                if (!($flag = $comite->save(false))) {
//                                Regresa la transaccion a su estado inicial
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
//                        Si el maestro y lso detalles se validan exitosamente termina la transaccion
                            $transaction->commit();
                            return $this->redirect(['//centroinfantil/cibv/view', 'id' => $model->cen_inf_id]);
                        }
                    } catch (Exception $ex) {
                        $transaction->rollBack();
                    }
                }
            }
        }
        return $this->render('create', [
                    'model' => $model,
                    'd_model' => (empty($d_model)) ? [new ComitePadresFlia] : $d_model,
                    'periodo' => $periodo,
                    'listCibv' => $listCibv,
        ]);
    }

    /**
     * Updates an existing GrupoComite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $cen_inf_id = null) {
        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }
        $d_model = $model->comitePadresFlias;
        $listCibv = [];
        if ($cen_inf_id != null) {
            $model->cen_inf_id = $cen_inf_id;
            $listCibv = Cibv::find()->where(['cen_inf_id' => $cen_inf_id])->all();
        } else {
            $listCibv = Cibv::find()->all();
        }
        $countDmodel = 0;
        if ($model->load(Yii::$app->request->post())) {

            $oldIds = ArrayHelper::map($d_model, 'comite_id', 'comite_id');
            $d_model = DynamicFormModel::createMultiple(ComitePadresFlia::className(), $d_model, 'comite_id', 'comite_id');
            DynamicFormModel::loadMultiple($d_model, Yii::$app->request->post());
            $deletedIds = array_diff($oldIds, array_filter(ArrayHelper::map($d_model, 'comite_id', 'comite_id')));

            $validate = $model->validate();
            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;
            foreach ($d_model as $value) {
                $countDmodel = ++$countDmodel;
            }
            if ($countDmodel < 4) {
                Yii::$app->getSession()->setFlash('error', 'El comite debe tener 4 representantes');
            } else {
                if ($validate) {
//                Instancia una transacción
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
//                    Se valida el padre con todos sus atributos, pero aun no esta guardado en la bd
                        if ($flag = $model->save(false)) {
                            if (!empty($deletedIds)) {
                                ComitePadresFlia::deleteAll(['comite_id' => $deletedIds]);
                            }
                            foreach ($d_model as $comite) {
//                            agrega el id del padre a cada detalle
                                $comite->grupo_comite_id = $model->grupo_comite_id;
//                            Valida la consistencia del detalle
                                if (!($flag = $comite->save(false))) {
//                                Regresa la transaccion a su estado inicial
                                    $transaction->rollBack();
                                    break;
                                }
                            }
                        }
                        if ($flag) {
//                        Si el maestro y lso detalles se validan exitosamente termina la transaccion
                            $transaction->commit();
                            return $this->redirect(['//centroinfantil/cibv/view', 'id' => $model->cen_inf_id]);
                        }
                    } catch (Exception $ex) {
                        $transaction->rollBack();
                    }
                }
            }
        }
        return $this->render('update', [
                    'model' => $model,
                    'periodo' => $periodo,
                    'd_model' => (empty($d_model)) ? [new ComitePadresFlia] : $d_model,
                    'listCibv' => $listCibv,
        ]);
    }

    /**
     * Deletes an existing GrupoComite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GrupoComite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GrupoComite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = GrupoComite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
