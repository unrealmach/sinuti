<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\centro_infantil\models\CibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Centros Infantiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cibv-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php
//    var_dump($dataProvider->getModels());
//    die();
    // echo $this->render('_search', ['model' => $searchModel]); 
//    TODO agregar a la vista las autoridades del cibv y el comite central
    ?>

    <?php if ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <p>

            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar nuevo'), ['create'], ['class' => 'btn btn-default']) ?>
        </p>




        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'cen_inf_id',
                ['attribute' => 'parroquia_id',
                    'value' => 'parroquia.parroquia_nombre'],
                ['attribute' => 'autoridad_cibv',
                    'value' => 'autoridadCibv.coordinador.coordinador_nombres'],
                'cen_inf_nombre',
                'cen_inf_direccion',
                'cen_inf_telefono',
                // 'cen_inf_correo',
                // 'cen_inf_activo',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'parroquia_id',
                    'value' => 'parroquia.parroquia_nombre'],
                ['attribute' => 'autoridad_cibv',
                    'value' => 'autoridadCibv.coordinador.coordinador_nombres'],
                'cen_inf_nombre',
                'cen_inf_direccion',
                'cen_inf_telefono',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view} {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador-gad') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                ['attribute' => 'parroquia_id',
                    'value' => 'parroquia.parroquia_nombre'],
                ['attribute' => 'autoridad_cibv',
                    'value' => 'autoridadCibv.coordinador.coordinador_nombres'],
                'cen_inf_nombre',
                'cen_inf_direccion',
                'cen_inf_telefono',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>
</div>
