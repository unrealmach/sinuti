<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */

$this->title = Yii::t('app', 'Crear Centro Infantil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Centros Infantiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cibv-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
