<?php

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use \backend\modules\centro_infantil\models\Cibv;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorReport.js', ['depends' => [frontend\assets\AppAsset::className()]]);

$BAJOPESO = $data['listaInfantes']['PESO']['BAJOPESO'];
$RIESGOBAJOPESO = $data['listaInfantes']['PESO']['RIESGOBAJOPESO'];
$PESONORMAL = $data['listaInfantes']['PESO']['PESONORMAL'];
$RIESGOSOBREPESO = $data['listaInfantes']['PESO']['RIESGOSOBREPESO'];
$SOBREPESO = $data['listaInfantes']['PESO']['SOBREPESO'];


$BAJATALLASEVERA = $data['listaInfantes']['TALLA']['BAJATALLASEVERA'];
$TALLABAJA = $data['listaInfantes']['TALLA']['TALLABAJA'];
$TALLANORMAL = $data['listaInfantes']['TALLA']['TALLANORMAL'];
$TALLAALTA = $data['listaInfantes']['TALLA']['TALLAALTA'];
$TALLAMUYALTA = $data['listaInfantes']['TALLA']['TALLAMUYALTA'];

$DESNUTRICIONCRONICA = $data['listaInfantes']['DESNUTRICIONTALLA']['DESNUTRICIONCRONICA'];
$DTNOAPLICA = $data['listaInfantes']['DESNUTRICIONTALLA']['NOAPLICA'];


$DESNUTRICIONGLOBAL = $data['listaInfantes']['DESNUTRICIONPESO']['DESNUTRICIONGLOBAL'];
$DESNUTRICIONAGUDA = $data['listaInfantes']['DESNUTRICIONPESO']['DESNUTRICIONAGUDA'];
$DPNOAPLICA = $data['listaInfantes']['DESNUTRICIONPESO']['NOAPLICA'];


?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode("Reporte Poblacional") ?></h1>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                    if ($is_admin) {
                        echo "<label class='col-sm-12 control-label'>  Centro Infantil</label> ";
                        echo "<div class='col-sm-12 control-label'>";
                        echo Select2::widget([
                            'name' => 'cibv_id',
                            'id' => 'cibv_id',
                            'value' => $cibv_id,
                            'data' => ArrayHelper::map(Cibv::find()->all(),
                                'cen_inf_id', 'cen_inf_nombre'),
                            'options' => ['class' => 'col-sm-12',
                                'onchange' =>
                                    "  location.href =
                'http://'+window.location.hostname+'/centroinfantil/cibv/report-estado-nutricional'+
                '?cibv_id='+$('#cibv_id').val()+'&fecha_consulta='+$('#fecha_consulta').val()",
                            ],
                        ]);
                        echo "</div>";
                    } else {
                        echo "<label class='col-sm-12 control-label'>  Centro Infantil: <b>" .
                            Cibv::findOne($cibv_id)->cen_inf_nombre .
                            "</b></label> ";

                        echo "<input type='hidden' name='cibv_id' id='cibv_id' value='$cibv_id'>";
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php

                    echo "<label class='col-sm-12 control-label'>  Seleccione el mes de consulta </label> ";
                    echo "<div class='col-sm-12 control-label'>";


                    echo DatePicker::widget([
                        'name' => 'fecha_consulta',
                        'id' => 'fecha_consulta',
                        'value' => $fecha_consulta,
                        'options' => ['placeholder' => 'Seleccione el mes a consultar',
                            'onchange' =>
                                "  location.href =
                'http://'+window.location.hostname+'/centroinfantil/cibv/report-estado-nutricional'+
                '?cibv_id='+$('#cibv_id').val()+'&fecha_consulta='+$('#fecha_consulta').val()",],
                        'readonly' => true,
                        'pluginOptions' => [
                            'format' => 'yyyy-mm',
                            'startView' => 'months',
                            'minViewMode' => 'months'
                        ]
                    ]);
                    echo "</div>";
                    ?>
                </div>
            </div>

        </div>
        </br>
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#vtab1" data-toggle="tab"><i class="fa fa-pie-chart"></i>
                                        Gráfica Seguimiento</a>
                                </li>
                                <li>
                                    <a href="#vtab2" data-toggle="tab"><i class="fa fa-user-md"></i>
                                        Detalle</a>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="vtab1">
                                    <div class="row">
                                        <?php
                                        echo $this->render('estado_nutricional/_reportDrillPie',
                                            [
                                                'data' => $data,
                                                'dataDays' => $dataDays,
                                                'cibv' => \backend\modules\centro_infantil\models\Cibv::findOne($cibv_id)->cen_inf_nombre, //TODO enviar nombrecibv
                                            ]);
                                        ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade " id="vtab2">
                                    <div class="row">
                                        <div class="panel with-nav-tabs">
                                            <div class="panel-heading">
                                                <h1>Seleccione Métrica</h1>
                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li class="dropdown active ">
                                                        <a href="#" role="tab" data-toggle="dropdown">
                                                            <!--<i class="fa fa-balance-scale"></i>-->
                                                            Peso</a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#ptab1" data-toggle="tab">Bajo Peso</a></li>
                                                            <li><a href="#ptab2" data-toggle="tab">Riesgo Bajo Peso</a></li>
                                                            <li><a href="#ptab3" data-toggle="tab">Peso Normal</a></li>
                                                            <li><a href="#ptab4" data-toggle="tab">Riesgo Sobre Peso</a>
                                                            </li>
                                                            <li><a href="#ptab5" data-toggle="tab">Sobre Peso</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" role="tab" data-toggle="dropdown">
                                                            <!--<i class="fa fa-info-circle"></i>-->
                                                            Talla</a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#ttab1" data-toggle="tab">Baja Talla Severa</a></li>
                                                            <li><a href="#ttab2" data-toggle="tab">Talla Baja</a></li>
                                                            <li><a href="#ttab3" data-toggle="tab">Talla Normal</a></li>
                                                            <li><a href="#ttab4" data-toggle="tab">Talla Alta</a></li>
                                                            <li><a href="#ttab5" data-toggle="tab">Talla Muy Alta</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" role="tab" data-toggle="dropdown">
                                                           <!-- <i class="fa fa-medkit"></i>-->
                                                            Desnutrición Peso</a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#dptab1" data-toggle="tab">Desnutrición Crónica</a></li>
                                                            <li><a href="#dptab2" data-toggle="tab">No Aplica</a></li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" role="tab" data-toggle="dropdown">
                                                     <!--       <i class="fa fa-heartbeat"></i>-->
                                                            Desnutrición Talla</a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#dttab1" data-toggle="tab">Desnutrición Global</a></li>
                                                            <li><a href="#dttab2" data-toggle="tab">Desnutrición Aguda</a></li>
                                                            <li><a href="#dttab3" data-toggle="tab">No Aplica</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade" id="ptab1">
                                                    <h1>Bajo Peso (<?= count($BAJOPESO) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/bajo_peso', ['BAJOPESO' => $BAJOPESO]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ptab2">
                                                    <h1>Riesgo Bajo Peso (<?= count($RIESGOBAJOPESO) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/riesgo_bajo_peso', ['RIESGOBAJOPESO' => $RIESGOBAJOPESO]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ptab3">
                                                    <h1>Peso Normal (<?= count($PESONORMAL) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/peso', ['PESONORMAL' => $PESONORMAL]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ptab4">
                                                    <h1>Riesgo Sobre Peso (<?= count($RIESGOSOBREPESO) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/riesgo_sobre_peso', ['RIESGOSOBREPESO' => $RIESGOSOBREPESO]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ptab5">
                                                    <h1>Sobre Peso (<?= count($SOBREPESO) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/sobre_peso', ['SOBREPESO' => $SOBREPESO]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ttab1">
                                                    <h1>Baja Talla Severa (<?= count($BAJATALLASEVERA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/baja_talla_severa', ['BAJATALLASEVERA' => $BAJATALLASEVERA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ttab2">
                                                    <h1>Talla Baja (<?= count($TALLABAJA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/talla_baja', ['TALLABAJA' => $TALLABAJA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ttab3">
                                                    <h1>Talla Normal (<?= count($TALLANORMAL) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/talla', ['TALLANORMAL' => $TALLANORMAL]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ttab4">
                                                    <h1>Talla Alta (<?= count($TALLAALTA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/talla_alta', ['TALLAALTA' => $TALLAALTA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="ttab5">
                                                    <h1>Talla Muy ALta (<?= count($TALLAMUYALTA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/talla_muy_alta', ['TALLAMUYALTA' => $TALLAMUYALTA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="dptab1">
                                                    <h1>Desnutrición Crónica (<?= count($DESNUTRICIONCRONICA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/desnutricion_cronica_peso', ['DESNUTRICIONCRONICA' => $DESNUTRICIONCRONICA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="dptab2">
                                                    <h1>No Aplica (<?= count($DPNOAPLICA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/desnutricion_peso_no_aplica', ['DPNOAPLICA' => $DPNOAPLICA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="dttab1">
                                                    <h1>Desnutrición Global (<?= count($DESNUTRICIONGLOBAL) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/desnutricion_global_talla', ['DESNUTRICIONGLOBAL' => $DESNUTRICIONGLOBAL]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="dttab2">
                                                    <h1>Desnutrición Aguda (<?= count($DESNUTRICIONAGUDA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/desnutricion_aguda_talla', ['DESNUTRICIONAGUDA' => $DESNUTRICIONAGUDA]);
                                                    ?>
                                                </div>
                                                <div class="tab-pane fade" id="dttab3">
                                                    <h1>No Aplica (<?= count($DTNOAPLICA) ?>)</h1>
                                                    <?php
                                                    echo $this->render('estado_nutricional/desnutricion_talla_no_aplica', ['DTNOAPLICA' => $DTNOAPLICA]);
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        //  yii\helpers\VarDumper::dump($data['listaInfantes'], 10, 0);
                                        // die();
                                        /*  echo '<ul style="list-style-type:square">';
                                          foreach ($data['listaInfantes'] as $key => $value) {
                                              echo "<li  class='lvl1'> <span>" . Yii::$app->Util->labelsEntendiblesMetricas($key) . " </span> "; //PESO;
                                              foreach ($value as $key2 => $value2) {
                                                  echo "<dl style=' padding-left: 5%;' class='lvl2'> <span>" . Yii::$app->Util->labelsEntendiblesMetricas($key2) . "</span>"; //BAJOPESO
                                                  if (count($value2) > 0) {
                                                      foreach ($value2 as $key3 => $value4) {
                                                          $tempInf = backend\modules\individuo\models\Infante::findOne($value4);
                                                          echo $iniLv3Li = "<dt class='lvl3'> <ul> <a target='blank' href='/individuo/infante/view?id=$value4'> $tempInf->nombreCompleto</a>"; //ID
                                                          echo $endLv3Li = "</dt>";
                                                      }
                                                  } else {
                                                      echo "<dt class='lvl3'> <ul> SIN REGISTROS </ul></dt>'";
                                                  }
                                                  echo "</dl>";
                                              }
                                              echo "</li>";
                                          }
                                          echo "</ul>"; */ ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <p class="container">
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'),

                ['/centroinfantil/cibv/'],
                ['class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


        </p>
    </div>
</div>




