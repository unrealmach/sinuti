<?=
\common\components\fixWidget\HighCharts::widget(
    [
        'modules' => [
            'drilldown.js',
        ],
        'clientOptions' => [
            'chart' => [
                'type' => 'pie',
//            'zoomType' => "xy",

            ],

            'title' => [
                'text' => 'Valoración Nutricional Poblacional'
//            'text' => $data['name']
            ],
            'subtitle' => [
                'text' => 'Centro: ' . $cibv . ' para : ' . $dataDays,
//            'text' => $data['subtitle']
            ],
//            'yAxis' => [
//                'allowDecimals' => false,
//                'title' => [
//                    'text' => 'Flats'
//                ]
//            ],
            'plotOptions' => array(
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => [
                        'enabled' => true,
                        'format' => "{point.name}: <b> {point.y:1f} </b>",
//                        'style' => [
//                            'color' => (Highcharts . theme && Highcharts . theme . contrastTextColor) || 'black'
//                        ]
                    ],
                ],
                'series' => array(
                    'borderWidth' => 0,
                    'dataLabels' => array(
                        'enabled' => true,
                    ),
                ),
            ),
            'tooltip' => ['pointFormat' => '<b>{point.y:1f} </b>',
            ],
            'series' => array(array(
//                    'name' => 'MyData',
                'colorByPoint' => true,
                'data' => $data['level1'],
            )),
            'drilldown' => array(
                'series' => $data['level2'],
            ),
        ]
    ]
);

?>