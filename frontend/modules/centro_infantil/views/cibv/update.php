<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cibv',
]) . ' ' . $model->cen_inf_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Centros Infantiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cen_inf_nombre, 'url' => ['view', 'id' => $model->cen_inf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cibv-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
