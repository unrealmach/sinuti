<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */

$this->title = 'Centro Infantil "' . $model->cen_inf_nombre . '"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Centros Infantiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="cibv-view" style="margin: 20px 0;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode($this->title) ?></h1>

        </div>

        <div class="panel-body">

            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>
                    <li><a href="#vtab2" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Coordinador
                            del centro infantil</a></li>
                    <li><a href="#vtab3" role="tab" data-toggle="tab"><i class="
fa fa-address-card pr-10"></i> Miembros del
                            comité central de padres de familia</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <div class="row">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
//                    'cen_inf_id',
                                    [
                                        'label' => 'Parroquia',
                                        'value' => $model->parroquia->parroquia_nombre
                                    ],
                                    'cen_inf_direccion',
                                    'cen_inf_telefono',
                                    'cen_inf_correo',
                                    'cen_inf_activo',
                                ],
                            ])
                            ?>

                            <p class="container">
                                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->cen_inf_id], ['class' => 'btn btn-primary']) ?>
                            </p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="vtab2">

                        <?php if ($rol == 'coordinador') : ?>
                            <?php if (!empty($autoridad_cibv)): ?>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $autoridad_cibv,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'label' => 'Coordinador',
                                            'value' => 'coordinador.nombreCompleto'
                                        ],
                                    ],
                                ]);
                                ?>
                            <?php else : ?>
                                <p>

                                    <?= "Aun no se ha registrado el coordinador al Cibv" ?>
                                </p>
                            <?php endif; ?>

                        <?php elseif ($rol == 'sysadmin' || $rol == 'coordinador-gad') : ?>
                            <?php if (!empty($autoridad_cibv)): ?>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $autoridad_cibv,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        [
                                            'label' => 'Coordinador',
                                            'value' => 'coordinador.nombreCompleto'
                                        ],
                                        [
                                            'class' => \yii\grid\ActionColumn::className(),
                                            'template' => '{update}',
                                            'buttons' => [
                                                'update' => function ($model1, $key) {
                                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['//recursoshumanos/autoridadescibv/update', 'id' => $key['autoridades_cibv_id']], ['title' => 'Actualizar Coordinador']);
                                                }
                                            ]
                                        ]
                                    ],
                                ]);
                                ?>
                            <?php else : ?>
                                <?php
                                $sin_coordinador = true;

                                ?>
                            <?php endif; ?>
                        <?php endif; ?>


                        <p class="container">

                            <?php
                            if (isset($sin_coordinador)) {
                                echo Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Asignar'), ['/recursoshumanos/autoridadescibv/create', 'cen_inf_id' => $model->cen_inf_id], ['class' => 'btn btn-primary']);
                            } ?>

                            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                'onclick' => ' window.history.back();',
                                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']); ?>
                        </p>


                    </div>

                    <div class="tab-pane fade" id="vtab3">
                        <?php
                        if (!empty($comite_central)):
                            ?>
                            <?=
                            GridView::widget([
                                'dataProvider' => $comite_central,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    'nombreCompleto',
                                    'cargo',
                                ],
                            ]);
                            ?>
                            <?=
                            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Update'), ['/centroinfantil/grupocomite/update',
                                'id' => $grupo_comite_id, 'cen_inf_id' => $model->cen_inf_id], ['class' => 'btn btn-primary'])
                            ?>
                        <?php else : ?>
                            <p class="container">
                                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['/centroinfantil/grupocomite/create', 'cen_inf_id' => $model->cen_inf_id], ['class' => 'btn btn-success']) ?>
                            </p>

                        <?php endif; ?>

                    </div>


                </div>


            </div>

        </div>
    </div>
</div>