<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\ubicacion\models\Parroquia;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cibv-form ">

    <?php $form = ActiveForm::begin([
        'options' => [
            // 'class'=>'form-inline'
        ]
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Centro Infantil')) : Html::encode(Yii::t('app', 'Actualizar Centro Infantil')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">

                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'parroquia_id', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])
                        ->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Parroquia::find()->all(), 'parroquia_id', 'parroquia_nombre'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione Parroquía', 'class' => '',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])
                        ->label(null, ['class' => 'col-sm-3 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'cen_inf_nombre', [
                        'template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])

                        ->textInput(['placeholder' => 'Ingrese ...','style' => 'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>

                </div>
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'cen_inf_direccion', [
                        'template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])
                        ->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'cen_inf_telefono', [
                        'template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'cen_inf_correo', [
                        'template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])
                        ->textInput(['placeholder' => 'Ingrese ...'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'cen_inf_activo', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])->dropDownList(['SI' => 'SI', 'NO' => 'NO',])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
            </div>
            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
                </div>


            </div>


        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
