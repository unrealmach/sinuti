<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\settings\models\Po */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="grupo-comite-form section default-bg">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: black !important; text-align:center; width:100%"><?= Html::encode("Comite Central de Padres de Familia") ?></h1>

        </div>
        <!--inicio maestro-->
        <div class="panel-body">
            <div class="row">
                <?=
                $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                    ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd'

                        ],
                        'options' => []
                    ])->label(null, ['class' => 'col-sm-2 control-label'])

                ?>
            </div>
            <div class="row">
                <?=
                $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                    ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                        'options' => []
                    ])->label(null, ['class' => 'col-sm-2 control-label'])

                ?>
            </div>
            <div class="row">
                <?=
                $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Periodo::find()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(null, ['class' => 'col-sm-2 control-label']);

                ?>
            </div>
            <div class="row">

                <?=
                $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map([0 => Cibv::findOne($model->cen_inf_id)], 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccione Cibv', 'class' => 'col-sm-2',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(null, ['class' => 'col-sm-2 control-label']);

                ?>
            </div>

        </div>
        <!--fin maestro-->
        <!--inicio detalle-->
        <div>
            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // indica la clase con la cual se identificara a cada item
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 4, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // indica la clase con la cual se identificara a los botones de insercion
                'deleteButton' => '.remove-item', // indica la clase con la cual se identificara a los botones de borradp
                'model' => $d_model[0],
                'formId' => 'dynamic-form', // id del formulario en donde se crearan los objetos dinamicamente, es el mismo que el form maestro
                'formFields' => [ // los campos que apareceran en el detalle
                    'comite_nombres',
                    'comite_numero_contacto',
                    'comite_dni',
                    'comite_numero_contacto',
                    'cargo'
                ],
            ]);

            ?>

            <div class=""><!-- widgetContainer -->
                <div class=" panel panel-default container-items"><!-- widgetBody -->
                    <!--<table >-->
                    <?php foreach ($d_model as $i => $comite): ?>

                        <div class="panel-heading">
                            <div class="panel-title row ">
                                <div class="col-sm-9 col-xs-9 col-lg-9 ">

                                        <h2 style="color: black !important; text-align:center; width:100%">
                                            <?= Html::encode("Representantes") ?>
                                        </h2>

                                </div>
                                <div class="col-sm-3 col-xs-3 col-lg-3">
                                    <div class="row">
                                        <span type="button" style="min-width: 29%"
                                              class=" col-sm-2 add-item btn radius btn-info btn-sm"><i
                                                    class="glyphicon glyphicon-plus"></i></span>
                                        <span type="button" style="min-width: 29%"
                                              class="col-sm-2 remove-item btn  radius btn-primary btn-sm"><i
                                                    class="glyphicon glyphicon-minus"></i></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--<tr>-->
                        <div class=" item panel-body">
                            <?php
                            // necessary for update action.
                            if (!$comite->isNewRecord) {
                                echo Html::activeHiddenInput($comite, "[{$i}]id");
                            }

                            ?>
                            <div class="row ">  <!-- se indica los campos del detalle para ser creados-->
                                <div class="col-sm-6">
                                    <?=
                                    $form->field($comite, "[{$i}]comite_dni", [
                                            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>']
                                    )
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])

                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <?=
                                    $form->field($comite, "[{$i}]comite_nombres", [
                                        'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                                        ->label(null, ['class' => 'col-sm-2 control-label'])

                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <?=
                                    $form->field($comite, "[{$i}]comite_apellidos", [
                                        'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                                        ->label(null, ['class' => 'col-sm-2 control-label'])

                                    ?>
                                </div>

                                <div class="col-sm-6">
                                    <?=
                                    $form->field($comite, "[{$i}]comite_numero_contacto", [
                                        'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])

                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <?=
                                    $form->field($comite, "[{$i}]cargo", ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->dropDownList(['PRESIDENTE' => 'PRESIDENTE', 'VICEPRESIDENTE' => 'VICEPRESIDENTE', 'TESORERO' => 'TESORERO', 'SECRETARIO' => 'SECRETARIO',], ['style' => 'color:black !important'])->label(null, ['class' => 'col-sm-2 control-label'])

                                    ?>
                                </div>
                            </div><!-- .row -->
                            <div class="separator"></div>
                        </div>
                        <!--</tr>-->

                    <?php endforeach; ?>
                    <!--</table>-->
                </div>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
        <!--inicio detalle-->
    </div>


    <div class="form-group">
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
