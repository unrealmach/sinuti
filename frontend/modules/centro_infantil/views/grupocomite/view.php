<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\GrupoComite */

$this->title = $model->grupo_comite_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comites de Padres de Familia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-comite-view">

    <center><h1><?= Html::encode($model->cenInf->cen_inf_nombre) ?></h1>
        <h2><?= Html::encode("Período: ".$model->periodo->limitesFechasPeriodo)?></h2>
    </center>
    
    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->grupo_comite_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->grupo_comite_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Comite Central Padres de Familia</h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//            'grupo_comite_id',
                    'fecha_inicio_actividad',
                    'fecha_fin_actividad',
//            'periodo_id',
//            'cen_inf_id',
                ],
            ])
            ?>

            <?=
            GridView::widget([
                'dataProvider' => $provider,
                'summary' => "",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'comite_dni',
                        'label' => 'Documento Nacional de Identidad'
                        ],
                    'nombre_completo',
                    ['attribute' => 'comite_numero_contacto',
                        'label' => 'Contacto'
                        ],
                    ['attribute' => 'cargo',
                        'label' => 'Cargo'
                        ],
//                ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
