<?php

use kartik\select2\Select2;
use backend\modules\parametros_sistema\models\Periodo;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="panel panel-default">
    <div class="panel-heading ">
        <div style="text-align:center; width:100%">
            <h2 style="color: black !important;">
                <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Información Básica')) : Html::encode(Yii::t('app', 'Actualizar Información Básica')) ?>
            </h2>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-6">
                <?=
                $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])
                    ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                        'options' => []
                    ])->label(null, ['class' => 'col-sm-5 control-label'])
                ?>
            </div>
            <div class="col-sm-6">
                <?=
                $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])
                    ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                        'inline' => false,
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',

                        ],
                        'options' => []
                    ])->label(null, ['class' => 'col-sm-4 control-label'])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?=
                $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(null, ['class' => 'col-sm-4 control-label']);
                ?>
            </div>
            <div class="col-sm-6">
                <?=
                $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-8">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($listCibv, 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                    'language' => 'es',
                    'options' => ['placeholder' => 'Seleccione Centro Infantil', 'class' => 'col-sm-2',],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(null, ['class' => 'col-sm-4 control-label']);
                ?>
            </div>
        </div>

        <div class="container">

            <div class="form-group">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-right']) . Yii::t('app', 'Next'), [

                    'class' => 'btn btn-default next-step',
                    'id'=>'stepwizard_step1_next']) ?>

            </div>


        </div>

    </div>
</div>