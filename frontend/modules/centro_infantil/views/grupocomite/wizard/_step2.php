<?php

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;

?>

<div class="panel panel-default">
    <div class="panel-heading ">
        <div style="text-align:center; width:100%">
            <h2 style="color: black !important;">
                <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Miembros')) : Html::encode(Yii::t('app', 'Actualizar Miembros')) ?>
            </h2>
        </div>
    </div>
    <div class="panel-body">
        <?php
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 3, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $d_model[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'comite_nombres',
                'comite_numero_contacto',
                'comite_dni',
                'comite_numero_contacto',
                'cargo'
            ],
        ]);
        ?>
        <div class="container-items"><!-- widgetContainer -->

            </button>
            <?php foreach ($d_model as $i => $comite): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <!--<h3 class="panel-title pull-left">Address</h3>-->
                        <div  style="padding-right: 25px;" class="pull-right">
                            <button style="margin-right: 25px" type="button" title="Añadir otro" class="add-item btn btn-success btn-sm"><i class="glyphicon glyphicon-plus"></i>
                            <button title="Quitar miembro" type="button" class="remove-item btn btn-danger btn-sm"><i
                                        class="glyphicon glyphicon-minus" ></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?php
                        // necessary for update action.
                        if (!$comite->isNewRecord) {
//                                var_dump($comite); die();
                            echo Html::activeHiddenInput($comite, "[{$i}]comite_id");
                        }
                        ?>
                        <div class="row ">  <!-- se indica los campos del detalle para ser creados-->
                            <div class="col-sm-4">
                                <?=
                                $form->field($comite, "[{$i}]cargo", ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                                    ->dropDownList(['PRESIDENTE' => 'PRESIDENTE', 'VICEPRESIDENTE' => 'VICEPRESIDENTE', 'TESORERO' => 'TESORERO', 'SECRETARIO' => 'SECRETARIO',], ['style' => 'color:black !important'])
                                    ->label(null, ['class' => 'col-sm-4 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <?=
                                $form->field($comite, "[{$i}]comite_dni", [
                                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>']
                                )
                                    ->textInput(array('placeholder' => 'Ingrese ...',))
                                    ->label(null, ['class' => 'col-sm-4 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <?=
                                $form->field($comite, "[{$i}]comite_nombres", [
                                    'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                                    ->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                                    ->label(null, ['class' => 'col-sm-4 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-4">

                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-sm-4">
                                <?=
                                $form->field($comite, "[{$i}]comite_apellidos", [
                                    'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                                    ->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                                    ->label(null, ['class' => 'col-sm-4 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-4"></div>
                            <div class="col-sm-4">
                                <?=
                                $form->field($comite, "[{$i}]comite_numero_contacto", [
                                    'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                                    ->textInput(array('placeholder' => 'Ingrese ...',))
                                    ->label(null, ['class' => 'col-sm-4 control-label'])
                                ?>
                            </div>

                        </div>


                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
    <div class="container">

        <div class="form-group">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-left']) . Yii::t('app', 'Previous'), [

                'class' => 'btn btn-default prev-step',
                'id'=>'stepwizard_step2_prev']) ?>


            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
        </div>


    </div>


</div>