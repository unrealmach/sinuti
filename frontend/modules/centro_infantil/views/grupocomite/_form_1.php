<?php

use yii\helpers\Html;

use yii\widgets\ActiveForm;


$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorMasterDetail.js', ['depends' => [frontend\assets\AppAsset::className()]]);

/* @var $this yii\web\View */

/* @var $form yii\widgets\ActiveForm */
?>
</br>
<div class="grupo-comite-form" >

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <div class="panel panel-default">
        <div class="panel-heading">

            <h1 style="color: black !important;text-align:center; width:100%">
                <?= $model->isNewRecord ? Html::encode(" Comité Central de Padres de Familia") : Html::encode("Comite Central de Padres de Familia") ?>
            </h1>

        </div>

        <div class="panel-body">
            <?php
            $wizard_config = [
                'id' => 'stepwizard',
                'steps' => [
                    1 => [
                        'title' => 'Información básica',
                        'icon' => 'glyphicon glyphicon-user',
                        'content' => $this->render('wizard/_step1', ['model' => $model, 'form' => $form, 'listCibv' => $listCibv]),
                        'buttons' => [
                            'next' => [
                                'options' => [

                                    'class' => 'disabled',
                                    'hidden' => 'hidden',
                                ]


                            ],
                        ],
                    ],
                    2 => [
                        'title' => 'Miembros del comité',
                        'icon' => 'glyphicon glyphicon-list',
                        'content' => $this->render('wizard/_step2', ['model' => $model, 'd_model' => $d_model, 'form' => $form]),
                        'buttons' => [
                            'save' => [
                                'options' => [
                                    'class' => 'disabled',
                                    'hidden' => 'hidden',
                                ],
                            ],
                            'prev' => ['options' => [
                                'class' => 'disabled',
                                'hidden' => 'hidden',
                            ],
                            ],
                        ],
                    ],

                ],
                'start_step' => 1, // Optional, start with a specific step
            ];

            ?>

            <?= drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
