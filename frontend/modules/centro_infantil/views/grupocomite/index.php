<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\centro_infantil\models\GrupocomiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comites de Padres de Familia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-comite-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= "" 
//        Html::a(Yii::t('app', 'Create', [
//                'modelClass' => 'Comite Central',
//                ]), ['create'], ['class' => 'btn btn-success'])
        ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'grupo_comite_id',
            'fecha_inicio_actividad',
            'fecha_fin_actividad',
            'periodo.limitesFechasPeriodo',
            'cenInf.cen_inf_nombre',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
