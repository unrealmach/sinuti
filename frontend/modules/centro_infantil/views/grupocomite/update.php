<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\GrupoComite */

//$this->title = Yii::t('app', 'Update {modelClass}: ', [
//            'modelClass' => 'Grupo Comite',
//        ]) . ' ' . $model->cenInf->cen_inf_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comites de Padres de Familia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grupo_comite_id, 'url' => ['view', 'id' => $model->grupo_comite_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="grupo-comite-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form_1', [
        'model' => $model,
        'periodo' => $periodo,
        'listCibv' => $listCibv,
        'd_model' => $d_model,
    ])
    ?>

</div>
