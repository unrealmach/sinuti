<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\GrupoComite */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comites de Padres de Familia'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-comite-create">
    </br>
    <?= $this->render('_form_1', [
        'model' => $model,
        'd_model' => $d_model,
        'periodo' => $periodo,
        'listCibv' => $listCibv,
    ]) ?>

</div>
