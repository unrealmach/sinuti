<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\SectorAsignadoInfante */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Sector Asignado Infante',
        ]) . ' ' . $model->sector_asignado_infante_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sector_asignado_infante_id, 'url' => ['view', 'id' => $model->sector_asignado_infante_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sector-asignado-infante-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
        'listInfantes' => $listInfantes,
        'listSectorAsignado' => $listSectorAsignado,
    ])
    ?>

</div>
