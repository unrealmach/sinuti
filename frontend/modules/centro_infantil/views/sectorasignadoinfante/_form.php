<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\SectorAsignadoInfante */
/* @var $form yii\widgets\ActiveForm */
?>
</br>
<div class="sector-asignado-infante-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Crear Sector Asignado Infante')): Html::encode(Yii::t('app', 'Actualizar Sector Asignado Infante')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($listInfantes, 'infante_id', 'nombreCompleto', 'infante_dni'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-2 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'sector_asignado_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($listSectorAsignado, 'sector_asignado_educador_id', 'salon.salon_nombre', 'educador.NombreCOmpleto'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
            </div>
            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
