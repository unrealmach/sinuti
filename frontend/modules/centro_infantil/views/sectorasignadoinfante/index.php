<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\centro_infantil\models\SectorasignadoinfanteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sector Asignado Infantes');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="sector-asignado-infante-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'sector_asignado_infante_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'sector_asignado_id',
                    'value' => 'sectorAsignado.educador.nombreCompleto'],
//                ['attribute' => 'Salon',
//                    'value' => 'sectorAsignado.salon.salon_nombre'],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'educador' || $rol == 'coordinador') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'sector_asignado_infante_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'sector_asignado_id',
                    'value' => 'sectorAsignado.educador.nombreCompleto'],
//                ['attribute' => 'salon',
//                    'value' => 'sectorAsignado.salon.salon_nombre'],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador-gad') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'sector_asignado_infante_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'sector_asignado_id',
                    'value' => 'sectorAsignado.educador.nombreCompleto'],
//                ['attribute' => 'Salon',
//                    'value' => 'sectorAsignado.salon.salon_nombre'],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>

</div>
