<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\SectorAsignadoInfante */

$this->title =  "Sector asignado para el/la infante: " . $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="sector-asignado-infante-view" style="margin: 20px 0;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1><?= Html::encode($this->title) ?></h1>

            </div>
        </div>

        <div class="panel-body">
        <?=
        DetailView::widget([
                'model' => $model,
                'attributes' => [
//                        'sector_asignado_infante_id',
                    [
                        'label' => 'Educador',
                        'value' => $model->sectorAsignado->educador->nombreCompleto
                    ],
                    [
                        'label' => 'Salon Infantil',
                        'value' => $model->sectorAsignado->salon->salon_nombre
                    ],
//            'sector_asignado_id',
                ],
            ])
            ?>
            <p class="container">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->sector_asignado_infante_id], ['class' => 'btn btn-primary']) ?>
            </p>
        </div>
    </div>
</div>
