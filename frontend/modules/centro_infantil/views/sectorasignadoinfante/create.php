<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\SectorAsignadoInfante */

$this->title = Yii::t('app', 'Crear Sector Asignado Infante');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sector-asignado-infante-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
        'listInfantes' => $listInfantes,
        'listSectorAsignado' => $listSectorAsignado,
    ])
    ?>

</div>
