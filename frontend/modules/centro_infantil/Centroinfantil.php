<?php

namespace frontend\modules\centro_infantil;

class Centroinfantil extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\centro_infantil\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
