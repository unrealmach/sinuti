<?php

namespace frontend\modules\metricas_individuo;

class Metricas_individuo extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\metricas_individuo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
