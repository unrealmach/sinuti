<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\metricas_individuo\models\DatoantropometricoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Histórico de datos antropométricos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dato-antropometrico-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    if ($rol == 'coordinador' || $rol == 'coordinador-gad'):
        ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'dat_antro_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'dat_antro_fecha_registro',
                'dat_antro_edad_meses',
                'dat_antro_talla_infante',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ]
            ],
        ]);
        ?>
    <?php elseif ($rol == 'educador') : ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'dat_antro_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'dat_antro_fecha_registro',
                'dat_antro_edad_meses',
                'dat_antro_talla_infante',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ]
            ],
        ]);
        ?>
    <?php elseif ($rol == 'Admin' || $rol == 'sysadmin') : ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'dat_antro_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'dat_antro_fecha_registro',
                'dat_antro_edad_meses',
                'dat_antro_talla_infante',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view} {update} {delete}',
                ]
            ],
        ]);
        ?>
    <?php endif; ?>
</div>
