<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Datos Antropométricos',
]) . ' ' . $model->dat_antro_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datos Antropométricos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => "Datos antropometricos de ". $model->infante->getNombreCompleto(), 'url' => ['view', 'id' => $model->dat_antro_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dato-antropometrico-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
