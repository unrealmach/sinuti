<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */

$this->title = 'Metricas de el/la infante' . $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datos Antropométricos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dato-antropometrico-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>

    <?php if ($rol == 'educador') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->dat_antro_id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//                        'dat_antro_id',
//            'infante_id',
                    'dat_antro_fecha_registro',
                    'dat_antro_edad_meses',
                    'dat_antro_talla_infante',
                    'dat_antro_peso_infante',
                    'dat_antro_imc_infante',
                    'dat_antro_interpretacion_peso',
                    'dat_antro_interpretacion_talla',
                    'dato_antro_desnutricion_peso',
                    'dato_antro_desnutricion_talla',
                ],
            ])
            ?>
        </div>
    </div>
</div>
