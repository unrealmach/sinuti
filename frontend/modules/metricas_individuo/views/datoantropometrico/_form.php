<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\individuo\models\Infante;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="dato-antropometrico-form">

    <?php $form = ActiveForm::begin();

    ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registro Datos Antropométricos')) : Html::encode(Yii::t('app', 'Actualizar Datos Antropométricos')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map([0 => Infante::findOne($model->infante_id)], 'infante_id', 'nombreCompleto', 'infante_dni'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-12',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);

                    ?>
                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'dat_antro_fecha_registro', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(DatePicker::className(), [
                            'options' => ['placeholder' => 'Seleccione fecha'],
                            'readonly' => true,
                            'disabled' => $model->isNewRecord ? false : true,
                            'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'todayHighlight' => true,
                                'startDate' => $model->infante->infantes_fecha_nacimiento,
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'dat_antro_talla_infante', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese talla en centimetros'))
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'dat_antro_peso_infante', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese el peso en kilogramos'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>

            </div>

            <?php
            $form->field($model, 'dat_antro_edad_meses', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array('readonly' => TRUE))
                ->label(null, ['class' => 'col-sm-12 control-label'])

            ?>

            <?php
            $form->field($model, 'dat_antro_imc_infante', [
                'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...', 'style' => 'text-transform: uppercase'])
                ->label(null, ['class' => 'col-sm-3 control-label'])

            ?>

        </div>
    </div>
    <div class="form-group">
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
