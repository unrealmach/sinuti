<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */

$this->title = Yii::t('app', 'Registrar Datos Antropométricos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Datos Antropométricos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dato-antropometrico-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
