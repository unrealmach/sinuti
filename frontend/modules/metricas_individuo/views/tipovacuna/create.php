<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipoVacuna */

$this->title = Yii::t('app', 'Crear Tipo Vacuna');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Vacunas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-vacuna-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
