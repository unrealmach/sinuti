<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipoVacuna */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Vacuna',
]) . ' ' . $model->tipo_vacuna_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Vacunas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tipo_vacuna_id, 'url' => ['view', 'id' => $model->tipo_vacuna_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipo-vacuna-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
