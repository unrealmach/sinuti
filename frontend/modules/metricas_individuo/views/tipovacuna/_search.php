<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipovacunaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-vacuna-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tipo_vacuna_id') ?>

    <?= $form->field($model, 'tipo_vac_enfermedad') ?>

    <?= $form->field($model, 'tipo_vac_nombre') ?>

    <?= $form->field($model, 'tipo_vac_num_dosis') ?>

    <?= $form->field($model, 'tipo_vac_dosis_canti') ?>

    <?php // echo $form->field($model, 'tipo_vac_dosis_medida') ?>

    <?php // echo $form->field($model, 'tipo_vac_via') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
