<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipoVacuna */

$this->title = $model->tipo_vacuna_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Vacunas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-vacuna-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>

    <p>
        <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->tipo_vacuna_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->tipo_vacuna_id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                        'tipo_vacuna_id',
            'tipo_vac_enfermedad',
            'tipo_vac_nombre',
            'tipo_vac_num_dosis',
            'tipo_vac_dosis_canti',
            'tipo_vac_dosis_medida',
            'tipo_vac_via',
            ],
            ]) ?>
        </div>
    </div>
</div>
