<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\metricas_individuo\models\TipovacunaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tipo Vacunas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-vacuna-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <p>
          <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

                    'tipo_vacuna_id',
            'tipo_vac_enfermedad',
            'tipo_vac_nombre',
            'tipo_vac_num_dosis',
            'tipo_vac_dosis_canti',
            // 'tipo_vac_dosis_medida',
            // 'tipo_vac_via',

        ['class' => 'yii\grid\ActionColumn'],
        ],
        ]); ?>
    
</div>
