<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipoVacuna */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-vacuna-form section default-bg">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Crear Tipo Vacuna')) : Html::encode(Yii::t('app', 'Actualizar Tipo Vacuna')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

            <?=
                    $form->field($model, 'tipo_vac_enfermedad', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

            <?=
                    $form->field($model, 'tipo_vac_nombre', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

            <?=
                    $form->field($model, 'tipo_vac_num_dosis', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

            <?=
                    $form->field($model, 'tipo_vac_dosis_canti', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

            <?=
                    $form->field($model, 'tipo_vac_dosis_medida', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->dropDownList([ 'ML' => 'ML', 'GOTAS' => 'GOTAS',])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

            <?=
                    $form->field($model, 'tipo_vac_via', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->dropDownList([ 'I.D.' => 'I.D.', 'V.O.' => 'V.O.', 'IM' => 'IM', 'VS' => 'VS',])
                    ->label(null, ['class' => 'col-sm-3 control-label'])
            ?>

        </div>
    </div>
    <div class="form-group">
        <center>       
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
