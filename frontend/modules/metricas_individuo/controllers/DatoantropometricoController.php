<?php

namespace frontend\modules\metricas_individuo\controllers;

use Yii;
use backend\modules\metricas_individuo\models\DatoAntropometrico;
use backend\modules\metricas_individuo\models\DatoantropometricoSearch;
use backend\modules\metricas_individuo\models\searchs\DatoantropometricoInactivoSearch;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\individuo\models\Infante;

/**
 * DatoantropometricoController implements the CRUD actions for DatoAntropometrico model.
 */
class DatoantropometricoController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DatoAntropometrico models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DatoantropometricoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }
    
    public function actionIndexInactivos() {
        $searchModel = new DatoantropometricoInactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('indexInactivos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single DatoAntropometrico model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new DatoAntropometrico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($infante_id = null) {
        $model = new DatoAntropometrico();
        $edad = 0;
        if ($infante_id != null) {
            $model->infante_id = $infante_id;
//            $infante = Infante::findOne(['infante_id' => $infante_id]);
        }
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo. Por favor comuniquese con el coordinador del GAD');
        }
        if ($model->load(Yii::$app->request->post())) {
            $edad = Yii::$app->Util->calcularEdad($model->infante->infantes_fecha_nacimiento, $model->dat_antro_fecha_registro);
            $model->dat_antro_edad_meses = $edad;
            $validate_metrica = DatoAntropometrico::find()->metricaByEdad($model->dat_antro_edad_meses, $model->infante_id)->one();
            if (!empty($validate_metrica)) {
                throw new NotFoundHttpException('El infante ya tiene los datos antropometricos para el mes actual');
            }
            $imc = $model->dat_antro_peso_infante / pow($model->dat_antro_talla_infante / 100, 2);
            $model->dat_antro_imc_infante = number_format($imc, 2);
            $model->trigger(DatoAntropometrico::INTERPRETACION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_TALLA);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_TALLA);
            if ($model->save()) {
                return $this->redirect(['/individuo/infante/view', 'id' => $model->infante_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DatoAntropometrico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $infante_id = null) {
        $model = $this->findModel($id);
        
        $model->dat_antro_fecha_registro = date_format(new \DateTime($model->dat_antro_fecha_registro), 'Y-m-d');
        if ($infante_id != null) {
            $model->infante_id = $infante_id;
//            $infante = Infante::findOne(['infante_id' => $infante_id]);
        }
        if ($model->load(Yii::$app->request->post())) {
            $imc = $model->dat_antro_peso_infante / pow($model->dat_antro_talla_infante / 100, 2);
            $model->dat_antro_imc_infante = number_format($imc, 2);
            $model->trigger(DatoAntropometrico::INTERPRETACION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_TALLA);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_TALLA);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->dat_antro_id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DatoAntropometrico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DatoAntropometrico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DatoAntropometrico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = DatoAntropometrico::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
