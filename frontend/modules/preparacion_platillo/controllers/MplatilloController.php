<?php

namespace frontend\modules\preparacion_platillo\controllers;

use Yii;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\preparacion_platillo\models\MplatilloSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\preparacion_platillo\models\DPlatillo;
use common\models\recursos\DynamicFormModel;
use backend\modules\preparacion_platillo\models\recursos\PreparacionPlatilloSession;
use \backend\modules\composicion_nutricional\models\ComposicionNutricionalPlatillo;
use yii\data\ActiveDataProvider;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;

/**
 * MplatilloController implements the CRUD actions for MPlatillo model.
 */
class MplatilloController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MPlatillo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MplatilloSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MPlatillo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => DPlatillo::find()->where(['m_platillo_id' => $model->m_platillo_id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'dataChart' => $model->dataToHighchart,
            'cantidad_comensales' => 1,
        ]);
    }

    protected function validateMasterDetailsModels($model, $d_model, $request)
    {

        if ($request->isAjax && $model->load($request->post())) {
            $d_model = DynamicFormModel::createMultiple(DPlatillo::className());
            DynamicFormModel::loadMultiple($d_model, \Yii::$app->request->post());
            $validate = $model->validate();

            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;
            if (!$validate) {


                return ['status' => $validate,
                    'errors' => \Yii::$app->Util->getTxtErrorsFromMasterDetailsModels($model, $d_model)

                ];
            }
        }
        return ['status' => TRUE,
            'errors' => ''

        ];
    }

    /**
     * Creates a new MPlatillo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return mixed
     */
    public function actionCreate($submit = false)
    {

        $model = new MPlatillo();
        $d_model = [new DPlatillo];


        $result = $this->validateMasterDetailsModels($model, $d_model, \Yii::$app->request);

        if (!$result['status']) {

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }


        if ($submit == true && $model->load(Yii::$app->request->post())) {


            //crea el objeto de multiples detalles
            $d_model = DynamicFormModel::createMultiple(DPlatillo::className());
            DynamicFormModel::loadMultiple($d_model, \Yii::$app->request->post());
            $validate = $model->validate();
            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;

            if ($validate) {
                $result = $this->procedureSave($model, $d_model);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return $result;
            } else {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'status' => FALSE,
                    'message' => 'Datos inválidos ',
                ];
            }
        } else if (!Yii::$app->request->isAjax) {
            Yii::$app->session->remove('ingredientesSeleccionados');
            Yii::$app->session->set('preparacionPlatilloSession', new PreparacionPlatilloSession());
            return $this->render('create', [
                'model' => $model,
                'd_model' => (empty($d_model)) ? [new DPlatillo] : $d_model
            ]);
        }
    }

    private function procedureSave($model, $d_model, $deletedIDs = null)
    {

//                Instancia una transacción
        $transaction = \Yii::$app->db->beginTransaction();
        try {

//                    Se valida el padre con todos sus atributos, pero aun no esta guardado en la bd
            if ($flag = $model->save(false)) {

                if (!is_null($deletedIDs) && !empty($deletedIDs)) {
                    DPlatillo::deleteAll(['d_platillo_id' => $deletedIDs]);
                }
                foreach ($d_model as $detalle) {
//                            agrega el id del padre a cada detalle
                    $detalle->m_platillo_id = $model->m_platillo_id;
//                            Valida la consistencia del detalle


                    if (!($flag = $detalle->save(false))) {
//                   Regresa la transaccion a su estado inicial

                        $transaction->rollBack();
                        break;
                    }
                }

            }


            if ($flag) {
                if(!$model->isNewRecord){
                ComposicionNutricionalPlatillo::deleteAll(['m_platillo_id'=>$model->m_platillo_id   ]);
                }

                $modelComposNutriPlat = new ComposicionNutricionalPlatillo();
                $tempSesion = Yii::$app->session->get('preparacionPlatilloSession');
                $dataMaster = $tempSesion->getDataMaestro();
                $data = $modelComposNutriPlat->
                changeTo100g($model->m_platillo_id, $dataMaster['composicionTotal'], $dataMaster['m_platillo_cantidad_total_g']);

                $modelComposNutriPlat->setAttributes($data);
                if (!($flag = $modelComposNutriPlat->save(false))) {
//                 Regresa la transaccion a su estado inicial
                    $transaction->rollBack();
                }


                if($flag){

                    $transaction->commit();
                    Yii::$app->session->remove('ingredientesSeleccionados');

                    return [
                        'status' => TRUE,
                        'errors' => 'Se guardo exitosamente',
                    ];
                }else{
                    return [
                        'status' => FALSE,
                        'errors' => 'Error al guardar la composición nutricional de la receta',
                    ];
                }
//

            } else {


            }
        } catch (\Exception $exc) {
            $transaction->rollBack();

            return [
                'status' => FALSE,
                'errors' => $exc->getMessage() . " " . $exc->getCode()

            ];
        }

    }


    /**
     * Actualiza el modelo de Mplatillo
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param int $id
     * @param boolean $submit
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);
        // instancia todos los detalles del maestro, a traves del geter
        $d_model = $model->dPlatillos;

        //poblar la sesion con lo del platillo


        $result = $this->validateMasterDetailsModels($model, $d_model, \Yii::$app->request);

        if (!$result['status']) {

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }


        if ($submit == true && $model->load(Yii::$app->request->post())) {
            //obtiene los ids de los detalles
            $oldIDs = ArrayHelper::map($d_model, 'd_platillo_id', 'd_platillo_id');


            $new_d_model = DynamicFormModel::createMultiple(DPlatillo::className());
            DynamicFormModel::loadMultiple($new_d_model, \Yii::$app->request->post());

            $valid = $model->validate();
            $valid = DynamicFormModel::validateMultiple($new_d_model) && $valid;

            if ($valid) {

             $result=   $this->procedureSave($model,$new_d_model,$oldIDs);
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             return $result;


            }else {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'status' => FALSE,
                    'message' => 'Datos inválidos ',
                ];
            }


        }
        else if (!Yii::$app->request->isAjax) {
            Yii::$app->session->remove('ingredientesSeleccionados');
            Yii::$app->session->set('preparacionPlatilloSession', new PreparacionPlatilloSession());
            $sesionDetalle = Yii::$app->session->get('preparacionPlatilloSession');
            $sesionDetalle->updateSesion($model);
            //coloca el id del ingrediente en el que usa para el select2 de alimentos
            $modelDetalle = $this->poblarIdentificadoresToUpdate($d_model);

            return $this->render('update', [
                'model' => $model,
                'd_model' => (empty($modelDetalle)) ? [new DPlatillo] : $modelDetalle
            ]);
        }
    }

    /**
     * Agrega el identificador a cada ingrediente que se agregue
     * @param type $modelsPoItem
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public
    function poblarIdentificadoresToUpdate($modelsPoItem)
    {
        foreach ($modelsPoItem as $key => $submodel) {
            $submodel->alimento_identificador = $submodel->alimento_id;
        }
        return $modelsPoItem;
    }

    /**
     * Deletes an existing MPlatillo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public
    function actionDelete($id)
    {
        $CartaModel = \backend\modules\preparacion_carta\models\DPreparacionCarta::find()->where(['m_platillo_id' => $id])->one();
        if (!is_null($CartaModel)&&count($CartaModel) > 0) {
            Yii::$app->getSession()->setFlash('error', 'No se puede eliminar esta receta, se esta usando en las cartas');
        } else {
            $ComposicionPlatillo = ComposicionNutricionalPlatillo::find()->where(['m_platillo_id' => $id])->one();
            $ComposicionPlatillo->delete();
            $detallesPlatillo = DPlatillo::find()->where(['m_platillo_id' => $id])->all();
            foreach ($detallesPlatillo as $key => $detalle) {
                $detalle->delete();
            }
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', 'Eliminación exitosa');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the MPlatillo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MPlatillo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = MPlatillo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Presenta la vista de mixitup
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public
    function actionVer()
    {
        return $this->render('mixitup');
    }

    /**
     * Crea la orden del pedido en formato PDF de acuerdo al mplatillo y la cantifaf
     * @param integer $m_platillo_id
     * @param integer $cantidad
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return mixed
     */
    public
    function actionMakePdfOrdenPedido($m_platillo_id, $cantidad)
    {
        $model = $this->findModel($m_platillo_id);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \backend\modules\preparacion_platillo\models\DPlatillo::find()->where(['m_platillo_id' => $model->m_platillo_id]),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $content = $this->renderPartial('reports/view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'dataChart' => $model->dataToHighchart,
            'cantidad_comensales' => $cantidad,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::DEST_BROWSER,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px} input{ width:100px !important, heigth: 200px !important}; .btn, button{visibility: hidden !important;}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Receta '],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Sinuti Report'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}
