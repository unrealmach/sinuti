<?php

namespace frontend\modules\preparacion_platillo;

class PreparacionPlatillo extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\preparacion_platillo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
