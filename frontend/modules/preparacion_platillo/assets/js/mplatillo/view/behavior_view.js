$(function() {
    $("#btnCalcular").on("click", function() {
        var numComensales = $("#numeroComensales").val()
        $('#table-ingredientes > tbody  > tr > td[data-cantidad_inicial]').each(function() {
            var nuevoValor = ($(this).data("cantidad_inicial") * numComensales)
            $(this).html("" + nuevoValor);
        });
    });
});

/**
 * Agrega los datos para generar la orden de pedido en PDF
 * @author Mauricio Chamorro
 * @returns {undefined}
 */
function enlazarReporte() {
    var m_platillo_id = $("input[name='m_platillo_id']").val();
    var cantidad = $("#numeroComensales").val();
//TODO : var url ="http://localhost/sinuti/frontend/web/index.php?r=preparacion_platillo%2Fmplatillo%2Fmake-pdf-orden-pedido&m_platillo_id="+m_platillo_id+"&cantidad="+cantidad;
    var url ="make-pdf-orden-pedido?m_platillo_id="+m_platillo_id+"&cantidad="+cantidad;
    window.open(url);

}
