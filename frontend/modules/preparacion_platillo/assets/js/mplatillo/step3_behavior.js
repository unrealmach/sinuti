/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ingredientesAll;
$(function() {




$("#btnCalcular").on('click',function(){
    generarResumenPreparacionPlatillo(ingredientesAll,$("#numeroComensales").val());
})



});

/**
 * Genera los ingredientes para el  numero de comensales
 * @param array ingredientes
 * @param integer numeroComensales
 * @author Mauricio Chamorro
 * @returns {undefined}
 */
function generarResumenPreparacionPlatillo(ingredientes,numeroComensales) {
    ingredientesAll = ingredientes;
    var outini = "<table border='1' style='width:100%' class='table table-striped table-bordered' > <tr><th>Ingrediente</th><th>Cantidad  [g]</th> <th>Cantidad Total [g]</th> </tr> ";
    var outbody = "";
    var outfin = " </table>";

    $(ingredientes).each(function(index, ingrediente) {
//      console.log(ingrediente);
        outbody += " <tr> " +
                " <td>" + ingrediente['d_alimento']['alimento_nombre'] + "</td>" +
                " <td>" + ingrediente['d_cantidad_g'] + "</td>"+
                " <td>" + (ingrediente['d_cantidad_g']* parseInt(numeroComensales)).toFixed(2) + "</td>"
        " </tr> ";
    });
    var out = outini + outbody + outfin;

    $("#resumenPrepPlatillo").children().remove();
    $("#resumenPrepPlatillo").append(out);
}

