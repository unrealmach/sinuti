//$(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
//    alert("se inserto algo");
//});
//
//$(".dynamicform_wrapper").on("afterInsert", function(e, item) {
//    console.log("afterInsert");
//});

//$(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
//    if (!confirm("Are you sure you want to delete this item?")) {
//        return false;
//    }
//    return true;
//});

//$(".dynamicform_wrapper").on("afterDelete", function(e) {
//    console.log("Deleted item!");
//});

//$(".dynamicform_wrapper").on("limitReached", function(e, item) {
//    alert("Limit reached");
//});



$(function() {
    var anchoPantalla = $(window).width();
//    var  altoPantalla = $(window).heigth();
// height:100%;
//    width:100%;
    $("#pestaña-desplegable a").click(function() {
        var id = $(this).attr("href").substring(1);
        $("html, body").animate({scrollTop: $("#" + id).offset().top}, 1000, function() {
            $("#pestaña-desplegable").slideReveal("hide");
        });
    });

    var slider = $("#pestaña-desplegable").slideReveal({
        push: false,
        position: "right",
        speed: 700,
        top: 60,
//           overlay:true,
        trigger: $(".handle"),
        width: anchoPantalla / 1.5,
//           heigth:altoPantalla,
        // autoEscape: false,
        shown: function(obj) {
            obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-right"></span>');
            obj.addClass("left-shadow-overlay");
        },
        hidden: function(obj) {
            obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-left"></span>');
            obj.removeClass("left-shadow-overlay");
        }
    });


    $("#pestaña-desplegable").mouseover(function() {
        $(this).css("background-color", "white");
        $(this).css("opacity", "1");
        $(this).css("filter", "alpha(opacity=100)");
    });
    $("#pestaña-desplegable").mousedown(function() {
        $(this).css("background-color", "white");
        $(this).css("opacity", "1");
        $(this).css("filter", "alpha(opacity=100)");
    });

    $("#pestaña-desplegable").mouseout(function() {
        $(this).css("background-color", "#ffffff");
        $(this).css("border", "1px solid black");
        $(this).css("opacity", "0.2");
        $(this).css("filter", "alpha(opacity=20)");
    });

//$( "#contentSlider div" ).data( "highcharts-chart" );

});