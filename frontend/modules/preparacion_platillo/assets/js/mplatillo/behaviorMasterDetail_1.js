/* 
 * @author Mauricio Chamorro
 */
//para almacenar la url para calcular el pie del maestro
var urlDetallestotal;
var das;
//controla el comportamiento del seletc2row cuando se selecciona un valor
function onChangeSelect2(select, url) {
    console.log("cambio del select2");
//    var posicion = $(select).attr('name').match(/\d+/); // obtiene el numero del item para trabajar con su respectiva descripcion y precio
    if ($(select).val() !== "") {
        getDataIngrediente($(select).val(), $(select).attr('name').match(/\d+/), url);
        $('#dplatillo-' + $(select).attr('name').match(/\d+/) + '-alimento_id').val($(select).select2('data')[0]['iden']); //coloca el id del articulo en l hiddentext
    }
    else {
        alert("no se selecciono ningun ingrediente");
    }
}

/*
 * da la informacion del ingrediente seleccionado con select2
 * @author Mauricio Chamorro
 */
function getDataIngrediente(idingrediente, numeroItem, url) {
    console.log("getDataIngrediente")
    var numero = parseInt(numeroItem)
    var cantidad = $("input[id= dplatillo-" + numeroItem + "-d_platillo_cantidad_g").val() !== "" ? $("input[id= dplatillo-" + numeroItem + "-d_platillo_cantidad_g").val() : 0;
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {idingrediente: JSON.stringify(idingrediente),
            posicion: JSON.stringify(numero),
            cantidad: JSON.stringify(cantidad),
        },
        success: function(data) {
            console.log("success ajax getDataIngrediente")
            changeDataTotal();
        }
    });
}

/*
 * para cuando se cambia la cantidad del ingrediente
 * @author Mauricio Chamorro
 */
function onChangeCantidad(object, url) {
    console.log("onChangeCantidad");
//    var number = $(object).attr('name').match(/\d+/); // obtiene el numero del detalle
    cambiarCantidad($(object).attr('name').match(/\d+/), url);
}

/*
* retorna los valores del objeto de sesion con los nuevos cambios
* @author Mauricio Chamorro
*/
function cambiarCantidad(number, url) {
    console.log("cambiarCantidad");
    var ingrediente_id = $("select[id=dplatillo-" + number + "-alimento_identificador").val();
    var cantidad = $("input[id= dplatillo-" + number + "-d_platillo_cantidad_g").val();
//    var descuento = $("input[id=dfactura-" + number + "-producto_descuento]").val() === "" ? 0 : $("input[id=dfactura-" + number + "-producto_descuento]").val();
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {
            posicion: JSON.stringify(number),
            ingrediente_id: JSON.stringify(ingrediente_id),
            cantidad: cantidad,
//            descuento: descuento,
        },
        success: function(data) {
            console.log("success ajax cambiarCantidad");
            changeDataTotal();
        }
    });
}

/*
* permite llamar a la funcion para calcular los datos del maestro a partir de sus detalles
* en el objeto de sesion
* @author Mauricio Chamorro
*/
function changeDataTotal() {
    console.log("changeDataTotal")
    $.ajax({
        beforeSend: function() {
            $("#pestaña-desplegable div span").removeClass('glyphicon-chevron-left ').addClass('fa fa-spinner fa-spin');

        },
        url: urlDetallestotal,
        dataType: 'json',
        method: 'GET',
        success: function(data) {
//            console.log(data);
            generarResumenPreparacionPlatillo(data['detailSesion'], 1);
            var cantidadTotal = data.masterSesion.m_platillo_cantidad_total_g == 0 ? "" : data.masterSesion.m_platillo_cantidad_total_g;
            $("#mplatillo-m_platillo_cantidad_total_g").val(cantidadTotal);
            optionsChart_w0.series[0].data = data.dataHighchart.data;
            optionsChart_w0.xAxis.categories = data.dataHighchart.categories;
            new Highcharts.Chart(optionsChart_w0)
        },
        complete: function() {
            $("#pestaña-desplegable div span").removeClass('fa fa-spinner fa-spin').addClass('glyphicon-chevron-left');
        }
    });
}

/*
* alimenta al objeto de sesion en donde se acumulan los objetos ya seleccionados
* para realizar una busqueda en los articulos exceptuando los preselecionados
* @author Mauricio Chamorro
*/
function ingredienteSeleccionado(url) {
    console.log("ingredienteSeleccionado")
    var itemsEnBandeja = $('select[id$=alimento_identificador]');
//    console.log
    var datosEnviar = [];
    $.each(itemsEnBandeja, function(index, value) {
        var aux = $(value).val()
        datosEnviar.push(aux);
    }) 
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {data: JSON.stringify(datosEnviar),
        },
        success: function(data) {
            console.log("success ajax ingredienteSeleccionado");
        }
    });
}

/*
* se comunica con el objeto de sesion para indicar que se elimino un detalle
* @author Mauricio Chamorro
*/
function ingredienteEliminado(url, object) {

    console.log("ingredienteEliminado");
    var posicion = $(object).parent().siblings('td').first().find('select').attr('id').match(/\d+/)[0];
//    console.log(posicion);
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
//        data: {posicionEliminado: posicion,
        data: {posicionEliminado: JSON.stringify(posicion),
        },
        success: function(data) {
            console.log("success ajax ingredienteEliminado")
            changeDataTotal();
        }
    });

}


/*
* para el evento onkeypress del input cantidad
* no permite que se teclee mas q numeros
* @author Mauricio Chamorro
*/
function validateOnlyNumbers(ingreso) {
    $(ingreso).keydown(function(e)
    {
        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
        // home, end, period, and numpad decimal
        console.log(key);
        return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });

}