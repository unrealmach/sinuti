<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */

$this->title = $model->m_platillo_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mplatillos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/view/behavior_view.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>
<div class="mplatillo-view" >

    <center> 
        <h1 class="center"><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>


    <div class="panel panel-success" id="print_platillo">
        <div class="panel-heading">
            <label> Número de comensales: </label>
            <input id="numeroComensales" class="input-lg "  type="number" value="<?= $cantidad_comensales ?>" min="1" max="2000">
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'm_platillo_id',
                    'tipoReceta.descripcionCompleja',
//            'm_platillo_nombre:ntext',
                    'm_platillo_descripcion:ntext',
                    'm_platillo_cantidad_total_g',
                ],
            ]);

            ?>

            <?= Html::hiddenInput('m_platillo_id', $model->m_platillo_id) ?>

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode("Lista de ingredientes") ?></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-8">
                            <div id ="resumenPrepPlatillo">
                                <table id="table-ingredientes" border='1' style='width:100%' class='table table-striped table-bordered' > 
                                    <tr><th>Ingrediente</th><th>Cantidad  [g]</th><th>Cantidad Total [g]</th></tr> 
                                    <?php
                                    echo ListView::widget([
                                        'dataProvider' => $dataProvider,
                                        'itemView' => '../listViews/_ingrediente',
                                        'viewParams' => ['numComensales' => $cantidad_comensales],
//                                        'itemView' => function ($model, $key, $index, $widget) {
//                                        return "<tr> <td>" . Html::encode($model->alimento->alimento_nombre) . " </td>"
//                                            . " <td> " . Html::encode($model->d_platillo_cantidad_g) . " </td>"
//                                            . " <td data-cantidad_inicial='".$model->d_platillo_cantidad_g."' > " . Html::encode($model->d_platillo_cantidad_g)*$numComensales . " </td> </tr>";
//                                    },
                                    ]);

                                    ?>
                                </table>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Html::encode("Composición nutricional") ?></h3>
                </div>
                <div class="panel-body">
                    <?php

                    ?>
                    <?=
                    \common\components\fixWidget\HighCharts::widget([
                        'clientOptions' => [
                            'chart' => [
                                'type' => 'column',
                                'zoomType' => "xy",
                            ],
                            'title' => [
                                'text' => 'Composición nutricional platillo'
                            ],
                            'subtitle' => [
                                'text' => 'Datos obtenidos de la tabla de alimentos de Ecuador'
                            ],
                            'xAxis' => [
                                'categories' =>
                                $dataChart['categories'],
//                                'type'=>'category',
                                'labels' => [
                                    'rotation' => -45,
                                    'style' => ['fontSize' => '13px',
                                        'fontFamily' => 'Verdana, sans-serif'
                                    ],
                                ],
                            ],
                            'yAxis' => [
                                'min' => 0,
                                'title' => [
                                    'text' => 'Cantidad',
//                                    'align' => 'high'
                                ],
                            ],
                            'legend' => [
                                'enabled' => false,
                            ],
                            'tooltip' => [
                                'pointFormat' => '<b>{point.y:.1f} </b>',
                            ],
                            'series' => [
                                ['name' => $model->m_platillo_nombre, 'data' => $dataChart['serie']],
                            ],
                            'plotOptions' => [
                                'column' => [
                                    'dataLabels' => [
                                        'enabled' => true,
                                        'crop' => false,
                                        'overflow' => 'none'
                                    ]
                                ]
                            ],
//                                'enabled' => true,
//                                'rotation' => -90,
//                                'color' => '#FFFFFF',
//                                'align' => 'right',
//                                'format' => '{point.y:.1f}', // one decimal
//                                'y' => -50, // 10 pixels down from the top
//                                'style' => [
//                                    'fontSize' => '13px',
//                                    'fontFamily' => 'Verdana, sans-serif'
//                                ],
                            'credits' => [
                                'enabled' => false
                            ],
                        ]
                    ]);

                    ?>
                </div>
            </div>


        </div>
    </div>
</div>
