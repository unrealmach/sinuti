<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */

$this->title = 'Receta: "'. $model->m_platillo_nombre.'"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Recetas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/view/behavior_view.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>

</br>

<div class="mplatillo-view" style="margin: 20px 0;">


    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode($this->title) ?></h1>

        </div>
        <div class="panel-body">
            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>
                    <li><a href="#vtab2" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Lista de ingredientes</a></li>
                    <li><a href="#vtab3" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Gráfico de barras</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
//                    'm_platillo_id',
                                'tipoReceta.descripcionCompleja',
//            'm_platillo_nombre:ntext',
                                'm_platillo_descripcion:ntext',
                                'm_platillo_cantidad_total_g',
                            ],
                        ]);

                        ?>

                        <?= Html::hiddenInput('m_platillo_id', $model->m_platillo_id) ?>

                    </div>
                    <div class="tab-pane fade" id="vtab2">

                        <div class="row">
                            <div class="col-sm-8">
                                <div id="resumenPrepPlatillo">
                                    <table id="table-ingredientes" border='1' style='width:100%'
                                           class='table table-striped table-bordered'>
                                        <tr>
                                            <th>Ingrediente</th>
                                            <th>Cantidad [g]</th>
                                            <th>Cantidad Total [g]</th>
                                        </tr>
                                        <?php
                                        echo ListView::widget([
                                            'dataProvider' => $dataProvider,
                                            'itemView' => 'listViews/_ingrediente',
                                            'viewParams' => ['numComensales' => $cantidad_comensales],
//                                        'itemView' => function ($model, $key, $index, $widget) {
//                                        return "<tr> <td>" . Html::encode($model->alimento->alimento_nombre) . " </td>"
//                                            . " <td> " . Html::encode($model->d_platillo_cantidad_g) . " </td>"
//                                            . " <td data-cantidad_inicial='".$model->d_platillo_cantidad_g."' > " . Html::encode($model->d_platillo_cantidad_g)*$numComensales . " </td> </tr>";
//                                    },
                                        ]);

                                        ?>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label> Número de comensales</label>
                                <input id="numeroComensales" type="number" value="<?= $cantidad_comensales ?>" min="1"
                                       max="2000">
                                <button class="btn btn-sm btn-success" id="btnCalcular" type="button" onclick="">
                                    Calcular
                                </button>
                                <button class="btn btn-sm btn-success" id="btnReporte" type="button"
                                        onclick="enlazarReporte()">Orden de pedido
                                </button>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane fade" id="vtab3">

                        <?=
                        \common\components\fixWidget\HighCharts::widget([
                            'clientOptions' => [
                                'chart' => [
                                    'type' => 'column',
                                    'zoomType' => "xy",
                                ],
                                'title' => [
                                    'text' => 'Composición nutricional platillo'
                                ],
                                'subtitle' => [
                                    'text' => 'Datos obtenidos de la tabla de alimentos de Ecuador'
                                ],
                                'xAxis' => [
                                    'categories' =>
                                        $dataChart['categories'],
//                                'type'=>'category',
                                    'labels' => [
                                        'rotation' => -45,
                                        'style' => ['fontSize' => '13px',
                                            'fontFamily' => 'Verdana, sans-serif'
                                        ],
                                    ],
                                ],
                                'yAxis' => [
                                    'min' => 0,
                                    'title' => [
                                        'text' => 'Cantidad',
//                                    'align' => 'high'
                                    ],
                                ],
                                'legend' => [
                                    'enabled' => false,
                                ],
                                'tooltip' => [
                                    'pointFormat' => '<b>{point.y:.1f} </b>',
                                ],
                                'series' => [
                                    ['name' => $model->m_platillo_nombre, 'data' => $dataChart['serie']],
                                ],
                                'plotOptions' => [
                                    'column' => [
                                        'dataLabels' => [
                                            'enabled' => true,
                                            'crop' => false,
                                            'overflow' => 'none'
                                        ]
                                    ]
                                ],
//                                'enabled' => true,
//                                'rotation' => -90,
//                                'color' => '#FFFFFF',
//                                'align' => 'right',
//                                'format' => '{point.y:.1f}', // one decimal
//                                'y' => -50, // 10 pixels down from the top
//                                'style' => [
//                                    'fontSize' => '13px',
//                                    'fontFamily' => 'Verdana, sans-serif'
//                                ],
                                'credits' => [
                                    'enabled' => false
                                ],
                            ]
                        ]);

                        ?>

                    </div>


                </div>


            </div>


            <p class="container">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->m_platillo_id], ['class' => 'btn btn-primary']) ?>


            </p>


        </div>
    </div>
</div>
