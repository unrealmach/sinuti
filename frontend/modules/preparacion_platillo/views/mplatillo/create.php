<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */

$this->title = Yii::t('app', 'Crear Receta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Recetas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mplatillo-create">

    </br>

    <?= $this->render('wizard/_form', [
        'model' => $model,
        'd_model' => $d_model,
    ]) ?>

</div>
