<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mplatillo',
]) . ' ' . $model->m_platillo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recetas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->m_platillo_nombre, 'url' => ['view', 'id' => $model->m_platillo_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mplatillo-update">

   </br>
    <?= $this->render('wizard/_form', [
        'model' => $model,
        'd_model'=>$d_model
    ]) ?>

</div>
