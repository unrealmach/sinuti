<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mplatillo-form section default-bg">




    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?=  Html::encode(Yii::t('app', 'Crear Receta')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

                <?= $form->field($model, 'tipo_receta_id', 
                        ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null,['class'=>'col-sm-3 control-label']) ?>

    <?= $form->field($model, 'com_nut_platillo_id', 
                        ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-3 control-label']) ?>

    <?= $form->field($model, 'm_platillo_nombre')->textarea(['rows' => 6,'style'=>'text-transform: uppercase'])?>

    <?= $form->field($model, 'm_platillo_descripcion')->textarea(['rows' => 6,'style'=>'text-transform: uppercase']) ?>

    <?= $form->field($model, 'm_platillo_cantidad_total_g', [
            'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
            ->label(null,['class'=>'col-sm-3 control-label']) ?>

        </div>
    </div>
    <div class="form-group">
        <center>       
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create') : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
