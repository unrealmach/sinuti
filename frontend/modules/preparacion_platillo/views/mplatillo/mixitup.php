<?php
use frontend\assets\MixitUpAsset;

MixitUpAsset::register($this);
$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behavior_mixitup.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>
<div class="controls">
    <label>Filter:</label>

    <button class="filter" data-filter="all">All</button>
    <button class="filter" data-filter=".category-1">Category 1</button>
    <button class="filter" data-filter=".category-2">Category 2</button>

    <label>Sort:</label>

    <button class="sort" data-sort="myorder:asc">Asc</button>
    <button class="sort" data-sort="myorder:desc">Desc</button>
</div>
<div class="panel-body">
    <div id="Container" class="mix_contanier">
        <div class="mix category-1" data-myorder="1">A</div>
        <div class="mix category-1" data-myorder="2">B</div>
        <div class="mix category-1" data-myorder="3">c</div>
        <div class="mix category-2" data-myorder="4">D</div>
        <div class="mix category-1" data-myorder="5">E</div>
        <div class="mix category-1" data-myorder="6">F</div>
        <div class="mix category-2" data-myorder="7">G</div>
        <div class="mix category-2" data-myorder="8">H</div>
        <div class="mix category-1" data-myorder="1">A1</div>
        <div class="mix category-1" data-myorder="2">B1</div>
        <div class="mix category-1" data-myorder="3">c1</div>
        <div class="mix category-2" data-myorder="4">D1</div>
        <div class="mix category-1" data-myorder="5">E1</div>
        <div class="mix category-1" data-myorder="6">F1</div>
        <div class="mix category-2" data-myorder="7">G1</div>
        <div class="mix category-2" data-myorder="8">H1</div>
        <div class="mix category-1" data-myorder="1">A2</div>
        <div class="mix category-1" data-myorder="2">B2</div>
        <div class="mix category-1" data-myorder="3">c2</div>
        <div class="mix category-2" data-myorder="4">D2</div>
        <div class="mix category-1" data-myorder="5">E2</div>
        <div class="mix category-1" data-myorder="6">F2</div>
        <div class="mix category-2" data-myorder="7">G2</div>
        <div class="mix category-2" data-myorder="8">H2</div>
        <div class="mix category-1" data-myorder="1">A3</div>
        <div class="mix category-1" data-myorder="2">B3</div>
        <div class="mix category-1" data-myorder="3">c3</div>
        <div class="mix category-2" data-myorder="4">D3</div>
        <div class="mix category-1" data-myorder="5">E3</div>
        <div class="mix category-1" data-myorder="6">F3</div>
        <div class="mix category-2" data-myorder="7">G3</div>
        <div class="mix category-2" data-myorder="8">H3</div>

        <div class="gap"></div>
        <div class="gap"></div>
    </div>
</div>
