<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

</br>
<div class="panel">
    <div class="mplatillo-form">
        <div class="panel panel-default">
            <div class="panel-heading">
                <center>
                    <h1 style="color: black !important;">
                        <?=  Html::encode(Yii::t('app', $model->isNewRecord?'Crear Receta':'Actualizar Receta')) ?>
                    </h1>
                </center>
            </div>
            <div class="panel-body">
                <?php
                $form = ActiveForm::begin(['id' => $model->formName(),
                        'enableAjaxValidation' => true,
                        'enableClientScript' => true,
                        'enableClientValidation' => true,
                    'options'=>[
                        'style'=>'margin: 0px !important;'
                    ]
                ]);

                ?>
                <?php
                $wizard_config = [
                    'id' => 'stepwizard',
                    'steps' => [
                        1 => [
                            'title' => 'Información Básica',
                            'icon' => 'glyphicon glyphicon-list ',
                            'content' => $this->render('partials/_step1', ['model' => $model, 'form' => $form]),
                            'buttons' => [
                                'next' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                            ],
                        ],
                        2 => [
                            'title' => 'Ingredientes',
                            'icon' => 'glyphicon glyphicon-grain',
                            'content' => $this->render('partials/_step2', ['model' => $model, 'd_model' => $d_model, 'form' => $form]),
                            'buttons' => [
                                'next' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                                'prev' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                            ],
                        ],
                        3 => [
                            'title' => 'Resumen',
                            'icon' => 'glyphicon glyphicon-list',
                            'content' => $this->render('partials/_step3'),
                            'buttons' => [
                                'prev' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                                'save' => [
//                                    'title' => 'Forward',
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                            ],
                        ],
                    ],
//                    'complete_content' => "You are done!", // Optional final screen
                    'start_step' => 1, // Optional, start with a specific step
                ];

                ?>
                <?= drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!--<div class="form-group">-->
            <!--<center>-->       
        <?=
        ''
//    Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) 

        ?>
        <!--</center>-->
        <!--</div>-->



    </div>

</div>

<div >
    <div id="pestaña-desplegable" title="Ver gráfica estadística">
        <div class="handle">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </div>
        <div id="contentSlider"  style="height:80%; width:100%; overflow: auto">
            <?=
            \common\components\fixWidget\HighCharts::widget([
                'clientOptions' => [
                    'chart' => [
                        'type' => 'bar',
                        'zoomType' => "xy",
                        'renderTo' => "contentSlider"
                    ],
                    'title' => [
                        'text' => 'Composición nutricional platillo'
                    ],
                    'subtitle' => [
                        'text' => 'Datos obtenidos de la tabla de alimentos de Ecuador'
                    ],
                    'xAxis' => [
                        'categories' =>
                        [
//                            'Africa', 'America', 'Asia', 'Europe', 'Oceania'
                        ],
                        'title' => [
                            'text' => null
                        ]
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => 'Cantidad',
                            'align' => 'high'
                        ],
                        'labels' => [
                            'overflow' => 'justify'
                        ]
                    ],
                    'tooltip' => [
//                        'valueSuffix' => ' millions'
                    ],
                    'plotOptions' => [
                        'bar' => [
                            'dataLabels' => [
                                'enabled' => true
                            ]
                        ]
                    ],
                    'legend' => [
                        'layout' => 'horizontal',
//                        'align' => 'right',
//                        'verticalAlign' => 'top',
//                        'x' => -40,
//                        'y' => 80,
//                        'floating' => true,
                        'borderWidth' => 1,
//                        'backgroundColor' => "((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF')",
                        'shadow' => true
                    ],
                    'credits' => [
                        'enabled' => false
                    ],
                    'series' => [
                        [
//                            'name' => ' ',
                            'data' => [0]
                        ]
//                        [
//                            'name' => 'Year 1900',
//                            'data' => [ 133, 156, 947, 408, 6]
//                        ],
//                        [
//                            'name' => 'Year 2012',
//                            'data' => [ 1052, 954, 4250, 740, 38]
//                        ]
                    ]
                ]
            ]);

            ?>
        </div>
    </div>

</div>