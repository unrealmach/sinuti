<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

?>

<!--inicio maestro-->
<div class="panel panel-default">
    <div class="panel-heading">
        <center>
            <h1 style="color: black !important;padding-right: 25px;">
                <?= Html::encode(Yii::t('app', 'Información Básica')) ?>
            </h1>
        </center>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-4">
                <?=
                $form->field($model, 'tipo_receta_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                    ->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(backend\modules\preparacion_platillo\models\TipoReceta::find()->all(), 'tipo_receta_id', 'descripcionCompleja', 'grupoPlatillo.grupo_platillo_nombre'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione el tipo', 'class' => 'col-sm-12',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                ?>

            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'm_platillo_nombre')
                    ->textarea(['rows' => 6,'style'=>'text-transform: uppercase']) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'm_platillo_descripcion')
                    ->textarea(['rows' => 3,'style'=>'text-transform: uppercase']) ?>
            </div>
            <?=
            $form->field($model, 'm_platillo_cantidad_total_g', [
                'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
                ->hiddenInput()->label(FALSE)
            ?>


        </div>

        <div class="container">

            <div class="form-group">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-right']) . Yii::t('app', 'Next'), [

                    'class' => 'btn btn-default next-step',

                ]) ?>

            </div>


        </div>
    </div>
</div>


    <!--fin maestro-->
