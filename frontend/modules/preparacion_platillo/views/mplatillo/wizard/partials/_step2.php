<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use backend\modules\nutricion\models\Alimento;
use backend\modules\nutricion\models\AlimentoSearch;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use frontend\assets\SlideAsset;

SlideAsset::register($this); //para registrar el slider

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorMasterDetail.js', ['depends' => [frontend\assets\AppAsset::className()]]);
$this->registerJsFile($webPath . '/behaviorMasterDetail_1.js', ['depends' => [frontend\assets\AppAsset::className()]]);

$urlAjaxIngredienteSeleccionado = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-ingrediente-seleccionado');
$urlAjaxIngredientesList = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-ingrediente-list');
$urlDetalles = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-generar-data-maestro');
$urlEliminarDetalle = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-eliminar-detalle');
$urlChangeCantidad = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-cambiar-cantidad');
$urlAjaxGetIngrediente = \Yii::$app->urlManager->createUrl('admin/preparacion_platillo/mplatillo/ajax-get-ingrediente');
/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */
/* @var $form yii\widgets\ActiveForm */

?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;padding-right: 25px;">
                    <?= Html::encode(Yii::t('app', 'Ingredientes')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

            <!--inicio detalle-->

            <?php
            DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // indica la clase con la cual se identificara a cada item
                'limit' => 100, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // indica la clase con la cual se identificara a los botones de insercion
                'deleteButton' => '.remove-item', // indica la clase con la cual se identificara a los botones de borradp
                'model' => $d_model[0],
                'formId' => $model->formName(), // id del formulario en donde se crearan los objetos dinamicamente, es el mismo que el form maestro
                'formFields' => [// los campos que apareceran en el detalle
                    'alimento_id',
                    'd_platillo_cantidad_g',
                ],
            ]);

            ?>
            <table border="1 px" class="container-items" style="width: 100%;">

                <tr>
                    <th>
                        <center>  <?= Html::encode("Ingrediente") ?></center>
                    </th>
                    <th style=" width: 20%">
                        <center> <?= Html::encode("Cantidad [g]") ?></center>
                    </th>
                    <th style=" width: 9%">
                        <div>

                            <button
                                    class=" col-sm-2 add-item btn radius btn-info btn-sm"
                                    onclick=" var urlAjaxIngredienteSeleccionado = '<?= $urlAjaxIngredienteSeleccionado ?>';
                                            ingredienteSeleccionado(urlAjaxIngredienteSeleccionado);" type="button"
                                    id="btn_add_item"><i class="glyphicon glyphicon-plus"></i>
                            </button>
                            <!--<button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>-->
                        </div>
                    </th>
                </tr>

                <?php foreach ($d_model as $i => $detalle): ?>

                    <tr class=" item " style="background-color: aliceblue; border: solid 1px black;">
                        <?php
                        // necessary for update action.
                        if (!$detalle->isNewRecord) {
                            echo Html::activeHiddenInput($detalle, "[{$i}]d_platillo_id");
                        }

                        ?>
                        <td style="padding-top: 10px" align="center" valign="middle"
                            onclick=" var urlAjaxIngredienteSeleccionado = '<?= $urlAjaxIngredienteSeleccionado ?>';
                                    ingredienteSeleccionado(urlAjaxIngredienteSeleccionado);"
                        >
                            <?php
                            if (!$detalle->isNewRecord) {
                                $initValueUpdate = $detalle->alimento->alimento_nombre;
                            } else {
                                $initValueUpdate = '';
                            }

                            echo $form->field($detalle, "[{$i}]alimento_identificador")->widget(Select2::classname(), [
                                'initValueText' => $initValueUpdate,
                                'language' => 'es',
//                                        'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Seleccione ingrediente', 'class' => 'col-sm-3',
                                    'onchange' =>
                                        " onChangeSelect2(this,url1);"
                                        . " ingredienteSeleccionado(url2);",
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlAjaxIngredientesList,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(articulo_mercaderia) { return articulo_mercaderia.text; }'),
                                    'templateSelection' => new JsExpression('function (articulo_mercaderia) { return articulo_mercaderia.text; }'),
                                ],
                            ])->label(FALSE);

                            ?>

                            <?= $form->field($detalle, "[{$i}]alimento_id")->hiddenInput()->label(FALSE) ?>
                        </td>
                        <td style="padding-top: 10px" align="center" valign="middle">
                            <?=
                            $form->field($detalle, "[{$i}]d_platillo_cantidad_g", [
                                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>',
                            ])
                                ->input('number', ['readonly' => false, 'onKeyPress' => 'validateOnlyNumbers(this)',
                                    'onChange' =>
                                        " onChangeCantidad(this,url);"
                                    , 'min' => 0.01, 'step' => 0.01,])->label(FALSE)

                            ?>
                        </td>
                        <td style="padding-top: 10px;">
                            <button type="button"
                                    onclick="
                                            var url2 = '<?= $urlAjaxIngredienteSeleccionado ?>';
                                            var url3 = '<?= $urlEliminarDetalle ?>';
                                            ingredienteSeleccionado(url2);
                                            ingredienteEliminado(url3, this);

                                            "
                                    class="col-sm-2 remove-item btn  radius btn-primary btn-sm"
                            ><i class="glyphicon glyphicon-minus"></i>
                            </button>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
            <?php DynamicFormWidget::end(); ?>

            <!--fin detalle-->


            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-left']) . Yii::t('app', 'Previous'), [

                        'class' => 'btn btn-default prev-step',
                        'id' => 'stepwizard_step2_prev']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-md btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;", 'id' => 'btn_submit_dynamicForm']) ?>
                </div>


            </div>
        </div>
    </div>
<?php

$parametrosExtras = $model->isNewRecord ? '?submit=true' : '&submit=true';

$script = <<< JS
   $(function(){
   urlDetallestotal= '{$urlDetalles}';
   url1= '{$urlAjaxGetIngrediente}';
   url2= '{$urlAjaxIngredienteSeleccionado}';
    url= '{$urlChangeCantidad}';
   });
   
//llama al formulario y mediante ajax antes de enviar realiza la validacion
    
    $("form#{$model->formName()} button[type='submit']").on('click', function(e){
            var r = confirm("¿Esta seguro que desea guardar esta receta?");
           if (r == true) {
                 var \$form= $("form#{$model->formName()}");
           $.ajax({
                beforeSend:function(){
                    $(\$form).submit();
                    showModalLoading();
                },
                type:"POST",
                url:\$form.attr("action")+"{$parametrosExtras}",  //envia la accion del formulario osea el action
                data:\$form.serialize(), //envia el formulario serializado 
                 success:function(result){
//        alert(\$form.attr("action")+"?submit=true");
                    if(result.status==true){
    
                        $("#_step2").parent().removeClass('active').addClass('disabled');
                        $("#step2").removeClass('active').addClass('disabled');
                        $("#_step1").parent().removeClass('active').addClass('disabled');
                        $("#step1").parent().removeClass('active').addClass('disabled');
                        
                         
                        
                        $("[aria-controls='step1'").parent().removeClass('active').addClass('disabled');

                        $("[aria-controls='step2'").parent().removeClass('active').addClass('disabled');
                        
                        $("[aria-controls='step3'").parent().removeClass('disabled').addClass('active');
                        $("#_step3").parent().addClass('active');

                        $("#step3").addClass('active');
                        $("#_step3").parent().trigger('click');
                        showFlashAlert(result.errors)
                        closeModal();
                    }else{
                        showFlashAlert(result.errors);
                        closeModal();
                        }
                 }})
                
            
                 
           } else {
               return false;
           }
    
          
    return false;
});
    
JS;
$this->registerJs($script);


