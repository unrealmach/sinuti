<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/step3_behavior.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>


<!--inicio step3-->
<div class="panel panel-default">
    <div class="panel-heading">
        <center>
            <h1 style="color: black !important;padding-right: 25px;">
                <?= Html::encode(Yii::t('app', 'Calculadora de Porciones')) ?>
            </h1>
        </center>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-8">
                <div id="resumenPrepPlatillo">

                </div>
            </div>
            <div class="col-sm-4">
                <label> Número de comensales</label>
                <input id="numeroComensales" type="number" value="1" min="1" max="2000">
                <button id="btnCalcular" type="button" onclick="">Calcular</button>
            </div>
        </div>

        <div class="container">

            <div class="form-group">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
            </div>


        </div>
    </div>
</div>
<!--fin step3-->