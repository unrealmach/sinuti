<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>

<tr>
    <td> <?= Html::encode($model->alimento->alimento_nombre) ?> </td>
    <td> <?= Html::encode($model->d_platillo_cantidad_g) ?> </td>
    <td data-cantidad_inicial='<?= $model->d_platillo_cantidad_g ?>'><?= Html::encode($model->d_platillo_cantidad_g*$numComensales) ?>  </td> 
</tr>
