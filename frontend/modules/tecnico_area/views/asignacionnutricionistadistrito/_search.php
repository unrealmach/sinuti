<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionnutricionistadistritoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignacion-nutricionista-distrito-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'asignacion_nutricionista_distrito_id') ?>

    <?= $form->field($model, 'nutricionista_id') ?>

    <?= $form->field($model, 'distrito_id') ?>

    <?= $form->field($model, 'fecha_inicio_actividad') ?>

    <?= $form->field($model, 'fecha_fin_actividad') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
