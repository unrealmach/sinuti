<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito */

$this->title = Yii::t('app', 'Crear Asignacion Nutricionista Distrito');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Nutricionista Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-nutricionista-distrito-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
    ]) ?>

</div>
