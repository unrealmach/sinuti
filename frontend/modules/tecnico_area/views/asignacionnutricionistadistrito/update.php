<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asignacion Nutricionista Distrito',
]) . ' ' . $model->asignacion_nutricionista_distrito_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Nutricionista Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asignacion_nutricionista_distrito_id, 'url' => ['view', 'id' => $model->asignacion_nutricionista_distrito_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-nutricionista-distrito-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
    ]) ?>

</div>
