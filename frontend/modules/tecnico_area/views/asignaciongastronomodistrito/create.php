<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionGastronomoDistrito */

$this->title = Yii::t('app', 'Crear Asignacion Gastronomo Distrito');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Gastronomo Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-gastronomo-distrito-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
    ])
    ?>

</div>
