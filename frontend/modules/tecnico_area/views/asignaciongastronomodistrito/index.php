<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tecnico_area\models\AsignaciongastronomodistritoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignacion Gastronomo Distritos');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="asignacion-gastronomo-distrito-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'asignacion_gastronomo_distrito_id',
            'gastronomo.nombreCompleto',
            'distrito.distrito_descripcion',
            'fecha_inicio_actividad',
            'fecha_fin_actividad',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
