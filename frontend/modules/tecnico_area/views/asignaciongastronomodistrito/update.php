<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionGastronomoDistrito */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Asignacion Gastronomo Distrito',
        ]) . ' ' . $model->asignacion_gastronomo_distrito_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Gastronomo Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asignacion_gastronomo_distrito_id, 'url' => ['view', 'id' => $model->asignacion_gastronomo_distrito_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-gastronomo-distrito-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
    ])
    ?>

</div>
