<?php
namespace frontend\modules\tecnico_area\controllers;

use Yii;
use backend\modules\tecnico_area\models\AsignacionGastronomoDistrito;
use backend\modules\tecnico_area\models\AsignaciongastronomodistritoSearch;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AsignaciongastronomodistritoController implements the CRUD actions for AsignacionGastronomoDistrito model.
 */
class AsignaciongastronomodistritoController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AsignacionGastronomoDistrito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AsignaciongastronomodistritoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AsignacionGastronomoDistrito model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AsignacionGastronomoDistrito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AsignacionGastronomoDistrito();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo');
        }
        if ($model->load(Yii::$app->request->post())) {

            //TODO
//            validar gastronomo distrito para el periodo actual
            if ($model->save()) {
                $gastronomo = \backend\modules\recursos_humanos\models\Gastronomo::findOne([$model->gastronomo_id]);
                $user = \webvimark\modules\UserManagement\models\User::findOne(['email' => $gastronomo->gastronomo_correo]);
                if (!empty($user)) {
                    \backend\modules\tecnico_area\models\AsignacionUserGastronomoDistrito::insertRegistro($user->id, $model->asignacion_gastronomo_distrito_id);
                } else {
                    $model->delete();
                    throw new NotFoundHttpException('No coinciden los emails del usuario y rrhh');
                }
//                
                return $this->redirect(['view', 'id' => $model->asignacion_gastronomo_distrito_id]);
            }
        } else {
            return $this->render('create', [
                    'model' => $model,
                    'periodo' => $periodo
            ]);
        }
    }

    /**
     * Updates an existing AsignacionGastronomoDistrito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if(empty($periodo)){
            throw new NotFoundHttpException('No existe un periodo activo');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->asignacion_gastronomo_distrito_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
                    'periodo' => $periodo,
            ]);
        }
    }

    /**
     * Deletes an existing AsignacionGastronomoDistrito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AsignacionGastronomoDistrito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AsignacionGastronomoDistrito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AsignacionGastronomoDistrito::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
