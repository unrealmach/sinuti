<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\parametros_sistema\models\Periodo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Periodo',
]) . ' ' . $model->periodo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Periodos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->periodo_id, 'url' => ['view', 'id' => $model->periodo_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="periodo-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
