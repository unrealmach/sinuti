<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\parametros_sistema\models\Periodo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="periodo-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Crear Periodo')) : Html::encode(Yii::t('app', 'Actualizar Período')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'fecha_inicio', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'fecha_fin', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'estado', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->dropDownList(['ACTIVO' => 'ACTIVO', 'INACTIVO' => 'INACTIVO',])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
            </div>
            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>


            </div>
        </div>

    </div>




    <?php ActiveForm::end(); ?>

</div>
