<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\parametros_sistema\models\Periodo */

$this->title = Yii::t('app', 'Crear Periodo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Periodos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periodo-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
