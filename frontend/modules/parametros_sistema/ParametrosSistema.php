<?php

namespace frontend\modules\parametros_sistema;

class ParametrosSistema extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\parametros_sistema\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
