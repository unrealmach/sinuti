<?php

namespace frontend\modules\individuo;

class Individuo extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\individuo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
