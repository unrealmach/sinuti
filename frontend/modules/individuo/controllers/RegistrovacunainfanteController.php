<?php

namespace frontend\modules\individuo\controllers;

use Yii;
use backend\modules\individuo\models\RegistroVacunaInfante;
use backend\modules\individuo\models\RegistrovacunainfanteSearch;
use backend\modules\individuo\models\searchs\RegistrovacunainfanteInactivoSearch;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegistrovacunainfanteController implements the CRUD actions for RegistroVacunaInfante model.
 */
class RegistrovacunainfanteController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegistroVacunaInfante models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new RegistrovacunainfanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }
    /**
     * Lists all RegistroVacunaInfante models.
     * @return mixed
     */
    public function actionIndexInactivos() {
        $searchModel = new RegistrovacunainfanteInactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('indexInactivos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single RegistroVacunaInfante model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new RegistroVacunaInfante model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($infante_id = null) {
        $model = new RegistroVacunaInfante();
        if ($infante_id != null) {
            $model->infante_id = $infante_id;
        }
         $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo. Por favor comuniquese con el coordinador del GAD');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/individuo/infante/view', 'id' => $model->infante_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RegistroVacunaInfante model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $infante_id = null) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/individuo/infante/view', 'id' => $model->infante_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RegistroVacunaInfante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RegistroVacunaInfante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RegistroVacunaInfante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = RegistroVacunaInfante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
