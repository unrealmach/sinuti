<?php

namespace frontend\modules\individuo\controllers;

use Yii;
use backend\modules\individuo\models\Infante;
use backend\modules\individuo\models\InfanteSearch;
use backend\modules\individuo\models\searchs\InfanteInactivoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\metricas_individuo\models\DatoAntropometrico;
use backend\modules\asistencia\models\Asistencia;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\individuo\models\RegistroVacunaInfante;
use backend\modules\individuo\models\Patologia;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\parametros_sistema\models\Periodo;
use common\models\recursos\DynamicFormModel;

/**
 * InfanteController implements the CRUD actions for Infante model.
 */
class InfanteController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Infante models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InfanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
            'user_id' => Yii::$app->user->id])->one();

//        print_r($dataProvider->query->createCommand()->getRawSql());
//        die();
        return $this->render('index',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'rol' => $authAsiignmentMdel->item_name,
            ]);
    }

    /**
     * Lists all Infante models.
     * @return mixed
     */
    public function actionIndexInactivos()
    {
        $searchModel = new InfanteInactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
            'user_id' => Yii::$app->user->id])->one();

        return $this->render('indexInactivos',
            [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'rol' => $authAsiignmentMdel->item_name,
            ]);
    }

    /**
     * Displays a single Infante model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $ninio = Infante::findOne((int)$id);
        $inicio = $ninio->infantes_fecha_nacimiento;
        $id = $ninio->infante_id;
        $reportes = new DatoAntropometrico();
        $reportePeso = $reportes->generarReport($inicio, $id, "peso");
        $reporteTalla = $reportes->generarReport($inicio, $id, "talla");
        $reporteImc = $reportes->generarReport($inicio, $id, "imc");

        $matricula = Matricula::find()->where(['infante_id' => $id, 'matricula_estado' => 'ACTIVO']);
        //Todo validar sector actual infante por periodo o algo asi
        $sectorInfante = SectorAsignadoInfante::find()->where(['infante_id' => $id]);
        $patologia = Patologia::find()->where(['infante_id' => $id]);
        $registro_vacunas = RegistroVacunaInfante::find()->where(['infante_id' => $id]);
        $authAsignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where([
            'user_id' => Yii::$app->user->id])->one();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $matricula,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $dataProvider2 = new \yii\data\ActiveDataProvider([
            'query' => $patologia,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        $dataProvider3 = new \yii\data\ActiveDataProvider([
            'query' => $registro_vacunas,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);
        $dataProvider4 = new \yii\data\ActiveDataProvider([
            'query' => $sectorInfante,
            'pagination' => [
                'pageSize' => 20,
            ]
        ]);

        return $this->render('view',
            [
                'model' => $this->findModel($id),
                'reportePeso' => $reportePeso,
                'reporteTalla' => $reporteTalla,
                'reporteImc' => $reporteImc,
                'matricula' => $dataProvider,
                'patologia' => $dataProvider2,
                'sectorInfante' => $dataProvider4,
                'registro_vacunas' => $dataProvider3,
                'rol' => $authAsignmentModel->item_name,
            ]);
    }

    /**
     * Finds the Infante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Infante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Infante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Infante model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new Infante();
        $periodo = Periodo::find()->periodoActivo()->one();

        $result = $this->validateMasterDetailsModels($model, \Yii::$app->request);

        if (!$result['status']) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }

        if ($submit && Yii::$app->request->isAjax) {

            $transaction = \Yii::$app->db->beginTransaction();
            try {


                if (empty($periodo)) {
                    throw new NotFoundHttpException('Aun no existe un periodo activo. Por favor comuniquese con el coordinador del GAD');
                }
                $authAsignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                    'user_id' => Yii::$app->user->id])->one();
                if ($authAsignmentModel->item_name == 'coordinador') {
                    $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();

                    if (empty($asignacionCoordinadorCibv)) {
                        throw new NotFoundHttpException('No existe un coordinador para el Centro Infantil en el periodo Actual');
                    }
                }
                if ($authAsignmentModel->item_name == 'educador') {
                    $asignacionEducador = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
                    if (empty($asignacionEducador)) {
                        throw new NotFoundHttpException('No existe un salón asignado para el educador en el periodo vigente');
                    }
                }


                if (!($flag = $model->save(false))) {
                    $transaction->rollBack();
                    return [
                        'status' => FALSE,
                        'errors' => "Error en los datos, si persiste comuniquese con el administrador"
                    ];
                }


                $resultFromMatricula = Infante::generarMatricula($model->infante_id);

                if (!($flag = $flag && $resultFromMatricula['status'])) {
                    $transaction->rollBack();
                    return $resultFromMatricula;
                }


                if ($authAsignmentModel->item_name == 'educador') {
                    $resultFromAsignacion = Infante::asignarSectorAsignadoInfante($model->infante_id);
                    if (!($flag = $flag && $resultFromAsignacion['status'])) {
                        $transaction->rollBack();
                        return $resultFromAsignacion;
                    }
                }


                if ($flag) {
                    $msg='Datos ingresados correctamente, se ha generado la matrícula automática.';
                    $transaction->commit();

                    if ($authAsignmentModel->item_name == 'educador') {
                        $msg= $msg." "." Se ha asignado el infante al salón ";
                    }
                    Yii::$app->getSession()->setFlash('success', $msg);
                    return $this->redirect(['view', 'id' => $model->infante_id]);

                } else {
                    $transaction->rollBack();

                }



            } catch (\Exception $exc) {
                $transaction->rollBack();

                return [
                    'status' => FALSE,
                    'errors' => $exc->getMessage() . " " . $exc->getCode()

                ];
            }
        } else {
            // die("qqqq");
            return $this->render('create',
               ['model' => $model,]);
        }
    }

    protected function validateMasterDetailsModels($model, $request)
    {
        if ($request->isAjax && $model->load($request->post())) {
            $validate = $model->validate();
            if (!$validate) {
                return ['status' => $validate,
                    'errors' => \Yii::$app->Util->getTxtErrorsFromMasterDetailsModels($model, null)
                ];
            }
        }
        return ['status' => TRUE,
            'errors' => ''
        ];
    }

    /**
     * Updates an existing Infante model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);

        $result = $this->validateMasterDetailsModels($model, \Yii::$app->request);

        if (!$result['status']) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }

        if ($submit) {

            $transaction = \Yii::$app->db->beginTransaction();
            try {

                if (!($flag = $model->save(false))) {
                    $transaction->rollBack();
                }

                if ($flag) {
                    $transaction->commit();
                    Yii::$app->getSession()->setFlash('success', 'Actualizado correctamente');
                    return $this->redirect(['view', 'id' => $model->infante_id]);

                } else {
                    $transaction->rollBack();

                }


            } catch (\Exception $exc) {
                $transaction->rollBack();

                return [
                    'status' => FALSE,
                    'errors' => $exc->getMessage() . " " . $exc->getCode()

                ];
            }
        } else if (!Yii::$app->request->isAjax) {

            return $this->render('update',
                ['model' => $model,]);
        }

    }

    /**
     * Deletes an existing Infante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
}