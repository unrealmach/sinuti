<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\individuo\models\RegistrovacunainfanteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Registro de vacunas  de infantes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-vacuna-infante-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= '' // Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php if ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/registrovacunainfante/index-inactivos'], ['class' => 'btn btn-info']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'reg_vac_inf_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'tipo_vacuna_id',
                    'value' => 'tipoVacuna.tipo_vac_nombre'],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador' || $rol == 'coordinador-gad') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/registrovacunainfante/index-inactivos'], ['class' => 'btn btn-info']) ?>
        </p>       
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'reg_vac_inf_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'tipo_vacuna_id',
                    'value' => 'tipoVacuna.tipo_vac_nombre'],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'educador') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'reg_vac_inf_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                ['attribute' => 'tipo_vacuna_id',
                    'value' => 'tipoVacuna.tipo_vac_nombre'],
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view} {update}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>
</div>
