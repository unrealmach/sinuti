<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\RegistrovacunainfanteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="registro-vacuna-infante-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'reg_vac_inf_id') ?>

    <?= $form->field($model, 'infante_id') ?>

    <?= $form->field($model, 'tipo_vacuna_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
