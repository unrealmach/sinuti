<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\RegistroVacunaInfante */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Registro Vacuna Infante',
]) . ' ' . $model->reg_vac_inf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registro Vacuna Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->reg_vac_inf_id, 'url' => ['view', 'id' => $model->reg_vac_inf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="registro-vacuna-infante-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
