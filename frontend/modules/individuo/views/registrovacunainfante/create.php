<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\RegistroVacunaInfante */

$this->title = Yii::t('app', 'Crear Registro Vacuna Infante');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registro Vacuna Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-vacuna-infante-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
