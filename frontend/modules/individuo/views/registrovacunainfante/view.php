<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\RegistroVacunaInfante */

$this->title = 'Esquema de vacunas de el/la infante' . $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registro Vacuna Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-vacuna-infante-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>

    <?php if ($rol == 'educador') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->reg_vac_inf_id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'reg_vac_inf_id',
                    [ 'label' => 'Vacuna',
                        'value' => $model->tipoVacuna->tipo_vac_nombre],
                    [ 'label' => 'Enfermedad',
                        'value' => $model->tipoVacuna->tipo_vac_enfermedad],
                    [ 'label' => 'Dosis',
                        'value' => $model->tipoVacuna->tipo_vac_dosis_canti],
                    [ 'label' => 'Medida',
                        'value' => $model->tipoVacuna->tipo_vac_dosis_medida],
                    [ 'label' => 'Medida',
                        'value' => $model->tipoVacuna->tipo_vac_via],
                    'tipo_vac_fecha_aplicacion',
                ],
            ])
            ?>
        </div>
    </div>
</div>
