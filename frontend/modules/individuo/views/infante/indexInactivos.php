<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\individuo\models\InfanteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('app', 'Histórico de infantes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infante-index">
    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    if ($rol == 'coordinador' || $rol == "educador" || $rol == 'coordinador-gad'):
        ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                    ['attribute' => 'nombre',
                    'value' => function($model) {
                        return $model->infante_nombres." ".$model->infante_apellidos;
                    }
                ],
                'infante_nacionalidad',
                    [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                    ['attribute' => 'nombre',
                    'value' => function($model) {
                        return $model->infante_nombres." ".$model->infante_apellidos;
                    }
                ],
                'infante_nacionalidad',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'nutricion') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                    ['attribute' => 'nombre',
                    'value' => function($model) {
                        return $model->infante_nombres." ".$model->infante_apellidos;
                    }
                ],
                'infante_nacionalidad',
                    [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
<?php endif; ?>


</div>
