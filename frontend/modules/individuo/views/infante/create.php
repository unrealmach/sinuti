<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */

$this->title = Yii::t('app', 'Registrar Infante');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="infante-create">

    <br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
