<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\individuo\models\InfanteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lista de Infantes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infante-index">
    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>


    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    if ($rol == 'coordinador'):
        ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Crear Infante'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/infante/index-inactivos'], ['class' => 'btn btn-info']) ?>
        </p>
        <?php


     echo   GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'infante_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->infante_nombres . " " . $model->infante_apellidos;
            }
                ],
                'infante_nacionalidad',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view} {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'Admin' || $rol == 'sysadmin') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/infante/index-inactivos'], ['class' => 'btn btn-info']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->infante_nombres . " " . $model->infante_apellidos;
            }
                ],
                'infante_nacionalidad',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador-gad') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/infante/index-inactivos'], ['class' => 'btn btn-info']) ?>

        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->infante_nombres . " " . $model->infante_apellidos;
            }
                ],
                'infante_nacionalidad',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'educador') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Crear Infante'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->infante_nombres . " " . $model->infante_apellidos;
            }
                ],
                'infante_nacionalidad',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view} {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'nutricion') : ?>
       <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/individuo/infante/index-inactivos'], ['class' => 'btn btn-info']) ?>

        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'infante_id',
                'infante_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->infante_nombres . " " . $model->infante_apellidos;
            }
                ],
                'infante_nacionalidad',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>


</div>
