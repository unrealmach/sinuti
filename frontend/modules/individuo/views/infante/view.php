<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */

$this->title = $model->infante_nombres . " " . $model->infante_apellidos;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
</br>
<div class="infante-view">

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1><?= Html::encode('Infante: "'.$this->title.'"') ?></h1>

            </center>
        </div>
        <div class="panel-body">


            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active">
                        <a href="#vtab1" role="tab" data-toggle="tab"><i class=""></i>
                            Información básica</a>
                    </li>
                    <li>
                        <a href="#vtab2" role="tab" data-toggle="tab"><i class="	fa fa-book pr-10"></i>
                            Matrícula</a>
                    </li>
                    <li>
                        <a href="#vtab3" role="tab" data-toggle="tab"><i class="	fa fa-book pr-10"></i>
                            Sector Asignado</a>
                    </li>
                    <li>
                        <a href="#vtab4" role="tab" data-toggle="tab"><i class="	fa fa-book pr-10"></i>
                            Hábitos de alimentación</a>
                    </li>
                    <li>
                        <a href="#vtab5" role="tab" data-toggle="tab"><i class="	fa fa-book pr-10"></i>
                            Vacunas</a>
                    </li>
                    <li>
                        <a href="#vtab6" role="tab" data-toggle="tab"><i class="	fa fa-book pr-10"></i>
                            Datos Antropométricos</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
//            'infante_id',
                                'infante_dni',
//            'infante_nombres',
//            'infante_apellidos',
                                'infante_nacionalidad',
                                'infante_genero',
                                'infante_etnia',
                                'infantes_fecha_nacimiento',
                                'infante_represent_documento',
                                'infante_represent_nombres',
                                'infante_represent_apellidos',
                                'infante_represent_parentesco',
                                'infante_direccion_domiciliaria',
                                'infante_represent_telefono',
                                'infante_represent_correo',
                            ],
                        ])


                        ?>
                        <p class="container">
                            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                'onclick' => ' window.history.back();',
                                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->infante_id], ['class' => 'btn btn-primary']) ?>
                        </p>

                    </div>
                    <div class="tab-pane fade " id="vtab2">
                        <?=
                        $this->render(
                            'historico/_matriculas.php', [
                                'matricula' => $matricula,
                                'model' => $model,
                                'rol' => $rol
                            ]
                        )
                        ?>
                    </div>
                    <div class="tab-pane fade " id="vtab3">
                        <?=
                        $this->render(
                            'historico/_sectorInfante.php', [
                                'sectorInfante' => $sectorInfante,
                                'model' => $model,
                                'rol' => $rol
                            ]
                        )
                        ?>
                    </div>
                    <div class="tab-pane fade " id="vtab4">
                        <?=
                        $this->render(
                            'historico/_patologias.php', [
                                'patologia' => $patologia,
                                'model' => $model,
                                'rol' => $rol
                            ]
                        )
                        ?>
                    </div>
                    <div class="tab-pane fade " id="vtab5">
                        <?=
                        $this->render(
                            'historico/_vacunas.php', [
                                'registro_vacunas' => $registro_vacunas,
                                'model' => $model,
                                'rol' => $rol
                            ]
                        )
                        ?>
                    </div>
                    <div class="tab-pane fade " id="vtab6">
                        <?=
                        $this->render(
                            'metricas/_datosantropometricos.php', [
                                'model' => $model,
                                'reportePeso' => $reportePeso,
                                'reporteTalla' => $reporteTalla,
                                'reporteImc' => $reporteImc,
                                'rol' => $rol
                            ]
                        )
                        ?>
                    </div>

                </div>


            </div>




        </div>


    </div>
</div>