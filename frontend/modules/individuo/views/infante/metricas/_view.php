<div class="horizontal">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active">
            <a href="#rtab1" role="tab" data-toggle="tab"><i class="fa fa-balance-scale"></i>
                Seguimiento del peso</a>
        </li>
        <li>
            <a href="#rtab2" role="tab" data-toggle="tab"><i class="fa fa-heartbeat"></i>
                Seguimiento de la talla</a>
        </li>
        <li>
            <a href="#rtab3" role="tab" data-toggle="tab"><i class="fa fa-calculator"></i>
                Seguimiento del IMC</a>
        </li>

    </ul>
    <!-- Tab panes -->
    <div class="tab-content" style="width: 100%;">
        <div class="tab-pane fade in active" id="rtab1">
            <?=
            $this->render(
                'reportes/_graficoCurvasPeso.php', [
                    'modelInfante' => $modelInfante,
                    'reporte' => $reportePeso,
                ]
            )

            ?>
        </div>
        <div class="tab-pane" id="rtab2">

            <?=
            $this->render(
                'reportes/_graficoCurvasTalla.php', [
                    'modelInfante' => $modelInfante,
                    'reporte' => $reporteTalla,
                ]
            )

            ?>
        </div>
        <div class="tab-pane" id="rtab3">
            <?=
            $this->render(
                'reportes/_graficoCurvasImc.php', [
                    'modelInfante' => $modelInfante,
                    'reporte' => $reporteImc,
                ]
            )

            ?>
        </div>
    </div>
</div>