<div class="panel ">
    <?=
    \common\components\fixWidget\HighCharts::widget([
        'clientOptions' => [
            'colors' => ['#EBAB17', '#FF9655', '#48DB6B', '#FFB0F2', '#373CDB', '#FF9655',],
            'chart' => [
//            'events' => array(
//                'load' => 'js:function(){Highcharts.drawTable(0)}',//0 porque es el primer chart en renderizar
//            ),
                'height' => '500',
                'backgroundColor' => [
                    'linearGradient' => ['x1' => 0, 'y1' => 0, 'x2' => 1, 'y2' => 1],
                    'stops' => [
                        [ 0, 'rgb(255, 255, 255)'],
                        [1, 'rgb(240, 240, 255)']
                    ]],
                'zoomType' => 'xy',
                'spacingRight' => 20,
                'borderWidth' => 2,
                'plotBackgroundColor' => 'rgba(255, 255, 255,.99)',
                'plotShadow' => true,
                'plotBorderWidth' => 1,
            ],
            'credits' => [
                'enabled' => false,
            ],
            'title' => [
//                'text' => 'Peso'
                'text' => $modelInfante->infante_genero == "FEMENINO" ? " Seguimiento de Peso para la edad Niña " : " Seguimiento de Peso para la edad Niño",
            ],
            'subtitle' => array(
                'text' => "Infante: " . $modelInfante->nombreCompleto,
            ),
            'xAxis' => [
                'title' => ['text' => 'Edad meses cumplidos'],
                'gridLineWidth' => 1,
                'lineColor' => '#000',
                'tickColor' => '#000',
                'allowDecimals' => false,
                'labels' => [
                    'rotation' => -45
                ],
                'categories' => $reporte['xAxis']['categories'],
            ],
            'plotOptions' => array(
                "series" => array("marker" =>
                    array("enabled" => false, //TODO
                        "states" => array(
                            "hover" => array(
                                "enabled" => true,
                                "radius" => 5)
                        )
                    ),),
                "area" => array("lineWidth" => 1,
                    "shadow" => false,
                ),
            ),
            'tooltip' => array(
                'enabled' => false,
            ),
            'yAxis' => array(
                'min' => 0,
                'max' => 30,
                'minorTickInterval' => 'auto',
                'title' => array('text' => 'Peso (kg)'),
                'lineColor' => '#000',
                'lineWidth' => 1,
                'tickWidth' => 1,
                'tickColor' => '#000',
            ),
            'series' => $reporte['series']
        ]
    ]);

    ?>
</div>