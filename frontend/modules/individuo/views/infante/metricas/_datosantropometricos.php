<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

?>

<?php if ($rol == 'educador' || $rol == 'Admin') : ?>

    <?php if ($model->datoAntropometricos): ?>

        <?=
        $this->render(
            '_view.php', [
                'modelInfante' => $model,
                'reportePeso' => $reportePeso,
                'reporteTalla' => $reporteTalla,
                'reporteImc' => $reporteImc,
            ]
        )
        ?>

    <?php endif; ?>
    <p class="container">
        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
            'onclick' => ' window.history.back();',
            'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Agregar nuevas medidas antropométricas'), ['/metricasindividuo/datoantropometrico/create', 'infante_id' => $model->infante_id], ['class' => 'btn btn-success']) ?>
    </p>
<?php elseif ($rol == 'coordinador' || $rol == 'coordinador-gad' || $rol == "nutricion") : ?>
    <?php if ($model->datoAntropometricos): ?>
        <?=
        $this->render(
            '_view.php', [
                'modelInfante' => $model,
                'reportePeso' => $reportePeso,
                'reporteTalla' => $reporteTalla,
                'reporteImc' => $reporteImc,
            ]
        )
        ?>
    <?php else : ?>
        <p>
            <?= Html::label('Aun no se han registrado medidas antropométricas para el infante') ?>
        </p>
    <?php endif; ?>
<?php endif; ?>

