<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

?>

<?php

if ($rol == 'coordinador' || $rol == 'Admin') :

    ?>
    <?php if ($model->sectorAsignadoInfante): ?>
    <?php if ($model->sectorAsignadoInfante->sectorAsignado->byPeriodoActivo): ?>
        <?=
        GridView::widget([
            'dataProvider' => $sectorInfante,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Salón',
                    'value' => 'sectorAsignado.salon.salon_nombre'
                ],
                [
                    'label' => 'Educador',
                    'value' => 'sectorAsignado.educador.nombreCompleto'
                ],
            ],
        ]);


        ?>
    <?php else : ?>
        <p>
            <?= Html::label('El sector asignado se debe registrar en la vista de detalles del educador') ?>
        </p>
    <?php endif; ?>
<?php else : ?>
    <p>
        <?= Html::label('El sector asignado se debe registrar en la vista de detalles del educador') ?>
    </p>

<?php endif; ?>
<?php elseif
($rol == 'educador') : ?>
    <?php if ($model->sectorAsignadoInfante): ?>
        <?php if ($model->sectorAsignadoInfante->sectorAsignado->byPeriodoActivo): ?>
            <?=
            GridView::widget(['dataProvider' => $sectorInfante,
                'columns' => [['class' => 'yii\grid\SerialColumn'],
                    ['label' => 'Salón',
                        'value' => 'sectorAsignado.salon.salon_nombre'],
                    ['label' => 'Educador',
                        'value' => 'sectorAsignado.educador.nombreCompleto'],],]);

            ?>
        <?php endif; ?>
    <?php endif; ?>
<?php elseif ($rol == 'coordinador-gad' || $rol = "nutricion") : ?>
    <?php if ($model->sectorAsignadoInfante): ?>
        <?php if ($model->sectorAsignadoInfante->sectorAsignado->byPeriodoActivo): ?>
            <?=
            GridView::widget(['dataProvider' => $sectorInfante,
                'columns' => [['class' => 'yii\grid\SerialColumn'],
                    ['label' => 'Salón',
                        'value' => 'sectorAsignado.salon.salon_nombre'],
                    ['label' => 'Educador',
                        'value' => 'sectorAsignado.educador.nombreCompleto'],],]);

            ?>
        <?php endif; ?>
    <?php else : ?>
        <p>
            <?= Html::label('Aun no se ha asignado el sector al infante') ?>
        </p>
    <?php endif; ?>
<?php endif; ?>


<p class="container">
    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), ['onclick' => ' window.history.back();',
        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


</p>
