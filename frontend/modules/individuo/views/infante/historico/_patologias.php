<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

?>

<?php if ($rol == 'educador' || $rol == 'Admin') : ?>
    <?php if ($model->patologias): ?>
        <?=
        GridView::widget([
            'dataProvider' => $patologia,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'patologia_pregunta_1',
                'patologia_pregunta_2',
                'patologia_pregunta_3',
                'patologia_pregunta_4',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($model1, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil "></span>', ['//individuo/patologia/update', 'id' => $key['patologia_id']], ['title' => 'Actualizar Patologías']);
                        }
                    ]
                ]
            ],
        ]);
        ?>
    <?php else : ?>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar'), ['/individuo/patologia/create', 'infante_id' => $model->infante_id], ['class' => 'btn btn-success']) ?>
        </p>

    <?php endif; ?>
<?php elseif ($rol == 'coordinador' || $rol == 'coordinador-gad' || $rol == "nutricion"): ?>
    <?php if ($model->patologias): ?>
        <?=
        GridView::widget([
            'dataProvider' => $patologia,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'patologia_pregunta_1',
                'patologia_pregunta_2',
                'patologia_pregunta_3',
                'patologia_pregunta_4',
            ],
        ]);
        ?>
    <?php else : ?>
        <p>
            <?= Html::label('Aun no se han registrado patologías para el infante') ?>
        </p>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


        </p>
    <?php endif; ?>
<?php endif; ?>


