<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

?>

<?php
if ($rol == 'coordinador' || $rol == 'Admin') :

    ?>

    <?php if ($model->matriculaActiva): ?>
    <?=
    GridView::widget([
        'dataProvider' => $matricula,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Centro infantil',
                'value' => 'cenInf.cen_inf_nombre'
            ],
            'periodo.limitesFechasPeriodo',
            'matricula_estado',
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template' => '{update}',
                'buttons' => [
                    'update' => function ($model1, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil "></span>', ['//inscripcion/matricula/update', 'id' => $key['matricula_id']], ['title' => 'Actualizar Matricula']);
                    }
                ]
            ]
        ],
    ]);

    ?>

<?php endif; ?>
<?php elseif ($rol == 'educador') : ?>
    <?php if ($model->matriculas): ?>
        <?=
        GridView::widget([
            'dataProvider' => $matricula,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Centro infantil',
                    'value' => 'cenInf.cen_inf_nombre'
                ],
                'periodo.limitesFechasPeriodo',
                'matricula_estado',
            ],
        ]);

        ?>
    <?php endif; ?>
<?php elseif ($rol == 'coordinador-gad') : ?>
    <?php if ($model->matriculaActiva): ?>
        <?=
        GridView::widget([
            'dataProvider' => $matricula,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label' => 'Centro infantil',
                    'value' => 'cenInf.cen_inf_nombre'
                ],
                'periodo.limitesFechasPeriodo',
                'matricula_estado',
            ],
        ]);

        ?>
    <?php else : ?>
        <p>
            <?= Html::label('Aun no se han registrado la matricula') ?>
        </p>
    <?php endif; ?>
<?php endif; ?>


<p class="container">
    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
        'onclick' => ' window.history.back();',
        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
    <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->infante_id], ['class' => 'btn btn-primary']) ?>
    <?php

    if($rol=='coordinador'|| $rol == 'Admin')
         echo Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['/inscripcion/matricula/create', 'infante_id' => $model->infante_id], ['class' => 'btn btn-success'])
    ?>
</p>
