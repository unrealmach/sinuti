<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

?>


<?php if ($rol == 'educador' || $rol == 'Admin') : ?>
    <?php if ($model->registroVacunaInfantes): ?>

        <?=
        GridView::widget([
            'dataProvider' => $registro_vacunas,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'tipoVacuna.tipo_vac_enfermedad',
                'tipoVacuna.tipo_vac_nombre',
                'tipo_vac_fecha_aplicacion',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{update}',
                    'buttons' => [
                        'update' => function ($model1, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil "></span>', ['//individuo/registrovacunainfante/update', 'id' => $key['reg_vac_inf_id']], ['title' => 'Actualizar vacuna']);
                        }
                    ]
                ]
            ],
        ]);

        ?>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Agregar nuevas vacunas'), ['/individuo/registrovacunainfante/create', 'infante_id' => $model->infante_id], ['class' => 'btn btn-success']) ?>
        </p>
    <?php else : ?>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['/individuo/registrovacunainfante/create', 'infante_id' => $model->infante_id], ['class' => 'btn btn-success']) ?>
        </p>

    <?php endif; ?>
<?php elseif ($rol == 'coordinador' || $rol == 'coordinador-gad' || $rol == "nutricion") : ?>
    <?php if ($model->registroVacunaInfantes): ?>
        <?=
        GridView::widget([
            'dataProvider' => $registro_vacunas,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'tipoVacuna.tipo_vac_enfermedad',
                'tipoVacuna.tipo_vac_nombre',
                'tipo_vac_fecha_aplicacion',
            ],
        ]);
        ?>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


        </p>
    <?php else : ?>
        <p>
            <?= Html::label('Aun no se han registrado vacunas para el infante') ?>
        </p>
        <p class="container">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


        </p>
    <?php endif; ?>
<?php endif; ?>

