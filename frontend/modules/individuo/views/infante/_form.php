<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */
/* @var $form yii\widgets\ActiveForm */



?>
</br>
<div class="infante-form ">

    <?php
    $form = ActiveForm::begin(['id' => $model->formName(),
        'enableAjaxValidation' => true,
        'enableClientScript' => true,
        'enableClientValidation' => true,
        'options'=>[
            'style'=>'margin: 0px !important;'
        ]
    ]);

    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Infante')) : Html::encode(Yii::t('app', 'Actualizar Infante')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">
            <fieldset>
                <legend>Datos personales</legend>
                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_dni', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                            ->textInput(['placeholder' => 'Ingrese ...',])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_nombres', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_apellidos', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, "infante_nacionalidad", [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>',
                        ])
                            ->widget(Select2::classname(), [
                                'data' => Yii::$app->Util->getNacionalidades(),
                                'language' => 'es',
                                'options' => ['placeholder' => 'Seleccione ...', 'class' => 'col-sm-12',],
                            ])
                            ->label(null, ['class' => 'col-sm-12 control-label'])


                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_genero', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->dropDownList([ 'FEMENINO' => 'FEMENINO', 'MASCULINO' => 'MASCULINO',])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_etnia', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->dropDownList([ 'MESTIZO' => 'MESTIZO', 'AFROAMERICANO' => 'AFROAMERICANO', 'INDIGENA' => 'INDIGENA',])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>

                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infantes_fecha_nacimiento', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                            ->widget(DatePicker::className(), [
//            'name' => 'm_c_sem_fecha_inicio',
                                'options' => ['placeholder' => 'Seleccione fecha'],
                                'readonly' => true,
                                'pluginOptions' => [
//                            'daysOfWeekDisabled' => '0,2,3,4,5,6',
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true,
//                            'calendarWeeks' => true,
                                ]
                            ])->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"></div>
                </div>

            </fieldset>
            <fieldset>
                <legend>Datos del representante</legend>
                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_documento', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_nombres', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_apellidos', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_parentesco', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->dropDownList([ 'MAMA' => 'MAMA', 'PAPA' => 'PAPA', 'ABUELO' => 'ABUELO', 'ABUELA' => 'ABUELA', 'HERMANO' => 'HERMANO', 'HERMANA' => 'HERMANA', 'TIO' => 'TIO', 'TIA' => 'TIA', 'MADRASTRA' => 'MADRASTRA', 'PADRASTRO' => 'PADRASTRO', 'REPRESENTANTE_LEGAL' => 'REPRESENTANTE LEGAL',])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_direccion_domiciliaria', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_telefono', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'infante_represent_correo', [
                            'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                            ->textInput(['placeholder' => 'Ingrese ...'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])

                        ?>
                    </div>
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4"></div>
                </div>




            </fieldset>
        </div>
        <div class="container">

            <div class="form-group">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

                <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
            </div>


        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

<?php
$parametrosExtras = $model->isNewRecord ? '?submit=true' : '&submit=true';

$script = <<< JS
   
//llama al formulario y mediante ajax antes de enviar realiza la validacion
    
    $("form#{$model->formName()} button[type='submit']").on('click', function(e){
        var r = confirm("¿Esta seguro que desea guardar esta información?");
           if (r == true) {
                 var \$form= $("form#{$model->formName()}");
               
           $.ajax({
                beforeSend:function(){
                    $(\$form).submit();
                    showModalLoading();
                },
                type:"POST",
                url:\$form.attr("action")+"{$parametrosExtras}",  //envia la accion del formulario osea el action
                data:\$form.serialize(), //envia el formulario serializado 
                //TODO: add to update logs
                success: function(result){
//        alert(\$form.attr("action")+"?submit=true");
                    if(result.status==true){
    
                      
                        showFlashAlert(result.errors)
                        closeModal();
                    }else{
                        showFlashAlert(result.errors);
                        closeModal();
                        }
                   console.log(result);
                   }
                 }) 

           } else {
               return false;
           }
    
          
    return false;    
});
    
JS;
$this->registerJs($script);


