<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */

$this->title = Yii::t('app', 'Actualizar {modelClass}: ', [
    'modelClass' => 'Infante',
]) . ' ' . $model->getNombreCompleto();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getNombreCompleto(), 'url' => ['view', 'id' => $model->infante_id]];
$this->params['breadcrumbs'][] = Yii::t('app', $this->title);

?>
<div class="infante-update">
    <br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
