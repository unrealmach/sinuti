<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */

$this->title = Yii::t('app', 'Registrar Hábitos de Alimentación');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hábitos de Alimentación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patologia-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
