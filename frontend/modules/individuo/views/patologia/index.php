<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\individuo\models\PatologiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Patologias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patologia-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    if ($rol == 'coordinador' || $rol == 'coordinador-gad'):
        ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'patologia_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'patologia_pregunta_1:ntext',
                'patologia_pregunta_2:ntext',
                'patologia_pregunta_3:ntext',
                // 'patologia_pregunta_4:ntext',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>

    <?php elseif ($rol == 'educador'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'patologia_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'patologia_pregunta_1:ntext',
                'patologia_pregunta_2:ntext',
                'patologia_pregunta_3:ntext',
                // 'patologia_pregunta_4:ntext',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'Admin' || $rol == 'sysadmin'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'patologia_id',
                ['attribute' => 'infante_id',
                    'value' => 'infante.nombreCompleto'],
                'patologia_pregunta_1:ntext',
                'patologia_pregunta_2:ntext',
                'patologia_pregunta_3:ntext',
                // 'patologia_pregunta_4:ntext',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update} {delete}',
                ],
            ],
        ]);
        ?>

    <?php endif; ?>

</div>
