<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\individuo\models\Infante;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */
/* @var $form yii\widgets\ActiveForm */
?>

</br>
<div class="patologia-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Hábitos de Alimentación')) : Html::encode(Yii::t('app', 'Actualizar Hábitos de Alimentación')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-2">
                    <?=
                    $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map([0 => Infante::findOne($model->infante_id)], 'infante_id', 'nombreCompleto', 'infante_dni'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-12',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
                <div class="col-sm-5">
                    <?=
                    $form->field($model, 'patologia_pregunta_1', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6, 'style' => 'text-transform: uppercase', 'placeholder' => 'Describa ...'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-5">
                    <?=
                    $form->field($model, 'patologia_pregunta_2', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6, 'style' => 'text-transform: uppercase', 'placeholder' => 'Describa ...'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>


            </div>

            <div class="row">
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'patologia_pregunta_3', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6, 'style' => 'text-transform: uppercase', 'placeholder' => 'Describa ...'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>

                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'patologia_pregunta_4', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6, 'style' => 'text-transform: uppercase', 'placeholder' => 'Describa ...'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
            </div>

            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-md btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
                </div>
            </div>
        </div>

    </div>


    <?php ActiveForm::end(); ?>

</div>
