<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Hábitos de alimentación',
]) . ' ' . $model->patologia_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hábitos de Alimentación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->patologia_id, 'url' => ['view', 'id' => $model->patologia_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="patologia-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
