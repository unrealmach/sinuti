<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */

$this->title = 'Patologías para el/la infante' . $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patologias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patologia-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>

    <?php if ($rol == 'educador') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->patologia_id], ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>    

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
//                    'patologia_id',
//                    'infante_id',
                    'patologia_pregunta_1:ntext',
                    'patologia_pregunta_2:ntext',
                    'patologia_pregunta_3:ntext',
                    'patologia_pregunta_4:ntext',
                ],
            ])
            ?>
        </div>
    </div>
</div>
