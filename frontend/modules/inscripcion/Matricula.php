<?php

namespace frontend\modules\inscripcion;

class Matricula extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\inscripcion\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
