<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */

$this->title = Yii::t('app', 'Crear Matricula');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matriculas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matricula-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
