<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inscripcion\models\MatriculaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Matriculas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="matricula-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if ($rol == 'coordinador' || $rol == 'educador') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'matricula_id',
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                ['attribute' => 'infante_id',
                    'value' => 'infante.cedulaPasaporteAndNombreCompleto'],
//                ['attribute'=>'infante_id',
//                    'value'=> 'infante.nombreCompleto'],
                ['attribute' => 'periodo_id',
                    'value' => 'periodo.limitesFechasPeriodo'],
                'matricula_estado',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>

        <?php elseif ($rol == 'Admin' || $rol == 'sysadmin') : ?>
    
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'matricula_id',
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                ['attribute' => 'infante_id',
                    'value' => 'infante.cedulaPasaporteAndNombreCompleto'],
//                ['attribute'=>'infante_id',
//                    'value'=> 'infante.nombreCompleto'],
                ['attribute' => 'periodo_id',
                    'value' => 'periodo.limitesFechasPeriodo'],
                'matricula_estado',
                [
                    'class' => \yii\grid\ActionColumn::className(),
//                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
        <?php elseif ($rol == 'coordinador-gad') : ?>
    
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'matricula_id',
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                ['attribute' => 'infante_id',
                    'value' => 'infante.cedulaPasaporteAndNombreCompleto'],
//                ['attribute'=>'infante_id',
//                    'value'=> 'infante.nombreCompleto'],
                ['attribute' => 'periodo_id',
                    'value' => 'periodo.limitesFechasPeriodo'],
                'matricula_estado',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>
</div>
