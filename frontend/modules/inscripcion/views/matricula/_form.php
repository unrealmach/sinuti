<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\individuo\models\Infante;
use backend\modules\parametros_sistema\models\Periodo;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */
/* @var $form yii\widgets\ActiveForm */

?>

</br>

<div class="matricula-form section default-bg">

    <?php $form = ActiveForm::begin(); ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Matricula')) : Html::encode(Yii::t('app', 'Actualizar Matricula')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
            <div class="col-sm-4">
            <?=
            $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Cibv::find()->all(), 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccione Cibv', 'class' => 'col-sm-6',],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(null, ['class' => 'col-sm-6 control-label']);

            ?>
            </div>
            <div class="col-sm-4">
            <?php
            if($model->isNewRecord){
         echo   $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                'data' => ArrayHelper::map( Infante::find()->noMatriculados()->all(), 'infante_id', 'nombreCompleto', 'infante_dni'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(null, ['class' => 'col-sm-2 control-label']);
        }
        else {
            ?>
            <div class="form-group">
                        <?php
                        echo Html::label('Infante: ', '', ['class' => 'col-sm-12 control-label']);
                        echo Html::input('text', 'data', $model->infante->nombreCompleto, ['class' => 'col-sm-12', 'readonly' => true]);
                        ?>
            </div>
       <?php } ?>
            </div>
            <div class="col-sm-4">
            <?php
            if($model->isNewRecord){
         echo   $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                'data' => ArrayHelper::map( Infante::find()->noMatriculados()->all(), 'infante_id', 'nombreCompleto', 'infante_dni'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(null, ['class' => 'col-sm-2 control-label']);
        }
            ?>
            </div>
            <div class="col-sm-4">
            <?=
            $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(null, ['class' => 'col-sm-2 control-label']);

            ?>
            </div>
            <div class="col-sm-4">
            <?=
            $form->field($model, 'matricula_estado', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(SwitchInput::className(), [
                'value' => $model->matricula_estado,
                'pluginOptions' => [
                    'size' => 'large',
                    'onColor' => 'success',
                    'offColor' => 'info',
                    'onText' => Yii::t('app', 'APROBADA'),
                    'offText' => Yii::t('app', 'ANULADA')
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label']);

            ?>
            </div>
            </div>
            
            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>


            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>


