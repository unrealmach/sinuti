<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Matricula',
    ]) . ' ' . $model->matricula_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matriculas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->matricula_id, 'url' => ['view', 'id' => $model->matricula_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="matricula-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])

    ?>

</div>
