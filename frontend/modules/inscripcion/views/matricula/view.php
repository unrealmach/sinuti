<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */

$this->title = $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matriculas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

</br>
<div class="matricula-view" style="margin: 20px 0;">
<div class="panel panel-default">
        <div class="panel-heading">
            <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode($this->title) ?></h1>

        </div>

        <div class="panel-body">

            <div class="horizontal">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>


                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <div class="row">
                        <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                //                        'matricula_id',
                                    'cenInf.cen_inf_nombre',
                //                'infante.nombreCompleto',
                                    'periodo.limitesFechasPeriodo',
                                    'matricula_estado'
                                ],
                            ])
                            ?>

                        <p class="container">
                        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>


                                <?php if ($rol == 'coordinador' || "sysadmin") : ?>

                                    <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->matricula_id], ['class' => 'btn btn-success']) ?>

                                <?php endif; ?>
                            </p>
                        </div>
                    </div>


                </div>


            </div>

        </div>


    </div>
