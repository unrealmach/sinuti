<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inscripcion\models\SalonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Salones');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salon-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar nuevo'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php if ($rol == 'coordinador') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'salon_id',
                ['attribute' => 'grupo_edad_id',
                    'value' => 'grupoEdad.grupo_edad_descripcion'],
                'salon_capacidad',
                'salon_nombre',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador-gad'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'salon_id',
                ['attribute' => 'grupo_edad_id',
                    'value' => 'grupoEdad.grupo_edad_descripcion'],
                'salon_capacidad',
                'salon_nombre',
                [
                    'class' => \yii\grid\ActionColumn::className(),
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>

    <?php elseif ($rol == 'Admin' || $rol == 'sysadmin'): ?>
<?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'salon_id',
                ['attribute' => 'grupo_edad_id',
                    'value' => 'grupoEdad.grupo_edad_descripcion'],
                'salon_capacidad',
                'salon_nombre',
                [
                    'class' => \yii\grid\ActionColumn::className(),

 //                   'template' => '{view}',

                ],
            ],
        ]);
        ?>

    <?php endif; ?>
</div>
