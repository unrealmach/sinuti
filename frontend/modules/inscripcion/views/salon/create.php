<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Salon */

$this->title = Yii::t('app', 'Crear Salón');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salon-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
