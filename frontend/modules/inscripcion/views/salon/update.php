<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Salon */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Salon',
]) . ' ' . $model->salon_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->salon_id, 'url' => ['view', 'id' => $model->salon_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="salon-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
