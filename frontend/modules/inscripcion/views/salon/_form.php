<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\metricas_individuo\models\GrupoEdad;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Salon */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="salon-form ">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Salón')) : Html::encode(Yii::t('app', 'Actualizar Salón')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'grupo_edad_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(GrupoEdad::find()->all(), 'grupo_edad_id', 'grupo_edad_descripcion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Edad', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'salon_capacidad', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>

                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'salon_nombre', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>

                </div>


            </div>
            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>


            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
