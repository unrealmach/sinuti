<?php

namespace frontend\modules\inscripcion\controllers;

use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use Yii;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\inscripcion\models\MatriculaSearch;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;

use backend\modules\recursos_humanos\models\AutoridadesCibv;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * MatriculaController implements the CRUD actions for Matricula model.
 */
class MatriculaController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Matricula models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MatriculaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single Matricula model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new Matricula model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($infante_id = null) {
        $model = new Matricula();
        if ($infante_id != null) {
            $model->infante_id = $infante_id;
        }
        //            TODO validar que solo tenga una matricula x cibv(no pertenecer a 2 cibv)
        $authAsignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($authAsignmentModel->item_name == 'coordinador') {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            if (!empty($asignacionCoordinadorCibv)) {
                $periodo = \backend\modules\parametros_sistema\models\Periodo::find()->periodoActivo()->one();
                $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
                $model->cen_inf_id = $autoridadCibv->cen_inf_id;
                $validateMatricula = Matricula::find()->deCibvAndPeriodo($model->cen_inf_id, $model->infante_id, $periodo->periodo_id)->one();
                if (!empty($validateMatricula)) {
                    throw new NotFoundHttpException('Ya existe una matricula para el período actual');
                }
            } else {
                throw new NotFoundHttpException('No existe un coordinador para el Centro Infantil en el periodo Actual');
            }
        }
        $authAsignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($authAsignmentModel->item_name == 'educador') {
            $asignacionEducador = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
            if (!empty($asignacionEducador)) {
                $sectorAsignado = SectorAsignadoEducador::findOne(['sector_asignado_educador_id' => $asignacionEducador->sector_asignado_educador_id]);
                if (!empty($autoridadCibv)) {
                    $autoridadCibv = AutoridadesCibv::findOne(['cen_inf_id' => $sectorAsignado->cen_inf_id]);
                    $model->cen_inf_id = $autoridadCibv->cen_inf_id;
                    $model->periodo_id = $autoridadCibv->periodo_id;
                    $validateMatricula = Matricula::find()->deCibvAndPeriodo($model->cen_inf_id, $model->infante_id, $model->periodo_id);
                    if (!empty($validateMatricula))
                        throw new NotFoundHttpException('Ya existe una matricula para el período actual');
                }
            }
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->matricula_estado == '0') {
                $model->matricula_estado = $model->ESTADO_RETIRADO;
            } else if ($model->matricula_estado = '1') {
                $model->matricula_estado = $model->ESTADO_ACTIVO;
            }
            if ($model->save()) {
                return $this->redirect(['/individuo/infante/view', 'id' => $model->infante_id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Matricula model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->matricula_estado == $model->ESTADO_ACTIVO) {
            $model->matricula_estado = TRUE;
        } else if ($model->matricula_estado == $model->ESTADO_RETIRADO) {
            $model->matricula_estado = FALSE;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->matricula_estado == '0') {
                $model->matricula_estado = $model->ESTADO_RETIRADO;
            } else if ($model->matricula_estado = '1') {
                $model->matricula_estado = $model->ESTADO_ACTIVO;
            }
            if ($model->save())
                return $this->redirect(['/individuo/infante/view', 'id' => $model->infante_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Matricula model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Matricula model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Matricula the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Matricula::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
