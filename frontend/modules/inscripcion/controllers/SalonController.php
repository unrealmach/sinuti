<?php

namespace frontend\modules\inscripcion\controllers;

use Yii;
use backend\modules\inscripcion\models\Salon;
use backend\modules\inscripcion\models\SalonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalonController implements the CRUD actions for Salon model.
 */
class SalonController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Salon models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SalonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single Salon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new Salon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Salon();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->salon_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Salon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->salon_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Salon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Salon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Salon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Salon::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
