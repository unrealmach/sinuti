<?php

namespace frontend\modules\recursos_humanos;

class Recursoshumanos extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\recursos_humanos\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
