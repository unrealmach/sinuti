<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\EducadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Histórico de educadores');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Educadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="educador-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 
    ?>
    <p>
        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-arrow-left']) . Yii::t('app', ' Volver'), [
            'onclick' => ' window.history.back();',
            'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
    </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'educador_id',
                'educador_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->educador_nombres . " " . $model->educador_apellidos;
            }
                ],
                'educador_telefono',
                'educador_correo',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>



</div>
