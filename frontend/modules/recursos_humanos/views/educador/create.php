<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Educador */

$this->title = Yii::t('app', 'Crear Educador');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Educadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="educador-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
