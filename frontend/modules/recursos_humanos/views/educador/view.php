<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Educador */

$this->title = $model->educador_nombres . " " . $model->educador_apellidos;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Educadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
</br>
<div class="educador-view" style="margin: 20px 0;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
        </div>

        <div class="panel-body">

            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>
                    <li><a href="#vtab2" role="tab" data-toggle="tab"><i class="fa fa-address-card-o pr-10"></i>
                            Salón Infantil Asignado</a></li>
                    <li><a href="#vtab3" role="tab" data-toggle="tab"><i class="fa fa-address-card pr-10"></i> Infantes Asignados</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <div class="row">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
    //                        'educador_id',
                                    'educador_dni',
    //            'educador_nombres',
    //            'educador_apellidos',
                                    'educador_telefono',
                                    'educador_correo'
                                ],
                            ])

                            ?>
                            <p class="container">
                                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->educador_id], ['class' => 'btn btn-primary']) ?>
                            </p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="vtab2">
                        <?php if ($model->sectorEducadorPeriodoActivo) : ?>
                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    ['attribute' => 'cen_inf_id',
                                        'value' => 'cenInf.cen_inf_nombre'],
                                    ['attribute' => 'periodo_id',
                                        'value' => 'periodo.limitesFechasPeriodo'],
                                    ['attribute' => 'salon_id',
                                        'value' => 'salon.salon_nombre'],
                                ],
                            ]);

                            ?>
                        <?php else : ?>
                            <?php if ($rol == 'coordinador' || $rol == 'sysadmin') : ?>
                                <p class="container">
                                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                        'onclick' => ' window.history.back();',
                                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                    <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['/recursoshumanos/sectorasignadoeducador/create', 'educador_id' => $model->educador_id], ['class' => 'btn btn-success']) ?>
                                </p>
                            <?php elseif ($rol == 'coordinador-gad'): ?>
                                <p class="container">
                                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                        'onclick' => ' window.history.back();',
                                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                    <?= Html::label('Aun no se ha asignado el salon al educador ') ?>
                                </p>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>

                    <div class="tab-pane fade" id="vtab3">
                        <?php if ($model->sectorEducadorPeriodoActivo) : ?>
                            <?php if ($model->sectorEducadorPeriodoActivo->sectorAsignadoInfante) : ?>
                                <p>
                                    <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Asignar infantes'), ['/centroinfantil/sectorasignadoinfante/create', 'sector_asig_id' => $sector_asig_id], ['class' => 'btn btn-success']) ?>
                                </p>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider2,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        'infante.nombreCompleto',
                                        [
                                            'class' => \yii\grid\ActionColumn::className(),
                                            'template' => '{view}',
                                            'buttons' => [
                                                'view' => function ($model1, $key) {
                                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['//individuo/infante/view', 'id' => $key['infante_id']], ['title' => 'Visualizar Infantes']);
                                                }
                                            ]
                                        ]
                                    ],
                                ]);

                                ?>
                            <?php else : ?>
                                <p class="container">
                                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                        'onclick' => ' window.history.back();',
                                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                    <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Asignar infantes'), ['/centroinfantil/sectorasignadoinfante/create', 'sector_asig_id' => $sector_asig_id], ['class' => 'btn btn-success']) ?>
                                </p>
                            <?php endif; ?>
                        <?php else : ?>
                            <p class="container">
                                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Asignar infantes'), ['/centroinfantil/sectorasignadoinfante/create', 'sector_asig_id' => $sector_asig_id], ['class' => 'btn btn-success']) ?>
                            </p>
                        <?php endif; ?>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>
