<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\EducadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Educadores');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="educador-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>

    <?php
    // echo $this->render('_search', ['model' => $searchModel]); 

    if ($rol == 'coordinador'):
        ?>
        <div class="col-sm-12">
            <p>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/recursoshumanos/educador/index-inactivos'], ['class' => 'btn btn-info']) ?>
            </p>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'educador_id',
                'educador_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->educador_nombres . " " . $model->educador_apellidos;
            }
                ],
                'educador_telefono',
                'educador_correo',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
 <?php elseif ($rol == 'sysadmin'):
        ?>
        <div class="col-sm-12">
            <p>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/recursoshumanos/educador/index-inactivos'], ['class' => 'btn btn-info']) ?>
            </p>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'educador_id',
                'educador_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->educador_nombres . " " . $model->educador_apellidos;
            }
                ],
                'educador_telefono',
                'educador_correo',
                [
                    'class' => 'yii\grid\ActionColumn',
 //                   'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>

    <?php elseif ($rol == 'educador') :
        ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'educador_id',
                'educador_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->educador_nombres . " " . $model->educador_apellidos;
            }
                ],
                'educador_telefono',
                'educador_correo',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'coordinador-gad') :
        ?>
        <div class="col-sm-12">
            <p>
                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/recursoshumanos/educador/index-inactivos'], ['class' => 'btn btn-info']) ?>
            </p>
        </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'educador_id',
                'educador_dni',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                return $model->educador_nombres . " " . $model->educador_apellidos;
            }
                ],
                'educador_telefono',
                'educador_correo',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    
    <?php endif; ?>

</div>
