<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\inscripcion\models\Salon;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Educador */
/* @var $form yii\widgets\ActiveForm */
?>
</br>
<div class="educador-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registrar Educador')) : Html::encode(Yii::t('app', 'Actualizar Educador')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_dni', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_nombres', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_apellidos', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_telefono', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_correo', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...',
                        'readonly'=> $model->isNewRecord?false:true))
                        ->label(null, ['class' => 'col-sm-3 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?php if ($model->isNewRecord) : ?>
                        <?=
                        $form->field($model, 'salon', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Salon::find()->all(), 'salon_id', 'salon_nombre'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(null, ['class' => 'col-sm-3 control-label']);
                        ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
