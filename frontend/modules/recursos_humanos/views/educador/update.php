<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Educador */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Educador',
        ]) . ' ' . $model->educador_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Educadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->educador_id, 'url' => ['view', 'id' => $model->educador_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="educador-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
