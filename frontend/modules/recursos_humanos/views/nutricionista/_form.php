<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Nutricionista */
/* @var $form yii\widgets\ActiveForm */

?>
</br>
<div class="nutricionista-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registar Nutricionista')) : Html::encode(Yii::t('app', 'Actualizar Nutricionista')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nutricionista_dni', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nutricionista_nombres', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nutricionista_apellidos', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nutricionista_telefono', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'nutricionista_correo', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...',
                        'readonly'=> $model->isNewRecord?false:true))
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'sectorAsignado', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
                        ->widget(kartik\select2\Select2::classname(), [
                            'data' => yii\helpers\ArrayHelper::map($modelZonaDistrital, 'distrito_id', 'distrito_codigo', 'distrito_descripcion'),

                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione la zona distrital', 'class' => 'col-sm-2',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(null, ['class' => 'col-sm-12 control-label']);

                    ?>
                </div>
            </div>

            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
