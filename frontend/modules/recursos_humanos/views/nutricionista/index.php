<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\NutricionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Nutricionistas');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="nutricionista-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php if ($rol == 'coordinador-gad' || $rol == 'sysadmin') : ?>
    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'nutricionista_id',
            'nutricionista_dni',
            'nutricionista_nombres',
            'nutricionista_apellidos',
            'nutricionista_telefono',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}, {update}',
            ],
        ],
    ]);
    ?>
    <?php elseif($rol == 'nutricion') : ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'nutricionista_id',
            'nutricionista_dni',
            'nutricionista_nombres',
            'nutricionista_apellidos',
            'nutricionista_telefono',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}, {update}',
            ],
        ],
    ]);
    ?>
    <?php endif; ?>
</div>
