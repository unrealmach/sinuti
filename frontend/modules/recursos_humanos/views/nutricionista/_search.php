<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\NutricionistaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nutricionista-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nutricionista_id') ?>

    <?= $form->field($model, 'nutricionista_dni') ?>

    <?= $form->field($model, 'nutricionista_nombres') ?>

    <?= $form->field($model, 'nutricionista_apellidos') ?>

    <?= $form->field($model, 'nutricionista_telefono') ?>

    <?php // echo $form->field($model, 'fecha_inicio_actividad') ?>

    <?php // echo $form->field($model, 'fecha_fin_actividad') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
