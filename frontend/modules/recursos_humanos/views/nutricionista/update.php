<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Nutricionista */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Nutricionista',
]) . ' ' . $model->nutricionista_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutricionistas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nutricionista_id, 'url' => ['view', 'id' => $model->nutricionista_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="nutricionista-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
        'modelZonaDistrital' => $modelZonaDistrital,
    ]) ?>

</div>
