<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Nutricionista */

$this->title = Yii::t('app', 'Crear Nutricionista');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutricionistas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutricionista-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
         'modelZonaDistrital' => $modelZonaDistrital,
    ]) ?>

</div>
