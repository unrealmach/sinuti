<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\AutoridadescibvSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autoridadescibv-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'autoridades_cibv_id') ?>

    <?= $form->field($model, 'periodo_id') ?>

    <?= $form->field($model, 'coordinador_id') ?>

    <?= $form->field($model, 'cen_inf_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
