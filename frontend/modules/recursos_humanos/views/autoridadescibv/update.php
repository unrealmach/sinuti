<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Autoridadescibv */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Autoridadescibv',
        ]) . ' ' . $model->autoridades_cibv_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Autoridades Centro Infanril'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->autoridades_cibv_id, 'url' => ['view', 'id' => $model->autoridades_cibv_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="autoridadescibv-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
        'cen_inf_id' => $cen_inf_id,
        'periodo' => $periodo,
        'listValidCoordCibv' => $listValidCoordCibv
    ])
    ?>

</div>
