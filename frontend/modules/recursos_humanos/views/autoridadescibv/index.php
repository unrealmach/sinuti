<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\AutoridadescibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Autoridades Centros Infantiles');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="autoridadescibv-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'autoridades_cibv_id',
            ['attribute' => 'cen_inf_id',
                'value' => 'cenInf.cen_inf_nombre'],
            ['attribute' => 'coordinador_id',
                'value' => 'coordinador.coordinador_nombres'],
            ['attribute' => 'periodo_id',
                'value' => 'periodo.limitesFechasPeriodo'],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>

</div>
