<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\recursos_humanos\models\CoordinadorCibv;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Autoridadescibv */
/* @var $form yii\widgets\ActiveForm */
?>
</br>
<div class="autoridadescibv-form ">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Crear Autoridades Centro Infantil')) :Html::encode(Yii::t('app', 'Actualizar Autoridades Centro Infantil')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-2 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($listValidCoordCibv, 'coordinador_id', 'nombreCompleto', 'coordinador_DNI'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Coordinador', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-2 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map([0 => Cibv::findOne($model->cen_inf_id)], 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Centro Infantil', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-6 control-label']);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => $periodo->fecha_inicio,
                                'endDate' => $periodo->fecha_fin,
                            ],
                        ])->label(null, ['class' => 'col-sm-6 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => $periodo->fecha_inicio,
                                'endDate' => $periodo->fecha_fin,
                            ],
                        ])->label(null, ['class' => 'col-sm-6 control-label'])
                    ?>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

                </div>
            </div>

        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
