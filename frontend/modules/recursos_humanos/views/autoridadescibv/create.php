<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Autoridadescibv */

$this->title = Yii::t('app', 'Crear Autoridades Centro Infantil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Autoridades Centro Infantil'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autoridadescibv-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
//        'cen_inf_id'=>$cen_inf_id,
         'listValidCoordCibv' => $listValidCoordCibv,
    ])
    ?>

</div>
