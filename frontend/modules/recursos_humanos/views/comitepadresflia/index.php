<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\ComitepadresfliaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comite Padres Flias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comite-padres-flia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Comite Padres Flia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'comite_id',
            'comite_nombres',
            'comite_apellidos',
            'comite_cedula_o_pasaporte',
            'comite_numero_contacto',
            // 'grupo_comite_id',
            // 'cargo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
