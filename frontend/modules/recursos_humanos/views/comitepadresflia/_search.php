<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitepadresfliaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comite-padres-flia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comite_id') ?>

    <?= $form->field($model, 'comite_nombres') ?>

    <?= $form->field($model, 'comite_apellidos') ?>

    <?= $form->field($model, 'comite_cedula_o_pasaporte') ?>

    <?= $form->field($model, 'comite_numero_contacto') ?>

    <?php // echo $form->field($model, 'grupo_comite_id') ?>

    <?php // echo $form->field($model, 'cargo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
