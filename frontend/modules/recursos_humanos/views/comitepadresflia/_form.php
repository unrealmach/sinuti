<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitePadresFlia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comite-padres-flia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
            $form->field($model, 'comite_nombres', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
            ->label(null, ['class' => 'col-sm-2 control-label'])
    ?>

    <?=
            $form->field($model, 'comite_apellidos', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
            ->label(null, ['class' => 'col-sm-2 control-label'])
    ?>

    <?=
            $form->field($model, 'comite_cedula_o_pasaporte', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
            ->label(null, ['class' => 'col-sm-2 control-label'])
    ?>

    <?=
            $form->field($model, 'comite_numero_contacto', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
            ->label(null, ['class' => 'col-sm-2 control-label'])
    ?>

        <?=
                $form->field($model, 'grupo_comite_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>

<?=
        $form->field($model, 'cargo', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'PRESIDENTE' => 'PRESIDENTE', 'VICEPRESIDENTE' => 'VICEPRESIDENTE', 'TESORERO' => 'TESORERO', 'SECRETARIO' => 'SECRETARIO',])
        ->label(null, ['class' => 'col-sm-2 control-label'])
?>

    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
