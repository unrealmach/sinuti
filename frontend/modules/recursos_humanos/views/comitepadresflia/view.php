<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitePadresFlia */

$this->title = $model->comite_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comite Padres Flias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comite-padres-flia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->comite_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->comite_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <!--<p>TODO hacer la vista para el comite de padres de familia</p>-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'comite_id',
            'comite_nombres',
            'comite_apellidos',
            'comite_cedula_o_pasaporte',
            'comite_numero_contacto',
            'grupo_comite_id',
            'cargo',
        ],
    ]) ?>

</div>
