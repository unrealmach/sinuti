<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitePadresFlia */

$this->title = Yii::t('app', 'Crear Comite Padres Flia');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comite Padres Flias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comite-padres-flia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
