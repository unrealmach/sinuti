<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\CoordinadorcibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hisotorico de coordinadores de centros infantiles');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="coordinador-cibv-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-arrow-left']) . Yii::t('app', ' Volver'), [
            'onclick' => ' window.history.back();',
            'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                        'coordinador_id',
            'coordinador_DNI',
            ['attribute' => 'nombre',
                'value' => function($model) {
            return $model->coordinador_nombres . " " . $model->coordinador_apellidos;
        }
            ],
            'coordinador_contacto',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>

</div>
