<?php

use backend\modules\parametros_sistema\models\Periodo;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorCibv */
/* @var $form yii\widgets\ActiveForm */

?>
</br>
<div class="coordinador-cibv-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registar Coordinador')) : Html::encode(Yii::t('app', 'Actualizar Coordinador')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_DNI', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-10 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_nombres', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_apellidos', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_contacto', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'coordinador_correo', [
                        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...',
                        'readonly' => $model->isNewRecord ? false : true))
                        ->label(null, ['class' => 'col-sm-3 control-label'])

                    ?>
                </div>
                <div class="col-sm-4">
                    <?php
                    if ($model->isNewRecord) {
                        echo $form->field($model, 'periodo', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(null, ['class' => 'col-sm-3 control-label']);
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?php
                    if ($model->isNewRecord) {
                        echo $form->field($model, 'cibv', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($listValidCibv, 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione Centro Infantil', 'class' => 'col-sm-2',],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(null, ['class' => 'col-sm-6 control-label']);
                    }
                    ?>
                </div>
                <div class="col-sm-4"></div>
            </div>
            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
