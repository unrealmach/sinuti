<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorCibv */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Coordinador Cibv',
        ]) . ' ' . $model->coordinador_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coordinador_id, 'url' => ['view', 'id' => $model->coordinador_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="coordinador-cibv-update">

    </br>
    <?=
    $this->render('_form', [
        'model' => $model,
        'listValidCibv' => $listValidCibv,
    ])
    ?>

</div>
