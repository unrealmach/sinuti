<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorcibvSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coordinador-cibv-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coordinador_id') ?>

    <?= $form->field($model, 'coordinador_nombres') ?>

    <?= $form->field($model, 'coordinador_apellidos') ?>

    <?= $form->field($model, 'coordinador_DNI') ?>

    <?= $form->field($model, 'coordinador_contacto') ?>

    <?php // echo $form->field($model, 'coordinador_correo') ?>

    <?php // echo $form->field($model, 'fecha_inicio_actividad') ?>

    <?php // echo $form->field($model, 'fecha_fin_actividad') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
