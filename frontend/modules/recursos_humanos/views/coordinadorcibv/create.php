<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorCibv */

$this->title = Yii::t('app', 'Crear Coordinador Centro Infantil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="coordinador-cibv-create">

    </br>

    <?=
    $this->render('_form', [
        'model' => $model,
        'listValidCibv' => $listValidCibv
    ])

    ?>

</div>
