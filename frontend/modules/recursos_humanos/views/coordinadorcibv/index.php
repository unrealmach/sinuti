<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\CoordinadorcibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coordinadores centros infantiles');
$this->params['breadcrumbs'][] = $this->title;

?>
</br>
<div class="coordinador-cibv-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if ($rol == 'coordinador-gad' || $rol == 'sysadmin') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-eye-open']) . Yii::t('app', 'Ver Historial'), ['/recursoshumanos/coordinadorcibv/index-inactivos'], ['class' => 'btn btn-info']) ?>
        </p>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'coordinador_id',
                'coordinador_DNI',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                    return $model->coordinador_nombres . " " . $model->coordinador_apellidos;
                }
                ],
                'coordinador_contacto',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]);

        ?>

    <?php elseif ($rol == 'coordinador') : ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'coordinador_id',
                'coordinador_DNI',
                ['attribute' => 'nombre',
                    'value' => function($model) {
                    return $model->coordinador_nombres . " " . $model->coordinador_apellidos;
                }
                ],
                'coordinador_contacto',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}, {update}',
                ],
            ],
        ]);

        ?>
    <?php endif; ?>
</div>
