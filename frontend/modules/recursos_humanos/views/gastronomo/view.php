<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Gastronomo */

$this->title = $model->gastronomo_nombres . " " . $model->gastronomo_apellidos;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gastrónomos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="gastronomo-view" style="margin: 20px 0;">

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1><?= Html::encode($this->title) ?></h1>

            </div>
        </div>

        <div class="panel-body">

            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <div class="row">
                            <?= DetailView::widget([
                                'model' => $model,
                                'attributes' => [
//                        'gastronomo_id',
                                    'gastronomo_dni',
//            'gastronomo_nombres',
//            'gastronomo_apellidos',
                                    'gastronomo_telefono',
                                    'gastronomo_correo',
                                ],
                            ]) ?>
                        </div>

                        <p class="container">
                            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                'onclick' => ' window.history.back();',
                                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                        <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->gastronomo_id], ['class' => 'btn btn-primary']) ?>
                    </p>
</div>
