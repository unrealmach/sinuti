<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Gastronomo */

$this->title = Yii::t('app', 'Crear Gastrónomo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gastrónomos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gastronomo-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
         'modelZonaDistrital' => $modelZonaDistrital,
    ]) ?>

</div>
