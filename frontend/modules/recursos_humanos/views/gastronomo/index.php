<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\GastronomoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Gastrónomos');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="gastronomo-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php if ($rol == 'coordinador-gad' || $rol == 'sysadmin'): ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'gastronomo_id',
                'gastronomo_dni',
                'gastronomo_nombres',
                'gastronomo_apellidos',
                'gastronomo_telefono',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]);
        ?>
    <?php elseif ($rol == 'gastronomo'): ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                    'gastronomo_id',
                'gastronomo_dni',
                'gastronomo_nombres',
                'gastronomo_apellidos',
                'gastronomo_telefono',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}, {update}',
                ],
            ],
        ]);
        ?>
    <?php endif; ?>
</div>
