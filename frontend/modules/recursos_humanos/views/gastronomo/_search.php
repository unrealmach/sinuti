<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\GastronomoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gastronomo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'gastronomo_id') ?>

    <?= $form->field($model, 'gastronomo_dni') ?>

    <?= $form->field($model, 'gastronomo_nombres') ?>

    <?= $form->field($model, 'gastronomo_apellidos') ?>

    <?= $form->field($model, 'gastronomo_telefono') ?>

    <?php // echo $form->field($model, 'fecha_inicio_actividad') ?>

    <?php // echo $form->field($model, 'fecha_fin_actividad') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
