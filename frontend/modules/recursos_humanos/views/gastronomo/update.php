<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Gastronomo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Gastronomo',
]) . ' ' . $model->gastronomo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gastronomos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gastronomo_id, 'url' => ['view', 'id' => $model->gastronomo_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="gastronomo-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
         'modelZonaDistrital' => $modelZonaDistrital,
    ]) ?>

</div>
