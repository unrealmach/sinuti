<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */

$this->title = "Sector asignado para el educador/a - " . $model->educador->nombreCompleto;
;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Educadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="sector-asignado-educador-view" style="margin: 20px 0;">

    <!-- TODO
        validar que los tabs solo se muestren si hay datos
        validar qque el calendario solo permita seleccionar hasta el mes actual
        validar que no se monte el menu sobre el reporte

     -->

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1><?= Html::encode($this->title) ?></h1>

            </div>
        </div>

        <div class="panel-body">

            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">
                        <div class="row">
                            <?=
                            DetailView::widget([
                                'model' => $model,
                                'attributes' => [
//                    'sector_asignado_educador_id',
                                    [
                                        'label' => 'Centro Infantil',
                                        'value' => $model->cenInf->cen_inf_nombre
                                    ],
                                    'periodo.limitesFechasPeriodo',
                                    [
                                        'label' => 'Salon Infantil',
                                        'value' => $model->salon->grupoEdad->grupo_edad_descripcion
                                    ],
                                    'fecha_inicio_actividad',
                                    'fecha_fin_actividad',
                                ],
                            ])
                            ?>
                            <p class="container">
                                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                                    'onclick' => ' window.history.back();',
                                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->sector_asignado_educador_id], ['class' => 'btn btn-primary']) ?>
                            </p>
                        </div>
                    </div>


                </div>


            </div>

        </div>


    </div>
</div>