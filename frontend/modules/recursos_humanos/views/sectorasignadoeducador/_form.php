<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\inscripcion\models\Salon;
use backend\modules\centro_infantil\models\Cibv;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */
/* @var $form yii\widgets\ActiveForm */
?>
</br>
<div class="sector-asignado-educador-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= $model->isNewRecord ? Html::encode(Yii::t('app', 'Registar Sector Asignado Educador')) : Html::encode(Yii::t('app', 'Actualizar Sector Asignado Educador')) ?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map($listCibv, 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-3 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'educador_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Educador::find()->all(), 'educador_id', 'nombreCompleto'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-3 control-label']);
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'salon_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Salon::find()->all(), 'salon_id', 'salon_nombre', 'grupoEdad.grupo_edad_descripcion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => $periodo->fecha_inicio,
                                'endDate' => $periodo->fecha_fin,
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => $periodo->fecha_inicio,
                                'endDate' => $periodo->fecha_fin,
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
            </div>
            <div class="container">
                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Registrar') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
