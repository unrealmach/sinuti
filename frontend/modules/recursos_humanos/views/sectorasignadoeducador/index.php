<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\SectorasignadoeducadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sector Asignado Educadores');
$this->params['breadcrumbs'][] = $this->title;
?>
</br>
<div class="sector-asignado-educador-index">

    <div style="text-align:center; width:100%"> <h1><?= Html::encode($this->title) ?></h1></div>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if ($rol == 'sysadmin') : ?>
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'sector_asignado_educador_id',
            ['attribute' => 'cen_inf_id',
                'value' => 'cenInf.cen_inf_nombre'],
            ['attribute' => 'periodo_id',
                'value' => 'periodo.limitesFechasPeriodo'],
            ['attribute' => 'educador_id',
                'value' => 'educador.nombreCompleto'],
            ['attribute' => 'salon_id',
                'value' => 'salon.grupoEdad.grupo_edad_descripcion'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}, {update}',
            ],
        ],
    ]);
    ?>

</div>
s