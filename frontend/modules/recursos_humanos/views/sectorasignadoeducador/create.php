<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */

$this->title = Yii::t('app', 'Crear Sector Asignado Educador');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Educadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sector-asignado-educador-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
        'listCibv' => $listCibv,
    ]) ?>

</div>
