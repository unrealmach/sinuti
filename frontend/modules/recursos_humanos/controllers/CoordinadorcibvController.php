<?php

namespace frontend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\CoordinadorCibv;
use backend\modules\recursos_humanos\models\CoordinadorcibvSearch;
use backend\modules\recursos_humanos\models\searchs\CoordinadorcibvInactivoSearch;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//use sengrid\lib\SendGrid;

/**
 * CoordinadorcibvController implements the CRUD actions for CoordinadorCibv model.
 */
class CoordinadorcibvController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CoordinadorCibv models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new CoordinadorcibvSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Lists all Educador models.
     * @return mixed
     */
    public function actionIndexInactivos() {
        $searchModel = new CoordinadorcibvInactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('indexInactivos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single CoordinadorCibv model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CoordinadorCibv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new CoordinadorCibv();
        $listValidCibv = Cibv::find()->cibvDisponibles()->all();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo.');
        }
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($authAsiignmentMdel->item_name=="sysadmin") {
            throw new NotFoundHttpException('Debe ingresar como supervissor para asignar coordinadores');
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        
        //\yii\helpers\VarDumper::dump( $model->load(Yii::$app->request->post()),10,true);
        //\yii\helpers\VarDumper::dump( Yii::$app->request->post(),10,true);
        //return $this->redirect(['/centroinfantil/cibv/view', 'id' => "1"]);
        //die("");
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            var_dump($model->trigger(CoordinadorCibv::EVENT_CREATE_NEW_USER_COORDINADOR));
            var_dump($model->trigger(CoordinadorCibv::EVENT_AUTORIDAD_CIBV));
            var_dump($model->trigger(CoordinadorCibv::EVENT_ASIG_USER_COORDINADOR_CIBV));
            
            
            return $this->redirect(['/centroinfantil/cibv/view', 'id' => $model->cibv]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'listValidCibv' => $listValidCibv
            ]);
        }
    }

    /**
     * Updates an existing CoordinadorCibv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        $autoridadCibv = AutoridadesCibv::findOne(['coordinador_id' => $id]);
        $listValidCibv = Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->coordinador_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'listValidCibv' => $listValidCibv,
            ]);
        }
    }

    /**
     * Deletes an existing CoordinadorCibv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CoordinadorCibv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CoordinadorCibv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CoordinadorCibv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
