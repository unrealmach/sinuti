<?php
namespace frontend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\Nutricionista;
use backend\modules\recursos_humanos\models\NutricionistaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NutricionistaController implements the CRUD actions for Nutricionista model.
 */
class NutricionistaController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nutricionista models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NutricionistaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single Nutricionista model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Nutricionista model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nutricionista();
        $modelZonaDistrital = \backend\modules\ubicacion\models\Distrito::find()->all();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->trigger(Nutricionista::EVENT_CREATE_NEW_USER_NUTRICIONISTA);
            $model->trigger(Nutricionista::EVENT_ASIGNAR_NUTRICIONISTA_SECTOR);
            $model->trigger(Nutricionista::EVENT_ASIG_USER_NUTRICIONISTA_DISTRITO);

            return $this->redirect(['view', 'id' => $model->nutricionista_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
                    'modelZonaDistrital' => $modelZonaDistrital,
            ]);
        }
    }

    /**
     * Updates an existing Nutricionista model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        $modelZonaDistrital = \backend\modules\ubicacion\models\Distrito::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nutricionista_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
                    'modelZonaDistrital' => $modelZonaDistrital,
            ]);
        }
    }

    /**
     * Deletes an existing Nutricionista model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nutricionista model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nutricionista the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nutricionista::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
