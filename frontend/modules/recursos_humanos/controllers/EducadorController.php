<?php

namespace frontend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\recursos_humanos\models\EducadorSearch;
use backend\modules\recursos_humanos\models\searchs\EducadorInactivoSearch;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EducadorController implements the CRUD actions for Educador model.
 */
class EducadorController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Educador models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EducadorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Lists all Educador models.
     * @return mixed
     */
    public function actionIndexInactivos() {
        $searchModel = new EducadorInactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('indexInactivos', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single Educador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        $sectorAsignado = SectorAsignadoEducador::find()->deEducadorAndPeriodoActivo($id);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $sectorAsignado,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $dataProvider2 = new \yii\data\ActiveDataProvider();
        $sectorAsignado = SectorAsignadoEducador::find()->deEducadorAndPeriodoActivo($id)->one();
        if (!empty($sectorAsignado)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->infanteEducadorAsignado($sectorAsignado->sector_asignado_educador_id);
            $dataProvider2 = new \yii\data\ActiveDataProvider([
                'query' => $sectorAsignadoInfante,
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]);
        }


        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProvider' => $dataProvider,
                    'dataProvider2' => $dataProvider2,
                    'sector_asig_id' => empty($sectorAsignado) ? null : $sectorAsignado->sector_asignado_educador_id,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new Educador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Educador();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($authAsiignmentMdel->item_name=="sysadmin") {
            throw new NotFoundHttpException('Debe ingresar como coordinador para asignar educadores');
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->trigger(Educador::EVENT_CREATE_NEW_USER_EDUCADOR);

            $model->trigger(Educador::SECTOR_ASIG_EDUCADOR);
            $model->trigger(Educador::ASIG_USER_EDUCADOR_CIBV);


            return $this->redirect(['view', 'id' => $model->educador_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Educador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->educador_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Educador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Educador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Educador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Educador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
