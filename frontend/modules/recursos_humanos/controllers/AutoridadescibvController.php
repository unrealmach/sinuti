<?php

namespace frontend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\recursos_humanos\models\AutoridadescibvSearch;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\recursos_humanos\models\CoordinadorCibv;

//use yii\web\NotFoundHttpException;

/**
 * AutoridadescibvController implements the CRUD actions for Autoridadescibv model.
 */
class AutoridadescibvController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Autoridadescibv models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AutoridadescibvSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Autoridadescibv model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Autoridadescibv model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cen_inf_id = null) {

        $model = new AutoridadesCibv();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo');
        }
        if ($cen_inf_id != null) {
            $validateAutoridadCibv = AutoridadesCibv::find()->deCibvAndPeriodo($cen_inf_id, $periodo->periodo_id)->one();
            if (!empty($validateAutoridadCibv))
                throw new NotFoundHttpException('Ya existe un coordinador asignado para el cibv');
            $model->cen_inf_id = $cen_inf_id;
        }

        if ($model->load(Yii::$app->request->post())) {


            $validateCoordinador = AutoridadesCibv::find()->deCoordinador($model->coordinador_id)->one();
            if (!empty($validateCoordinador))
                throw new NotFoundHttpException('El coordinador ya tiene un cibv asignado');
            if ($model->save()) {
//            Crea la bitacora de los usuarios coordinadores de los cibv
                $coordinador = CoordinadorCibv::findOne($model->coordinador_id);
                $user = \webvimark\modules\UserManagement\models\User::findOne(['email' => $coordinador->coordinador_correo]);
                if (!empty($user)) {
                    AsignacionUserCoordinadorCibv::insertRegistro($user->id, $model->autoridades_cibv_id);
                } else {
                    $model->delete();
                    throw new NotFoundHttpException('No coinciden los emails del usuario y rrhh');
                }
                return $this->redirect(['/centroinfantil/cibv/view', 'id' => $model->cen_inf_id]);
            }
        } else {
            $listValidCoordCibv = CoordinadorCibv::find()->all();
            return $this->render('create', [
                        'model' => $model,
                        'periodo' => $periodo,
                        'listValidCoordCibv' => $listValidCoordCibv
            ]);
        }
    }

    /**
     * Updates an existing Autoridadescibv model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $cen_inf_id = null) {

        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo');
        }

        $listValidCoordCibv = CoordinadorCibv::find()->where(['coordinador_id' => $model->coordinador_id])->all();


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //TODO: Sofy crear la bitacora en la tabla asignacionusercoordinadorcibv
            return $this->redirect(['view', 'id' => $model->autoridades_cibv_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'cen_inf_id' => $cen_inf_id,
                        'periodo' => $periodo,
                        'listValidCoordCibv' => $listValidCoordCibv
            ]);
        }
    }

    /**
     * Deletes an existing Autoridadescibv model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Autoridadescibv model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Autoridadescibv the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Autoridadescibv::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
