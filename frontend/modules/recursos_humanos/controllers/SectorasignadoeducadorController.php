<?php

namespace frontend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\recursos_humanos\models\SectorasignadoeducadorSearch;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\Cibv;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectorasignadoeducadorController implements the CRUD actions for SectorAsignadoEducador model.
 */
class SectorasignadoeducadorController extends Controller {

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                        [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                        [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SectorAsignadoEducador models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new SectorasignadoeducadorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single SectorAsignadoEducador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Creates a new SectorAsignadoEducador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($educador_id = null) {
        $model = new SectorAsignadoEducador();
        $listCibv = [];
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }
        if ($educador_id != null) {
            $model->educador_id = $educador_id;
        }
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($authAsiignmentMdel->item_name == 'coordinador' || $authAsiignmentMdel->item_name == 'educador') {
            $asignacionCoordinador = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            if (!empty($asignacionCoordinador)) {
                $autoridadCibv = AutoridadesCibv::find()->deAutoridadAndPeriodoActivo($asignacionCoordinador->autoridades_cibv_id)->one();
                if (!empty($autoridadCibv)) {
                    $listCibv = Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
                } else {
                    $listCibv = Cibv::find()->all();
                }
            }
        } else if ($authAsiignmentMdel->item_name == 'sysadmin') {
            $listCibv = Cibv::find()->all();
        }
        
        if ($model->load(Yii::$app->request->post())) {
            $validate = SectorAsignadoEducador::find()->deCentroInfantil($model->cen_inf_id, $model->educador_id)->one();
            if (!empty($validate)) {
                throw new NotFoundHttpException('El educador ya tiene un salon asignado para el periodo vigente');
            }
            if ($model->save()) {
                $educador = Educador::findOne(['educador_id' => $model->educador_id]);
                $user = \webvimark\modules\UserManagement\models\User::findOne(['email' => $educador->educador_correo]);
                if (!empty($user)) {
                    AsignacionUserEducadorCibv::insertRegistro($user->id, $model->sector_asignado_educador_id);
                    return $this->redirect(['view', 'id' => $model->sector_asignado_educador_id]);
                } else {
                    $model->delete();
                    throw new NotFoundHttpException('No coinciden los emails del usuario y rrhh');
                }
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'periodo' => $periodo,
                        'listCibv' => $listCibv,
            ]);
        }
    }

    /**
     * Updates an existing SectorAsignadoEducador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un periodo activo');
        }
        $listCibv = [];
//        BUSQUEDA SEARCH RRHH validada
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $asignacionCoordinador = AsignacionUserCoordinadorCibv::find()->deUserId(Yii::$app->user->id)->all();
            if (!empty($asignacionCoordinador)) {
                foreach ($asignacionCoordinador as $value) {
                    $autoridadCibv = AutoridadesCibv::find()->deAutoridadAndPeriodoActivo($value->autoridades_cibv_id)->one();
                    if (!empty($autoridadCibv)) {
                        $listCibv = Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
                    }
                }
            }
        } else {
            $listCibv = Cibv::find()->all();
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            TODO si add fecha fin antes de fin de periodo add a historial
            return $this->redirect(['view', 'id' => $model->sector_asignado_educador_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'periodo' => $periodo,
                        'listCibv' => $listCibv,
            ]);
        }
    }

    /**
     * Deletes an existing SectorAsignadoEducador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SectorAsignadoEducador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SectorAsignadoEducador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SectorAsignadoEducador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
