var tiempo_comida = ['DESAYUNO', 'REFRIGERIO DE LA MAÑANA', 'ALMUERZO', 'REFRIGERIO DE LA TARDE'];
var temp = null;

//TODO: add to update logs
var dynamicFormObj=null;

$(function () {


    var mprepcarta = $("#mprepcarta-m_prep_carta_id").val();
    if (mprepcarta != "") {
        if ($('#mprepcarta-tiempo_comida_id option:selected').val() != "" && $('#mprepcarta-grupo_edad_id option:selected').val() != "") {
            $("#stepwizard_step1_next").removeClass('disabled').addClass('enabled btn btn-default next-step');
            $("#stepwizard_step1_next").show();
        }
    }
    $("#stepwizard_step1_next").on('click', function () {
        if($("#tipoModel").data("tipo") == 'create'){
            if (temp != $('#mprepcarta-tiempo_comida_id option:selected').text() ) {
                temp = $('#mprepcarta-tiempo_comida_id option:selected').text();

                onChangeValidateMax(temp);

            }
        } else {
            temp = $('#mprepcarta-tiempo_comida_id option:selected').text();
            onChangeValidateMax(temp);
        }




    });

    //TODO: add to update logs
    dynamicFormObj= $($("form#MPrepCarta .dynamicform_wrapper").attr('data-dynamicform'));
});


/**
 * Permite cambiar el limite del objetoDynamicForm deacuerdo al tiempo de comida seleccionado
 * @param {type} select tiempo_comida
 * @param {type} objDyamicForm
 * @author Sofia Mejia
 * @returns {undefined}
 */
function onChangeValidateMax(select) {
//    alert("validate");
//    TODO validar el cambio de tiempo de comida para eliminar los detalles y empezar desde cero
//    var dynamic =S "<?= $_SESSION['hashVarObjectDynamicForm']; ?>";


    $.each(tiempo_comida, function (index, value) {

        if (select === value) {

            newLimite = validateLimiteDynamicForm(select);

            if (newLimite[0] != 0) {
                dynamicFormObj.limit = newLimite[0];
                dynamicFormObj.min = newLimite[1];
                console.log(dynamicFormObj);
            }
        }
    });

    //cambia el dynamicForm


    //evento del dynamicform
    if ($("#tipoModel").data("tipo") == 'update') {


        if ($("#tipoModel").data("old_tiempo_comida") == $("#mprepcarta-tiempo_comida_id").val()) {

        } else if ($("#tipoModel").data("old_tiempo_comida") != $("#mprepcarta-tiempo_comida_id").val()) {
            console.log("quitando")
            $(".remove-item").each(function (val) {
                $(".add-item").trigger("click");
                $(this).trigger("click");
                $(".add-item").trigger("click");
                $(this).trigger("click");
            });

        }
    }
    //cuando es create
    if ($("#tipoModel").data("tipo") == 'create') {

        $(".remove-item").each(function (val) {
            console.log("aca")
            $(".add-item").trigger("click");
            $(this).trigger("click");
            $(".add-item").trigger("click");
            $(this).trigger("click");
        });
    }

    // showModalWithData("<center> Contenedor del menú creado</center>");
}

/*
 * Envia el tiempoComida a la sesion para validar los tipos de preparacion
 * @param string nombreTiempo
 * @param string url
 * @author Sofia Mejia
 * @returns {undefined}
 */
function setTiempoTipoPreparacion(nombreTiempo, edad, url) {

    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {
            nombreTiempo: nombreTiempo, edad: edad
        },
        success: function (data) {
            console.log(data);
//            $('#mplatillo-tipo_receta_id').html('');
//
//            $('#mplatillo-tipo_receta_id').select2({data: data});

        }
    });
}

/*
 * Agrega el max y min permitidos en el dynamicForm de acuerdo al tiempoComida seleccionado
 * @param string tiempoComida
 * @author Sofia Mejia
 * @returns {Array}
 */
function validateLimiteDynamicForm(tiempoComida) {
//    console.log("validar: " + tiempoComida);
    if (tiempoComida === 'DESAYUNO') {
        return [3, 2];
    }
    else if (tiempoComida === 'REFRIGERIO DE LA MAÑANA') {
        return [2, 1];
    }
    else if (tiempoComida === 'REFRIGERIO DE LA TARDE') {
        return [2, 1];
    }
    else if (tiempoComida === 'ALMUERZO') {
        return [5, 5];
    } else {
        return [0, 0];
    }
}

/**
 * Valida que exista un tiempo_comida y grupo_edad seleccionados para acivar el siguiente step
 * @param int select
 * @param int select1
 * @author Sofia Mejia
 * @returns {undefined}
 */
function validateSelectedTiempoComida(select, select1) {
//    if ($("#tipoModel").data("tipo") == "CREATE") {
    if (select != "" && select1 != "") {
//        alert("show");
        $("#stepwizard_step1_next").removeClass('disabled').addClass('enabled btn btn-default next-step');
        $("#stepwizard_step1_next").show();
    } else {
//        alert("hide");
        $("#stepwizard_step1_next").removeClass('enabled').addClass('disabled');
        $("#stepwizard_step1_next").hide();
    }
//    }
}

/**
 *
 * @param {type} grupo_platillo_id
 * @param {type} url
 * @author Mauricio Chamorro
 * @returns {undefined}
 */
function llamarlista(grupo_platillo_id, url) {

    grupo_platillo_id = parseInt(grupo_platillo_id);
    $.ajax({    
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {
            id: grupo_platillo_id
        },
        success: function (data) {
            $('#mplatillo-tipo_receta_id').html('');

            $('#mplatillo-tipo_receta_id').select2({data: data});

        }
    });

}

