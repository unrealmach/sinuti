$(function() {
    var anchoPantalla = $(window).width();
//    var  altoPantalla = $(window).heigth();
// height:100%;
//    width:100%;
    $("#pestaña-desplegable a").click(function() {
        var id = $(this).attr("href").substring(1);
        $("html, body").animate({scrollTop: $("#" + id).offset().top}, 1000, function() {
            $("#pestaña-desplegable").slideReveal("hide");
        });
    });

    var slider = $("#pestaña-desplegable").slideReveal({
        push: false,
        position: "right",
        speed: 700,
        top: 60,
//           overlay:true,
        trigger: $(".handle"),
        width: anchoPantalla / 1.5,
//           heigth:altoPantalla,
        // autoEscape: false,
        shown: function(obj) {
            obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-right"></span>');
            obj.addClass("left-shadow-overlay");
        },
        hidden: function(obj) {
            obj.find(".handle").html('<span class="glyphicon glyphicon-chevron-left"></span>');
            obj.removeClass("left-shadow-overlay");
        }
    });
});