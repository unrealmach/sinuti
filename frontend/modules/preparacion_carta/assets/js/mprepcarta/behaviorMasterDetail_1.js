
/*
 * @author Sofía Mejía
 */
//para almacenar todos los detalles del maestro
var urlDetallestotal;
//var das;


/**
 * controla el comportamiento del seletc2row cuando se selecciona un valor
 * select string que contiene el alimento o platillo seleccionado
 * @author Sofia Mejia
 * @returns {undefined}
 */
function onChangeSelect2(select, url) {

//    var posicion = $(select).attr('name').match(/\d+/); // obtiene el id del alimento o platillo seleccionado
    if ($(select).val() !== "") {
        console.log($(select).val());
        getDataAlimentoPlatillo($(select).val(), $(select).attr('name').match(/\d+/), url);
        $('#dpreparacioncarta-' + $(select).attr('name').match(/\d+/) + '-alimento_id').val($(select).select2('data')[0]['iden']); //coloca el id del alimento o platillo en el hiddentext
    }
    else {
        alert("no se selecciono ningun alimento o preparación");
    }
}

/**
 * Recupera la informacion del alimento o platillo seleccionado con select2
 * idalimentoplatillo int el id de el alimento o platillo seleccionado
 * @author Sofia Mejia
 * @returns {undefined}
 */
function getDataAlimentoPlatillo(idalimentoplatillo, numeroItem, url) {
    var numero = parseInt(numeroItem)
    var cantidad = $("input[id= dpreparacioncarta-" + numeroItem + "-d_prep_carta_cantidad_g").val() !== "" ? $("input[id= dpreparacioncarta-" + numeroItem + "-d_prep_carta_cantidad_g").val() : 0;
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {idalimentoplatillo: JSON.stringify(idalimentoplatillo),
            posicion: JSON.stringify(numero),
            cantidad: JSON.stringify(cantidad),
        },
        success: function(data) {
            changeDataTotal();
        }
    });
}

/**
 * Comportamiento cuando cambia la cantidad de el alimento o platillo 
 * @param Object object que contiene el detalle seleccionado
 * @param stirng url
 * @author Sofia Mejia
 * @returns {undefined}
 */
function onChangeCantidad(object, url) {
//    var number = $(object).attr('name').match(/\d+/); // obtiene el id de el alimento o platillo seleccionado
    cambiarCantidad($(object).attr('name').match(/\d+/), url);
}

/**
 * Actualiza el valor del objeto de sesion con la nueva cantidad
 * @param number int el id del alimento o platillo seleccionado
 * @param stirng url
 * @author Sofia Mejia
 * @returns {undefined}
 */
function cambiarCantidad(number, url) {
    var alimentoplatillo_id = $("select[id=dpreparacioncarta-" + number + "-alimento_identificador").val();
    var cantidad = $("input[id= dpreparacioncarta-" + number + "-d_prep_carta_cantidad_g").val();
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {
            posicion: JSON.stringify(number),
            alimentoplatillo_id: JSON.stringify(alimentoplatillo_id),
            cantidad: cantidad,
        },
        success: function(data) {
            console.log(data);
            changeDataTotal();
        }
    });
}


//categ='';
/**
 * Permite cargar todos los detalles de la sesion con su respectiva composicion
 * para mostrar la gráfica
 * @author Sofia Mejia
 * @returns {undefined}
 */
function changeDataTotal() {
    console.log("cambiando datos totales")
    $.ajax({
        beforeSend: function() {
            $("#pestaña-desplegable div span").removeClass('glyphicon-chevron-left ').addClass('fa fa-spinner fa-spin');

        },
        url: urlDetallestotal,
        dataType: 'json',
        method: 'GET',
        success: function(data) {
            var cantidadTotal = data.mprepcarta_cantidad_togal_g == 0 ? "" : data.mprepcarta_cantidad_togal_g;
            $("#mprepcarta-cantidad_togal_g").val(cantidadTotal);


//            categ=data.dataHighchart.categories;
            optionsChart_w0.xAxis[0].categories = data.dataHighchart.categories;
            optionsChart_w0.series[0].data = data.dataHighchart.data.compos_total;
            optionsChart_w0.series[0].name = "cantidad del menú";
            optionsChart_w0.series[0].type = "column";

            optionsChart_w0.series[1].data = data.dataHighchart.data.serie_min;
            optionsChart_w0.series[1].name = "valor min";
            optionsChart_w0.series[2].data = data.dataHighchart.data.serie_max;
            optionsChart_w0.series[2].name = "valor máximo";

            new Highcharts.Chart(optionsChart_w0);
            $("#pestaña-desplegable div span").removeClass('fa fa-spinner fa-spin').addClass('glyphicon-chevron-left');
        }
    });
}

/**alimenta al objeto de sesion en donde se acumulan los objetos ya seleccionados
 * para realizar una busqueda en los alimentos y platillos exceptuando los preselecionados
 * @param stirng url
 * @author Sofia Mejia
 */
function alimentoPlatilloSeleccionado(url) {
    var alimentoPlatilloSeleccionado = $('select[id$=alimento_identificador]');
    var datosEnviar = [];
    $.each(alimentoPlatilloSeleccionado, function(index, value) {
        var aux = $(value).val()
        datosEnviar.push(aux);
    }) // indica los alimentos o platillos que ya se encuentran seleccionados
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {data: JSON.stringify(datosEnviar),
        },
        success: function(data) {
        }
    });
}
/**se comunica con el objeto de sesion para indicar que se elimino un detalle
 * @param stirng url
 * @param Object object del detalle seleccionado
 * @author Sofia Mejia
 * @returns {undefined}
 */
function alimentoPlatilloEliminado(url, object) {
    var posicion = $(object).parent().siblings('td').first().find('select').attr('id').match(/\d+/)[0];
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {posicionEliminado: JSON.stringify(posicion),
        },
        success: function(data) {
            changeDataTotal();
        }
    });

}

/**para el evento onkeypress del input cantidad
 * no permite que se teclee mas q numeros
 * @param string ingreso
 * @author Sofia Mejia
 * @returns {undefined}
 */

function validateOnlyNumbers(ingreso) {
    $(ingreso).keydown(function(e)
    {
        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
        // home, end, period, and numpad decimal
        console.log(key);
        return (
                key == 8 ||
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
    });

}