<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorMasterDetail.js', ['depends' => [frontend\assets\AppAsset::className()]]);

?>

</br>

<div class="panel">
    <div class="mplatillo-form ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <center>
                    <h1 style="color: black !important;padding-right: 25px;">
                        <?= Html::encode(Yii::t('app', 'Menú')) ?>
                    </h1>
                </center>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['id' => $model->formName(),
                    'enableAjaxValidation' => true,
                    'enableClientScript' => true,
                    'enableClientValidation' => true,
                    'options'=>[
                        'style'=>'margin: 0px !important;'
                    ]]); ?>
                <?php
                $wizard_config = [
                    'id' => 'stepwizard',
                    'steps' => [
                        1 => [
                            'title' => 'Información básica',
                            'icon' => 'glyphicon glyphicon-list ',
                            'content' => $this->render('partials/_step1', ['model' => $model, 'form' => $form]),
                            'buttons' => [
                                'next' => [
                                    'title' => Yii::t('app', 'Next'),
                                    'options' => [
                                        'id' => 'nobtn',
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                            ],
                        ],
                        2 => [
                            'title' => 'Detalle del menú',
                            'icon' => 'glyphicon glyphicon-apple',
                            'content' => $this->render('partials/_step2', ['model' => $model, 'd_model' => $d_model, 'form' => $form]),
                            'buttons' => [
                                'save' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],
                                'prev' => [
                                    'options' => [
                                        'class' => 'disabled',
                                        'hidden' => 'hidden',
                                    ],
                                ],

                            ],
                        ],
//                        3 => [
//                            'title' => 'Step 3',
//                            'icon' => 'glyphicon glyphicon-transfer',
//                            'content' => $this->render('partials/_step3', ['model' => $model, 'form' => $form]),
//                        ],
                    ],
//                    'complete_content' => "You are done!", // Optional final screen
                    'start_step' => 1, // Optional, start with a specific step
                ];

                ?>
                <?= drsdre\wizardwidget\WizardWidget::widget($wizard_config); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>

<div>
    <div id="pestaña-desplegable" title="Ver gráfica estadística">
        <div class="handle">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </div>
        <div id="contentSlider" style="height:80%; width:100%; overflow: auto">
            <?=
            \common\components\fixWidget\HighCharts::widget([
                'clientOptions' => [
                    'chart' => [
                        'zoomType' => 'xy',
                        'renderTo' => "contentSlider"
                    ],
                    'title' => [
                        'text' => 'Composición nutricional menú'
                    ],
                    'subtitle' => [
                        'text' => 'Datos obtenidos de la tabla de alimentos de Ecuador'
                    ],
                    'xAxis' => [[
                        'categories' =>
                            [
//                            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
                            ],
                        'crosshair' => true
                    ]],
                    'yAxis' => [[// Primary yAxis
                        'labels' => [
                            'overflow' => 'justify',
                            'style' => [
                                'color' => 'Highcharts.getOptions().colors[2]'
                            ]
                        ],
                        'title' => [
                            'text' => 'Cantidad',
//                            'style' => [
//                                'color' => 'Highcharts.getOptions().colors[2]'
//                            ]
                        ],
//                        'opposite' => true
                    ],
                    ],
                    'tooltip' => [
                        'shared' => true
                    ],
                    'legend' => [
                        'layout' => 'vertical',
                        'align' => 'left',
                        'x' => 80,
                        'verticalAlign' => 'top',
                        'y' => 55,
                        'floating' => true,
                        'enabled' => false,
                        'backgroundColor' => "#ffffff !important"
                    ],
                    'series' => [
                        [
                            'name' => '',
                            'data' => [0]
                        ],
                        [
                            'name' => ' ',
                            'data' => [0]
                        ],
                        [
                            'name' => ' ',
                            'data' => [0]
                        ]
                    ]
                ]]);

            ?>
        </div>
    </div>
</div>