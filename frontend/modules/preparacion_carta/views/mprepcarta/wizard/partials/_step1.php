<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use backend\modules\preparacion_carta\models\TiempoComida;
use backend\modules\metricas_individuo\models\GrupoEdad;
use yii\helpers\ArrayHelper;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/step1_behavior.js', ['depends' => [frontend\assets\AppAsset::className()]]);

//setTiempoTipoPreparacion
$urlAjaxJoinTiempoTipo = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-set-tiempo-tipo');
?>
    <div id="tipoModel"
         data-old_edad="<?= $model->isNewRecord ? 'null' : $model['grupo_edad_id'] ?>"
         data-old_tiempo_comida="<?= $model->isNewRecord ? 'null' : $model['tiempo_comida_id'] ?>"
         data-tipo="<?= $model->isNewRecord ? 'create' : 'update' ?>"
         tipo="<?= $model->isNewRecord ? 'create' : 'update' ?>"></div>
    <!--inicio maestro-->
    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;padding-right: 25px;">
                    <?= Html::encode(Yii::t('app', 'Información Básica')) ?>
                </h1>
            </center>
        </div>
        <div class="panel-body">
            <?= Html::activeHiddenInput($model, "m_prep_carta_id"); ?>
            <div class="row">
                <div class="col-sm-4">
                    <?=
                    $form->field($model, 'm_prep_carta_nombre', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
                <div class="col-sm-4">

                    <?php
                    $tiempos_comidas = ArrayHelper::map(TiempoComida::find()->all(), 'tiem_com_id', 'tiem_com_nombre');

                    foreach ($tiempos_comidas as $key => &$value) {
                        $value = str_replace("_", " ", $value);
                    }

                    echo $form->field($model, 'tiempo_comida_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => $tiempos_comidas,
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione tiempo de comida', 'class' => 'col-sm-12',
                            'onchange' =>
//            "var objDyamicForm = eval('" . Yii::$app->session->get('hashVarObjectDynamicForm') . "');"
//            "var objDyamicForm = dynamicFormObj"
//             "onChangeValidateMax($('#mprepcarta-tiempo_comida_id option:selected').text() );"
                                "setTiempoTipoPreparacion($('#mprepcarta-tiempo_comida_id option:selected').text(),  $('#mprepcarta-grupo_edad_id option:selected').val(),  urlAjaxJoinTiempoTipo );"
                                . "validateSelectedTiempoComida($('#mprepcarta-tiempo_comida_id option:selected').val(), $('#mprepcarta-grupo_edad_id option:selected').val());"
                            ,
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>
                <div class="col-sm-4">

                    <?=
                    $form->field($model, 'grupo_edad_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(GrupoEdad::find()->all(), 'grupo_edad_id', 'grupo_edad_descripcion'),
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione grupo de edad', 'class' => 'col-sm-12',
                            'onchange' =>
                                "setTiempoTipoPreparacion($('#mprepcarta-tiempo_comida_id option:selected').text(),  $('#mprepcarta-grupo_edad_id option:selected').val(),  urlAjaxJoinTiempoTipo );"
                                . "validateSelectedTiempoComida($('#mprepcarta-tiempo_comida_id option:selected').val(), $('#mprepcarta-grupo_edad_id option:selected').val());",
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);
                    ?>
                </div>

            </div>

            <div class="row">
                <div class="col-sm-12">
                    <?=
                    $form->field($model, 'm_prep_carta_descripcion', [
                        'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->textarea(['rows' => 6,'style'=>'text-transform: uppercase'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])
                    ?>
                </div>
            </div>


            <?=
            Html::activeHiddenInput($model, "cantidad_togal_g");
            ?>
            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-right']) . Yii::t('app', 'Next'), [

                        'class' => 'btn btn-default next-step',
                        'style'=>'display: none;',
                        'id' => 'stepwizard_step1_next']) ?>

                </div>


            </div>
        </div>
    </div>
    <!--fin maestro-->
<?php
$script = <<< JS
   $(function(){
   urlAjaxJoinTiempoTipo= '{$urlAjaxJoinTiempoTipo}';     
   
   });
JS;
$this->registerJs($script);
