<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use backend\modules\nutricion\models\Alimento;
use backend\modules\nutricion\models\AlimentoSearch;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\preparacion_platillo\models\MplatilloSearch;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use frontend\assets\SlideAsset;
use backend\modules\preparacion_carta\models\TipoPreparacionCarta;

SlideAsset::register($this); //para registrar el slider

$webPath = Yii::$app->Util->getPublishAssetDirectory();
//$this->registerJsFile($webPath . '/behaviorMasterDetail.js', ['depends' => [frontend\assets\AppAsset::className()]]);
$this->registerJsFile($webPath . '/behaviorMasterDetail_1.js', ['depends' => [frontend\assets\AppAsset::className()]]);

$urlAjaxAlimentoPlatilloSeleccionado = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-alimento-seleccionado');
$urlAjaxAlimentoPlatilloList = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-alimento-list');
$urlDetalles = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-generar-data-maestro');
$urlEliminarDetalle = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-eliminar-detalle-alimento');
$urlChangeCantidad = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-cambiar-cantidad');
$urlAjaxGetAlimentoPlatillo = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-get-alimento');
$urlAjaxJoinTiempoTipo = \Yii::$app->urlManager->createUrl('admin/preparacion_carta/mprepcarta/ajax-join-tiempo-tipo');

///* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="">
        <center>
            <h2 style="color:black !important">
                <?= Html::encode("Preparaciones y alimentos") ?>
            </h2>
        </center>

        <!--inicio detalle-->
        <div id="">
            <?php
            DynamicFormWidget::begin([
                
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // indica la clase con la cual se identificara a cada item
                'limit' => 5, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // indica la clase con la cual se identificara a los botones de insercion
                'deleteButton' => '.remove-item', // indica la clase con la cual se identificara a los botones de borradp
                'model' => $d_model[0],
                'formId' => $model->formName(), // id del formulario en donde se crearan los objetos dinamicamente, es el mismo que el form maestro
                'formFields' => [// los campos que apareceran en el detalle
                    'tipo_preparacion_id',
                    'alimento_id',
                    'm_platillo_id',
                    'd_prep_carta_cantidad_g'
                ],
            ]);
            ?>
            <table border="1 px" class="container-items" style="width: 100%;">
                <tr>
                    <th style=" width: 25%">
                        <center>  <?= Html::encode("Tipo ") ?></center>
                    </th>
                    <th style="">
                        <center>  <?= Html::encode("Alimento") ?></center>
                    </th>
                    <th style=" width: 20%">
                        <center> <?= Html::encode("Cantidad [g] ") ?></center>
                    </th>
                    <th>
                        <div style="">
                            <button
                                    class=" col-sm-2 add-item btn radius btn-info btn-sm"
                                    onclick=" var urlAjaxIngredienteSeleccionado = '<?= $urlAjaxAlimentoPlatilloSeleccionado ?>';
                                            alimentoPlatilloSeleccionado(urlAjaxIngredienteSeleccionado);" type="button"
                                    id="btn_add_item"><i class="glyphicon glyphicon-plus"></i>
                            </button>
                            <!--<button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>-->
                        </div>
                    </th>
                </tr>

                <?php foreach ($d_model as $i => $detalle): ?>

                    <tr class=" item " style="background-color: aliceblue; border: solid 1px black;">
                        <?php
                        // necessary for update action.
                        if (!$detalle->isNewRecord) {
                            echo Html::activeHiddenInput($detalle, "[{$i}]d_prep_carta_id");
                        }
                        ?>
                        <td style="padding-top: 10px" align="center" valign="middle">
                            <?=
                            $form->field($detalle, "[{$i}]tipo_preparacion_id", [
                                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>',
                            ])
                                ->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(TipoPreparacionCarta::find()->all(), 'tipo_prep_carta_id', 'tipo_prep_carta_nombre'),
                                    'language' => 'es',
                                    'options' => ['placeholder' => 'Seleccione grupo de edad', 'class' => 'col-sm-2',],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'ajax' => [
                                            'url' => $urlAjaxJoinTiempoTipo,
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression('function(alimento_platillo) { return alimento_platillo.text; }'),
                                        'templateSelection' => new JsExpression('function (alimento_platillo) { return alimento_platillo.text; }'),
                                    ],
                                ])
                                ->label(FALSE)
                            ?>
                        </td>

                        <td style="padding-top: 10px" align="center" valign="middle"
                            onclick="
                                    var urlAjaxIngredienteSeleccionado = '<?= $urlAjaxAlimentoPlatilloSeleccionado ?>';
                                    alimentoPlatilloSeleccionado(urlAjaxIngredienteSeleccionado);"
                        >
                            <?php

                            if (!$detalle->isNewRecord) {
                                $initValueUpdate = backend\modules\preparacion_carta\models\MPrepCarta::getNombreDetalle($detalle->alimento_identificador);


                            } else {
                                $initValueUpdate = '';
                            }

                            echo $form->field($detalle, "[{$i}]alimento_identificador")->widget(Select2::classname(), [
                                'initValueText' => $initValueUpdate,
                                'language' => 'es',
//                                        'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Seleccione ingrediente', 'class' => 'col-sm-3',
                                    'onchange' =>
                                        "onChangeSelect2(this,url1);"
                                        . " alimentoPlatilloSeleccionado(url2);",
                                ],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'ajax' => [
                                        'url' => $urlAjaxAlimentoPlatilloList,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(alimento_platillo) { return alimento_platillo.text; }'),
                                    'templateSelection' => new JsExpression('function (alimento_platillo) { return alimento_platillo.text; }'),
                                ],
                            ])->label(FALSE);
                            ?>

                            <?= $form->field($detalle, "[{$i}]alimento_id")->hiddenInput()->label(FALSE) ?>
                        </td>
                        <td style="padding-top: 10px" align="center" valign="middle">
                            <?=
                            $form->field($detalle, "[{$i}]d_prep_carta_cantidad_g", [
                                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>',
                            ])
                                ->input('number', ['readonly' => false, 'onKeyPress' => 'validateOnlyNumbers(this)',
                                    'onChange' =>
                                        " onChangeCantidad(this,url);"
                                    , 'min' => 0.01, 'step' => 0.01,])
                                ->label(FALSE)
                            ?>
                        </td>
                        <td style="padding-top: 10px;">
                            <!--<div style="">-->
                            <!--<button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>-->
                            <button type="button"
                                    onclick="
                                            var url2 = '<?= $urlAjaxAlimentoPlatilloSeleccionado ?>';
                                            var url3 = '<?= $urlEliminarDetalle ?>';
                                            alimentoPlatilloSeleccionado(url2);
                                            alimentoPlatilloEliminado(url3, this);

                                            "
                                    class="col-sm-2 remove-item btn  radius btn-primary btn-sm"
                            ><i class="glyphicon glyphicon-minus"></i>
                            </button>
                            <!--</div>-->
                        </td>
                    </tr>

                <?php endforeach; ?>
            </table>
            <?php DynamicFormWidget::end(); ?>
        </div>
        <!--fin detalle-->
    </div>


    <div class="container">

        <div class="form-group">
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                'onclick' => ' window.history.back();',
                'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
            <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-chevron-circle-left']) . Yii::t('app', 'Previous'), [

                'class' => 'btn btn-default prev-step',
                'id' => 'stepwizard_step2_prev']) ?>


            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-md btn-hvr hvr-radial-out' : 'btn btn-success btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>

        </div>


    </div>


<?php
$parametrosExtras = $model->isNewRecord ? '?submit=true' : '&submit=true';

$script = <<< JS
     $(function(){
        urlDetallestotal= '{$urlDetalles}';
        url= '{$urlChangeCantidad}';
        url1= '{$urlAjaxGetAlimentoPlatillo}';
        url2= '{$urlAjaxAlimentoPlatilloSeleccionado}';
        });


$("form#{$model->formName()} button[type='submit']").on('click', function(e){
  
            var r = confirm("¿Esta seguro que desea guardar este menú?");
           
           if (r == true) {
                var \$form= $("form#{$model->formName()}");
           $.ajax({
                beforeSend:function(){
                    //fuerza a que se muestren los errores en el formulario 
                     $(\$form).submit();
                    showModalLoading();
                    
                },
                type:"POST",
                url:\$form.attr("action")+"{$parametrosExtras}",  //envia la accion del formulario osea el action
                data:\$form.serialize(), //envia el formulario serializado 
                 //TODO: add to update logs
                 success:function(result){
                    
                     if(result.status==true){
                         closeModal();
                     }else{
                         showFlashAlert(result.errors);
                         closeModal();
                         }
                    }
                });
           } else {
               return false;
           }
    
          
    return false;
});

    
JS;
$this->registerJs($script);
