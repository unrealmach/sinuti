<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use backend\modules\nutricion\models\Alimento;
use backend\modules\nutricion\models\AlimentoSearch;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MPlatillo */
/* @var $form yii\widgets\ActiveForm */

?>
<?=
$form->field($model, 'm_platillo_nombre')
    ->textarea(['rows' => 6,'style'=>'text-transform: uppercase'])

?>

<?=
$form->field($model, 'm_platillo_descripcion')->textarea(['rows' => 6,'style'=>'text-transform: uppercase'])

?>

<?=
    $form->field($model, 'm_platillo_cantidad_total_g', [
        'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(['placeholder' => 'Ingrese ...','style'=>'text-transform: uppercase'])
    ->label(null, ['class' => 'col-sm-3 control-label'])

?>

<div class="form-group">
    <center>       
        <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
    </center>
</div>

<div class="row">
</div>