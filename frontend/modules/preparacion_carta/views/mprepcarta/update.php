<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MPrepCarta */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Mprep Carta',
]) . ' ' . $model->m_prep_carta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Actualiza Menú'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->m_prep_carta_id, 'url' => ['view', 'id' => $model->m_prep_carta_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="mprep-carta-update">

   </br>
    <?= $this->render('wizard/_form', [
        'model' => $model,
        'd_model' =>$d_model,
    ]) ?>

</div>
