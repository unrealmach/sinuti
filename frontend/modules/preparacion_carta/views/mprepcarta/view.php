<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MPrepCarta */

$this->title =Yii::t('app', 'Menú '). $model->m_prep_carta_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Menús '), 'url' => ['index']];
$this->params['breadcrumbs'][] =  $this->title;

?>


</br>

    <div class="mprep-carta-view" style="margin: 20px 0;">
    <?php if (!$isModal) { ?>

    <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 style="color: black !important;text-align:center; width:100%"><?= Html::encode($this->title) ?></h1>

            </div>
        <div class="panel-body">
            <div class="horizontal">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#vtab1" role="tab" data-toggle="tab"><i class="fa fa-book pr-10"></i>
                            Información básica</a></li>
                    <li><a href="#vtab2" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Detalle del menú</a></li>
                    <li><a href="#vtab3" role="tab" data-toggle="tab"><i class="	fa fa-address-card-o pr-10"></i>
                            Gráfico de barras</a></li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="width: 100%;">
                    <div class="tab-pane fade in active" id="vtab1">

                        <?=
                        DetailView::widget([
                            'model' => $model,
                            'attributes' => [
//                        'm_prep_carta_id',
//                    'm_prep_carta_nombre',
                                'm_prep_carta_descripcion:ntext',
                                [
                                    'label' => 'tiempo Comida',
                                    'value' => $model->tiempoComida->tiem_com_nombre
                                ],
                                [
                                    'label' => 'Grupo Edad',
                                    'value' => $model->grupoEdad->grupo_edad_descripcion
                                ],
//                    'grupoEdad.grupo_edad_descripcion',
                            ],
                        ])

                        ?>
                    </div>
                    <div class="tab-pane fade" id="vtab2">
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'label' => 'Tipo',
                                    'value' => 'tipoPreparacion.tipo_prep_carta_nombre'
                                ],
                                'd_prep_carta_cantidad_g',
                                [
                                    'label' => 'Alimento',
                                    'value' => 'alimento.alimento_nombre'
                                ],
                                [
                                    'label' => 'Preparación',
                                    'value' => 'mPlatillo.m_platillo_nombre'
                                ],
                            ],
                        ]);

                        ?>



                    </div>
                    <div class="tab-pane fade" id="vtab3">
                        <?php
                        $categories = array_keys($composicionTotal);
                        $categoriesJson = json_encode(array_keys($composicionTotal));
                        $valueSerie = array_values($composicionTotal);
                        $valueSerieJson = json_encode(array_values($composicionTotal));

                        ?>
                        <div id="grafica">
                            <?=
                            \common\components\fixWidget\HighCharts::widget([
                                'clientOptions' => [
                                    'chart' => [
                                        'zoomType' => 'xy',
                                        'renderTo' => "grafica"
                                    ],
                                    'title' => [
                                        'text' => 'Composición nutricional menú'
                                    ],
                                    'subtitle' => [
                                        'text' => 'Datos obtenidos de la tabla de alimentos de Ecuador'
                                    ],
                                    'xAxis' => [[
                                        'categories' => $categories,
                                        'crosshair' => true
                                    ]],
                                    'yAxis' => [[// Primary yAxis
                                        'labels' => [
                                            'overflow' => 'justify',
                                            'style' => [
                                                'color' => 'Highcharts.getOptions().colors[2]'
                                            ]
                                        ],
                                        'title' => [
                                            'text' => 'Cantidad',
//                            'style' => [
//                                'color' => 'Highcharts.getOptions().colors[2]'
//                            ]
                                        ],
//                        'opposite' => true
                                    ],
                                    ],
                                    'tooltip' => [
                                        'shared' => true
                                    ],
                                    'legend' => [
                                        'layout' => 'vertical',
                                        'align' => 'left',
                                        'x' => 80,
                                        'verticalAlign' => 'top',
                                        'y' => 55,
                                        'floating' => true,
                                        'enabled' => false,
                                        'backgroundColor' => "#ffffff !important"
                                    ],
                                    'series' => [
                                        [
                                            'name' => 'cantidad',
                                            'data' => $valueSerie,
                                            'type' => 'column'
                                        ],
                                    ]
                                ]]);

                            ?>
                        </div>
                        <?php if ($isModal) { ?>

                            <button id="btn-calcula-grafica" class="btn btn-success" > Mostrar Gráfica </button>
                            <div id="datagrafica" data-categories='<?= $categoriesJson ?>' data-serie='<?= $valueSerieJson ?>' >

                            </div>
                        <?php } ?>
                    </div>



                </div>


            </div>


            <p class="container">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>

                <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->m_prep_carta_id], ['class' => 'btn btn-primary']) ?>

            </p>
        </div>
    </div>



