<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\preparacion_carta\models\MprepcartaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Lista de Menús');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mprep-carta-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php if ($rol == 'nutricion' || $rol == 'Admin') :?>
    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-default']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'm_prep_carta_id',
            'm_prep_carta_nombre',
            'm_prep_carta_descripcion:ntext',
            ['attribute' => 'tiempo_comida_id',
                'value' => 'tiempoComida.tiem_com_nombre'],
            ['attribute' => 'grupo_edad_id',
                'value' => 'grupoEdad.grupo_edad_descripcion'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}, {update}',
            ],
        ],
    ]);
    ?>
    <?php elseif($rol == 'coordinador-gad' || $rol == 'gastronomo'):?>
     <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'm_prep_carta_id',
            'm_prep_carta_nombre',
            'm_prep_carta_descripcion:ntext',
            ['attribute' => 'tiempo_comida_id',
                'value' => 'tiempoComida.tiem_com_nombre'],
            ['attribute' => 'grupo_edad_id',
                'value' => 'grupoEdad.grupo_edad_descripcion'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>
    <?php elseif($rol == 'Admin' || $rol == 'sysadmin'):?>
     <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
     <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'm_prep_carta_id',
            'm_prep_carta_nombre',
            'm_prep_carta_descripcion:ntext',
            ['attribute' => 'tiempo_comida_id',
                'value' => 'tiempoComida.tiem_com_nombre'],
            ['attribute' => 'grupo_edad_id',
                'value' => 'grupoEdad.grupo_edad_descripcion'],
            [
                'class' => 'yii\grid\ActionColumn',
//                'template' => '{view}',
            ],
        ],
    ]);
    ?>
    <?php endif; ?>
</div>

