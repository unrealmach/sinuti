<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MPrepCarta */

$this->title = $model->m_prep_carta_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mprep Cartas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="mprep-carta-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>


    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'm_prep_carta_id',
                    'm_prep_carta_nombre',
                    'm_prep_carta_descripcion:ntext',
                    'tiempo_comida_id',
                    'grupo_edad_id',
                ],
            ])

            ?>
        </div>
    </div>
</div>
