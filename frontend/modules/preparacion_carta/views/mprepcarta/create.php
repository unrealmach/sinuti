<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MPrepCarta */

$this->title = Yii::t('app', 'Crear Menú');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lista de Menús'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mprep-carta-create">

    </br>

    <?= $this->render('wizard/_form', [
        'model' => $model,
        'd_model' => $d_model,
    ]) ?>

</div>
