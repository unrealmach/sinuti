<?php

namespace frontend\modules\preparacion_carta;

class PreparacionCarta extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\preparacion_carta\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
