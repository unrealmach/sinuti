<?php

namespace frontend\modules\preparacion_carta\controllers;

use Yii;
use backend\modules\preparacion_carta\models\MPrepCarta;
use backend\modules\preparacion_carta\models\MprepcartaSearch;
use backend\modules\carta_semanal\models\DCartaSemanal;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\recursos\DynamicFormModel;
use backend\modules\preparacion_carta\models\recursos\PreparacionCartaSession;
use backend\modules\preparacion_carta\models\DPreparacionCarta;
use backend\modules\parametros_sistema\models\Periodo;
use yii\helpers\ArrayHelper;

use \backend\modules\preparacion_carta\controllers\MprepcartaController as BackMprepcartaController;

/**
 * MprepcartaController implements the CRUD actions for MPrepCarta model.
 */
class MprepcartaController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MPrepCarta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MprepcartaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'rol' => $authAsiignmentMdel->item_name,
        ]);
    }

    /**
     * Displays a single MPrepCarta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $d_model = DPreparacionCarta::find()->where(['m_prep_carta_id' => $id]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $d_model,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        Yii::$app->session->remove('alimentoPlatilloSeleccionado');
        Yii::$app->session->set('preparacionCartaSession', new PreparacionCartaSession());
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->updateSesion($model);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'composicionTotal' => $sesionDetalle->maestro['composicionTotal'],
            'isModal' => FALSE,
        ]);
    }

    /**
     * Para renderizar en el modal los ingredientes de esa carta
     * @param type $carta_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return type
     */
    public function actionAjaxViewAllIngredientes($carta_id)
    {
        $id = $carta_id;

        $model = $this->findModel($id);
        $d_model = DPreparacionCarta::find()->where(['m_prep_carta_id' => $id]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $d_model,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        Yii::$app->session->remove('alimentoPlatilloSeleccionado');
        Yii::$app->session->set('preparacionCartaSession', new PreparacionCartaSession());
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->updateSesion($model);

        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'composicionTotal' => $sesionDetalle->maestro['composicionTotal'],
            'isModal' => TRUE,
        ]);


//        return $this->renderPartial('view', [
//                'model' => $this->findModel($carta_id),
//        ]);
    }

    protected function validateMasterDetailsModels($model, $d_model, $request)
    {

        if ($request->isAjax && $model->load($request->post())) {
            $d_model = DynamicFormModel::createMultiple(DPreparacionCarta::className());
            DynamicFormModel::loadMultiple($d_model, \Yii::$app->request->post());
            $validate = $model->validate();

            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;
            if (!$validate) {


                return ['status' => $validate,
                    'errors' => \Yii::$app->Util->getTxtErrorsFromMasterDetailsModels($model, $d_model)

                ];
            }
        }
        return ['status' => TRUE,
            'errors' => ''

        ];
    }

    /**
     * Creates a new MPrepCarta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($submit = false)
    {
        $model = new MPrepCarta();
        $d_model = [new DPreparacionCarta];
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo. Por favor comuniquese con el coordinador del GAD');
        }

        $result = $this->validateMasterDetailsModels($model, $d_model, \Yii::$app->request);

        if (!$result['status']) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }


        if ($submit == true && $model->load(Yii::$app->request->post())) {

            //crea el objeto de multiples detalles
            $d_model = DynamicFormModel::createMultiple(DPreparacionCarta::className());
            DynamicFormModel::loadMultiple($d_model, Yii::$app->request->post());
            $validate = $model->validate();
            $validate = DynamicFormModel::validateMultiple($d_model) && $validate;
            if ($validate) {

                $result = $this->procedureSave($model,$d_model);

                if(!$result['status']){
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $result;
                }
            }
        } else if (!Yii::$app->request->isAjax) {

            Yii::$app->session->remove('alimentoPlatilloSeleccionado');
            Yii::$app->session->set('preparacionCartaSession', new PreparacionCartaSession());
            return $this->render('create', [
                'model' => $model,
                'd_model' => (empty($d_model)) ? [new DPreparacionCarta] : $d_model
            ]);
        }
    }


    private function procedureSave($model,$d_model,$toDeleteIds=null){

//                Instancia una transacción
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if(!is_null($toDeleteIds)){
                DPreparacionCarta::deleteAll(['d_prep_carta_id'=>$toDeleteIds]);
            }
//                    Se valida el padre con todos sus atributos, pero aun no esta guardado en la bd
            if ($flag = $model->save(false)) {
                foreach ($d_model as $detalle) {
//                            agrega el id del padre a cada detalle
                    $detalle->m_prep_carta_id = $model->m_prep_carta_id;
                    if ($detalle->alimento_id) {
                        $aux = $model->validateDetalleAlimento($detalle->alimento_id);
                        if ($aux[0] == "alimento_id") {
                            $detalle->alimento_id = $aux[1];
                            $detalle->m_platillo_id = null;
                        } else {
                            $detalle->alimento_id = null;
                            $detalle->m_platillo_id = $aux[1];
                        }
                    }

//                            Valida la consistencia del detalle
                    if (!($flag = $detalle->save(false))) {
//                                Regresa la transaccion a su estado inicial
                        $transaction->rollBack();
                        break;
                    }
                }
            }
            if ($flag) {
//                        Si el maestro y los detalles se validan exitosamente termina la transaccion
                $transaction->commit();
                Yii::$app->session->remove('alimentoPlatilloSeleccionado');


                return $this->redirect(['view', 'id' => $model->m_prep_carta_id]);
            } else {

                return [
                    'status' => FALSE,
                    'errors' => 'Inconsistencia detectada en los datos',
                ];
            }
        } catch (Exception $exc) {
            $transaction->rollBack();

            return [
                'status' => FALSE,
                'errors' => $exc->getMessage() . " " . $exc->getCode()

            ];
        }
    }

    /**
     * Recupera la session de la carta semanal
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function actionGetSession()
    {
        $session = Yii::$app->session->get('preparacionCartaSession');
        var_dump($session);
        die();
    }

    protected function validateUpdate($id)
    {

        $DCSemanal = DCartaSemanal::find()->where(['m_prep_carta_id' => $id])->all();
        foreach ($DCSemanal as $value) {
            $cSemanal = $value->mCartaSemanal->asignacionInfanteCSemanals;
            if (!empty($cSemanal)) {
                throw new NotFoundHttpException('El menú ya tiene una asignación infantil. No se puede actualizar');
            }
        }
    }

    /**
     * Updates an existing MPrepCarta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return mixed
     */
    public function actionUpdate($id, $submit = false)
    {
        $model = $this->findModel($id);
//

        $this->validateUpdate($id);


        //instanciar detalles del maestro
        $modelsDetalle = $model->dPreparacionCartas;

        $result = $this->validateMasterDetailsModels($model, $modelsDetalle, \Yii::$app->request);


        if (!$result['status']) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $result;
        }


        if ($submit == true && $model->load(Yii::$app->request->post())) {

            //obtiene los ids de los detalles para borrar
            $oldIDs = ArrayHelper::map($modelsDetalle, 'd_prep_carta_id', 'd_prep_carta_id');

            $d_model = DynamicFormModel::createMultiple(DPreparacionCarta::className());
            DynamicFormModel::loadMultiple($d_model, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = DynamicFormModel::validateMultiple($d_model) && $valid;
            if ($valid) {

                $result = $this->procedureSave($model,$d_model,$oldIDs);

                if(!$result['status']){
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return $result;
                }

            } else {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'status' => FALSE,
                    'errors' => 'Inconsistencia detectada en los datos',
                ];

            }
        } else if (!Yii::$app->request->isAjax) {

            Yii::$app->session->remove('alimentoPlatilloSeleccionado');
            Yii::$app->session->set('preparacionCartaSession', new PreparacionCartaSession());
            $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
            $sesionDetalle->updateSesion($model);

            BackMprepcartaController::ajaxSetTiempoTipo(
                str_replace("_", " ", $model->tiempoComida->tiem_com_nombre), $model->grupo_edad_id);

            //coloca el id del ingrediente en el que usa para el select2 de alimentos
            $modelsDetalle = $this->poblarIdentificadoresToUpdate($modelsDetalle);

            return $this->render('update', [
                'model' => $model,
                'd_model' => (empty($modelsDetalle)) ? [new DPreparacionCarta] : $modelsDetalle
            ]);

        }

    }

    /**
     * Remplaza los ids de los detalles del maestro
     * @param array $modelsDetalle
     * @return type
     */
    public function poblarIdentificadoresToUpdate($modelsDetalle)
    {
        foreach ($modelsDetalle as $key => &$submodel) {
            if ($submodel->alimento_id != null) {
                $submodel->alimento_identificador = $submodel->alimento_id;
            } else {
                $submodel->alimento_identificador = $submodel->m_platillo_id;
            }
        }
        return $modelsDetalle;
    }

    /**
     * Deletes an existing MPrepCarta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MPrepCarta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MPrepCarta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MPrepCarta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
