<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bitacora-consumo-infantil-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'b_cons_inf_id') ?>

    <?= $form->field($model, 'asig_inf_c_sem_id') ?>

    <?= $form->field($model, 'b_cons_inf_porcentaje_consumo') ?>

    <?= $form->field($model, 'm_prep_carta_id') ?>

    <?= $form->field($model, 'b_cons_inf_tiempo_comida') ?>

    <?php // echo $form->field($model, 'b_cons_inf_observaciones') ?>

    <?php // echo $form->field($model, 'b_cons_inf_fecha_consumo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
