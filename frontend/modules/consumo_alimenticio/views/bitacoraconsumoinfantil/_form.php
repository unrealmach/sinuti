<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */
/* @var $form yii\widgets\ActiveForm */

?>

</br>
<div class="bitacora-consumo-infantil-form ">


    <?php
    $dataPorcentajeConsumo = ['1.00' => '100 %', '0.75' => '75 %', '0.5' => '50 %', '0.25' => '25 %', '0.10' => '10 %', '0' => '0 %'];

    ?>
    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <center>
                <h1 style="color: black !important;">
                    <?= Html::encode(Yii::t('app', 'Actualizar Bitácora de Consumo Infantil')) ?>
                </h1>
            </center>
        </div>

        <div class="panel-body">

            <div class="row">

                <div class="col-sm-3">
                    <?=
                    $form->field($model, 'b_cons_inf_fecha_consumo', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                            'inline' => false,
                            'clientOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                            ],
                            'options' => [
                                'readonly' => true,
                                'disabled' => true,
                            ]
                        ])->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <?php
                        echo Html::label('Infante: ', '', ['class' => 'col-sm-12 control-label']);
                        echo Html::input('text', 'data', $model->asigInfCSem->infante->nombreCompleto, ['class' => 'col-sm-12', 'readonly' => true]);
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <?=
                    $form->field($model, 'm_prep_carta_id', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->widget(Select2::classname(), [
                            'data' => ArrayHelper::map([$model->m_prep_carta_id => \backend\modules\preparacion_carta\models\MPrepCarta::findOne($model->m_prep_carta_id)], 'm_prep_carta_id', 'm_prep_carta_nombre'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione tipo de platillo', 'class' => 'col-sm-12', 'disabled' => true],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label(null, ['class' => 'col-sm-12 control-label']);

                    ?>
                </div>
                <div class="col-sm-3">
                    <?=
                    $form->field($model, 'b_cons_inf_tiempo_comida', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                        ->dropDownList(['DESAYUNO' => 'DESAYUNO', 'REFRIGERIO_DE_LA_MAÑANA' => 'REFRIGERIO DE LA MAÑANA', 'ALMUERZO' => 'ALMUERZO', 'REFRIGERIO_DE_LA_TARDE' => 'REFRIGERIO DE LA TARDE',], ['disabled' => 'disabled'])
                        ->label(null, ['class' => 'col-sm-12 control-label'])

                    ?>
                </div>


            </div>

        <div class="row">

            <div class="col-sm-4">
                <?=
                $form->field($model, 'b_cons_inf_porcentaje_consumo', ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                    ->widget(Select2::classname(), [
                        'data' => $dataPorcentajeConsumo,
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione Porcentaje de Consumo', 'class' => 'col-sm-12',],
                        'pluginOptions' => [
//                    'allowClear' => true
                        ],
                    ])->label(null, ['class' => 'col-sm-12 control-label']);

                ?>

            </div>
            <div class="col-sm-8">
                <?=
                $form->field($model, 'b_cons_inf_observaciones', [
                    'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                    ->textarea(['rows' => 4,'style'=>'text-transform: uppercase'])
                    ->label(null, ['class' => 'col-sm-12 control-label']);

                ?>

            </div>
        </div>




            <div class="container">

                <div class="form-group">
                    <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                        'onclick' => ' window.history.back();',
                        'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-md btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
                </div>


            </div>


        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
