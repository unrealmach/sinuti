<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Histórico de la Bitácora del Consumo Infantil');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="bitacora-consumo-infantil-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>

    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-bar-chart']) . Yii::t('app', 'Ver consumo ideal infantil'), ['report-consumo-ideal'], ['class' => 'btn btn-gray']) ?>
    </p>


    <?php
    Pjax::begin(['id' => 'bitacoraGrid']);
    $dataPorcentajeConsumo = ['1.00' => '100 %', '0.75' => '75 %', '0.5' => '50 %', '0.25' => '25 %', '0.10' => '10 %', '0' => '0 %'];
    $tiempos_comidas = ArrayHelper::map(backend\modules\preparacion_carta\models\TiempoComida::find()->all(), 'tiem_com_nombre', 'tiem_com_nombre');

    ?>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
//        'asigInfCSem.mCartaSemanal.rangoFechas',
        [
            'label' => 'Nombre del Infante',
            'attribute'=>'asig_inf_c_sem_id',
            'value' => 'asigInfCSem.infante.nombreCompleto',
            'group' => true,
        ],
        ['attribute' => 'm_prep_carta_id',
            'value' => 'mPrepCarta.m_prep_carta_nombre'
        ],
        [
            'attribute' => 'b_cons_inf_tiempo_comida',
            'format' => 'raw',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $tiempos_comidas,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Seleccione'],
        ],
//        'b_cons_inf_porcentaje_consumo',
        [
            'attribute' => 'b_cons_inf_porcentaje_consumo',
            'format' => 'raw',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $dataPorcentajeConsumo,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Seleccione'],
            'value' => function ($model, $index, $widget) {
            switch ($model['b_cons_inf_porcentaje_consumo']) {
                case '0.10':
                    return '10 %';
                case '0.25':
                    return '25 %';
                case '0.50':
                    return '50 %';
                case '0.75':
                    return '75 %';
                case '1.00':
                    return '100 %';

                default:
                    return '0 %';
            }
        },
        ],
        'b_cons_inf_observaciones',
        [
            'width'=>'250px',
            'attribute' => 'b_cons_inf_fecha_consumo',
            'value' => 'b_cons_inf_fecha_consumo',
            'format' => 'raw',
            'filter' => \kartik\date\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'b_cons_inf_fecha_consumo',
                'options' => ['placeholder' => 'Seleccione la fecha ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]),
            'group' => true,
        ],
    ];

    ?>


<?php
$fullExportMenu = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'asDropdown' => false, // this is important for this case so we just need to get a HTML list
        'dropdownOptions' => [
            'label' => '<i class = "glyphicon glyphicon-export"></i> Full',
        ],
    ]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<h3 class = "panel-title"><i class = "glyphicon glyphicon-list"></i>Bitácora</h3>',
    ],
    // the toolbar setting is default
    'toolbar' => [
        '{export}',
        ['content' => Html::a('<i class = "glyphicon glyphicon-repeat"></i>', ['/consumoalimenticio/bitacoraconsumoinfantil/'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('kvgrid', 'Reset Grid')]),
        ],
    ],
    // configure your GRID inbuilt export dropdown to include additional items
    'export' => [
        'fontAwesome' => true,
        'itemsAfter' => [
            '<li role = "presentation" class = "divider"></li>',
            '<li class = "dropdown-header">Exportar todos los datos</li>       ',
            $fullExportMenu,
        ],
    ],
]);

?>
    <?php Pjax::end(); ?>

</div>
