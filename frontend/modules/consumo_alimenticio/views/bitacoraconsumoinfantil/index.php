<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bitácora del Consumo Infantil');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitacora-consumo-infantil-index">

    <center><h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>


    <?php
    $dataPorcentajeConsumo = ['1.00' => '100 %', '0.75' => '75 %', '0.5' => '50 %',
        '0.25' => '25 %', '0.10' => '10 %', '0' => '0 %'];
    $tiempos_comidas = ArrayHelper::map(backend\modules\preparacion_carta\models\TiempoComida::find()->all(),
        'tiem_com_nombre', 'tiem_com_nombre');


    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
//        'asigInfCSem.mCartaSemanal.rangoFechas',
        [
            'label' => 'Nombre del Infante',
            'attribute' => 'asig_inf_c_sem_id',
            'value' => 'asigInfCSem.infante.nombreCompleto',
            'group' => true,
        ],
        ['attribute' => 'm_prep_carta_id',
            'value' => 'mPrepCarta.m_prep_carta_nombre'
        ],
        [
            'attribute' => 'b_cons_inf_tiempo_comida',
            'format' => 'raw',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $tiempos_comidas,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Seleccione'],
        ],
//        'b_cons_inf_porcentaje_consumo',
        [
            'attribute' => 'b_cons_inf_porcentaje_consumo',
            'format' => 'raw',
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => $dataPorcentajeConsumo,
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Seleccione'],
            'value' => function ($model, $index, $widget) {
                switch ($model['b_cons_inf_porcentaje_consumo']) {
                    case '0.10':
                        return '10 %';
                    case '0.25':
                        return '25 %';
                    case '0.50':
                        return '50 %';
                    case '0.75':
                        return '75 %';
                    case '1.00':
                        return '100 %';

                    default:
                        return '0 %';
                }
            },
        ],
        'b_cons_inf_observaciones',
        [
            'width' => '250px',
            'attribute' => 'b_cons_inf_fecha_consumo',
            'value' => 'b_cons_inf_fecha_consumo',
            'format' => 'raw',
            'filter' => \kartik\date\DatePicker::widget([
                'model' => $searchModel,
                'attribute' => 'b_cons_inf_fecha_consumo',
                'options' => ['placeholder' => 'Seleccione la fecha ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true
                ]
            ]),
            'group' => true,
        ],
//        ['class' => 'yii\grid\ActionColumn'],
        ['class' => 'yii\grid\ActionColumn',
            'template' => '{update}'],
    ];


    $fullExportMenu = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'asDropdown' => false, // this is important for this case so we just need to get a HTML list
        'dropdownOptions' => [
            'label' => '<i class = "glyphicon glyphicon-export"></i> Full',
        ],
        'exportConfig' => [

            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,

            ExportMenu::FORMAT_EXCEL_X => false,
            ExportMenu::FORMAT_TEXT => false,

        ]
    ]);

   ?>


    <?php
    $layout = <<< HTML

<div class="pull-left">
<!-- TODO poner los enlaces dinamicamente para que aparesca index.php
href="index.php?r=/consumoalimenticio/bitacoraconsumoinfantil/create -->
        <a class="btn btn-success" href="/consumoalimenticio/bitacoraconsumoinfantil/create" title="Solo para los educadores">
        <i class="fa fa-fw fa-plus"></i>Registrar todos</a>
        <a class="btn btn-gray" href="/consumoalimenticio/bitacoraconsumoinfantil/report-consumo-ideal">
        <i class="fa fa-bar-chart"></i>Ver consumo ideal infantil</a>        
        <a class="btn btn-info" href="/consumoalimenticio/bitacoraconsumoinfantil/index-inactivos">
        <i class=""></i>Ver historial</a>
{summary}
</div>
<div class="pull-right">
{export}
</div>
<div class="clearfix"></div>
{items}
{pager}
HTML;
?>
    <?php
    Pjax::begin(['id' => 'bitacoraGrid']);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'layout' => $layout,
        'export' => [
                'label'=>'Exportar',
            'fontAwesome' => true,
            'itemsAfter' => [
                '<li role = "presentation" class = "divider"></li>',
                '<li class = "dropdown-header">Exportar todos los datos</li>',
                $fullExportMenu,
            ],
        ],
        'exportConfig' => [
            GridView::PDF => true,
            GridView::EXCEL => true,

        ],

    ]);

    Pjax::end();
    ?>

</div>
