<?php
use frontend\assets\TableAsset;

TableAsset::register($this);

?>
<center>
    <h3>Tabla de consumo del Infante <?= $infanteModel->nombreCompleto ?></h3>
</center>
<div class="" style="overflow: auto;">
    <?php echo $table; ?>
    <br>
    <center>  <div > Indicadores:  </div></center>

    <div>
        Si el color del promedio es <span class="sobre"> azul </span> ,
        <span class="inferior"> rojo </span> o negro se interpretara como
        riesgo, exceso o normal respectivamente.

    </div>
    <?php echo $indicadores; ?>

</div>