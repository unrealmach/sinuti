<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\date\DatePicker;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behavior_report.js', ['depends' => [frontend\assets\AppAsset::className()]]);

$urlAjaxListInfantes = \Yii::$app->urlManager->createUrl('admin/individuo/infante/ajax-list-infantes');
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Consumo ideal');
$this->params['breadcrumbs'][] = $this->title;

?>
</br>
</br>
<div class="consumo-ideal-infantil-index">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div style="text-align:center; width:100%">
                <h1 style="color: black !important;">
                    <?= Html::encode(Yii::t('app', 'Reporte de Consumo Ideal'))?>
                </h1>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    echo '<label class="control-label">Infante </label>';
                    echo Select2::widget([
                        'name' => 'selector_infantes',
                        'data' => [],
                        'language' => 'es',
                        'options' => ['placeholder' => 'Seleccione por nombres o apellidos', 'class' => 'col-sm-12',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'ajax' => [
                                'url' => $urlAjaxListInfantes,
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(data) { return data.text+" / "+data.iden; }'),
                            'templateSelection' => new JsExpression('function (data) { return data.text; }'),
                        ],
                    ]);

                    ?>
                </div>
                <div class="col-sm-6">
                    <?php
                    echo '<label>Fecha </label>';
                    echo DatePicker::widget([
                        'id' => 'fecha',
                        'name' => 'fecha',
                        'options' => ['placeholder' => 'Seleccione un día de la semana'],
                        'pluginOptions' => [
                            'daysOfWeekDisabled' => '0,6',
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'calendarWeeks' => true,
                            'autoclose'=>true
                        ]
                    ])

                    ?>
                </div>


            </div>
            <div class="row">
                <div class="container">

                    <div class="form-group">
                        <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close']) . Yii::t('app', 'Cancel'), [
                            'onclick' => ' window.history.back();',
                            'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                        <button class = 'btn btn-default btn-md btn-hvr hvr-radial-out' type="button" id="calcular">
                            <i class="fa fa-bar-chart"></i>Calcular</button>
                    </div>


                </div>



            </div>
        </div>
    </div>








</div>
