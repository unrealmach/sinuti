<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bitacora Consumo Infantil',
]) . ' ' . $model->b_cons_inf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bitacora Consumo Infantils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->b_cons_inf_id, 'url' => ['view', 'id' => $model->b_cons_inf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bitacora-consumo-infantil-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
