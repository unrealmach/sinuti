<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */

$this->title = Yii::t('app', 'Crear Bitacora Consumo Infantil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bitacora Consumo Infantils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitacora-consumo-infantil-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
