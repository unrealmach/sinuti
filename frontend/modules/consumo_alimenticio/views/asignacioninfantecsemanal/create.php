<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal */

$this->title = Yii::t('app', 'Asignación de Infantes al Plan de Alimentación Semanal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignación de Infantes al Plan de Alimentación Semanal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-infante-csemanal-create">

    </br>

    <?= $this->render('_form', [
        'model' => $model,
//        'lista_infantes_2' => $lista_infantes_2,
        'lista_infantes'=>$lista_infantes
    ]) ?>

</div>
