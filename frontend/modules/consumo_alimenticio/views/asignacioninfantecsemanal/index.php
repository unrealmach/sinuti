<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignación de Infantes al Plan de Alimentación');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="asignacion-infante-csemanal-index">

    <center> <h1><?= Html::encode($this->title) ?></h1></center>
    <div class="separator"></div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Asignar'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'infante_id',
                'value' => 'infante.nombreCompleto'],
            ['attribute' => 'm_carta_semanal_id',
                'value' => 'mCartaSemanal.rangoFechasAndObservacion'],
            'asi_inf_c_sem_id_observaciones:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{eliminar}',
                'buttons' => [
                    'eliminar' => function ($url, $model) {
                    $isvalid = AsignacionInfanteCSemanal::checkIsValidToDelete($model->asig_inf_c_sem_id);
                    if ($isvalid) {
                        return Html::a('<span class="glyphicon glyphicon-off"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar asignación'),
                        ]);
                    }
                }
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'eliminar') {
                    $url = Url::to(['eliminar', 'id' => $model->asig_inf_c_sem_id]);
                    return $url;
                }
            }
            ]
        ],
    ]);

    ?>

</div>
