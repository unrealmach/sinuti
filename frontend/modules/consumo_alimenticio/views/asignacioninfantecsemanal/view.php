<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal */

$this->title = $model->asig_inf_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Infante Csemanals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="asignacion-infante-csemanal-view">

    <center> 
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="separator"></div>
    </center>

    <p>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->asig_inf_c_sem_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->asig_inf_c_sem_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])

        ?>
    </p>

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="panel-body">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'asig_inf_c_sem_id',
                    'infante_id',
                    'm_carta_semanal_id',
                    'asi_inf_c_sem_id_observaciones:ntext',
                ],
            ])

            ?>
        </div>
    </div>
</div>
