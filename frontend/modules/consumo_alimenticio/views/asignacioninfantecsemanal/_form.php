<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behavior_form.js',
    ['depends' => [frontend\assets\AppAsset::className()]]);

$urlAjaxCapacidadCarta = \Yii::$app->urlManager->createUrl('admin/carta_semanal/mcartasemanal/ajax-get-capacidad-carta');
$urlAjaxGetNumInfantesYaAsignadosMenu = \Yii::$app->urlManager->createUrl('admin/consumo_alimenticio/asigcseminfantehasinf/ajax-get-num-infantes');
$urlAjaxInfanteListBySalon = \Yii::$app->urlManager->createUrl('admin/consumo_alimenticio/asiginfcsemanal/ajax-infante-list');
/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal */
/* @var $form yii\widgets\ActiveForm */
?>
    </br>
    <div class="asignacion-infante-csemanal-form ">
        <?php $form = ActiveForm::begin(); ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <center>
                    <h1 style="color: black !important;">
                        <?=
                        Html::encode(Yii::t('app',
                            'Asignación de Infantes al Plan de Alimentación Semanal'))
                        ?>
                    </h1>
                </center>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-4">

                        <?php
                        $carta_semanal_vigente = ArrayHelper::map(\backend\modules\carta_semanal\models\MCartaSemanal::find()->getCartaSemanalVigenteByCentroInfantil(Yii::$app->user->id)->all(),
                            'm_c_sem_id', 'rangoFechasAndObservacion',
                            'cenInf.cen_inf_nombre');
                        if (empty($carta_semanal_vigente)) {
                            throw new \yii\web\NotFoundHttpException('El Cibv no tiene asignado un plan de alimentación semanal');
                        }

                        echo $form->field($model, 'm_carta_semanal_id',
                            ['template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])
                            ->widget(Select2::classname(),
                                [
                                    'data' => $carta_semanal_vigente,
                                    'language' => 'es',
                                    'options' => ['placeholder' => 'Seleccione el plan de alimentación semanal',
                                        'class' => 'col-sm-12',
                                        'onchange' => 'obtenerCapacidadMenu("' . $urlAjaxCapacidadCarta . '","' . $urlAjaxGetNumInfantesYaAsignadosMenu . '",$("#asignacioninfantecsemanal-m_carta_semanal_id").val())'
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label('Planificación de Alimentación Semanal Vigente');
                        ?>

                    </div>

                    <div class="col-sm-4">
                        <?=
                        $form->field($model, 'capacidad_carta_semanal',
                            [
                                'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput(array(
                            'placeholder' => 'Ingrese ...', 'disabled' => 'disabled'))
                            ->label(null, ['class' => 'col-sm-12 control-label'])
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <?=
                        $form->field($model,
                            'total_infantes_asignados_carta_semanal',
                            [
                                'template' => '{label}<div class="col-sm-12">{input}{hint}{error}</div>'])->textInput([
                            'placeholder' => 'Ingrese ...', 'disabled' => 'disabled'])
                            ->label(null, ['class' => 'col-sm-12 control-label'])
                        ?>
                    </div>


                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <?=
                        $form->field($model, "lista_infantes")->widget(Select2::classname(),
                            [
                                'data' => $lista_infantes,
                                'maintainOrder' => true,
                                'options' => ['placeholder' => 'Seleccione los infantes', 'multiple' => true,
                                    'onchange' => "validarCantidadAsignaciones()"
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
//                    'maximumInputLength' => 10
                                ],
                            ]);
                        ?>
                    </div>
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <center><h4>
                                        Buscador de infantes
                                    </h4></center>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?=
                                        $form->field($model, "salones_aux")->widget(Select2::classname(),
                                            [
                                                'data' => ArrayHelper::map(backend\modules\inscripcion\models\Salon::find()->all(),
                                                    'salon_id', 'salon_nombre'),
                                                'maintainOrder' => true,
                                                'options' => ['placeholder' => 'Salones',
                                                    'multiple' => false,
//                        'onchange' => "validarCantidadAsignaciones()"
                                                ],
                                                'pluginOptions' => [
                                                    'tags' => true,
//                    'maximumInputLength' => 10
                                                ],
                                            ]);
                                        ?>
                                    </div>
                                    <!--$("#select2-asignacioninfantecsemanal-lista_infantes_aux-results").find("li").trigger("mouseup")-->
                                    <div class="col-md-6">
                                        <?php
                                        echo $form->field($model, "lista_infantes_aux")->input('select')->widget(Select2::classname(),
                                            [
                                                'language' => 'es',
                                                'options' => ['placeholder' => 'Seleccione infantes',
                                                    'multiple' => true, 'class' => 'col-sm-3'
                                                ],
                                                'pluginOptions' => [
                                                    'multiple' => true,
                                                    'allowClear' => true,
                                                    'ajax' => [
                                                        'url' => $urlAjaxInfanteListBySalon,
                                                        'dataType' => 'json',
                                                        'tags' => true,
                                                        'data' => new JsExpression('function(params) { return {q:params.term,id:$("#asignacioninfantecsemanal-salones_aux").val()}; }'),
                                                        'results' => new JsExpression('function(data,page) { return {results:data.results}; }'),
                                                        'cache' => true
                                                    ],
                                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                                    'templateResult' => new JsExpression('function(articulo_mercaderia) { return articulo_mercaderia.text; }'),
                                                    'templateSelection' => new JsExpression('function (articulo_mercaderia) { return articulo_mercaderia.text; }'),
                                                ],
                                            ]);
                                        ?>

                                    </div>
                                    <div class="col-sm-2">
                                        <a class="btn btn-default btn-sm" type="button" id="consolidarlista">
                                            Consolidar
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>


                <div class="row container">
                    <?=
                    $form->field($model, 'asi_inf_c_sem_id_observaciones')
                        ->textarea(['rows' => 2,'style'=>'text-transform: uppercase'])
                    ?>
                </div>

            </div>
            <p class="container">
                <?= Html::button(Html::tag('i', '', ['class' => 'fa fa-close'])

                    . Yii::t('app', 'Cancel'), [
                    'onclick' => ' window.history.back();',
                    'class' => 'btn btn-warning btn-md btn-hvr hvr-radial-out']) ?>
                <?=
                Html::submitButton($model->isNewRecord ? Html::tag('i', '',
                        ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Asignar')
                    : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app',
                        'Update'),
                    ['class' => $model->isNewRecord ? 'btn btn-primary btn-md btn-hvr hvr-radial-out'
                        : 'btn btn-primary btn-md btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"])
                ?>
            </p>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<< JS

   $(function(){

     //objeto para localstorage
     session = {'ids': []};
    localStorage.setItem('session', JSON.stringify(session));

// permite selecionar todos los elementos de la lista temporal
  $("#s2-togall-asignacioninfantecsemanal-lista_infantes_aux").on('mousedown', function () {
    //valida si se puede selecionar
    if ($("#s2-togall-asignacioninfantecsemanal-lista_infantes_aux").hasClass("s2-togall-select")) {
        $("#select2-asignacioninfantecsemanal-lista_infantes_aux-results").find("li").trigger("mouseup");
    }

})
    $("#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal").data("num_asignados",$("#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal").val());
   
    $("#consolidarlista").on('click',function(){
    consolidarListas();
        });
    
   
   });

function consolidarListas(){
       var lista_temporal = $("#asignacioninfantecsemanal-lista_infantes_aux").val();
       
   var localstorageids =manejarlocalstorage(lista_temporal)
    if(lista_temporal===null){
            alert('Use la lista temporal');     
            }
    else{
        $("#asignacioninfantecsemanal-lista_infantes").val(localstorageids.ids).trigger("change");
    
         }
   
}

    //permite guardar un arreglo de ids en localstorage
function manejarlocalstorage(lista_temporal){
    //temporal para obtener ids no repetidos
      arraySinDuplicados = [];

    //obtiene el objeto en localstorage y lo parse de JSON
    session = JSON.parse(localStorage.getItem('session'));
    //Recorre la lista de ids de la lista temporal y los almacena en el objeto localstorage
    for (var i = 0; i < lista_temporal.length; i++) {
        session.ids.push(lista_temporal[i]);
    }
    //elimina duplicados
    $.each(session.ids, function(i, el){
        if($.inArray(el, arraySinDuplicados) === -1) arraySinDuplicados.push(el);
    });
    // coloca los ids en la posicion del Object localstorage
    session.ids=arraySinDuplicados;
    //guarda el localstorage con un json stringifly
    localStorage.setItem('session', JSON.stringify(session));
    
        return session;
   }
    

JS;
$this->registerJs($script);

