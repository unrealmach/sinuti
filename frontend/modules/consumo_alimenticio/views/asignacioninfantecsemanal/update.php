<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asignación de Infantes al Plan de Alimentación',
]) . ' ' . $model->asig_inf_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignación de Infantes al Plan de Alimentación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asig_inf_c_sem_id, 'url' => ['view', 'id' => $model->asig_inf_c_sem_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-infante-csemanal-update">

   </br>
    <?= $this->render('_form', [
        'model' => $model,
        'lista_infantes'=>$lista_infantes
    ]) ?>

</div>
