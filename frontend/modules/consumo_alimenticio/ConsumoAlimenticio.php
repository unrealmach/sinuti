<?php

namespace frontend\modules\consumo_alimenticio;

class ConsumoAlimenticio extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\consumo_alimenticio\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
