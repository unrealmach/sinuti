<?php
namespace frontend\modules\consumo_alimenticio\controllers;

use Yii;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante;
use backend\modules\carta_semanal\models\MCartaSemanal;

/**
 * AsignacioninfantecsemanalController implements the CRUD actions for AsignacionInfanteCSemanal model.
 */
class AsignacioninfantecsemanalController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AsignacionInfanteCSemanal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AsignacionInfanteCSemanalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AsignacionInfanteCSemanal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AsignacionInfanteCSemanal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $dataToGrid = [];
        $salones = [];
        //primero pregunta si existe un menu semanal para este cibv para esta semana, segun el id del coordinador cibv
        $cartas_semanales_vigentes = MCartaSemanal::find()->getCartaSemanalVigenteByCentroInfantil(\Yii::$app->user->id)->all();

        if (!is_null($cartas_semanales_vigentes)) {
            //trae una lista de infantes disponibles para asignacion
            $containerInfantesModel = $this->getInfantesDisponiblesAsignacionSemanal($cartas_semanales_vigentes);

            $model = new AsignacionInfanteCSemanal();
            if ($model->load(Yii::$app->request->post())) {
                $request = Yii::$app->request;
                $lista_infantes = $request->post('AsignacionInfanteCSemanal')['lista_infantes'];
                $m_carta_semanal_id = $request->post('AsignacionInfanteCSemanal')['m_carta_semanal_id'];
                if (!empty($lista_infantes)) {
                    foreach ($lista_infantes as $key => $infante_id) {
                        $modl = new AsignacionInfanteCSemanal();
                        $modl->infante_id = $infante_id;
                        $modl->m_carta_semanal_id = $m_carta_semanal_id;
                        $modl->asi_inf_c_sem_id_observaciones = $request->post('AsignacionInfanteCSemanal')['asi_inf_c_sem_id_observaciones'];
                        $modl->save(false);
                        $junctionModel = new AsignacionInfCSemanalHasInfante(); //para guardar en junctiontable
                        $junctionModel->asig_inf_c_sem_id = $modl->asig_inf_c_sem_id;
                        $junctionModel->infante_id = $infante_id;
                        $junctionModel->save(false); //todo poner en una transaccion si es que es posible
                    }
                    Yii::$app->getSession()->setFlash('success', 'Created successfully');
                    return $this->redirect(['index']);
                } else {
                    throw new NotFoundHttpException('No tiene infantes disponibles');
                }
            } else {
                Yii::$app->session->set('lista_infantes', array_keys($containerInfantesModel));
                return $this->render('create', [
                        'model' => $model,
                        'lista_infantes' => $containerInfantesModel
                ]);
            }
        } else {
            Yii::$app->getSession()->setFlash('error', 'El Cibv no tiene asignado un plan de alimentación semanal');
            throw new \yii\web\ForbiddenHttpException;
        }
    }

    /**
     * Updates an existing AsignacionInfanteCSemanal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Yii::$app->getSession()->setFlash('error', 'No es posible actualizar, elimine la asignación del infante y reasigne nuevamente');
        throw new \yii\web\ForbiddenHttpException;
    }

    /**
     * Deletes an existing AsignacionInfanteCSemanal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return mixed
     */
    public function actionEliminar($id)
    {
        $data = AsignacionInfanteCSemanal::checkIsValidToDelete($id);

        if ($data) {
            $junctionModel = new AsignacionInfCSemanalHasInfante();
            $model_junction_delete = $junctionModel->findIdAsignacionSemanaActualByCartaSemanal($id);
            $model_junction_delete->delete();
            $this->findModel($id)->delete();
            Yii::$app->getSession()->setFlash('success', 'Registro eliminado');
            return $this->redirect(['index']);
        } else {
            Yii::$app->getSession()->setFlash('Error', 'Imposible eliminar este registro, ya tiene una bitácora registrada');
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the AsignacionInfanteCSemanal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AsignacionInfanteCSemanal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AsignacionInfanteCSemanal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Retorna una lista key -> value donde key es el id del infante y value es
     * el nombre completo del infante
     * @param array $cartas_semanales_vigentes Cartas semanales dispuestas para
     * el consumo del cibv
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    private function getInfantesDisponiblesAsignacionSemanal($cartas_semanales_vigentes)
    {
        //obtiene los infantes que tiene el cibv en el periodo actual, deacuerdo a su usario coordinador_cibv
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $periodo = \backend\modules\parametros_sistema\models\Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('No existe un período válido');
        }
        $asignacionCoordinadorCibv = \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        $autoridadCibv = \backend\modules\recursos_humanos\models\AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
        $infantesCentroInfantil = \backend\modules\inscripcion\models\Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, $periodo->periodo_id)->all();
        $containerInfantesModel = []; //castea para la vista key->value
        foreach ($infantesCentroInfantil as $key => $value) {
            $containerInfantesModel[$value->infante->infante_id] = $value->infante->nombreCompleto;
        }

        //busca los infantes en la junctiontable para eliminar los repetidos
        $modelsInfantesAsignado = [];
        foreach ($cartas_semanales_vigentes as $cartaSemanal) {
            $modelsInfantessCartaTemp = AsignacionInfCSemanalHasInfante::findInfantesByCartaSemanalId($cartaSemanal->m_c_sem_id);
            if (!empty($modelsInfantessCartaTemp)) {
                foreach ($modelsInfantessCartaTemp as $asignacion) {
                    $modelsInfantesAsignado[$asignacion->infante_id] = $asignacion;
                }
            }
//            TODO
//            else {
//                throw new NotFoundHttpException('No existen infantes disponibles');
//            }
        }

        $listInfantesYaAsignado = []; //castea para compara con la otra lista
        foreach ($modelsInfantesAsignado as $key => $asignacion) {
            $listInfantesYaAsignado[$asignacion->infante_id] = $asignacion->infante_id;
        }
        //busca los mismos infantes y los elimina de la lista a enviar a la vista
        foreach ($listInfantesYaAsignado as $key => $value) {
            if (array_key_exists($value, $containerInfantesModel)) {
                unset($containerInfantesModel[$value]);
            }
        }

        return $containerInfantesModel;
    }
}
