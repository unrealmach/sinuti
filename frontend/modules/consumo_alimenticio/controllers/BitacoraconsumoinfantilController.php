<?php
namespace frontend\modules\consumo_alimenticio\controllers;

use Yii;
use backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil;
use backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch;
use backend\modules\consumo_alimenticio\models\searchs\BitacoraconsumoinfantilinactivoSearch;
use \backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \kartik\mpdf\Pdf;

/**
 * BitacoraconsumoinfantilController implements the CRUD actions for BitacoraConsumoInfantil model.
 */
class BitacoraconsumoinfantilController extends Controller
{

    /**
     * Permite el filtrado de las acciones y del RBAC
     * @return array
     */
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BitacoraConsumoInfantil models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BitacoraconsumoinfantilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lista todos los modelos de BitacoraConsumo infantil que tengan fecha fin
     * @return type
     */
    public function actionIndexInactivos()
    {
        $searchModel = new BitacoraconsumoinfantilinactivoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexInactivos', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BitacoraConsumoInfantil model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BitacoraConsumoInfantil model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BitacoraConsumoInfantil();
        $user_id = Yii::$app->user->id;
        $educadorModel = \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv::find()
                ->where(['user_id' => $user_id])->one();
        if (!is_null($educadorModel)) {

            $model->llenarBitacora(Yii::$app->user->id);
        } else {

            throw new NotFoundHttpException('Este educador no tiene asignado un cibv');
        }
        return $this->redirect('index');
    }

    /**
     * Updates an existing BitacoraConsumoInfantil model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BitacoraConsumoInfantil model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the BitacoraConsumoInfantil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BitacoraConsumoInfantil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BitacoraConsumoInfantil::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Genera el reporte del consumo ideal
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return mixed
     */
    public function actionReportConsumoIdeal()
    {
        $searchModel = new BitacoraconsumoinfantilSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('report/index', [
        ]);
    }

    /**
     * Permite general el PDF del consumo ideal de un infante de acuerdo a la fecha
     * @param string $fecha
     * @param int $infante_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return PDF
     * @throws NotFoundHttpException Cuando no encuentra registros de consumo para ese infante en una fecha especificada
     */
    public function actionAjaxcalcrep($fecha, $infante_id)
    {
        $date = new \DateTime($fecha);
        $numSemana = $date->format("W");
        $anio = $date->format("Y");
        $junctionTable = AsignacionInfCSemanalHasInfante::findByInfanteAnioNumSemana($infante_id, $anio, $numSemana); //en junction buscar la asignacion
        if (is_null($junctionTable)) {
            throw new NotFoundHttpException('Datos invalidos, asegurese de tener una bitacora para la fecha seleccionada');
        }
        $consumos = BitacoraConsumoInfantil::generarReporteConsumoIdeal($junctionTable->asigInfCSem);
        $out = BitacoraConsumoInfantil::construirTableConsumoVs($consumos);
        $html = $out['html'];
        $data = $out['data'];
        $indicadores = BitacoraConsumoInfantil::semaforoBitacoraConsumo($out['porcentajeAdecuacion']);
        $infanteModel = \backend\modules\individuo\models\Infante::findOne($infante_id);
        $content = $this->renderPartial('report/_tableConsumoVs', [
            'table' => $html,
            'infanteModel' => $infanteModel,
            'indicadores' => $indicadores,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::DEST_BROWSER,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.css',
//            'cssFile' => ' @web/css/table.css',
            // any css to be embedded if required
            'cssInline' => '
                        .sobre{
                            color:blue;
                        }
                        .normal{
                            color:black;
                        }
                        .inferior{
                            color:brown;
                        }',
            // set mPDF properties on the fly
            'options' => ['title' => 'Porcentaje de consumo ideal '],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Sinuti Report'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }
}
