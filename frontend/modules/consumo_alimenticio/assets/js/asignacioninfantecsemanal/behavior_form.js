/**
 * Permite validar las cantidades de asignaciones para que no pasen el limite
 * de la cantidad de comensales de la carta semanal
 * @author Mauricio Chamorro
 */
function validarCantidadAsignaciones()
{
    var numselec = $('#asignacioninfantecsemanal-lista_infantes').siblings('.select2-container--krajee').find('li.select2-selection__choice').size();;
    $('#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal').val(parseInt($('#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal').data('num_asignados')) + numselec);
    var numasignados = parseInt($('#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal').val());
    var capacidad = parseInt($('#asignacioninfantecsemanal-capacidad_carta_semanal').val());
    if (numasignados > capacidad) {
        alert('Se excedió la cantidad de infantes para este menu');
        $('form').find(':submit').hide();
    } else {
        $('form').find(':submit').show();
    }
}

/**
 * Mediante ajax encuentra la capacidad total y los infantes asignados a una carta semanal
 * @param {String} url url para buscar la capacidad total
 * @param {String} url2 url para buscar el numero de infantes asignados
 * @param {int} id carta semanal id
 * @author Mauricio Chamorro
 * @returns {undefined}
 */
function obtenerCapacidadMenu(url, url2, id)
{
    $.get(url, {mcarta_id: id})
            .done(function(data) {
                $("#asignacioninfantecsemanal-capacidad_carta_semanal").val(data);
            })
            .complete(function(data) {
                $.get(url2, {carta_semanal_id: id})
                        .done(function(retorno) {
                            $("#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal").data("num_asignados", retorno);
                            $("#asignacioninfantecsemanal-total_infantes_asignados_carta_semanal").val(retorno);
                        })
                        .complete(function() {
                            validarCantidadAsignaciones();
                        })
                        ;
            })
            ;




}