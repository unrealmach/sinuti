tagloading = "";
$(function() {
    tagloading = $("#tagLoading");

})


/**
 * Permite llamar al modal en donde se visualiza la imagen de loading
 * @returns {undefined}
 */
function showModalLoading2() {
    $("#labelMainModal2Header").css("visibility", "visible");
    $("#mainModal2 #mainModal2Body").children().remove();
    $("#mainModal2 #mainModal2Body").append(tagloading);
    $("#mainModal2").modal();
}
function showModalLoading() {
    //$('body').css('overflow', 'hidden');

    $("#labelMainModalHeader").css("visibility", "visible");
    $("#mainModal #mainModalBody").children().remove();
    $("#mainModal #mainModalBody").append(tagloading);
    $("#mainModal").modal().on('shown', function(){
        $('body').css('overflow', 'hidden');
    }).on('hidden', function(){
        $('body').css('overflow', 'auto');
    })
    /*$("#mainModal").modal().on('shown', function(){
        $('body').css('overflow', 'hidden');
});*/
}

/**
 * Cierra el modal
 * @returns {undefined}
 */
function closeModal() {
    //$('body').css('overflow', 'auto');
    $("#mainModal").modal("hide")
   /* $("#mainModal").modal().on('hidden', function(){
        $('body').css('overflow', 'auto');
    })*/
}
function closeModal2() {
    $("#mainModal2").modal("hide")
}

/**
 * Permite que se muestre contenido en un modal
 * @param {type} html
 * @returns {undefined}
 */
function showModalWithData(html) {
    $("#mainModal .modal-body").children().remove();
    console.log(html);
    
    $("#mainModal .modal-body").append("<center>" + html + "</center>");
    $("#labelMainModalHeader").css("visibility", "hidden")
    $("#mainModal").modal();
}

/**
 * Permite que se muestre contenido en un modal
 * @param {type} html
 * @returns {undefined}
 */
function showModalWithData2(html) {
    $("#mainModal2 .modal-body").children().remove();
    $("#mainModal2 .modal-body").append("<center>" + html + "</center>");
    $("#labelMainModal2Header").css("visibility", "hidden")
    $("#mainModal2").modal();
}


/**
 * Para cargar un formulario en un modal
 * TODO
 * @param {type} actionUrl
 * @returns {undefined}
 */
function showModalForm(actionUrl) {
}