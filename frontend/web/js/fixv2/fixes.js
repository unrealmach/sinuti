$(function () {

    //remove slideDpwn content of menu
    $("li.dropdown.mega-menu").unbind();

});


function showFlashAlert(msg){
    $('#flashAlert').addClass("alert-danger").addClass("alertMessage").addClass("alert");
    $('#contentFlashAlert').html(msg);
    $('#flashAlert').fadeIn().delay(6000).fadeOut("slow");
}