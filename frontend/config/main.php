<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'homeUrl' => '/', //define la ruta para frontend
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'request' => [//como envia el url desde el sistema
            'baseUrl' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true, //limpia la url
            'showScriptName' => false, //desactiva visualizacion de index.php
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'Util' => [
            'class' => 'common\components\Util'
        ],
        'assetManager' => [
            'bundles' => [
                'wbraganca\dynamicform\DynamicFormAsset' => false,
                // 'yii\web\JqueryAsset' =>false,
 
            ],
        ],
    ],
    'modules' => [
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            'on beforeAction' => function(yii\base\ActionEvent $event) {
            if ($event->action->uniqueId == 'user-management/auth/login') {
                $event->action->controller->layout = 'loginLayout.php';
            };
        },
        ],
        'individuo' => [
            'class' => 'frontend\modules\individuo\Individuo',
        ],
        'preparacion_platillo' => [
            'class' => 'frontend\modules\preparacion_platillo\PreparacionPlatillo',],
        'centroinfantil' => [
            'class' => 'frontend\modules\centro_infantil\Centroinfantil',
        ],
        'recursoshumanos' => [
            'class' => 'frontend\modules\recursos_humanos\Recursoshumanos',
        ],
        'preparacioncarta' => [
            'class' => 'frontend\modules\preparacion_carta\PreparacionCarta',
        ],
        'cartasemanal' => [
            'class' => 'frontend\modules\carta_semanal\Cartasemanal',
        ],
        'consumoalimenticio' => [
            'class' => 'frontend\modules\consumo_alimenticio\ConsumoAlimenticio',
        ],
        'parametrossistema' => [
            'class' => 'frontend\modules\parametros_sistema\ParametrosSistema',
        ],
        'asistencia' => [
            'class' => 'frontend\modules\asistencia\Asistencia',
        ],
        'inscripcion' => [
            'class' => 'frontend\modules\inscripcion\Matricula',
        ],
        'metricasindividuo' => [
            'class' => 'frontend\modules\metricas_individuo\Metricas_individuo',
        ],
        'tecnicoarea' => [
            'class' => 'frontend\modules\tecnico_area\Tecnico_area',
        ],
    ],
    'params' => $params,
];
