<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class FixesAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    

    
    public $css = [
                'css/fixes.css',
                'css/fixv2/fixes.css',
                'css/fixv2/wizard.css',
    ];
    public $js = [
        'js/fixes.js',
        'js/fixv2/fixes.js',
        'js/fixv2/yii2-dynamic-form.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'frontend\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}