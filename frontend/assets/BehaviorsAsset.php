<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class BehaviorsAsset extends AssetBundle
{

 public $basePath = '@webroot';
    public $baseUrl = '@web';
    
//    public $sourcePath='@bower/idea_v1.3/html/';
    
    public $css = [
//                'css/fixes.css',
    ];
    public $js = [
        'js/behaviors.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'frontend\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
