<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class UpdateYiiAsset extends AssetBundle
{

//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
    
    public $sourcePath='@vendor/components/';
    
    public $css = [
    ];
    public $js = [
        'jquery/jquery.js',
    ];
    public $depends = [

    ];

}
