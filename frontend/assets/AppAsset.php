<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
    
    public $sourcePath='@bower/idea_v1.3/html/';
    
    public $css = [
//        'bootstrap/css/bootstrap.css',
        'fonts/font-awesome/css/font-awesome.css',
        'fonts/fontello/css/fontello.css',
        'plugins/rs-plugin/css/settings.css',
        'plugins/rs-plugin/css/extralayers.css',
        'plugins/magnific-popup/magnific-popup.css',
        'css/animations.css',
        'plugins/owl-carousel/owl.carousel.css',
        'css/style.css',
     //   'css/skins/dark_cyan.css',
        'css/custom.css',
    ];
    public $js = [
//        'plugins/jquery.min.js',
//        'bootstrap/js/bootstrap.min.js',
        'plugins/modernizr.js',
        'plugins/rs-plugin/js/jquery.themepunch.tools.min.js',
        'plugins/rs-plugin/js/jquery.themepunch.revolution.min.js',
        'plugins/isotope/isotope.pkgd.min.js',
        'plugins/owl-carousel/owl.carousel.js',
        'plugins/magnific-popup/jquery.magnific-popup.min.js',
        'plugins/jquery.appear.js',
        'plugins/jquery.countTo.js',
        'plugins/jquery.parallax-1.1.3.js',
        'plugins/jquery.browser.js',
        'plugins/SmoothScroll.js',
        'js/template.js',
        'js/custom.js',



    ];
    public $depends = [
        // 'frontend\assets\UpdateYiiAsset',
        'yii\web\YiiAsset', //yii.js and jquery.js
        'yii\bootstrap\BootstrapAsset', //bootstrap.css
    ];

}
