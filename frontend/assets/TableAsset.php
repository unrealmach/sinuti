<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class TableAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    
    public $css = [
        'css/table.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset', //yii.js and jquery.js
        'yii\bootstrap\BootstrapAsset', //bootstrap.css
    ];

}
