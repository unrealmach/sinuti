<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 * @since 2.0
 */
class MixitUpAsset extends AssetBundle
{

//    public $basePath = '@webroot';
//    public $baseUrl = '@web';
    
    public $sourcePath='@bower/mixitup-master/';
    
//    public $css = [
//                'css/fixes.css',
//    ];
    public $js = [
        'src/jquery.mixitup.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
//         'frontend\assets\AppAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
