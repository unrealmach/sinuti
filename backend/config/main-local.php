<?php
$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'LlraDe246oTUA_lRjQAUKhRZy1ty5Ey7',
        ],
    ],
];

if (YII_ENV_DEV) {
//if (YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
   $config['components']['assetManager']['forceCopy'] = true; // borrar en produccion

    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';

    //para el generador gii CRUD con el template ADMIN LTE
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'generators' => [
            'crud' => [
                //TODO buscar los anteriores
//                'class' => 'common\code_generators\crud\Generator',
                'class' => 'yii\gii\generators\crud\GeneratorAdmLTE',
                'templates' => ['admin_LTE' => '@common/code_generators/crud/admin_LTE'],
            ]
        ],
       
        'allowedIPs' => ['127.0.0.1'],
    ];
}

return $config;
