<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
    'timeZone' => 'America/Lima',
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        
        'user-management' => [
            'class' => 'webvimark\modules\UserManagement\UserManagementModule',
            // 'enableRegistration' => true,
            // Here you can set your handler to change layout for any controller or action
            // Tip: you can use this event in any module
            'on beforeAction' => function(yii\base\ActionEvent $event) {
            if ($event->action->uniqueId == 'user-management/auth/login') {
                $event->action->controller->layout = 'loginLayout.php';
            };},
        ],
        'centro_infantil' => [
            'class' => 'backend\modules\centro_infantil\Centro_infantil',
        ],
        'inscripcion' => [
            'class' => 'backend\modules\inscripcion\Inscripcion',
        ],
        'individuo' => [
            'class' => 'backend\modules\individuo\Individuo',
        ],
        'parametros_sistema' => [
            'class' => 'backend\modules\parametros_sistema\Parametros_sistema',
        ],
        'recursos_humanos' => [
            'class' => 'backend\modules\recursos_humanos\Recursos_humanos',
        ],
        'asistencia' => [
            'class' => 'backend\modules\asistencia\Asistencia',
        ],
        'metricas_individuo' => [
            'class' => 'backend\modules\metricas_individuo\Metricas_individuo',
        ],
        'composicion_nutricional' => [
            'class' => 'backend\modules\composicion_nutricional\Composicion_nutricional',
        ],
        'consumo_alimenticio' => [
            'class' => 'backend\modules\consumo_alimenticio\Consumo_alimenticio',
        ],
        'carta_semanal' => [
            'class' => 'backend\modules\carta_semanal\Carta_semanal',
        ],
        'preparacion_platillo' => [
            'class' => 'backend\modules\preparacion_platillo\Preparacion_platillo',
        ],
        'racion_alimenticia' => [
            'class' => 'backend\modules\racion_alimenticia\Racion_alimenticia',
        ],
        'preparacion_carta' => [
            'class' => 'backend\modules\preparacion_carta\Preparacion_carta',
        ],
        'nutricion' => [
            'class' => 'backend\modules\nutricion\Nutricion',
        ],
        'tecnico_area' => [
            'class' => 'backend\modules\tecnico_area\Tecnico_area',
        ],
        'ubicacion' => [
            'class' => 'backend\modules\ubicacion\Ubicacion',
        ],
    ],
    'homeUrl' => '/admin', //define como /admin la url para backend
    'components' => [
        'assetManager' => [
            'appendTimestamp' => false,
        ],
        'request' => [ //indica al sistema como debe de tratar las urls 
            //   /backend/web/ cambiandolas por /admin
            'baseUrl' => '/admin',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true, // mejora la visualizacion de simbolos
            'showScriptName' => false, // eliminara index.php de la url
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
//        'i18n' => [
//            'translations' => [
//                'yii' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'sourceLanguage' => 'en-US',
//                    'basePath' => '@app/messages'
//                ],
//            ],
//        ],
//        'view' => [
//            'theme' => [
//                'pathMap' => [
//                    '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-advanced-app'
//                ],
//            ],
//        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue-light',
                ],
            ],
        ],
    ],
    'params' => $params,
];
