<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Mauricio Chamorro
 * @since 2.0
 */
class ThemeAsset extends AssetBundle
{

    public $sourcePath = '@vendor/almasaeed2010/adminlte';
    public $css = [
        'plugins/iCheck/all.css',    
    ];
    public $js = [
        'plugins/iCheck/icheck.min.js',
        'plugins/slimScroll/jquery.slimscroll.min.js',
    ];
    public $depends = [
        'dmstr\web\AdminLteAsset',
    ];

}
