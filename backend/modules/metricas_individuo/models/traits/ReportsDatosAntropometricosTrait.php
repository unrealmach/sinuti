<?php
namespace backend\modules\metricas_individuo\models\traits;

use \backend\modules\metricas_individuo\models\DatoAntropometrico;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 */
trait ReportsDatosAntropometricosTrait
{

    public static function getMetricasByCibvAndMonth($cibv_id, $keyMetrica, $metrica, $fecha_ini, $fecha_fin)
    {

//        select cen_inf_id, dato_antropometrico.*   from dato_antropometrico
//inner join infante on dato_antropometrico.infante_id = infante.infante_id
//left join sector_asignado_infante on infante.infante_id = sector_asignado_infante.infante_id
//inner join sector_asignado_educador on sector_asignado_educador.sector_asignado_educador_id = sector_asignado_infante.sector_asignado_id
//
//where dat_antro_interpretacion_peso = 'PESONORMAL' and (dato_antropometrico.dat_antro_fecha_registro between '2016-04-01 00:00:00' and '2016-04-30 23:59:59')
// and  cen_inf_id = 1

        $rows = (new \yii\db\Query())
            ->select('cen_inf_id, dato_antropometrico.*')
            ->from('dato_antropometrico')
            ->join('INNER JOIN', 'infante', 'dato_antropometrico.infante_id = infante.infante_id')
            ->join('LEFT JOIN', 'sector_asignado_infante', 'infante.infante_id = sector_asignado_infante.infante_id')
            ->join('INNER JOIN', 'sector_asignado_educador', 'sector_asignado_educador.sector_asignado_educador_id = sector_asignado_infante.sector_asignado_id');
        switch ($keyMetrica) {
            case 'PESO':
                switch ($metrica) {
                    case 'BAJOPESO': $rows->where(['dat_antro_interpretacion_peso' => 'BAJOPESO']);
                        break;
                    case 'RIESGOBAJOPESO': $rows->where(['dat_antro_interpretacion_peso' => 'RIESGOBAJOPESO']);
                        break;
                    case 'PESONORMAL': $rows->where(['dat_antro_interpretacion_peso' => 'PESONORMAL']);
                        break;
                    case 'RIESGOSOBREPESO': $rows->where(['dat_antro_interpretacion_peso' => 'RIESGOSOBREPESO']);
                        break;
                    case 'SOBREPESO': $rows->where(['dat_antro_interpretacion_peso' => 'SOBREPESO']);
                        break;
                }
                break;
            case 'TALLA':
                switch ($metrica) {
                    case 'BAJATALLASEVERA': $rows->where(['dat_antro_interpretacion_talla' => 'BAJATALLASEVERA']);
                        break;
                    case 'TALLABAJA': $rows->where(['dat_antro_interpretacion_talla' => 'TALLABAJA']);
                        break;
                    case 'TALLANORMAL': $rows->where(['dat_antro_interpretacion_talla' => 'TALLANORMAL']);
                        break;
                    case 'TALLAALTA': $rows->where(['dat_antro_interpretacion_talla' => 'TALLAALTA']);
                        break;
                    case 'TALLAMUYALTA': $rows->where(['dat_antro_interpretacion_talla' => 'TALLAMUYALTA']);
                        break;
                }
                break;
            case 'DESNUTRICIONTALLA':
                switch ($metrica) {
                    case 'DESNUTRICIONCRONICA': $rows->where(['dato_antro_desnutricion_talla' => 'DESNUTRICIONCRONICA']);
                        break;
                    case 'NOAPLICA': $rows->where(['dato_antro_desnutricion_talla' => 'NOAPLICA']);
                        break;
                }
                break;
            case 'DESNUTRICIONPESO': switch ($metrica) {
                    case 'DESNUTRICIONGLOBAL': $rows->where(['dato_antro_desnutricion_peso' => 'DESNUTRICIONGLOBAL']);
                        break;
                    case 'DESNUTRICIONAGUDA': $rows->where(['dato_antro_desnutricion_peso' => 'DESNUTRICIONAGUDA']);
                        break;
                    case 'NOAPLICA': $rows->where(['dato_antro_desnutricion_peso' => 'NOAPLICA']);
                        break;
                }
                break;
        }
//        var_dump($cibv_id);die();
        
        $rows->andWhere(['between', 'dato_antropometrico.dat_antro_fecha_registro', $fecha_ini, $fecha_fin])
            ->andWhere([ 'cen_inf_id' => $cibv_id]);
//            ->all();
//        print_r($rows->createCommand()->getRawSql());
//        die();
        return $rows->all();
    }

    /**
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function getDataToHighchart($cibv_id, $days, $metricas)
    {
        $dataGrid = [];
        $subDataG = [];
        $dataHighCharttemp = [];
        $cantidadMetricas = [];
        $cantidadSubmetricas = [];
//        $cantidadMetricas=['PESO'=>0,
//                            'TALLA'=>0];
//        $cantidadSubMetrica=['PESO'=>[
//            'BAJOPESO'=>0,
//            'PESONORMAL'=>0]
//            ];
//            
//            
//        var_dump($dataGrid['PESO']['PESONORMAL']['2016-04-13']);
        //analiza cada metrica para cada dia de un mes en especifico
        foreach ($metricas as $keyMetrica => $metrica) {
            $cantidadMetrica = 0;
            foreach ($metrica as $key2 => $submetrica) {
                $cantidaSubmetrica = 0;
                $listaInfantesSubmetrica = [];
                foreach ($days as $day) {

                    $datatemp = DatoAntropometrico::getMetricasByCibvAndMonth($cibv_id, $keyMetrica, $submetrica, $day . " 00:00:00", $day . ' 23:59:59');
                   
                    if (!empty($datatemp)) {
                        foreach ($datatemp as $key => $value) {
                            array_push($listaInfantesSubmetrica, $value['infante_id']);
                        }
                    }
//                    $dataHighCharttemp[$keyMetrica][$submetrica][$day] = count($datatemp);
                    $cantidadMetrica+=count($datatemp);
                    $cantidaSubmetrica+=count($datatemp);
                }
                 $dataGrid[$keyMetrica][$submetrica] = $listaInfantesSubmetrica;
                $cantidadSubmetricas[$keyMetrica][$submetrica] = $cantidaSubmetrica;
            }
            $cantidadMetricas[$keyMetrica] = $cantidadMetrica;
        }

       
////        var_dump($dataGrid['PESO']['PESONORMAL']['2016-04-13']);
//        die();
        $level1 = array();
//
        $level1[] = array('name' => 'PESO', 'y' => $cantidadMetricas['PESO'], 'drilldown' => 'dd1');
        $level1[] = array('name' => 'TALLA', 'y' => $cantidadMetricas['TALLA'], 'drilldown' => 'dd2');
        $level1[] = array('name' => 'DESNUTRICIÓN TALLA', 'y' => $cantidadMetricas['DESNUTRICIONTALLA'], 'drilldown' => 'dd3');
        $level1[] = array('name' => 'DESNUTRICIÓN PESO', 'y' => $cantidadMetricas['DESNUTRICIONPESO'], 'drilldown' => 'dd4');


        $level2 = array();
        $level2[] = array('id' => 'dd1', 'data' => [
                ['BAJO PESO', $cantidadSubmetricas['PESO']['BAJOPESO']],
                ['RIESGO BAJO PESO', $cantidadSubmetricas['PESO']['RIESGOBAJOPESO']],
                ['PESO NORMAL', $cantidadSubmetricas['PESO']['PESONORMAL']],
                ['RIESGO SOBREPESO', $cantidadSubmetricas['PESO']['RIESGOSOBREPESO']],
                ['SOBREPESO', $cantidadSubmetricas['PESO']['SOBREPESO']],
        ]);
        $level2[] = array('id' => 'dd2', 'data' => [
                ['BAJATALLA SEVERA', $cantidadSubmetricas['TALLA']['BAJATALLASEVERA']],
                ['TALLA BAJA', $cantidadSubmetricas['TALLA']['TALLABAJA']],
                ['TALLA NORMAL', $cantidadSubmetricas['TALLA']['TALLANORMAL']],
                ['TALLA ALTA', $cantidadSubmetricas['TALLA']['TALLAALTA']],
                ['TALLA MUYALTA', $cantidadSubmetricas['TALLA']['TALLAMUYALTA']],
        ]);
        $level2[] = array('id' => 'dd3', 'data' => [
                ['DESNUTRICIÓN CRÓNICA', $cantidadSubmetricas['DESNUTRICIONTALLA']['DESNUTRICIONCRONICA']],
                ['NO APLICA', $cantidadSubmetricas['DESNUTRICIONTALLA']['NOAPLICA']],
        ]);
        $level2[] = array('id' => 'dd4', 'data' => [
                ['DESNUTRICIÓN GLOBAL', $cantidadSubmetricas['DESNUTRICIONPESO']['DESNUTRICIONGLOBAL']],
                ['DESNUTRICIÓN AGUDA', $cantidadSubmetricas['DESNUTRICIONPESO']['DESNUTRICIONAGUDA']],
                ['NO APLICA', $cantidadSubmetricas['DESNUTRICIONPESO']['NOAPLICA']],
        ]);

// var_dump($level1);
//        var_dump($level2);
//        die();

        return ['level1' => $level1, 'level2' => $level2,'listaInfantes'=>$dataGrid];
    }
}
