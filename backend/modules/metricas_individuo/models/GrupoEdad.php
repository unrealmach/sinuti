<?php

namespace backend\modules\metricas_individuo\models;
use backend\modules\inscripcion\models\Salon;

use Yii;

/**
 * This is the model class for table "grupo_edad".
 *
 * @property integer $grupo_edad_id
 * @property string $grupo_edad_descripcion
 *
 * @property MPrepCarta[] $mPrepCartas
 * @property RangoNutriente[] $rangoNutrientes
 * @property Salon[] $salons
 */
class GrupoEdad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grupo_edad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_edad_descripcion'], 'required'],
            [['grupo_edad_descripcion'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grupo_edad_id' => Yii::t('app', 'Grupo Edad ID'),
            'grupo_edad_descripcion' => Yii::t('app', 'Descripción'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPrepCartas()
    {
        return $this->hasMany(MPrepCarta::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRangoNutrientes()
    {
        return $this->hasMany(RangoNutriente::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalons()
    {
        return $this->hasMany(Salon::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }
}
