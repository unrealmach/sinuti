<?php

namespace backend\modules\metricas_individuo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\metricas_individuo\models\GrupoEdad;

/**
 * GrupoedadSearch represents the model behind the search form about `backend\modules\metricas_individuo\models\GrupoEdad`.
 */
class GrupoedadSearch extends GrupoEdad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_edad_id'], 'integer'],
            [['grupo_edad_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrupoEdad::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'grupo_edad_id' => $this->grupo_edad_id,
        ]);

        $query->andFilterWhere(['like', 'grupo_edad_descripcion', $this->grupo_edad_descripcion]);

        return $dataProvider;
    }
}
