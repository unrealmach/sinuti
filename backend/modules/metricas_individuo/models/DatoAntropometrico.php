<?php
namespace backend\modules\metricas_individuo\models;

use backend\modules\metricas_individuo\components\dato_antropometrico\tablasOMS;
use Yii;
use backend\modules\individuo\models\Infante;
use backend\modules\metricas_individuo\components\dato_antropometrico\rangosCurvasOms;

/**
 * This is the model class for table "dato_antropometrico".
 *
 * @property integer $dat_antro_id
 * @property integer $infante_id
 * @property string $dat_antro_fecha_registro
 * @property integer $dat_antro_edad_meses
 * @property string $dat_antro_talla_infante
 * @property string $dat_antro_peso_infante
 * @property string $dat_antro_interpretacion_peso
 * @property string $dat_antro_interpretacion_talla
 * @property string $dato_antro_desnutricion_peso
 * @property string $dato_antro_desnutricion_talla
 * @property string $dat_antro_imc_infante
 *
 * @property Infante $infante
 */
class DatoAntropometrico extends \yii\db\ActiveRecord
{

    use \backend\modules\metricas_individuo\models\traits\ReportsDatosAntropometricosTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dato_antropometrico';
    }

    const BAJO_PESO = "BAJOPESO";
    const RIESGO_BAJO_PESO = "RIESGOBAJOPESO";
    const PESO_NORMAL = "PESONORMAL";
    const RIESGO_SOBRE_PESO = "RIESGOSOBREPESO";
    const SOBRE_PESO = "SOBREPESO";
    const TALLA_BAJA_SEVERA = "BAJATALLASEVERA";
    const TALLA_BAJA = "TALLABAJA";
    const TALLA_NORMAL = "TALLANORMAL";
    const TALLA_ALTA = "TALLAALTA";
    const TALLA_MUY_ALTA = "TALLAMUYALTA";
    const DESNUTRICION_CRONICA = "DESNUTRICIONCRONICA";
    const DESNUTRICION_GLOBAL = "DESNUTRICIONGLOBAL";
    const DESNUTRICION_AGUDA = "DESNUTRICIONAGUDA";
    const NO_APLICA = "NOAPLICA";

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_id', 'dat_antro_edad_meses','dat_antro_talla_infante','dat_antro_peso_infante','dat_antro_fecha_registro'], 'required'],
            [['infante_id', 'dat_antro_edad_meses'], 'integer'],
            [['dat_antro_fecha_registro'], 'safe'],
            [['dat_antro_talla_infante'], 'number', 'min' => 60.00],
            [['dat_antro_peso_infante'], 'number', 'min' => 6.00],
            [['dat_antro_imc_infante', 'dat_antro_talla_infante', 'dat_antro_peso_infante', 'dat_antro_imc_infante', 'dat_antro_interpretacion_peso',
                'dat_antro_interpretacion_talla', 'dato_antro_desnutricion_talla',
                'dato_antro_desnutricion_peso'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dat_antro_id' => Yii::t('app', 'Dat Antro ID'),
            'infante_id' => Yii::t('app', 'Infante'),
            'dat_antro_fecha_registro' => Yii::t('app', 'Fecha de Registro'),
            'dat_antro_edad_meses' => Yii::t('app', 'Edad en Meses'),
            'dat_antro_talla_infante' => Yii::t('app', 'Talla en [cm]'),
            'dat_antro_peso_infante' => Yii::t('app', 'Peso en [Kg]'),
            'dat_antro_imc_infante' => Yii::t('app', 'Imc'),
            'dat_antro_interpretacion_peso' => Yii::t('app', 'Interpretación Peso'),
            'dat_antro_interpretacion_talla' => Yii::t('app', 'Interpretación Talla'),
            'dato_antro_desnutricion_talla' => Yii::t('app', 'Desnutrición Crónica'),
            'dato_antro_desnutricion_peso' => Yii::t('app', 'Desnutrición Global o Aguda'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /** @inheritdoc
     * @return \backend\modules\metricas_individuo\models\sctivesqueries\DatoAntropometricoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\metricas_individuo\models\sctivesqueries\DatoAntropometricoQuery(get_called_class());
    }

    /**
     * Función que genera el reporte para IMC, peso o talla  del infante
     * @param date $inicio fecha de nacimiento //todo quitar variable
     * @param int $idNinio identificador del niño
     * @param String $tipo puede ser imc, talla o peso
     * @author Mauricio Chamorro <unrealmac@hotmail.com>
     * @return array
     */
    public function generarReport($inicio = null, $idNinio, $tipo)
    {
        $report = array();
        $report['xAxis']['categories'] = array();
        $report['series'] = array();
        $data = array();
        $i = 0;
        $j = 0;

        for ($index = 0; $index < 61; $index++) {
            if ($i == 12) {
                $j++;
                $i = 0;
            }
            $categoria = ($i == 0) ? $j . "<b>" . " año(s) " . "</b>" : $i;
            $report['xAxis']['categories'][] = $categoria;
            switch ($tipo) {
                case "imc": $cons = $this->consulta($idNinio, null, null, "imc", $index);
                    break;
                case "talla": $cons = $this->consulta($idNinio, null, null, "talla", $index);
                    break;
                case "peso": $cons = $this->consulta($idNinio, null, null, "peso", $index);
                    break;

                default:
                    break;
            }
            if ($cons[0] != 0 || $cons[0] != null || !empty($cons[0])) {
                array_pop($report['xAxis']['categories']);
                $report['xAxis']['categories'][] = "<strong>  (" . $cons[0] . ") ----> </strong>" . $categoria;
            }
            array_push($data, $cons[0]);
            $i++;
        }
        array_push($report['xAxis']['categories'], '5<b> año(s) </b>');
        $ninio = Infante::findOne((int) $idNinio);

        switch ($tipo) {
            case "imc": {
                    $curvaN3D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "-3DE");
                    $curvaN2D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "-2DE");
                    $curvaN1D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "-1DE");
                    $curvaMediana = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "mediana");
                    $curva1D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "1DE");
                    $curva2D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "2DE");
                    $curva3D = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "3DE");
                    $curvaMAXD = tablasOMS::getPosImcIndiceZ($ninio->infante_genero, "MAXDE");
//                    array_push($report['series'], array('name' => "IMC de" . Ninios::model()->findByPk((int) $idNinio), 'data' => $data, 'type' => 'line', 'color' => "#000000", "marker" => array("enabled" => true), 'zIndex' => 60));
                    array_push($report['series'], array('name' => "IMC de " . Infante::findOne((int) $idNinio)->nombreCompleto, 'data' => $data, 'type' => 'line', 'color' => "#000000", "marker" => array("enabled" => true), 'zIndex' => 60));
                    array_push($report['series'], array('name' => "Sobrepeso", 'data' => $curvaMAXD, 'type' => 'area', 'color' => '#8DD62B')); //percentil3
                    array_push($report['series'], array('name' => "Riesgo sobrepeso", 'data' => $curva3D, 'type' => 'area', 'color' => '#8DD62B')); //percentil3
                    array_push($report['series'], array('name' => "Peso normal", 'data' => $curva2D, 'type' => 'area', 'color' => '#33FF33')); //percentil3
                    array_push($report['series'], array('name' => "Mediana", 'data' => $curvaMediana, 'type' => 'spline', 'color' => 'blue',)); //percentil3
                    array_push($report['series'], array('name' => "Riesgo Bajo peso", 'data' => $curvaN2D, 'type' => 'area', 'color' => 'pink',)); //percentil3
                    array_push($report['series'], array('name' => "Bajo peso", 'data' => $curvaN3D, 'type' => 'area', 'color' => '#FF6666',)); //percentil3
                }break;
            case "talla": {
                    $curvaN3D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "-3DE");
                    $curvaN2D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "-2DE");
                    $curvaN1D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "-1DE");
                    $curvaMediana = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "mediana");
                    $curva1D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "1DE");
                    $curva2D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "2DE");
                    $curva3D = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "3DE");
                    $curvaMAXD = tablasOMS::getPosTallaIndiceZ($ninio->infante_genero, "MAXDE");
                    array_push($report['series'], array('name' => "Talla de " . Infante::findOne((int) $idNinio)->nombreCompleto, 'data' => $data, 'type' => 'line', 'color' => "#000000", "marker" => array("enabled" => true), 'zIndex' => 60));
                    array_push($report['series'], array('name' => "Talla muy Alta", 'data' => $curvaMAXD, 'type' => 'area', 'color' => '#8DD62B')); //percentil3
                    array_push($report['series'], array('name' => "Talla Alta", 'data' => $curva3D, 'type' => 'area', 'color' => '#8DD62B')); //percentil3
                    array_push($report['series'], array('name' => "Talla Normal", 'data' => $curva2D, 'type' => 'area', 'color' => '#33FF33')); //percentil3
                    array_push($report['series'], array('name' => "Mediana", 'data' => $curvaMediana, 'type' => 'spline', 'color' => 'blue',)); //percentil3
                    array_push($report['series'], array('name' => "Talla Baja", 'data' => $curvaN2D, 'type' => 'area', 'color' => 'pink',)); //percentil3
                    array_push($report['series'], array('name' => "Baja Talla Severa", 'data' => $curvaN3D, 'type' => 'area', 'color' => '#FF6666',)); //percentil3
                }break;
            case "peso": {
                    $curvaN3D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "-3DE");
                    $curvaN2D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "-2DE");
                    $curvaN1D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "-1DE");
                    $curvaMediana = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "mediana");
                    $curva1D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "1DE");
                    $curva2D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "2DE");
                    $curva3D = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "3DE");
                    $curvaMAXD = tablasOMS::getPosPesoIndiceZ($ninio->infante_genero, "MAXDE");

                    array_push($report['series'], array('identificadorNombre' => "ok", 'name' => "Peso de " . Infante::findOne((int) $idNinio)->nombreCompleto, 'data' => $data, 'type' => 'line', 'color' => "#000000", "marker" => array("enabled" => true), 'zIndex' => 60));
                    array_push($report['series'], array('name' => "Peso Elevado Severo ", 'data' => $curvaMAXD, 'type' => 'area', 'color' => '#FF6666')); //percentil3
                    array_push($report['series'], array('name' => "Peso elevado", 'data' => $curva3D, 'type' => 'area', 'color' => '#FF9933')); //percentil3
                    array_push($report['series'], array('name' => "Peso Normal", 'data' => $curva2D, 'type' => 'area', 'color' => '#33FF33')); //percentil3
                    array_push($report['series'], array('name' => "Mediana", 'data' => $curvaMediana, 'type' => 'spline', 'color' => 'blue',)); //percentil3
                    array_push($report['series'], array('name' => "Bajo Peso", 'data' => $curvaN2D, 'type' => 'area', 'color' => 'pink',)); //percentil3
                    array_push($report['series'], array('name' => "Bajo Peso Severo", 'data' => $curvaN3D, 'type' => 'area', 'color' => '#FF6666',)); //percentil3
                }

            default:
                break;
        }
        return $report;
    }
//    TODO hacer consulta para rangos de imc de acuerdo a la edad sprint 3
//    SELECT * FROM `datos_antropomorficos` WHERE `fecha` between '2014-10-01 00:00:00' and '2014-10-31 23:59:59' and `edad`= 22 and `peso`

    /**
     * Consulta para el dato especifico segun la variable (tipo) de un niño en un rango de fechas
     * @param int $idninio identificador del infante
     * @param date $inicio fecha de inicio de la busqueda
     * @param date $fin fecha de fin de la busqueda
     * @param String $tipo puede ser imc, peso, talla o all
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    public function consulta($idninio, $inicio = null, $fin = null, $tipo, $edad)
    {
        $data = array();
        $command = new \yii\db\Query();
//        $command = Yii::app()->db->createCommand();
        switch ($tipo) {
            case "imc": $command->select('da.dat_antro_imc_infante  as imc');
                break;
            case "talla": $command->select('da.dat_antro_talla_infante  as talla');
                break;
            case "peso": $command->select('da.dat_antro_peso_infante  as peso');
                break;
            case "all": $command->select('da.dat_antro_imc_infante  as imc,da.dat_antro_talla_infante  as talla,da.dat_antro_peso_infante  as peso');
                break;

            default:
                break;
        }
        $command->from('dato_antropometrico  da')
            ->leftJoin("infante n", "da.infante_id=n.infante_id")
            ->where("n.infante_id=$idninio")
            ->andWhere("da.dat_antro_edad_meses =$edad")
            ->orderBy("da.dat_antro_fecha_registro DESC")
//                ->order("da.dat_antro_fecha_registro DESC")
        ;
//        $options = $command->queryAll();
        $options = $command->all();
        if (!empty($options)) {
            switch ($tipo) {
                case "imc":$data[] = (float) $options[0]['imc'];
                    break;
                case "talla":$data[] = (float) $options[0]['talla'];
                    break;
                case "peso":$data[] = (float) $options[0]['peso'];
                    break;
                default:
                    break;
            }
        } else {
            $data[] = null;
        }
        return $data;
    }

    /**
     * ----------------EVENTOS-----------------
     */
    const INTERPRETACION_PESO = 'interpretacion-peso';
    const INTERPRETACION_TALLA = 'interpretacion-talla';
    const INTERPRETACION_DESNUTRICION_TALLA = 'interpretacion-desnutricion-talla';
    const INTERPRETACION_DESNUTRICION_PESO = 'interpretacion-desnutricion-peso';

    /**
     * Evento que recupara la interpretación del peso para la edad actual del infante
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function pesoInfante()
    {
        $infante_id = $this->infante_id;
        $infante = $infante = Infante::findOne(['infante_id' => $infante_id]);
        $this->dat_antro_interpretacion_peso = rangosCurvasOms::getRangoInterpretadoPeso($infante->infante_genero, $this->dat_antro_edad_meses, $this->dat_antro_peso_infante);
    }

    /**
     * Evento que recupara la interpretación de la talla para la edad actual del infante
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function tallaInfante()
    {
        $infante_id = $this->infante_id;
        $infante = $infante = Infante::findOne(['infante_id' => $infante_id]);
        $this->dat_antro_interpretacion_talla = rangosCurvasOms::getRangoInterpretadoTalla($infante->infante_genero, $this->dat_antro_edad_meses, $this->dat_antro_talla_infante);
    }

    /**
     * Evento que recupara la interpretación de la desnutricion cronica de acuerdo 
     * a la talla para la edad actual del infante
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function desnutricionCronica()
    {
        $infante_id = $this->infante_id;
        $infante = $infante = Infante::findOne(['infante_id' => $infante_id]);
        $this->dato_antro_desnutricion_talla = rangosCurvasOms::getRangoInterpretadoDesnutricionTalla($infante->infante_genero, $this->dat_antro_edad_meses, $this->dat_antro_talla_infante);
    }

    /**
     * Evento que recupara la interpretación de la desnutricion global o aguda
     * de acuerdo al  peso para la talla o edad actual del infante
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function desnutricionGlobalAndAguda()
    {
        $infante_id = $this->infante_id;
        $infante = $infante = Infante::findOne(['infante_id' => $infante_id]);
        $this->dato_antro_desnutricion_peso = rangosCurvasOms::getRangoInterpretadoDesnutricionPeso($infante->infante_genero, $this->dat_antro_edad_meses, $this->dat_antro_peso_infante);
    }

    /**
     * Permite hacer la llamada de los eventos 
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function init()
    {
        $this->on(self::INTERPRETACION_PESO, [$this, 'pesoInfante']);
        $this->on(self::INTERPRETACION_TALLA, [$this, 'tallaInfante']);
        $this->on(self::INTERPRETACION_DESNUTRICION_TALLA, [$this, 'desnutricionCronica']);
        $this->on(self::INTERPRETACION_DESNUTRICION_PESO, [$this, 'desnutricionGlobalAndAguda']);
        return parent::init();
    }
}
