<?php

namespace backend\modules\metricas_individuo\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\metricas_individuo\models\DatoAntropometrico;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\parametros_sistema\models\Periodo;
use \backend\modules\rbac\models\AuthAssignment;

/**
 * DatoantropometricoSearch represents the model behind the search form about `backend\modules\metricas_individuo\models\DatoAntropometrico`.
 */
class DatoantropometricoInactivoSearch extends DatoAntropometrico {

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['dat_antro_id'], 'integer'],
                [['dat_antro_fecha_registro', 'infante_id', 'dat_antro_edad_meses'],
                'safe'],
                [['dat_antro_talla_infante', 'dat_antro_peso_infante', 'dat_antro_imc_infante',
                'dat_antro_interpretacion_peso',
                'dat_antro_interpretacion_talla', 'dato_antro_desnutricion_talla',
                'dato_antro_desnutricion_peso'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DatoAntropometrico::find();

        $query->joinWith('infante', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->join('inner join', 'matricula',
            'dato_antropometrico.infante_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'RETIRADO' ");

        $authAsiignmentMdel = AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $periodo            = Periodo::find()->periodoActivo()->one();
        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador' || 'Admin'||'coordinador-gad': // si rol es coordinador le permite ver las metricas de los infantes que pertenecen al centro infantil durante un periodo activo
                $arrayIds = $this->getCoincidenciasCoordinador($periodo);
                $query->andWhere(['IN', 'matricula.infante_id', $arrayIds]);
                break;

            case 'educador': // si rol es educador le permite ver las metricas de los infantes que pertenecen al salon del educador durante un periodo activo
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasEducador($periodo);
                $query->andWhere(['IN', 'matricula.infante_id', $arrayIds]);
                break;

//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'dat_antro_id' => $this->dat_antro_id,
            'dat_antro_edad_meses' => $this->dat_antro_edad_meses,
            'dat_antro_talla_infante' => $this->dat_antro_talla_infante,
            'dat_antro_peso_infante' => $this->dat_antro_peso_infante,
            'dat_antro_imc_infante' => $this->dat_antro_imc_infante,
        ]);

//        $query->andFilterWhere(
//            ['like', 'infante.infante_dni', $this->infante_id]
//        );
        $query->andFilterWhere(['like', 'dat_antro_fecha_registro',
            $this->dat_antro_fecha_registro]);
        $query->andFilterWhere(['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " " ,infante.infante_apellidos )'),
            $this->infante_id]);


        return $dataProvider;
    }

    private function getCoincidenciasCoordinador($periodo)
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            if (!empty($autoridadCibv)) {
                $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id,
                        null)->all();
                if (!empty($infantesbyCibv)) {
                    foreach ($infantesbyCibv as $value) {
                        array_push($arrayIds, $value->infante_id);
                    }
                }
            }
        }
        return $arrayIds;
    }

    private function getCoincidenciasEducador($periodo)
    {
        $arrayIds = [];

        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->infante_id);
                }
            }
        }
        return $arrayIds;
    }

}
