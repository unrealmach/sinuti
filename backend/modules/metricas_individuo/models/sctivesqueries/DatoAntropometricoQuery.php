<?php

namespace backend\modules\metricas_individuo\models\sctivesqueries;

/**
 * This is the ActiveQuery class for [[\backend\modules\metricas_individuo\models\DatoAntropometrico]].
 *
 * @see \backend\modules\metricas_individuo\models\DatoAntropometrico
 */
class DatoAntropometricoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\metricas_individuo\models\DatoAntropometrico[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\metricas_individuo\models\DatoAntropometrico|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * Devuelve las metricas de un infante de acuerdo a la edad
     * @param int $edad
     * @param int $infante_id
     * @author Sofia Mejia amandasofiamejia@gmail.com
     * @return query
     */
    public function metricaByEdad($edad, $infante_id){
        return $this->where("dat_antro_edad_meses = :edad AND infante_id = :infante_id")
                ->addParams([":edad" => $edad, ":infante_id" => $infante_id]);
    }
}