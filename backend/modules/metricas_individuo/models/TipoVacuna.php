<?php

namespace backend\modules\metricas_individuo\models;

use Yii;

/**
 * This is the model class for table "tipo_vacuna".
 *
 * @property integer $tipo_vacuna_id
 * @property string $tipo_vac_enfermedad
 * @property string $tipo_vac_nombre
 * @property integer $tipo_vac_num_dosis
 * @property string $tipo_vac_dosis_canti
 * @property string $tipo_vac_dosis_medida
 * @property string $tipo_vac_via
 *
 * @property RegistroVacunaInfante[] $registroVacunaInfantes
 */
class TipoVacuna extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_vacuna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_vac_enfermedad', 'tipo_vac_nombre', 'tipo_vac_num_dosis', 'tipo_vac_dosis_canti', 'tipo_vac_dosis_medida', 'tipo_vac_via'], 'required'],
            [['tipo_vac_num_dosis'], 'integer'],
            [['tipo_vac_dosis_canti'], 'number'],
            [['tipo_vac_dosis_medida', 'tipo_vac_via'], 'string'],
            [['tipo_vac_enfermedad', 'tipo_vac_nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tipo_vacuna_id' => Yii::t('app', 'Tipo Vacuna ID'),
            'tipo_vac_enfermedad' => Yii::t('app', 'Enfermedad que previene'),
            'tipo_vac_nombre' => Yii::t('app', 'Nombre Vacuna'),
            'tipo_vac_num_dosis' => Yii::t('app', 'Dosis'),
            'tipo_vac_dosis_canti' => Yii::t('app', 'Cantidad'),
            'tipo_vac_dosis_medida' => Yii::t('app', 'Medida'),
            'tipo_vac_via' => Yii::t('app', 'Vía'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistroVacunaInfantes()
    {
        return $this->hasMany(RegistroVacunaInfante::className(), ['tipo_vacuna_id' => 'tipo_vacuna_id']);
    }
}
