<?php

namespace backend\modules\metricas_individuo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\metricas_individuo\models\TipoVacuna;

/**
 * TipovacunaSearch represents the model behind the search form about `backend\modules\metricas_individuo\models\TipoVacuna`.
 */
class TipovacunaSearch extends TipoVacuna
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_vacuna_id', 'tipo_vac_num_dosis'], 'integer'],
            [['tipo_vac_enfermedad', 'tipo_vac_nombre', 'tipo_vac_dosis_medida', 'tipo_vac_via'], 'safe'],
            [['tipo_vac_dosis_canti'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipoVacuna::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipo_vacuna_id' => $this->tipo_vacuna_id,
            'tipo_vac_num_dosis' => $this->tipo_vac_num_dosis,
            'tipo_vac_dosis_canti' => $this->tipo_vac_dosis_canti,
        ]);

        $query->andFilterWhere(['like', 'tipo_vac_enfermedad', $this->tipo_vac_enfermedad])
            ->andFilterWhere(['like', 'tipo_vac_nombre', $this->tipo_vac_nombre])
            ->andFilterWhere(['like', 'tipo_vac_dosis_medida', $this->tipo_vac_dosis_medida])
            ->andFilterWhere(['like', 'tipo_vac_via', $this->tipo_vac_via]);

        return $dataProvider;
    }
}
