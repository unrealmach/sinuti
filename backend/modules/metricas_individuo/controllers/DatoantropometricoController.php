<?php

namespace backend\modules\metricas_individuo\controllers;

use Yii;
use backend\modules\metricas_individuo\models\DatoAntropometrico;
use backend\modules\metricas_individuo\models\DatoantropometricoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\individuo\models\Infante;

/**
 * DatoantropometricoController implements the CRUD actions for DatoAntropometrico model.
 */
class DatoantropometricoController extends Controller {

    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DatoAntropometrico models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new DatoantropometricoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DatoAntropometrico model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DatoAntropometrico model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new DatoAntropometrico();

        if ($model->load(Yii::$app->request->post())) {
            $imc = $model->dat_antro_peso_infante / pow($model->dat_antro_talla_infante / 100, 2);
            $model->dat_antro_imc_infante = number_format($imc, 2);
            $model->trigger(DatoAntropometrico::INTERPRETACION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_TALLA);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_PESO);
            $model->trigger(DatoAntropometrico::INTERPRETACION_DESNUTRICION_TALLA);
            if ($model->save())
                return $this->redirect(['view', 'id' => $model->dat_antro_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DatoAntropometrico model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->dat_antro_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DatoAntropometrico model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DatoAntropometrico model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DatoAntropometrico the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = DatoAntropometrico::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    public function actionAjaxObtenerReporteIMC($idNinio)
//    {
////        $ninio = Ninios::model()->findByPk((int) $idNinio);
//        $ninio = Infante::findOne((int) $idNinio);
//        $inicio = $ninio->infantes_fecha_nacimiento;
//        $id = $ninio->infante_id;
////        $reporteIMC = DatoAntropometrico::model()->generarReport($inicio, $id, "imc");
////        $reporteIMC = new DatoAntropometrico() ;
////        $reporteIMC = $reporteIMC->generarReport($inicio,$id,"imc");
////        $reporteTalla = new DatoAntropometrico() ;
////        $reporteTalla = $reporteTalla->generarReport($inicio,$id,"talla");
//        $reportePeso = new DatoAntropometrico() ;
//        $reportePeso = $reportePeso->generarReport($inicio,$id,"peso");
//        
////        $reporteTalla = DatoAntropometrico::model()->generarReport($inicio, $id, "talla");
////        $reportePeso = DatoAntropometrico::model()->generarReport($inicio, $id, "peso");
////        $datosTabla = DatoAntropometrico::model()->generarArrayDatos($inicio, $id);
//
//        $this->render("/../views/ninios/reports/index", array(
////            "report" => $reporteIMC,
////            "reportTalla" => $reporteTalla,
//            "reportPeso" => $reportePeso,
////            "datosTabla" => $datosTabla,
////            "ninio" => $ninio,
//        ));
//    }
}
