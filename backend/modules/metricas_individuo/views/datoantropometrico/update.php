<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dato Antropometrico',
]) . ' ' . $model->dat_antro_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dato Antropometricos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->dat_antro_id, 'url' => ['view', 'id' => $model->dat_antro_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dato-antropometrico-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
