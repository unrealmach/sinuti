<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoantropometricoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dato-antropometrico-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'dat_antro_id') ?>

    <?= $form->field($model, 'infante_id') ?>

    <?= $form->field($model, 'dat_antro_fecha_registro') ?>

    <?= $form->field($model, 'dat_antro_edad_meses') ?>

    <?= $form->field($model, 'dat_antro_talla_infante') ?>

    <?php // echo $form->field($model, 'dat_antro_peso_infante') ?>

    <?php // echo $form->field($model, 'dat_antro_imc_infante') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
