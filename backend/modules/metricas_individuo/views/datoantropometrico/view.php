<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */

$this->title = $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dato Antropometricos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dato-antropometrico-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->dat_antro_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->dat_antro_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])

            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'dat_antro_id',
                'infante.nombreCompleto',
                'dat_antro_fecha_registro',
                'dat_antro_edad_meses',
                'dat_antro_talla_infante',
                'dat_antro_peso_infante',
                'dat_antro_imc_infante',
            ],
        ])

        ?>

    </div>
</div>
