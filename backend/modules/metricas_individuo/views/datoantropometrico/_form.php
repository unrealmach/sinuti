<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\individuo\models\Infante;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\DatoAntropometrico */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="dato-antropometrico-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Infante::find()->all(), 'infante_id', 'infante_dni', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
        <?=
            $form->field($model, 'dat_antro_fecha_registro', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
            ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'dat_antro_edad_meses', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->input('number',array('placeholder' => 'Ingrese ...','min'=>1, 'max'=>60))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'dat_antro_talla_infante', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->input('number',array('placeholder' => 'Ingrese ...', 'step'=>0.01, 'min'=>40))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'dat_antro_peso_infante', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->input('number', array('placeholder' => 'Ingrese ...', 'step'=>0.01, 'min'=>3))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
