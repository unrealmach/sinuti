<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\metricas_individuo\models\TipoVacuna */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-vacuna-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'tipo_vac_enfermedad', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tipo_vac_nombre', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tipo_vac_num_dosis', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tipo_vac_dosis_canti', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tipo_vac_dosis_medida', 
                    ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'ML' => 'ML', 'GOTAS' => 'GOTAS', ], ['prompt' => 'Seleccione ...'])
                    ->label(null, ['class' => 'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tipo_vac_via', 
                    ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'I.D.' => 'I.D.', 'V.O.' => 'V.O.', 'IM' => 'IM', 'VS' => 'VS', ], ['prompt' => 'Seleccione ...'])
                    ->label(null, ['class' => 'col-sm-2 control-label']) ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
            <center>
                <?= Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create') : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
            </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
