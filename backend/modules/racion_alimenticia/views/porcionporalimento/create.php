<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\racion_alimenticia\models\PorcionPorAlimento */

$this->title = Yii::t('app', 'Create Porcion Por Alimento');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Porcion Por Alimentos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="porcion-por-alimento-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
