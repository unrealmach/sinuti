<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\racion_alimenticia\models\PorcionporcategoriaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="porcion-por-categoria-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'porc_cat_id') ?>

    <?= $form->field($model, 'categoria_id') ?>

    <?= $form->field($model, 'porc_cat_cantidad_g') ?>

    <?= $form->field($model, 'porc_cat_descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
