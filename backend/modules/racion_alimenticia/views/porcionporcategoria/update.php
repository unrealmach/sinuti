<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\racion_alimenticia\models\PorcionPorCategoria */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Porcion Por Categoria',
]) . ' ' . $model->porc_cat_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Porcion Por Categorias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->porc_cat_id, 'url' => ['view', 'id' => $model->porc_cat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="porcion-por-categoria-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
