<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\racion_alimenticia\models\PorcionPorCategoria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="porcion-por-categoria-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'categoria_id', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'porc_cat_cantidad_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'porc_cat_descripcion', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
            <center>
                <?= Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create') : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
            </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
