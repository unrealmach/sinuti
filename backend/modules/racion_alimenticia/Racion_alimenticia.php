<?php

namespace backend\modules\racion_alimenticia;

class Racion_alimenticia extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\racion_alimenticia\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
