<?php

namespace backend\modules\racion_alimenticia\models;

use Yii;

/**
 * This is the model class for table "porcion_por_categoria".
 *
 * @property integer $porc_cat_id
 * @property integer $categoria_id
 * @property string $porc_cat_cantidad_g
 * @property string $porc_cat_descripcion
 *
 * @property Categoria $categoria
 */
class PorcionPorCategoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'porcion_por_categoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria_id', 'porc_cat_descripcion'], 'required'],
            [['categoria_id'], 'integer'],
            [['porc_cat_cantidad_g'], 'number'],
            [['porc_cat_descripcion'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'porc_cat_id' => Yii::t('app', 'Porc Cat ID'),
            'categoria_id' => Yii::t('app', 'Categoria ID'),
            'porc_cat_cantidad_g' => Yii::t('app', 'Porc Cat Cantidad G'),
            'porc_cat_descripcion' => Yii::t('app', 'Porc Cat Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['categoria_id' => 'categoria_id']);
    }
}
