<?php

namespace backend\modules\racion_alimenticia\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\racion_alimenticia\models\PorcionPorCategoria;

/**
 * PorcionporcategoriaSearch represents the model behind the search form about `backend\modules\racion_alimenticia\models\PorcionPorCategoria`.
 */
class PorcionporcategoriaSearch extends PorcionPorCategoria
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['porc_cat_id', 'categoria_id'], 'integer'],
            [['porc_cat_cantidad_g'], 'number'],
            [['porc_cat_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PorcionPorCategoria::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'porc_cat_id' => $this->porc_cat_id,
            'categoria_id' => $this->categoria_id,
            'porc_cat_cantidad_g' => $this->porc_cat_cantidad_g,
        ]);

        $query->andFilterWhere(['like', 'porc_cat_descripcion', $this->porc_cat_descripcion]);

        return $dataProvider;
    }
}
