<?php

namespace backend\modules\racion_alimenticia\models;

use Yii;

/**
 * This is the model class for table "porcion_por_alimento".
 *
 * @property integer $porc_alim_id
 * @property integer $alimento_id
 * @property string $porc_alim_cantidad
 * @property string $porc_alim_medida_cacera
 *
 * @property Alimento $alimento
 */
class PorcionPorAlimento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'porcion_por_alimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alimento_id', 'porc_alim_medida_cacera'], 'required'],
            [['alimento_id'], 'integer'],
            [['porc_alim_cantidad'], 'number'],
            [['porc_alim_medida_cacera'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'porc_alim_id' => Yii::t('app', 'Porc Alim ID'),
            'alimento_id' => Yii::t('app', 'Alimento ID'),
            'porc_alim_cantidad' => Yii::t('app', 'Porc Alim Cantidad'),
            'porc_alim_medida_cacera' => Yii::t('app', 'Porc Alim Medida Cacera'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['alimento_id' => 'alimento_id']);
    }
}
