<?php

namespace backend\modules\racion_alimenticia\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\racion_alimenticia\models\PorcionPorAlimento;

/**
 * PorcionporalimentoSearch represents the model behind the search form about `backend\modules\racion_alimenticia\models\PorcionPorAlimento`.
 */
class PorcionporalimentoSearch extends PorcionPorAlimento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['porc_alim_id', 'alimento_id'], 'integer'],
            [['porc_alim_cantidad'], 'number'],
            [['porc_alim_medida_cacera'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PorcionPorAlimento::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'porc_alim_id' => $this->porc_alim_id,
            'alimento_id' => $this->alimento_id,
            'porc_alim_cantidad' => $this->porc_alim_cantidad,
        ]);

        $query->andFilterWhere(['like', 'porc_alim_medida_cacera', $this->porc_alim_medida_cacera]);

        return $dataProvider;
    }
}
