<?php

namespace backend\modules\asistencia;

class Asistencia extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\asistencia\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
