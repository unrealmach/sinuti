<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\individuo\models\Infante;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */

$this->title = $model->asistencia_fecha;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->asistencia_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->asistencia_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'asistencia_id',
            'asistencia_hora_ingreso',
            'asistencia_hora_salida',
            'asistencia_fecha',
            'infantes.nombreCompleto',
        ],
        ]) ?>

    </div>
</div>
