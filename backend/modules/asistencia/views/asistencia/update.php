<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asistencia',
]) . ' ' . $model->asistencia_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asistencia_id, 'url' => ['view', 'id' => $model->asistencia_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asistencia-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
