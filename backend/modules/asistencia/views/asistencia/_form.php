<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use backend\modules\individuo\models\Infante;

/* @var $this yii\web\View */
/* @var $model backend\modules\asistencia\models\Asistencia */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="asistencia-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
            $form->field($model, 'asistencia_hora_ingreso', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(TimePicker::classname(), [])
            ->label(null, ['class' => 'col-sm-2 control-label'])
//        $form->field($model, 'asistencia_hora_ingreso', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
//            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
        $form->field($model, 'asistencia_hora_salida', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(TimePicker::classname(), [])
        ->label(null, ['class' => 'col-sm-2 control-label'])
//        $form->field($model, 'asistencia_hora_salida', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
//            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'asistencia_fecha', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
            ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
        $form->field($model, 'infantes_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Infante::find()->all(), 'infante_id', 'infante_dni', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
//        $form->field($model, 'infantes_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
//            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
