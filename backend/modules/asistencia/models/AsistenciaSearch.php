<?php

namespace backend\modules\asistencia\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\asistencia\models\Asistencia;
use backend\modules\individuo\models\Infante;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\inscripcion\models\Matricula;

/**
 * AsistenciaSearch represents the model behind the search form about `backend\modules\asistencia\models\Asistencia`.
 */
class AsistenciaSearch extends Asistencia {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['asistencia_id'], 'integer'],
            [['asistencia_hora_ingreso', 'asistencia_hora_salida', 'asistencia_fecha', 'infantes_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Asistencia::find();

        $query->joinWith('infantes', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $query->join('inner join', 'matricula', 'asistencia.infantes_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'ACTIVO' ");

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        // si rol es coordinador le permite ver las matriculas de los infantes que pertenecen al centro infantil durante un periodo activo
        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $periodo = Periodo::find()->periodoActivo()->one();
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, $periodo->periodo_id)->all();
            if (!empty($infantesbyCibv)) {
                foreach ($infantesbyCibv as $value) {
                    $query->orHaving(['asistencia.infantes_id' => $value->infante_id]);
                    $query->orFilterWhere(['like', 'matricula.infante_id', $value->infante_id]);
                }
            } else {
                $query->orHaving(['asistencia.infantes_id' => 0]);
            }
        }
        // si rol es educador le permite ver las matriculas de los infantes que pertenecen al salon del educador durante un periodo activo
        if ($authAsiignmentMdel->item_name == 'educador') {
            $query->join('inner join', 'sector_asignado_infante', 'asistencia.infantes_id = sector_asignado_infante.infante_id');
            $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    $query->orHaving(['asistencia.infantes_id' => $value->infante_id]);
                    $query->orFilterWhere(['like', 'asistencia.infantes_id', $value->infante_id]);
                }
            } else {
                $query->orHaving(['asistencia.infantes_id' => 0]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asistencia_id' => $this->asistencia_id,
            'asistencia_hora_ingreso' => $this->asistencia_hora_ingreso,
            'asistencia_hora_salida' => $this->asistencia_hora_salida,
            'asistencia_fecha' => $this->asistencia_fecha,
//            'infantes_id' => $this->infantes_id,
        ]);

        $query->andFilterWhere(
                ['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " ", infante.infante_apellidos)'), $this->infantes_id]
        );

        return $dataProvider;
    }

}
