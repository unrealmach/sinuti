<?php

namespace backend\modules\asistencia\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\asistencia\models\Asistencia]].
 *
 * @see \backend\modules\asistencia\models\Asistencia
 */
class AsistenciaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\asistencia\models\Asistencia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\asistencia\models\Asistencia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * permite saber si un infante ya tiene la asistencia para una fecha dada
     * @param int $infante_id
     * @param string $asistencia
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deAsistenciaDiaria($infante_id, $asistencia){
        return $this->where(['infantes_id' => $infante_id, 'asistencia_fecha' => $asistencia]);
    }
    
}