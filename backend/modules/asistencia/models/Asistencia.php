<?php

namespace backend\modules\asistencia\models;

use Yii;
use backend\modules\individuo\models\Infante;

/**
 * This is the model class for table "asistencia".
 *
 * @property integer $asistencia_id
 * @property string $asistencia_hora_ingreso
 * @property string $asistencia_hora_salida
 * @property string $asistencia_fecha
 * @property integer $infantes_id
 *
 * @property Infante $infantes
 */
class Asistencia extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'asistencia';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['asistencia_hora_ingreso', 'asistencia_hora_salida', 'asistencia_fecha', 'infantes_id'], 'required'],
            [['asistencia_hora_ingreso', 'asistencia_hora_salida', 'asistencia_fecha'], 'safe'],
            [['infantes_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'asistencia_id' => Yii::t('app', 'Asistencia ID'),
            'asistencia_hora_ingreso' => Yii::t('app', 'Hora Ingreso'),
            'asistencia_hora_salida' => Yii::t('app', 'Hora Salida'),
            'asistencia_fecha' => Yii::t('app', 'Fecha'),
            'infantes_id' => Yii::t('app', 'Infante'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfantes() {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infantes_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\asistencia\models\activequeries\AsistenciaQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\asistencia\models\activequeries\AsistenciaQuery(get_called_class());
    }

}
