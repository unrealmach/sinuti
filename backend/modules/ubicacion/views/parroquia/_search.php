<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\ubicacion\models\ParroquiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="parroquia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'parroquia_id') ?>

    <?= $form->field($model, 'parroquia_codigo') ?>

    <?= $form->field($model, 'parroquia_nombre') ?>

    <?= $form->field($model, 'localidad_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
