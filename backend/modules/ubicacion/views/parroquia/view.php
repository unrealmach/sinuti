<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ubicacion\models\Parroquia */

$this->title = $model->parroquia_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parroquias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parroquia-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->parroquia_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->parroquia_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'parroquia_id',
                [
                    'label' => 'Localidad',
                    'value' => $model->localidad->localidad_nombre
                ],
                'parroquia_codigo',
//                'parroquia_nombre',
//                'localidad_id',
            ],
        ])
        ?>

    </div>
</div>
