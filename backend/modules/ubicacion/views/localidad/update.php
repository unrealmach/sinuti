<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\ubicacion\models\Localidad */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Localidad',
        ]) . ' ' . $model->localidad_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Localidads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->localidad_id, 'url' => ['view', 'id' => $model->localidad_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="localidad-update box box-info">
    <div class="box-header with-border">
        <?php
        $ar = explode(" ", ($this->title));
        $title = "";
        for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];
        }
        ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
