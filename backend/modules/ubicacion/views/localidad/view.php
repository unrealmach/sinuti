<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\ubicacion\models\Localidad */

$this->title = $model->localidad_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Localidads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localidad-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->localidad_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->localidad_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'localidad_id',
                [
                    'label' => 'Distrito',
                    'value' => $model->distrito->distrito_descripcion
                ],
                'distrito.distrito_descripcion',
                'localidad_codigo',
//                'localidad_nombre',
            ],
        ])
        ?>

    </div>
</div>
