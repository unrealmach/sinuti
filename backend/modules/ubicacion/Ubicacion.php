<?php

namespace backend\modules\ubicacion;

class Ubicacion extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\ubicacion\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
