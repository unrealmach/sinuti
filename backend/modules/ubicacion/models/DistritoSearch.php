<?php

namespace backend\modules\ubicacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ubicacion\models\Distrito;

/**
 * DistritoSearch represents the model behind the search form about `backend\modules\ubicacion\models\Distrito`.
 */
class DistritoSearch extends Distrito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['distrito_id'], 'integer'],
            [['distrito_codigo', 'distrito_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Distrito::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'distrito_id' => $this->distrito_id,
        ]);

        $query->andFilterWhere(['like', 'distrito_codigo', $this->distrito_codigo])
            ->andFilterWhere(['like', 'distrito_descripcion', $this->distrito_descripcion]);

        return $dataProvider;
    }
}
