<?php

namespace backend\modules\ubicacion\models;
use backend\modules\ubicacion\models\Localidad;
use backend\modules\centro_infantil\models\Cibv;

use Yii;

/**
 * This is the model class for table "parroquia".
 *
 * @property integer $parroquia_id
 * @property integer $parroquia_codigo
 * @property string $parroquia_nombre
 * @property integer $localidad_id
 *
 * @property Cibv[] $cibvs
 * @property Localidad $localidad
 */
class Parroquia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parroquia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parroquia_codigo', 'parroquia_nombre', 'localidad_id'], 'required'],
            [['parroquia_codigo', 'localidad_id'], 'integer'],
            [['parroquia_codigo'], 'unique'],
            [['parroquia_nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parroquia_id' => Yii::t('app', 'Parroquia ID'),
            'parroquia_codigo' => Yii::t('app', 'Codigo'),
            'parroquia_nombre' => Yii::t('app', 'Nombre'),
            'localidad_id' => Yii::t('app', 'Localidad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCibvs()
    {
        return $this->hasMany(Cibv::className(), ['parroquia_id' => 'parroquia_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidad()
    {
        return $this->hasOne(Localidad::className(), ['localidad_id' => 'localidad_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\activequeries\ParroquiaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\ubicacion\models\activequeries\ParroquiaQuery(get_called_class());
    }
}
