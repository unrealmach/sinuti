<?php

namespace backend\modules\ubicacion\models;

use Yii;

/**
 * This is the model class for table "distrito".
 *
 * @property integer $distrito_id
 * @property string $distrito_codigo
 * @property string $distrito_descripcion
 *
 * @property Localidad[] $localidads
 */
class Distrito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['distrito_codigo', 'distrito_descripcion'], 'required'],
            [['distrito_codigo'], 'string', 'max' => 5],
            [['distrito_codigo'], 'unique'],
            [['distrito_descripcion'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'distrito_id' => Yii::t('app', 'Distrito ID'),
            'distrito_codigo' => Yii::t('app', 'Codigo'),
            'distrito_descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }
    
    public function getCodigoDescripcion() {
        return $this->distrito_codigo . " " . $this->distrito_descripcion;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidads()
    {
        return $this->hasMany(Localidad::className(), ['distrito_id' => 'distrito_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\activequeries\DistritoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\ubicacion\models\activequeries\DistritoQuery(get_called_class());
    }
}
