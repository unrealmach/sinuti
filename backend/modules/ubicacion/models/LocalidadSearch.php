<?php

namespace backend\modules\ubicacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ubicacion\models\Localidad;

/**
 * LocalidadSearch represents the model behind the search form about `backend\modules\ubicacion\models\Localidad`.
 */
class LocalidadSearch extends Localidad {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['localidad_id', 'localidad_codigo', 'distrito_id'], 'integer'],
            [['localidad_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Localidad::find();

        $query->joinWith('distrito', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'localidad_id' => $this->localidad_id,
            'localidad_codigo' => $this->localidad_codigo,
            'distrito_id' => $this->distrito_id,
        ]);

        $query->andFilterWhere(['like', 'localidad_nombre', $this->localidad_nombre]);

        $query->andFilterWhere(
                ['like', 'distrito.distrito_descripcion', $this->distrito_id]
        );

        return $dataProvider;
    }

}
