<?php

namespace backend\modules\ubicacion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\ubicacion\models\Parroquia;

/**
 * ParroquiaSearch represents the model behind the search form about `backend\modules\ubicacion\models\Parroquia`.
 */
class ParroquiaSearch extends Parroquia {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['parroquia_id', 'parroquia_codigo', 'localidad_id'], 'integer'],
            [['parroquia_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Parroquia::find();

        $query->joinWith('localidad', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'parroquia_id' => $this->parroquia_id,
            'parroquia_codigo' => $this->parroquia_codigo,
            'localidad_id' => $this->localidad_id,
        ]);

        $query->andFilterWhere(['like', 'parroquia_nombre', $this->parroquia_nombre]);
        
         $query->andFilterWhere(
                ['like', 'localidad.localidad_nombre', $this->localidad_id]
        );
        return $dataProvider;
    }

}
