<?php

namespace backend\modules\ubicacion\models;
use backend\modules\ubicacion\models\Distrito;
use backend\modules\ubicacion\models\Parroquia;
use Yii;

/**
 * This is the model class for table "localidad".
 *
 * @property integer $localidad_id
 * @property integer $localidad_codigo
 * @property string $localidad_nombre
 * @property integer $distrito_id
 *
 * @property Distrito $distrito
 * @property Parroquia[] $parroquias
 */
class Localidad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'localidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['localidad_codigo', 'localidad_nombre', 'distrito_id'], 'required'],
            [['localidad_codigo', 'distrito_id'], 'integer'],
            [['localidad_codigo'], 'unique'],
            [['localidad_nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'localidad_id' => Yii::t('app', 'Localidad ID'),
            'localidad_codigo' => Yii::t('app', 'Codigo'),
            'localidad_nombre' => Yii::t('app', 'Nombre'),
            'distrito_id' => Yii::t('app', 'Distrito'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrito()
    {
        return $this->hasOne(Distrito::className(), ['distrito_id' => 'distrito_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParroquias()
    {
        return $this->hasMany(Parroquia::className(), ['localidad_id' => 'localidad_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\activequeries\LocalidadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\ubicacion\models\activequeries\LocalidadQuery(get_called_class());
    }
}
