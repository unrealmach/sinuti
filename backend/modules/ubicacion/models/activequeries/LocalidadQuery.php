<?php

namespace backend\modules\ubicacion\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\ubicacion\models\Localidad]].
 *
 * @see \backend\modules\ubicacion\models\Localidad
 */
class LocalidadQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\Localidad[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\Localidad|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}