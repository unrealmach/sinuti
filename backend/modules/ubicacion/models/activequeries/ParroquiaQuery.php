<?php

namespace backend\modules\ubicacion\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\ubicacion\models\Parroquia]].
 *
 * @see \backend\modules\ubicacion\models\Parroquia
 */
class ParroquiaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\Parroquia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\ubicacion\models\Parroquia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}