<?php

namespace backend\modules\parametros_sistema\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\parametros_sistema\models\Periodo]].
 *
 * @see \backend\modules\parametros_sistema\models\Periodo
 */
class PeriodoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\parametros_sistema\models\Periodo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\parametros_sistema\models\Periodo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
/**
 * Devuelve el periodo con  estado activo
 * @author Mauricio Chamorro
 * @return \backend\modules\parametros_sistema\models\activequeries\PeriodoQuery
 */    
       public function periodoActivo()
    {
        $this->andWhere(['estado'=>'ACTIVO']);
        return $this;
    }
}