<?php

namespace backend\modules\parametros_sistema\models;

use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\inscripcion\models\Matricula;
use Yii;

/**
 * This is the model class for table "periodo".
 *
 * @property integer $periodo_id
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $estado
 *
 * @property AutoridadesCibv[] $autoridadesCibvs
 * @property GrupoComite[] $grupoComites
 * @property Matricula[] $matriculas
 * @property Salon[] $salons
 * @property SectorAsignado[] $sectorAsignados
 */
class Periodo extends \yii\db\ActiveRecord {

    public $ESTADO_ACTIVO = 'ACTIVO';
    public $ESTADO_INACTIVO = 'INACTIVO';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'periodo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['fecha_inicio', 'fecha_fin'], 'required'],
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['estado'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'periodo_id' => Yii::t('app', 'Periodo ID'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'estado' => Yii::t('app', 'Estado'),
            'limitesFechasPeriodo' => Yii::t('app', 'Período'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLimitesFechasPeriodo() {
        return $this->fecha_inicio . " - " . $this->fecha_fin;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoridadesCibvs() {
        return $this->hasMany(AutoridadesCibv::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoComites() {
        return $this->hasMany(GrupoComite::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas() {
        return $this->hasMany(Matricula::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalons() {
        return $this->hasMany(Salon::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignados() {
        return $this->hasMany(SectorAsignado::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\parametros_sistema\models\activequeries\PeriodoQuery the active query used by this AR class. 
     */
    public static function find() {
        return new \backend\modules\parametros_sistema\models\activequeries\PeriodoQuery(get_called_class());
    }

}
