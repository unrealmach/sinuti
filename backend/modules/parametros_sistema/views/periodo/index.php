<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\parametros_sistema\models\PeriodoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Periodos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="periodo-index box box-success">
    <div class="box-header with-border">
        <div style="text-align:center; width:100%">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
    <div class="box-body" style="overflow: auto">


        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Periodo'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                'periodo_id',
                'fecha_inicio',
                'fecha_fin',
                'estado',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>
