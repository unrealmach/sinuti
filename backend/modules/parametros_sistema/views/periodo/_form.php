<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\switchinput\SwitchBox;

/* @var $this yii\web\View */
/* @var $model backend\modules\parametros_sistema\models\Periodo */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="periodo-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
            $form->field($model, 'fecha_inicio', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
            ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'fecha_fin', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
            ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label'])

        ?>

        <?=
        $form->field($model, 'estado', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(SwitchBox::className(), [
            'options' => [
                'label' => false,
            ],
            'clientOptions' => [
                'size' => 'large',
                'onColor' => 'success',
                'offColor' => 'info',
                'onText' => Yii::t('app', 'Activo'),
                'offText' => Yii::t('app', 'Inactivo')
            ]
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
