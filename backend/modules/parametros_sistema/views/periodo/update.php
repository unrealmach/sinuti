<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\parametros_sistema\models\Periodo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Periodo',
]) . ' ' . $model->periodo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Periodos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->periodo_id, 'url' => ['view', 'id' => $model->periodo_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="periodo-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
