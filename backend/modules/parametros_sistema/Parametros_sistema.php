<?php

namespace backend\modules\parametros_sistema;

class Parametros_sistema extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\parametros_sistema\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
