<?php
namespace backend\modules\preparacion_carta\models;

use backend\modules\preparacion_carta\models\TiempoComida;
use backend\modules\metricas_individuo\models\GrupoEdad;
use backend\modules\nutricion\models\Alimento;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\carta_semanal\models\DCartaSemanal;
use Yii;

/**
 * This is the model class for table "m_prep_carta".
 *
 * @property integer $m_prep_carta_id
 * @property string $m_prep_carta_nombre
 * @property string $m_prep_carta_descripcion
 * @property integer $tiempo_comida_id
 * @property integer $grupo_edad_id
 *
 * @property DCartaSemanal[] $dCartaSemanals
 * @property DPreparacionCarta[] $dPreparacionCartas
 * @property TiempoComida $tiempoComida
 * @property GrupoEdad $grupoEdad
 */
class MPrepCarta extends \yii\db\ActiveRecord
{

    public $cantidad_togal_g;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_prep_carta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_prep_carta_nombre', 'm_prep_carta_descripcion', 'tiempo_comida_id', 'grupo_edad_id'], 'required'],
            [['m_prep_carta_descripcion'], 'string'],
            [['tiempo_comida_id', 'grupo_edad_id'], 'integer'],
            [['m_prep_carta_nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'm_prep_carta_id' => Yii::t('app', 'Prep Carta'),
            'm_prep_carta_nombre' => Yii::t('app', 'Nombre'),
            'm_prep_carta_descripcion' => Yii::t('app', 'Descripción'),
            'tiempo_comida_id' => Yii::t('app', 'Tiempo Comida'),
            'grupo_edad_id' => Yii::t('app', 'Grupo Edad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDCartaSemanals()
    {
        return $this->hasMany(DCartaSemanal::className(), ['m_prep_carta_id' => 'm_prep_carta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDPreparacionCartas()
    {
        return $this->hasMany(DPreparacionCarta::className(), ['m_prep_carta_id' => 'm_prep_carta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiempoComida()
    {
        return $this->hasOne(TiempoComida::className(), ['tiem_com_id' => 'tiempo_comida_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoEdad()
    {
        return $this->hasOne(GrupoEdad::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }

    /**
     * Agrega el tipo de detalle que tendra la carta
     * @param int $alimento_id
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array del detalle
     */
    public function validateDetalleAlimento($alimento_id)
    {
        if ($alimento_id != '') {
            list($tipoIngreso, $idIngreso) = explode("_", $alimento_id);
            if ($tipoIngreso == "a") {
                return ['alimento_id', $idIngreso];
            } else {
                return ['m_platillo_id', $idIngreso];
            }
        }
    }
    
    /**
     * Devuelve el nombre del detalle de la carta
     * @param int $alimento_id
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return string con el nombre
     */
    public static function getNombreDetalle($alimento_id)
    {
        if ($alimento_id != '') {
            list($tipoIngreso, $idIngreso) = explode("_", $alimento_id);
            
            if ($tipoIngreso == "a") {
                $alimento = Alimento::findOne($idIngreso);
                return "a:" . $alimento->alimento_nombre;
            } else if($tipoIngreso == "p") {
                $platillo = MPlatillo::findOne($idIngreso);
                return "p:" . $platillo->m_platillo_nombre;
            }
        }
    }
}
