<?php

namespace backend\modules\preparacion_carta\models;

use Yii;

/**
 * This is the model class for table "tipo_preparacion_carta".
 *
 * @property integer $tipo_prep_carta_id
 * @property string $tipo_prep_carta_nombre
 * @property string $tipo_prep_carta_descripcion
 *
 * @property DPreparacionCarta[] $dPreparacionCartas
 * @property TipoReceta[] $tipoRecetas
 */
class TipoPreparacionCarta extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_preparacion_carta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_prep_carta_nombre', 'tipo_prep_carta_descripcion'], 'required'],
            [['tipo_prep_carta_nombre'], 'string', 'max' => 45],
            [['tipo_prep_carta_descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tipo_prep_carta_id' => Yii::t('app', 'Tipo Prep. Carta'),
            'tipo_prep_carta_nombre' => Yii::t('app', 'Nombre'),
            'tipo_prep_carta_descripcion' => Yii::t('app', 'Descripci&oacuten'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDPreparacionCartas()
    {
        return $this->hasMany(DPreparacionCarta::className(), ['tipo_preparacion_id' => 'tipo_prep_carta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoRecetas()
    {
        return $this->hasMany(TipoReceta::className(), ['tipo_prep_carta_id' => 'tipo_prep_carta_id']);
    }
}
