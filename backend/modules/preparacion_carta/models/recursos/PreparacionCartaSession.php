<?php

namespace backend\modules\preparacion_carta\models\recursos;

use backend\modules\preparacion_carta\models\TipoPreparacionCarta;
use backend\modules\composicion_nutricional\models\RangoNutriente;
use backend\modules\preparacion_carta\models\TiempoComida;
use backend\modules\metricas_individuo\models\GrupoEdad;

/**
 * Clase que permite realizar los calculos de los detalles y maestro
 * @author Sofia Mejía <amandasofiamejia@gmail.com>
 */
class PreparacionCartaSession {

    // almacena el numero de detalles permitidos en sesion de acuerdo al tiempo_comida
    public $min = "";
    public $tiempo_comida_seleccionado = "";
    public $grupo_edad_seleccionado = "";
    //objeto que sirve de contenedor de los detalles
    public $detalles = [];
    public $rango_nutrientes = [];
    //objeto que contiene las variables a ser calculadas en el maestro
    public $maestro = ['mprepcarta_cantidad_togal_g' => 0];
    public $_join_time_type = [
        "DESAYUNO" => ["liquido", "solido", "otros"],
        "REFRIGERIO DE LA MAÑANA" => ["fruta", "liquido"],
        "ALMUERZO" => ["sopa", "plato fuerte", "acompañado", "ensalada", "jugo"],
        "REFRIGERIO DE LA TARDE" => ["liquido", "fruta"],
    ];

    public function getMin() {
        return $this->min;
    }

    public function setMin($min) {
        $this->min = $min;
    }

    public function getRangoNutrientes() {
        return $this->rango_nutrientes;
    }

    public function setRangoNutrientes($rango) {
        $this->rango_nutrientes = $rango;
    }

    public function getGrupoEdadSeleccionado() {
        return $this->grupo_edad_seleccionado;
    }

    public function setGrupoEdadSeleccionado($edad) {
        $this->grupo_edad_seleccionado = $edad;
    }

    public function getTiempoComidaSeleccionado() {
        return $this->tiempo_comida_seleccionado;
    }

    public function setTiempoComidaSeleccionado($time) {
        $this->tiempo_comida_seleccionado = $time;
    }

    /**
     * Permite que se use elconstructor de la clase
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function init() {
        parent::init();
    }

    public function getAllJoinTimeType() {
        return $this->_join_time_type;
    }

    /**
     * Permite recuperar los tipo_preparacion de acuerdo al tempo_comida
     * @param string $time tiempo_comida en la sesion
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function getSelectedJoinTimeType($time) {
        return $this->_join_time_type[$time];
    }

    /**
     * Busca los tipo_preparacion_carta de acuerdo al tempo_comida y le agrega el id
     * @param string $time el tiempo_comida en la sesion 
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function getIDType($time) {
        $out = $this->getSelectedJoinTimeType($time);
        $out2 = [];
        $temp = [];
        foreach ($out as $tipo_preparacion) {
            $temp = TipoPreparacionCarta::findOne(['tipo_prep_carta_nombre' => $tipo_preparacion])->tipo_prep_carta_id;
            $aux = [ 'id' => strval($temp),
                'text' => $tipo_preparacion];
            array_push($out2, $aux
            );
        }
        return $out2;
    }

    /**
     * Ubica en sesion con la misma posicion que en la vista
     * @param int $posicion del detalle
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array Retorna el detalle en dicha posicion
     */
    public function getDetallePorPosicion($posicion) {
        return $this->detalles[$posicion];
    }

    /**
     * Obtiene los detalles de la sesion
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array Contiene los detalles
     */
    public function getDetalles() {
        return $this->detalles;
    }

    /**
     * Obtiene los datos del maestro de la sesion
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array Contiene los datos del maestro
     */
    public function getDataMaestro() {
        return $this->maestro;
    }

    /**
     * Permite agregar a los objetos detalles a la sesion (alimentos o detalles)
     * @param int $posicion la posicion del detalle 
     * @param int $alimento_id el identificador del alimento o platillo
     * @param double $cantidad_g la cantidad de dicho alimento o platillo
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return boolean TRUE: se agrego correctamente
     */
    public function agregarDetalle($posicion, $alimento_id, $cantidad_g) { //genera un nuevo objeto detalle y lo llena con los datos correspondientes
        $detalle = ['posicion' => $posicion,
            'd_alimento_id' => $alimento_id,
            'd_prep_carta_cantidad_g' => $cantidad_g,
            'd_composicion' => $this->porcentajeComposAlimento($alimento_id, $cantidad_g)
        ];
        //ingresa el detalle en el objeto de sesion
        $this->detalles[$posicion] = $detalle;
        return TRUE;
    }

    /**
     * Elimina un detalle deacuerdo a su posición
     * La posición en la vista es la misma posicion en la sesion por lo que se elimina sin problemas
     * @param int $posicionEliminado
     * @author Sofia Mejía <amandasofiamejia@gmail.com>  
     * @return boolean TRUE: si se elimino caso contrario FALSE
     */
    public function eliminarDetalle($posicionEliminado) {
        if (sizeof($this->detalles) > $this->getMin()) {
            unset($this->detalles[$posicionEliminado]); // elimina el detalle de dicha posicion
            // reordena los keys 
            // debido a que el maestro detalle en html se comporta de la misma manera
            // dicho detalle que es eliminado pasa a ser reemplazado por el 
            // siguiente si existiese en su posicion
            $this->detalles = array_values($this->detalles);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Permite modificar la cantidad del ingrediente
     * @param int $posicion indica la posicion del alimento
     * @param int $alimento_id indica el alimento o platillo a ser modificado
     * @param double $cantidad_g indica la cantidad que tendra dicho alimento o platillo
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return boolean True: si se realiza la operacion
     */
    public function cambiarCantidadDetalle($posicion, $alimento_id, $cantidad_g) {
        if ($this->detalles[$posicion]['d_alimento_id'] === $alimento_id) {
            $temp = $this->porcentajeComposAlimento($alimento_id, $cantidad_g);
            $this->detalles[$posicion]['d_composicion'] = $temp;
            $this->detalles[$posicion]['d_prep_carta_cantidad_g'] = $cantidad_g;
            return TRUE;
        }
    }

    /**
     * Permite calcular la composicion nutricional del alimento de acuerdo a su cantidad
     * mediante una regla de tres, debido a que el sistema tiene todos sus alimentos
     * en 100 gramos para la obtencion de su respectiva composicion
     * @param int $alimento_id identificador del alimento o platillo
     * @param double $cantidad_g cantidad de dicho alimento o platillo
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array Con la composición de dicho alimento segun su cantidad
     */
    public function porcentajeComposAlimento($alimento_id, $cantidad_g) {
        //Realiza el casteo para obtener el alimento o platillo
        list($tipoIngreso, $idIngreso) = explode("_", $alimento_id);

        if ($tipoIngreso == "a") {
            $modelAlimento = \backend\modules\nutricion\models\Alimento::findOne(['alimento_id' => $idIngreso]);
            //obtiene la composicion nutricional de dicho alimento
            $composicionAlimento = $modelAlimento->composicionNutricionals[0]->attributes;
        } else if ($tipoIngreso == "p") {
            $modelAlimento = \backend\modules\preparacion_platillo\models\MPlatillo::findOne(['m_platillo_id' => $idIngreso]);
            //obtiene la composicion nutricional de dicho platillo
            $composicionAlimento = $modelAlimento->composicionNutricionalPlatillos[0]->attributes;
        }

        $newComposAlimento = [];
        //realiza la regla de tres
        foreach ($composicionAlimento as $key => $value) {
            if ($key != 'com_nut_id' || $key != 'alimento_id' || $key != 'com_nut_platillo_id' || $key != 'm_platillo_id') {
                $aux = round(($value * $cantidad_g) / 100, 2);
                $newComposAlimento[$key] = $aux;
            }
        }
        return $newComposAlimento;
    }

    /**
     * Calcula y genera la suma de los valores nutricionales de cada ingrediente
     * en sesion dando por resultado un total de su composicion, tambien suma
     * la cantidad de cada ingrediente y la guarda en sesion
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return boolean TRUE: indica que se realizo el procedimiento
     */
    public function generarTotalComposicion() {
        //obtiene los detalles de la sesion
        $detalles = $this->getDetalles();
        $cantidadTotalGramos = 0;

        $containerCantidadesValoresNutricionales = $this->crearContenedorValNutCantidad();
        //suma cada valor nutricional de cada detalle
        foreach ($detalles as $key => $value) {
            $cantidadBromotologicaIngrediente = $value['d_composicion'];
            array_shift($cantidadBromotologicaIngrediente);
            array_shift($cantidadBromotologicaIngrediente);

            foreach ($cantidadBromotologicaIngrediente as $key2 => $value2) {
                if ($key2 != "es_real") {
                    $containerCantidadesValoresNutricionales[$key2]+=$value2;
                }
            }
            //suma la cantidad de cada ingrediente
            $cantidadTotalGramos+=$value['d_prep_carta_cantidad_g'];
        }
        //elmina estos campos no son valores nutricionales 
        unset($containerCantidadesValoresNutricionales['com_nut_id']);
        unset($containerCantidadesValoresNutricionales['alimento_id']);

        $this->maestro['mprepcarta_cantidad_togal_g'] = round($cantidadTotalGramos, 2);
        $this->maestro['composicionTotal'] = $containerCantidadesValoresNutricionales;

        return TRUE;
    }

    /**
     * Crea un array asociativo con los valores nutricionales siendo
     * key: valor nutricional, value: cantidad
     * por defecto la cantidad se pone en 0 para que posteriormente
     * se llenen con los valores nutricionales de todos los detalles
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array  
     */
    public function crearContenedorValNutCantidad() {
        //procedimiento para obtener los valores nutricionales a partir del modelo
        $temp = \backend\modules\composicion_nutricional\models\ComposicionNutricional::findOne(['com_nut_id' => 1]);
        $listaValoresNutricionales = $temp->attributes;
        $containerCantidadesValoresNutricionales = [];
        //crea un array asociativo con los valores nutricionales siendo key: valor nutricional, value: cantidad
        //por defecto la cantidad se pone en 0 
        foreach ($listaValoresNutricionales as $key => $value) {
            $containerCantidadesValoresNutricionales[$key] = 0;
        }
        ksort($containerCantidadesValoresNutricionales);
        return $containerCantidadesValoresNutricionales;
    }

    /**
     * Obtiene el rango de nutrientes para el tiempo_comida_seleccionado y 
     * el grupo_edad_seleccionado
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function obtenerRangoNutrientes() {
        $time = str_replace(" ", "_", $this->tiempo_comida_seleccionado);
        $tiempo_comida = TiempoComida::findOne(['tiem_com_nombre' => $time])->tiem_com_id;
        $grupo_edad = intval($this->getGrupoEdadSeleccionado());
        $model = new RangoNutriente();
        $rango_nutrientes = [];
        $resultado = $model->obtenerRangNutByTimeEdad($tiempo_comida, $grupo_edad);
        foreach ($resultado as $key => $value) {
            $aux = [
                'val_min' => $value->ran_nut_valor_min,
                'val_max' => $value->ran_nut_valor_max,
                'nutriente' => $value->nutriente->nutriente_nombre,
            ];
            $rango_nutrientes[$value->nutriente_id] = $aux;
        }
        ksort($rango_nutrientes);
        $this->setRangoNutrientes($rango_nutrientes);
    }

    /**
     * Prepara los datos para que se interpreten en el chart de highchart
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return type
     */
    public function getCategoriesAndDataToHighchart() {
        $data = [];
        $data['categories'] = array_keys($this->maestro['composicionTotal']);
        $data['data']['compos_total'] = array_values($this->maestro['composicionTotal']);

        $aux = $this->getRangoNutrientes();
        $new_rangos = [];
        foreach ($aux as $key => $value) {
            $new_rangos[$value['nutriente']] = [
                'val_min' => $value['val_min'],
                'val_max' => $value['val_max']
            ];
        }
        ksort($new_rangos);
        $serie_rango_min = [];
        $serie_rango_max = [];
        foreach ($new_rangos as $key => $value) {
            array_push($serie_rango_max, floatval($value['val_max']));
            array_push($serie_rango_min, floatval($value['val_min']));
        }
        $data['data']['serie_min'] = $serie_rango_min;
        $data['data']['serie_max'] = $serie_rango_max;
        return $data;
    }

    /**
     * Valida los minimos detalles permitos de acuerdo al tiempo_comida
     * @param string $tiempoComida
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function validateCantidadDynamicForm($tiempoComida) {
        if ($tiempoComida === 'DESAYUNO') {
            $this->setMin(2);
//            return [3, 2];
        } else if ($tiempoComida === 'REFRIGERIO DE LA MAÑANA' ) {
            $this->setMin(1);
//            return [2, 1];
        } else if ($tiempoComida === 'REFRIGERIO DE LA TARDE') {
            $this->setMin(1);
//            return [2, 1];
        } else if ($tiempoComida === 'ALMUERZO') {
            $this->setMin(5);
//            return [5, 5];
        }
    }

    /**
     * Actualiza la sesion de acuerdo a un modelo
     * @param MPrepCarta $model 
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function updateSesion($model) {
        $d_models = $model->dPreparacionCartas;
        $alimento_id = 0;
        foreach ($d_models as $key => $submodel) {
            $alimento_id = $this->agregarIdentificadorDetalle($submodel);
            $this->agregarDetalle($key, $alimento_id, $submodel->d_prep_carta_cantidad_g);
        }
        $this->generarTotalComposicion();
    }

    /**
     * Permite agregarle un identifador al ingrediente que tendra el detalle
     * si es un alimento o una receta(platillo) 
     * @param dprepcarta detalle de la carta 
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return string
     */
    public function agregarIdentificadorDetalle($detalle) {
        if ($detalle->alimento_id != null) {
            $detalle->alimento_id = "a_" . $detalle->alimento_id;
            return $detalle->alimento_id;
        } else if ($detalle->m_platillo_id != null) {
            $detalle->m_platillo_id = "p_" . $detalle->m_platillo_id;
            return $detalle->m_platillo_id;
        }
    }

}
