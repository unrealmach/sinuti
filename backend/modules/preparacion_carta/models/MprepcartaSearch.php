<?php

namespace backend\modules\preparacion_carta\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_carta\models\MPrepCarta;

/**
 * MprepcartaSearch represents the model behind the search form about `backend\modules\preparacion_carta\models\MPrepCarta`.
 */
class MprepcartaSearch extends MPrepCarta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_prep_carta_id'], 'integer'],
            [['m_prep_carta_nombre', 'm_prep_carta_descripcion','tiempo_comida_id','grupo_edad_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MPrepCarta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith("tiempoComida");
        $query->joinWith("grupoEdad");

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'm_prep_carta_id' => $this->m_prep_carta_id,

            
        ]);

        $query->andFilterWhere(['like', 'm_prep_carta_nombre', $this->m_prep_carta_nombre])
            ->andFilterWhere(['like', 'm_prep_carta_descripcion', $this->m_prep_carta_descripcion])
            ->andFilterWhere(['like','tiempo_comida.tiem_com_nombre',strtoupper($this->tiempo_comida_id)])
            ->andFilterWhere(['like','UPPER(grupo_edad.grupo_edad_descripcion)',strtoupper($this->grupo_edad_id)])
            ;

     //   print_r($query->createCommand()->rawSql);die();

        return $dataProvider;
    }
}
