<?php

namespace backend\modules\preparacion_carta\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_carta\models\TipoPreparacionCarta;

/**
 * TipopreparacioncartaSearch represents the model behind the search form about `backend\modules\preparacion_carta\models\TipoPreparacionCarta`.
 */
class TipopreparacioncartaSearch extends TipoPreparacionCarta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_prep_carta_id'], 'integer'],
            [['tipo_prep_carta_nombre', 'tipo_prep_carta_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipoPreparacionCarta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipo_prep_carta_id' => $this->tipo_prep_carta_id,
        ]);

        $query->andFilterWhere(['like', 'tipo_prep_carta_nombre', $this->tipo_prep_carta_nombre])
            ->andFilterWhere(['like', 'tipo_prep_carta_descripcion', $this->tipo_prep_carta_descripcion]);

        return $dataProvider;
    }
}
