<?php

namespace backend\modules\preparacion_carta\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\preparacion_carta\models\DPreparacionCarta]].
 *
 * @see \backend\modules\preparacion_carta\models\DPreparacionCarta
 */
class DPreparacionCartaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\preparacion_carta\models\DPreparacionCarta[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\preparacion_carta\models\DPreparacionCarta|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * Devuelve el detalle de la carta semanal de acuerdo al mprepcarta 
     * @param int $mprepcarta_id
     * @return query
     */
    public function byMPrepCarta($mprepcarta_id){
        return $this->andWhere(['m_prep_carta_id' =>$mprepcarta_id]);
    }
}