<?php

namespace backend\modules\preparacion_carta\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_carta\models\DPreparacionCarta;

/**
 * DpreparacioncartaSearch represents the model behind the search form about `backend\modules\preparacion_carta\models\DPreparacionCarta`.
 */
class DpreparacioncartaSearch extends DPreparacionCarta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_prep_carta_id', 'm_prep_carta_id', 'tipo_preparacion_id', 'alimento_id', 'm_platillo_id'], 'integer'],
            [['d_prep_carta_cantidad_g'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DPreparacionCarta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'd_prep_carta_id' => $this->d_prep_carta_id,
            'm_prep_carta_id' => $this->m_prep_carta_id,
            'tipo_preparacion_id' => $this->tipo_preparacion_id,
            'd_prep_carta_cantidad_g' => $this->d_prep_carta_cantidad_g,
            'alimento_id' => $this->alimento_id,
            'm_platillo_id' => $this->m_platillo_id,
        ]);

        return $dataProvider;
    }
}
