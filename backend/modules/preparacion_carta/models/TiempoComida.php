<?php

namespace backend\modules\preparacion_carta\models;
use backend\modules\composicion_nutricional\models\RangoNutriente;

use Yii;

/**
 * This is the model class for table "tiempo_comida".
 *
 * @property integer $tiem_com_id
 * @property string $tiem_com_nombre
 * @property string $tiem_com_hora_inicio
 * @property string $tiempo_com_hora_fin
 *
 * @property MPrepCarta[] $mPrepCartas
 * @property RangoNutriente[] $rangoNutrientes
 */
class TiempoComida extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiempo_comida';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tiem_com_nombre', 'tiem_com_hora_inicio', 'tiempo_com_hora_fin'], 'required'],
            [['tiem_com_hora_inicio', 'tiempo_com_hora_fin'], 'safe'],
            [['tiem_com_nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tiem_com_id' => Yii::t('app', 'Tiempo Comida'),
            'tiem_com_nombre' => Yii::t('app', 'Nombre'),
            'tiem_com_hora_inicio' => Yii::t('app', 'Hora Inicio'),
            'tiempo_com_hora_fin' => Yii::t('app', 'Hora Fin'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPrepCartas()
    {
        return $this->hasMany(MPrepCarta::className(), ['tiempo_comida_id' => 'tiem_com_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRangoNutrientes()
    {
        return $this->hasMany(RangoNutriente::className(), ['tiempo_comida_id' => 'tiem_com_id']);
    }
}
