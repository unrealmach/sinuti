<?php

namespace backend\modules\preparacion_carta\models;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\nutricion\models\Alimento;
use Yii;

/**
 * This is the model class for table "d_preparacion_carta".
 *
 * @property integer $d_prep_carta_id
 * @property integer $m_prep_carta_id
 * @property integer $tipo_preparacion_id
 * @property string $d_prep_carta_cantidad_g
 * @property integer $alimento_id
 * @property integer $m_platillo_id
 *
 * @property MPrepCarta $mPrepCarta
 * @property TipoPreparacionCarta $tipoPreparacion
 * @property Alimento $alimento
 * @property MPlatillo $mPlatillo
 */
class DPreparacionCarta extends \yii\db\ActiveRecord
{
    public  $alimento_identificador;
    public $platillo_identificador;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'd_preparacion_carta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_preparacion_id', 'd_prep_carta_cantidad_g'], 'required'],
            [['m_prep_carta_id', 'tipo_preparacion_id'], 'integer'],
            [['alimento_id','m_platillo_id'], 'string'],
            [['d_prep_carta_cantidad_g'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'd_prep_carta_id' => Yii::t('app', 'D Prep Carta'),
            'm_prep_carta_id' => Yii::t('app', 'M Prep Carta'),
            'tipo_preparacion_id' => Yii::t('app', 'Tipo de Receta'),
            'd_prep_carta_cantidad_g' => Yii::t('app', 'Cantidad en gramos'),
            'alimento_id' => Yii::t('app', 'Alimento'),
            'm_platillo_id' => Yii::t('app', 'Platillo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPrepCarta()
    {
        return $this->hasOne(MPrepCarta::className(), ['m_prep_carta_id' => 'm_prep_carta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPreparacion()
    {
        return $this->hasOne(TipoPreparacionCarta::className(), ['tipo_prep_carta_id' => 'tipo_preparacion_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['alimento_id' => 'alimento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPlatillo()
    {
        return $this->hasOne(MPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }
    
    /**
     * @inheritdoc
     * @return \backend\modules\preparacion_carta\models\activequeries\DPreparacionCartaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\preparacion_carta\models\activequeries\DPreparacionCartaQuery(get_called_class());
    } 
}
