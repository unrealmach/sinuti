<?php

namespace backend\modules\preparacion_carta\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_carta\models\TiempoComida;

/**
 * TiempocomidaSearch represents the model behind the search form about `backend\modules\preparacion_carta\models\TiempoComida`.
 */
class TiempocomidaSearch extends TiempoComida
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tiem_com_id'], 'integer'],
            [['tiem_com_nombre', 'tiem_com_hora_inicio', 'tiempo_com_hora_fin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TiempoComida::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tiem_com_id' => $this->tiem_com_id,
            'tiem_com_hora_inicio' => $this->tiem_com_hora_inicio,
            'tiempo_com_hora_fin' => $this->tiempo_com_hora_fin,
        ]);

        $query->andFilterWhere(['like', 'tiem_com_nombre', $this->tiem_com_nombre]);

        return $dataProvider;
    }
}
