<?php

namespace backend\modules\preparacion_carta;

class Preparacion_carta extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\preparacion_carta\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
