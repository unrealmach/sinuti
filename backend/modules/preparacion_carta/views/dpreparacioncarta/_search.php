<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\DpreparacioncartaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dpreparacion-carta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'd_prep_carta_id') ?>

    <?= $form->field($model, 'm_prep_carta_id') ?>

    <?= $form->field($model, 'tipo_preparacion_id') ?>

    <?= $form->field($model, 'd_prep_carta_cantidad_g') ?>

    <?= $form->field($model, 'alimento_id') ?>

    <?php // echo $form->field($model, 'm_platillo_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
