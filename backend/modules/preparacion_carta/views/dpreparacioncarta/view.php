<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\DPreparacionCarta */

$this->title = $model->d_prep_carta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dpreparacion Cartas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dpreparacion-carta-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->d_prep_carta_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->d_prep_carta_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'd_prep_carta_id',
            'm_prep_carta_id',
            'tipo_preparacion_id',
            'd_prep_carta_cantidad_g',
            'alimento_id',
            'm_platillo_id',
        ],
        ]) ?>

    </div>
</div>
