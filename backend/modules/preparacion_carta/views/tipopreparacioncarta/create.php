<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\TipoPreparacionCarta */

$this->title = Yii::t('app', 'Create Tipo Preparacion Carta');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Preparacion Cartas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipo-preparacion-carta-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
