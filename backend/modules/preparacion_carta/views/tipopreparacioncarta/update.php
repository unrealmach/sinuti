<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\TipoPreparacionCarta */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Preparacion Carta',
]) . ' ' . $model->tipo_prep_carta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Preparacion Cartas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tipo_prep_carta_id, 'url' => ['view', 'id' => $model->tipo_prep_carta_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipo-preparacion-carta-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
