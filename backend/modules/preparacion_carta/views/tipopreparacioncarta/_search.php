<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\TipopreparacioncartaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-preparacion-carta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tipo_prep_carta_id') ?>

    <?= $form->field($model, 'tipo_prep_carta_nombre') ?>

    <?= $form->field($model, 'tipo_prep_carta_descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
