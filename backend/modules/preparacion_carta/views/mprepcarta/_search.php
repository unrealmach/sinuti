<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MprepcartaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mprep-carta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'm_prep_carta_id') ?>

    <?= $form->field($model, 'm_prep_carta_nombre') ?>

    <?= $form->field($model, 'm_prep_carta_descripcion') ?>

    <?= $form->field($model, 'tiempo_comida_id') ?>

    <?= $form->field($model, 'grupo_edad_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
