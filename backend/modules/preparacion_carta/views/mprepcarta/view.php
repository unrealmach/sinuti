<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\MPrepCarta */

$this->title = $model->m_prep_carta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mprep Cartas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mprep-carta-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->m_prep_carta_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->m_prep_carta_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'm_prep_carta_id',
            'm_prep_carta_nombre',
            'm_prep_carta_descripcion:ntext',
            'tiempo_comida_id',
            'grupo_edad_id',
        ],
        ]) ?>

    </div>
</div>
