<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_carta\models\TiempocomidaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiempo-comida-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tiem_com_id') ?>

    <?= $form->field($model, 'tiem_com_nombre') ?>

    <?= $form->field($model, 'tiem_com_hora_inicio') ?>

    <?= $form->field($model, 'tiempo_com_hora_fin') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
