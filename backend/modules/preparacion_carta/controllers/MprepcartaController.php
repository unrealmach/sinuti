<?php
namespace backend\modules\preparacion_carta\controllers;

use Yii;
use backend\modules\preparacion_carta\models\MPrepCarta;
use backend\modules\preparacion_carta\models\MprepcartaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MprepcartaController implements the CRUD actions for MPrepCarta model.
 */
class MprepcartaController extends Controller
{

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                        [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                        [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MPrepCarta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MprepcartaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MPrepCarta model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MPrepCarta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MPrepCarta();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_prep_carta_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MPrepCarta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_prep_carta_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MPrepCarta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MPrepCarta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MPrepCarta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MPrepCarta::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * obtiene los datos de los alimentos o platillos mediante ajax
     * crea el objeto para la clase PreparacionCartaSesion
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return JSON arreglo con el alimento o platillo ya en la sesion
     */
    public function actionAjaxGetAlimento()
    {
        $alimentoplatillo_id = \yii\helpers\Json::decode($_GET['idalimentoplatillo']);
        $posicion = \yii\helpers\Json::decode($_GET['posicion']);
        $cantidad = \yii\helpers\Json::decode($_GET['cantidad']);
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $dataCantidad = ['d_prep_carta_cantidad_g' => 0];
        if ($sesionDetalle->agregarDetalle($posicion, $alimentoplatillo_id, $cantidad)) {
            $cantidadDetalle = $sesionDetalle->getDetallePorPosicion($posicion);
            $dataCantidad = [
                'd_prep_carta_cantidad_g' => $cantidadDetalle['d_prep_carta_cantidad_g'],
            ];
        };
        return \yii\helpers\Json::encode($dataCantidad);
    }

    /**
     * Permite verificar los alimentos que se encuentran en la vista
     * y los coloca en la lista de alimentos y platillos seleccionados para que no se 
     * puedan repetir
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return JSON 
     */
    public function actionAjaxAlimentoSeleccionado()
    {
        $arrayItems = \yii\helpers\Json::decode($_GET['data']);
        Yii::$app->session->set('alimentoPlatilloSeleccionado', $arrayItems); ///objeto temporal
        return \yii\helpers\Json::encode($arrayItems);
    }

    /**
     * Se comunica con la sesion para indicar la elminacion de un detalle
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return boolean True si se elimino correctamente el detalle
     */
    public function actionAjaxEliminarDetalleAlimento()
    {
        $posicionEliminado = \yii\helpers\Json::decode($_GET['posicionEliminado']);
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        if ($sesionDetalle->eliminarDetalle($posicionEliminado)) {
            return \yii\helpers\Json::encode([true]);
        }
    }

    /**
     * Se comunica con la sesion para indicar que se cambio una cantidad 
     * @author Sofia Mejía <amandasofiamejia@gmail.com> 
     * @return boolean True indica que se realizo la operacion
     */
    public function actionAjaxCambiarCantidad()
    {
        $posicion = current(\yii\helpers\Json::decode($_GET['posicion']));
        $alimentoplatillo_id = \yii\helpers\Json::decode($_GET['alimentoplatillo_id']);
        $cantidad = \yii\helpers\Json::decode($_GET['cantidad']);
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        if ($sesionDetalle->cambiarCantidadDetalle($posicion, $alimentoplatillo_id, $cantidad)) {
            $detalleCambiado = $sesionDetalle->getDetallePorPosicion($posicion);
        }
        return \yii\helpers\Json::encode(['true']);
    }

    /**
     * Se comunica con la sesion para generar los datos del maestro
     *  a partir de los detalles y su composicion 
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return JSON array con los datos del maestro 
     */
    public function actionAjaxGenerarDataMaestro()
    {
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->generarTotalComposicion();
        $sesionDetalle->obtenerRangoNutrientes();
        $dataMaster = $sesionDetalle->getDataMaestro();
        $dataHighchart = $sesionDetalle->getCategoriesAndDataToHighchart();
        $dataTotal['masterSesion'] = $dataMaster;
        $dataTotal['dataHighchart'] = $dataHighchart;

//        $dataTotal["rango_nutrientes"] = $sesionDetalle->getRangoNutrientes();
        return \yii\helpers\Json::encode($dataTotal);
    }

    /**
     * permite la busque por ajax de los ingredientes, ademas de controlar los que
     *  ya se encuentran en la sesion
     * Alimenta los datos del Select2 alimento_id de cada detalle
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array con los datos para alimentar el select2
     * TODO: poner en modelo, probar en update con el parametro $id falta la implementacion
     */
    public function actionAjaxAlimentoList($q = null, $id = null)
    {
        $q=strtoupper($q);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query1 = new yii\db\Query;
        $query2 = new yii\db\Query;
        if (is_null($q)) {
            $query1->select([
                    new \yii\db\Expression("CONCAT('a_', alimento_id ) as id"),
                    new \yii\db\Expression("CONCAT('a:',alimento_nombre ) AS text"),
                    new \yii\db\Expression("CONCAT('a_', alimento_id ) AS iden")
                ])
                ->from('alimento')
            ;
        }
        if (!is_null($q)) {
            $query1->select(
                    [
                        new \yii\db\Expression("CONCAT('a_', alimento_id ) as id"),
                        new \yii\db\Expression("CONCAT('a:', alimento_nombre) AS text"),
                        new \yii\db\Expression("CONCAT('a_', alimento_id ) AS iden")
                ])
                ->from('alimento')
                ->where("alimento_nombre LIKE '%$q%'");
            $query2->select(
                    [
                        new \yii\db\Expression("CONCAT('p_', m_platillo_id ) as id "),
                        new \yii\db\Expression("CONCAT('p:', m_platillo_nombre ) AS text "),
                        new \yii\db\Expression("CONCAT('p_', m_platillo_id ) AS iden")
                ])
                ->from('m_platillo')
                ->where("m_platillo_nombre LIKE '%$q%'");

            $query1->union($query2, false);
        }
        $model = new MPrepCarta();
        if (!is_null(Yii::$app->session->get('alimentoPlatilloSeleccionado'))) {
            foreach (Yii::$app->session->get('alimentoPlatilloSeleccionado') as $value) {
                $aux = $model->validateDetalleAlimento($value);
                if ($aux[0] == "alimento_id" && $value != '') {
                    $query1->andWhere("alimento_id <> '$aux[1]'");
                } elseif ($aux[0] == "m_platillo_id" && $value != '') {
                    $query2->andWhere("m_platillo_id <> '$aux[1]'");
                }
            }
        }
        $query1->limit(10);
        $query2->limit(10);
        $command = $query1->createCommand();

        $data = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }

    /**
     * Permite almacenar en sesion los tipos de preparación de acuerdo a
     * un tiempo de comida seleccionado
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     * @return array que contiene el data del tipo_preparacion
     */
    public function actionAjaxJoinTiempoTipo()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $tiempo_comida = $sesionDetalle->getTiempoComidaSeleccionado();
        $out = $sesionDetalle->getIDType($tiempo_comida);
        $out2["results"] = $out;
        return $out2;
    }

    /**
     * Agrega a la session el tiempo_comida y la edad para generar los rangos
     * para la composicion nutricional del menú
     * @param string $nombreTiempo el nombre del tiempo de comida seleccionado
     * @param int $edad seleccionada
     * @author Sofia Mejía <amandasofiamejia@gmail.com>
     */
    public function actionAjaxSetTiempoTipo($nombreTiempo, $edad)
    {

        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->setTiempoComidaSeleccionado($nombreTiempo);
        $sesionDetalle->setGrupoEdadSeleccionado($edad);
        $sesionDetalle->validateCantidadDynamicForm($nombreTiempo);
    }

    public static function ajaxSetTiempoTipo($nombreTiempo, $edad)
    {

        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
        $sesionDetalle->setTiempoComidaSeleccionado($nombreTiempo);
        $sesionDetalle->setGrupoEdadSeleccionado($edad);
        $sesionDetalle->validateCantidadDynamicForm($nombreTiempo);
    }


//
//    public function actionVer()
//    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
//        $sesionDetalle->obtenerRangoNutrientes();
//        $out["rango_nutrientes"] = $sesionDetalle->getRangoNutrientes();
//        return $out;
//    }
//    public function actionEsReal()
//    {
//        $sesionDetalle = Yii::$app->session->get('preparacionCartaSession');
//        var_dump($sesionDetalle->getDetalles());
//        die();
//    }

    /* actions ajax para carta semanal */

    /**
     * Realiza una consulta de acuerdo a una palabra o letra
     * @param string $palabraClave
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return JSON
     */
    public function actionAjaxGetCartasSearch($palabraClave)
    {

       
        //todo pasar al modelo
        $modelToSearch = new MPrepCarta();
        $data = $modelToSearch->find()
            ->where('m_prep_carta_nombre like :palabraClave')
            ->addParams([':palabraClave' => $palabraClave])
            ->limit(40)
            ->all();
        $outData = [];
        foreach ($data as $key => $model) {
            $outData[$key] = [
                'm_prep_carta_id' => $model->m_prep_carta_id,
                'm_prep_carta_nombre' => $model->m_prep_carta_nombre,
                'm_prep_carta_descripcion' => $model->m_prep_carta_descripcion,
                'tiempo_comida_id' => str_replace(" ", "_", $model->tiempoComida->tiem_com_nombre),
                'grupo_edad_id' => $model->grupoEdad->grupo_edad_descripcion
            ];
        }
        //obtener el parametro pasado xx ajax
        //buscar en el modelo
        //devolver los datos del modelo
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $outData;
    }
}
