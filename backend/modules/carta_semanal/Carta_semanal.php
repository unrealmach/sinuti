<?php

namespace backend\modules\carta_semanal;

class Carta_semanal extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\carta_semanal\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
