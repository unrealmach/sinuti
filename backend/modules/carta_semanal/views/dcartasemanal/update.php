<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\DCartaSemanal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Dcarta Semanal',
]) . ' ' . $model->d_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dcarta Semanals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->d_c_sem_id, 'url' => ['view', 'id' => $model->d_c_sem_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="dcarta-semanal-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
