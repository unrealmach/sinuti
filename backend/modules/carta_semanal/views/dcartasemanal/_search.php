<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\DcartasemanalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dcarta-semanal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'd_c_sem_id') ?>

    <?= $form->field($model, 'd_c_sem_dia_semana') ?>

    <?= $form->field($model, 'm_carta_semanal_id') ?>

    <?= $form->field($model, 'm_prep_carta_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
