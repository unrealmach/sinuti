<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\MCartaSemanal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mcarta-semanal-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'cen_inf_id', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_fecha_inicio', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), 
                ['language' => 'es',
                 'inline' => false,
                 'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
                ])->label(null, ['class' => 'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_fecha_fin', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), 
                ['language' => 'es',
                 'inline' => false,
                 'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
                ])->label(null, ['class' => 'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_user_responsable', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_num_semana', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_capacidad_infantes', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'm_c_sem_observaciones')->textarea(['rows' => 6]) ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
            <center>
                <?= Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create') : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
            </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
