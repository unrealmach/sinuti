<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\carta_semanal\models\MCartaSemanal */

$this->title = $model->m_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mcarta Semanals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mcarta-semanal-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->m_c_sem_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->m_c_sem_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'm_c_sem_id',
            'cen_inf_id',
            'm_c_sem_fecha_inicio',
            'm_c_sem_fecha_fin',
            'm_c_sem_user_responsable',
            'm_c_sem_num_semana',
            'm_c_sem_capacidad_infantes',
            'm_c_sem_observaciones:ntext',
        ],
        ]) ?>

    </div>
</div>
