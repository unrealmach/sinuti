<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\carta_semanal\models\McartasemanalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Mcarta Semanals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mcarta-semanal-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Mcarta Semanal'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                        'm_c_sem_id',
            'cen_inf_id',
            'm_c_sem_fecha_inicio',
            'm_c_sem_fecha_fin',
            'm_c_sem_user_responsable',
            // 'm_c_sem_num_semana',
            // 'm_c_sem_capacidad_infantes',
            // 'm_c_sem_observaciones:ntext',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
