<?php
namespace backend\modules\carta_semanal\models\activequeries;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\Cibv;

/**
 * This is the ActiveQuery class for [[\backend\modules\carta_semanal\models\MCartaSemanal]].
 *
 * @see \backend\modules\carta_semanal\models\MCartaSemanal
 */
class MCartaSemanalQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\carta_semanal\models\MCartaSemanal[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\carta_semanal\models\MCartaSemanal|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Devuelve la carta semanal que tiene que ser usada en la semana actual
     * @param int $user_id id del usuario de rol coordinadorCibv
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function getCartaSemanalVigenteByCentroInfantil($user_id)
    {

        $CoodinadorModel = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(\Yii::$app->user->id)->one();

        if (!is_null($CoodinadorModel)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $CoodinadorModel->autoridades_cibv_id]);
            $cibv_id=  Cibv::find()->where(['cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
           
            return $this
                // TODO:REVISAR de donde concatena esos valores de m_c_sem_fecha_inicio y m_c_sem_fecha_fin
                //->andWhere("NOW() BETWEEN" .
                //new \yii\db\Expression(" CONCAT(m_c_sem_fecha_inicio,' ','00:00:00')") .
                //"  AND " . new \yii\db\Expression("CONCAT(m_c_sem_fecha_fin,' ','23:59:59')"))   )
                    ->andWhere(['cen_inf_id' => $cibv_id]);
        } else {
            throw new \yii\web\NotFoundHttpException('Este coordinador no tiene asignado un Cibv');
        }
        
       
        return null;
    }

    /**
     * Devuelve la(s) carta semanal que tiene que ser usada en la semana actual 
     * por el cibv al cual pertenesca el educador
     * @param int $user_id id del usuario de rol educador
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function getCartaSemanalVigenteByCentroInfantilByEducador($user_id)
    {
        $PeriodoActivo = \backend\modules\parametros_sistema\models\Periodo::find()->periodoActivo()->one();
        $SectorAsignadoEducadorModel = \backend\modules\recursos_humanos\models\SectorAsignadoEducador::find()
                ->where(['educador_id' => $user_id, 'periodo_id' => $PeriodoActivo->periodo_id])->one();
        $cibv_id = $SectorAsignadoEducadorModel->cen_inf_id;
        return $this->andWhere('NOW() BETWEEN m_c_sem_fecha_inicio AND m_c_sem_fecha_fin')
                ->andWhere(['cen_inf_id' => $cibv_id])
        ;
    }

    /**
     * Devuelve la(s) carta semanal que tiene un centro infantil
     * @param $int $cen_inf_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function cartaSemanalByCibv($cen_inf_id)
    {
        return $this->where('cen_inf_id = :cen_inf_id')
                ->addParams([':cen_inf_id' => $cen_inf_id]);
    }
}
