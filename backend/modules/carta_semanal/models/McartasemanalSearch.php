<?php

namespace backend\modules\carta_semanal\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\carta_semanal\models\MCartaSemanal;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

/**
 * McartasemanalSearch represents the model behind the search form about `backend\modules\carta_semanal\models\MCartaSemanal`.
 */
class McartasemanalSearch extends MCartaSemanal
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_c_sem_id', 'm_c_sem_num_semana', 'm_c_sem_capacidad_infantes'], 'integer'],
            [['m_c_sem_user_responsable', 'm_c_sem_fecha_inicio', 'm_c_sem_fecha_fin', 'm_c_sem_user_responsable', 'm_c_sem_observaciones', 'cen_inf_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MCartaSemanal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith('cenInf', 'm_carta_semanal.cen_inf_id = cibv.cen_inf_id');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        // pregunta si es coordinador
        if ($authAsiignmentMdel->item_name == 'coordinador') {

            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            //obtiene la asignacion del usuario hacia rol coordinador
            if (!empty($asignacionCoordinadorCibv)) {

                $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
                // busca la autoridad segun la asignacion
                if (!empty($autoridadCibv)) {

                    $cartasByCIbv = MCartaSemanal::find()->cartaSemanalByCibv($autoridadCibv->cen_inf_id)->all();
                    //busca si hay plan de alimentacion para el cibv
                    if (!empty($cartasByCIbv)) {
                        foreach ($cartasByCIbv as $value) {
                            //       $query->orHaving(['m_carta_semanal.m_c_sem_id' => $value->m_c_sem_id]);
                            $query->orFilterWhere(['=', 'm_carta_semanal.m_c_sem_id', $value->m_c_sem_id]);
                        }
                    } else {
                        //  $query->orHaving(['m_carta_semanal.m_c_sem_id' => 0]);
                    }
                } else {
                    //$query->orHaving(['m_carta_semanal.m_c_sem_id' => 0]);
                }
            } else {
                //$query->orHaving(['m_carta_semanal.m_c_sem_id' => 0]);
            }
        }
        $query->andFilterWhere([
            'm_c_sem_num_semana' => $this->m_c_sem_num_semana,
            'm_c_sem_capacidad_infantes' => $this->m_c_sem_capacidad_infantes,
            'm_c_sem_user_responsable'=> $this->m_c_sem_user_responsable,
        ]);
    
        $query
            ->andFilterWhere(['like', 'cibv.cen_inf_nombre', strtoupper($this->cen_inf_id)])
            ->andFilterWhere(['like', 'm_c_sem_observaciones', $this->m_c_sem_observaciones])
            ->andFilterWhere(['like', new \yii\db\Expression('(m_c_sem_fecha_inicio::TEXT)'), $this->m_c_sem_fecha_inicio])
            ->andFilterWhere(['like', new \yii\db\Expression('(m_c_sem_fecha_fin::TEXT)'), $this->m_c_sem_fecha_fin]);
     

        return $dataProvider;
    }

}

