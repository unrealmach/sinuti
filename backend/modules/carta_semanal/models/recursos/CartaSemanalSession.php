<?php

namespace backend\modules\carta_semanal\models\recursos;

use backend\modules\preparacion_carta\models\DPreparacionCarta;
use backend\modules\preparacion_carta\models\TipoPreparacionCarta;
use backend\modules\nutricion\models\Alimento;
use backend\modules\preparacion_platillo\models\MPlatillo;

/**
 * Clase que permite realizar los calculos de los detalles y maestro
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 */
class CartaSemanalSession {

    public function init() {
        parent::init();
    }

    private $cartaSemanal = [
        'LUNES' => [
            'DESAYUNO' => 0,
            'REFRIGERIO_DE_LA_MAÑANA' => 0,
            'ALMUERZO' => 0,
            'REFRIGERIO_DE_LA_TARDE' => 0,
//            ]
        ],
//        [
        'MARTES' => [
            'DESAYUNO' => 0,
            'REFRIGERIO_DE_LA_MAÑANA' => 0,
            'ALMUERZO' => 0,
            'REFRIGERIO_DE_LA_TARDE' => 0,
//            ]
        ],
//        [
        'MIERCOLES' => [
            'DESAYUNO' => 0,
            'REFRIGERIO_DE_LA_MAÑANA' => 0,
            'ALMUERZO' => 0,
            'REFRIGERIO_DE_LA_TARDE' => 0,
//            ]
        ],
//        [
        'JUEVES' => [
            'DESAYUNO' => 0,
            'REFRIGERIO_DE_LA_MAÑANA' => 0,
            'ALMUERZO' => 0,
            'REFRIGERIO_DE_LA_TARDE' => 0,
//            ]
        ],
//        [
        'VIERNES' => [
            'DESAYUNO' => 0,
            'REFRIGERIO_DE_LA_MAÑANA' => 0,
            'ALMUERZO' => 0,
            'REFRIGERIO_DE_LA_TARDE' => 0,
//            ]
        ],
    ];

    //  solo cuando es update, guarda los id de los detalles
//    private $cartaSemanalUpdate = [
//        'LUNES' => [
//            'DESAYUNO' => NULL,
//            'REFRIGERIO_DE_LA_MAÑANA' => NULL,
//            'ALMUERZO' => NULL,
//            'REFRIGERIO_DE_LA_TARDE' => NULL,
//        ],
//        'MARTES' => [
//            'DESAYUNO' => NULL,
//            'REFRIGERIO_DE_LA_MAÑANA' => NULL,
//            'ALMUERZO' => NULL,
//            'REFRIGERIO_DE_LA_TARDE' => NULL,
//        ],
//        'MIERCOLES' => [
//            'DESAYUNO' => NULL,
//            'REFRIGERIO_DE_LA_MAÑANA' => NULL,
//            'ALMUERZO' => NULL,
//            'REFRIGERIO_DE_LA_TARDE' => NULL,
//        ],
//        'JUEVES' => [
//            'DESAYUNO' => NULL,
//            'REFRIGERIO_DE_LA_MAÑANA' => NULL,
//            'ALMUERZO' => NULL,
//            'REFRIGERIO_DE_LA_TARDE' => NULL,
//        ],
//        'VIERNES' => [
//            'DESAYUNO' => NULL,
//            'REFRIGERIO_DE_LA_MAÑANA' => NULL,
//            'ALMUERZO' => NULL,
//            'REFRIGERIO_DE_LA_TARDE' => NULL,
//        ],
//    ];

    public function getCartaSemanal() {
        return $this->cartaSemanal;
    }

//    public function getCartaSemanalUpdate()
//    {
//        return $this->cartaSemanalUpdate;
//    }

    /**
     * Agrega todos los detalles de la CartaSemanal 
     * @param string $day el día de la semana correspondiente
     * @param string $time el tiempo de comida correspondiente
     * @param int $menu_id el id del MPrepCarta
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function setCartIdByDayAndTime($day, $time, $menu_id = 0) {
        $this->cartaSemanal[$day][$time] = $menu_id;
        $this->cartaSemanalWithHtml[$day][$time] = $menu_id;
    }

//    public function setCartUpdateIdByDayAndTime($day, $time, $detalle_id)
//    {
//        $this->cartaSemanalUpdate[$day][$time] = [$detalle_id => $detalle_id];
////        $this->cartaSemanalWithHtml[$day][$time] = $detalle_id;
//    }
//    public function setCartUpdateRemplazoIdByDayAndTime($day, $time)
//    {
//        var_dump($this->cartaSemanalUpdate[$day][$time]);
//        die();
//        foreach ($this->cartaSemanalUpdate[$day][$time] as $key => &$value) {
//            $this->cartaSemanalUpdate[$day][$time][$key]=NULL;
////            $value = NULL;
//        }
////        $this->cartaSemanalWithHtml[$day][$time] = $detalle_id;
//    }

    public function getDataDia($time) {
        return $this->getCartaSemanal[$time];
    }

    //variables para generar el HTML de los DPrepCarta
    public $tempHtmlDesayunosL = "<td>líquido</td>";
    public $tempHtmlDesayunosS = "<td>sólido</td>";
    public $tempHtmlDesayunosO = "<td>otro</td>";
    public $tempHtmlRefrigerioMMF = "<td>fruta</td>";
    public $tempHtmlRefrigerioMML = "<td>líquido</td>";
    public $tempHtmlAlmuerzoS = "<td>sopa</td>";
    public $tempHtmlAlmuerzoPF = "<td>plato fuerte</td>";
    public $tempHtmlAlmuerzoA = "<td>acompañado</td>";
    public $tempHtmlAlmuerzoE = "<td>ensalada</td>";
    public $tempHtmlAlmuerzoJ = "<td>jugo</td>";
    public $tempHtmlRefrigerioMTL = "<td>líquido</td>";
    public $tempHtmlRefrigerioMTS = "<td>sólido</td>";

    /**
     * Crea un html con los objetos de la carta semanal
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return string
     */
    public function createHTMLFromSessionObject() {

        $tabll = $this->cartaSemanal;
        $total1 = [];
        $desayunos = [];
        $almuerzos = [];
        $mm = [];
        $mt = [];
        $out = "";
        foreach ($tabll as $key => $value) {
            $desayunos[$key] = '<td>' . $value['DESAYUNO'] . '</td>';
            $almuerzos[$key] = '<td>' . $value['ALMUERZO'] . '</td>';
//            $mm[$key] = '<td>' . utf8_decode($value['REFRIGERIO_DE_LA_MAÃ‘ANA']) . '</td>';
            $mm[$key] = '<td>' . $value['REFRIGERIO_DE_LA_MAÑANA'] . '</td>';
            $mt[$key] = '<td>' . $value['REFRIGERIO_DE_LA_TARDE'] . '</td>';
        }

        $total1['DESAYUNO'] = $desayunos;
        $total1['ALMUERZO'] = $almuerzos;
        $total1['REFRIGERIO_DE_LA_MAÑANA'] = $mm;
        $total1['REFRIGERIO_DE_LA_TARDE'] = $mt;

        $rowCabecera = "<tr>	<th>	TIEMPO DE COMIDA	</th><th>	LUNES 	</th><th>	MARTES	</th><th>	MIERCOLES 	</th><th>	JUEVES 	</th><th>	VIERNES 	</th>	</tr>";
        $rowDesayuno = "<tr>	<td>	DESAYUNO (8h0)	</td><td>		</td><td>		</td><td>		</td><td>		</td><td>		</td>	</tr>";
        $rowRefrigerioMM = "<tr>	<td>	REFRIGERIO DE LA MAÑANA (10h0)	</td><td>		</td><td>		</td><td>		</td><td>		</td><td>		</td>	</tr>";
        $rowAlmuerzo = "<tr>	<td>	ALMUERZO (12h0)	</td><td>		</td><td>		</td><td>		</td><td>		</td><td>		</td>	</tr>";
        $rowRefrigerioMT = "<tr>	<td>	REFRIGERIO DE LA TARDE (15h0)	</td><td>		</td><td>		</td><td>		</td><td>		</td><td>		</td>	</tr>";

        $this->tempHtmlDesayunosL = "<td>líquido</td>";
        $this->tempHtmlDesayunosS = "<td>sólido</td>";
        $this->tempHtmlDesayunosO = "<td>otro</td>";
        $this->tempHtmlRefrigerioMMF = "<td>fruta</td>";
        $this->tempHtmlRefrigerioMML = "<td>líquido</td>";
        $this->tempHtmlAlmuerzoS = "<td>sopa</td>";
        $this->tempHtmlAlmuerzoPF = "<td>plato fuerte</td>";
        $this->tempHtmlAlmuerzoA = "<td>acompañado</td>";
        $this->tempHtmlAlmuerzoE = "<td>ensalada</td>";
        $this->tempHtmlAlmuerzoJ = "<td>jugo</td>";
        $this->tempHtmlRefrigerioMTL = "<td>líquido</td>";
        $this->tempHtmlRefrigerioMTS = "<td>sólido</td>";

        $outHtmlIni = "<table id='dprepcarta' class='table table-striped table-bordered' border='1'>";
        $outHtmlBody = "";
        $outHtmlFin = "</table>";
        $outHtmlBody.=$outHtmlIni;
        foreach ($total1 as $key => $dia) {
            foreach ($dia as $value) {
                switch ($key) {
                    case "DESAYUNO":
                        $this->generarHTMLDesayuno($value);
                        break;
                    case "REFRIGERIO_DE_LA_MAÑANA":
                        $this->generarHTMLRefrigerioMediaM($value);
                        break;
                    case "ALMUERZO":
                        $this->generarHTMLRefrigerioAlmuerzo($value);
                        break;
                    case "REFRIGERIO_DE_LA_TARDE":
                        $this->generarHTMLRefrigerioMediaT($value);
                        break;

                    default:
                        break;
                }
            }
        }
        $outHtmlBody.=$outHtmlFin;
        $out = $outHtmlIni . $rowCabecera .
                $rowDesayuno .
                "<tr>" .
                $this->tempHtmlDesayunosL .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlDesayunosS .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlDesayunosO .
                "</tr>" .
                $rowRefrigerioMM .
                "<tr>" .
                $this->tempHtmlRefrigerioMMF .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlRefrigerioMML .
                "</tr>" .
                $rowAlmuerzo .
                "<tr>" .
                $this->tempHtmlAlmuerzoS .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlAlmuerzoPF .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlAlmuerzoA .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlAlmuerzoE .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlAlmuerzoJ .
                "</tr>" .
                $rowRefrigerioMT .
                "<tr>" .
                $this->tempHtmlRefrigerioMTL .
                "</tr>" .
                "<tr>" .
                $this->tempHtmlRefrigerioMTS .
                "</tr>" .
                $outHtmlFin;
        return $out;
    }

    /**
     * Recupera los detalles de la carta semanal que pertenescan al desayuno
     * @param array $value
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function generarHTMLDesayuno($value) {
        preg_match_all('!\d+!', $value, $carta); //extrae numeros de una cadena
        $carta_id = (int) $carta[0][0];
        $alimentoPlatillo = $this->buscarPrepCarta($carta_id);
        if (!empty($alimentoPlatillo)) {
            if (array_key_exists('liquido', $alimentoPlatillo)) {
                $this->tempHtmlDesayunosL.= '<td>' . $alimentoPlatillo['liquido'] . '</td>';
            } else {
                $this->tempHtmlDesayunosL.= '<td></td>';
            }
            if (array_key_exists('solido', $alimentoPlatillo)) {
                $this->tempHtmlDesayunosS.= '<td>' . $alimentoPlatillo['solido'] . '</td>';
            } else {
                $this->tempHtmlDesayunosS.='<td></td>';
            }
            if (array_key_exists('otro', $alimentoPlatillo)) {
                $this->tempHtmlDesayunosO.= '<td>' . $alimentoPlatillo['otro'] . '</td>';
            } else {
                $this->tempHtmlDesayunosO.= '<td></td>';
            }
        } else {
            $this->tempHtmlDesayunosL.= '<td></td>';
            $this->tempHtmlDesayunosS.='<td></td>';
            $this->tempHtmlDesayunosO.= '<td></td>';
        }
    }

    /**
     * Recupera los detalles de la carta semanal que pertenescan al refrigerio
     * de la media mañana
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @param array $value
     */
    public function generarHTMLRefrigerioMediaM($value) {
        preg_match_all('!\d+!', $value, $carta); //extrae numeros de una cadena
        $carta_id = (int) $carta[0][0];
        $alimentoPlatillo = $this->buscarPrepCarta($carta_id);
        if (!empty($alimentoPlatillo)) {
            if (array_key_exists('liquido', $alimentoPlatillo)) {
                $this->tempHtmlRefrigerioMML.= '<td>' . $alimentoPlatillo['liquido'] . '</td>';
            } else {
                $this->tempHtmlRefrigerioMML.='<td></td>';
            }
            if (array_key_exists('fruta', $alimentoPlatillo)) {
                $this->tempHtmlRefrigerioMMF.= '<td>' . $alimentoPlatillo['fruta'] . '</td>';
            } else {
                $this->tempHtmlRefrigerioMMF.= '<td></td>';
            }
        } else {
            $this->tempHtmlRefrigerioMML.='<td></td>';
            $this->tempHtmlRefrigerioMMF.= '<td></td>';
        }
    }

    /**
     * Recupera los detalles de la carta semanal que pertenescan al almuerzo
     * @param array $value
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function generarHTMLRefrigerioAlmuerzo($value) {
        preg_match_all('!\d+!', $value, $carta); //extrae numeros de una cadena
        $carta_id = (int) $carta[0][0];
        $alimentoPlatillo = $this->buscarPrepCarta($carta_id);
        if (!empty($alimentoPlatillo)) {
            if (array_key_exists('sopa', $alimentoPlatillo)) {
                $this->tempHtmlAlmuerzoS.= '<td>' . $alimentoPlatillo['sopa'] . '</td>';
            } else {
                $this->tempHtmlAlmuerzoS.= '<td></td>';
            }
            if (array_key_exists('plato fuerte', $alimentoPlatillo)) {
                $this->tempHtmlAlmuerzoPF.= '<td>' . $alimentoPlatillo['plato fuerte'] . '</td>';
            } else {
                $this->tempHtmlAlmuerzoPF .= '<td></td>';
            }
            if (array_key_exists('acompañado', $alimentoPlatillo)) {
                $this->tempHtmlAlmuerzoA.= '<td>' . $alimentoPlatillo['acompañado'] . '</td>';
            } else {
                $this->tempHtmlAlmuerzoA .= '<td></td>';
            }
            if (array_key_exists('ensalada', $alimentoPlatillo)) {
                $this->tempHtmlAlmuerzoE.= '<td>' . $alimentoPlatillo['ensalada'] . '</td>';
            } else {
                $this->tempHtmlAlmuerzoE .= '<td></td>';
            }
            if (array_key_exists('jugo', $alimentoPlatillo)) {
                $this->tempHtmlAlmuerzoJ.= '<td>' . $alimentoPlatillo['jugo'] . '</td>';
            } else {
                $this->tempHtmlAlmuerzoJ.='<td></td>';
            }
        } else {
            $this->tempHtmlAlmuerzoS.= '<td></td>';
            $this->tempHtmlAlmuerzoPF.=

                    '<td></td>';
            $this->tempHtmlAlmuerzoA.='<td></td>';
            $this->tempHtmlAlmuerzoE .='<td></td>';
            $this->tempHtmlAlmuerzoJ.='<td></td>';
        }
    }

    /**
     * Recupera los detalles de la carta semanal que pertenescan al refrigerio
     * de la media tarde 
     * @param array $value
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function generarHTMLRefrigerioMediaT($value) {
        preg_match_all('!\d+!', $value, $carta); //extrae numeros de una cadena
        $carta_id = (int) $carta[0][0];
        $alimentoPlatillo = $this->buscarPrepCarta($carta_id);
        if (!empty($alimentoPlatillo)) {
            if (array_key_exists('liquido', $alimentoPlatillo)) {
                $this->tempHtmlRefrigerioMTL.= '<td>' . $alimentoPlatillo['liquido'] . '</td>';
            } else {
                $this->tempHtmlRefrigerioMTL .= '<td></td>';
            }
            if (array_key_exists('solido', $alimentoPlatillo)) {
                $this->tempHtmlRefrigerioMTS.= '<td>' . $alimentoPlatillo['solido'] . '</td>';
            } else {
                $this->
                        tempHtmlRefrigerioMTS.='<td></td>';
            }
        } else {
            $this->tempHtmlRefrigerioMTL.= '<td></td>';
            $this->tempHtmlRefrigerioMTS.='<td></td>';
        }
    }

    /**
     * busca el mprepcarta de acuerdo al id
     * @param int $carta_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return array descripción y nombre del mprepcarta seleccionado
     */
    public function buscarPrepCarta($carta_id) {
        $detalles_carta = [];
        $aux = [];
        if ($carta_id != 0) {
            $d_prep_carta = DPreparacionCarta::find()->byMPrepCarta($carta_id)->all();
            foreach ($d_prep_carta as $key => $value) {
                $tipo_prep_id = $value->tipo_preparacion_id;
                $tipo_prep = TipoPreparacionCarta::findOne(['tipo_prep_carta_id' => $tipo_prep_id]);
                $alimento_id = $value->alimento_id;
                $platillo_id = $value->m_platillo_id;
                if ($alimento_id != null) {
                    $alimento = Alimento::findOne(['alimento_id' => $alimento_id]);
                    $aux[$tipo_prep->tipo_prep_carta_descripcion] = $alimento->alimento_nombre;
                } else {
                    $platillo = MPlatillo::findOne(['m_platillo_id' => $platillo_id]);
                    $aux[$tipo_prep->tipo_prep_carta_descripcion] = $platillo->m_platillo_nombre;
                }
            }
        }
        return $aux;
    }

}
