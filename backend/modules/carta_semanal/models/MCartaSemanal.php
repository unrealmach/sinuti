<?php

namespace backend\modules\carta_semanal\models;

use backend\modules\centro_infantil\models\Cibv;
use backend\modules\carta_semanal\models\DCartaSemanal;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use Yii;

/**
 * This is the model class for table "m_carta_semanal".
 *
 * @property integer $m_c_sem_id
 * @property integer $cen_inf_id
 * @property string $m_c_sem_fecha_inicio
 * @property string $m_c_sem_fecha_fin
 * @property integer $m_c_sem_user_responsable
 * @property integer $m_c_sem_num_semana
 * @property integer $m_c_sem_capacidad_infantes
 * @property string $m_c_sem_observaciones
 *
 * @property AsignacionInfanteCSemanal[] $asignacionInfanteCSemanals
 * @property DCartaSemanal[] $dCartaSemanals
 * @property Cibv $cenInf
 */
class MCartaSemanal extends \yii\db\ActiveRecord {

    public $nombre_user;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'm_carta_semanal';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cen_inf_id', 'm_c_sem_fecha_inicio', 'm_c_sem_fecha_fin', 'm_c_sem_user_responsable', 'm_c_sem_num_semana', 'm_c_sem_capacidad_infantes', 'm_c_sem_observaciones'], 'required'],
            [['cen_inf_id', 'm_c_sem_num_semana', 'm_c_sem_capacidad_infantes'], 'integer'],
            [[ 'm_c_sem_num_semana'], 'integer',  'min'=>1],
            [[ 'm_c_sem_capacidad_infantes'],'integer', 'min'=>1],
            [['m_c_sem_fecha_inicio', 'm_c_sem_fecha_fin'], 'safe'],
            [['m_c_sem_observaciones'], 'string'],
            [['m_c_sem_user_responsable'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'm_c_sem_id' => Yii::t('app', 'M C Sem ID'),
            'cen_inf_id' => Yii::t('app', 'Centro Infantil'),
            'm_c_sem_fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'm_c_sem_fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'm_c_sem_user_responsable' => Yii::t('app', 'Usuario Responsable'),
            'nombre_user' => Yii::t('app', 'Usuario Responsable'),

            'm_c_sem_num_semana' => Yii::t('app', 'N° Semana'),
            'm_c_sem_capacidad_infantes' => Yii::t('app', 'Capacidad Infantes'),
            'm_c_sem_observaciones' => Yii::t('app', 'Observaciones'),
            'rangoFechas' => Yii::t('app', 'Rango de fechas'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignacionInfanteCSemanals() {
        return $this->hasMany(AsignacionInfanteCSemanal::className(), ['m_carta_semanal_id' => 'm_c_sem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDCartaSemanals() {
        return $this->hasMany(DCartaSemanal::className(), ['m_carta_semanal_id' => 'm_c_sem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenInf() {
        return $this->hasOne(Cibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    public function getUserResponsable() {
        return $this->hasOne(\common\models\User::className(), ['id' => 'm_c_sem_user_responsable']);
    }

    /**
     * Transforma el rangp de fechas
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return String
     */
    public function getRangoFechas() {
        $utilitario = new \common\components\Util();
        return $utilitario->transformDateTimeToDate($this->m_c_sem_fecha_inicio) . " - " . $utilitario->transformDateTimeToDate($this->m_c_sem_fecha_fin);
    }

    /**
     * devuele las obsweervaciones y transforma el rango de fechas 
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return string
     */
    public function getRangoFechasAndObservacion() {
        $utilitario = new \common\components\Util();
        return $this->m_c_sem_observaciones . " / " . $utilitario->transformDateTimeToDate($this->m_c_sem_fecha_inicio) . " - " . $utilitario->transformDateTimeToDate($this->m_c_sem_fecha_fin);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\carta_semanal\models\activequeries\MCartaSemanalQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\carta_semanal\models\activequeries\MCartaSemanalQuery(get_called_class());
    }

}
