<?php

namespace backend\modules\carta_semanal\models;

use backend\modules\preparacion_carta\models\MPrepCarta;
use Yii;

/**
 * This is the model class for table "d_carta_semanal".
 *
 * @property integer $d_c_sem_id
 * @property string $d_c_sem_dia_semana
 * @property integer $m_carta_semanal_id
 * @property integer $m_prep_carta_id
 *
 * @property MCartaSemanal $mCartaSemanal
 * @property MPrepCarta $mPrepCarta
 */
class DCartaSemanal extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'd_carta_semanal';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['d_c_sem_dia_semana', 'm_prep_carta_id'], 'required'],  
            [['d_c_sem_dia_semana'], 'string'],
            [['m_carta_semanal_id', 'm_prep_carta_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'd_c_sem_id' => Yii::t('app', 'D C Sem ID'),
            'd_c_sem_dia_semana' => Yii::t('app', 'D C Sem Dia Semana'),
            'm_carta_semanal_id' => Yii::t('app', 'M Carta Semanal ID'),
            'm_prep_carta_id' => Yii::t('app', 'M Prep Carta ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMCartaSemanal() {
        return $this->hasOne(MCartaSemanal::className(), ['m_c_sem_id' => 'm_carta_semanal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPrepCarta() {
        return $this->hasOne(MPrepCarta::className(), ['m_prep_carta_id' => 'm_prep_carta_id']);
    }

}
