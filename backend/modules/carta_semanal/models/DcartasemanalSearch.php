<?php

namespace backend\modules\carta_semanal\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\carta_semanal\models\DCartaSemanal;

/**
 * DcartasemanalSearch represents the model behind the search form about `backend\modules\carta_semanal\models\DCartaSemanal`.
 */
class DcartasemanalSearch extends DCartaSemanal
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_c_sem_id', 'm_carta_semanal_id', 'm_prep_carta_id'], 'integer'],
            [['d_c_sem_dia_semana'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DCartaSemanal::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'd_c_sem_id' => $this->d_c_sem_id,
            'm_carta_semanal_id' => $this->m_carta_semanal_id,
            'm_prep_carta_id' => $this->m_prep_carta_id,
        ]);

        $query->andFilterWhere(['like', 'd_c_sem_dia_semana', $this->d_c_sem_dia_semana]);

        return $dataProvider;
    }
}
