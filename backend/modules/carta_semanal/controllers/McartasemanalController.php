<?php
namespace backend\modules\carta_semanal\controllers;

use Yii;
use backend\modules\carta_semanal\models\MCartaSemanal;
use backend\modules\carta_semanal\models\McartasemanalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\preparacion_carta\models\MPrepCarta;
use backend\modules\preparacion_carta\models\TiempoComida;
use backend\modules\preparacion_carta\models\TipoPreparacionCarta;
use backend\modules\preparacion_carta\models\DPreparacionCarta;
use backend\modules\nutricion\models\Alimento;
use backend\modules\preparacion_platillo\models\MPlatillo;

/**
 * McartasemanalController implements the CRUD actions for MCartaSemanal model.
 */
class McartasemanalController extends Controller
{

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MCartaSemanal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new McartasemanalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MCartaSemanal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MCartaSemanal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MCartaSemanal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_c_sem_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MCartaSemanal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_c_sem_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MCartaSemanal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MCartaSemanal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MCartaSemanal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MCartaSemanal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Agrega a la sesion un mprepcarta via ajax de acuuerdo al tiempo
     *  de comida y día de la semana
     * Solo en create
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return type
     */
    public function actionAjaxDPrepCarta()
    {
        $carta_id = \yii\helpers\Json::decode($_GET['cartaid']);
        $dia = \yii\helpers\Json::decode($_GET['dia']);
        $tiempoComida = \yii\helpers\Json::decode($_GET['tiempoComida']);
        $session = Yii::$app->session->get('cartaSemanalSession');
        if ($carta_id == 0 && $dia == 0 && $tiempoComida == 0) {
//            No hace nada solo es para cargar en el modal los detalles
        } else {
            $session->setCartIdByDayAndTime($dia, $tiempoComida, $carta_id);
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $session->createHTMLFromSessionObject();
    }

    /**
     * Agrega a la sesion un mprepcarta via ajax de acuuerdo al tiempo
     *  de comida y día de la semana
     * Solo en Update
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return type
     */
    public function actionAjaxDprepCu()
    {
        $carta_id = \yii\helpers\Json::decode($_GET['cartaid']);
        $dia = \yii\helpers\Json::decode($_GET['dia']);
        $tiempoComida = \yii\helpers\Json::decode($_GET['tiempoComida']);
        $session = Yii::$app->session->get('cartaSemanalSession');
        if ($carta_id == 0 && $dia == 0 && $tiempoComida == 0) {
//            No hace nada solo es para cargar en el modal los detalles
        } else {
            $session->setCartIdByDayAndTime($dia, $tiempoComida, $carta_id);
        }
//        $session->setCartUpdateRemplazoIdByDayAndTime($dia, $tiempoComida);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $session->createHTMLFromSessionObject();
    }

    /**
     * Obtiene la capacidad de la carta semanal 
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return modelo del mcartasemanal seleccionado
     */
    public function actionAjaxGetCapacidadCarta()
    {

        $mcarta_id = $_GET['mcarta_id'];
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new MCartaSemanal();
        return $model->findOne(['m_c_sem_id' => $mcarta_id])->m_c_sem_capacidad_infantes;
    }

    /**
     * Por medio de una fecha envia el numero de semana y asigna la fecha fin 
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * return array $out 
     */
    public function actionAjaxNumSemSelect()
    {
        $fecha = $_GET['fecha'];
        $semana = strftime("%W", strtotime($fecha));
        $fecha_fin = date('Y-m-d', strtotime('next sunday', strtotime($fecha)));
        $out[$semana] = $fecha_fin;
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $out;
    }

    /**
     * Permite ver la session actual de la carta semanal
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function actionVerSession()
    {
        $session = Yii::$app->session->get('cartaSemanalSession');
        var_dump($session);
        die();
    }
    /**
     * 
     * @param type $dia
     * @param type $fecha
     * @return type
     */
//    public function actionAjaxDelCarta($dia = NULL, $fecha = NULL)
//    {
//        if (!is_null($dia) && !is_null($fecha)) {
//            
//        }
//        
////        print_r($this->renderAjax("@frontend/modules/carta_semanal/views/mcartasemanal/wizard/partials/_deleteMenu"));
////        die();
//        return $this->redirect("index");
//    }
}
