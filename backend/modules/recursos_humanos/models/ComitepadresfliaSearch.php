<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\ComitePadresFlia;

/**
 * ComitepadresfliaSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\ComitePadresFlia`.
 */
class ComitepadresfliaSearch extends ComitePadresFlia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comite_id'], 'integer'],
            [['comite_nombres', 'comite_apellidos', 'comite_dni', 'comite_numero_contacto', 'cargo', 'grupo_comite_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ComitePadresFlia::find();
        
        $query->joinWith('grupoComite', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comite_id' => $this->comite_id,
            //'grupo_comite_id' => $this->grupo_comite_id,
        ]);

        $query->andFilterWhere(['like', 'comite_nombres', $this->comite_nombres])
            ->andFilterWhere(['like', 'comite_apellidos', $this->comite_apellidos])
            ->andFilterWhere(['like', 'comite_dni', $this->comite_dni])
            ->andFilterWhere(['like', 'comite_numero_contacto', $this->comite_numero_contacto])
            ->andFilterWhere(['like', 'cargo', $this->cargo]);
        
        $query->andFilterWhere(
                ['like', 'grupo_comite.cibv.cen_inf_nombre', $this->grupo_comite_id]);

        return $dataProvider;
    }
}
