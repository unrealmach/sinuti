<?php
namespace backend\modules\recursos_humanos\models;

use Yii;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\recursos_humanos\models\CoordinadorCibv;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\inscripcion\models\Matricula;

/**
 * This is the model class for table "autoridades_cibv".
 *
 * @property integer $autoridades_cibv_id
 * @property integer $periodo_id
 * @property integer $coordinador_id
 * @property integer $cen_inf_id
 * @property string $fecha_inicio_actividad 
 * @property string $fecha_fin_actividad 
 *
 * @property Cibv $cenInf
 * @property CoordinadorCibv $coordinador
 * @property Periodo $periodo
 */
class AutoridadesCibv extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoridades_cibv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['periodo_id', 'coordinador_id', 'cen_inf_id', 'fecha_inicio_actividad'], 'required'],
            [['periodo_id', 'coordinador_id', 'cen_inf_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'autoridades_cibv_id' => Yii::t('app', 'Autoridades Cibv'),
            'periodo_id' => Yii::t('app', 'Período'),
            'coordinador_id' => Yii::t('app', 'Coordinador'),
            'cen_inf_id' => Yii::t('app', 'Centro Infantil'),
            'fecha_inicio_actividad' => 'Fecha Inicio Actividad',
            'fecha_fin_actividad' => 'Fecha Fin Actividad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenInf()
    {
        return $this->hasOne(Cibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoordinador()
    {
        return $this->hasOne(CoordinadorCibv::className(), ['coordinador_id' => 'coordinador_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodo()
    {
        return $this->hasOne(Periodo::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\recursos_humanos\models\activequeries\AutoridadesCibvQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\recursos_humanos\models\activequeries\AutoridadesCibvQuery(get_called_class());
    }

    /**
     * Obtiene una lista de infantes segun el cibv del coordinador
     * @param int $user_id coordinador
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public function getListInfantesByCoordinadorCibv($user_id)
    {
//        $user_id = Yii::$app->user->getId();
        $modelAsignacionCoordCibv = AsignacionUserCoordinadorCibv::find()->deUserId($user_id)->one();
        if ($modelAsignacionCoordCibv != null) {
            $centroInfantil_id = $modelAsignacionCoordCibv->autoridadCibv->cen_inf_id;
            $coordinador_cibv_id = $modelAsignacionCoordCibv->autoridadCibv->coordinador_id;

            $periodoActivo = Periodo::find()->periodoActivo()->one(); //TODO: poner en cache
            $periodoActivo_id = $periodoActivo->periodo_id;
            $validateCoordinadorAtual = $this->find()->deCibvAndCoordinadorAndPeriodo($centroInfantil_id, $coordinador_cibv_id, $periodoActivo_id)->one();
            if ($validateCoordinadorAtual != null) {
                //obtiene las matriculas para ese cibv y ese periodo
                $infantesCentroInfantil = Matricula::find()->deCentroInfantiIdAndPeriodoId($centroInfantil_id, $periodoActivo_id)->all();
                return $infantesCentroInfantil;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    /**
     * Recupera las autoridades validando si existe un periodo activo
     * @param int $cen_inf_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public static function getAutoridadesByPeriodo($cen_inf_id)
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            $autoridadCibvValido = AutoridadesCibv::find()->deCibvAndPeriodo($cen_inf_id, $periodo->periodo_id);
            return $autoridadCibvValido;
        } else {
            return AutoridadesCibv::find()->deCibvAndPeriodo($cen_inf_id, null);
        }
    }

    /**
     * Devuelve el id del CIbv al cual se asigno la autoridad
     * @param $asignacionCoordinadorCibv
     * @return int
     */
    public static function getCibvIdByAutoridad($asignacionCoordinadorCibv)
    {
        $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
        return  $autoridadCibv->cen_inf_id;
    }
}
