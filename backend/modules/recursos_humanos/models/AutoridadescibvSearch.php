<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

/**
 * AutoridadescibvSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\AutoridadesCibv`.
 */
class AutoridadescibvSearch extends AutoridadesCibv {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['autoridades_cibv_id'], 'integer'],
            [['periodo_id', 'coordinador_id', 'cen_inf_id', 'fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AutoridadesCibv::find();

        $query->joinWith('cenInf', true, 'INNER JOIN')->all();
        $query->joinWith('coordinador', true, 'INNER JOIN')->all();
        $query->joinWith('periodo', true, 'INNER JOIN')->all();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'autoridades_cibv_id' => $this->autoridades_cibv_id,
            //'periodo_id' => $this->periodo_id,
            //'coordinador_id' => $this->coordinador_id,
           // 'cen_inf_id' => $this->cen_inf_id,
        ]);

        $query->andFilterWhere(
                        ['like', 'cibv.cen_inf_nombre', $this->cen_inf_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(coordinador_nombres, " " ,coordinador_apellidos )'), $this->coordinador_id])
                ->andFilterWhere(['like', 'periodo.limitesFechasPeriodo', $this->periodo_id]
        );
        return $dataProvider;
    }

}
