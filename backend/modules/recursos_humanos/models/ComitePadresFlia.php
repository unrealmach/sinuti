<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use backend\modules\centro_infantil\models\GrupoComite;

/**
 * This is the model class for table "comite_padres_flia".
 *
 * @property integer $comite_id
 * @property string $comite_nombres
 * @property string $comite_apellidos
 * @property string $comite_dni
 * @property string $comite_numero_contacto
 * @property integer $grupo_comite_id
 * @property string $cargo
 *
 * @property GrupoComite $grupoComite
 */
class ComitePadresFlia extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'comite_padres_flia';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['comite_nombres', 'comite_apellidos', 'comite_dni', 'comite_numero_contacto', 'cargo'], 'required'],
//            [['comite_nombres', 'comite_apellidos', 'comite_cedula_o_pasaporte', 'comite_numero_contacto', 'grupo_comite_id', 'cargo'], 'required'],
            [['grupo_comite_id'], 'integer'],
            [['cargo'], 'string'],
            [['comite_nombres', 'comite_apellidos'], 'string', 'max' => 150],
            [['comite_dni'], 'string', 'max' => 45],
            [['comite_numero_contacto'], 'string', 'max' => 15],
            [['comite_dni'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'comite_id' => Yii::t('app', 'Comite ID'),
            'comite_nombres' => Yii::t('app', 'Nombres'),
            'comite_apellidos' => Yii::t('app', 'Apellidos'),
            'comite_dni' => Yii::t('app', 'DNI'),
            'comite_numero_contacto' => Yii::t('app', 'Contacto'),
            'grupo_comite_id' => Yii::t('app', 'Grupo Comite'),
            'cargo' => Yii::t('app', 'Cargo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoComite() {
        return $this->hasOne(GrupoComite::className(), ['grupo_comite_id' => 'grupo_comite_id']);
    }

    public function getNombreCompleto() {
        return $this->comite_nombres . " " . $this->comite_apellidos;
    }

}
