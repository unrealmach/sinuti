<?php

namespace backend\modules\recursos_humanos\models;

use backend\modules\centro_infantil\models\Cibv;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\inscripcion\models\Salon;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use Yii;

/**
 * This is the model class for table "sector_asignado_educador".
 *
 * @property integer $sector_asignado_educador_id
 * @property integer $cen_inf_id
 * @property integer $periodo_id
 * @property integer $educador_id
 * @property integer $salon_id
 * @property string $fecha_inicio_actividad 
 * @property string $fecha_fin_actividad 
 *
 * @property Cibv $cenInf
 * @property Periodo $periodo
 * @property Educador $educador
 * @property Salon $salon
 * @property SectorAsignadoInfante[] $sectorAsignadoInfantes
 */
class SectorAsignadoEducador extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sector_asignado_educador';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cen_inf_id', 'periodo_id', 'educador_id', 'salon_id', 'fecha_inicio_actividad'], 'required'],
            [['cen_inf_id', 'periodo_id', 'educador_id', 'salon_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'sector_asignado_educador_id' => Yii::t('app', 'Sector Asignado Educador ID'),
            'cen_inf_id' => Yii::t('app', 'Centro Infantil'),
            'periodo_id' => Yii::t('app', 'Período'),
            'educador_id' => Yii::t('app', 'Educador'),
            'salon_id' => Yii::t('app', 'Salón Infantil'),
            'fecha_inicio_actividad' => 'Fecha Inicio Actividad',
            'fecha_fin_actividad' => 'Fecha Fin Actividad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenInf() {
        return $this->hasOne(Cibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodo() {
        return $this->hasOne(Periodo::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducador() {
        return $this->hasOne(Educador::className(), ['educador_id' => 'educador_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalon() {
        return $this->hasOne(Salon::className(), ['salon_id' => 'salon_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignadoInfantes() {
        return $this->hasMany(SectorAsignadoInfante::className(), ['sector_asignado_id' => 'sector_asignado_educador_id']);
    }

    public function getSectorAsignadoInfante() {
        return $this->hasOne(SectorAsignadoInfante::className(), ['sector_asignado_id' => 'sector_asignado_educador_id']);
    }

    public function getByPeriodoActivo() {
        return $this->find()->andPeriodoActivo();
    }

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\activequeries\SectorAsignadoEducadorQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\recursos_humanos\models\activequeries\SectorAsignadoEducadorQuery(get_called_class());
    }

}
