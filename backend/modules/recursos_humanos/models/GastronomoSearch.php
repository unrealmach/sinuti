<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\Gastronomo;

/**
 * GastronomoSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\Gastronomo`.
 */
class GastronomoSearch extends Gastronomo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gastronomo_id'], 'integer'],
            [['gastronomo_dni', 'gastronomo_nombres', 'gastronomo_apellidos', 'gastronomo_telefono'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gastronomo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'gastronomo_id' => $this->gastronomo_id,
        ]);

        $query->andFilterWhere(['like', 'gastronomo_dni', $this->gastronomo_dni])
            ->andFilterWhere(['like', 'gastronomo_nombres', $this->gastronomo_nombres])
            ->andFilterWhere(['like', 'gastronomo_apellidos', $this->gastronomo_apellidos])
            ->andFilterWhere(['like', 'gastronomo_telefono', $this->gastronomo_telefono]);

        return $dataProvider;
    }
}
