<?php
namespace backend\modules\recursos_humanos\models\recursos;

use common\models\User;
use backend\modules\rbac\models\AuthAssignment;
use backend\modules\recursos_humanos\models\CoordinadorCibv;
use backend\modules\recursos_humanos\models\Nutricionista;
use backend\modules\recursos_humanos\models\Gastronomo;
use backend\modules\recursos_humanos\models\Educador;
//use autor

/**
 * Description of RrhhModel
 * Permite manejar a todos los usuarios de los x recursos humanos y viceversa
 * @author Mauricio Chamorro
 */
class RrhhModelAux extends \yii\base\Model
{

    /**
     * Devuelve el nombre completo del personal de talento humano 
     * de acuerdo al id del usuaario
     * @param integer $user_id
     * @return string NOmbre completo
     */
    public static function getNombresTalentHumanoByUser($user_id)
    {
        $user = User::find()->where('id = :user_id')->params([':user_id' => $user_id])->one();
        $rol = AuthAssignment::find()->where('user_id= :user_id')->params([':user_id' => $user_id])->one()->item_name;
        switch ($rol) {
            case 'coordinador':
                $coordinador = CoordinadorCibv::find()
                        ->where(['coordinador_correo' => $user->email])->one();
                return $coordinador->nombreCompleto;
            case 'nutricion':
                $nutricion = Nutricionista::find()
                        ->where(['nutricionista_correo' => $user->email])->one();
                return $nutricion->nombreCompleto;
            case 'gastronomo':
                $gastronomo = Gastronomo::find()
                        ->where(['gastronomo_correo' => $user->email])->one();
                return $gastronomo->nombreCompleto;
            case 'educador':
                $educador = Educador::find()
                        ->where(['educador_correo' => $user->email])->one();
                return $educador->nombreCompleto;
        }
    }

    /**
     * busca el personal de talento humano deacuerdo a sus roles y los devuelve
     *        en un arreglo con sus respectivos usuarios
     */
    public static function getUsersFromRrhhByRoles($listaRoles)
    {
        $listaUsuarios = [];
        foreach ($listaRoles as $key => $rol) {
            $usuarios = AuthAssignment::find()
                ->where("item_name = :nombre_rol")
                ->addParams([':nombre_rol' => $rol])
                ->all();
            foreach ($usuarios as $key2 => $usuario) {
                $talentoHumano = self::getNombresTalentHumanoByUser($usuario->user_id);
//                $listaUsuarios[$usuario->user_id]=$talentoHumano;
                array_push($listaUsuarios, ['user_id'=>$usuario->user_id,'nombres'=>$talentoHumano]);
            }
        }
        return $listaUsuarios;
//        var_dump($listaUsuarios);
//        die();
    }
}
