<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\Nutricionista;

/**
 * NutricionistaSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\Nutricionista`.
 */
class NutricionistaSearch extends Nutricionista
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutricionista_id'], 'integer'],
            [['nutricionista_dni', 'nutricionista_nombres', 'nutricionista_apellidos', 'nutricionista_telefono', 'nutricionista_correo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nutricionista::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'nutricionista_id' => $this->nutricionista_id,
        ]);

        $query->andFilterWhere(['like', 'nutricionista_dni', $this->nutricionista_dni])
            ->andFilterWhere(['like', 'nutricionista_nombres', $this->nutricionista_nombres])
            ->andFilterWhere(['like', 'nutricionista_apellidos', $this->nutricionista_apellidos])
            ->andFilterWhere(['like', 'nutricionista_telefono', $this->nutricionista_telefono]);

        return $dataProvider;
    }
}
