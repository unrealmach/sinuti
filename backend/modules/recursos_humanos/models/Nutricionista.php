<?php

namespace backend\modules\recursos_humanos\models;
use backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito;
use backend\modules\tecnico_area\models\AsignacionUserNutricionistaDistrito;


use Yii;

/**
 * This is the model class for table "nutricionista".
 *
 * @property integer $nutricionista_id
 * @property string $nutricionista_dni
 * @property string $nutricionista_nombres
 * @property string $nutricionista_apellidos
 * @property string $nutricionista_telefono
 * @property string $nutricionista_correo 
 * 
 * @property AsignacionNutricionistaDistrito[] $asignacionNutricionistaDistritos
 */
class Nutricionista extends \yii\db\ActiveRecord {

    public $sectorAsignado;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'nutricionista';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['nutricionista_dni', 'nutricionista_nombres', 'nutricionista_apellidos', 'nutricionista_telefono', 'nutricionista_correo', 'sectorAsignado'], 'required'],
            [['nutricionista_dni'], 'string', 'max' => 45],
            [['nutricionista_nombres', 'nutricionista_apellidos', 'nutricionista_correo'], 'string', 'max' => 150],
            [['nutricionista_telefono'], 'string', 'max' => 15],
            [['nutricionista_dni', 'nutricionista_correo'], 'unique'],
            [['nutricionista_correo'], 'email'],
            [['sectorAsignado'], 'integer'],
            ['nutricionista_correo', 'uniqueEmail'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['nutricionista_dni',
            'nutricionista_nombres',
            'nutricionista_apellidos',
            'nutricionista_telefono', 'sectorAsignado']; //Scenario Values Only Accepted
        return $scenarios;
    }

    public function uniqueEmail($attribute, $params) {
        $userModel = new \webvimark\modules\UserManagement\models\User();
        $userModel = $userModel->findOne(['email' => $this->nutricionista_correo]);
        if (!empty($userModel) ) {
            $this->addError($attribute, 'Este email ya ha sido usado');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'nutricionista_id' => Yii::t('app', 'Nutricionista ID'),
            'nutricionista_dni' => Yii::t('app', 'Documento Nacional de Identidad'),
            'nutricionista_nombres' => Yii::t('app', 'Nombres'),
            'nutricionista_apellidos' => Yii::t('app', 'Apellidos'),
            'nutricionista_telefono' => Yii::t('app', 'Teléfono'),
            'nutricionista_correo' => Yii::t('app', 'Correo'),
        ];
    }

    public function getNombreCompleto() {
        return $this->nutricionista_nombres . " " . $this->nutricionista_apellidos;
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getAsignacionNutricionistaDistritos() {
        return $this->hasMany(AsignacionNutricionistaDistrito::className(), ['nutricionista_id' => 'nutricionista_id']);
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getNombreUser() {
        $util = Yii::$app->Util;
        $str = $util->generarStringToUser($this->nutricionista_nombres, $this->nutricionista_apellidos, $this->nutricionista_dni);
        return $str;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getPasswordUser() {
        $util = Yii::$app->Util;
        $str = $util->generarContraseniaToUser($this->nutricionista_nombres, $this->nutricionista_apellidos);
        return $str;
    }

    /* --------------------------EVENTOS----------------------------- */

    /**
     * Permite crear un nuevo usuario de rol coordinador
     */
    const EVENT_CREATE_NEW_USER_NUTRICIONISTA = 'create-new-user-nutricionista';

    public function init() {
        $this->on(self::EVENT_CREATE_NEW_USER_NUTRICIONISTA, [$this, 'crearUsuarioNutricionista']);
        $this->on(self::EVENT_ASIGNAR_NUTRICIONISTA_SECTOR, [$this, 'asignarNutricionistaSector']);
         $this->on(self::EVENT_ASIG_USER_NUTRICIONISTA_DISTRITO, [$this, 'asignarUserNutricionistaDistrito']);
        return parent::init();
    }

    /**
     * Evento para crear un usuario de rol educador con los datos
     * del coordinador Cibv
     * @author Mauricio Chamorro
     * @param type $event
     */
    public function crearUsuarioNutricionista() {
        $modelUser = new \webvimark\modules\UserManagement\models\User();
        $modelUser->username = $this->nombreUser;
        $modelUser->password = $this->passwordUser;
        $modelUser->email = $this->nutricionista_correo;
        $modelUser->status = 1;
        $modelUser->email_confirmed = 0;
        $modelUser->save();

        $asignacionRol = $modelUser->assignRole($modelUser->id, 'nutricion');
        $modelUser->revokeRole($modelUser->id, 'educador');
        $modelUser->revokeRole($modelUser->id, 'Admin');
        $modelUser->revokeRole($modelUser->id, 'coordinador-gad');
        $modelUser->revokeRole($modelUser->id, 'coordinador');
        $modelUser->revokeRole($modelUser->id, 'gastronomo');


        if ($asignacionRol) {
            Yii::$app->SendGrid->sendEmailSendGrid(
                    [$modelUser->email], 'Credenciales SINUTI', 'texto nuevo', "<h1>User : $modelUser->username </h1> <h2>pass:   $modelUser->password </h2>", Yii::$app->SendGrid->getIdTemplate('creacion_usuario'));
            Yii::$app->getSession()->setFlash('success', 'Se ha creado el usuario con rol nutricionista, notificación enviada al email');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo crear el usuario');
        }
    }

    const EVENT_ASIGNAR_NUTRICIONISTA_SECTOR = 'asignar-nutricionista-sector';
    const EVENT_ASIG_USER_NUTRICIONISTA_DISTRITO = 'asignar-user-nutricionista-distrito';

    /**
     * Asigna el nutricionista a su distrito
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function asignarNutricionistaSector() {
        $modelSector = new AsignacionNutricionistaDistrito();
        $modelSector->nutricionista_id = $this->nutricionista_id;
        $modelSector->distrito_id = $this->sectorAsignado;
        $modelSector->fecha_inicio_actividad = date('Y-m-d', time());
        $modelSector->save();
    }
    
    
    /**
     * evento que permite crear un registro en la tabla asignacionusergastronomodistrito
     * @author Sofia Mejia <amandasofia@gmail.com>
     */
    public function asignarUserNutricionistaDistrito() {

        $asigNutricionistaDistrito = AsignacionNutricionistaDistrito::find()->deNutricionistaAndPeriodoActivo($this->nutricionista_id)->one();
        if (!empty($asigNutricionistaDistrito)) {
            $asigUserNutricionistaDistrito = new AsignacionUserNutricionistaDistrito();
            $userNutricionista = \webvimark\modules\UserManagement\models\User::findOne(['email' => $this->nutricionista_correo]);
            $asigUserNutricionistaDistrito->asignacion_nutricionista_distrito_id = $asigNutricionistaDistrito->asignacion_nutricionista_distrito_id;
            $asigUserNutricionistaDistrito->user_id = $userNutricionista->id;
            $asigUserNutricionistaDistrito->fecha = date('Y-m-d');
            $asigUserNutricionistaDistrito->observaciones = "Asignacion nutricionsita a un distrito";
            $asigUserNutricionistaDistrito->save();
        } else {
            Yii::$app->getSession()->setFlash('error', 'No existe un distrito asignado para ese nutricionista');
        }
    }

}
