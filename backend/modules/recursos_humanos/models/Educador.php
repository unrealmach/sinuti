<?php
namespace backend\modules\recursos_humanos\models;

use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use yii\helpers\VarDumper;
use Yii;

/**
 * This is the model class for table "educador".
 *
 * @property integer $educador_id
 * @property string $educador_dni
 * @property string $educador_nombres
 * @property string $educador_apellidos
 * @property string $educador_telefono
 * @property string $educador_correo 
 *
 * @property SectorAsignadoEducador[] $sectorAsignadoEducadors
 */
class Educador extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public $salon;
    public $nombre;

    public static function tableName()
    {
        return 'educador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['educador_dni', 'educador_nombres', 'educador_apellidos', 'educador_telefono', 'educador_correo', 'salon'], 'required'],
            [['educador_dni'], 'string', 'max' => 45],
            [['educador_nombres', 'educador_apellidos', 'educador_correo'], 'string', 'max' => 150],
            [['educador_telefono'], 'string', 'max' => 15],
            [['educador_dni'], 'unique'],
            [['educador_correo'], 'email'],
            ['educador_correo', 'uniqueEmail'],
            [['salon'], 'safe'],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['educador_dni', 'educador_nombres', 'educador_apellidos', 'educador_telefono'];
        return $scenarios;
    }
    

    public function uniqueEmail($attribute, $params)
    {
        
        // $userModel = $userModel->findOne(['email' => $this->educador_correo]);
        $checkExistUser = \webvimark\modules\UserManagement\models\User::find()
        ->where(['email' =>  $this->educador_correo])->exists();
        
        
        if ($checkExistUser) {
            $this->addError($attribute, 'Este email ya ha sido usado');
        }
    }
    
    
 
 
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'educador_id' => Yii::t('app', 'Educador ID'),
            'educador_dni' => Yii::t('app', 'Documento Nacional de Identidad'),
            'educador_nombres' => Yii::t('app', 'Nombres'),
            'educador_apellidos' => Yii::t('app', 'Apellidos'),
            'educador_telefono' => Yii::t('app', 'Teléfono'),
            'educador_correo' => Yii::t('app', 'Correo'),
            'salon' => Yii::t('app', 'Salón a asignar'),
            'nombreCompleto' => Yii::t('app', 'Nombre Completo'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    public function getNombreCompleto()
    {
        return $this->educador_nombres . " " . $this->educador_apellidos;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignadoEducadors()
    {
        return $this->hasMany(SectorAsignadoEducador::className(), ['educador_id' => 'educador_id']);
    }
    public function getSectorEducadorPeriodoActivo()
    {
        return $this->hasOne(SectorAsignadoEducador::className(), ['educador_id' => 'educador_id'])->andPeriodoActivo();
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getNombreUser()
    {
        $util = Yii::$app->Util;
        $str = $util->generarStringToUser($this->educador_nombres, $this->educador_apellidos, $this->educador_dni);
        return $str;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getPasswordUser()
    {
        $util = Yii::$app->Util;
        $str = $util->generarContraseniaToUser($this->educador_nombres, $this->educador_apellidos);
        return $str;
    }
    /* --------------------------EVENTOS----------------------------- */

    /**
     * Permite crear un nuevo usuario de rol coordinador
     */
    const EVENT_CREATE_NEW_USER_EDUCADOR = 'create-new-user-educador';

    public function init()
    {
        $this->on(self::EVENT_CREATE_NEW_USER_EDUCADOR, [$this, 'crearUsuarioEducador']);
        $this->on(self::SECTOR_ASIG_EDUCADOR, [$this, 'asignacionEducadorSalon']);
        $this->on(self::ASIG_USER_EDUCADOR_CIBV, [$this, 'asignarUserEducadorCibv']);
        return parent::init();
    }

    /**
     * Evento para crear un usuario de rol educador con los datos
     * del coordinador Cibv
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param type $event
     */
    public function crearUsuarioEducador()
    {
        $modelUser = new \webvimark\modules\UserManagement\models\User();
        $modelUser->username = $this->nombreUser;
        $modelUser->password = $this->passwordUser;
        $modelUser->email = $this->educador_correo;
        $modelUser->status = 1;
        $modelUser->email_confirmed = 0;
        $modelUser->save();
        
        $asignacionRol = $modelUser->assignRole($modelUser->id, 'educador');
        $modelUser->revokeRole($modelUser->id, 'coordinador');
        $modelUser->revokeRole($modelUser->id, 'Admin');
        $modelUser->revokeRole($modelUser->id, 'coordinador-gad');
        $modelUser->revokeRole($modelUser->id, 'gastronomo');
        $modelUser->revokeRole($modelUser->id, 'nutricion');
      
        
        if ($asignacionRol) {
            Yii::$app->SendGrid->sendEmailSendGrid(
                [$modelUser->email], 'credenciales SINUTI', 'texto nuevo', "<h1>User : $modelUser->username </h1> <h2>pass:  $modelUser->password </h2>", Yii::$app->SendGrid->getIdTemplate('creacion_usuario'));
            Yii::$app->getSession()->setFlash('success', 'Se ha creado el usuario con rol educador, notificación enviada al email');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo crear el usuario');
        }
    }

    const SECTOR_ASIG_EDUCADOR = 'sector-asignado-educador';
    const ASIG_USER_EDUCADOR_CIBV = 'asignacion-user-educador-cibv';

    /**
     * evento que asigna un salon a un educador 
     * @author Sofia Mejia <amandasofia@gmail.com>
     */
    public function asignacionEducadorSalon()
    {
        $validateAsignacion = SectorAsignadoEducador::find()->deEducadorAndPeriodoActivo($this->educador_id)->one();
        if (empty($validateAsignacion)) {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            if (!empty($asignacionCoordinadorCibv)) {
                $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
                $sectorAsignadoEducador = new SectorAsignadoEducador();
                $sectorAsignadoEducador->cen_inf_id = $autoridadCibv->cen_inf_id;
                $sectorAsignadoEducador->periodo_id = $autoridadCibv->periodo_id;
                $sectorAsignadoEducador->educador_id = $this->educador_id;
                $sectorAsignadoEducador->salon_id = $this->salon;
                $sectorAsignadoEducador->fecha_inicio_actividad = date('Y-m-d');
                $result = $sectorAsignadoEducador->save();
                if ($result) {
                    Yii::$app->getSession()->setFlash('success', 'Salon infantil asignado');
                } else {
//                    foreach ($sectorAsignadoEducador->getErrors() as $key => $value) {
//                        Yii::$app->getSession()->setFlash('error', 'No se registro el salon asignado ' . $value[0]);
//                    }
                    Yii::$app->getSession()->setFlash('error', 'error asignacion');
                }
            } else {
                Yii::$app->getSession()->setFlash('error', 'No existe un coordinador asignado al cibv');
            }
        } else {
            Yii::$app->getSession()->setFlash('error', 'El educador ya tiene asignado un salon infantil');
        }
    }

    /**
     * evento que permite crear un registro en la tabla asignacionusereducadorcibv
     * @author Sofia Mejia <amandasofia@gmail.com>
     */
    public function asignarUserEducadorCibv()
    {
        $sectorAsignadoEducador = SectorAsignadoEducador::find()->deEducadorAndPeriodoActivo($this->educador_id)->one();

        if (!empty($sectorAsignadoEducador)) {
            $asigUserEducadorCibv = new AsignacionUserEducadorCibv();
            $userEducador = \webvimark\modules\UserManagement\models\User::findOne(['email' => $this->educador_correo]);
            $asigUserEducadorCibv->sector_asignado_educador_id = $sectorAsignadoEducador->sector_asignado_educador_id;
            $asigUserEducadorCibv->user_id = $userEducador->id;
            $asigUserEducadorCibv->fecha = date('Y-m-d');
            $asigUserEducadorCibv->observaciones = "Asignacion educador al cibv";
            $asigUserEducadorCibv->save();
        } else {
            Yii::$app->getSession()->setFlash('error', 'No existe un salón infantil asignado para ese educador');
        }
    }
}
