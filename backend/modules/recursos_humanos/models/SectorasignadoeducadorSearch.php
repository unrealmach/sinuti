<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

/**
 * SectorasignadoeducadorSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\SectorAsignadoEducador`.
 */
class SectorasignadoeducadorSearch extends SectorAsignadoEducador {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sector_asignado_educador_id'], 'integer'],
            [['cen_inf_id', 'periodo_id', 'educador_id', 'salon_id', 'fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = SectorAsignadoEducador::find();

        $query->joinWith('cenInf', true, 'INNER JOIN')->all();
        $query->joinWith('periodo', true, 'INNER JOIN')->all();
        $query->joinWith('educador', true, 'INNER JOIN')->all();
        $query->joinWith('salon.grupoEdad', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();

        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->all();
            if (!empty($asignacionCoordinadorCibv)) {
                foreach ($asignacionCoordinadorCibv as $value1) {
                    $autoridadCibv = AutoridadesCibv::find()->deAutoridadAndPeriodoActivo($value1->autoridades_cibv_id)->one();
                    if (!empty($autoridadCibv)) {
                        $asignacionEducador = SectorAsignadoEducador::find()->deCibvAndPeriodoActivo($autoridadCibv->cen_inf_id)->all();

                        if (!empty($asignacionEducador)) {
                            foreach ($asignacionEducador as $value) {
                            //    $query->orHaving(['sector_asignado_educador.sector_asignado_educador_id' => $value->sector_asignado_educador_id]);
                                $query->orFilterWhere(['=', 'sector_asignado_educador.sector_asignado_educador_id', $value->sector_asignado_educador_id]);
                            }
                        } else {
                           // $query->orHaving(['sector_asignado_educador.sector_asignado_educador_id' => 0]);
                        }
                    } else {
                        // $query->orHaving(['sector_asignado_educador.sector_asignado_educador_id' => 0]);
                    }
                }
            } else {
                // $query->orHaving(['sector_asignado_educador.sector_asignado_educador_id' => 0]);
            }
        }

        $query->andFilterWhere([
            'sector_asignado_educador_id' => $this->sector_asignado_educador_id,
            'fecha_inicio_actividad' => $this->fecha_inicio_actividad,
            'fecha_fin_actividad' => $this->fecha_fin_actividad,
//            'cen_inf_id' => $this->cen_inf_id,
//            'periodo_id' => $this->periodo_id,
//            'educador_id' => $this->educador_id,
//            'salon_id' => $this->salon_id,
        ]);

        $query->andFilterWhere(
                        ['like', 'cibv.cen_inf_nombre', $this->cen_inf_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(periodo.fecha_inicio, " - ", periodo.fecha_fin)'), $this->periodo_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(educador.educador_nombres, " ", educador.educador_apellidos)'), $this->educador_id])
                ->andFilterWhere(['like', 'grupo_edad.grupo_edad_descripcion', $this->salon_id] //TODO no funciona 
        );



        return $dataProvider;
    }

}
