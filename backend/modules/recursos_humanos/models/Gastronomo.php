<?php

namespace backend\modules\recursos_humanos\models;
use backend\modules\tecnico_area\models\AsignacionGastronomoDistrito;
use backend\modules\tecnico_area\models\AsignacionUserGastronomoDistrito;
use backend\modules\ubicacion\models\Distrito;

use Yii;

/**
 * This is the model class for table "gastronomo".
 *
 * @property integer $gastronomo_id
 * @property string $gastronomo_dni
 * @property string $gastronomo_nombres
 * @property string $gastronomo_apellidos
 * @property string $gastronomo_telefono
 * @property string $gastronomo_correo 
 * 
 * @property AsignacionGastronomoDistrito[] $asignacionGastronomoDistritos
 */
class Gastronomo extends \yii\db\ActiveRecord {

    public $sectorAsignado;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'gastronomo';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sectorAsignado', 'gastronomo_dni', 'gastronomo_nombres', 'gastronomo_apellidos', 'gastronomo_telefono', 'gastronomo_correo'], 'required'],
            [['gastronomo_dni'], 'string', 'max' => 45],
            [['gastronomo_nombres', 'gastronomo_apellidos', 'gastronomo_correo'], 'string', 'max' => 150],
            [['gastronomo_telefono'], 'string', 'max' => 15],
            [['gastronomo_dni', 'gastronomo_correo'], 'unique'],
            [['gastronomo_correo'], 'email'],
            [['sectorAsignado'], 'integer'],
            ['gastronomo_correo', 'uniqueEmail'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['sectorAsignado',
            'gastronomo_dni',
            'gastronomo_nombres',
            'gastronomo_apellidos',
            'gastronomo_telefono',
            'sectorAsignado',
        ]; //Scenario Values Only Accepted
        return $scenarios;
    }

    public function uniqueEmail($attribute, $params) {
        $userModel = new \webvimark\modules\UserManagement\models\User();
        $userModel = $userModel->findOne(['email' => $this->gastronomo_correo]);
        if (!empty($userModel)) {
            $this->addError($attribute, 'Este email ya ha sido usado');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'gastronomo_id' => Yii::t('app', 'Gastrónomo ID'),
            'gastronomo_dni' => Yii::t('app', 'Documento Nacional de Identidad'),
            'gastronomo_nombres' => Yii::t('app', 'Nombres'),
            'gastronomo_apellidos' => Yii::t('app', 'Apellidos'),
            'gastronomo_telefono' => Yii::t('app', 'Teléfono'),
            'gastronomo_correo' => Yii::t('app', 'Correo')
        ];
    }

    public function getNombreCompleto() {
        return $this->gastronomo_nombres . " " . $this->gastronomo_apellidos;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getNombreUser() {
        $util = Yii::$app->Util;
        $str = $util->generarStringToUser($this->gastronomo_nombres, $this->gastronomo_apellidos, $this->gastronomo_dni);
        return $str;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getPasswordUser() {
        $util = Yii::$app->Util;
        $str = $util->generarContraseniaToUser($this->gastronomo_nombres, $this->gastronomo_apellidos);
        return $str;
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getAsignacionGastronomoDistritos() {
        return $this->hasMany(AsignacionGastronomoDistrito::className(), ['gastronomo_id' => 'gastronomo_id']);
    }

    /* --------------------------EVENTOS----------------------------- */

    /**
     * Permite crear un nuevo usuario de rol coordinador
     */
    const EVENT_CREATE_NEW_USER_GASTRONOMO = 'create-new-user-gastronomo';

    public function init() {
        $this->on(self::EVENT_CREATE_NEW_USER_GASTRONOMO, [$this, 'crearUsuarioGastronomo']);
        $this->on(self::EVENT_ASIGNAR_GASTRONOMO_SECTOR, [$this, 'asignarGastronomoSector']);
        $this->on(self::EVENT_ASIG_USER_GASTRONOMO_DISTRITO, [$this, 'asignarUserGastronomoDistrito']);
        return parent::init();
    }

    /**
     * Evento para crear un usuario de rol educador con los datos
     * del coordinador Cibv
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param type $event
     */
    public function crearUsuarioGastronomo() {
        $modelUser = new \webvimark\modules\UserManagement\models\User();
        $modelUser->username = $this->nombreUser;
        $modelUser->password = $this->passwordUser;
        $modelUser->email = $this->gastronomo_correo;
        $modelUser->status = 1;
        $modelUser->email_confirmed = 0;
        $modelUser->save();

        $asignacionRol = $modelUser->assignRole($modelUser->id, 'gastronomo');
        $modelUser->revokeRole($modelUser->id, 'educador');
        $modelUser->revokeRole($modelUser->id, 'Admin');
        $modelUser->revokeRole($modelUser->id, 'coordinador-gad');
        $modelUser->revokeRole($modelUser->id, 'coordinador');
        $modelUser->revokeRole($modelUser->id, 'nutricion');

        if ($asignacionRol) {
            Yii::$app->SendGrid->sendEmailSendGrid(
                    [$modelUser->email], 'Credenciales SINUTI', 'texto nuevo', "<h1>User : $modelUser->username </h1> <h2>pass:  $modelUser->password </h2>", Yii::$app->SendGrid->getIdTemplate('creacion_usuario'));
            Yii::$app->getSession()->setFlash('success', 'Se ha creado el usuario con rol gastronomo, notificación enviada al email');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo crear el usuario');
        }
    }

    const EVENT_ASIGNAR_GASTRONOMO_SECTOR = 'asignar-gastronomo-sector';
    const EVENT_ASIG_USER_GASTRONOMO_DISTRITO = 'asignar-user-gastronomo-distrito';

    /**
     * Asigna el gastronomo a su distrito
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function asignarGastronomoSector() {
        
        $modelSector = new AsignacionGastronomoDistrito();
        $modelSector->gastronomo_id = $this->gastronomo_id;
        $modelSector->distrito_id = $this->sectorAsignado;
        $modelSector->fecha_inicio_actividad = date('Y-m-d', time());
        $modelSector->save();
    }

    /**
     * evento que permite crear un registro en la tabla asignacionusergastronomodistrito
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public function asignarUserGastronomoDistrito() {

        $asigGastronomoDistrito = AsignacionGastronomoDistrito::find()->deGastronomoAndPeriodoActivo($this->gastronomo_id)->one();
        if (!empty($asigGastronomoDistrito)) {
            $asigUsergastronomoDistrito = new AsignacionUserGastronomoDistrito();
            $userGastronomo = \webvimark\modules\UserManagement\models\User::findOne(['email' => $this->gastronomo_correo]);
            $asigUsergastronomoDistrito->asignacion_gastronomo_distrito_id = $asigGastronomoDistrito->asignacion_gastronomo_distrito_id;
            $asigUsergastronomoDistrito->user_id = $userGastronomo->id;
            $asigUsergastronomoDistrito->fecha = date('Y-m-d');
            $asigUsergastronomoDistrito->observaciones = "Asignacion gastronomo a un distrito";
            $asigUsergastronomoDistrito->save();
        } else {
            Yii::$app->getSession()->setFlash('error', 'No existe un distrito asignado para ese gastronomo');
        }
    }

}


