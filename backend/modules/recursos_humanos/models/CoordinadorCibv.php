<?php

namespace backend\modules\recursos_humanos\models;

use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use yii\helpers\VarDumper;
use Yii;

/**
 * This is the model class for table "coordinador_cibv".
 *
 * @property integer $coordinador_id
 * @property string $coordinador_nombres
 * @property string $coordinador_apellidos
 * @property string $coordinador_DNI
 * @property string $coordinador_contacto
 * @property string $coordinador_correo
 *
 * @property AutoridadesCibv[] $autoridadesCibvs
 */
class CoordinadorCibv extends \yii\db\ActiveRecord {

    public $periodo;
    public $cibv;
    public $nombre;

    
    public static function tableName() {
        return 'coordinador_cibv';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['coordinador_nombres', 'coordinador_apellidos', 'coordinador_DNI', 'coordinador_contacto', 'coordinador_correo','cibv','periodo'], 'required'],
            [['coordinador_nombres', 'coordinador_apellidos', 'coordinador_correo'], 'string', 'max' => 150],
            [['coordinador_DNI'], 'string', 'max' => 45],
            [['coordinador_contacto'], 'string', 'max' => 15],
            [['coordinador_DNI', 'coordinador_correo'], 'unique'],
            [['coordinador_correo'], 'email'],
            [['periodo', 'cibv'], 'safe'],
            ['coordinador_correo', 'uniqueEmail'],
        ];
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['coordinador_nombres',
            'coordinador_apellidos',
            'coordinador_DNI',
            'coordinador_contacto',
        ]; //Scenario Values Only Accepted
        return $scenarios;
    }

    public function uniqueEmail($attribute, $params) {
        $userModel = new \webvimark\modules\UserManagement\models\User();
        $userModel = $userModel->findOne(['email' => $this->coordinador_correo]);
        //TODO: add to changes
        if (!empty($userModel)) {
            $this->addError($attribute, 'Este email ya ha sido usado');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'coordinador_id' => Yii::t('app', 'Coordinador ID'),
            'coordinador_nombres' => Yii::t('app', 'Nombres'),
            'coordinador_apellidos' => Yii::t('app', 'Apellidos'),
            'coordinador_DNI' => Yii::t('app', 'Documento Nacional de Identidad'),
            'coordinador_contacto' => Yii::t('app', 'Contacto'),
            'coordinador_correo' => Yii::t('app', 'Correo'),
            'periodo' => Yii::t('app', 'Período'),
            'cibv' => Yii::t('app', 'Centro Infantil'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    public function getNombreCompleto() {
        return $this->coordinador_nombres . " " . $this->coordinador_apellidos;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getNombreUser() {
        $util = Yii::$app->Util;
        $str = $util->generarStringToUser($this->coordinador_nombres, $this->coordinador_apellidos, $this->coordinador_DNI);
        return $str;
    }

    /**
     * Genera una cadena para el username
     * @author Mauricio Chamorro
     * @return type
     */
    public function getPasswordUser() {
        $util = Yii::$app->Util;
        $str = $util->generarContraseniaToUser($this->coordinador_nombres, $this->coordinador_apellidos);
        return $str;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoridadesCibvs() {
        return $this->hasMany(AutoridadesCibv::className(), ['coordinador_id' => 'coordinador_id']);
    }

    public function getAutoridadCibv() {
        return $this->hasOne(AutoridadesCibv::className(), ['coordinador_id' => 'coordinador_id']);
    }

    /**
     * Obtiene el id del Usuario al que pertenece el registro del coordinador 
     * comparando los emails
     * @return type
     */
    public  function getUserByEmail() {
        return $this->hasOne(\webvimark\modules\UserManagement\models\User::className(), ['email' => 'coordinador_correo']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\recursos_humanos\models\activequeries\CoordinadorCibvQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\recursos_humanos\models\activequeries\CoordinadorCibvQuery(get_called_class());
    }

    /* --------------------------EVENTOS----------------------------- */

    /**
     * Permite crear un nuevo usuario de rol coordinador
     */
    const EVENT_CREATE_NEW_USER_COORDINADOR = 'create-new-user-coordinador';

    public function init() {
        $this->on(self::EVENT_CREATE_NEW_USER_COORDINADOR, [$this, 'crearUsuarioCoordinador']);
        $this->on(self::EVENT_AUTORIDAD_CIBV, [$this, 'crearAutoridadCibv']);
        $this->on(self::EVENT_ASIG_USER_COORDINADOR_CIBV, [$this, 'asignarUserCoordinadorCibv']);
        return parent::init();
    }

    /**
     * Evento para crear un usuario de rol coordinador con los datos
     * del coordinador Cibv
     * @author Mauricio Chamorro <unrealmach@hotmsil.com>
     * @param type $event
     */
    public function crearUsuarioCoordinador() {
        $modelUser = new \webvimark\modules\UserManagement\models\User();
        $modelUser->username = $this->nombreUser;
        $modelUser->password = $this->passwordUser;
        $modelUser->email = $this->coordinador_correo;
        $modelUser->status = 1;
        $modelUser->email_confirmed = 0;
        $modelUser->save();

        $asignacionRol = $modelUser->assignRole($modelUser->id, 'coordinador');
        $modelUser->revokeRole($modelUser->id, 'educador');
        $modelUser->revokeRole($modelUser->id, 'Admin');
        $modelUser->revokeRole($modelUser->id, 'coordinador-gad');
        $modelUser->revokeRole($modelUser->id, 'gastronomo');
        $modelUser->revokeRole($modelUser->id, 'nutricion');


        if ($asignacionRol) {
            Yii::$app->SendGrid->sendEmailSendGrid(
                    [$modelUser->email], 'Credenciales SINUTI', 'texto nuevo', "<h1>Usuario: $modelUser->username </h1> <h2>Password:  $modelUser->password </h2>", Yii::$app->SendGrid->getIdTemplate('creacion_usuario'));
            Yii::$app->getSession()->setFlash('success', 'Se ha creado el usuario con rol coordinador, notificación enviada al email');
        } else {
         
            Yii::$app->getSession()->setFlash('error', 'No se pudo crear el usuario');
         //   \yii\helpers\VarDumper::dump( $model->errors,10,true);
         //   die("crearusercoordinador");
        }
    }

    const EVENT_AUTORIDAD_CIBV = 'autoridades-cibv';

    public function crearAutoridadCibv() {
        $autoridadCibv = new AutoridadesCibv();
        $autoridadCibv->periodo_id = $this->periodo;
        $autoridadCibv->cen_inf_id = $this->cibv;
        $autoridadCibv->coordinador_id = $this->coordinador_id;
        $autoridadCibv->fecha_inicio_actividad = date('Y-m-d');
        if ($autoridadCibv->save()) {
            Yii::$app->getSession()->setFlash('success', 'Se asigno el coordinador al cibv');
        } else {
           
            Yii::$app->getSession()->setFlash('error', 'No se pudo asignar el coordinador al cibv');
         //   \yii\helpers\VarDumper::dump( $autoridadCibv->errors,10,true);
         //   die("crearautoridadcibv");
        }
    }

    const EVENT_ASIG_USER_COORDINADOR_CIBV = 'asignacacion-user-coordinador-cibv';

    /**
     * Evento que permite crear un registro en la tabla asignacion_user_coordinador_cibv
     * de acuerdo al coordinador_cibv seleccionado
     * @author Sofia Mejia <amandasofia@gmail.com>
     */
    public function asignarUserCoordinadorCibv() {
        $userCoordinadorCibv = \webvimark\modules\UserManagement\models\User::findOne(['email' => $this->coordinador_correo]);
        if (!empty($userCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::find()->deCoordinador($this->coordinador_id)->one();
            if (!empty($autoridadCibv)) {
                $modelAsignacionUserCoordinadorCibv = new AsignacionUserCoordinadorCibv();
                $modelAsignacionUserCoordinadorCibv->user_id = $userCoordinadorCibv->id;
                $modelAsignacionUserCoordinadorCibv->autoridades_cibv_id = $autoridadCibv->autoridades_cibv_id;
                $modelAsignacionUserCoordinadorCibv->fecha = date("Y-m-d");
                $modelAsignacionUserCoordinadorCibv->observaciones = "Asignacion coordinador al cibv";
                if ($modelAsignacionUserCoordinadorCibv->save()) {
                    Yii::$app->getSession()->setFlash('success', 'autoridad cibv asignada');
                } else {
                   
                    Yii::$app->getSession()->setFlash('error', 'No se pudo asignar autoridad cibv');
                 //   \yii\helpers\VarDumper::dump( $modelAsignacionUserCoordinadorCibv->errors,10,true);
                  //  die("asignarusercoordinadorcibv");
                }
            }else{
                Yii::$app->getSession()->setFlash('error', 'autoridadCibv vacio');
              //  \yii\helpers\VarDumper::dump( $userCoordinadorCibv,10,true);
              //  die("empty2");
            }
        
    }else{
        Yii::$app->getSession()->setFlash('error', 'user coordinador cibv vacio');
     //   \yii\helpers\VarDumper::dump( $userCoordinadorCibv,10,true);
     //   die("empty");
    }
    }

}
