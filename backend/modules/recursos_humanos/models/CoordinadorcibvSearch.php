<?php

namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\CoordinadorCibv;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

/**
 * CoordinadorcibvSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\CoordinadorCibv`.
 */
class CoordinadorcibvSearch extends CoordinadorCibv
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['coordinador_id'], 'integer'],
                [['coordinador_nombres', 'coordinador_apellidos', 'coordinador_DNI',
                'coordinador_contacto',
                'coordinador_correo', 'nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CoordinadorCibv::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                'user_id' => Yii::$app->user->id])->one();
        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // si rol es coordinador le permite ver las metricas de los infantes que pertenecen al centro infantil durante un periodo activo
                $arrayIds = $this->getCoincidenciasCoordinador();
                $query->andWhere(['IN', 'coordinador_cibv.coordinador_id',
                    $arrayIds]);
                break;
            case 'Admin'||'coordinador-gad': // si rol es coordinador le permite ver las metricas de los infantes que pertenecen al centro infantil durante un periodo activo
                $arrayIds = $this->getCoincidenciasAdministrador();
                $query->andWhere(['IN', 'coordinador_cibv.coordinador_id',
                    $arrayIds]);
                break;

            //          case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        if ($authAsiignmentMdel->item_name == 'coordinador') {

        }
        $query->andFilterWhere([
            'coordinador_id' => $this->coordinador_id,
        ]);

        $query->andFilterWhere(['like', 'coordinador_DNI', $this->coordinador_DNI])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(coordinador_nombres, " " ,coordinador_apellidos )'),
                $this->nombre])
            ->andFilterWhere(['like', 'coordinador_contacto', $this->coordinador_contacto])
            ->andFilterWhere(['like', 'coordinador_correo', $this->coordinador_correo]);

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador()
    {
        $arrayIds                  = [];
        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserId(Yii::$app->user->id)->all();
        if (!empty($asignacionCoordinadorCibv)) {
            foreach ($asignacionCoordinadorCibv as $value1) {
                $autoridadesCibv = AutoridadesCibv::find()->where(['autoridades_cibv_id' => $value1->autoridades_cibv_id])->all();
                if (!empty($autoridadesCibv)) {
                    foreach ($autoridadesCibv as $value2) {
                        array_push($arrayIds, $value2->coordinador_id);
                    }
                }
            }
        }
        return $arrayIds;
    }
    private function getCoincidenciasAdministrador()
    {
        $arrayIds                  = [];
                $autoridadesCibv = AutoridadesCibv::find()->all();
                if (!empty($autoridadesCibv)) {
                    foreach ($autoridadesCibv as $value2) {
                        array_push($arrayIds, $value2->coordinador_id);
                    }
                }
        return $arrayIds;
    }
}