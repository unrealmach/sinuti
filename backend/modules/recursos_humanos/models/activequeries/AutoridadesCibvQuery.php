<?php

namespace backend\modules\recursos_humanos\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\recursos_humanos\models\AutoridadesCibv]].
 *
 * @see \backend\modules\recursos_humanos\models\AutoridadesCibv
 */
class AutoridadesCibvQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\AutoridadesCibv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\AutoridadesCibv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function deCibvAndCoordinadorAndPeriodo($cibv_id, $coordinador_id, $periodo_id)
    {
        return $this->where(['cen_inf_id' => $cibv_id, 'coordinador_id' => $coordinador_id, 'periodo_id' => $periodo_id]);
    }

    public function deCibvAndPeriodo($cen_inf_id, $periodo_id)
    { $dasd=$this->where(['cen_inf_id' => $cen_inf_id, 'periodo_id' => $periodo_id, 'fecha_fin_actividad' => NULL]);

        return $dasd;

    }

    public function deCoordinador($coordinador_id)
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where(['coordinador_id' => $coordinador_id, 'fecha_fin_actividad' => NULL, 'periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where(['coordinador_id' => $coordinador_id, 'periodo_id' => null]);
        }
    }

    public function deAutoridadAndPeriodoActivo($autoridad_id)
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where(['autoridades_cibv_id' => $autoridad_id, 'periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where(['autoridades_cibv_id' => $autoridad_id, 'periodo_id' => null]);
        }
    }

    public function dePeriodoActivo()
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where(['periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where(['periodo_id' => null]);
        }
    }



}
