<?php

namespace backend\modules\recursos_humanos\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\recursos_humanos\models\SectorAsignadoEducador]].
 *
 * @see \backend\modules\recursos_humanos\models\SectorAsignadoEducador
 */
class SectorAsignadoEducadorQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\SectorAsignadoEducador[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\SectorAsignadoEducador|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Devuele el sector(s) de acuerdo al educador
     * @param int $educador_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deEducadorAndPeriodoActivo($educador_id) {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where(['educador_id' => $educador_id, 'periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where('educador_id = :educador_id AND fecha_fin_actividad is not NULL')->addParams([':educador_id' => $educador_id]);
        }
    }

    /**
     * Devuelve el sector validando si el periodo es activo o no
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function dePeriodoActivo() {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('periodo_id = :periodo_id AND fecha_fin_actividad is NULL')
                            ->addParams(['periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where('fecha_fin_actividad is not NULL');
        }
    }

    /**
     * Devuelve el sector si el periodo es activo 
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function andPeriodoActivo() {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('periodo_id = :periodo_id AND fecha_fin_actividad is NULL')
                            ->addParams(['periodo_id' => $periodo->periodo_id, ]);
        } else {
            return $this->where('fecha_fin_actividad is NULL');
        }
    }

    /**
     * Devuele el sector de acuerdo al centro infantil validando el periodo activo
     * @param int $cen_inf_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deCentroInfantil($cen_inf_id, $educador_id) {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('cen_inf_id = :cen_inf_id AND periodo_id = :periodo_id AND fecha_fin_actividad is NULL AND educador_id = :educador_id')
                            ->addParams(['cen_inf_id' => $cen_inf_id, 'periodo_id' => $periodo->periodo_id,  ':educador_id' => $educador_id]);
        } else {
            return $this->where('fecha_fin_actividad is not NULL');
        }
    }

    /**
     * Devuele el sector de acuerdo al centro infantil y fecha fin no es null
     * @param int $cen_inf_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deCibv($cen_inf_id) {
        return $this->where('cen_inf_id = :cen_inf_id AND fecha_fin_actividad is not NULL')
                        ->addParams(['cen_inf_id' => $cen_inf_id]);
    }

    /**
     * Devuele el sector de acuerdo al centro infantil validando el periodo activo
     * @param int $cibv_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deCibvAndPeriodoActivo($cibv_id) {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('cen_inf_id = :cen_inf_id AND periodo_id = :periodo_id AND fecha_fin_actividad is NULL')
                            ->addParams([':cen_inf_id' => $cibv_id, ':periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where('cen_inf_id = :cen_inf_id AND fecha_fin_actividad is not NULL')
                            ->addParams([':cen_inf_id' => $cibv_id]);
        }
    }

    /**
     * Devuele el sector de acuerdo al id validando el periodo activo
     * @param int $sector_asig_educador_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deSectorAndPeriodoActivo($sector_asig_educador_id) {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('sector_asignado_educador_id = :sector_asignado_educador_id AND periodo_id = :periodo_id AND fecha_fin_actividad is NULL')
                            ->addParams([':sector_asignado_educador_id' => $sector_asig_educador_id, ':periodo_id' => $periodo->periodo_id, ]);
        } else {
            return $this->where('sector_asignado_educador_id = :sector_asignado_educador_id AND fecha_fin_actividad is not NULL')
                            ->addParams([':sector_asignado_educador_id' => $sector_asig_educador_id]);
        }
    }

}
