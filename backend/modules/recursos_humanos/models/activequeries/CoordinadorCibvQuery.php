<?php
namespace backend\modules\recursos_humanos\models\activequeries;
use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\recursos_humanos\models\AutoridadesCibv]].
 *
 * @see \backend\modules\recursos_humanos\models\AutoridadesCibv
 */
class CoordinadorCibvQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\AutoridadesCibv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\recursos_humanos\models\AutoridadesCibv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * valida todos coordinadores disponibles para un centro infantil
     * @return \backend\modules\recursos_humanos\models\activequeries\CoordinadorCibvQuery
     */
    public function coordinadoresDisponiblesByPeriodo(){
        $periodo_actual = Periodo::find()->periodoActivo()->one();
        
        $this->join(' left join ', 'autoridades_cibv','coordinador_cibv.coordinador_id = autoridades_cibv.coordinador_id' );
        $this->join(' inner join ', 'periodo','autoridades_cibv.periodo_id = periodo.periodo_id ');
        $this->where('autoridades_cibv.coordinador_id IS NULL')
            ->andWhere('periodo.periodo_id = :periodo_id')
            ->addParams([':periodo_id' => $periodo_actual->periodo_id]);
        return $this;
    }
    

//   SELECT * 
//FROM  `coordinador_cibv` cc
//INNER JOIN autoridades_cibv ac ON cc.coordinador_id != ac.coordinador_id
//INNER JOIN periodo p ON p.periodo_id = ac.periodo_id
//WHERE p.estado =  'ACTIVO'
//LIMIT 0 , 30 
}
