<?php
namespace backend\modules\recursos_humanos\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;

/**
 * EducadorSearch represents the model behind the search form about `backend\modules\recursos_humanos\models\Educador`.
 */
class EducadorSearch extends Educador
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['educador_id'], 'integer'],
                [['educador_dni', 'educador_nombres', 'educador_apellidos', 'educador_telefono',
                'educador_correo', 'nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Educador::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->join('inner join', 'sector_asignado_educador', 'educador.educador_id = sector_asignado_educador.educador_id');

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                'user_id' => Yii::$app->user->id])->one();

        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // 
                $arrayIds = $this->getCoincidenciasCoordinador();
                $query->andWhere(['IN', 'sector_asignado_educador.sector_asignado_educador_id', $arrayIds]);
                break;

            case 'educador': //
                $arrayIds = $this->getCoincidenciasEducador();
                $query->andWhere(['IN', 'educador.educador_id', $arrayIds]);
                break;
            case 'Admin'||'coordinador-gad':
                $arrayIds = $this->getCoincidenciasAdministrador();
                $query->andWhere(['IN', 'educador.educador_id', $arrayIds]);
                break;

            //          case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        if ($authAsiignmentMdel->item_name == 'educador') {
            
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'educador_id' => $this->educador_id,
        ]);

        $query->andFilterWhere(['like', 'educador_dni', $this->educador_dni])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(educador_nombres, " " ,educador_apellidos )'),
                $this->nombre])
//                ->andFilterWhere(['like', 'educador_nombres', $this->educador_nombres])
//                ->andFilterWhere(['like', 'educador_apellidos', $this->educador_apellidos])
            ->andFilterWhere(['like', 'educador_correo', $this->educador_correo])
            ->andFilterWhere(['like', 'educador_telefono', $this->educador_telefono]);

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador()
    {
        $arrayIds = [];
        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->all();
        if (!empty($asignacionCoordinadorCibv)) {
            foreach ($asignacionCoordinadorCibv as $value1) {
                $autoridadCibv = AutoridadesCibv::find()->deAutoridadAndPeriodoActivo($value1->autoridades_cibv_id)->one();
                if (!empty($autoridadCibv)) {
                    $asignacionEducador = SectorAsignadoEducador::find()->deCibvAndPeriodoActivo($autoridadCibv->cen_inf_id)->all();
                    if (!empty($asignacionEducador)) {
                        foreach ($asignacionEducador as $value) {
                            array_push($arrayIds, $value->sector_asignado_educador_id);
                        }
                    }
                }
            }
        }
        return $arrayIds;
    }

    private function getCoincidenciasEducador()
    {
        $arrayIds = [];
        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $asignacionSectorEducador = SectorAsignadoEducador::find()->deSectorAndPeriodoActivo($asignacionEducadorCibv->sector_asignado_educador_id)->one();
            if (!empty($asignacionSectorEducador)) {
                array_push($arrayIds, $asignacionSectorEducador->educador_id);
            }
        }
        return $arrayIds;
    }

    private function getCoincidenciasAdministrador()
    {
        $arrayIds = [];
        //TODO: verificar que fitre con periodo activo?
        $asignacionSectorEducador = SectorAsignadoEducador::find()->all();
        if (!empty($asignacionSectorEducador)) {
            foreach ($asignacionSectorEducador as $key => $value) {
                array_push($arrayIds, $value->educador_id);
            }
        }
        return $arrayIds;
    }
}
