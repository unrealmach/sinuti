<?php

namespace backend\modules\recursos_humanos;

class Recursos_humanos extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\recursos_humanos\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
