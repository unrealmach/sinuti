<?php

namespace backend\modules\recursos_humanos\controllers;

use Yii;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\recursos_humanos\models\SectorasignadoeducadorSearch;
use backend\modules\parametros_sistema\models\Periodo;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectorasignadoeducadorController implements the CRUD actions for SectorAsignadoEducador model.
 */
class SectorasignadoeducadorController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
           'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SectorAsignadoEducador models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectorasignadoeducadorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SectorAsignadoEducador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SectorAsignadoEducador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SectorAsignadoEducador();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sector_asignado_educador_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'periodo' => $periodo,
            ]);
        }
    }

    /**
     * Updates an existing SectorAsignadoEducador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->sector_asignado_educador_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'periodo' => $periodo,
            ]);
        }
    }

    /**
     * Deletes an existing SectorAsignadoEducador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SectorAsignadoEducador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SectorAsignadoEducador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SectorAsignadoEducador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
