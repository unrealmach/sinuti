<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\NutricionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Nutricionistas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutricionista-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Nutricionista'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                        'nutricionista_id',
            'nutricionista_dni',
            'nutricionista_nombres',
            'nutricionista_apellidos',
            'nutricionista_telefono',
            // 'fecha_inicio_actividad',
            // 'fecha_fin_actividad',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
