<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Nutricionista */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Nutricionista',
]) . ' ' . $model->nutricionista_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutricionistas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nutricionista_id, 'url' => ['view', 'id' => $model->nutricionista_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="nutricionista-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
