<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Gastronomo */

$this->title = $model->gastronomo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gastronomos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gastronomo-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->gastronomo_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->gastronomo_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'gastronomo_id',
            'gastronomo_dni',
            'gastronomo_nombres',
            'gastronomo_apellidos',
            'gastronomo_telefono',
            'fecha_inicio_actividad',
            'fecha_fin_actividad',
        ],
        ]) ?>

    </div>
</div>
