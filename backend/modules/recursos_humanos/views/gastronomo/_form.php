<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\Gastronomo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gastronomo-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
                $form->field($model, 'gastronomo_dni', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'gastronomo_nombres', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'gastronomo_apellidos', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'gastronomo_telefono', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'gastronomo_telefono', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'gastronomo_correo', [
                    'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
