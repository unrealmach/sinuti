<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\CoordinadorcibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coordinadores Cibv');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="coordinador-cibv-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Coordinador Cibv'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                        'coordinador_id',
                'coordinador_nombres',
                'coordinador_apellidos',
                'coordinador_DNI',
                'coordinador_contacto',
                // 'coordinador_correo',
                // 'fecha_inicio_actividad',
                // 'fecha_fin_actividad',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        ?>

    </div>
</div>
