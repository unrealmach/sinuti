<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorCibv */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coordinador-cibv-form form-horizontal">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <?=
                $form->field($model, 'coordinador_nombres', [
                    'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
        <?=
                $form->field($model, 'coordinador_apellidos', [
                    'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
        <?=
                $form->field($model, 'coordinador_DNI', [
                    'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'coordinador_contacto', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
        <?=
                $form->field($model, 'coordinador_correo', [
                    'template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
        <?=
                $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => $periodo->fecha_inicio,
                        'endDate' => $periodo->fecha_fin,
                    ],
                    'options' => [
                        'readonly' => true,
                        'disabled' => true,
                    ]
                ])->label(null, ['class' => 'col-sm-3 control-label'])
        ?>
        <?=
                $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-9">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'endDate' => $periodo->fecha_fin,
                    ],
                ])->label(null, ['class' => 'col-sm-3  control-label'])
        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
