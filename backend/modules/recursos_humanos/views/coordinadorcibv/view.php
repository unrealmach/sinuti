<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\CoordinadorCibv */

$this->title = $model->coordinador_nombres . " " . $model->coordinador_apellidos;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coordinador-cibv-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->coordinador_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->coordinador_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                    'coordinador_id',
//            'coordinador_nombres',
//            'coordinador_apellidos',
            'coordinador_DNI',
            'coordinador_contacto',
            'coordinador_correo',
            'fecha_inicio_actividad',
            'fecha_fin_actividad',
        ],
        ]) ?>

    </div>
</div>
