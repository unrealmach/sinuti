<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\EducadorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="educador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'educador_id') ?>

    <?= $form->field($model, 'educador_dni') ?>

    <?= $form->field($model, 'educador_nombres') ?>

    <?= $form->field($model, 'educador_apellidos') ?>

    <?= $form->field($model, 'educador_telefono') ?>

    <?php // echo $form->field($model, 'fecha_inicio_actividad') ?>

    <?php // echo $form->field($model, 'fecha_fin_actividad') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
