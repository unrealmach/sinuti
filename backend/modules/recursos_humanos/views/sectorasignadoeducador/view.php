<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */

$this->title ="Asignación de salón a la/el educador: " . $model->educador->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Educadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sector-asignado-educador-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->sector_asignado_educador_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->sector_asignado_educador_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
//                    'sector_asignado_educador_id',
                'cenInf.cen_inf_nombre',
                'periodo.limitesFechasPeriodo',
//                'educador.nombreCompleto',
                [
                    'label' => 'Salon',
                    'value' => $model->salon->grupoEdad->grupo_edad_descripcion
                ],
            ],
        ])
        ?>

    </div>
</div>
