<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\recursos_humanos\models\Educador;
use backend\modules\inscripcion\models\Salon;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sector-asignado-educador-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cibv::find()->all(), 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
        $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
        $form->field($model, 'educador_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Educador::find()->all(), 'educador_id', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
        $form->field($model, 'salon_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Salon::find()->all(), 'salon_id', 'grupoEdad.grupo_edad_descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>

        <?=
                $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => $periodo->fecha_inicio,
                        'endDate' => $periodo->fecha_fin
                    ]
                ])->label(null, ['class' => 'col-sm-2 control-label'])
        ?>

        <?=
                $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'startDate' => $periodo->fecha_inicio,
                        'endDate' => $periodo->fecha_fin
                    ]
                ])->label(null, ['class' => 'col-sm-2 control-label'])
        ?>

    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
