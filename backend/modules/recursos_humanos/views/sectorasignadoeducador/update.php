<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\SectorAsignadoEducador */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
            'modelClass' => 'Sector Asignado Educador',
        ]) . ' ' . $model->sector_asignado_educador_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sector Asignado Educadors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sector_asignado_educador_id, 'url' => ['view', 'id' => $model->sector_asignado_educador_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sector-asignado-educador-update box box-info">
    <div class="box-header with-border">
        <?php
        $ar = explode(" ", ($this->title));
        $title = "";
        for ($i = 1; $i < count($ar); $i++) {
            $title .= " " . $ar[$i];
        }
        ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?=
    $this->render('_form', [
        'model' => $model,
        'periodo' => $periodo,
    ])
    ?>

</div>
