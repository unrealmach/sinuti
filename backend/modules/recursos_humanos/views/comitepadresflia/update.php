<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitePadresFlia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Comite Padres Flia',
]) . ' ' . $model->comite_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Comite Padres Flias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->comite_id, 'url' => ['view', 'id' => $model->comite_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="comite-padres-flia-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
