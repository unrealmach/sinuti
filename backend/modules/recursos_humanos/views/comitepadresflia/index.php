<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\recursos_humanos\models\ComitepadresfliaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Comites Centrales de Padres de Familia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comite-padres-flia-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Comite Central Padres de Familia'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                'comite_id',
                'comite_nombres',
                'comite_apellidos',
                'comite_dni',
                'comite_numero_contacto',
                 [
                    'label' => 'Comite',
                    'attribute' => 'grupo_comite_id',
                    'value' => 'grupoComite.cenInf.cen_inf_nombre',
                ],
                // 'grupo_comite_id',
                 'cargo',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>
