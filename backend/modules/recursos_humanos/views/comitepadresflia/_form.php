<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\centro_infantil\models\GrupoComite;

/* @var $this yii\web\View */
/* @var $model backend\modules\recursos_humanos\models\ComitePadresFlia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comite-padres-flia-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
                $form->field($model, 'comite_nombres', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'comite_apellidos', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'comite_dni', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'comite_numero_contacto', [
                    'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
        $form->field($model, 'grupo_comite_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(GrupoComite::find()->dePeriodoActivo()->all(), 'grupo_comite_id', 'cenInf.cen_inf_nombre', 'cenInf.cen_inf_direccion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
                $form->field($model, 'cargo', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList(['PRESIDENTE' => 'PRESIDENTE', 'VICEPRESIDENTE' => 'VICEPRESIDENTE', 'TESORERO' => 'TESORERO', 'SECRETARIO' => 'SECRETARIO',], ['prompt' => 'Seleccione ...'])
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
