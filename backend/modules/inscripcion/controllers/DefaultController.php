<?php
namespace backend\modules\inscripcion\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
//        $modl = \backend\modules\rbac\models\AuthAssignment::find();
//        //metodo all trae todos los datos en forma de vector, por lo que las relaciones del activeRecord
//        //se las debe de aplicar a cada
//        $modl2 = $modl->getOnlyCoordinador()->all();
//        $allUsers = [];
//        foreach ($modl2 as $key => $subm) {
//            array_push($allUsers, $subm->user);
//        }
//        $mm = \yii\helpers\ArrayHelper::map($allUsers, 'id', 'first_name');
//        
//        var_dump($mm);
//
//        die();

        return $this->render('index');
    }
}
