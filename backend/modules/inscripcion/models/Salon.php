<?php

namespace backend\modules\inscripcion\models;
use backend\modules\metricas_individuo\models\GrupoEdad;

use Yii;

/**
 * This is the model class for table "salon".
 *
 * @property integer $salon_id
 * @property integer $grupo_edad_id
 * @property integer $salon_capacidad
 * @property integer $salon_nombre
 *
 * @property GrupoEdad $grupoEdad
 */
class Salon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'salon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_edad_id', 'salon_capacidad'], 'required'],
            [['grupo_edad_id'], 'integer'],
            [['salon_capacidad'], 'integer', 'min' => '5'],
            [['salon_nombre'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'salon_id' => Yii::t('app', 'Salon ID'),
            'grupo_edad_id' => Yii::t('app', 'Grupo Edad'),
            'salon_capacidad' => Yii::t('app', 'Capacidad Infantes'),
            'salon_nombre' =>Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoEdad()
    {
        return $this->hasOne(GrupoEdad::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }
}
