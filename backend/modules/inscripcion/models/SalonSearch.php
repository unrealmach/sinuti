<?php

namespace backend\modules\inscripcion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inscripcion\models\Salon;

/**
 * SalonSearch represents the model behind the search form about `backend\modules\inscripcion\models\Salon`.
 */
class SalonSearch extends Salon {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['salon_id', 'grupo_edad_id', 'salon_capacidad', 'salon_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Salon::find();

        $query->joinWith('grupoEdad', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'salon_id' => $this->salon_id,
//            'grupo_edad_id' => $this->grupo_edad_id,
            'salon_capacidad' => $this->salon_capacidad,
            'salon_nombre' => $this->salon_nombre,
        ]);

        $query->andFilterWhere(
                ['like', 'grupo_edad.grupo_edad_descripcion', $this->grupo_edad_id]
        );
        return $dataProvider;
    }

}
