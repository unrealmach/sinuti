<?php

namespace backend\modules\inscripcion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use \backend\modules\rbac\models\AuthAssignment;
use \yii\db\Expression;

/**
 * MatriculaSearch represents the model behind the search form about `backend\modules\inscripcion\models\Matricula`.
 */
class MatriculaSearch extends Matricula
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matricula_id'], 'integer'],
            [['cen_inf_id', 'infante_id', 'periodo_id', 'matricula_estado'],
                'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Matricula::find();

        $query->joinWith('cenInf', true, 'INNER JOIN')->all();
        $query->joinWith('infante', true, 'INNER JOIN')->all();
        $query->joinWith('periodo', true, 'INNER JOIN')->all();
        $query->andWhere("matricula_estado = 'ACTIVO'");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $authAsiignmentMdel = AuthAssignment::find()->getRoleByUser();

        switch ($authAsiignmentMdel->item_name) {
            case 'educador'://
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasEducador();

                $query->andFilterWhere(['IN', 'matricula.infante_id',
                    $arrayIds]);
                break;
            case 'coordinador': // 
                $arrayIds = $this->getCoincidenciasCoordinador();
                $query->andWhere(['IN', 'matricula.cen_inf_id', $arrayIds]);
                break;


            case 'Admin' || 'coordinador-gad':// 
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasAdministrador();

                $query->orFilterWhere(['IN', 'matricula.infante_id',
                    $arrayIds]);
                break;


//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :
                throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        $query->andFilterWhere([
            'matricula_id' => $this->matricula_id,
        ]);


        $query->andFilterWhere(
            ['like', 'cibv.cen_inf_nombre', $this->cen_inf_id])
            ->andFilterWhere(['like', 'infante.infante_dni', $this->infante_id])
            ->orFilterWhere(['like', 'CONCAT(infante.infante_nombres, infante.infante_apellidos)', strtoupper($this->infante_id)])
            ->andFilterWhere(['like', new Expression('CONCAT(periodo.fecha_inicio, " - ", periodo.fecha_fin)'),
                $this->periodo_id])
            ->andFilterWhere(['like', new Expression('(matricula.matricula_estado::TEXT)'), strtoupper($this->matricula_estado)]);
     //   print_r($query->createCommand()->getRawSql());
      //  die();

        return $dataProvider;
    }

    /**
     * Obtiene el id del cibv al cual esta asignado el ccordinador
     * @return array
     */
    private function getCoincidenciasCoordinador()
    {
        $arrayIds = [];
        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->getOneAsignacionUserCoordinadorCibv();

        if (!empty($asignacionCoordinadorCibv)) {
            $cibv_id = AutoridadesCibv::getCibvIdByAutoridad($asignacionCoordinadorCibv);
            if (!empty($cibv_id)) {
                array_push($arrayIds, $cibv_id);
            }
        }
        return $arrayIds;
    }

    /**
     * Obtiene los ids de las matriculas de los infantes asssignados a su centro infantil
     * @return array
     */
    private function getCoincidenciasEducador()
    {
        $arrayIds = [];
        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->getOneEducadorAsignadoActivo();


        if (!empty($asignacionEducadorCibv)) {
            if (!empty($asignacionEducadorCibv->sectorAsignadoEducador)) {
                $listaSectoresAsignadosInfantes = $asignacionEducadorCibv->sectorAsignadoEducador->sectorAsignadoInfantes;
                if (!empty($listaSectoresAsignadosInfantes)) {
                    foreach ($listaSectoresAsignadosInfantes as $sectoresAsignadosInfante) {
                        array_push($arrayIds, $sectoresAsignadosInfante->infante_id);
                    }
                }else{
                    throw new \yii\web\NotFoundHttpException('Los infantes no estan asignados a su salón');
                }
            }
        }

        return empty($arrayIds) ? [0 => 0] : $arrayIds;
    }

    /**
     * Obtiene todos los infantes con matricula activa
     * @return array
     */
    private function getCoincidenciasAdministrador()
    {
        $arrayIds = [];

        $sectorAsignadoInfante = SectorAsignadoInfante::find()
            ->joinWith('sectorAsignado', true, 'INNER JOIN')
            ->innerJoin('periodo','periodo.periodo_id = sector_asignado_educador.periodo_id')
            ->innerJoin('matricula','periodo.periodo_id = matricula.periodo_id')
            ->where("matricula.matricula_estado = 'ACTIVO'")
            ->all();
        if (!empty($sectorAsignadoInfante)) {
            foreach ($sectorAsignadoInfante as $value) {
                $arrayIds[$value->infante_id]=$value->infante_id;

            }
        }


        return empty($arrayIds) ? [0 => 0] : $arrayIds;;
    }
}