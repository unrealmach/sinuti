<?php

namespace backend\modules\inscripcion\models;

use backend\modules\centro_infantil\models\Cibv;
use backend\modules\individuo\models\Infante;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use Yii;

/**
 * This is the model class for table "matricula".
 *
 * @property integer $matricula_id
 * @property integer $cen_inf_id
 * @property integer $infante_id
 * @property integer $periodo_id
 * * @property string $matricula_estado 
 *
 * @property Cibv $cenInf
 * @property Infante $infante
 * @property Periodo $periodo
 */
class Matricula extends \yii\db\ActiveRecord {
    
    public $ESTADO_ACTIVO = 'ACTIVO';
    public $ESTADO_RETIRADO = 'RETIRADO';

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'matricula';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cen_inf_id', 'infante_id', 'periodo_id'], 'required'],
            [['cen_inf_id', 'infante_id', 'periodo_id'], 'integer'],
            [['matricula_estado'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'matricula_id' => Yii::t('app', 'Matrícula'),
            'cen_inf_id' => Yii::t('app', 'Centro Infantil'),
            'infante_id' => Yii::t('app', 'Infante'),
            'periodo_id' => Yii::t('app', 'Período'),
            'matricula_estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenInf() {
        return $this->hasOne(Cibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante() {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodo() {
        return $this->hasOne(Periodo::className(), ['periodo_id' => 'periodo_id']);
    }
    
    /**
     * Devuelve la matricula para el periodo activo
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodoActivo() {
        return $this->hasOne(Periodo::className(), ['periodo_id' => 'periodo_id'])->periodoActivo();
    }
    
    /** 
     * @inheritdoc 
     * @return \backend\modules\inscripcion\models\activequeries\MatriculaQuery the active query used by this AR class.
     */ 
    public static function find() 
    { 
        return new \backend\modules\inscripcion\models\activequeries\MatriculaQuery(get_called_class());
    } 

}
