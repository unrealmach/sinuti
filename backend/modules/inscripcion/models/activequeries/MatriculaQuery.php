<?php
namespace backend\modules\inscripcion\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\inscripcion\models\Matricula]].
 *
 * @see \backend\modules\inscripcion\models\Matricula
 */
class MatriculaQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\inscripcion\models\Matricula[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\inscripcion\models\Matricula|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Busca los infantes de acuerdo al periodo
     * @param int $cibv_id
     * @param int|null $periodo_id puede ser null para indicar que no necesita filtrar por periodo
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return \backend\modules\inscripcion\models\Matricula|array|null
     */
    public function deCentroInfantiIdAndPeriodoId($cibv_id, $periodo_id)
    {
        if (is_null($periodo_id)) {
            $q= $this->where("cen_inf_id = :cibv_id   AND matricula_estado = 'RETIRADO'")
                    ->addParams([':cibv_id' => $cibv_id]);
        } else {
//        'cen_inf_id'=>$centroInfantil_id,'periodo_id'=>$periodoActivo_id
            $q= $this->where("cen_inf_id = :cibv_id AND periodo_id = :periodo_id  AND matricula_estado = 'ACTIVO'")
                    ->addParams([':cibv_id' => $cibv_id, 'periodo_id' => $periodo_id]);
        }

        return $q;
    }

    /**
     * Obtiene las matriculas del centro infantil y del periodo que se requiera
     * @param int $cibv_id
     * @param int $infante_id
     * @param int $periodo_id
     * @author Sofia Mejia <unrealmach@gmail.com>
     * @return \backend\modules\inscripcion\models\activequeries\MatriculaQuery
     */
    public function deCibvAndPeriodo($cibv_id, $infante_id, $periodo_id)
    {
        return $this->where('cen_inf_id = :cibv_id AND infante_id = :infante_id AND periodo_id = :periodo_id AND matricula_estado = matricula_estado ')
                ->addParams([':cibv_id' => $cibv_id, ':infante_id' => $infante_id, 'periodo_id' => $periodo_id]);
    }

    /**
     * Devuelve del periodo actual si es valido
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return \backend\modules\inscripcion\models\activequeries\MatriculaQuery
     */
    public function byPeriodoActual()
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            $this->andWhere('periodo_id=:periodo_id')->addParams([':periodo_id' => $periodo->periodo_id]);
            return $this;
        } else {
            $this->andWhere('periodo_id=:periodo_id')->addParams([':periodo_id' => null]);
        }
    }

    /**
     * Obtiene la matricula con estado activo
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return \backend\modules\inscripcion\models\activequeries\MatriculaQuery
     */
    public function matriculaActiva()
    {
        $this->andWhere("matricula_estado='ACTIVO'");
        return $this;
    }
}
