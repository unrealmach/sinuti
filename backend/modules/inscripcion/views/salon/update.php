<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Salon */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Salon',
]) . ' ' . $model->salon_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->salon_id, 'url' => ['view', 'id' => $model->salon_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="salon-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
