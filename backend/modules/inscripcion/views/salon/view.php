<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Salon */

$this->title = "Salon para " . $model->grupoEdad->grupo_edad_descripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="salon-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->salon_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->salon_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])

            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
//                    'salon_id',
//            'grupoEdad.grupo_edad_descripcion',
                'salon_capacidad',
                'salon_nombre'
            ],
        ])

        ?>

    </div>
</div>
