<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\inscripcion\models\MatriculaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Matriculas');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="matricula-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Matricula'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php \yii\widgets\Pjax::begin() ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                ['matricula_id',
//                   ],
                ['attribute' => 'cen_inf_id',
                    'value' => 'cenInf.cen_inf_nombre'],
                ['attribute' => 'infante_id',
                    'value' => 'infante.cedulaPasaporteAndNombreCompleto'],
//                ['attribute'=>'infante_id',
//                    'value'=> 'infante.nombreCompleto'],
                ['attribute' => 'periodo_id',
                    'value' => 'periodo.limitesFechasPeriodo'],
                'matricula_estado',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        ?>
        <?php \yii\widgets\Pjax::end() ?>

    </div>
</div>
