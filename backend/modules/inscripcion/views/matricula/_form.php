<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\individuo\models\Infante;
use backend\modules\parametros_sistema\models\Periodo;
use dosamigos\switchinput\SwitchBox;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="matricula-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cibv::find()->all(), 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Cibv', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
        <?=
        $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Infante::find()->all(), 'infante_id', 'infante_dni', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
        <?=
        $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Periodo::find()->periodoActivo()->all(), 'periodo_id', 'limitesFechasPeriodo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
        <?=
        $form->field($model, 'matricula_estado', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(SwitchBox::className(), [
            'options' => [
                'label' => false,
            ],
            'checked' => $model->matricula_estado,
            'clientOptions' => [
                'size' => 'large',
                'onColor' => 'success',
                'offColor' => 'info',
                'onText' => Yii::t('app', 'Aprobada'),
                'offText' => Yii::t('app', 'Anulada')
            ]
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
    <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
<?php ActiveForm::end(); ?>
</div>
