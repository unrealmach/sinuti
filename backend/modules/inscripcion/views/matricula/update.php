<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\inscripcion\models\Matricula */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Matricula',
]) . ' ' . $model->matricula_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Matriculas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->matricula_id, 'url' => ['view', 'id' => $model->matricula_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="matricula-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
