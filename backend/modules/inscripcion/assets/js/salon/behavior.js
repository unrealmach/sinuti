/**
 * @author Sofía Mejía
 */

function calcularEdadInfante(infante_id, url) {
    $.ajax({
        url: url,
        dataType: 'json',
        method: 'GET',
        data: {infante_id: infante_id
        },
        success: function(data) {
//            console.log(data);
            showModalWithData("<center>" + data + "</center>");

        }
    });
}

