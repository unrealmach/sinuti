<?php

namespace backend\modules\inscripcion;

class Inscripcion extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\inscripcion\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
