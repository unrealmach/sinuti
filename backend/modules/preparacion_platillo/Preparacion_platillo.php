<?php

namespace backend\modules\preparacion_platillo;

class Preparacion_platillo extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\preparacion_platillo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
