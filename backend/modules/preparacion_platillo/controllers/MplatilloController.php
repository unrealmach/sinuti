<?php
namespace backend\modules\preparacion_platillo\controllers;

use Yii;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\preparacion_platillo\models\MplatilloSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * MplatilloController implements the CRUD actions for MPlatillo model.
 */
class MplatilloController extends Controller
{

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MPlatillo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MplatilloSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MPlatillo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MPlatillo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MPlatillo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_platillo_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MPlatillo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->m_platillo_id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MPlatillo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MPlatillo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MPlatillo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MPlatillo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    

    //    --------------------------- Actions Ajax -------------------------------
    /**
     * obtienen los datos del ingrediente mediante ajax y permite la 
     * creacion de dicho objeto para la clase de sesion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return JSON arreglo con el ingrediente ingresado en session
     */
    public function actionAjaxGetIngrediente()
    {
        $ingrediente_id = \yii\helpers\Json::decode($_GET['idingrediente']);
        $posicion = \yii\helpers\Json::decode($_GET['posicion']);
        $cantidad = \yii\helpers\Json::decode($_GET['cantidad']);
        $sesionDetalle = Yii::$app->session->get('preparacionPlatilloSession');
        $dataArticulo = [ 'd_cantidad_g' => 0];
        if ($sesionDetalle->agregarDetalle($posicion, $ingrediente_id, $cantidad)) {
            $articuloDetalle = $sesionDetalle->getDetallePorPosicion($posicion);
            $dataArticulo = [
                'd_cantidad_g' => $articuloDetalle['d_cantidad_g'],
                'd_composicion' => $articuloDetalle['d_composicion'],
            ];
        };
        return \yii\helpers\Json::encode($dataArticulo);
    }

    /**
     * Se comunica con la sesion para indicar la elminacion de un detalle
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return boolean True si se elimino correctamente el detalle
     */
    public function actionAjaxEliminarDetalle()
    {
        $posicionEliminado = \yii\helpers\Json::decode($_GET['posicionEliminado']);
        $sesionDetalle = Yii::$app->session->get('preparacionPlatilloSession');
        if ($sesionDetalle->eliminarDetalle($posicionEliminado)) {
            return \yii\helpers\Json::encode([true]);
        }
    }

    /**
     * Se comunica con la sesion para indicar que se cambio una cantidad 
     * @author Mauricio Chamorro <unrealmac@hotmail.com>
     * @return boolean True indica que se realizo la operacion
     */
    public function actionAjaxCambiarCantidad()
    {
        $posicion = current(\yii\helpers\Json::decode($_GET['posicion']));
        $ingrediente_id = \yii\helpers\Json::decode($_GET['ingrediente_id']);
        $cantidad = \yii\helpers\Json::decode($_GET['cantidad']);
        $sesionDetalle = Yii::$app->session->get('preparacionPlatilloSession');
        if ($sesionDetalle->cambiarCantidadDetalle($posicion, $ingrediente_id, $cantidad)) {
            $detalleCambiado = $sesionDetalle->getDetallePorPosicion($posicion);
        }
        return \yii\helpers\Json::encode(['true']);
    }

    /**
     * Se comunica con la sesion para generar los datos del maestro
     * partir de los detalles 
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return JSON array con los datos del maestro y higchart Data
     */
    public function actionAjaxGenerarDataMaestro()
    {
        $sesionDetalle = Yii::$app->session->get('preparacionPlatilloSession');
        $sesionDetalle->generarTotalComposicion();
        $dataMaster = $sesionDetalle->getDataMaestro();
        $dataDetail = $sesionDetalle->getOnlyIngredientes();
        $dataHighchart = $sesionDetalle->getCategoriesAndDataToHighchart();
        $dataTotal['masterSesion'] = $dataMaster;
        $dataTotal['dataHighchart'] = $dataHighchart;
        $dataTotal['detailSesion'] = $dataDetail;
        return \yii\helpers\Json::encode($dataTotal);
    }

    /**
     * Permite verificar los ingredientes que se encuentran en la vista
     * y los coloca en la lista de ingredientes seleccionados para que no se 
     * puedan repetir
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return JSON 
     */
    public function actionAjaxIngredienteSeleccionado()
    {
        $arrayItems = \yii\helpers\Json::decode($_GET['data']);
        Yii::$app->session->set('ingredientesSeleccionados', $arrayItems); ///objeto temporal
        return \yii\helpers\Json::encode($arrayItems);
    }

    /**
     * permite la busque por ajax de los ingredientes, ademas de controlar los que
     *  ya se encuentran en el formulario actual contenidos en la sesion
     * Alimenta los datos del Select2 ingrediente_id de cada detalle
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array con los datos para alimentar el select2
     * TODO: poner en modelo, probar en update con el parametro $id falta la implementacion
     */
    public function actionAjaxIngredienteList($q = null, $id = null)
    {
        $q=strtoupper($q);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        $out = ['results' => ['id' => '', 'text' => ''],'otro'=>'']; //siempre debe de existir un id y un text
        $query = new yii\db\Query;
        if (is_null($q)) {
            $query->select(['alimento_id as id',
                'alimento_nombre AS text',
                'alimento_id AS iden'])
                ->from('alimento');
        }
        if (!is_null($q)) {
            $query->select(['alimento_id as id',
                'alimento_nombre AS text',
                'alimento_id AS iden'])
                ->from('alimento')
                ->where("alimento_nombre LIKE '%$q%'");
        }
        if (!is_null(Yii::$app->session->get('ingredientesSeleccionados'))) {
            foreach (Yii::$app->session->get('ingredientesSeleccionados') as $value) {
                if ($value != '') {
                    $query->andWhere("alimento_id <> '$value'");
                }
            }
        }
        $query->limit(100);
        $command = $query->createCommand();

        $data = $command->queryAll();

        $out['results'] = array_values($data);
//        $out = array_values($data);
        return $out;
    }
}
