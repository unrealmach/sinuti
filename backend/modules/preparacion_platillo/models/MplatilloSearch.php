<?php
namespace backend\modules\preparacion_platillo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_platillo\models\MPlatillo;

/**
 * MplatilloSearch represents the model behind the search form about `backend\modules\preparacion_platillo\models\MPlatillo`.
 */
class MplatilloSearch extends MPlatillo
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_platillo_id'], 'integer'],
            [['m_platillo_nombre', 'm_platillo_descripcion', 'tipo_receta_id'], 'safe'],
            [['m_platillo_cantidad_total_g'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MPlatillo::find();

//        $query->joinWith('tipoReceta', true, 'INNER JOIN')->all();
        //$query->joinWith('tipoReceta.tipoPrepCarta', true, 'INNER JOIN')->all();
        $query->joinWith("tipoReceta");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'm_platillo_id' => $this->m_platillo_id,
//            'tipo_receta_id' => $this->tipo_receta_id,
            'm_platillo_cantidad_total_g' => $this->m_platillo_cantidad_total_g,
        ]);

        $query->andFilterWhere(['like', 'UPPER(m_platillo_nombre)', strtoupper($this->m_platillo_nombre)])
            ->andFilterWhere(['like',
                new \yii\db\Expression("UPPER(tipo_receta.tipo_receta_nombre)"), strtoupper($this->tipo_receta_id)])
            ->andFilterWhere(['like', 'UPPER(m_platillo_descripcion)', strtoupper($this->m_platillo_descripcion)]);

//        print_r($query->createCommand()->getRawSql());die();
        return $dataProvider;
    }
}
