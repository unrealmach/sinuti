<?php
namespace backend\modules\preparacion_platillo\models;

use Yii;
use backend\modules\preparacion_platillo\models\MPlatillo;
use backend\modules\preparacion_platillo\models\GrupoPlatillo;
use backend\modules\preparacion_carta\models\TipoPreparacionCarta;

/**
 * This is the model class for table "tipo_receta".
 *
 * @property integer $tipo_receta_id
 * @property integer $grupo_platillo_id
 * @property integer $tipo_prep_carta_id
 * @property string $tipo_receta_nombre
 * @property string $tipo_receta_descripcion
 * @property string $tipo_receta_cantidad_max
 *
 * @property MPlatillo[] $mPlatillos
 * @property GrupoPlatillo $grupoPlatillo
 * @property TipoPreparacionCarta $tipoPrepCarta
 */
class TipoReceta extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipo_receta';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_platillo_id', 'tipo_prep_carta_id', 'tipo_receta_nombre', 'tipo_receta_descripcion', 'tipo_receta_cantidad_max'], 'required'],
            [['grupo_platillo_id', 'tipo_prep_carta_id'], 'integer'],
            [['tipo_receta_cantidad_max'], 'number'],
            [['tipo_receta_nombre'], 'string', 'max' => 45],
            [['tipo_receta_descripcion'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tipo_receta_id' => Yii::t('app', 'Tipo Receta ID'),
            'grupo_platillo_id' => Yii::t('app', 'Grupo Platillo '),
            'tipo_prep_carta_id' => Yii::t('app', 'Equivalente Preparación Carta '),
            'tipo_receta_nombre' => Yii::t('app', 'Receta'),
            'tipo_receta_descripcion' => Yii::t('app', 'Descripcion'),
            'tipo_receta_cantidad_max' => Yii::t('app', 'Cantidad Max [gr]'),
        ];
    }

    
    public function getDescripcionCompleja(){
    return $this->tipoPrepCarta->tipo_prep_carta_nombre." - ".$this->tipo_receta_nombre ;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPlatillos()
    {
        return $this->hasMany(MPlatillo::className(), ['tipo_receta_id' => 'tipo_receta_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoPlatillo()
    {
        return $this->hasOne(GrupoPlatillo::className(), ['grupo_platillo_id' => 'grupo_platillo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoPrepCarta()
    {
        return $this->hasOne(TipoPreparacionCarta::className(), ['tipo_prep_carta_id' => 'tipo_prep_carta_id']);
    }
}
