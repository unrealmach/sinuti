<?php
namespace backend\modules\preparacion_platillo\models;

use backend\modules\composicion_nutricional\models\ComposicionNutricionalPlatillo;
use Yii;

/**
 * This is the model class for table "m_platillo".
 *
 * @property integer $m_platillo_id
 * @property integer $tipo_receta_id
 * @property string $m_platillo_nombre
 * @property string $m_platillo_descripcion
 * @property string $m_platillo_cantidad_total_g
 *
 * @property ComposicionNutricionalPlatillo[] $composicionNutricionalPlatillos
 * @property DPlatillo[] $dPlatillos
 * @property DPreparacionCarta[] $dPreparacionCartas
 * @property TipoReceta $tipoReceta
 */
class MPlatillo extends \yii\db\ActiveRecord
{

    /**
     * Comportamientos para auditoria
     * @author Mauricio Chamorro
     * @return array
     */
    public function behaviors()
    {
        return [
            'AuditTrailBehavior' => [
                'class' => 'bedezign\yii2\audit\AuditTrailBehavior',
                'allowed' => ['tipo_receta_id','m_platillo_nombre'],
                'active' => true,
                'dateFormat' => 'Y-m-d H:i:s',
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm_platillo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_receta_id', 'm_platillo_nombre', 'm_platillo_descripcion', 'm_platillo_cantidad_total_g'], 'required'],
            [['tipo_receta_id'], 'integer'],
            [['m_platillo_nombre', 'm_platillo_descripcion'], 'string'],
            [['m_platillo_cantidad_total_g'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'm_platillo_id' => Yii::t('app', 'M Platillo ID'),
            'tipo_receta_id' => Yii::t('app', 'Tipo de Receta '),
            'm_platillo_nombre' => Yii::t('app', 'Nombre'),
            'm_platillo_descripcion' => Yii::t('app', 'Descripción'),
            'm_platillo_cantidad_total_g' => Yii::t('app', 'Cantidad Total G'),
        ];
    }

    /**
     * Permite obtener los datos necesarios de la composicion nutricional del platillo
     * y formatearlos para ser enviados a highchart
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array 
     */
    public function getDataToHighchart()
    {
        $out = [];
        $out = $this->composicionNutricionalPlatillos[0]->attributes;
        $cantidad = $this->m_platillo_cantidad_total_g;
        //elimina los valores que no necesitan ser presentados
        unset($out['com_nut_platillo_id']);
        unset($out['m_platillo_id']);
        unset($out['es_real']);
        $categories = array_keys($out); //obtiene los nombres para las categorias
        $serie = array_values($out); //obtiene los valores para las series
        $serieout=[];
        // CHO      Cantidad
        // 127      100
        //   x      400 <- cantidad
        //transforma y realiza la regla de 3 para que se representen los valores
        //nutricionales deacuerdo a la cantidad en el menu
        foreach ($serie as $key => $val) {
            $serieout[$key]=  floatval($val)*$cantidad/100;
        }
        return ['categories' => $categories, 'serie' => $serieout];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComposicionNutricionalPlatillos()
    {
        return $this->hasMany(ComposicionNutricionalPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }
    
    public function getComposicionNutricionalPlatillo()
    {
        return $this->hasOne(ComposicionNutricionalPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDPlatillos()
    {
        return $this->hasMany(DPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDPreparacionCartas()
    {
        return $this->hasMany(DPreparacionCarta::className(), ['m_platillo_id' => 'm_platillo_id']);
    }
    
    public function getDPreparacionCarta()
    {
        return $this->hasOne(DPreparacionCarta::className(), ['m_platillo_id' => 'm_platillo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoReceta()
    {
        return $this->hasOne(TipoReceta::className(), ['tipo_receta_id' => 'tipo_receta_id']);
    }
}
