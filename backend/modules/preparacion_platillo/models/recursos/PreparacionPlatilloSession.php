<?php
namespace backend\modules\preparacion_platillo\models\recursos;

/**
 * Clase que permite realizar los calculos de los detalles y maestro
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 */
class PreparacionPlatilloSession
{

    //cantidad de detalles minimos en cada sesion
    const min = 1;

    //objeto que sirve de contenedor de los detalles
    public $detalles = [];
    //objeto que contiene las variables a ser calculadas en el maestro
    public $maestro = ['m_platillo_cantidad_total_g' => 0, 'composicionTotal' => []];

    /**
     * Permite que se use elconstructor de la clase
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Ubicar en sesion con la misma posicion que en la vista
     * @param int $posicion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array Retorna el detalle en dicha posicion
     */
    public function getDetallePorPosicion($posicion)
    {
        return $this->detalles[$posicion];
    }

    /**
     * Obtiene los detalles de la sesion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array Contiene los detalles
     */
    public function getDetalles()
    {
        return $this->detalles;
    }

    /**
     * Obtiene los datos del maestro de la sesion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array Contiene los datos del maestro
     */
    public function getDataMaestro()
    {
        return $this->maestro;
    }

    public function setDataMaestroCantidadTotal($m_platillo_cantidad_total_g)
    {
        $this->maestro['m_platillo_cantidad_total_g'] = $m_platillo_cantidad_total_g;
    }

    /**
     * Permite agregar a los objetos detalles a la sesion - ingrediente(alimento)
     * @param int $posicion la posicion del detalle en la vista
     * @param int $alimento_id el identificador del ingrediente
     * @param double $cantidad_g la cantidad de dicho ingrediente
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return boolean TRUE: se agrego correctamente
     */
    public function agregarDetalle($posicion, $alimento_id, $cantidad_g)
    { //genera un nuevo objeto detalle y lo llena con los datos correspondientes
        $detalle = ['posicion' => $posicion,
            'd_alimento_id' => $alimento_id,
            'd_cantidad_g' => $cantidad_g,
            'd_composicion' => $this->porcentajeComposAlimento($alimento_id, $cantidad_g)
        ];
        //ingresa el detalle en el objeto de sesion
        $this->detalles[$posicion] = $detalle;
        return TRUE;
    }

    /**
     * Elimina un detalle deacuerdo a su posición
     * La posición en la vista es la misma posicion en la sesion por lo que se elimina sin problemas
     * @param int $posicionEliminado
     * @author Mauricio Chamorro <unrealmach@hotmail.com>  
     * @return boolean TRUE: si se elimino caso contrario FALSE
     */
    public function eliminarDetalle($posicionEliminado)
    {
        if (sizeof($this->detalles) > self::min) {
            unset($this->detalles[$posicionEliminado]); // elimina el detalle de dicha posicion
            // reordena los keys 
            // debido a que el maestro detalle en html se comporta de la misma manera
            // es decir dicho articulo que es eliminado pasa a ser reemplazado por el 
            // siguiente si existiese en su posicion
            $this->detalles = array_values($this->detalles);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Permite modificar la cantidad del ingrediente
     * @param int $posicion indica la posiciÃ³n del alimento en el formulario de vista
     * @param int $alimento_id indica el ingrediente a ser modificado
     * @param double $cantidad_g indica la cantidad que tendra dicho ingrediente
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return boolean True: si se realiza la operacion
     */
    public function cambiarCantidadDetalle($posicion, $alimento_id, $cantidad_g)
    {
        if ($this->detalles[$posicion]['d_alimento_id'] === $alimento_id) {
            $temp = $this->porcentajeComposAlimento($alimento_id, $cantidad_g);
            $this->detalles[$posicion]['d_composicion'] = $temp;
            $this->detalles[$posicion]['d_cantidad_g'] = $cantidad_g;
            return TRUE;
        }
    }

    /**
     * Permite calcular la composicion nutricional del alimento de acuerdo a su cantidad
     * mediante una regla de tres, debido a que el sistema tiene todos sus alimentos
     * en 100 gramos para la obtencion de su respectiva composicion
     * @param int $alimento_id identificador del alimento
     * @param double $cantidad_g cantidad de dicho alimento
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array Con la composición de dicho alimento segun su cantidad
     */
    public function porcentajeComposAlimento($alimento_id, $cantidad_g)
    {
        //obtiene el alimento
        $modelAlimento = \backend\modules\nutricion\models\Alimento::findOne(['alimento_id' => $alimento_id]);
        //obtiene la composicion nutricional de dicho alimento
        $composicionAlimento = $modelAlimento->composicionNutricionals[0]->attributes;
        $newComposAlimento = [];
        //realiza la regla de tres
        foreach ($composicionAlimento as $key => $value) {
            if ($key != 'com_nut_id' || $key != 'alimento_id') {
                $aux = round(($value * $cantidad_g) / 100, 2);
                $newComposAlimento[$key] = $aux;
            }
        }
        return $newComposAlimento;
    }

    /**
     * Calcula y genera la suma de los valores nutricionales de cada ingrediente
     * en sesion dando por resultado un total de su composicion, tambien suma
     * la cantidad de cada ingrediente y la guarda en sesion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return boolean TRUE: indica que se realizo el procedimiento
     */
    public function generarTotalComposicion()
    {
        //obtiene los detalles de la sesion
        $detalles = $this->getDetalles();
        $cantidadTotalGramos = 0;

        $containerCantidadesValoresNutricionales = $this->crearContenedorValNutCantidad();
        //suma cada valor nutricional de cada detalle
        foreach ($detalles as $key => $value) {
            $cantidadBromotologicaIngrediente = $value['d_composicion'];
            foreach ($cantidadBromotologicaIngrediente as $key2 => $value2) {
                $containerCantidadesValoresNutricionales[$key2]+=$value2;
            }
            //suma la cantidad de cada ingrediente
            $cantidadTotalGramos+=$value['d_cantidad_g'];
        }
        //elmina estos campos no son valores nutricionales 
        unset($containerCantidadesValoresNutricionales['com_nut_id']);
        unset($containerCantidadesValoresNutricionales['alimento_id']);

        $this->maestro['m_platillo_cantidad_total_g'] = round($cantidadTotalGramos, 2);
        $this->maestro['composicionTotal'] = $containerCantidadesValoresNutricionales;

        return TRUE;
    }

    /**
     * Crea un array asociativo con los valores nutricionales siendo
     * key: valor nutricional, value: cantidad
     * por defecto la cantidad se pone en 0 para que posteriormente
     * se llenen con los valores nutricionales de todos los detalles
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array  
     */
    public function crearContenedorValNutCantidad()
    {
        //procedimiento para obtener los valores nutricionales a partir del modelo
        $temp = \backend\modules\composicion_nutricional\models\ComposicionNutricional::findOne(['com_nut_id' => 1]);
        $listaValoresNutricionales = $temp->attributes;
        $containerCantidadesValoresNutricionales = [];
        //crea un array asociativo con los valores nutricionales siendo key: valor nutricional, value: cantidad
        //por defecto la cantidad se pone en 0 
        foreach ($listaValoresNutricionales as $key => $value) {
            $containerCantidadesValoresNutricionales[$key] = 0;
        }
        return $containerCantidadesValoresNutricionales;
    }

    /**
     * Prepara los datos para que se interpreten en el chart de highchart
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public function getCategoriesAndDataToHighchart()
    {

        $data = [];
        $data['categories'] = array_keys($this->maestro['composicionTotal']);
        $data['data'] = array_values($this->maestro['composicionTotal']);
        return $data;
    }

    /**
     * Genera los datos suficientes para el resumen de la preparacion del platillo
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    public function getOnlyIngredientes()
    {
        $detalles = $this->getDetalles();
        $out = [];
        foreach ($detalles as $detalle) {
            $tempAlimento = \backend\modules\nutricion\models\Alimento::findOne(['alimento_id' => $detalle['d_alimento_id']]);
            array_push($out, [
                'd_alimento' => $tempAlimento->attributes,
                'd_cantidad_g' => $detalle['d_cantidad_g']
                ]
            );
        }

        return $out;
    }
    //-------------------------------------- update MODEL ----------------------------//

    /**
     * Actualiza la sesion deacuerdo a un modelo
     * @param MPlatillo $model 
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function updateSesion($model)
    {
        $d_models = $model->dPlatillos;
        foreach ($d_models as $key => $submodel) {
            $this->agregarDetalle($key, $submodel->alimento_id, $submodel->d_platillo_cantidad_g);
        }
        $this->generarTotalComposicion();
    }
}
