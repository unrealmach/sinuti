<?php

namespace backend\modules\preparacion_platillo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_platillo\models\TipoReceta;

/**
 * TiporecetaSearch represents the model behind the search form about `backend\modules\preparacion_platillo\models\TipoReceta`.
 */
class TiporecetaSearch extends TipoReceta
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo_receta_id', 'grupo_platillo_id', 'tipo_prep_carta_id'], 'integer'],
            [['tipo_receta_nombre', 'tipo_receta_descripcion'], 'safe'],
            [['tipo_receta_cantidad_max'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TipoReceta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tipo_receta_id' => $this->tipo_receta_id,
            'grupo_platillo_id' => $this->grupo_platillo_id,
            'tipo_prep_carta_id' => $this->tipo_prep_carta_id,
            'tipo_receta_cantidad_max' => $this->tipo_receta_cantidad_max,
        ]);

        $query->andFilterWhere(['like', 'tipo_receta_nombre', $this->tipo_receta_nombre])
            ->andFilterWhere(['like', 'tipo_receta_descripcion', $this->tipo_receta_descripcion]);

        return $dataProvider;
    }
}
