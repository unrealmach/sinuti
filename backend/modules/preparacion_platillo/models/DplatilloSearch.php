<?php

namespace backend\modules\preparacion_platillo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_platillo\models\DPlatillo;

/**
 * DplatilloSearch represents the model behind the search form about `backend\modules\preparacion_platillo\models\DPlatillo`.
 */
class DplatilloSearch extends DPlatillo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_platillo_id', 'alimento_id', 'm_platillo_id'], 'integer'],
            [['d_platillo_cantidad_g'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DPlatillo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'd_platillo_id' => $this->d_platillo_id,
            'alimento_id' => $this->alimento_id,
            'm_platillo_id' => $this->m_platillo_id,
        ]);

        $query->andFilterWhere(['like', 'd_platillo_cantidad_g', $this->d_platillo_cantidad_g]);

        return $dataProvider;
    }
}
