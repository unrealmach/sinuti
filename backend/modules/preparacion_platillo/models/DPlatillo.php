<?php

namespace backend\modules\preparacion_platillo\models;
use \backend\modules\nutricion\models\Alimento;

use Yii;

/**
 * This is the model class for table "d_platillo".
 *
 * @property integer $d_platillo_id
 * @property integer $alimento_id
 * @property integer $m_platillo_id
 * @property string $d_platillo_cantidad_g
 *
 * @property Alimento $alimento
 * @property MPlatillo $mPlatillo
 */
class DPlatillo extends \yii\db\ActiveRecord
{
//    public $id;
    //esta variable es con la que trabajaran el select2 dropdown,
    //ya que el alimento_id es la propia variable para guardar en el detalle
    public $alimento_identificador; 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'd_platillo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alimento_id', 'd_platillo_cantidad_g'], 'required'],
            [['alimento_id', 'm_platillo_id'], 'integer'],
            [['d_platillo_cantidad_g'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'd_platillo_id' => Yii::t('app', 'D Platillo ID'),
            'alimento_id' => Yii::t('app', 'Alimento'),
            'm_platillo_id' => Yii::t('app', 'Platillo'),
            'd_platillo_cantidad_g' => Yii::t('app', 'Cantidad Gramos'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['alimento_id' => 'alimento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPlatillo()
    {
        return $this->hasOne(MPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }
}
