<?php

namespace backend\modules\preparacion_platillo\models;

use Yii;

/**
 * This is the model class for table "grupo_platillo".
 *
 * @property integer $grupo_platillo_id
 * @property string $grupo_platillo_nombre
 * @property string $grupo_platillo_descripcion
 *
 * @property TipoReceta[] $tipoRecetas
 */
class GrupoPlatillo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grupo_platillo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_platillo_nombre', 'grupo_platillo_descripcion'], 'required'],
            [['grupo_platillo_nombre'], 'string', 'max' => 45],
            [['grupo_platillo_descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grupo_platillo_id' => Yii::t('app', 'Grupo Platillo ID'),
            'grupo_platillo_nombre' => Yii::t('app', 'Nombre'),
            'grupo_platillo_descripcion' => Yii::t('app', 'Descripcion'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoRecetas()
    {
        return $this->hasMany(TipoReceta::className(), ['grupo_platillo_id' => 'grupo_platillo_id']);
    }
}
