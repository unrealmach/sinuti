<?php

namespace backend\modules\preparacion_platillo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\preparacion_platillo\models\GrupoPlatillo;

/**
 * GrupoplatilloSearch represents the model behind the search form about `backend\modules\preparacion_platillo\models\GrupoPlatillo`.
 */
class GrupoplatilloSearch extends GrupoPlatillo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_platillo_id'], 'integer'],
            [['grupo_platillo_nombre', 'grupo_platillo_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrupoPlatillo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'grupo_platillo_id' => $this->grupo_platillo_id,
        ]);

        $query->andFilterWhere(['like', 'grupo_platillo_nombre', $this->grupo_platillo_nombre])
            ->andFilterWhere(['like', 'grupo_platillo_descripcion', $this->grupo_platillo_descripcion]);

        return $dataProvider;
    }
}
