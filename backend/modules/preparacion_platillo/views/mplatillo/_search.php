<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\MplatilloSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mplatillo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'm_platillo_id') ?>

    <?= $form->field($model, 'tipo_receta_id') ?>

    <?= $form->field($model, 'm_platillo_nombre') ?>

    <?= $form->field($model, 'm_platillo_descripcion') ?>

    <?= $form->field($model, 'm_platillo_cantidad_total_g') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
