<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\preparacion_platillo\models\GrupoplatilloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Grupo Platillos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-platillo-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Grupo Platillo'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                        'grupo_platillo_id',
            'grupo_platillo_nombre',
            'grupo_platillo_descripcion',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
