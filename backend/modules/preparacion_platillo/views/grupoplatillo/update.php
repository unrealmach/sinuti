<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\GrupoPlatillo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Grupo Platillo',
]) . ' ' . $model->grupo_platillo_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Grupo Platillos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->grupo_platillo_id, 'url' => ['view', 'id' => $model->grupo_platillo_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="grupo-platillo-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
