<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\GrupoplatilloSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-platillo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'grupo_platillo_id') ?>

    <?= $form->field($model, 'grupo_platillo_nombre') ?>

    <?= $form->field($model, 'grupo_platillo_descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
