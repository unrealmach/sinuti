<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\DplatilloSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dplatillo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'd_platillo_id') ?>

    <?= $form->field($model, 'alimento_id') ?>

    <?= $form->field($model, 'm_platillo') ?>

    <?= $form->field($model, 'd_platillo_cantidad_g') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
