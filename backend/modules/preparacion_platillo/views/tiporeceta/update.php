<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\TipoReceta */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tipo Receta',
]) . ' ' . $model->tipo_receta_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipo Recetas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tipo_receta_id, 'url' => ['view', 'id' => $model->tipo_receta_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tipo-receta-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
