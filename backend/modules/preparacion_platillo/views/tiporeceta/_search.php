<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\preparacion_platillo\models\TiporecetaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-receta-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tipo_receta_id') ?>

    <?= $form->field($model, 'grupo_platillo_id') ?>

    <?= $form->field($model, 'tipo_prep_carta_id') ?>

    <?= $form->field($model, 'tipo_receta_nombre') ?>

    <?= $form->field($model, 'tipo_receta_descripcion') ?>

    <?php // echo $form->field($model, 'tipo_receta_cantidad_max') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
