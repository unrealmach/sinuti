<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\preparacion_platillo\models\TiporecetaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tipo Recetas');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tipo-receta-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Tipo Receta'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                'tipo_receta_id',
                ['attribute' => 'grupo_platillo_id',
                    'value' => 'grupoPlatillo.grupo_platillo_nombre'],
                ['attribute' => 'tipo_prep_carta_id',
                    'value' => 'tipoPrepCarta.tipo_prep_carta_nombre'],
                'tipo_receta_nombre',
                'tipo_receta_descripcion',
                 'tipo_receta_cantidad_max',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        ?>

    </div>
</div>
