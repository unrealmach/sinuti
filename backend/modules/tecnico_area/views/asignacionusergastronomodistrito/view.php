<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionUserGastronomoDistrito */

$this->title = " Usuario gastrónomo para el distrito: " .  $model->gastronomoDistrito->distrito->codigoDescripcion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Gastronomo Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-gastronomo-distrito-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->asignacion_user_gastronomo_distrito_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->asignacion_user_gastronomo_distrito_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'asignacion_user_gastronomo_distrito_id',
                    [
                    'label' => 'Usuario',
                    'value' => $model->user->username,
                ],
                    [
                    'label' => 'Gastronomo',
                    'value' => $model->gastronomoDistrito->gastronomo->nombreCompleto,
                ],
                'fecha',
                'observaciones:ntext',
            ],
        ])
        ?>

    </div>
</div>
