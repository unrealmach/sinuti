<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\tecnico_area\models\AsignacionusernutricionistadistritoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignacion Usuarios Nutricionistas Distritos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-nutricionista-distrito-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Asignacion Usuario Nutricionista Distrito'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                // 'asignacion_user_nutricionista_distrito_id',
                [
                    'label' => 'Usuario',
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                ],
                    [
                    'label' => 'Nutricionista',
                    'attribute' => 'asignacion_nutricionista_distrito_id',
                    'value' => 'nutricionistaDistrito.nutricionista.nombreCompleto',
                ],
                'fecha',
                'observaciones:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>
