<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\tecnico_area\models\AsignacionUserNutricionistaDistrito */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asignacion User Nutricionista Distrito',
]) . ' ' . $model->asignacion_user_nutricionista_distrito_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Nutricionista Distritos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asignacion_user_nutricionista_distrito_id, 'url' => ['view', 'id' => $model->asignacion_user_nutricionista_distrito_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-user-nutricionista-distrito-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
