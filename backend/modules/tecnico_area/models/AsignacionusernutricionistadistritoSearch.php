<?php

namespace backend\modules\tecnico_area\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tecnico_area\models\AsignacionUserNutricionistaDistrito;
use backend\modules\recursos_humanos\models\Nutricionista;
use webvimark\modules\UserManagement\models\User;

/**
 * AsignacionusernutricionistadistritoSearch represents the model behind the search form about `backend\modules\tecnico_area\models\AsignacionUserNutricionistaDistrito`.
 */
class AsignacionusernutricionistadistritoSearch extends AsignacionUserNutricionistaDistrito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asignacion_user_nutricionista_distrito_id'], 'integer'],
            [['fecha', 'observaciones', 'asignacion_nutricionista_distrito_id', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsignacionUserNutricionistaDistrito::find();
        
        $query->joinWith('user', true, 'INNER JOIN')->all();
        $query->joinWith('nutricionistaDistrito.nutricionista', true, 'INNER JOIN')->all();
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asignacion_user_nutricionista_distrito_id' => $this->asignacion_user_nutricionista_distrito_id,
            //'user_id' => $this->user_id,
            //'asignacion_nutricionista_distrito_id' => $this->asignacion_nutricionista_distrito_id,
            'fecha' => $this->fecha,
        ]);
        
        $query->andFilterWhere(
                ['like', 'user.username', $this->user_id])
                ->andFilterWhere(['like',new \yii\db\Expression( 'CONCAT(nutricionista.nutricionista_nombres, " ", nutricionista.nutricionista_apellidos)'),
                $this->asignacion_nutricionista_distrito_id]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
