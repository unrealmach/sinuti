<?php
namespace backend\modules\tecnico_area\models;
use backend\modules\tecnico_area\models\AsignacionGastronomoDistrito;
use webvimark\modules\UserManagement\models\User;

use Yii;

/**
 * This is the model class for table "asignacion_user_gastronomo_distrito".
 *
 * @property integer $asignacion_user_gastronomo_distrito_id
 * @property integer $user_id
 * @property integer $asignacion_gastronomo_distrito_id
 * @property string $fecha
 * @property string $observaciones
 */
class AsignacionUserGastronomoDistrito extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_user_gastronomo_distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'asignacion_gastronomo_distrito_id', 'fecha', 'observaciones'], 'required'],
            [['user_id', 'asignacion_gastronomo_distrito_id'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asignacion_user_gastronomo_distrito_id' => Yii::t('app', 'Asignacion User Gastronomo Distrito ID'),
            'user_id' => Yii::t('app', 'User'),
            'asignacion_gastronomo_distrito_id' => Yii::t('app', 'Asignación Gastrónomo Distrito'),
            'fecha' => Yii::t('app', 'Fecha'),
            'observaciones' => Yii::t('app', 'Observaciones'),
        ];
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastronomoDistrito()
    {
        return $this->hasOne(AsignacionGastronomoDistrito::className(), ['asignacion_gastronomo_distrito_id' => 'asignacion_gastronomo_distrito_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Agrega un registro de la asignacion de un gastronomo a un distrito 
     * @param int $user_id
     * @param int $asignacion_gastronomo_distrito_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public static function insertRegistro($user_id, $asignacion_gastronomo_distrito_id)
    {
        $asigUsergastronomoDistrito = new AsignacionUserGastronomoDistrito();
        $asigUsergastronomoDistrito->asignacion_gastronomo_distrito_id = $asignacion_gastronomo_distrito_id;
        $asigUsergastronomoDistrito->user_id = $user_id;
        $asigUsergastronomoDistrito->fecha = date('Y-m-d');
        $asigUsergastronomoDistrito->observaciones = "Asignacion gastronomo a un distrito";

        if ($asigUsergastronomoDistrito->save()) {
            Yii::$app->getSession()->setFlash('success', 'Se ha asignado el gastrónomo al distrito');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo asignar el gastrónomo al distrito');
        }
    }
}
