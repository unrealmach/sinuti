<?php

namespace backend\modules\tecnico_area\models;
use backend\modules\recursos_humanos\models\Nutricionista;
use backend\modules\ubicacion\models\Distrito;

use Yii;

/**
 * This is the model class for table "asignacion_nutricionista_distrito".
 *
 * @property integer $asignacion_nutricionista_distrito_id
 * @property integer $nutricionista_id
 * @property integer $distrito_id
 * @property string $fecha_inicio_actividad
 * @property string $fecha_fin_actividad
 *
 * @property Nutricionista $nutricionista
 * @property Distrito $distrito
 */
class AsignacionNutricionistaDistrito extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_nutricionista_distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutricionista_id', 'distrito_id', 'fecha_inicio_actividad'], 'required'],
            [['nutricionista_id', 'distrito_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asignacion_nutricionista_distrito_id' => Yii::t('app', 'Asignacion Nutricionista Distrito ID'),
            'nutricionista_id' => Yii::t('app', 'Nutricionista'),
            'distrito_id' => Yii::t('app', 'Distrito'),
            'fecha_inicio_actividad' => Yii::t('app', 'Fecha Inicio Actividad'),
            'fecha_fin_actividad' => Yii::t('app', 'Fecha Fin Actividad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutricionista()
    {
        return $this->hasOne(Nutricionista::className(), ['nutricionista_id' => 'nutricionista_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrito()
    {
        return $this->hasOne(Distrito::className(), ['distrito_id' => 'distrito_id']);
    }
    
     /**
     * @inheritdoc 
     * @return \backend\modules\tecnico_area\models\activequeries\AsignacionNutricionistaDistritoQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\tecnico_area\models\activequeries\AsignacionNutricionistaDistritoQuery(get_called_class());
    }
    
}
