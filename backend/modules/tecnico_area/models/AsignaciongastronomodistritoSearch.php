<?php

namespace backend\modules\tecnico_area\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tecnico_area\models\AsignacionGastronomoDistrito;

/**
 * AsignaciongastronomodistritoSearch represents the model behind the search form about `backend\modules\tecnico_area\models\AsignacionGastronomoDistrito`.
 */
class AsignaciongastronomodistritoSearch extends AsignacionGastronomoDistrito {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['asignacion_gastronomo_distrito_id'], 'integer'],
            [['gastronomo_id', 'distrito_id', 'fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AsignacionGastronomoDistrito::find();

        $query->joinWith('gastronomo', true, 'INNER JOIN')->all();
        $query->joinWith('distrito', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asignacion_gastronomo_distrito_id' => $this->asignacion_gastronomo_distrito_id,
//            'gastronomo_id' => $this->gastronomo_id,
//            'distrito_id' => $this->distrito_id,
            'fecha_inicio_actividad' => $this->fecha_inicio_actividad,
            'fecha_fin_actividad' => $this->fecha_fin_actividad,
        ]);

        $query->andFilterWhere(
                        ['like', new \yii\db\Expression('CONCAT(distrito.distrito_codigo, " ", distrito.distrito_descripcion)'), $this->distrito_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(gastronomo.gastronomo_nombres, " ", gastronomo.gastronomo_apellidos)'), $this->gastronomo_id]
        );

        return $dataProvider;
    }

}
