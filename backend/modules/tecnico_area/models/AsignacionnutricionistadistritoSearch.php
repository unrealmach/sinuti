<?php

namespace backend\modules\tecnico_area\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito;

/**
 * AsignacionnutricionistadistritoSearch represents the model behind the search form about `backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito`.
 */
class AsignacionnutricionistadistritoSearch extends AsignacionNutricionistaDistrito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asignacion_nutricionista_distrito_id', 'nutricionista_id', 'distrito_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsignacionNutricionistaDistrito::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asignacion_nutricionista_distrito_id' => $this->asignacion_nutricionista_distrito_id,
            'nutricionista_id' => $this->nutricionista_id,
            'distrito_id' => $this->distrito_id,
            'fecha_inicio_actividad' => $this->fecha_inicio_actividad,
            'fecha_fin_actividad' => $this->fecha_fin_actividad,
        ]);

        return $dataProvider;
    }
}
