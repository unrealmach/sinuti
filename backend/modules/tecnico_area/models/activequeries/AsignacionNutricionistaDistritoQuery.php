<?php

namespace backend\modules\tecnico_area\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito]].
 *
 * @see \backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito
 */
class AsignacionNutricionistaDistritoQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    public function deNutricionistaAndPeriodoActivo($nutricionista_id) {
//        $periodo = Periodo::find()->periodoActivo()->one();
//        if (!empty($periodo)) {
        return $this->where(['nutricionista_id' => $nutricionista_id, 'fecha_fin_actividad' => NULL]);
//        }else {
//             return $this->where(['gastronomo_id' => $gastronomo_id, 'periodo_id' => null]);
//        }
    }

    public function dePeriodoActivo() {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('fecha_inicio_actividad BETWEEN :fecha_inicio AND :fecha_fin')
                            ->params([':fecha_inicio' => $periodo->fecha_inicio, ':fecha_fin' => $periodo->fecha_fin]);
        } else {
            return $this->where(['fecha_inicio_actividad' => null]);
        }
    }

}