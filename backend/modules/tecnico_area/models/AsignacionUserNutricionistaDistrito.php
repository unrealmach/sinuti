<?php
namespace backend\modules\tecnico_area\models;
use backend\modules\tecnico_area\models\AsignacionNutricionistaDistrito;
use webvimark\modules\UserManagement\models\User;

use Yii;

/**
 * This is the model class for table "asignacion_user_nutricionista_distrito".
 *
 * @property integer $asignacion_user_nutricionista_distrito_id
 * @property integer $user_id
 * @property integer $asignacion_nutricionista_distrito_id
 * @property string $fecha
 * @property string $observaciones
 */
class AsignacionUserNutricionistaDistrito extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_user_nutricionista_distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'asignacion_nutricionista_distrito_id', 'fecha', 'observaciones'], 'required'],
            [['user_id', 'asignacion_nutricionista_distrito_id'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asignacion_user_nutricionista_distrito_id' => Yii::t('app', 'Asignación User Nutricionista Distrito'),
            'user_id' => Yii::t('app', 'User'),
            'asignacion_nutricionista_distrito_id' => Yii::t('app', 'Asignación Nutricionista Distrito'),
            'fecha' => Yii::t('app', 'Fecha'),
            'observaciones' => Yii::t('app', 'Observaciones'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutricionistaDistrito()
    {
        return $this->hasOne(AsignacionNutricionistaDistrito::className(), ['asignacion_nutricionista_distrito_id' => 'asignacion_nutricionista_distrito_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Agrega un registro de la asignacion de un nutricionista al distrito
     * @param int $user_id
     * @param int $asignacion_nutricionista_distrito_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public static function insertRow($user_id, $asignacion_nutricionista_distrito_id)
    {
        $asigUserNutricionistaDistrito = new AsignacionUserNutricionistaDistrito();
//        $userNutricionista = \webvimark\modules\UserManagement\models\User::findOne(['email' => $this->nutricionista_correo]);
        $asigUserNutricionistaDistrito->asignacion_nutricionista_distrito_id = $asignacion_nutricionista_distrito_id;
        $asigUserNutricionistaDistrito->user_id = $user_id;
        $asigUserNutricionistaDistrito->fecha = date('Y-m-d');
        $asigUserNutricionistaDistrito->observaciones = "Asignacion nutricionsita a un distrito";
        if($asigUserNutricionistaDistrito->save()){
             Yii::$app->getSession()->setFlash('success', 'Se ha asignado el nutricionista al distrito');
        }else{
             Yii::$app->getSession()->setFlash('error', 'Se ha asignado el nutricionista al distrito');
        }
        
    }
}
