<?php

namespace backend\modules\tecnico_area\models;

use backend\modules\recursos_humanos\models\Gastronomo;
use backend\modules\ubicacion\models\Distrito;
use Yii;

/**
 * This is the model class for table "asignacion_gastronomo_distrito".
 *
 * @property integer $asignacion_gastronomo_distrito_id
 * @property integer $gastronomo_id
 * @property integer $distrito_id
 * @property string $fecha_inicio_actividad
 * @property string $fecha_fin_actividad
 *
 * @property Distrito $distrito
 * @property Gastronomo $gastronomo
 */
class AsignacionGastronomoDistrito extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'asignacion_gastronomo_distrito';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['gastronomo_id', 'distrito_id', 'fecha_inicio_actividad'], 'required'],
            [['gastronomo_id', 'distrito_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'asignacion_gastronomo_distrito_id' => Yii::t('app', 'Asignacion Gastr&oacutenomo Distrito'),
            'gastronomo_id' => Yii::t('app', 'Gastrónomo'),
            'distrito_id' => Yii::t('app', 'Distrito'),
            'fecha_inicio_actividad' => Yii::t('app', 'Fecha Inicio Actividad'),
            'fecha_fin_actividad' => Yii::t('app', 'Fecha Fin Actividad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrito() {
        return $this->hasOne(Distrito::className(), ['distrito_id' => 'distrito_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGastronomo() {
        return $this->hasOne(Gastronomo::className(), ['gastronomo_id' => 'gastronomo_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\tecnico_area\models\activequeries\AsignacionGastronomoDistritoQuery the active query used by this AR class.
     */
    public static function find() {
        return new \backend\modules\tecnico_area\models\activequeries\AsignacionGastronomoDistritoQuery(get_called_class());
    }

}
