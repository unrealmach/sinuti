<?php

namespace backend\modules\tecnico_area;

class Tecnico_area extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tecnico_area\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
