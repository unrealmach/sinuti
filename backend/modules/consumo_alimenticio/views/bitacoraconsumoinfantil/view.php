<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */

$this->title = $model->b_cons_inf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bitacora Consumo Infantils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitacora-consumo-infantil-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->b_cons_inf_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->b_cons_inf_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'b_cons_inf_id',
            'asig_inf_c_sem_id',
            'b_cons_inf_porcentaje_consumo',
            'm_prep_carta_id',
            'b_cons_inf_tiempo_comida',
            'b_cons_inf_observaciones',
            'b_cons_inf_fecha_consumo',
        ],
        ]) ?>

    </div>
</div>
