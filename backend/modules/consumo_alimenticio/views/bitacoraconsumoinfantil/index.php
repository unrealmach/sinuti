<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\consumo_alimenticio\models\BitacoraconsumoinfantilSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Bitacora Consumo Infantils');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitacora-consumo-infantil-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Bitacora Consumo Infantil'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                        'b_cons_inf_id',
            'asig_inf_c_sem_id',
            'b_cons_inf_porcentaje_consumo',
            'm_prep_carta_id',
            'b_cons_inf_tiempo_comida',
            // 'b_cons_inf_observaciones',
            // 'b_cons_inf_fecha_consumo',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
