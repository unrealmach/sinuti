<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="bitacora-consumo-infantil-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
            $form->field($model, 'asig_inf_c_sem_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'b_cons_inf_porcentaje_consumo', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'm_prep_carta_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'b_cons_inf_tiempo_comida', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'DESAYUNO' => 'DESAYUNO', 'REFRIGERIO_DE_LA_MAÑANA' => 'REFRIGERIO DE LA MAÑANA', 'ALMUERZO' => 'ALMUERZO', 'REFRIGERIO_DE_LA_TARDE' => 'REFRIGERIO DE LA TARDE',], ['prompt' => 'Seleccione ...'])
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'b_cons_inf_observaciones', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
            <?=
                $form->field($model, 'b_cons_inf_fecha_consumo', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label(null, ['class' => 'col-sm-2 control-label'])

            ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
<?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
<?php ActiveForm::end(); ?>
</div>
