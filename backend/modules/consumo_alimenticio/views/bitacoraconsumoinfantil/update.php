<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Bitacora Consumo Infantil',
]) . ' ' . $model->b_cons_inf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bitacora Consumo Infantils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->b_cons_inf_id, 'url' => ['view', 'id' => $model->b_cons_inf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="bitacora-consumo-infantil-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
