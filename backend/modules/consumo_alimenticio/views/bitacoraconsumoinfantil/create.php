<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil */

$this->title = Yii::t('app', 'Create Bitacora Consumo Infantil');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Bitacora Consumo Infantils'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bitacora-consumo-infantil-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
