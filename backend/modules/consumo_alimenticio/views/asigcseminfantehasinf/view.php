<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante */

$this->title = $model->asig_inf_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Inf Csemanal Has Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-inf-csemanal-has-infante-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'asig_inf_c_sem_id' => $model->asig_inf_c_sem_id, 'infante_id' => $model->infante_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'asig_inf_c_sem_id' => $model->asig_inf_c_sem_id, 'infante_id' => $model->infante_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'asig_inf_c_sem_id',
            'infante_id',
        ],
        ]) ?>

    </div>
</div>
