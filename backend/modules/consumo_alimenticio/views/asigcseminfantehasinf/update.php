<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asignacion Inf Csemanal Has Infante',
]) . ' ' . $model->asig_inf_c_sem_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion Inf Csemanal Has Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asig_inf_c_sem_id, 'url' => ['view', 'asig_inf_c_sem_id' => $model->asig_inf_c_sem_id, 'infante_id' => $model->infante_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-inf-csemanal-has-infante-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
