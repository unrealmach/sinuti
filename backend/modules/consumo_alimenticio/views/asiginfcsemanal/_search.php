<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\consumo_alimenticio\models\AsignacioninfantecsemanalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignacion-infante-csemanal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'asig_inf_c_sem_id') ?>

    <?= $form->field($model, 'infante_id') ?>

    <?= $form->field($model, 'm_carta_semanal_id') ?>

    <?= $form->field($model, 'asi_inf_c_sem_id_observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
