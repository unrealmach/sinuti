<?php

namespace backend\modules\consumo_alimenticio;

class Consumo_alimenticio extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\consumo_alimenticio\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
