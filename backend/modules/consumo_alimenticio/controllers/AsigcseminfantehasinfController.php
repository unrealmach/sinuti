<?php

namespace backend\modules\consumo_alimenticio\controllers;

use Yii;
use backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante;
use backend\modules\consumo_alimenticio\models\AsignacioninfcsemanalhasinfanteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AsigcseminfantehasinfController implements the CRUD actions for AsignacionInfCSemanalHasInfante model.
 */
class AsigcseminfantehasinfController extends Controller {

    public function behaviors() {
        return ['ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AsignacionInfCSemanalHasInfante models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AsignacioninfcsemanalhasinfanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AsignacionInfCSemanalHasInfante model.
     * @param integer $asig_inf_c_sem_id
     * @param integer $infante_id
     * @return mixed
     */
    public function actionView($asig_inf_c_sem_id, $infante_id) {
        return $this->render('view', [
                    'model' => $this->findModel($asig_inf_c_sem_id, $infante_id),
        ]);
    }

    /**
     * Creates a new AsignacionInfCSemanalHasInfante model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AsignacionInfCSemanalHasInfante();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'asig_inf_c_sem_id' => $model->asig_inf_c_sem_id, 'infante_id' => $model->infante_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AsignacionInfCSemanalHasInfante model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $asig_inf_c_sem_id
     * @param integer $infante_id
     * @return mixed
     */
    public function actionUpdate($asig_inf_c_sem_id, $infante_id) {
        $model = $this->findModel($asig_inf_c_sem_id, $infante_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'asig_inf_c_sem_id' => $model->asig_inf_c_sem_id, 'infante_id' => $model->infante_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AsignacionInfCSemanalHasInfante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $asig_inf_c_sem_id
     * @param integer $infante_id
     * @return mixed
     */
    public function actionDelete($asig_inf_c_sem_id, $infante_id) {
        $this->findModel($asig_inf_c_sem_id, $infante_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AsignacionInfCSemanalHasInfante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $asig_inf_c_sem_id
     * @param integer $infante_id
     * @return AsignacionInfCSemanalHasInfante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($asig_inf_c_sem_id, $infante_id) {
        if (($model = AsignacionInfCSemanalHasInfante::findOne(['asig_inf_c_sem_id' => $asig_inf_c_sem_id, 'infante_id' => $infante_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Devuelve la cantidad de infantes asignados a una carta semanal
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return int con el  numero de infantes
     */
    public function actionAjaxGetNumInfantes() {
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $carta_semanal_id = $_GET['carta_semanal_id'];
        $model = new AsignacionInfCSemanalHasInfante();
        $data_out=$model->findInfantesByCartaSemanalId($carta_semanal_id);
        $temp = !is_null($data_out)?count($data_out):0;
        return $temp;
    }

}
