<?php
namespace backend\modules\consumo_alimenticio\controllers;

use Yii;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AsiginfcsemanalController implements the CRUD actions for AsignacionInfanteCSemanal model.
 */
class AsiginfcsemanalController extends Controller
{

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AsignacionInfanteCSemanal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AsignacionInfanteCSemanalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single AsignacionInfanteCSemanal model.
     * @param integer $id
     * @return mixed
     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

    /**
     * Creates a new AsignacionInfanteCSemanal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AsignacionInfanteCSemanal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing AsignacionInfanteCSemanal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->asig_inf_c_sem_id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing AsignacionInfanteCSemanal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AsignacionInfanteCSemanal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AsignacionInfanteCSemanal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AsignacionInfanteCSemanal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Trae una lista de infantes que esten disponibles para ser asignados
     * a una carta semanal, deacuerdo aun id de salon
     * @param string $q nombres o apellidos del infante
     * @param int $id es el id del salon
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return Json
     */
    public function actionAjaxInfanteList($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query = \Yii::$app->db;
        $lista_infantes = Yii::$app->session->get('lista_infantes');
        $stringIn = '';
        foreach ($lista_infantes as $key => $infante_id) {
            $stringIn.=" $infante_id, ";
        }
        $lista = trim($stringIn, ', ');
        if (!is_null($q)) {
            $outTemp = $query->createCommand("
               SELECT sector_asignado_infante.infante_id  as id , ".
               new \yii\db\Expression("CONCAT( infante.infante_nombres,  ' ', infante.infante_apellidos )AS text").",
               sector_asignado_infante.infante_id  as iden 
               FROM sector_asignado_infante 
                inner join sector_asignado_educador 
                on sector_asignado_infante.sector_asignado_id = sector_asignado_educador.sector_asignado_educador_id
                inner join infante
                on infante.infante_id =sector_asignado_infante.infante_id
                inner join periodo
                on periodo.periodo_id = sector_asignado_educador.periodo_id
                inner join matricula
                on matricula.infante_id = infante.infante_id
                left join  salon 
                on sector_asignado_educador.salon_id= salon.salon_id
                where salon.salon_id = $id and periodo.estado = 'ACTIVO' and matricula.matricula_estado = 'ACTIVO' and
                ".new \yii\db\Expression(" CONCAT( infante.infante_nombres,  ' ', infante.infante_apellidos )")." like '%$q%'
                and   sector_asignado_infante.infante_id IN ( $lista )")->queryAll();
        } else {
            $outTemp = $query->createCommand("
               SELECT sector_asignado_infante.infante_id  as id , 
               ".new \yii\db\Expression("CONCAT( infante.infante_nombres,  ' ', infante.infante_apellidos )AS text").",
               sector_asignado_infante.infante_id  as iden 
               FROM sector_asignado_infante 
                inner join sector_asignado_educador 
                on sector_asignado_infante.sector_asignado_id = sector_asignado_educador.sector_asignado_educador_id
                inner join infante
                on infante.infante_id =sector_asignado_infante.infante_id
                inner join periodo
                on periodo.periodo_id = sector_asignado_educador.periodo_id
                inner join matricula
                on matricula.infante_id = infante.infante_id
                left join  salon 
                on sector_asignado_educador.salon_id= salon.salon_id
                where salon.salon_id = $id and periodo.estado = 'ACTIVO' and matricula.matricula_estado = 'ACTIVO'
                and   sector_asignado_infante.infante_id IN ( $lista )")->queryAll();
        }
        $out['results'] = array_values($outTemp);
        return $out;
    }
}
