<?php

namespace backend\modules\consumo_alimenticio\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "bitacora_consumo_infantil".
 *
 * @property integer $b_cons_inf_id
 * @property integer $asig_inf_c_sem_id
 * @property string $b_cons_inf_porcentaje_consumo
 * @property integer $m_prep_carta_id
 * @property string $b_cons_inf_tiempo_comida
 * @property string $b_cons_inf_observaciones
 * @property string $b_cons_inf_fecha_consumo
 *
 * @property AsignacionInfanteCSemanal $asigInfCSem
 */
class BitacoraConsumoInfantil extends \yii\db\ActiveRecord
{

    use traits\BitacoraConsumoTrait;

//    public $infante_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bitacora_consumo_infantil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asig_inf_c_sem_id', 'b_cons_inf_porcentaje_consumo', 'm_prep_carta_id', 'b_cons_inf_tiempo_comida', 'b_cons_inf_observaciones', 'b_cons_inf_fecha_consumo'], 'required'],
            [['asig_inf_c_sem_id', 'm_prep_carta_id'], 'integer'],
            [['b_cons_inf_porcentaje_consumo'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
//            [['b_cons_inf_porcentaje_consumo'], 'number'],
            [['b_cons_inf_tiempo_comida', 'b_cons_inf_porcentaje_consumo'], 'string'],
//            [['b_cons_inf_fecha_consumo'], 'safe'],
            [['b_cons_inf_observaciones'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'b_cons_inf_id' => Yii::t('app', 'B Cons Inf ID'),
            'asig_inf_c_sem_id' => Yii::t('app', 'ID asignación'),
            'b_cons_inf_porcentaje_consumo' => Yii::t('app', 'Porcentaje de Consumo'),
            'm_prep_carta_id' => Yii::t('app', 'Menú'),
            'b_cons_inf_tiempo_comida' => Yii::t('app', 'Tiempo de Comida'),
            'b_cons_inf_observaciones' => Yii::t('app', 'Observaciones'),
            'b_cons_inf_fecha_consumo' => Yii::t('app', 'Fecha de Consumo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsigInfCSem()
    {
        return $this->hasOne(AsignacionInfanteCSemanal::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\consumo_alimenticio\models\activequeries\BitacoraConsumoInfantilQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\consumo_alimenticio\models\activequeries\BitacoraConsumoInfantilQuery(get_called_class());
    }

    public function getMPrepCarta()
    {
        return $this->hasOne(\backend\modules\preparacion_carta\models\MPrepCarta::className(), ['m_prep_carta_id' => 'm_prep_carta_id']);
    }

    /**
     * Llena registros de bitacora para todos los infantes de un sector asignado con la carta
     * semanal que se aprobo para la institucion, a traves el id del usuario educador
     * @param int $user_id id del usuario de rol Eduador
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    public function llenarBitacora($user_id)
    {
        //buscar el sector asignado del educador
        $asignacionEducadorCibv = \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo($user_id)
            ->one();
//        var_dump($asignacionEducadorCibv);
//        die();

        //buscar los infantes  de ese sector
        if (!is_null($asignacionEducadorCibv)) {
            $lista_infantes = \backend\modules\centro_infantil\models\SectorAsignadoInfante::find()
                ->infanteEducadorAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
        } else {
            throw new NotFoundHttpException('Operación no válida - No existe un período activo');
        }


        //valida que exista infantes para el salon
        if (is_null($lista_infantes) || empty($lista_infantes)) {
            throw new NotFoundHttpException('El sector asignado (salón) no tiene infantes');
        }
        //saca los ids de los infantes del sctor asignado en una lista
        $listInf = [];
        foreach ($lista_infantes as $key => $SectorAsignadoInfante) {
            array_push($listInf, $SectorAsignadoInfante->infante_id);
        }
        //busca los registros de la asignacion de c semanal para esos infantes

        $asignacionesValidas = AsignacionInfCSemanalHasInfante::findAllInfantesToAsignacionCompareTo($listInf);
        if (is_null($asignacionesValidas) || empty($asignacionesValidas)) {
            throw new NotFoundHttpException('Todos los infantes asignados a una carta semanal ya fueron registrados');
        }
        $listaBitacora = $this->prepararDatosParaConsumo($asignacionesValidas);

        //prepara los datos para ser ingresados en un batch
        $arrayDataBatch = [];
        foreach ($listaBitacora as $key => $value) {
            unset($value['carta_semanal_id']);
            array_push($arrayDataBatch, array_values($value));
        }

        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {

            $flag = $this->doTransaccionLLenarBitacora($arrayDataBatch, $db);
            $transaction2 = $db->beginTransaction();

            try {

                if ($flag) {
                    $flag = $flag && $this->doTransaccionUpdateJunctionTable($asignacionesValidas, $db);
                }

                if ($flag == FALSE) {
                    $transaction2->rollBack();
                    $transaction->rollBack();
                } else {
                    $transaction->commit();
                    $transaction2->commit();
                }


            } catch (\Exception $exc) {
                $transaction->rollBack();
                //  throw new NotFoundHttpException('Los infantes no tienen asignado un plan de alimentación semanal');
                throw  new NotFoundHttpException($exc->getMessage() . " " . $exc->getCode());
            }


        } catch (\Exception $exc) {
            $transaction->rollBack();
            //  throw new NotFoundHttpException('Los infantes no tienen asignado un plan de alimentación semanal');
            throw  new NotFoundHttpException($exc->getMessage() . " " . $exc->getCode());
        }

    }

    /**
     * Genera las filas necesarias para la tabla de consumo , las que posteriormente
     * seran insertadas en un batch
     * @param array $asignacionesValidas
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    private function prepararDatosParaConsumo($asignacionesValidas)
    {
        //llena los datos para la bitacora, deacuerdo a la lista de asignaciones
        $arrayDataAsignacion = [];
        foreach ($asignacionesValidas as $key => $objAsignacion) {
            $AsignacionInfCSemanalHasInfante = AsignacionInfCSemanalHasInfante::findOne($objAsignacion['asignacion_junction_id']);
            $temp = [
                'asig_inf_c_sem_id' => $AsignacionInfCSemanalHasInfante['asig_inf_c_sem_id'],
                'carta_semanal_id' => $AsignacionInfCSemanalHasInfante->asigInfCSem->m_carta_semanal_id,
                'b_cons_inf_porcentaje_consumo' => 1,
                'b_cons_inf_observaciones' => "Ninguna",
            ];
            array_push($arrayDataAsignacion, $temp);
        }
        //llena los datos de la carta para cada fila de bitacora anterior
        //busca los cartas
        $listaBitacora = [];
        $modelCartaSemanal = new \backend\modules\carta_semanal\models\MCartaSemanal();
        foreach ($arrayDataAsignacion as $key => $datos) {
            $cartaSemanalTemp = $modelCartaSemanal->find()->where(['m_c_sem_id' => $datos['carta_semanal_id']])->one();
            $finicioCartaSemanal = Yii::$app->Util->transformDateTimeToDate($cartaSemanalTemp->m_c_sem_fecha_inicio);
            foreach ($cartaSemanalTemp->dCartaSemanals as $ky2 => $dcartasemanal) {
                $tempCarta['m_prep_carta_id'] = $dcartasemanal->m_prep_carta_id;
                $tempCarta['b_cons_inf_tiempo_comida'] = $dcartasemanal->mPrepCarta->tiempoComida->tiem_com_nombre;
                $tempCarta['b_cons_inf_fecha_consumo'] = date('Y-m-d', strtotime("$finicioCartaSemanal + " . Yii::$app->Util->getNumberOfDaysToAddMonday($dcartasemanal->d_c_sem_dia_semana) . " day"));
                $tempAsignacion = $arrayDataAsignacion[$key];
                //completa una fila de BitacoraConsumoINfantil table
                array_push($listaBitacora, array_merge_recursive($tempCarta, $tempAsignacion));
            }
        }
        unset($arrayDataAsignacion);
        return $listaBitacora;
    }
//    

    /**
     * LLena los datos de la bitacora de consumo en una transaccion a partir de
     * conjunto de datos dispuestos para que se usen en un batch
     * @param array $arrayDataBatch datos
     * @throws NotFoundHttpException
     * @throws \backend\modules\consumo_alimenticio\models\Exception
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    private function doTransaccionLLenarBitacora($arrayDataBatch, $db)
    {
        //Ingresa los datos en la bdd con una transaccion
        $status = NULL;
        $status = $db->createCommand()->batchInsert(
            'bitacora_consumo_infantil', ['m_prep_carta_id',
            'b_cons_inf_tiempo_comida',
            'b_cons_inf_fecha_consumo',
            'asig_inf_c_sem_id',
            'b_cons_inf_porcentaje_consumo',
            'b_cons_inf_observaciones'], $arrayDataBatch
        )->execute();


        return empty($status) ? FALSE : TRUE;
    }

    /**
     * Realiza un update al campo fue_consumido de la tabla de juntura infante-cartasemanal
     * a partir de una lista de asignaciones validas
     * @param array $asignacionesValidas lista de asignaciones validas
     * @throws NotFoundHttpException
     * @throws \backend\modules\consumo_alimenticio\models\Exception
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     */
    private function doTransaccionUpdateJunctionTable($asignacionesValidas, $db)
    {

        $result = NULL;
        $dataWhen = ' ';
        $dataIn = ' ';
        foreach ($asignacionesValidas as $key => $asignacion) {
            $dataWhen .= " WHEN " . $asignacion['asignacion_junction_id'] . " THEN TRUE ";
            $dataIn .= $asignacion['asignacion_junction_id'] . ",";
        }

        $dataIn = trim($dataIn, ',');
        $result = $db->createCommand("UPDATE asignacion_inf_c_semanal_has_infante"
            . " SET fue_consumido = (CASE asignacion_junction_id  $dataWhen END) WHERE asignacion_junction_id  "
            . " IN($dataIn)")->execute();

        return empty($result) ? FALSE : TRUE;

    }
}
