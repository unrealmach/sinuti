<?php

namespace backend\modules\consumo_alimenticio\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante;

/**
 * AsignacioninfcsemanalhasinfanteSearch represents the model behind the search form about `backend\modules\consumo_alimenticio\models\AsignacionInfCSemanalHasInfante`.
 */
class AsignacioninfcsemanalhasinfanteSearch extends AsignacionInfCSemanalHasInfante
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asig_inf_c_sem_id', 'infante_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsignacionInfCSemanalHasInfante::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asig_inf_c_sem_id' => $this->asig_inf_c_sem_id,
            'infante_id' => $this->infante_id,
        ]);

        return $dataProvider;
    }
}
