<?php

namespace backend\modules\consumo_alimenticio\models;

use backend\modules\parametros_sistema\models\Periodo;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use \yii\db\Expression;
use \backend\modules\rbac\models\AuthAssignment;
use \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use \backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\inscripcion\models\Matricula;
use yii\web\NotFoundHttpException;

/**
 * AsignacioninfantecsemanalSearch represents the model behind the search form about `backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal`.
 */
class AsignacionInfanteCSemanalSearch extends AsignacionInfanteCSemanal
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asig_inf_c_sem_id'], 'integer'],
            [['asi_inf_c_sem_id_observaciones', 'infante_id', 'm_carta_semanal_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AsignacionInfanteCSemanal::find();
        $query->join('inner join', 'matricula',
            'asignacion_infante_c_semanal.infante_id = matricula.infante_id');
        $query->joinWith('infante');
        $query->joinWith('mCartaSemanal');

        $query->where("matricula.matricula_estado = 'ACTIVO' ");

        $arrayIds = [];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $authAsiignmentMdel = AuthAssignment::find()->getRoleByUser();

        $periodo = Periodo::find()->periodoActivo()->one();

        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador':

                $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->getOneAsignacionUserCoordinadorCibv();

                $autoridadCibv = !empty($asignacionCoordinadorCibv) ? $asignacionCoordinadorCibv->autoridadCibv : null;


                $matriculasCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id,
                    $periodo->periodo_id)->all();

               // var_dump($matriculasCibv); die();
                if (!empty($asignacionCoordinadorCibv) && !empty($matriculasCibv)) {


                    foreach ($matriculasCibv as $matricula) {

                        $infantesByAsigCS = AsignacionInfanteCSemanal::find()->AsignacionByInfante($matricula->infante_id)->all();
                        if (!empty($infantesByAsigCS )) {
                            foreach ($infantesByAsigCS as $value2) {
                                array_push($arrayIds, $value2->asig_inf_c_sem_id);

                            }
                        }

                    }
                }

                break;
            case 'educador':
                throw new NotFoundHttpException('No tiene permisos para esto');
                break;

            default: breaK;
        }


        if ($authAsiignmentMdel->item_name == 'coordinador' || $authAsiignmentMdel->item_name == 'educador') {
            $query->andFilterWhere(['IN', 'asignacion_infante_c_semanal.asig_inf_c_sem_id',
                empty($arrayIds)?0:$arrayIds]);
        }


        $query->andFilterWhere(['like',
            new Expression("CONCAT(to_date(m_carta_semanal.m_c_sem_fecha_inicio::TEXT,'YYYY-MM-DD')::TEXT,' - ', to_date(m_carta_semanal.m_c_sem_fecha_fin::text,'YYYY-MM-DD')::TEXT)"), $this->m_carta_semanal_id,]);


        $query->andFilterWhere(['like', "CONCAT(infante.infante_nombres,' ', infante.infante_apellidos)", strtoupper($this->infante_id)]);

        $query->andFilterWhere(['like', 'asi_inf_c_sem_id_observaciones', $this->asi_inf_c_sem_id_observaciones]);
      //  print_r($query->createCommand()->getRawSql());        die();


        $dataProvider->pagination = [
            'pageSize' => 20,
        ];

        return $dataProvider;
    }
}