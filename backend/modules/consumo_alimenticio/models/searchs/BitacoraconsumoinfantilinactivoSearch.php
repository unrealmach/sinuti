<?php

namespace backend\modules\consumo_alimenticio\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil;
use backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal;
use \backend\modules\recursos_humanos\models\AutoridadesCibv;
use \backend\modules\inscripcion\models\Matricula;
use \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use \backend\modules\centro_infantil\models\SectorAsignadoInfante;
use \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;

/**
 * BitacoraconsumoinfantilSearch represents the model behind the search form about `backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil`.
 */
class BitacoraconsumoinfantilinactivoSearch extends BitacoraConsumoInfantil
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['b_cons_inf_id'], 'integer'],
                [['b_cons_inf_porcentaje_consumo'], 'number'],
                [['b_cons_inf_tiempo_comida', 'b_cons_inf_observaciones', 'b_cons_inf_fecha_consumo'],
                'safe'],
                [['asig_inf_c_sem_id', 'm_prep_carta_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = BitacoraConsumoInfantil::find();

        $query->joinWith('asigInfCSem.infante.matricula', true, 'INNER JOIN')->all();
        $query->joinWith('mPrepCarta', true, 'INNER JOIN')->all();

        $query->where("matricula.matricula_estado = 'RETIRADO' ");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                'user_id' => Yii::$app->user->id])->one();
        $periodo            = \backend\modules\parametros_sistema\models\Periodo::find()->periodoActivo()->one();

        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // si el rol es coordinador mustra las bitacoras que pertenecen al centro infantil del coordinador
                $arrayIds = $this->getCoincidenciasCoordinador($periodo);
                $query->andWhere(['IN', 'bitacora_consumo_infantil.b_cons_inf_id',
                    $arrayIds]);
                break;
            case 'educador':// si el rol es ducador mustra todas las bitacoras que pertenscan a los infantes del salon del educador
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasEducador($periodo);
                $query->andWhere(['IN', 'bitacora_consumo_infantil.b_cons_inf_id',
                    $arrayIds]);
                break;
            case 'Admin'||'coordinador-gad':// si el rol es ducador mustra todas las bitacoras que pertenscan a los infantes del salon del educador
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasAdministrador($periodo);
                $query->andWhere(['IN', 'bitacora_consumo_infantil.b_cons_inf_id',
                    $arrayIds]);
                break;
//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :die("dasd");throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        $query->andFilterWhere([
            'b_cons_inf_id' => $this->b_cons_inf_id,
            'b_cons_inf_fecha_consumo' => $this->b_cons_inf_fecha_consumo,
        ]);

        $query->andFilterWhere(['like', new \yii\db\Expression('(b_cons_inf_tiempo_comida::TEXT)'), $this->b_cons_inf_tiempo_comida])
            ->andFilterWhere(['like', 'b_cons_inf_observaciones', $this->b_cons_inf_observaciones])
            ->andFilterWhere(['=', 'b_cons_inf_porcentaje_consumo', $this->b_cons_inf_porcentaje_consumo])
            ->andFilterWhere(['like', 'm_prep_carta.m_prep_carta_nombre', $this->m_prep_carta_id])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " " , infante.infante_apellidos)'),
                $this->asig_inf_c_sem_id,])
        ;
        $query->orderBy(['b_cons_inf_fecha_consumo' => SORT_DESC]);

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador($periodo)
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv  = AutoridadesCibv::findOne([
                    'autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id,
                    null)->all();
            if (!empty($infantesbyCibv)) {
                //coge los infantes matriculados en ese periodo
                foreach ($infantesbyCibv as $value) {
                    //busca las asignaciones de la carta semanal con el infante
                    $infantesByAsigCS = AsignacionInfanteCSemanal::find()->AsignacionByInfante($value->infante_id)->all();
                    if (!empty($infantesByAsigCS)) {
                        foreach ($infantesByAsigCS as $value2) {
                            $bitacorasByAsiCS = BitacoraConsumoInfantil::find()->bitacoraByAsignacionCS($value2->asig_inf_c_sem_id)->all();
                            if (!empty($bitacorasByAsiCS)) {
                                foreach ($bitacorasByAsiCS as $value3) {
                                    array_push($arrayIds, $value3->b_cons_inf_id);
                                }
                            }
                        }
                    }
                }
            }
        }

        return $arrayIds;
    }

    private function getCoincidenciasEducador($periodo)
    {
        $arrayIds               = [];
        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();

        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    $infantesByAsigCS = AsignacionInfanteCSemanal::find()->AsignacionByInfante($value->infante_id)->all();
                    if (!empty($infantesByAsigCS)) {
                        foreach ($infantesByAsigCS as $value2) {
                            $bitacorasByAsiCS = BitacoraConsumoInfantil::find()->bitacoraByAsignacionCS($value2->asig_inf_c_sem_id)->all();
                            if (!empty($bitacorasByAsiCS)) {
                                foreach ($bitacorasByAsiCS as $value3) {
                                    array_push($arrayIds, $value3->b_cons_inf_id);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $arrayIds;
    }
    
    private function getCoincidenciasAdministrador($periodo)
    {
        $arrayIds = [];

        $bitacorasByAsiCS = BitacoraConsumoInfantil::find()->all();
        if (!empty($bitacorasByAsiCS)) {
            foreach ($bitacorasByAsiCS as $value3) {
                array_push($arrayIds, $value3->b_cons_inf_id);
            }
        }
        return $arrayIds;
    }
}