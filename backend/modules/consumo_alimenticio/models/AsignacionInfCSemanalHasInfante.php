<?php
namespace backend\modules\consumo_alimenticio\models;

use Yii;
use backend\modules\individuo\models\Infante;

/**
 * This is the model class for table "asignacion_inf_c_semanal_has_infante".
 *
 * @property integer $asignacion_junction_id
 * @property integer $asig_inf_c_sem_id
 * @property integer $infante_id
 * @property integer $anio
 * @property integer $num_semana
 * @property integer $fue_consumido
 *
 * @property AsignacionInfanteCSemanal $asigInfCSem
 * @property Infante $infante
 */
class AsignacionInfCSemanalHasInfante extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_inf_c_semanal_has_infante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asig_inf_c_sem_id', 'infante_id'], 'required'],
            [['asig_inf_c_sem_id', 'infante_id', 'anio','num_semana','fue_consumido'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asig_inf_c_sem_id' => Yii::t('app', 'Asig Inf C Sem ID'),
            'infante_id' => Yii::t('app', 'Infante ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsigInfCSem()
    {
        return $this->hasOne(AsignacionInfanteCSemanal::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * Trae todos los infantes asignados a la carta semanal en la semana actual
     * @param int $carta_semanal_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return query
     */
    public static function findInfantesByCartaSemanalId($carta_semanal_id)
    {
        return static::find()
                ->join('JOIN', 'asignacion_infante_c_semanal', " asignacion_infante_c_semanal.asig_inf_c_sem_id=asignacion_inf_c_semanal_has_infante.asig_inf_c_sem_id")
                ->where(['m_carta_semanal_id' => $carta_semanal_id])
                ->andWhere('asignacion_inf_c_semanal_has_infante.anio= extract(year from now())::varchar ')
                ->andWhere(' asignacion_inf_c_semanal_has_infante.num_semana= extract( week from now())::varchar ')
                 ->all();
    }

    /**
     * Trae el registro relacionado con la asignacion de la carta semanal al infante
     * valida que este en la semana actual
     * @param int $asignacion_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return AsignacionInfCSemanalHasInfante
     */
    public static function findIdAsignacionSemanaActualByCartaSemanal($asignacion_id)
    {
        return static::find()
//                ->join('JOIN', 'asignacion_infante_c_semanal', " asignacion_infante_c_semanal.asig_inf_c_sem_id=asignacion_inf_c_semanal_has_infante.asig_inf_c_sem_id")
                ->where(['asig_inf_c_sem_id' => $asignacion_id])
                ->andWhere('asignacion_inf_c_semanal_has_infante.anio= extract(year from now())::varchar')
                ->andWhere(' asignacion_inf_c_semanal_has_infante.num_semana= extract( week from now())::varchar ')
                ->one();
    }

    /**
     * Busca las asignaciones de todos los infantes en ese año y numero de semana
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return query
     */
    public static function findAllInfantesToAsignacion()
    {
        $finder = static::find()
            ->andWhere('asignacion_inf_c_semanal_has_infante.anio= extract(year from now())::varchar')
            ->andWhere(' asignacion_inf_c_semanal_has_infante.num_semana= extract( week from now())::varchar ');
        return $finder->all();
    }

    /**
     * Busca las asignaciones (disponible) por año y numero de semana actual  y compara con una lista 
     * de ids de infantes
     * @param array $arrayInfantes lista de infantes
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public static function findAllInfantesToAsignacionCompareTo($arrayInfantes)
    {
        $subquery = \yii\db\ActiveRecord::findBySql("SELECT *  FROM asignacion_inf_c_semanal_has_infante sel1
                                            where (sel1.anio= extract(year from now())::varchar
                                            and sel1.num_semana= extract(week from now())::varchar and sel1.fue_consumido = FALSE )");
        $extraWhere = "";
        foreach ($arrayInfantes as $key => $value) {
            $extraWhere.= " (sel2.infante_id = $value) ";
            end($arrayInfantes);
            if ($key !== key($arrayInfantes))
                $extraWhere.=" or ";
        }

        $query = \yii\db\ActiveRecord::findBySql("SELECT *  FROM (" . $subquery->createCommand()->getRawSql() . ") sel2 where $extraWhere");

        return $query->createCommand()->queryAll();
    }

    /**
     * Obtiene un registro de la tabla de juntura deacuedo al id del infante,
     * anio y numero de semana
     * @param int $infante_id
     * @param string $anio
     * @param string $numSemana
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * return query
     */
    public static function findByInfanteAnioNumSemana($infante_id, $anio, $numSemana)
    {
        $finder = static::find()
            ->andWhere(['infante_id' => $infante_id, 'anio' => $anio, 'num_semana' => $numSemana]);
        return $finder->one();
    }
}
