<?php

namespace backend\modules\consumo_alimenticio\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil]].
 *
 * @see \backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil
 */
class BitacoraConsumoInfantilQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * Devuelve la bitacora de acuerdo a la asignación de la carta semanal
     * @param int $asig_inf_c_sem_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return query
     */
    public function bitacoraByAsignacionCS($asig_inf_c_sem_id){
        return $this->where('asig_inf_c_sem_id = :asig_inf_c_sem_id')
            ->addParams([':asig_inf_c_sem_id' => $asig_inf_c_sem_id]);
    }
}