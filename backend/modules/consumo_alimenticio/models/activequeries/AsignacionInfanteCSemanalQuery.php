<?php

namespace backend\modules\consumo_alimenticio\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal]].
 *
 * @see \backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal
 */
class AsignacionInfanteCSemanalQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\consumo_alimenticio\models\AsignacionInfanteCSemanal|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Busca las asignaciones deacuerdo a un id de un infante
     * @param int $infante_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return query
     */
    public function AsignacionByInfante($infante_id) {
        $periodo_actual = Periodo::find()->periodoActivo()->one();
        $this->join('inner join', 'm_carta_semanal', 'm_carta_semanal.m_c_sem_id = asignacion_infante_c_semanal.m_carta_semanal_id');
        return $this->where('infante_id = :infante_id AND m_carta_semanal.m_c_sem_fecha_inicio BETWEEN :fecha_inicio AND :fecha_fin ')
                        ->addParams([':infante_id' => $infante_id, ':fecha_inicio' => $periodo_actual->fecha_inicio, ':fecha_fin' => $periodo_actual->fecha_fin]);
    }

//    public function ValidToDelete(){
//        $this->join('inner join', 'bitacora_consumo_infantil', 'asignacion_infante_c_semanal.asig_inf_c_sem_id= bitacora_consumo_infantil.asig_inf_c_sem_id');
//        return $this->where('infante_id = :infante_id')
//                ->addParams([':infante_id' => $infante_id]);
//
//    }
}
