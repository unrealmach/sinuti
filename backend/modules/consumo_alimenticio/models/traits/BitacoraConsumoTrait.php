<?php
namespace backend\modules\consumo_alimenticio\models\traits;

use backend\modules\composicion_nutricional\models\RangoNutriente;

/**
 * @author Mauricio Chamorro <unrealmach@hotmail.com>
 */
trait BitacoraConsumoTrait
{

    /**
     * Genera los datos necesarios para trabajar en la construccion de la tabla
     * comparativa del consumo ideal, separa por dias los valores nutrimentales del 
     * consumo ideal y del real
     * @param AsignacionInfCSemanalModel $asignacion_model Modelo de la asignacion del infante con la carta semanal
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    public static function generarReporteConsumoIdeal($asignacion_model)
    {
        $bitacoraInfantil = $asignacion_model->bitacoraConsumoInfantils;
        //clasificar por dia
        $bitacoraPorDias = [];
        foreach ($bitacoraInfantil as $key => $bitacora1) {
            $bitacoraPorDias[$bitacora1->b_cons_inf_fecha_consumo][] = $bitacora1;
        }

        $totalComposicionNutricionalReal = [];
        $totalComposicionNutricionalIdeal = [];
        foreach ($bitacoraPorDias as $day => $bitacoras) {
            foreach ($bitacoras as $key => $bitacora) {
                $totalComposicionNutricionalReal[$day][] = self::obtenerValorNutricionalCarta($bitacora);
                $totalComposicionNutricionalIdeal[$day][] = self::llenarConsumoIdeal($bitacora->mPrepCarta->grupo_edad_id);
            }
        }
        $composCero = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0,
            'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0,
            'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0,
            'riboflavina_mg' => 0, 'niacina_mg' => 0, 'vitamina_c_mg' => 0,
            'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0,
            'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0,
            'colesterol_mg' => 0, 'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0,
            'magnesio_mg' => 0, 'vitamina_b6_mg' => 0, 'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];


        $composRealDiario = [];
        $composIdealDiario = [];
        foreach ($totalComposicionNutricionalReal as $day => $composArray) {
            $composTotalReal = $composCero;
            foreach ($composArray as $key => $compos) {
                foreach ($compos as $nut => $cantidad) {
                    $composTotalReal[$nut]+=$cantidad;
                }
            }

            $composRealDiario[$day] = $composTotalReal;
        }
        foreach ($totalComposicionNutricionalIdeal as $day => $composArray) {
            foreach ($composArray as $key => $compos) {
                $composTotalIdeal = $composCero;
                foreach ($compos as $nut => $cantidad) {
                    $composTotalIdeal[$nut]+=$cantidad;
                }
            }
            $composIdealDiario[$day] = $composTotalIdeal;
        }
        return [
            'composRealDiario' => $composRealDiario,
            'composIdealDiario' => $composIdealDiario,
        ];
    }

    /**
     * Obtiene el valor nutricional de una carta
     * Busca por alimentos y por recetas o preparaciones
     * @param BitacoraModel $bitacoraModel Bitacora a ser analizada
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array retorna el total de la composicion nutricional de la carta
     */
    public static function obtenerValorNutricionalCarta($bitacoraModel)
    {
        $composicionNutricionalTotal = [];
        $detallesCarta = $bitacoraModel->mPrepCarta->dPreparacionCartas;
        $composicionNutricionalAlimentos = [];
        $composicionNutricionalPreparaciones = [];
        foreach ($detallesCarta as $key => $detalleCarta) {
            if (is_null($detalleCarta->alimento_id)) {
                array_push($composicionNutricionalPreparaciones, self::calcularValoresNutricionales(
                        $bitacoraModel->b_cons_inf_porcentaje_consumo, $detalleCarta->mPlatillo->composicionNutricionalPlatillo->attributes));
            } else {
                array_push($composicionNutricionalAlimentos, self::calcularValoresNutricionales(
                        $bitacoraModel->b_cons_inf_porcentaje_consumo, self::calcularValoresNutricionalesCarta($detalleCarta->d_prep_carta_cantidad_g, $detalleCarta->alimento->composicionNutricional->attributes))
                );
            }
        }
        if (count($composicionNutricionalPreparaciones) > 0) {
            $composicionNutricionalTotal = array_fill_keys(array_keys($composicionNutricionalPreparaciones[0]), 0);
        } else {
            $composicionNutricionalTotal = array_fill_keys(array_keys($composicionNutricionalAlimentos[0]), 0);
        }
        foreach ($composicionNutricionalAlimentos as $composAlimento) {
            foreach ($composAlimento as $nutriente => $cantidad) {
                $composicionNutricionalTotal[$nutriente]+=$cantidad;
            }
        }
        foreach ($composicionNutricionalPreparaciones as $composPreparacion) {
            foreach ($composPreparacion as $nutriente => $cantidad) {
                $composicionNutricionalTotal[$nutriente]+=$cantidad;
            }
        }
        return $composicionNutricionalTotal;
    }

    /**
     * LLena la posicion del consumo Ideal segun el identificador del grupo edad
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param int $grupo_edad_id id
     * @return array vector con los valores nutricionales 
     */
    public static function llenarConsumoIdeal($grupo_edad_id)
    {
        $composCeroIdeal = ['energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0,
            'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0, 'fosforo_mg' => 0,
            'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0, 'niacina_mg' => 0,
            'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0, 'acidos_grasos_monoinsaturados_g' => 0,
            'acidos_grasos_poliinsaturados_g' => 0, 'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0,
            'potasio_mg' => 0, 'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0,
            'vitamina_b12_mcg' => 0, 'folato_mcg' => 0];

        $out = RangoNutriente::getRangoNutrientesMinDiariosByGrupoEdad($grupo_edad_id);
        foreach ($out as $key => $value) {
            switch ($value['nutriente_nombre']) {
                case "acidos_grasos_monoinsat_g":$composCeroIdeal['acidos_grasos_monoinsaturados_g'] = $value['minimo_diario'];
                    breaK;
                case "acidos_grasos_poliinsaturad_g":$composCeroIdeal['acidos_grasos_poliinsaturados_g'] = $value['minimo_diario'];
                    breaK;
                case "acidos_grasos_saturados_g": $composCeroIdeal['acidos_grasos_saturados_g'] = $value['minimo_diario'];
                    breaK;
                case "vitamina_a_eqiv_retinol_mcg": $composCeroIdeal['vitamina_a_equiv_retinol_mcg'] = $value['minimo_diario'];
                    breaK;
                default : $composCeroIdeal[$value['nutriente_nombre']] = $value['minimo_diario'];
            }
        }
        return $composCeroIdeal;
    }

    /**
     * Obtiene los valores nutricionales del alimento o platillo deacuerdo 
     * a la cantidad de gramos que tenga la carta
     * @param decimal $cantidad_g
     * @param array $composicion
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return type
     */
    public static function calcularValoresNutricionales($cantidad_g, $composicion)
    {
        foreach ($composicion as $key => &$value) {
            $value = round($value * $cantidad_g, 2);
        }
        unset($composicion['com_nut_id']);
        unset($composicion['alimento_id']);
        unset($composicion['m_platillo_id']);
        unset($composicion['com_nut_platillo_id']);
        unset($composicion['es_real']);
        return $composicion;
    }

    /**
     * Calcula los valores nutricionales de la carta de acuerdo a la cantidad en 
     * gramos y la composicion nutricional
     * @param decimal $cantidad_g 
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param array $composicion 
     */
    public static function calcularValoresNutricionalesCarta($cantidad_g, $composicion)
    {
        foreach ($composicion as $key => &$value) {
            $value = round($value * $cantidad_g / 100, 2);
        }
        return $composicion;
    }

    /**
     * Construye la tabla con el consumo ideal vs el real 
     * @param aray $array_consumos
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return string
     */
    public static function construirTableConsumoVs($array_consumos)
    {

        $tableIni = "<table border='1' cellpadding='0' cellspacing='0' align='center' style='font-size: 12px; text-align: center;' class=' table-striped table-header-rotated'>";
        $tableEnd = "</table >";
        $rowCabecera = self::createHtmlHeaderTableConsumoVs();
        $out = self::createRowsConsumos($array_consumos);
        $consumo = $out['consumo'];
        $data = $out['data'];
        $porcentajeAdecuacion = $out['porcentajeAdecuacion'];
        $cuerpoTable = "";

        foreach ($consumo as $key => $datas) {
            $cuerpoTable.=$datas['real'] . $datas['ideal'] . $datas['porcentajeAdecuacion'];
        }
        $out = $tableIni .
            "<thead>$rowCabecera</thead>" .
            "<tbody>$cuerpoTable</tbody>" .
            $tableEnd;
        return ['html' => $out, 'data' => $data, 'porcentajeAdecuacion' => $porcentajeAdecuacion];
    }

    /**
     * Crea la cabecera para la tabla html
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return string
     */
    public static function createHtmlHeaderTableConsumoVs()
    {
        $NutrienteModel = \backend\modules\composicion_nutricional\models\Nutriente::find()->all();
        $nutrientes = [];
        foreach ($NutrienteModel as $key => $model) {
            $nutrientes[] = $model->nutriente_nombre;
        }

        $rowCabecera = '<tr >
                        <th class="rotate" ></th>
                        <th class="rotate" ><div><span>Consumo</span></div></th>';
        foreach ($nutrientes as $key => $nutriente) {
            $rowCabecera.="<th class='rotate' ><div><span>" . ucfirst(str_replace("_", " ", $nutriente)) . "</span></div></th>";
        }
        $rowCabecera.="</tr>";
        return $rowCabecera;
    }

    /**
     * Crea las filas en html para los consumos
     * @param array $arrayConsumos se envia arreglo con el consumo ideal y el real
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array
     */
    public static function createRowsConsumos($arrayConsumos)
    {
        $consumo = [
            0 => ['real' => '', 'ideal' => '', 'porcentajeAdecuacion' => '', 'fecha' => ''],
            1 => ['real' => '', 'ideal' => '', 'porcentajeAdecuacion' => '', 'fecha' => ''],
            2 => ['real' => '', 'ideal' => '', 'porcentajeAdecuacion' => '', 'fecha' => ''],
            3 => ['real' => '', 'ideal' => '', 'porcentajeAdecuacion' => '', 'fecha' => ''],
            4 => ['real' => '', 'ideal' => '', 'porcentajeAdecuacion' => '', 'fecha' => ''],
        ];

        $porcentajeAdecuacion = [];
        $consumoReal = $arrayConsumos['composRealDiario'];
        $consumoIdeal = $arrayConsumos['composIdealDiario'];
        $fechas = array_keys($arrayConsumos['composRealDiario']);
        for ($index = 0; $index < count($fechas); $index++) {
            $consumo[$index]['real'] = self::createOneRowConsumoReal($consumoReal[$fechas[$index]], $fechas[$index]);
            $consumo[$index]['ideal'] = self::createOneRowConsumoIdeal($consumoIdeal[$fechas[$index]]);
            $consumo[$index]['porcentajeAdecuacion'] = self::createOneRowPorcentajeAdecuacion($consumoReal[$fechas[$index]], $consumoIdeal[$fechas[$index]]);
            $porcentajeAdecuacion[$index] = self::calcularPorcentajeAdecuacion($consumoReal[$fechas[$index]], $consumoIdeal[$fechas[$index]]);


            $consumo[$index]['fecha'] = $fechas[$index];
        }
//        var_dumCp( $this->_semaforoBitacora);
//        die();
        return ['consumo' => $consumo, 'data' => $arrayConsumos, 'porcentajeAdecuacion' => $porcentajeAdecuacion];
    }

    /**
     * Crea una fila de consumo ideal
     * @param array $composicion nutricional
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return string
     */
    public static function createOneRowConsumoIdeal($composicion)
    {
        $rowConsumoIdeal = '<tr>
                          <td> Ideal</td>';
        foreach ($composicion as $nutriente => $cantidad) {
            $rowConsumoIdeal.="<td>$cantidad</td>";
        }
        $rowConsumoIdeal.=' </tr>';
        return $rowConsumoIdeal;
    }

    /**
     * Crea una fila del consumo real
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @param array $composicion nutricional
     * @param string $fecha
     * @return string
     */
    public static function createOneRowConsumoReal($composicion, $fecha)
    {
        $rowConsumoReal = '<tr>
    <td  rowspan="3">' . $fecha . '</td>
    <td >Real</td>';
        foreach ($composicion as $nutriente => $cantidad) {
            $rowConsumoReal.="<td>$cantidad</td>";
        }
        $rowConsumoReal.=' </tr>';
        return $rowConsumoReal;
    }

    /**
     * Crea una fila con el porcentaje de adecuacion
     * @param array $composicionReal
     * @param array $composicionIdeal
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return string
     */
    public static function createOneRowPorcentajeAdecuacion($composicionReal, $composicionIdeal)
    {

        $rowPorcentajeAd = '<tr style="font-weight: 600">
                            <td >% adec.</td>';
        foreach ($composicionReal as $nutriente => $cantidad) {
            if ($composicionReal[$nutriente] == 0 || $composicionIdeal[$nutriente] == 0) {
                $porcentaje = 0;
            } else {
                $porcentaje = round(($composicionReal[$nutriente] / $composicionIdeal[$nutriente]) * 100, 2);
            }

            $rowPorcentajeAd.="<td>$porcentaje</td>";
        }
        $rowPorcentajeAd.=' </tr>';
        return $rowPorcentajeAd;
    }

    /**
     * Obtiene el porcentaje de consumo ideal deacuerdo a la composicionReal y
     * a la composicionIdeal
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * return array $out
     */
    public static function calcularPorcentajeAdecuacion($composicionReal, $composicionIdeal)
    {
        $out = [];
        foreach ($composicionReal as $nutriente => $cantidad) {
            if ($composicionReal[$nutriente] == 0 || $composicionIdeal[$nutriente] == 0) {
                $porcentaje = 0;
            } else {
                $porcentaje = round(($composicionReal[$nutriente] / $composicionIdeal[$nutriente]) * 100, 2);
            }
            $out[$nutriente] = $porcentaje;
        }
        return $out;
    }

    /**
     * Obtiene el porcentaje del consumo ideal para la semana y lo interpreta como semaforo
     * % por reporte
     *  x >= 125               sobre
     *  74.99 < x   <= 124.99     normal
     *  x <= 75                inferior
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * return string 
     */
    public static function semaforoBitacoraConsumo($porcentajesAdecuacion)
    {
        $total = [ 'energia_kcal' => 0, 'proteinas_g' => 0, 'grasa_total_g' => 0,
            'carbohidratos_g' => 0, 'fibra_dietetica_g' => 0, 'calcio_mg' => 0,
            'fosforo_mg' => 0, 'hierro_mg' => 0, 'tiamina_mg' => 0, 'riboflavina_mg' => 0,
            'niacina_mg' => 0, 'vitamina_c_mg' => 0, 'vitamina_a_equiv_retinol_mcg' => 0,
            'acidos_grasos_monoinsaturados_g' => 0, 'acidos_grasos_poliinsaturados_g' => 0,
            'acidos_grasos_saturados_g' => 0, 'colesterol_mg' => 0, 'potasio_mg' => 0,
            'sodio_mg' => 0, 'zinc_mg' => 0, 'magnesio_mg' => 0, 'vitamina_b6_mg' => 0,
            'vitamina_b12_mcg' => 0, 'folato_mcg' => 0,];

        $cantidad_dias = !is_null($porcentajesAdecuacion)? count($porcentajesAdecuacion):0;
        $out = " ";
        foreach ($porcentajesAdecuacion as $key => $valoresNut) {
            foreach ($valoresNut as $nutriente => $valor) {
                $total[$nutriente]+=$valor;
            }
        }

        $outIniCabecera = " ";

// saca el promedio segun los dias
        foreach ($total as $nut => $val) {
            $outIniCabecera.="<td> <br>".ucfirst(str_replace("_", " ", $nut))."</br></td>";
            if($cantidad_dias==0){
                 throw new \yii\web\NotFoundHttpException('No existen días asignados a ese infante en su alimentacion');
            }
//            var_dump($cantidad_dias);
 //           die();
            $valor = abs(100 - round($val / $cantidad_dias, 2));
            switch ($valor) {
                case ($valor > 125):$out .= '<td><div><span  class="numberInCircle sobre"> ' . $valor . '</span></div></td>';
                    break;
                case (75 <= $valor && $valor <= 125) :$out .= '<td><span class="numberInCircle normal">' . $valor . '</span></td>';
                    break;
                case ($valor < 75):$out .= '<td><span class="numberInCircle inferior">' . $valor . '</span></td>';
                    break;
            }
        }
        return "<table border='1' cellpadding='0'"
        . " cellspacing='0' align='center' "
            . "style='font-size: 12px !important;"
            . " text-align: center;' "
            . "class=' table-striped table-header-rotated'>"
            . "<tr>"."<td> Nutrientes </td>" . $outIniCabecera . "</tr>" 
            . "<tr>"."<td> Promedio </td>" . $out . "</tr>" 
            . "</table>";
    }
}
