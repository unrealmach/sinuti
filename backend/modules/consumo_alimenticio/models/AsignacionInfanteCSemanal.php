<?php
namespace backend\modules\consumo_alimenticio\models;

use backend\modules\individuo\models\Infante;
use backend\modules\carta_semanal\models\MCartaSemanal;
use Yii;

/**
 * This is the model class for table "asignacion_infante_c_semanal".
 *
 * @property integer $asig_inf_c_sem_id
 * @property integer $infante_id
 * @property integer $m_carta_semanal_id
 * @property string $asi_inf_c_sem_id_observaciones
 *
 * @property AsignacionInfCSemanalHasInfante[] $asignacionInfCSemanalHasInfantes
 * @property Infante[] $infantes
 * @property Infante $infante
 * @property MCartaSemanal $mCartaSemanal
 * @property BitacoraConsumoInfantil[] $bitacoraConsumoInfantils
 */
class AsignacionInfanteCSemanal extends \yii\db\ActiveRecord
{

    public $lista_infantes; //para cargar los ids de los infantes en una lista
    public $lista_infantes_aux;
    public $salones_aux;
    public $capacidad_carta_semanal = 0;
    public $total_infantes_asignados_carta_semanal = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
            return 'asignacion_infante_c_semanal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_id', 'm_carta_semanal_id', 'asi_inf_c_sem_id_observaciones', 'lista_infantes'], 'required'],
            [['infante_id', 'm_carta_semanal_id'], 'integer'],
            [['asi_inf_c_sem_id_observaciones',], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asig_inf_c_sem_id' => Yii::t('app', 'Asig Inf C Sem ID'),
            'infante_id' => Yii::t('app', 'Infante'),
            'm_carta_semanal_id' => Yii::t('app', 'Plan de alimentación Semanal'),
            'asi_inf_c_sem_id_observaciones' => Yii::t('app', 'Observaciones'),
            'capacidad_carta_semanal' => Yii::t('app', 'Capacidad menú'),
            'total_infantes_asignados_carta_semanal' => Yii::t('app', 'Total de infantes asignados'),
            'salones_aux' => Yii::t('app', 'Salones'),
            'lista_infantes_aux' => Yii::t('app', 'Lista temporal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignacionInfCSemanalHasInfantes()
    {
        return $this->hasMany(AsignacionInfCSemanalHasInfante::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfantes()
    {
        return $this->hasMany(Infante::className(), ['infante_id' => 'infante_id'])->viaTable('asignacion_inf_c_semanal_has_infante', ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMCartaSemanal()
    {
        return $this->hasOne(MCartaSemanal::className(), ['m_c_sem_id' => 'm_carta_semanal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBitacoraConsumoInfantils()
    {
        return $this->hasMany(BitacoraConsumoInfantil::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    public function getBitacoraConsumoInfantilOne()
    {
        return $this->hasOne(BitacoraConsumoInfantil::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\consumo_alimenticio\models\activequeries\AsignacionInfanteCSemanalQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\consumo_alimenticio\models\activequeries\AsignacionInfanteCSemanalQuery(get_called_class());
    }

    /**
     * Valida todas las asignaciones para elimnar
     * @param int $asig_inf_c_sem_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return boolean
     */
    public static function checkIsValidToDelete($asig_inf_c_sem_id)
    {
        $connection = \Yii::$app->db;
        $data = $connection->createCommand(
                "SELECT count(*) as cantidad FROM bitacora_consumo_infantil 
            inner join asignacion_infante_c_semanal  on 
            asignacion_infante_c_semanal.asig_inf_c_sem_id=
            bitacora_consumo_infantil.asig_inf_c_sem_id
            where asignacion_infante_c_semanal.asig_inf_c_sem_id = $asig_inf_c_sem_id
            ")
            ->queryOne();
        if (intval($data['cantidad']) > 0) {
            return false;
        } else {
            return true;
        }
    }
}
