<?php
namespace backend\modules\consumo_alimenticio\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil;

/**
 * BitacoraconsumoinfantilSearch represents the model behind the search form about `backend\modules\consumo_alimenticio\models\BitacoraConsumoInfantil`.
 */
class BitacoraconsumoinfantilreportSearch extends BitacoraConsumoInfantil
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['b_cons_inf_id'], 'integer'],
            [['b_cons_inf_porcentaje_consumo'], 'number'],
            [['b_cons_inf_tiempo_comida', 'b_cons_inf_observaciones', 'b_cons_inf_fecha_consumo'], 'safe'],
            [['asig_inf_c_sem_id','m_prep_carta_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = BitacoraConsumoInfantil::find();
         
        $query->joinWith('asigInfCSem.infante', true, 'INNER JOIN')->all();
        $query->joinWith('mPrepCarta', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'b_cons_inf_id' => $this->b_cons_inf_id,
            'b_cons_inf_fecha_consumo' => $this->b_cons_inf_fecha_consumo,
        ]);

        $query->andFilterWhere(['like', 'b_cons_inf_tiempo_comida', $this->b_cons_inf_tiempo_comida])
            ->andFilterWhere(['like', 'b_cons_inf_observaciones', $this->b_cons_inf_observaciones])
            ->andFilterWhere(['=', 'b_cons_inf_porcentaje_consumo', $this->b_cons_inf_porcentaje_consumo])
            ->andFilterWhere(['like',  'm_prep_carta.m_prep_carta_nombre' , $this->m_prep_carta_id])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " " , infante.infante_apellidos)'), $this->asig_inf_c_sem_id,])
            ;
//            var_dump($query);
//            die();
        return $dataProvider;
    }
}
