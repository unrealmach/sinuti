<?php

namespace backend\modules\individuo\controllers;

use Yii;
use backend\modules\individuo\models\Infante;
use backend\modules\individuo\models\InfanteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

/**
 * InfanteController implements the CRUD actions for Infante model.
 */
class InfanteController extends Controller
{

    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                        [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                        [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Infante models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new InfanteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index',
                [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Infante model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view',
                [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Infante model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Infante();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->infante_id]);
        } else {
            return $this->render('create',
                    [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Infante model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->infante_id]);
        } else {
            return $this->render('update',
                    [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Infante model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Infante model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Infante the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Infante::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Devuelve una lista de infantes filtrando por la asignacion,
     * si es coordinador devuelve solo la lista de sus infantes,
     * si es coordinador-gad devuelve la lista de todos
     * @author Mauricio Chamorro
     * @param string $q
     * @param int $id
     * @return JSON
     */
    public function actionAjaxListInfantes($q = null, $id = null)
    {
        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();

        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            $cibv_id       = $autoridadCibv->cen_inf_id;
        }

          $authoAssigment = \backend\modules\rbac\models\AuthAssignment::findOne(['user_id' => Yii::$app->user->id]);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query                       = new \yii\db\Query();

        $query->select(['infante.infante_id as id',new \yii\db\Expression( "CONCAT(infante.infante_nombres, ' ', infante.infante_apellidos) AS text"),
                'infante.infante_dni AS iden'])
            ->from('infante')
            ->innerJoin('sector_asignado_infante',' sector_asignado_infante.infante_id= infante.infante_id')
            ->innerJoin('sector_asignado_educador',' sector_asignado_educador.sector_asignado_educador_id= sector_asignado_infante.sector_asignado_id')
            ->innerJoin('cibv',' cibv.cen_inf_id= sector_asignado_educador.cen_inf_id');

        if($authoAssigment->item_name=='coordinador'){
            $query->where("cibv.cen_inf_id = $cibv_id");
        }

        if (!is_null($q)) {
            $query->andWhere(new \yii\db\Expression('CONCAT(infante_nombres, " " , infante_apellidos) LIKE "%'.$q.'%"'));
        }


        $query->limit(10);
        $command        = $query->createCommand();
        $data           = $command->queryAll();
        $out['results'] = array_values($data);
        return $out;
    }
}