<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */

$this->title = $model->infante->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Patologias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patologia-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->patologia_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->patologia_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'patologia_id',
            'infante.nombreCompleto',
            'patologia_pregunta_1:ntext',
            'patologia_pregunta_2:ntext',
            'patologia_pregunta_3:ntext',
            'patologia_pregunta_4:ntext',
        ],
        ]) ?>

    </div>
</div>
