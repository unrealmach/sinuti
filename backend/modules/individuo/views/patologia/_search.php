<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\PatologiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patologia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'patologia_id') ?>

    <?= $form->field($model, 'infante_id') ?>

    <?= $form->field($model, 'patologia_pregunta_1') ?>

    <?= $form->field($model, 'patologia_pregunta_2') ?>

    <?= $form->field($model, 'patologia_pregunta_3') ?>

    <?php // echo $form->field($model, 'patologia_pregunta_4') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
