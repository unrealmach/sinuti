<?php
//TODO arreglar css para textareainput
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\individuo\models\Infante;
/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Patologia */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="patologia-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
         $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Infante::find()->all(), 'infante_id', 'infante_dni', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Infante', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
//            $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
//            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?= $form->field($model, 'patologia_pregunta_1')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'patologia_pregunta_2')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'patologia_pregunta_3')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'patologia_pregunta_4')->textarea(['rows' => 6]) ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
