<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\RegistroVacunaInfante */

$this->title = Yii::t('app', 'Create Registro Vacuna Infante');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Registro Vacuna Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registro-vacuna-infante-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
