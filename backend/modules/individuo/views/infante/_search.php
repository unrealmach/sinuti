<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\InfanteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="infante-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'infante_id') ?>

    <?= $form->field($model, 'infante_dni') ?>

    <?= $form->field($model, 'infante_nombres') ?>

    <?= $form->field($model, 'infante_apellidos') ?>

    <?= $form->field($model, 'infante_nacionalidad') ?>

    <?php // echo $form->field($model, 'infante_genero') ?>

    <?php // echo $form->field($model, 'infante_etnia') ?>

    <?php // echo $form->field($model, 'infantes_fecha_nacimiento') ?>

    <?php // echo $form->field($model, 'infante_represent_documento') ?>

    <?php // echo $form->field($model, 'infante_represent_nombres') ?>

    <?php // echo $form->field($model, 'infante_represent_apellidos') ?>

    <?php // echo $form->field($model, 'infante_represent_parentesco') ?>

    <?php // echo $form->field($model, 'infante_direccion_domiciliaria') ?>

    <?php // echo $form->field($model, 'infante_represent_telefono') ?>

    <?php // echo $form->field($model, 'infante_represent_correo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
