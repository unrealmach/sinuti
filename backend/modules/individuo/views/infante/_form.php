<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="infante-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
            $form->field($model, 'infante_dni', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_nombres', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_apellidos', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_nacionalidad', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_genero', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'FEMENINO' => 'FEMENINO', 'MASCULINO' => 'MASCULINO',], ['prompt' => 'Seleccione ...'])
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_etnia', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'MESTIZO' => 'MESTIZO', 'AFROAMERICANO' => 'AFROAMERICANO', 'INDIGENA' => 'INDIGENA',], ['prompt' => 'Seleccione ...'])
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infantes_fecha_nacimiento', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
            ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                'inline' => false,
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ])->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_documento', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_nombres', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_apellidos', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_parentesco', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList([ 'MAMA' => 'MAMA', 'PAPA' => 'PAPA', 'ABUELO' => 'ABUELO', 'ABUELA' => 'ABUELA', 'HERMANO' => 'HERMANO', 'HERMANA' => 'HERMANA', 'TIO' => 'TIO', 'TIA' => 'TIA', 'MADRASTRA' => 'MADRASTRA', 'PADRASTRO' => 'PADRASTRO', 'REPRESENTANTE_LEGAL' => 'REPRESENTANTE LEGAL',], ['prompt' => 'Seleccione ...'])
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_direccion_domiciliaria', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_telefono', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'infante_represent_correo', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
