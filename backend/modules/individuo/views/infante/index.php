<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\individuo\models\InfanteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Infantes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infante-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Infante'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//                        'infante_id',
            'infante_dni',
            'infante_nombres',
            'infante_apellidos',
            'infante_nacionalidad',
            // 'infante_genero',
            // 'infante_etnia',
            // 'infantes_fecha_nacimiento',
            // 'infante_represent_documento',
            // 'infante_represent_nombres',
            // 'infante_represent_apellidos',
            // 'infante_represent_parentesco',
            // 'infante_direccion_domiciliaria',
            // 'infante_represent_telefono',
            // 'infante_represent_correo',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
