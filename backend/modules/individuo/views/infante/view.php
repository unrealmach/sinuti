<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\individuo\models\Infante */

$this->title = $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Infantes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="infante-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->infante_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->infante_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                    'infante_id',
            'infante_dni',
            'infante_nombres',
            'infante_apellidos',
            'infante_nacionalidad',
            'infante_genero',
            'infante_etnia',
            'infantes_fecha_nacimiento',
            'infante_represent_documento',
            'infante_represent_nombres',
            'infante_represent_apellidos',
            'infante_represent_parentesco',
            'infante_direccion_domiciliaria',
            'infante_represent_telefono',
            'infante_represent_correo',
        ],
        ]) ?>

    </div>
</div>
