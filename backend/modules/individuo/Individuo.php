<?php

namespace backend\modules\individuo;

class Individuo extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\individuo\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
