<?php

namespace backend\modules\individuo\models;

use backend\modules\metricas_individuo\models\DatoAntropometrico;
use backend\modules\asistencia\models\Asistencia;
use backend\modules\individuo\models\Patologia;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\individuo\models\RegistroVacunaInfante;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use yii\web\Response;

//use PHPUnit\Util\Json;
use Yii;
use \backend\modules\rbac\models\AuthAssignment;
use \backend\modules\parametros_sistema\models\Periodo;
use yii\helpers\Json;

/**
 * This is the model class for table "infante".
 *
 * @property integer $infante_id
 * @property string $infante_dni
 * @property string $infante_nombres
 * @property string $infante_apellidos
 * @property string $infante_nacionalidad
 * @property string $infante_genero
 * @property string $infante_etnia
 * @property string $infantes_fecha_nacimiento
 * @property string $infante_represent_documento
 * @property string $infante_represent_nombres
 * @property string $infante_represent_apellidos
 * @property string $infante_represent_parentesco
 * @property string $infante_direccion_domiciliaria
 * @property string $infante_represent_telefono
 * @property string $infante_represent_correo
 *
 * @property AsignacionInfCSemanalHasInfante[] $asignacionInfCSemanalHasInfantes
 * @property AsignacionInfanteCSemanal[] $asigInfCSems
 * @property AsignacionInfanteCSemanal[] $asignacionInfanteCSemanals
 * @property Asistencia[] $asistencias
 * @property DatoAntropometrico[] $datoAntropometricos
 * @property Matricula[] $matriculas
 * @property Patologia[] $patologias
 * @property RegistroVacunaInfante[] $registroVacunaInfantes
 * @property Salon[] $salons
 */
class Infante extends \yii\db\ActiveRecord
{

    public $nombre;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'infante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_dni', 'infante_nombres', 'infante_apellidos', 'infante_nacionalidad', 'infantes_fecha_nacimiento', 'infante_represent_documento', 'infante_represent_nombres', 'infante_represent_apellidos', 'infante_direccion_domiciliaria', 'infante_represent_telefono'], 'required'],
            [['infante_genero', 'infante_etnia', 'infante_represent_parentesco'], 'string'],
            [['infantes_fecha_nacimiento'], 'safe'],
            [['infante_dni', 'infante_represent_documento'], 'string', 'max' => 45],
            [['infante_nombres', 'infante_apellidos', 'infante_represent_nombres', 'infante_represent_apellidos'], 'string', 'max' => 150],
            [['infante_nacionalidad'], 'string', 'max' => 50],
            [['infante_direccion_domiciliaria'], 'string', 'max' => 300],
            [['infante_represent_telefono'], 'string', 'max' => 15],
            [['infante_represent_correo'], 'string', 'max' => 100],
            [['infante_dni'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'infante_id' => 'Infante ID',
            'infante_dni' => 'D.N.I. o C.I.',
            'infante_nombres' => 'Nombres',
            'infante_apellidos' => 'Apellidos',
            'infante_nacionalidad' => 'Nacionalidad',
            'infante_genero' => 'Género',
            'infante_etnia' => 'Etnia',
            'infantes_fecha_nacimiento' => 'Fecha Nacimiento',
            'infante_represent_documento' => ' D.N.I o C.I.',
            'infante_represent_nombres' => 'Nombres del Representante',
            'infante_represent_apellidos' => 'Apellidos del Representante',
            'infante_represent_parentesco' => 'Parentesco ',
            'infante_direccion_domiciliaria' => 'Dirección Domiciliaria',
            'infante_represent_telefono' => 'Teléfono ',
            'infante_represent_correo' => 'Email ',
            'nombreCompleto' => 'Nombre Completo',
            'nombre' => 'Nombre',
        ];
    }

    public function getNombreCompleto()
    {
        return $this->infante_nombres . " " . $this->infante_apellidos;
    }

    public function getCedulaPasaporteAndNombreCompleto()
    {
        return $this->infante_dni . " - " . $this->infante_nombres . " " . $this->infante_apellidos;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignacionInfCSemanalHasInfantes()
    {
        return $this->hasMany(AsignacionInfCSemanalHasInfante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsigInfCSems()
    {
        return $this->hasMany(AsignacionInfanteCSemanal::className(), ['asig_inf_c_sem_id' => 'asig_inf_c_sem_id'])->viaTable('asignacion_inf_c_semanal_has_infante', ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsignacionInfanteCSemanals()
    {
        return $this->hasMany(AsignacionInfanteCSemanal::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAsistencias()
    {
        return $this->hasMany(Asistencia::className(), ['infantes_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDatoAntropometricos()
    {
        return $this->hasMany(DatoAntropometrico::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::className(), ['infante_id' => 'infante_id']);
    }


    public function getMatricula()
    {
        return $this->hasOne(Matricula::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * Devuelve la matricula con estado activo y periodo activo
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function getMatriculaActiva()
    {
        return $this->hasOne(Matricula::className(), ['infante_id' => 'infante_id'])->matriculaActiva()->byPeriodoActual();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatologias()
    {
        return $this->hasMany(Patologia::className(), ['infante_id' => 'infante_id']);
    }

    public function getPatologia()
    {
        return $this->hasOne(Patologia::className(), ['infante_id' => 'infante_id']);
    }

    public function getSectorAsignadoInfantes()
    {
        return $this->hasMany(SectorAsignadoInfante::className(), ['infante_id' => 'infante_id']);
    }

    public function getSectorAsignadoInfante()
    {
        return $this->hasOne(SectorAsignadoInfante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistroVacunaInfantes()
    {
        return $this->hasMany(RegistroVacunaInfante::className(), ['infante_id' => 'infante_id']);
    }

    public function getRegistroVacunaInfante()
    {
        return $this->hasOne(RegistroVacunaInfante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalons()
    {
        return $this->hasMany(Salon::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * ----------------------EVENTOS-------------------------
     */

    // public static function asignarSectorAsignadoInfante($infante_id)
    // {
    //     \Yii::$app->response->format = Response::FORMAT_JSON;
    //     $sectorAsignado = new SectorAsignadoInfante();
    //     $sectorAsignado->infante_id = $infante_id;
    //     $asignacionEducador = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
    //     $sectorAsignado->sector_asignado_id = $asignacionEducador->sector_asignado_educador_id;
    //     if ($sectorAsignado->save()) {

    //         return ['status' => TRUE, 'errors' => ""];
    //     } else {
    //         return ['status' => FALSE, 'errors' => "Error al generar la matricula"];

    //     }
    // }



    /**
     *Asigna el infante al salon con el id del educador con el cual esta trabajando
     * en ese instante en el navegador
     * @param $infante_id
     * @return array
     */
    //TODO: cambiar al modelo de sector asignado infante
    public static function asignarSectorAsignadoInfante($infante_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $sectorAsignado = new SectorAsignadoInfante();
        $sectorAsignado->infante_id = $infante_id;
        $asignacionEducador = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        $sectorAsignado->sector_asignado_id = $asignacionEducador->sector_asignado_educador_id;
        if ($sectorAsignado->save()) {

            return ['status' => TRUE, 'errors' => ""];
        } else {
            return ['status' => FALSE, 'errors' => "Error al asignar el infante al salón"];

        }
    }


    /**
     * evento que permite generar la matricula de acuerdo al infante
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public static function generarMatricula($infante_id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $authAsignmentModel = AuthAssignment::find()->getRoleByUser();
        $user_id = Yii::$app->user->id;
        $periodo = Periodo::find()->periodoActivo()->one();

        if ($authAsignmentModel->item_name == 'coordinador') {
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo($user_id)->one();


            if (!empty($asignacionCoordinadorCibv)) {
                $autoridadCibv = AutoridadesCibv::find()->deCibvAndPeriodo($asignacionCoordinadorCibv->autoridadCibv->cenInf->cen_inf_id, $periodo->periodo_id)->one();

                if (!empty($autoridadCibv)) {
                    $matricula = new Matricula();
                    $matricula->infante_id = $infante_id;
                    $matricula->cen_inf_id = $autoridadCibv->cen_inf_id;
                    $matricula->periodo_id = $autoridadCibv->periodo_id;
                    $matricula->matricula_estado = $matricula->ESTADO_ACTIVO;

//no lanza l tru devuev nuoo
                    if ($matricula->save()) {

                        return ['status' => TRUE, 'errors' => ""];
                    } else {
                        return ['status' => FALSE, 'errors' => "Error al generar la matricula"];

                    }

                } else {
                    return ['status' => FALSE, 'errors' => "No hay coordinador para el cibv"];
                }
            } else {
                return ['status' => FALSE,
                    'errors' => "Aun no existe un coordinador para el centro infantil en el periodo actual"];

            }
        } else if ($authAsignmentModel->item_name == 'educador') {
            $asignacionEducador = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo($user_id)->one();
            if (!empty($asignacionEducador)) {
                $sectorAsignado = SectorAsignadoEducador::findOne(['sector_asignado_educador_id' => $asignacionEducador->sector_asignado_educador_id]);
                $autoridadCibv = AutoridadesCibv::find()->deCibvAndPeriodo($sectorAsignado->cen_inf_id, $periodo->periodo_id)->one();
                if (!empty($autoridadCibv)) {
                    $matricula = new Matricula();
                    $matricula->infante_id = $infante_id;
                    $matricula->cen_inf_id = $autoridadCibv->cen_inf_id;
                    $matricula->periodo_id = $autoridadCibv->periodo_id;
                    $matricula->matricula_estado = $matricula->ESTADO_ACTIVO;
                    if ($matricula->save()) {

                        return ['status' => TRUE, 'errors' => ""];
                    } else {
                        return ['status' => FALSE, 'errors' => "Error al generar la matricula"];

                    }


                }
            } else {
                return ['status' => FALSE, 'errors' => "No hay coordinador para el cibv"];
            }
            return ['status' => FALSE,
                'errors' => "Aun no existe un coordinador para el centro infantil en el periodo actual"];
        } else {
            return ['status' => FALSE, 'errors' => "Este usuario no puede generar matrículas"];
        }
    }

    /* public function init()
     {
         $this->on(self::GENERACION_MATRICULA, [$this, 'generarMatricula']);

         return parent::init();
     }*/

    public static function find()
    {
        return new \backend\modules\individuo\models\activequeries\InfanteQuery(get_called_class());
    }

}
