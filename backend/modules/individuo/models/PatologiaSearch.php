<?php

namespace backend\modules\individuo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\individuo\models\Patologia;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use frontend\modules\inscripcion\Matricula;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;

/**
 * PatologiaSearch represents the model behind the search form about `backend\modules\individuo\models\Patologia`.
 */
class PatologiaSearch extends Patologia {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['patologia_id'], 'integer'],
            [['patologia_pregunta_1', 'patologia_pregunta_2', 'patologia_pregunta_3', 'patologia_pregunta_4', 'infante_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Patologia::find();

        $query->joinWith('infante', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->join('inner join', 'matricula', 'patologia.infante_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'ACTIVO' ");

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        // si rol es coordinador le permite ver las patologias de los infantes que pertenecen al centro infantil durante un periodo activo
        if ($authAsiignmentMdel->item_name == 'coordinador') {
            $periodo = Periodo::find()->periodoActivo()->one();
            $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, $periodo->periodo_id)->all();
            if (!empty($infantesbyCibv)) {
                foreach ($infantesbyCibv as $value) {
                    $query->orHaving(['patologia.infante_id' => $value->infante_id]);
                    $query->orFilterWhere(['like', 'patologia.infante_id', $value->infante_id]);
                }
            } else {
                $query->orHaving(['patologia.infante_id' => 0]);
            }
        }
        // si rol es educador le permite ver las patolgias de los infantes que pertenecen al salon del educador durante un periodo activo
        if ($authAsiignmentMdel->item_name == 'educador') {
            $query->join('inner join', 'sector_asignado_infante', 'patologia.infante_id = sector_asignado_infante.infante_id');
            $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
            if (!empty($asignacionEducadorCibv)) {
                $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
                if (!empty($sectorAsignadoInfante)) {
                    foreach ($sectorAsignadoInfante as $value) {
                        $query->orHaving(['patologia.infante_id' => $value->infante_id]);
                        $query->orFilterWhere(['like', 'patologia.infante_id', $value->infante_id]);
                    }
                } else {
                    $query->orHaving(['patologia.infante_id' => 0]);
                }
            } else {
                $query->orHaving(['patologia.infante_id' => 0]);
            }
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'patologia_id' => $this->patologia_id,
            'infante_id' => $this->infante_id,
        ]);

        $query->andFilterWhere(['like', 'patologia_pregunta_1', $this->patologia_pregunta_1])
                ->andFilterWhere(['like', 'patologia_pregunta_2', $this->patologia_pregunta_2])
                ->andFilterWhere(['like', 'patologia_pregunta_3', $this->patologia_pregunta_3])
                ->andFilterWhere(['like', 'patologia_pregunta_4', $this->patologia_pregunta_4]);

        $query->andFilterWhere(
                ['like', 'infante.infante_dni', $this->infante_id]
        );
        return $dataProvider;
    }

}
