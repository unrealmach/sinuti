<?php
namespace backend\modules\individuo\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\individuo\models\Infante;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\inscripcion\models\Matricula;
use \backend\modules\rbac\models\AuthAssignment;

/**
 * InfanteSearch represents the model behind the search form about `backend\modules\individuo\models\Infante`.
 */
class InfanteInactivoSearch extends Infante
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['infante_id'], 'integer'],
                [['infante_dni', 'infante_nombres', 'infante_apellidos', 'infante_nacionalidad',
                'infante_genero', 'infante_etnia',
                'infantes_fecha_nacimiento', 'infante_represent_documento', 'infante_represent_nombres',
                'infante_represent_apellidos', 'infante_represent_parentesco', 'infante_direccion_domiciliaria',
                'infante_represent_telefono', 'infante_represent_correo', 'nombre'],
                'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Infante::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->join('inner join', 'matricula', 'infante.infante_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'RETIRADO' ");

        $authAsiignmentMdel = AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $periodo = Periodo::find()->periodoActivo()->one();
        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // si el rol es coordinador mustra las bitacoras que pertenecen al centro infantil del coordinador
                $arrayIds = $this->getCoincidenciasCoordinador($periodo);
                $query->andWhere(['IN', 'matricula.infante_id',
                    $arrayIds]);
                break;
            case 'Admin' || 'coordinador-gad':
                $arrayIds = $this->getCoincidenciasAdministrador();
                $query->andWhere(['IN', 'matricula.infante_id',
                    $arrayIds]);
                break;

//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        $query->andFilterWhere([
            'infante_id' => $this->infante_id,
            'infantes_fecha_nacimiento' => $this->infantes_fecha_nacimiento,
        ]);

        $query->andFilterWhere(['like', 'infante_dni', $this->infante_dni])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(infante_nombres, " " ,infante_apellidos )'),
                $this->nombre])
            ->andFilterWhere(['like', 'infante_nombres', $this->infante_nombres])
            ->andFilterWhere(['like', 'infante_apellidos', $this->infante_apellidos])
            ->andFilterWhere(['like', 'infante_nacionalidad', $this->infante_nacionalidad])
            ->andFilterWhere(['like', 'infante_genero', $this->infante_genero])
            ->andFilterWhere(['like', 'infante_etnia', $this->infante_etnia])
            ->andFilterWhere(['like', 'infante_represent_documento', $this->infante_represent_documento])
            ->andFilterWhere(['like', 'infante_represent_nombres', $this->infante_represent_nombres])
            ->andFilterWhere(['like', 'infante_represent_apellidos', $this->infante_represent_apellidos])
            ->andFilterWhere(['like', 'infante_represent_parentesco', $this->infante_represent_parentesco])
            ->andFilterWhere(['like', 'infante_direccion_domiciliaria', $this->infante_direccion_domiciliaria])
            ->andFilterWhere(['like', 'infante_represent_telefono', $this->infante_represent_telefono])
            ->andFilterWhere(['like', 'infante_represent_correo', $this->infante_represent_correo]);

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador($periodo)
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            if (!empty($autoridadCibv) && !empty($periodo)) {
                $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, null)->all();
                if (!empty($infantesbyCibv)) {
                    foreach ($infantesbyCibv as $value) {
                        array_push($arrayIds, $value->infante_id);
                    }
                }
            }
        }

        return $arrayIds;
    }

    private function getCoincidenciasEducador($periodo)
    {
        $arrayIds = [];

        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->infante_id);
                }
            }
        }

        return $arrayIds;
    }

    private function getCoincidenciasAdministrador()
    {
        $arrayIds = [];

        $sectorAsignadoInfante = SectorAsignadoInfante::find()->all();
        if (!empty($sectorAsignadoInfante)) {
            foreach ($sectorAsignadoInfante as $value) {
                array_push($arrayIds, $value->infante_id);
            }
        }

        return $arrayIds;
    }
}
