<?php

namespace backend\modules\individuo\models;
use backend\modules\metricas_individuo\models\TipoVacuna;
use backend\modules\individuo\models\Infante;

use Yii;

/**
 * This is the model class for table "registro_vacuna_infante".
 *
 * @property integer $reg_vac_inf_id
 * @property integer $infante_id
 * @property integer $tipo_vacuna_id
 * @property string $tipo_vac_fecha_aplicacion
 *
 * @property Infante $infante
 * @property TipoVacuna $tipoVacuna
 */
class RegistroVacunaInfante extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registro_vacuna_infante';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_id', 'tipo_vacuna_id', 'tipo_vac_fecha_aplicacion'], 'required'],
            [['infante_id', 'tipo_vacuna_id'], 'integer'],
            [['tipo_vac_fecha_aplicacion'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'reg_vac_inf_id' => Yii::t('app', 'Reg Vac Inf ID'),
            'infante_id' => Yii::t('app', 'Infante'),
            'tipo_vacuna_id' => Yii::t('app', 'Tipo Vacuna'),
            'tipo_vac_fecha_aplicacion' => Yii::t('app', 'Fecha de registro'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoVacuna()
    {
        return $this->hasOne(TipoVacuna::className(), ['tipo_vacuna_id' => 'tipo_vacuna_id']);
    }
}
