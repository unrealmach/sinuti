<?php

namespace backend\modules\individuo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\individuo\models\Infante;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\inscripcion\models\Matricula;
use \backend\modules\rbac\models\AuthAssignment;
use yii\web\NotFoundHttpException;

/**
 * InfanteSearch represents the model behind the search form about `backend\modules\individuo\models\Infante`.
 */
class InfanteSearch extends Infante
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['infante_id'], 'integer'],
                [['infante_dni', 'infante_nombres', 'infante_apellidos', 'infante_nacionalidad',
                'infante_genero', 'infante_etnia',
                'infantes_fecha_nacimiento', 'infante_represent_documento', 'infante_represent_nombres',
                'infante_represent_apellidos', 'infante_represent_parentesco', 'infante_direccion_domiciliaria',
                'infante_represent_telefono', 'infante_represent_correo', 'nombre'],
                'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        var_dump(Yii::$app->user->getIdentity()->email);
//        die();
        $query = Infante::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
//             $query->where('0=1');
            return $dataProvider;
        }
        $query->join('inner join', 'matricula',
            'infante.infante_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'ACTIVO' ");

        $authAsiignmentMdel = AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        $periodo            = Periodo::find()->periodoActivo()->one();
        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // si el rol es coordinador mustra las bitacoras que pertenecen al centro infantil del coordinador
                $arrayIds = $this->getCoincidenciasCoordinador($periodo);
                $query->andWhere(['IN', 'matricula.infante_id',
                    $arrayIds]);
                break;

            case 'educador':// si el rol es ducador mustra todas las bitacoras que pertenscan a los infantes del salon del educador
                $query->join('inner join', 'sector_asignado_infante',
                    'infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasEducador($periodo);
                $query->andWhere(['IN', 'sector_asignado_infante.infante_id',
                    $arrayIds]);
                break;
            case 'nutricion':
                $arrayIds = $this->getCoincidenciasNutricionista();
                $query->andWhere(['IN', 'infante.infante_id',
                    $arrayIds]);
                break;
            case 'Admin'||'coordinador-gad':
                $arrayIds = $this->getCoincidenciasAdministrador();
                $query->andWhere(['IN', 'infante.infante_id',
                    $arrayIds]);
                break;
//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }

        $query->andFilterWhere([
            'infante_id' => $this->infante_id,
            'infantes_fecha_nacimiento' => $this->infantes_fecha_nacimiento,
        ]);

        $query->andFilterWhere(['like', 'infante_dni', $this->infante_dni])
            ->andFilterWhere(['like',new \yii\db\Expression( 'CONCAT(infante_nombres, " " ,infante_apellidos )'),
                $this->nombre])
            ->andFilterWhere(['like', 'infante_nombres', strtoupper($this->infante_nombres)])
            ->andFilterWhere(['like', 'infante_apellidos', strtoupper($this->infante_apellidos)])
            ->andFilterWhere(['like', 'infante_nacionalidad', strtoupper($this->infante_nacionalidad)])
            ->andFilterWhere(['like', 'infante_genero', strtoupper($this->infante_genero)])
            ->andFilterWhere(['like', 'infante_etnia', strtoupper($this->infante_etnia)])
            ->andFilterWhere(['like', 'infante_represent_documento', $this->infante_represent_documento])
            ->andFilterWhere(['like', 'infante_represent_nombres', strtoupper($this->infante_represent_nombres)])
            ->andFilterWhere(['like', 'infante_represent_apellidos', strtoupper($this->infante_represent_apellidos)])
            ->andFilterWhere(['like', 'infante_represent_parentesco', strtoupper($this->infante_represent_parentesco)])
            ->andFilterWhere(['like', 'infante_direccion_domiciliaria', strtoupper($this->infante_direccion_domiciliaria)])
            ->andFilterWhere(['like', 'infante_represent_telefono', $this->infante_represent_telefono])
            ->andFilterWhere(['like', 'infante_represent_correo', $this->infante_represent_correo]);

//        $query->orHaving(['infante.infante_id' => 0]);
      //  print_r($query->createCommand()->getRawSql());        die();
        return $dataProvider;
    }

    private function getCoincidenciasCoordinador($periodo)
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            if (!empty($autoridadCibv) && !empty($periodo)) {
                $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id,
                        $periodo->periodo_id)->all();
                if (!empty($infantesbyCibv)) {
                    foreach ($infantesbyCibv as $value) {
                        array_push($arrayIds, $value->infante_id);
                    }
                }
            }
        }

        return $arrayIds;
    }

    private function getCoincidenciasEducador($periodo)
    {
        $arrayIds = [];

        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();

            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->infante_id);
                }
            }
        }else{
            throw new NotFoundHttpException('No tiene asignado un sector de educador, comuniquese con el administraor');
        }


        return $arrayIds;
    }
    
    private function getCoincidenciasAdministrador()
    {
        $arrayIds = [];

            $sectorAsignadoInfante = SectorAsignadoInfante::find()->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->infante_id);
                }
        }

        return $arrayIds;
    }
    /*     * Yii::$app->user->id
     * Busca la coincidencia por distrito->parroquia->cibv
     */

    private function getCoincidenciasNutricionista()
    {
        $nutricionista = \backend\modules\recursos_humanos\models\Nutricionista::find()
                ->where(
                    ['nutricionista_correo' => Yii::$app->user->getIdentity()->email])->one()
        ;
        $infantes_id   = Yii::$app->db->createCommand("
                   select infante.infante_id from infante
        inner join matricula on matricula.infante_id=infante.infante_id
        INNER join periodo on matricula.periodo_id = periodo.periodo_id
        INNER join cibv on matricula.cen_inf_id= cibv.cen_inf_id

        INNER join parroquia on parroquia.parroquia_id= cibv.parroquia_id
        inner JOIN localidad on localidad.localidad_id= parroquia.localidad_id
        inner join distrito on distrito.distrito_id = localidad.distrito_id
        INNER join asignacion_nutricionista_distrito asign on asign.distrito_id=distrito.distrito_id
        inner join nutricionista on nutricionista.nutricionista_id= asign.nutricionista_id

        where  periodo.estado='ACTIVO' and matricula.matricula_estado='ACTIVO'
        and  nutricionista.nutricionista_id= $nutricionista->nutricionista_id
        ;")->queryAll();

        $arrayIds = [];
        foreach ($infantes_id as $key => $value) {
            array_push($arrayIds, $value['infante_id']);
        }
        return $arrayIds;
    }
}