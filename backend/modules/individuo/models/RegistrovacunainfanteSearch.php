<?php
namespace backend\modules\individuo\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\individuo\models\RegistroVacunaInfante;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\inscripcion\models\Matricula;

/**
 * RegistrovacunainfanteSearch represents the model behind the search form about `backend\modules\individuo\models\RegistroVacunaInfante`.
 */
class RegistrovacunainfanteSearch extends RegistroVacunaInfante
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['reg_vac_inf_id'], 'integer'],
                [['tipo_vac_fecha_aplicacion', 'infante_id', 'tipo_vacuna_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistroVacunaInfante::find();
        $query->joinWith('infante', true, 'INNER JOIN')->all();
        $query->joinWith('tipoVacuna', true, 'INNER JOIN')->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->join('inner join', 'matricula', 'registro_vacuna_infante.infante_id = matricula.infante_id');
        $query->where("matricula.matricula_estado = 'ACTIVO'" );

        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                'user_id' => Yii::$app->user->id])->one();

        $periodo = Periodo::find()->periodoActivo()->one();
        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': // si rol es coordinador le permite ver los registros de las vacunas de los infantes que pertenecen al centro infantil durante un periodo activo
                $arrayIds = $this->getCoincidenciasCoordinador($periodo);
                $query->andWhere(['IN', 'registro_vacuna_infante.infante_id',
                    $arrayIds]);
                break;
            case 'Admin' || 'coordinador-gad': // si rol es coordinador le permite ver los registros de las vacunas de los infantes que pertenecen al centro infantil durante un periodo activo
                $arrayIds = $this->getCoincidenciasAdministrador($periodo);
                $query->andWhere(['IN', 'registro_vacuna_infante.infante_id',
                    $arrayIds]);
                break;

            case 'educador': // si rol es educador le permite ver los registros de vacunas de los infantes que pertenecen al salon del educador durante un periodo activo
                $query->join('inner join', 'sector_asignado_infante', 'registro_vacuna_infante.infante_id = sector_asignado_infante.infante_id');
                $arrayIds = $this->getCoincidenciasEducador($periodo);
                $query->andWhere(['IN', 'registro_vacuna_infante.infante_id',
                    $arrayIds]);
                break;


//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }


        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
                'reg_vac_inf_id' => $this->reg_vac_inf_id,
                'tipo_vac_fecha_aplicacion' => $this->tipo_vac_fecha_aplicacion,
            ])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " " ,infante.infante_apellidos )'),
                $this->infante_id])
            ->andFilterWhere(['like', 'tipo_vacuna.tipo_vac_nombre',
                $this->tipo_vacuna_id]);

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador($periodo)
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            if (!empty($autoridadCibv) && !empty($periodo)) {
                $infantesbyCibv = Matricula::find()->deCentroInfantiIdAndPeriodoId($autoridadCibv->cen_inf_id, $periodo->periodo_id)->all();
                if (!empty($infantesbyCibv)) {
                    foreach ($infantesbyCibv as $value) {
                        array_push($arrayIds, $value->infante_id);
                    }
                }
            }
        }
        return $arrayIds;
    }

    private function getCoincidenciasEducador($periodo)
    {
        $arrayIds = [];

        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->infante_id);
                }
            }
        }
        return $arrayIds;
    }

    private function getCoincidenciasAdministrador($periodo)
    {
        $arrayIds = [];

        $sectorAsignadoInfante = SectorAsignadoInfante::find()->all();
        if (!empty($sectorAsignadoInfante)) {
            foreach ($sectorAsignadoInfante as $value) {
                array_push($arrayIds, $value->infante_id);
            }
        }
        return $arrayIds;
    }
}
