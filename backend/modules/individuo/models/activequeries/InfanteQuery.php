<?php

namespace backend\modules\individuo\models\activequeries;


class InfanteQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\Cibv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\Cibv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    /**
     * Devuelve los infatnes que no estan matriculados
     * @return InfanteQuery
     */
    public function noMatriculados()
    {

        // subquery que trae los infantes con matriculas anteriores
      $subq1=  (new \yii\db\Query)
          ->from('infante')
            ->select(['infante.infante_id'])
          ->distinct(true)
          ->innerJoin('matricula','matricula.infante_id = infante.infante_id')

            ->where(['matricula.matricula_estado'=>'RETIRADO']);


// subquery que trae los infantes jamas matriculados
    $subq2 = (new \yii\db\Query)
        ->from('infante')
        ->select(['infante.infante_id'])
        ->join('FULL OUTER JOIN', 'matricula', 'infante.infante_id = matricula.infante_id')
        ->where('matricula.matricula_id IS NULL');

        $unionQuery = (new \yii\db\Query())
            ->from(['infante' => $subq1->union($subq2)])
            ;

// busca unicamente los IDS para traerlos con el finder del mdelo
        return $this->where(['IN','infante_id',$unionQuery]);
        // print_r($this->createCommand()->getRawSql());die();

    }


}