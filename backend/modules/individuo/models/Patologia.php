<?php

namespace backend\modules\individuo\models;

use Yii;
use backend\modules\individuo\models\Infante;
/**
 * This is the model class for table "patologia".
 *
 * @property integer $patologia_id
 * @property integer $infante_id
 * @property string $patologia_pregunta_1
 * @property string $patologia_pregunta_2
 * @property string $patologia_pregunta_3
 * @property string $patologia_pregunta_4
 *
 * @property Infante $infante
 */
class Patologia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'patologia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_id', 'patologia_pregunta_1', 'patologia_pregunta_2', 'patologia_pregunta_3', 'patologia_pregunta_4'], 'required'],
            [['infante_id'], 'integer'],
            [['patologia_pregunta_1', 'patologia_pregunta_2', 'patologia_pregunta_3', 'patologia_pregunta_4'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'patologia_id' => Yii::t('app', 'Patologia ID'),
            'infante_id' => Yii::t('app', 'Infante'),
            'patologia_pregunta_1' => Yii::t('app', '¿Qué alimentos le gusta consumir?'),
            'patologia_pregunta_2' => Yii::t('app', '¿Qué alimentos le desagrada consumir?'),
            'patologia_pregunta_3' => Yii::t('app', '¿Tiene reacción alergica causada por consumir algun alimento?'),
            'patologia_pregunta_4' => Yii::t('app', '¿Consume algún tipo de suplemento vitaminico o/y mineral?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }
}
