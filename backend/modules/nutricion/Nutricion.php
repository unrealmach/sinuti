<?php

namespace backend\modules\nutricion;

class Nutricion extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\nutricion\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
