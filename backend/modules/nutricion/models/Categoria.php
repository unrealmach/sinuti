<?php

namespace backend\modules\nutricion\models;

use Yii;

/**
 * This is the model class for table "categoria".
 *
 * @property integer $categoria_id
 * @property string $categoria_nombre
 * @property string $categoria_descripcion
 *
 * @property Alimento[] $alimentos
 * @property PorcionPorCategoria[] $porcionPorCategorias
 */
class Categoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria_nombre', 'categoria_descripcion'], 'required'],
            [['categoria_nombre'], 'string', 'max' => 100],
            [['categoria_descripcion'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'categoria_id' => Yii::t('app', 'Categor&iacutea'),
            'categoria_nombre' => Yii::t('app', 'Nombre'),
            'categoria_descripcion' => Yii::t('app', 'Descripci&oacuten'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimentos()
    {
        return $this->hasMany(Alimento::className(), ['categoria_id' => 'categoria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorcionPorCategorias()
    {
        return $this->hasMany(PorcionPorCategoria::className(), ['categoria_id' => 'categoria_id']);
    }
}
