<?php
namespace backend\modules\nutricion\models;

use Yii;
use backend\modules\nutricion\models\Categoria;
use backend\modules\composicion_nutricional\models\ComposicionNutricional;
use backend\modules\racion_alimenticia\models\PorcionPorAlimento;

/**
 * This is the model class for table "alimento".
 *
 * @property integer $alimento_id
 * @property integer $categoria_id
 * @property string $alimento_nombre
 * @property string $alimento_descripcion
 *
 * @property Categoria $categoria
 * @property ComposicionNutricional[] $composicionNutricionals
 * @property PorcionPorAlimento[] $porcionPorAlimentos
 */
class Alimento extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alimento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria_id', 'alimento_nombre', 'alimento_descripcion'], 'required'],
            [['categoria_id'], 'integer'],
            [['alimento_descripcion'], 'string'],
            [['alimento_nombre'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'alimento_id' => Yii::t('app', 'Alimento '),
            'categoria_id' => Yii::t('app', 'Categor&iacutea'),
            'alimento_nombre' => Yii::t('app', 'Nombre'),
            'alimento_descripcion' => Yii::t('app', 'Descripci&oacuten'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['categoria_id' => 'categoria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComposicionNutricionals()
    {
        return $this->hasMany(ComposicionNutricional::className(), ['alimento_id' => 'alimento_id']);
    }
    
    public function getComposicionNutricional()
    {
        return $this->hasOne(ComposicionNutricional::className(), ['alimento_id' => 'alimento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorcionPorAlimentos()
    {
        return $this->hasMany(PorcionPorAlimento::className(), ['alimento_id' => 'alimento_id']);
    }
}
