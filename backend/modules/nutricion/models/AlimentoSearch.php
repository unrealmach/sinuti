<?php

namespace backend\modules\nutricion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\nutricion\models\Alimento;

/**
 * AlimentoSearch represents the model behind the search form about `backend\modules\nutricion\models\Alimento`.
 */
class AlimentoSearch extends Alimento
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alimento_id', 'categoria_id'], 'integer'],
            [['alimento_nombre', 'alimento_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Alimento::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'alimento_id' => $this->alimento_id,
            'categoria_id' => $this->categoria_id,
        ]);

        $query->andFilterWhere(['like', 'alimento_nombre', $this->alimento_nombre])
            ->andFilterWhere(['like', 'alimento_descripcion', $this->alimento_descripcion]);

        return $dataProvider;
    }
}
