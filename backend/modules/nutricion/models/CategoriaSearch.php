<?php

namespace backend\modules\nutricion\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\nutricion\models\Categoria;

/**
 * CategoriaSearch represents the model behind the search form about `backend\modules\nutricion\models\Categoria`.
 */
class CategoriaSearch extends Categoria
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoria_id'], 'integer'],
            [['categoria_nombre', 'categoria_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Categoria::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'categoria_id' => $this->categoria_id,
        ]);

        $query->andFilterWhere(['like', 'categoria_nombre', $this->categoria_nombre])
            ->andFilterWhere(['like', 'categoria_descripcion', $this->categoria_descripcion]);

        return $dataProvider;
    }
}
