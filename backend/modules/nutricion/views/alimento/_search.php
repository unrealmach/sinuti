<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\nutricion\models\AlimentoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alimento-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'alimento_id') ?>

    <?= $form->field($model, 'categoria_id') ?>

    <?= $form->field($model, 'alimento_nombre') ?>

    <?= $form->field($model, 'alimento_descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
