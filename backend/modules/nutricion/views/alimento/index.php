<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\nutricion\models\AlimentoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Alimentos');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="alimento-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Alimento'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                'alimento_id',
                 ['attribute' => 'categoria_id',
                    'value' => 'categoria.categoria_nombre'],
                'alimento_nombre',
                'alimento_descripcion:ntext',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        ?>

    </div>
</div>
