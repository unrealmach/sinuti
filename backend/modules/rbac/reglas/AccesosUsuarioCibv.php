<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace backend\modules\rbac\reglas;

use yii\rbac\Rule;
use backend\modules\centro_infantil\models\AsignacionUserCibv;
/**
 * Comprueba si el usuario es propietario de una institucion para poder administrarlo
 *
 * @author Sofía
 */
class AccesosUsuarioCibv extends Rule
{

    public $name = 'isAuthor';

    public function execute($user, $item, $params)
    {

        if (isset($params['cibv'])) {
            //buscar en asignacionusercibv el cibv segun el parametro de entrada
            //comparar si ese registro esta asignado al usuario
        
           $asign= AsignacionUserCibv::findOne(['centro_infantil_id'=>$params['cibv']->cen_inf_id]);
           
           if(!empty($asign)){
               return($asign->user_id==$user);
           }
           
           
           
            
        }

        return false;
    }
}
