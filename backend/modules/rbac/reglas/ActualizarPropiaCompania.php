<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace backend\modules\rbac\reglas;

use yii\rbac\Rule;

/**
 * Description of ActualizarPropiaCompania
 *
 * @author Mauricio
 */
class ActualizarPropiaCompania extends Rule
{

    public $name = 'isAuthor';

    public function execute($user, $item, $params)
    {
        return isset($params['companie']) ?
            $params['companie']->createdBy === (String) $user :
            false;
    }
}
