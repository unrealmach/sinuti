<?php

namespace backend\modules\rbac\models\activequery;

/**
 * This is the ActiveQuery class for [[\backend\modules\rbac\models\AuthAssignment]].
 *
 * @see \backend\modules\rbac\models\AuthAssignment
 */
class AuthAssignmentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\rbac\models\AuthAssignment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\rbac\models\AuthAssignment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function getOnlyCoordinador()
    {
//        TODO validar cuando sea null
        return $this->where('item_name = "coordinador"');
    }

    /**
     * Busca el rol del usuario deacuerdo a su ID, si no se indica el ID, este toma
     * el usuario con el cual se esta logeado actualmente
     * @param null $user_id
     * @return array|\backend\modules\rbac\models\AuthAssignment|null
     */
    public function getRoleByUser($user_id = null)
    {
        return $this->where("user_id = :user_id")
            ->params([':user_id' => empty($user_id) ? \Yii::$app->user->id:$user_id  ])
            ->one();

    }

}