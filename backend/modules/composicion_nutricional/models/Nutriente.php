<?php
namespace backend\modules\composicion_nutricional\models;

use Yii;

/**
 * This is the model class for table "nutriente".
 *
 * @property integer $nutriente_id
 * @property string $nutriente_nombre
 *
 * @property RangoNutriente[] $rangoNutrientes
 */
class Nutriente extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nutriente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutriente_nombre'], 'required'],
            [['nutriente_nombre'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nutriente_id' => Yii::t('app', 'Nutriente ID'),
            'nutriente_nombre' => Yii::t('app', 'Nutriente Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRangoNutrientes()
    {
        return $this->hasMany(RangoNutriente::className(), ['nutriente_id' => 'nutriente_id']);
    }

    
}
