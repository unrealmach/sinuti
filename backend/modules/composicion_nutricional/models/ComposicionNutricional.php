<?php

namespace backend\modules\composicion_nutricional\models;

use Yii;

/**
 * This is the model class for table "composicion_nutricional".
 *
 * @property integer $com_nut_id
 * @property integer $alimento_id
 * @property string $energia_kcal
 * @property string $proteinas_g
 * @property string $grasa_total_g
 * @property string $carbohidratos_g
 * @property string $fibra_dietetica_g
 * @property string $calcio_mg
 * @property string $fosforo_mg
 * @property string $hierro_mg
 * @property string $tiamina_mg
 * @property string $riboflavina_mg
 * @property string $niacina_mg
 * @property string $vitamina_c_mg
 * @property string $vitamina_a_equiv_retinol_mcg
 * @property string $acidos_grasos_monoinsaturados_g
 * @property string $acidos_grasos_poliinsaturados_g
 * @property string $acidos_grasos_saturados_g
 * @property string $colesterol_mg
 * @property string $potasio_mg
 * @property string $sodio_mg
 * @property string $zinc_mg
 * @property string $magnesio_mg
 * @property string $vitamina_b6_mg
 * @property string $vitamina_b12_mcg
 * @property string $folato_mcg
 *
 * @property Alimento $alimento
 */
class ComposicionNutricional extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'composicion_nutricional';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alimento_id'], 'required'],
            [['alimento_id'], 'integer'],
            [['energia_kcal', 'proteinas_g', 'grasa_total_g', 'carbohidratos_g', 'fibra_dietetica_g', 'calcio_mg', 'fosforo_mg', 'hierro_mg', 'tiamina_mg', 'riboflavina_mg', 'niacina_mg', 'vitamina_c_mg', 'vitamina_a_equiv_retinol_mcg', 'acidos_grasos_monoinsaturados_g', 'acidos_grasos_poliinsaturados_g', 'acidos_grasos_saturados_g', 'colesterol_mg', 'potasio_mg', 'sodio_mg', 'zinc_mg', 'magnesio_mg', 'vitamina_b6_mg', 'vitamina_b12_mcg', 'folato_mcg'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'com_nut_id' => Yii::t('app', 'Com Nut ID'),
            'alimento_id' => Yii::t('app', 'Alimento ID'),
            'energia_kcal' => Yii::t('app', 'Energía Kcal'),
            'proteinas_g' => Yii::t('app', 'Proteínas G'),
            'grasa_total_g' => Yii::t('app', 'Grasa Total G'),
            'carbohidratos_g' => Yii::t('app', 'Carbohidratos G'),
            'fibra_dietetica_g' => Yii::t('app', 'Fibra Dietetica G'),
            'calcio_mg' => Yii::t('app', 'Calcio Mg'),
            'fosforo_mg' => Yii::t('app', 'Fosforo Mg'),
            'hierro_mg' => Yii::t('app', 'Hierro Mg'),
            'tiamina_mg' => Yii::t('app', 'Tiamina Mg'),
            'riboflavina_mg' => Yii::t('app', 'Riboflavina Mg'),
            'niacina_mg' => Yii::t('app', 'Niacina Mg'),
            'vitamina_c_mg' => Yii::t('app', 'Vitamina C Mg'),
            'vitamina_a_equiv_retinol_mcg' => Yii::t('app', 'Vitamina A Equiv Retinol Mcg'),
            'acidos_grasos_monoinsaturados_g' => Yii::t('app', 'Ácidos Grasos Monoinsaturados G'),
            'acidos_grasos_poliinsaturados_g' => Yii::t('app', 'Ácidos Grasos Poliinsaturados G'),
            'acidos_grasos_saturados_g' => Yii::t('app', 'Ácidos Grasos Saturados G'),
            'colesterol_mg' => Yii::t('app', 'Colesterol Mg'),
            'potasio_mg' => Yii::t('app', 'Potasio Mg'),
            'sodio_mg' => Yii::t('app', 'Sodio Mg'),
            'zinc_mg' => Yii::t('app', 'Zinc Mg'),
            'magnesio_mg' => Yii::t('app', 'Magnesio Mg'),
            'vitamina_b6_mg' => Yii::t('app', 'Vitamina B6 Mg'),
            'vitamina_b12_mcg' => Yii::t('app', 'Vitamina B12 Mcg'),
            'folato_mcg' => Yii::t('app', 'Folato Mcg'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlimento()
    {
        return $this->hasOne(Alimento::className(), ['alimento_id' => 'alimento_id']);
    }
}
