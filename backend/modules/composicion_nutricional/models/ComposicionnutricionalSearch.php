<?php

namespace backend\modules\composicion_nutricional\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\composicion_nutricional\models\ComposicionNutricional;

/**
 * ComposicionnutricionalSearch represents the model behind the search form about `backend\modules\composicion_nutricional\models\ComposicionNutricional`.
 */
class ComposicionnutricionalSearch extends ComposicionNutricional
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['com_nut_id', 'alimento_id'], 'integer'],
            [['energia_kcal', 'proteinas_g', 'grasa_total_g', 'carbohidratos_g', 'fibra_dietetica_g', 'calcio_mg', 'fosforo_mg', 'hierro_mg', 'tiamina_mg', 'riboflavina_mg', 'niacina_mg', 'vitamina_c_mg', 'vitamina_a_equiv_retinol_mcg', 'acidos_grasos_monoinsaturados_g', 'acidos_grasos_poliinsaturados_g', 'acidos_grasos_saturados_g', 'colesterol_mg', 'potasio_mg', 'sodio_mg', 'zinc_mg', 'magnesio_mg', 'vitamina_b6_mg', 'vitamina_b12_mcg', 'folato_mcg'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ComposicionNutricional::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'com_nut_id' => $this->com_nut_id,
            'alimento_id' => $this->alimento_id,
            'energia_kcal' => $this->energia_kcal,
            'proteinas_g' => $this->proteinas_g,
            'grasa_total_g' => $this->grasa_total_g,
            'carbohidratos_g' => $this->carbohidratos_g,
            'fibra_dietetica_g' => $this->fibra_dietetica_g,
            'calcio_mg' => $this->calcio_mg,
            'fosforo_mg' => $this->fosforo_mg,
            'hierro_mg' => $this->hierro_mg,
            'tiamina_mg' => $this->tiamina_mg,
            'riboflavina_mg' => $this->riboflavina_mg,
            'niacina_mg' => $this->niacina_mg,
            'vitamina_c_mg' => $this->vitamina_c_mg,
            'vitamina_a_equiv_retinol_mcg' => $this->vitamina_a_equiv_retinol_mcg,
            'acidos_grasos_monoinsaturados_g' => $this->acidos_grasos_monoinsaturados_g,
            'acidos_grasos_poliinsaturados_g' => $this->acidos_grasos_poliinsaturados_g,
            'acidos_grasos_saturados_g' => $this->acidos_grasos_saturados_g,
            'colesterol_mg' => $this->colesterol_mg,
            'potasio_mg' => $this->potasio_mg,
            'sodio_mg' => $this->sodio_mg,
            'zinc_mg' => $this->zinc_mg,
            'magnesio_mg' => $this->magnesio_mg,
            'vitamina_b6_mg' => $this->vitamina_b6_mg,
            'vitamina_b12_mcg' => $this->vitamina_b12_mcg,
            'folato_mcg' => $this->folato_mcg,
        ]);

        return $dataProvider;
    }
}
