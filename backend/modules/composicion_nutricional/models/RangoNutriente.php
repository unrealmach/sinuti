<?php
namespace backend\modules\composicion_nutricional\models;

use Yii;
use backend\modules\metricas_individuo\models\GrupoEdad;
use backend\modules\composicion_nutricional\models\Nutriente;
use backend\modules\preparacion_carta\models\TiempoComida;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "rango_nutriente".
 *
 * @property integer $ran_nut_id
 * @property integer $nutriente_id
 * @property integer $tiempo_comida_id
 * @property integer $grupo_edad_id
 * @property string $ran_nut_valor_min
 * @property string $ran_nut_valor_max
 *
 * @property GrupoEdad $grupoEdad
 * @property Nutriente $nutriente
 * @property TiempoComida $tiempoComida
 */
class RangoNutriente extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rango_nutriente';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutriente_id', 'tiempo_comida_id', 'grupo_edad_id'], 'required'],
            [['nutriente_id', 'tiempo_comida_id', 'grupo_edad_id'], 'integer'],
            [['ran_nut_valor_min', 'ran_nut_valor_max'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ran_nut_id' => Yii::t('app', 'Ran Nut ID'),
            'nutriente_id' => Yii::t('app', 'Nutriente ID'),
            'tiempo_comida_id' => Yii::t('app', 'Tiempo Comida ID'),
            'grupo_edad_id' => Yii::t('app', 'Grupo Edad ID'),
            'ran_nut_valor_min' => Yii::t('app', 'Ran Nut Valor Min'),
            'ran_nut_valor_max' => Yii::t('app', 'Ran Nut Valor Max'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoEdad()
    {
        return $this->hasOne(GrupoEdad::className(), ['grupo_edad_id' => 'grupo_edad_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutriente()
    {
        return $this->hasOne(Nutriente::className(), ['nutriente_id' => 'nutriente_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiempoComida()
    {
        return $this->hasOne(TiempoComida::className(), ['tiem_com_id' => 'tiempo_comida_id']);
    }

    /**
     * Obtiene los rangos de un nutriente de acuerdo al tiempo de comida y grupo de edad
     * @param int $tiempo_comida
     * @param int $grupo_edad
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return dataprovider
     */
    public function obtenerRangNutByTimeEdad($tiempo_comida, $grupo_edad)
    {
        $query = $this->find();
        $query->joinWith('tiempoComida', true, 'INNER JOIN')->all();
        $query->joinWith('grupoEdad', true, 'INNER JOIN')->all();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andFilterWhere([
            'rango_nutriente.tiempo_comida_id' => $tiempo_comida,
            'rango_nutriente.grupo_edad_id' => $grupo_edad,
        ]);

        return $dataProvider->query->all();
    }

    /**
     * Obtiene el valor minimo de los rangos de los nutrientes para la edad
     * @param int $grupo_edad_id id del grupo edad
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return query
     */
    public static function getRangoNutrientesMinDiariosByGrupoEdad($grupo_edad_id)
    {
         $command = Yii::$app->db->createCommand(
                    'SELECT SUM( ran_nut_valor_min ) AS  "minimo_diario", 
                    rango_nutriente.nutriente_id, 
                    nutriente.nutriente_nombre
                    FROM  rango_nutriente 
                    INNER JOIN nutriente ON nutriente.nutriente_id = rango_nutriente.nutriente_id
                    WHERE  grupo_edad_id ='. $grupo_edad_id.'
                    GROUP BY nutriente_nombre,rango_nutriente.nutriente_id');
        $result = $command->queryAll();
        return $result;
    }
}
