<?php

namespace backend\modules\composicion_nutricional\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\composicion_nutricional\models\RangoNutriente;

/**
 * RangonutrienteSearch represents the model behind the search form about `backend\modules\composicion_nutricional\models\RangoNutriente`.
 */
class RangonutrienteSearch extends RangoNutriente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ran_nut_id', 'nutriente_id', 'tiempo_comida_id', 'grupo_edad_id'], 'safe'],
            [['ran_nut_valor_min', 'ran_nut_valor_max'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RangoNutriente::find();
        
         $query->joinWith('nutriente', true, 'INNER JOIN')->all();
         $query->joinWith('tiempoComida', true, 'INNER JOIN')->all();
         $query->joinWith('grupoEdad', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'ran_nut_id' => $this->ran_nut_id,
//            'nutriente_id' => $this->nutriente_id,
//            'tiempo_comida_id' => $this->tiempo_comida_id,
//            'grupo_edad_id' => $this->grupo_edad_id,
            'ran_nut_valor_min' => $this->ran_nut_valor_min,
            'ran_nut_valor_max' => $this->ran_nut_valor_max,
        ]);
        
         $query->andFilterWhere(
                ['like', 'nutriente.nutriente_nombre', $this->nutriente_id])
             ->andFilterWhere(['like', 'tiempo_comida.tiem_com_nombre', $this->tiempo_comida_id])
             ->andFilterWhere(['like', 'grupo_edad.grupo_edad_descripcion', $this->grupo_edad_id]
        );

        return $dataProvider;
    }
}
