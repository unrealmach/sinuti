<?php
namespace backend\modules\composicion_nutricional\models;

use Yii;

/**
 * This is the model class for table "composicion_nutricional_platillo".
 *
 * @property integer $com_nut_platillo_id
 * @property integer $m_platillo_id
 * @property string $energia_kcal
 * @property string $proteinas_g
 * @property string $grasa_total_g
 * @property string $carbohidratos_g
 * @property string $fibra_dietetica_g
 * @property string $calcio_mg
 * @property string $fosforo_mg
 * @property string $hierro_mg
 * @property string $tiamina_mg
 * @property string $riboflavina_mg
 * @property string $niacina_mg
 * @property string $vitamina_c_mg
 * @property string $vitamina_a_equiv_retinol_mcg
 * @property string $acidos_grasos_monoinsaturados_g
 * @property string $acidos_grasos_poliinsaturados_g
 * @property string $acidos_grasos_saturados_g
 * @property string $colesterol_mg
 * @property string $potasio_mg
 * @property string $sodio_mg
 * @property string $zinc_mg
 * @property string $magnesio_mg
 * @property string $vitamina_b6_mg
 * @property string $vitamina_b12_mcg
 * @property string $folato_mcg
 * @property string $es_real
 *
 * @property MPlatillo $mPlatillo
 */
class ComposicionNutricionalPlatillo extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'composicion_nutricional_platillo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['m_platillo_id'], 'required'],
            [['m_platillo_id'], 'integer'],
            [['energia_kcal', 'proteinas_g', 'grasa_total_g', 'carbohidratos_g', 'fibra_dietetica_g', 'calcio_mg', 'fosforo_mg', 'hierro_mg', 'tiamina_mg', 'riboflavina_mg', 'niacina_mg', 'vitamina_c_mg', 'vitamina_a_equiv_retinol_mcg', 'acidos_grasos_monoinsaturados_g', 'acidos_grasos_poliinsaturados_g', 'acidos_grasos_saturados_g', 'colesterol_mg', 'potasio_mg', 'sodio_mg', 'zinc_mg', 'magnesio_mg', 'vitamina_b6_mg', 'vitamina_b12_mcg', 'folato_mcg'], 'number'],
            [['es_real'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'com_nut_platillo_id' => Yii::t('app', 'Com Nut Platillo ID'),
            'm_platillo_id' => Yii::t('app', 'M Platillo ID'),
            'energia_kcal' => Yii::t('app', 'Energía Kcal'),
            'proteinas_g' => Yii::t('app', 'Proteínas G'),
            'grasa_total_g' => Yii::t('app', 'Grasa Total G'),
            'carbohidratos_g' => Yii::t('app', 'Carbohidratos G'),
            'fibra_dietetica_g' => Yii::t('app', 'Fibra Dietetica G'),
            'calcio_mg' => Yii::t('app', 'Calcio Mg'),
            'fosforo_mg' => Yii::t('app', 'Fosforo Mg'),
            'hierro_mg' => Yii::t('app', 'Hierro Mg'),
            'tiamina_mg' => Yii::t('app', 'Tiamina Mg'),
            'riboflavina_mg' => Yii::t('app', 'Riboflavina Mg'),
            'niacina_mg' => Yii::t('app', 'Niacina Mg'),
            'vitamina_c_mg' => Yii::t('app', 'Vitamina C Mg'),
            'vitamina_a_equiv_retinol_mcg' => Yii::t('app', 'Vitamina A Equiv Retinol Mcg'),
            'acidos_grasos_monoinsaturados_g' => Yii::t('app', 'Ácidos Grasos Monoinsaturados G'),
            'acidos_grasos_poliinsaturados_g' => Yii::t('app', 'Ácidos Grasos Poliinsaturados G'),
            'acidos_grasos_saturados_g' => Yii::t('app', 'Ácidos Grasos Saturados G'),
            'colesterol_mg' => Yii::t('app', 'Colesterol Mg'),
            'potasio_mg' => Yii::t('app', 'Potasio Mg'),
            'sodio_mg' => Yii::t('app', 'Sodio Mg'),
            'zinc_mg' => Yii::t('app', 'Zinc Mg'),
            'magnesio_mg' => Yii::t('app', 'Magnesio Mg'),
            'vitamina_b6_mg' => Yii::t('app', 'Vitamina B6 Mg'),
            'vitamina_b12_mcg' => Yii::t('app', 'Vitamina B12 Mcg'),
            'folato_mcg' => Yii::t('app', 'Folato Mcg'),
            'es_real' => Yii::t('app', 'Es Real'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMPlatillo()
    {

        return $this->hasOne(MPlatillo::className(), ['m_platillo_id' => 'm_platillo_id']);
    }

    /**
     * Realiza el cambio de la composicion nutricional para 100g de cada nuevo 
     * platillo, mediante una regla de tres
     *        cantidadTtal      vl
     *         34g ............. 2.8 <-CHO
     *          100g ............ x
     * @param int $m_platillo_id identificador del m_platillo a ser creado
     * @param array $composicionTotal contenedor de la composicionTotal del platillo
     * @param double $cantidadTotal cantidad total del platillo
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return array con los datos necesarios para crear un nuevo modelo de composicion nutricional platillo
     */
    public function changeTo100g($m_platillo_id, $composicionTotal, $cantidadTotal)
    {
        $out = [];
        foreach ($composicionTotal as $valorNutricional => $valor) {
            $out[$valorNutricional] = (100 * $valor) / $cantidadTotal;
        }
        $out['m_platillo_id'] = $m_platillo_id;
        $out['es_real'] = 'NO';
        return$out;
    }
}
