<?php

namespace backend\modules\composicion_nutricional\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\composicion_nutricional\models\Nutriente;

/**
 * NutrienteSearch represents the model behind the search form about `backend\modules\composicion_nutricional\models\Nutriente`.
 */
class NutrienteSearch extends Nutriente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutriente_id'], 'integer'],
            [['nutriente_nombre'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Nutriente::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'nutriente_id' => $this->nutriente_id,
        ]);

        $query->andFilterWhere(['like', 'nutriente_nombre', $this->nutriente_nombre]);

        return $dataProvider;
    }
}
