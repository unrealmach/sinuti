<?php

namespace backend\modules\composicion_nutricional;

class Composicion_nutricional extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\composicion_nutricional\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
