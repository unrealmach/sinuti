<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\composicion_nutricional\models\ComposicionnutricionalplatilloSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Composicion Nutricional Platillos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="composicion-nutricional-platillo-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create Composicion Nutricional Platillo'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

                    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

                        'com_nut_platillo_id',
            'm_platillo_id',
            'energia_kcal',
            'proteinas_g',
            'grasa_total_g',
            // 'carbohidratos_g',
            // 'fibra_dietetica_g',
            // 'calcio_mg',
            // 'fosforo_mg',
            // 'hierro_mg',
            // 'tiamina_mg',
            // 'riboflavina_mg',
            // 'niacina_mg',
            // 'vitamina_c_mg',
            // 'vitamina_a_equiv_retinol_mcg',
            // 'acidos_grasos_monoinsaturados_g',
            // 'acidos_grasos_poliinsaturados_g',
            // 'acidos_grasos_saturados_g',
            // 'colesterol_mg',
            // 'potasio_mg',
            // 'sodio_mg',
            // 'zinc_mg',
            // 'magnesio_mg',
            // 'vitamina_b6_mg',
            // 'vitamina_b12_mcg',
            // 'folato_mcg',
            // 'es_real',

            ['class' => 'yii\grid\ActionColumn'],
            ],
            ]); ?>
        
    </div>
</div>
