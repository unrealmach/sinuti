<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\ComposicionNutricional */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="composicion-nutricional-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'alimento_id', 
                        ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
                        ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'energia_kcal', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'proteinas_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'grasa_total_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'carbohidratos_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'fibra_dietetica_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'calcio_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'fosforo_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'hierro_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'tiamina_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'riboflavina_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'niacina_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'vitamina_c_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'vitamina_a_equiv_retinol_mcg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'acidos_grasos_monoinsaturados_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'acidos_grasos_poliinsaturados_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'acidos_grasos_saturados_g', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'colesterol_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'potasio_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'sodio_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'zinc_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'magnesio_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'vitamina_b6_mg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'vitamina_b12_mcg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    <?= $form->field($model, 'folato_mcg', [
            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null,['class'=>'col-sm-2 control-label']) ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
            <center>
                <?= Html::submitButton($model->isNewRecord ? Html::tag('i','',['class'=>'fa fa-fw fa-plus']).Yii::t('app', 'Create') : Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','style'=>"padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
            </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
