<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\ComposicionNutricional */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Composicion Nutricional',
]) . ' ' . $model->com_nut_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Composicion Nutricionals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->com_nut_id, 'url' => ['view', 'id' => $model->com_nut_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="composicion-nutricional-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
