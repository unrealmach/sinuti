<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\ComposicionNutricional */

$this->title = $model->com_nut_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Composicion Nutricionals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="composicion-nutricional-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->com_nut_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->com_nut_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'com_nut_id',
            'alimento_id',
            'energia_kcal',
            'proteinas_g',
            'grasa_total_g',
            'carbohidratos_g',
            'fibra_dietetica_g',
            'calcio_mg',
            'fosforo_mg',
            'hierro_mg',
            'tiamina_mg',
            'riboflavina_mg',
            'niacina_mg',
            'vitamina_c_mg',
            'vitamina_a_equiv_retinol_mcg',
            'acidos_grasos_monoinsaturados_g',
            'acidos_grasos_poliinsaturados_g',
            'acidos_grasos_saturados_g',
            'colesterol_mg',
            'potasio_mg',
            'sodio_mg',
            'zinc_mg',
            'magnesio_mg',
            'vitamina_b6_mg',
            'vitamina_b12_mcg',
            'folato_mcg',
        ],
        ]) ?>

    </div>
</div>
