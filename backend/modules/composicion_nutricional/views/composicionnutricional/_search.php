<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\ComposicionnutricionalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="composicion-nutricional-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'com_nut_id') ?>

    <?= $form->field($model, 'alimento_id') ?>

    <?= $form->field($model, 'energia_kcal') ?>

    <?= $form->field($model, 'proteinas_g') ?>

    <?= $form->field($model, 'grasa_total_g') ?>

    <?php // echo $form->field($model, 'carbohidratos_g') ?>

    <?php // echo $form->field($model, 'fibra_dietetica_g') ?>

    <?php // echo $form->field($model, 'calcio_mg') ?>

    <?php // echo $form->field($model, 'fosforo_mg') ?>

    <?php // echo $form->field($model, 'hierro_mg') ?>

    <?php // echo $form->field($model, 'tiamina_mg') ?>

    <?php // echo $form->field($model, 'riboflavina_mg') ?>

    <?php // echo $form->field($model, 'niacina_mg') ?>

    <?php // echo $form->field($model, 'vitamina_c_mg') ?>

    <?php // echo $form->field($model, 'vitamina_a_equiv_retinol_mcg') ?>

    <?php // echo $form->field($model, 'acidos_grasos_monoinsaturados_g') ?>

    <?php // echo $form->field($model, 'acidos_grasos_poliinsaturados_g') ?>

    <?php // echo $form->field($model, 'acidos_grasos_saturados_g') ?>

    <?php // echo $form->field($model, 'colesterol_mg') ?>

    <?php // echo $form->field($model, 'potasio_mg') ?>

    <?php // echo $form->field($model, 'sodio_mg') ?>

    <?php // echo $form->field($model, 'zinc_mg') ?>

    <?php // echo $form->field($model, 'magnesio_mg') ?>

    <?php // echo $form->field($model, 'vitamina_b6_mg') ?>

    <?php // echo $form->field($model, 'vitamina_b12_mcg') ?>

    <?php // echo $form->field($model, 'folato_mcg') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
