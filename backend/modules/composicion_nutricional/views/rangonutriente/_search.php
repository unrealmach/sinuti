<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\RangonutrienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rango-nutriente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ran_nut_id') ?>

    <?= $form->field($model, 'nutriente_id') ?>

    <?= $form->field($model, 'tiempo_comida_id') ?>

    <?= $form->field($model, 'grupo_edad_id') ?>

    <?= $form->field($model, 'ran_nut_valor_min') ?>

    <?php // echo $form->field($model, 'ran_nut_valor_max') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
