<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\composicion_nutricional\models\RangonutrienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rango Nutrientes');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="rango-nutriente-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Rango Nutriente'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'ran_nut_id',
                [
                    'attribute' => 'nutriente_id',
                    'value' => 'nutriente.nutriente_nombre'
                ],
                [
                    'attribute' => 'tiempo_comida_id',
                    'value' => 'tiempoComida.tiem_com_nombre'
                ],
                [
                    'attribute' => 'grupo_edad_id',
                    'value' => 'grupoEdad.grupo_edad_descripcion'
                ],
                'ran_nut_valor_min',
                'ran_nut_valor_max',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);

        ?>

    </div>
</div>
