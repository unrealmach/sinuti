<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\RangoNutriente */

$this->title = Yii::t('app', 'Create Rango Nutriente');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rango Nutrientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rango-nutriente-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
