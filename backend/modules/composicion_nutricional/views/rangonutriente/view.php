<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\composicion_nutricional\models\RangoNutriente */

$this->title = $model->ran_nut_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rango Nutrientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rango-nutriente-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->ran_nut_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->ran_nut_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'ran_nut_id',
            'nutriente.nutriente_nombre',
            'tiempoComida.tiem_com_nombre',
            'grupoEdad.grupo_edad_descripcion',
            'ran_nut_valor_min',
            'ran_nut_valor_max',
        ],
        ]) ?>

    </div>
</div>
