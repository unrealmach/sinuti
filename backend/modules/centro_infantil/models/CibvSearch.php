<?php

namespace backend\modules\centro_infantil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;
use \backend\modules\recursos_humanos\models\AutoridadesCibv;
use \backend\modules\rbac\models\AuthAssignment;
use yii\web\NotFoundHttpException;

/**
 * CibvSearch represents the model behind the search form about `backend\modules\centro_infantil\models\Cibv`.
 */
class CibvSearch extends Cibv
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cen_inf_id'], 'integer'],
            [['cen_inf_nombre', 'cen_inf_direccion', 'cen_inf_telefono', 'cen_inf_correo', 'cen_inf_activo',
                'parroquia_id', 'autoridad_cibv'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {


        $query = Cibv::find();


        $query->joinWith('parroquia', true, 'LEFT JOIN')->all();
        $query->joinWith('autoridadCibv.coordinador', true, 'LEFT JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $authAsiignmentMdel = AuthAssignment::find()->getRoleByUser();

        switch ($authAsiignmentMdel->item_name) {
            case "coordinador":

                $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()
                    ->deUserAndPeriodo(Yii::$app->user->id)->one();


                if (!empty($asignacionCoordinadorCibv)) {
                    $periodo = Periodo::find()->periodoActivo()->one();
                    $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id, 'periodo_id' => $periodo->periodo_id]);
                    if (!empty($autoridadCibv)) {
                        $query->where(['=', 'cibv.cen_inf_id', $autoridadCibv->cen_inf_id]);
                    } else {
                        throw new NotFoundHttpException('No tiene asignado un centro infantil para el periodo actual');
                        //$query->where(['cibv.cen_inf_id' => 0]);
                    }
                } else {
                    //si no se ha asignado un coordinador, entonces muestra la lista vacia
                    //  $query->where(['cibv.cen_inf_id' => 0]);
                    throw new NotFoundHttpException('No tiene asignado un centro infantil para el periodo actual');
                };
                break;
            case 'admin':
                break;
            case  'superadmin':
                break;
            case 'coordinador-gad':
                break;
            default :
                throw new NotFoundHttpException('No tiene permisos para esto');
        }


        $query->andFilterWhere(['like', 'cen_inf_nombre', strtoupper($this->cen_inf_nombre)])
            ->andFilterWhere(['like', 'parroquia.parroquia_nombre', strtoupper($this->parroquia_id)])
            ->andFilterWhere(['like',
                new \yii\db\Expression("CONCAT(coordinador_cibv.coordinador_nombres, ' ' , coordinador_cibv.coordinador_apellidos)"),
                strtoupper($this->autoridad_cibv)])
            ->andFilterWhere(['like', 'cen_inf_direccion', strtoupper($this->cen_inf_direccion)])
            ->andFilterWhere(['like', 'cen_inf_telefono', strtoupper($this->cen_inf_telefono)])
            ->andFilterWhere(['like', 'cen_inf_correo', $this->cen_inf_correo])
            ->andFilterWhere(['like', 'cen_inf_activo', $this->cen_inf_activo]);

        // var_dump($query->createCommand()->getRawSql());die();

        return $dataProvider;
    }

}
