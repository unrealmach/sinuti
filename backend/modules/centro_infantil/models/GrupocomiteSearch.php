<?php

namespace backend\modules\centro_infantil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\GrupoComite;

/**
 * GrupocomiteSearch represents the model behind the search form about `backend\modules\centro_infantil\models\GrupoComite`.
 */
class GrupocomiteSearch extends GrupoComite
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grupo_comite_id'], 'integer'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad', 'periodo_id', 'cen_inf_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GrupoComite::find();
        
        $query->joinWith('cenInf', true, 'INNER JOIN')->all();
        $query->joinWith('periodo', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'grupo_comite_id' => $this->grupo_comite_id,
            'fecha_inicio_actividad' => $this->fecha_inicio_actividad,
            'fecha_fin_actividad' => $this->fecha_fin_actividad,
//            'periodo_id' => $this->periodo_id,
//            'cen_inf_id' => $this->cen_inf_id,
        ]);
        
          $query->andFilterWhere(
            ['like', 'cibv.cen_inf_nombre', $this->cen_inf_id]) 
    ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(periodo.fecha_inicio, " - ", periodo.fecha_fin)'), $this->periodo_id]
        );

        return $dataProvider;
    }
}
