<?php
namespace backend\modules\centro_infantil\models;

use \backend\modules\recursos_humanos\models\AutoridadesCibv;
use common\models\User;
use Yii;

/**
 * This is the model class for table "asignacion_user_coordinador_cibv".
 *
 * @property integer $asignacion_user_cibv_id
 * @property integer $user_id
 * @property integer $autoridades_cibv_id
 * @property string $fecha
 * @property string $observaciones
 */
class AsignacionUserCoordinadorCibv extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_user_coordinador_cibv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'autoridades_cibv_id', 'fecha', 'observaciones'], 'required'],
            [['user_id', 'autoridades_cibv_id'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asignacion_user_cibv_id' => Yii::t('app', 'Asignacion User Cibv ID'),
            'user_id' => Yii::t('app', 'Usuario'),
            'autoridades_cibv_id' => Yii::t('app', 'Autoridades Cibv'),
            'fecha' => Yii::t('app', 'Fecha'),
            'observaciones' => Yii::t('app', 'Observaciones'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoridadCibv()
    {
        return $this->hasOne(AutoridadesCibv::className(), ['autoridades_cibv_id' => 'autoridades_cibv_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\centro_infantil\models\activequeries\AsignacionUserCoordinadorCibvQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\centro_infantil\models\activequeries\AsignacionUserCoordinadorCibvQuery(get_called_class());
    }

    /**
     * Crea  un nuevo registro de un coordinador asignado a un centro infantil
     * @param int $user_id
     * @param int $autoridad_cibv_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public static function insertRegistro($user_id, $autoridad_cibv_id)
    {
        $modelAsignacionUserCoordinadorCibv = new AsignacionUserCoordinadorCibv();
        $modelAsignacionUserCoordinadorCibv->user_id = $user_id;
        $modelAsignacionUserCoordinadorCibv->autoridades_cibv_id = $autoridad_cibv_id;
        $modelAsignacionUserCoordinadorCibv->fecha = date("Y-m-d");
        $modelAsignacionUserCoordinadorCibv->observaciones = "Asignacion coordinador al cibv";
        if ($modelAsignacionUserCoordinadorCibv->save()) {
            Yii::$app->getSession()->setFlash('success', 'coordinador asignado al centro infantil');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo asignar el coordinador al centro infantil');
        }
    }
}
