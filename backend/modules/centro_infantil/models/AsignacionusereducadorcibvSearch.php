<?php

namespace backend\modules\centro_infantil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\AsignacionUserEducadorCibv;

/**
 * AsignacionusereducadorcibvSearch represents the model behind the search form about `backend\modules\centro_infantil\models\AsignacionUserEducadorCibv`.
 */
class AsignacionusereducadorcibvSearch extends AsignacionUserEducadorCibv {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['asignacion_user_educador_cibv_id'], 'integer'],
            [['fecha', 'observaciones', 'user_id', 'sector_asignado_educador_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AsignacionUserEducadorCibv::find();
        
         $query->joinWith('user', true, 'INNER JOIN')->all();
        $query->joinWith('sectorAsignadoEducador.educador', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asignacion_user_educador_cibv_id' => $this->asignacion_user_educador_cibv_id,
            //'user_id' => $this->user_id,
            //'sector_asignado_educador_id' => $this->sector_asignado_educador_id,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);
        
        $query->andFilterWhere(
                ['like', 'user.username', $this->user_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(educador.educador_nombres, " ", educador.educador_apellidos)'),
                $this->sector_asignado_educador_id]);

        return $dataProvider;
    }

}
