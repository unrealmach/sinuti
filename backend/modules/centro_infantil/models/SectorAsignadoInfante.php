<?php

namespace backend\modules\centro_infantil\models;
use backend\modules\individuo\models\Infante;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;

use Yii;

/**
 * This is the model class for table "sector_asignado_infante".
 *
 * @property integer $sector_asignado_infante_id
 * @property integer $infante_id
 * @property integer $sector_asignado_id
 *
 * @property Infante $infante
 * @property SectorAsignadoEducador $sectorAsignado
 */
class SectorAsignadoInfante extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sector_asignado_infante';
    }
    
    public $salon; 

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['infante_id', 'sector_asignado_id'], 'required'],
            [['infante_id', 'sector_asignado_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sector_asignado_infante_id' => Yii::t('app', 'Sector Asignado Infante ID'),
            'infante_id' => Yii::t('app', 'Infante'),
            'sector_asignado_id' => Yii::t('app', 'Educador - Salón Infantil'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfante()
    {
        return $this->hasOne(Infante::className(), ['infante_id' => 'infante_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignado()
    {
        return $this->hasOne(SectorAsignadoEducador::className(), ['sector_asignado_educador_id' => 'sector_asignado_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\activequeries\SectorAsignadoInfanteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\centro_infantil\models\activequeries\SectorAsignadoInfanteQuery(get_called_class());
    }
}
