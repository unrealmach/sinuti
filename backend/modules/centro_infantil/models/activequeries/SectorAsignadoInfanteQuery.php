<?php

namespace backend\modules\centro_infantil\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\centro_infantil\models\SectorAsignadoInfante]].
 *
 * @see \backend\modules\centro_infantil\models\SectorAsignadoInfante
 */
class SectorAsignadoInfanteQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\SectorAsignadoInfante[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\SectorAsignadoInfante|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Busca el sector asignado por el id
     * @param int $sector_asig_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deSalonAsignado($sector_asig_id) {
        return $this->where(['sector_asignado_id' => $sector_asig_id]);
    }

    /**
     * Busca el infante de un sector asignado validando si el periodo esta activo
     * @param int $infante_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return \backend\modules\centro_infantil\models\activequeries\SectorAsignadoInfanteQuery
     */
    public function InfanteSalonAsignado($infante_id) {
        $periodo_actual = Periodo::find()->periodoActivo()->one();
        if ($periodo_actual) {
            
            $this->join('left join', 'sector_asignado_educador', 'sector_asignado_infante.sector_asignado_id = sector_asignado_educador.sector_asignado_educador_id');
            $this->where('sector_asignado_educador.periodo_id = :periodo_id')
                    ->andWhere('sector_asignado_infante.infante_id = :infante_id')
                    ->addParams([':periodo_id' => $periodo_actual->periodo_id, ':infante_id' => $infante_id]);
//            var_dump($this->createCommand()->getRawSql()); die();
            return $this;
        } else {
            $this->join('left join', 'sector_asignado_educador', 'sector_asignado_infante.sector_asignado_id = sector_asignado_educador.sector_asignado_educador_id')
                    ->where('sector_asignado_infante.infante_id = :infante_id')
                    ->addParams([':infante_id' => $infante_id]);
            return $this;
        }
    }

    /**
     * Obtiene la lista de infantes para un educador
     * @param int $sector_asig_id
     * @author Mauricio Chamorro <unrealmach@hotmail.com>
     * @return \backend\modules\centro_infantil\models\activequeries\SectorAsignadoInfanteQuery
     */
    public function infanteEducadorAsignado($sector_asig_id) {
        $this->join('inner join', 'sector_asignado_educador', 'sector_asignado_educador.sector_asignado_educador_id = sector_asignado_infante.sector_asignado_id');
        $this->join('left join', 'matricula',  'matricula.infante_id = sector_asignado_infante.infante_id' );
        $this->where('sector_asignado_infante.sector_asignado_id = :sector_asignado_id AND matricula.matricula_estado = :estado')
                ->addParams([':sector_asignado_id' => $sector_asig_id, ':estado' => 'ACTIVO']); 
        return $this;
    }

}
