<?php

namespace backend\modules\centro_infantil\models\activequeries;

/**
 * This is the ActiveQuery class for [[\backend\modules\centro_infantil\models\Cibv]].
 *
 * @see \backend\modules\centro_infantil\models\Cibv
 */
class CibvQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\Cibv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\Cibv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
    
    /**
     * Busca todos los cibv que no tengan asignado un coordinador
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return \backend\modules\centro_infantil\models\activequeries\CibvQuery
     */
    public function cibvDisponibles(){
        $this->join('left join', 'autoridades_cibv', 'cibv.cen_inf_id = autoridades_cibv.cen_inf_id');
        $this->where('autoridades_cibv.cen_inf_id IS NULL');
        return $this;
    }
}