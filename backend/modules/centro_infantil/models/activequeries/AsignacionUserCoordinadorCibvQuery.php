<?php

namespace backend\modules\centro_infantil\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv]].
 *
 * @see \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv
 */
class AsignacionUserCoordinadorCibvQuery extends \yii\db\ActiveQuery
{
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Busca por el id del usuario
     * @param int $user_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deUserId($user_id)
    {
        return $this->where('user_id = :user_id')->addParams([':user_id' => $user_id]);
    }

    /**
     * Busca de acuerdo al id del coordinador del cibv
     * @param $int $coordinador_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deCoordinadorId($coordinador_id)
    {
        return $this->where('coordinador_cibv_id = :coordinador_id')->addParams([':coordinador_id' => $coordinador_id]);
    }

    /**
     * devuelve un usuario para el periodo activo
     * @param int $user_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deUserAndPeriodo($user_id=null)
    {
        $periodo = Periodo::find()->periodoActivo()->one();

        if (!empty($periodo)) {
            $this->where('user_id = :user_id AND fecha BETWEEN :fecha_inicio AND :fecha_fin')
                ->addParams([':user_id' => empty($user_id) ? \Yii::$app->user->id : $user_id, ':fecha_inicio' => $periodo->fecha_inicio, ':fecha_fin' => $periodo->fecha_fin]);
        } else {
            $this->where('user_id = :user_id AND fecha BETWEEN :fecha_inicio AND :fecha_fin')
                ->addParams([':user_id' => empty($user_id) ? \Yii::$app->user->id : $user_id, ':fecha_inicio' => null, ':fecha_fin' => null]);
        }

        //var_dump($this->createCommand()->getRawSql());die();
        return $this;
    }

    /**
     * Devuelve la asignacion con periodo Activo del usuario coordinador de su respectivo CIbv
     * @param null $user_id
     * @return mixed
     */
    public function getOneAsignacionUserCoordinadorCibv($user_id=null){

        return $this->deUserAndPeriodo(empty($user_id) ? \Yii::$app->user->id : $user_id)->one();
    }

    /**
     * Devuelve todas las asignaciones con periodo Activo del usuario  coordinador de su respectivo CIBV
     * @param null $user_id
     * @return mixed
     */
    public function getAllAsignacionUserCoordinadorCibv($user_id=null){

        return $this->deUserAndPeriodo(empty($user_id) ? \Yii::$app->user->id : $user_id)->one();
    }

}
