<?php

namespace backend\modules\centro_infantil\models\activequeries;
use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\centro_infantil\models\GrupoComite]].
 *
 * @see \backend\modules\centro_infantil\models\GrupoComite
 */
class GrupoComiteQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\GrupoComite[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\GrupoComite|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Busca por el cibv y el periodo
     * @param int $cen_inf_id
     * @param int $periodo_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deCibvAndPeriodo($cen_inf_id, $periodo_id) {
        return $this->where(['cen_inf_id' => $cen_inf_id, 'periodo_id' => $periodo_id]);
    }

    public function dePeriodoActivo() {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where(['periodo_id' => $periodo->periodo_id]);
        } else {
            return $this->where(['periodo_id' => null]);
        }
    }

}
