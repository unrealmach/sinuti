<?php

namespace backend\modules\centro_infantil\models\activequeries;

use backend\modules\parametros_sistema\models\Periodo;

/**
 * This is the ActiveQuery class for [[\backend\modules\centro_infantil\models\AsignacionUserEducadorCibv]].
 *
 * @see \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv
 */
class AsignacionUserEducadorCibvQuery extends \yii\db\ActiveQuery {
    /* public function active()
      {
      $this->andWhere('[[status]]=1');
      return $this;
      } */

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\modules\centro_infantil\models\AsignacionUserEducadorCibv|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }

    /**
     * Busca los sectores asignados de los educadores validando si el periodo 
     * esta activo
     * @param int $user_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function deEducadorAsignadoActivo($user_id=null) {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            return $this->where('user_id = :user_id AND fecha BETWEEN :fecha_inicio AND :fecha_fin')
                            ->addParams([':user_id' => is_null($user_id)?\Yii::$app->user->id:$user_id, ':fecha_inicio' => $periodo->fecha_inicio, ':fecha_fin' => $periodo->fecha_fin]);
        } else {
            return $this->where('user_id = :user_id AND fecha BETWEEN :fecha_inicio AND :fecha_fin')
                            ->addParams([':user_id' => is_null($user_id)?\Yii::$app->user->id:$user_id, ':fecha_inicio' => null, ':fecha_fin' => null]);
        }
    }

    /**
     * Devuelve un registro del educador activo
     * @param null $user_id
     * @return mixed
     */
    public function getOneEducadorAsignadoActivo($user_id=null){
     return $this->deEducadorAsignadoActivo($user_id)->one();
    }

    
    /**
     * Busca por el id del usuario
     * @param int $user_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return query
     */
    public function byUserId($user_id){
         return $this->where('user_id = :user_id')
                            ->addParams([':user_id' => $user_id]);
    }

}
