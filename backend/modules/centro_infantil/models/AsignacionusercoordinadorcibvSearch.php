<?php

namespace backend\modules\centro_infantil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;

/**
 * AsignacionusercoordinadorcibvSearch represents the model behind the search form about `backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv`.
 */
class AsignacionusercoordinadorcibvSearch extends AsignacionUserCoordinadorCibv {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['asignacion_user_cibv_id'], 'integer'],
            [['fecha', 'observaciones', 'user_id', 'autoridades_cibv_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = AsignacionUserCoordinadorCibv::find();
        $query->joinWith('user', true, 'INNER JOIN')->all();
        $query->joinWith('autoridadCibv.coordinador', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'asignacion_user_cibv_id' => $this->asignacion_user_cibv_id,
           // 'user_id' => $this->user_id,
           // 'autoridades_cibv_id' => $this->autoridades_cibv_id,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'observaciones', $this->observaciones]);
        
        $query->andFilterWhere(
                ['like', 'user.username', $this->user_id])
                ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(coordinador_cibv.coordinador_nombres, " ", coordinador_cibv.coordinador_apellidos)'),
                $this->autoridades_cibv_id]);

        return $dataProvider;
    }

}
