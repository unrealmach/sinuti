<?php
namespace backend\modules\centro_infantil\models;
use webvimark\modules\UserManagement\models\User;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;

use Yii;

/**
 * This is the model class for table "asignacion_user_educador_cibv".
 *
 * @property integer $asignacion_user_educador_cibv_id
 * @property integer $user_id
 * @property integer $sector_asignado_educador_id
 * @property string $fecha
 * @property string $observaciones
 */
class AsignacionUserEducadorCibv extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asignacion_user_educador_cibv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sector_asignado_educador_id', 'fecha', 'observaciones'], 'required'],
            [['user_id', 'sector_asignado_educador_id'], 'integer'],
            [['fecha'], 'safe'],
            [['observaciones'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asignacion_user_educador_cibv_id' => Yii::t('app', 'Asignacion User Educador Cibv ID'),
            'user_id' => Yii::t('app', 'Usuario'),
            'sector_asignado_educador_id' => Yii::t('app', 'Sector Asignado Educador'),
            'fecha' => Yii::t('app', 'Fecha'),
            'observaciones' => Yii::t('app', 'Observaciones'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignadoEducador()
    {
        return $this->hasOne(SectorAsignadoEducador::className(), ['sector_asignado_educador_id' => 'sector_asignado_educador_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\centro_infantil\models\activequeries\AsignacionUserEducadorCibvQuery the active query used by this AR class. 
     */
    public static function find()
    {
        return new \backend\modules\centro_infantil\models\activequeries\AsignacionUserEducadorCibvQuery(get_called_class());
    }

    /**
     * Crea un nuevo registro de un salon asignado a un educador
     * @param int $user_id
     * @param int $sector_asig_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     */
    public static function insertRegistro($user_id, $sector_asig_id)
    {
        $asigUserEducadorCibv = new AsignacionUserEducadorCibv();
        $asigUserEducadorCibv->sector_asignado_educador_id = $sector_asig_id;
        $asigUserEducadorCibv->user_id = $user_id;
        $asigUserEducadorCibv->fecha = date('Y-m-d');
        $asigUserEducadorCibv->observaciones = "Asignacion educador al cibv";
        if ($asigUserEducadorCibv->save()) {
            Yii::$app->getSession()->setFlash('success', 'sector asignado al educador');
        } else {
            Yii::$app->getSession()->setFlash('error', 'No se pudo asignar el sector al educador');
        }
    }
}
