<?php

namespace backend\modules\centro_infantil\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\centro_infantil\models\SectorAsignadoInfante;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;
use backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv;

/**
 * SectorasignadoinfanteSearch represents the model behind the search form about `backend\modules\centro_infantil\models\SectorAsignadoInfante`.
 */
class SectorasignadoinfanteSearch extends SectorAsignadoInfante
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['sector_asignado_infante_id'], 'integer'],
                [['infante_id', 'sector_asignado_id'], 'string'],
                [['salon'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SectorAsignadoInfante::find();

        $query->joinWith('infante', true, 'INNER JOIN')->all();
        $query->joinWith('sectorAsignado.educador', true, 'INNER JOIN')->all();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $authAsiignmentMdel = \backend\modules\rbac\models\AuthAssignment::find()->where([
                'user_id' => Yii::$app->user->id])->one();

        switch ($authAsiignmentMdel->item_name) {
            case 'coordinador': //
                $arrayIds = $this->getCoincidenciasCoordinador();
                $query->andWhere(['IN', 'sector_asignado_infante.sector_asignado_infante_id',
                    $arrayIds]);
                break;
            case 'educador'://
                $arrayIds = $this->getCoincidenciasEducador();
                $query->andWhere(['IN', 'sector_asignado_infante.sector_asignado_infante_id',
                    $arrayIds]);
                break;
            case 'Admin'||'coordinador-gad'://
                $arrayIds = $this->getCoincidenciasAdministrador();
                $query->andWhere(['IN', 'sector_asignado_infante.sector_asignado_infante_id',
                    $arrayIds]);
                break;

//            case 'coordinador-gad':
//      TODO: completar para el coordinador-gad
//                ;
            default :throw new \yii\web\NotFoundHttpException('No tiene permisos para esto');
        }


        $query->andFilterWhere([
            'sector_asignado_infante_id' => $this->sector_asignado_infante_id,
//            'infante_id' => $this->infante_id,
//            'sector_asignado_id' => $this->sector_asignado_id,
        ]);

        $query->andFilterWhere(
                ['like', new \yii\db\Expression('CONCAT(infante.infante_nombres, " ", infante.infante_apellidos)'),
                    $this->infante_id])
            ->andFilterWhere(['like', new \yii\db\Expression('CONCAT(educador.educador_nombres, " ", educador.educador_apellidos)'),
                $this->sector_asignado_id]
        );

        return $dataProvider;
    }

    private function getCoincidenciasCoordinador()
    {
        $arrayIds = [];

        $asignacionCoordinadorCibv = AsignacionUserCoordinadorCibv::find()->deUserAndPeriodo(Yii::$app->user->id)->one();
        if (!empty($asignacionCoordinadorCibv)) {
            $autoridadCibv = AutoridadesCibv::findOne(['autoridades_cibv_id' => $asignacionCoordinadorCibv->autoridades_cibv_id]);
            if (!empty($autoridadCibv)) {
                $asignacionEducador = SectorAsignadoEducador::find()->where([
                        'cen_inf_id' => $autoridadCibv->cen_inf_id])->all();
                if (!empty($asignacionEducador)) {
                    foreach ($asignacionEducador as $value) {
                        $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($value->sector_asignado_educador_id)->all();
                        if (!empty($sectorAsignadoInfante)) {
                            foreach ($sectorAsignadoInfante as $value2) {
                                array_push($arrayIds,
                                    $value2->sector_asignado_infante_id);
                            }
                        }
                    }
                }
            }
        }

        return $arrayIds;
    }

    private function getCoincidenciasEducador()
    {
        $arrayIds               = [];
        $asignacionEducadorCibv = AsignacionUserEducadorCibv::find()->deEducadorAsignadoActivo(Yii::$app->user->id)->one();
        if (!empty($asignacionEducadorCibv)) {
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->deSalonAsignado($asignacionEducadorCibv->sector_asignado_educador_id)->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->sector_asignado_infante_id);
                }
            }
        }

        return $arrayIds;
    }
    private function getCoincidenciasAdministrador()
    {
        $arrayIds               = [];
            $sectorAsignadoInfante = SectorAsignadoInfante::find()->all();
            if (!empty($sectorAsignadoInfante)) {
                foreach ($sectorAsignadoInfante as $value) {
                    array_push($arrayIds, $value->sector_asignado_infante_id);
                }
            }

        return $arrayIds;
    }
}