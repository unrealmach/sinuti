<?php

namespace backend\modules\centro_infantil\models;
use backend\modules\inscripcion\models\Matricula;
use backend\modules\ubicacion\models\Parroquia;
use backend\modules\recursos_humanos\models\AutoridadesCibv;

use Yii;

/**
 * This is the model class for table "cibv".
 *
 * @property integer $cen_inf_id
 * @property string $cen_inf_nombre
 * @property string $cen_inf_direccion
 * @property string $cen_inf_telefono
 * @property string $cen_inf_correo
 * @property string $cen_inf_activo
 *
 * @property AutoridadesCibv[] $autoridadesCibvs
 * @property GrupoComite[] $grupoComites
 * @property MCartaSemanal[] $mCartaSemanals
 * @property Matricula[] $matriculas
 * @property SectorAsignadoEducador[] $sectorAsignadoEducadors
 */
class Cibv extends \yii\db\ActiveRecord
{
    public $autoridad_cibv;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cibv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parroquia_id','cen_inf_nombre', 'cen_inf_direccion', 'cen_inf_telefono', 'cen_inf_correo'], 'required'],
            [['cen_inf_activo','autoridad_cibv'], 'string'],
            [['cen_inf_nombre'], 'string', 'max' => 150],
            [['cen_inf_direccion'], 'string', 'max' => 300],
            [['cen_inf_telefono'], 'string', 'max' => 15],
            [['cen_inf_correo'], 'string', 'max' => 100],
            [['cen_inf_nombre'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cen_inf_id' => Yii::t('app', 'Cen Inf ID'),
            'cen_inf_nombre' => Yii::t('app', 'Nombre'),
            'cen_inf_direccion' => Yii::t('app', 'Dirección'),
            'cen_inf_telefono' => Yii::t('app', 'Teléfono'),
            'cen_inf_correo' => Yii::t('app', 'Correo'),
            'cen_inf_activo' => Yii::t('app', 'Activo'),
            'parroquia_id' => Yii::t('app', 'Parroquia'),
            'autoridad_cibv' =>  Yii::t('app', 'Coordinador')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoridadesCibvs()
    {
        return $this->hasMany(AutoridadesCibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }
    public function getAutoridadCibv()
    {
        return $this->hasOne(AutoridadesCibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupoComites()
    {
        return $this->hasMany(GrupoComite::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMCartaSemanals()
    {
        return $this->hasMany(MCartaSemanal::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculas()
    {
        return $this->hasMany(Matricula::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectorAsignadoEducadors()
    {
       return $this->hasMany(SectorAsignadoEducador::className(), ['cen_inf_id' => 'cen_inf_id']);
    }
    
       /** 
    * @return \yii\db\ActiveQuery 
    */ 
   public function getParroquia() 
   { 
       return $this->hasOne(Parroquia::className(), ['parroquia_id' => 'parroquia_id']); 
   }
   
    /* @inheritdoc 
     * @return \backend\modules\centro_infantil\models\activequeries\CibvQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\centro_infantil\models\activequeries\CibvQuery(get_called_class());
    }
}
