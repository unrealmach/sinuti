<?php
namespace backend\modules\centro_infantil\models;

use Yii;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;
use backend\modules\recursos_humanos\models\ComitePadresFlia;

/**
 * This is the model class for table "grupo_comite".
 *
 * @property integer $grupo_comite_id
 * @property string $fecha_inicio_actividad
 * @property string $fecha_fin_actividad
 * @property integer $periodo_id
 * @property integer $cen_inf_id
 *
 * @property ComitePadresFlia[] $comitePadresFlias
 * @property Cibv $cenInf
 * @property Periodo $periodo
 */
class GrupoComite extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grupo_comite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha_inicio_actividad','fecha_fin_actividad', 'periodo_id', 'cen_inf_id'], 'required'],
            [['fecha_inicio_actividad', 'fecha_fin_actividad'], 'safe'],
            [['periodo_id', 'cen_inf_id'], 'integer'],
            [['fecha_fin_actividad'],'fechavalida']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grupo_comite_id' => Yii::t('app', 'Grupo Comite ID'),
            'fecha_inicio_actividad' => Yii::t('app', 'Fecha Inicio Actividad'),
            'fecha_fin_actividad' => Yii::t('app', 'Fecha Fin Actividad'),
            'periodo_id' => Yii::t('app', 'Periodo'),
            'cen_inf_id' => Yii::t('app', 'Centro Infantil'),
        ];
    }


    /**
     * La fecha de fin de actividad tiene que ser mayor a la de inicio de actividad
     * @param $attribute_name
     * @param $params
     * @return bool
     */
    public function fechavalida($attribute_name, $params)
    {
        if($this->fecha_fin_actividad<$this->fecha_inicio_actividad){

            $this->addError($attribute_name, Yii::t('app', 'La fecha de fin de actividad tiene que ser mayor a la de inicio de actividad '));

            return false;
        }


        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComitePadresFlias()
    {
        return $this->hasMany(ComitePadresFlia::className(), ['grupo_comite_id' => 'grupo_comite_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCenInf()
    {
        return $this->hasOne(Cibv::className(), ['cen_inf_id' => 'cen_inf_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeriodo()
    {
        return $this->hasOne(Periodo::className(), ['periodo_id' => 'periodo_id']);
    }

    /**
     * @inheritdoc 
     * @return \backend\modules\centro_infantil\models\activequeries\GrupoComiteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\modules\centro_infantil\models\activequeries\GrupoComiteQuery(get_called_class());
    }

    /**
     * Recupera el comite de un centro infantil validando que el periodo sea activo
     * @param int $cen_inf_id
     * @author Sofia Mejia <amandasofiamejia@gmail.com>
     * @return string
     */
    public static function getComiteByPeriodo($cen_inf_id)
    {
        $periodo = Periodo::find()->periodoActivo()->one();
        if (!empty($periodo)) {
            $grupoComiteValido = GrupoComite::find()->deCibvAndPeriodo($cen_inf_id, $periodo->periodo_id);
            return $grupoComiteValido;
        }else{
            return GrupoComite::find()->deCibvAndPeriodo($cen_inf_id, null);
        }
    }
}
