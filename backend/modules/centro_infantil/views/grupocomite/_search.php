<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\GrupocomiteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-comite-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'grupo_comite_id') ?>

    <?= $form->field($model, 'fecha_inicio_actividad') ?>

    <?= $form->field($model, 'fecha_fin_actividad') ?>

    <?= $form->field($model, 'periodo_id') ?>

    <?= $form->field($model, 'cen_inf_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
