<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\GrupoComite */

$this->title = $model->grupo_comite_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Grupo Comites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grupo-comite-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->grupo_comite_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->grupo_comite_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'grupo_comite_id',
                'fecha_inicio_actividad',
                'fecha_fin_actividad',
                'periodo.limitesFechasPeriodo',
                'cenInf.cen_inf_nombre',
            ],
        ])
        ?>

    </div>
</div>
