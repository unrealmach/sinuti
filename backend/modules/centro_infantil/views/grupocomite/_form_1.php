<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use backend\modules\parametros_sistema\models\Periodo;
use backend\modules\centro_infantil\models\Cibv;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;

$webPath = Yii::$app->Util->getPublishAssetDirectory();
$this->registerJsFile($webPath . '/behaviorMasterDetail.js', ['depends' => [frontend\assets\AppAsset::className()]]);

/* @var $this yii\web\View */
/* @var $model backend\modules\settings\models\Po */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="grupo-comite-form" style="overflow: auto">

<?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
<?= Html::encode("Comite Central de Padres de Familia") ?>
<!--inicio maestro-->
<!--<div class="panel-body">-->
<div class="row">
    <div class="col-sm-6">
        <?=
                $form->field($model, 'fecha_inicio_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => []
                ])->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
    </div>
    <div class="col-sm-6">
        <?=
                $form->field($model, 'fecha_fin_actividad', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ],
                    'options' => []
                ])->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?=
        $form->field($model, 'periodo_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Periodo::find()->all(), 'periodo_id', 'limitesFechasPeriodo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Periodo', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
    </div>
    <div class="col-sm-6">
        <?=
        $form->field($model, 'cen_inf_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cibv::find()->all(), 'cen_inf_id', 'cen_inf_nombre', 'cen_inf_direccion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Cibv', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
    </div>
</div>
<!--</div>-->
<!--</div>-->
<!--fin maestro-->
<!--inicio detalle-->
<!--<div class="panel panel-default">
    <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Addresses</h4></div>
    <div class="panel-body">-->
        <?php
        DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
            'widgetBody' => '.container-items', // required: css class selector
            'widgetItem' => '.item', // required: css class
            'limit' => 4, // the maximum times, an element can be cloned (default 999)
            'min' => 4, // 0 or 1 (default 1)
            'insertButton' => '.add-item', // css class
            'deleteButton' => '.remove-item', // css class
            'model' => $d_model[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'comite_nombres',
                'comite_numero_contacto',
                'comite_dni',
                'comite_numero_contacto',
                'cargo'
            ],
        ]);
        ?>
        <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($d_model as $i => $comite): ?>
                <div class="item "><!-- widgetBody -->
                    <div class="">
                        <h3 class="pull-left">Address</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="">
                        <?php
                        // necessary for update action.
                        if (!$comite->isNewRecord) {
                            echo Html::activeHiddenInput($comite, "[{$i}]id");
                        }
                        ?>
                        <div class="row ">  <!-- se indica los campos del detalle para ser creados-->
                            <div class="col-sm-6">
                                <?=
                                        $form->field($comite, "[{$i}]comite_dni", [
                                            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>']
                                        )
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?=
                                        $form->field($comite, "[{$i}]comite_nombres", [
                                            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?=
                                        $form->field($comite, "[{$i}]comite_apellidos", [
                                            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?=
                                        $form->field($comite, "[{$i}]comite_numero_contacto", [
                                            'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->textInput(array('placeholder' => 'Ingrese ...',))
                                        ->label(null, ['class' => 'col-sm-2 control-label'])
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?=
                                        $form->field($comite, "[{$i}]cargo", ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                                        ->dropDownList([ 'PRESIDENTE' => 'PRESIDENTE', 'VICEPRESIDENTE' => 'VICEPRESIDENTE', 'TESORERO' => 'TESORERO', 'SECRETARIO' => 'SECRETARIO',], ['style' => 'color:black !important'])->label(null, ['class' => 'col-sm-2 control-label'])
                                ?>
                            </div>
                        </div><!-- .row -->
                        <!--<div class="separator"></div>-->
                    </div>
                </div>
            <?php endforeach; ?>
        <!--</div>-->
        <?php DynamicFormWidget::end(); ?>
<!--    </div>
</div>-->

<div class="form-group">
    <center>
        <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out' : 'btn btn-light-gray btn-lg btn-hvr hvr-radial-out', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
    </center>
</div>

<?php ActiveForm::end(); ?>

</div>
