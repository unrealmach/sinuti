<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use backend\modules\individuo\models\Infante;
use backend\modules\recursos_humanos\models\SectorAsignadoEducador;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\SectorAsignadoInfante */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sector-asignado-infante-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); 
//        TODO frontend valdiar que solo vea los infantes del cibv el coordinador_cibv y lsoeducadores de ese cibv
        ?>
        
        <?=
        $form->field($model, 'infante_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Infante::find()->all(), 'infante_id', 'nombreCompleto'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
        $form->field($model, 'sector_asignado_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(SectorAsignadoEducador::find()->all(), 'sector_asignado_educador_id', 'salon.grupoEdad.grupo_edad_descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione ', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
