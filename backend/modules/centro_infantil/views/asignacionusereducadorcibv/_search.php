<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionusereducadorcibvSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignacion-user-educador-cibv-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'asignacion_user_educador_cibv_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'sector_asignado_educador_id') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'observaciones') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
