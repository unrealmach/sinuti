<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionUserEducadorCibv */

$this->title = "Usuario del salón: " . $model->sectorAsignadoEducador->salon->salon_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Educador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-educador-cibv-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->asignacion_user_educador_cibv_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->asignacion_user_educador_cibv_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'asignacion_user_educador_cibv_id',
                [
                    'label' => 'Usuario',
                    'value' => $model->sectorAsignadoEducador->educador->nombreUser,
                ],
                    [
                    'label' => 'Educador',
                    'value' => $model->sectorAsignadoEducador->educador->nombreCompleto,
                ],
                'fecha',
                'observaciones:ntext',
            ],
        ])
        ?>

    </div>
</div>
