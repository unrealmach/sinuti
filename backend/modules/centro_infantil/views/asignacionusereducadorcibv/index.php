<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\centro_infantil\models\AsignacionusereducadorcibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignacion Usuarios Educadores Centros Infantiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-educador-cibv-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Asignacion Usuario Educador Centro Infantil'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                        'asignacion_user_educador_cibv_id',
                [
                    'label' => 'Usuario',
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                ],
                    [
                    'label' => 'Educador',
                    'attribute' => 'sector_asignado_educador_id',
                    'value' => 'sectorAsignadoEducador.educador.nombreCompleto',
                ],
                'fecha',
                'observaciones:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>
