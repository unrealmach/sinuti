<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv */

$this->title = "Usuario asignado al Centro Infantil: " . $model->autoridadCibv->cenInf->cen_inf_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-coordinador-cibv-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['update', 'id' => $model->asignacion_user_cibv_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-remove']) . Yii::t('app', 'Delete'), ['delete', 'id' => $model->asignacion_user_cibv_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </p>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'asignacion_user_cibv_id',
                [
                    'label' => 'Usuario',
                    'value' => $model->autoridadCibv->coordinador->nombreUser,
                ],
                    [
                    'label' => 'Coordinador',
                    'value' => $model->autoridadCibv->coordinador->nombreCompleto,
                ],
                'fecha',
                'observaciones:ntext',
            ],
        ])
        ?>

    </div>
</div>
