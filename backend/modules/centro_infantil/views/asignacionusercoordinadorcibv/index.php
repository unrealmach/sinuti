<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\centro_infantil\models\AsignacionusercoordinadorcibvSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Asignacion Usuarios Coordinadores Centros Infantiles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-coordinador-cibv-index box box-success">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create Asignacion Usuario Coordinador Centro Infantil'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                        'asignacion_user_cibv_id',
                [
                    'label' => 'Usuario',
                    'attribute' => 'user_id',
                    'value' => 'user.username',
                ],
                [
                    'label' => 'Coordinador',
                    'attribute' => 'autoridades_cibv_id',
                    'value' => 'autoridadCibv.coordinador.nombreCompleto',
                ],
                'fecha',
                'observaciones:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>
