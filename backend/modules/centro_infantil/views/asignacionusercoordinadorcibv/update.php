<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Asignacion User Coordinador Cibv',
]) . ' ' . $model->asignacion_user_cibv_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asignacion_user_cibv_id, 'url' => ['view', 'id' => $model->asignacion_user_cibv_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="asignacion-user-coordinador-cibv-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
