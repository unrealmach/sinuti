<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;
use backend\modules\recursos_humanos\models\AutoridadesCibv;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asignacion-user-coordinador-cibv-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'user_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione usuario', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>
        <?=
        $form->field($model, 'autoridades_cibv_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(AutoridadesCibv::find()->dePeriodoActivo()->all(), 'autoridades_cibv_id', 'coordinador.nombreCompleto', 'cenInf.cen_inf_nombre'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Coordinador', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);
        ?>        
        <?=
                $form->field($model, 'fecha', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])
                ->widget(dosamigos\datepicker\DatePicker::className(), ['language' => 'es',
                    'inline' => false,
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
        <?=
                $form->field($model, 'observaciones', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textarea(['rows' => 6])
                ->label(null, ['class' => 'col-sm-2 control-label'])
        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
            <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
    <?php ActiveForm::end(); ?>
</div>
