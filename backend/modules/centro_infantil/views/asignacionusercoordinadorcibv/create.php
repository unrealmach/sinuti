<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\AsignacionUserCoordinadorCibv */

$this->title = Yii::t('app', 'Create Asignacion User Coordinador Cibv');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Asignacion User Coordinador Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asignacion-user-coordinador-cibv-create box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
