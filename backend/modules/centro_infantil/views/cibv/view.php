<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */

$this->title = "Centro Infantil: " . $model->cen_inf_nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cibv-view box box-warning">
    <div class="box-header with-border">
        <center>
            <h1><?= Html::encode($this->title) ?></h1>
        </center>
    </div>
    <div class="box-body" style="overflow: auto">
        <p>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-pencil']).Yii::t('app', 'Update'), ['update', 'id' => $model->cen_inf_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Html::tag('i','',['class'=>'fa fa-fw fa-remove']).Yii::t('app', 'Delete'), ['delete', 'id' => $model->cen_inf_id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
            'method' => 'post',
            ],
            ]) ?>
        </p>

        <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                    'cen_inf_id',
//            'cen_inf_nombre',
            'cen_inf_direccion',
            'cen_inf_telefono',
            'cen_inf_correo',
            'cen_inf_activo',
        ],
        ]) ?>

    </div>
</div>
