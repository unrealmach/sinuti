<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use backend\modules\ubicacion\models\Parroquia;
/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="cibv-form form-horizontal">
    <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?=
        $form->field($model, 'parroquia_id', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Parroquia::find()->all(), 'parroquia_id', 'parroquia_nombre'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione Parroquía', 'class' => 'col-sm-2',],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(null, ['class' => 'col-sm-2 control-label']);

        ?>
        <?=
            $form->field($model, 'cen_inf_nombre', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'cen_inf_direccion', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'cen_inf_telefono', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'cen_inf_correo', [
                'template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->textInput(array('placeholder' => 'Ingrese ...'))
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
        <?=
            $form->field($model, 'cen_inf_activo', ['template' => '{label}<div class="col-sm-10">{input}{hint}{error}</div>'])->dropDownList(['SI' => 'SI', 'NO' => 'NO',], ['prompt' => 'Seleccione ...'])
            ->label(null, ['class' => 'col-sm-2 control-label'])

        ?>
    </div>
    <div class="box-footer">
        <!--<div class="form-group">-->
        <center>
<?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-fw fa-plus']) . Yii::t('app', 'Create') : Html::tag('i', '', ['class' => 'fa fa-fw fa-pencil']) . Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => "padding-left: 5% ! important; padding-right: 5% ! important;"]) ?>
        </center>
        <!--</div>-->

    </div>
<?php ActiveForm::end(); ?>
</div>
