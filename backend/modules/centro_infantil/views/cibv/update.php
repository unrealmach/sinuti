<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\Cibv */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cibv',
]) . ' ' . $model->cen_inf_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cibvs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cen_inf_id, 'url' => ['view', 'id' => $model->cen_inf_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cibv-update box box-info">
    <div class="box-header with-border">
         <?php
            $ar = explode(" ", ($this->title));
            $title = "";
            for ($i = 1; $i < count($ar); $i++) {
            $title.=" " . $ar[$i];}
          ?> 
        <center>
            <h1 class="box-title"><?= Html::encode($ar[0]) ?></h1>
        </center>
    </div>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
