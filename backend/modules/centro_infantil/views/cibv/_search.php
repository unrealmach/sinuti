<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\centro_infantil\models\CibvSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cibv-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cen_inf_id') ?>

    <?= $form->field($model, 'cen_inf_nombre') ?>

    <?= $form->field($model, 'cen_inf_direccion') ?>

    <?= $form->field($model, 'cen_inf_telefono') ?>

    <?= $form->field($model, 'cen_inf_correo') ?>

    <?php // echo $form->field($model, 'cen_inf_activo') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
