<?php

namespace backend\modules\centro_infantil\controllers;

use Yii;
use backend\modules\centro_infantil\models\GrupoComite;
use backend\modules\centro_infantil\models\GrupocomiteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\centro_infantil\models\recursos\DynamicFormModel;
use yii\data\ArrayDataProvider;
use backend\modules\recursos_humanos\models\ComitePadresFlia;
use backend\modules\parametros_sistema\models\Periodo;

/**
 * GrupocomiteController implements the CRUD actions for GrupoComite model.
 */
class GrupocomiteController extends Controller {

    public function behaviors() {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                        [
                        'allow' => false,
                        'roles' => ['?']
                    ],
                        [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GrupoComite models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new GrupocomiteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GrupoComite model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GrupoComite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new GrupoComite();
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->grupo_comite_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'periodo' => $periodo,
            ]);
        }
    }

    /**
     * Updates an existing GrupoComite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $periodo = Periodo::find()->periodoActivo()->one();
        if (empty($periodo)) {
            throw new NotFoundHttpException('Aun no existe un periodo activo.');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->grupo_comite_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
                        'periodo' => $periodo,
            ]);
        }
    }

    /**
     * Deletes an existing GrupoComite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GrupoComite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GrupoComite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = GrupoComite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
