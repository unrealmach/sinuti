<?php

namespace backend\modules\centro_infantil;

class Centro_infantil extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\centro_infantil\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
