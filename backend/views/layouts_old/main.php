<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use \yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script>
            var baseUrl = "<?php echo Url::current(); ?>";
        </script>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'Sistema Nutrimental Infantil',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
            ];

            if (Yii::$app->user->can('sysadmin')) {
                $menuItems[] = ['label' => 'Administracion RBAC', 'url' => ['/admin']];
                $menuItems[] = ['label' => 'Registrar usuario', 'url' => ['/site/signup']];
            }
            if (!Yii::$app->user->can('sysadmin') && Yii::$app->user->can('Usuario')) {
                
            }


            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            ?>

            <?php
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
            ?>

                <?php
//                foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
//                    echo '<div class="alert alert-' . $key . '">' . $message . '</div>';
//                }
                ?>

            <div class="alert-modify">
                <div class="alert alert-success">Ingreso correcto, ahora puede dar permisos a ese usuario</div>            
            </div>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= $content ?>

            </div>
        </div>

        <?php
//creacion de modal main
        Modal::begin([
            'headerOptions' => ['id' => 'mainModalHeader', 'style' => 'text-align:center'],
            'id' => 'mainModal',
            'size' => 'modal-lg',
//                    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE] //para que se cierre unicamente con la x
        ]);

        echo "<div id='mainModalContent'><div style='text-align:center'><img src='img/loading.gif'></div></div>";
        Modal::end();
        ?>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; Solii <?= date('Y') ?></p>
                <p class="pull-right"><?= '' ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
