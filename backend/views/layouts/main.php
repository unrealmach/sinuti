<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;

/* @var $this \yii\web\View */
/* @var $content string */


//if (class_exists('backend\assets\AppAsset')) {
backend\assets\AppAsset::register($this);
backend\assets\ThemeAsset::register($this);
//} else {
//    app\assets\AppAsset::register($this);
//}
//var_dump(Yii::$app->user->id);
$authAsiignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
//var_dump($authAsiignmentModel->item_name);
//die();
if ($authAsiignmentModel->item_name == 'nutricion'
    || $authAsiignmentModel->item_name == 'educador' 
    || $authAsiignmentModel->item_name == 'coordinador'
    || $authAsiignmentModel->item_name == 'supervisor'
    || $authAsiignmentModel->item_name == 'Admin'
    ) {
    Yii::$app->user->logout();
}


dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="images/favicon.ico">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="<?= \dmstr\helpers\AdminLteHelper::skinClass() ?> fixed">
        <?php $this->beginBody() ?>
        <div class="wrapper">

            <?=
            $this->render(
                'header.php', ['directoryAsset' => $directoryAsset]
            )

            ?>

            <?=
            $this->render(
                'left.php', ['directoryAsset' => $directoryAsset]
            )

            ?>

            <?=
            $this->render(
                'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
            )

            ?>

            <?php
            //creacion de modal main
            Modal::begin([
                'id' => 'mainModal',
                'headerOptions' => [
                    'id' => 'mainModalHeader',
                    'style' => 'text-align:center'
                ],
                'header' =>
                '<h4 class="modal-title" id="labelMainModalHeader">LOADING... </h4>',
                'footer' => '<button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Close</button>',
                'footerOptions' => [
                    'id' => 'mainModalFooter',
                ],
                'size' => 'modal-lg',
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE] //para que se cierre unicamente con la x
            ]);

            ?>

            <!--contenido del modal-->
            <center id ="tagLoading">
                <?= Html::img('@web/img/loading.gif') ?>
            </center>
            <!--fin contenido modal-->
            <?php
            Modal::end();

            ?>

            <?php
            Modal::begin([
                'id' => 'mainModal2',
                'headerOptions' => [
                    'id' => 'mainModal2Header',
                    'style' => 'text-align:center'
                ],
                'header' =>
                '<h4 class="modal-title" id="labelMainModal2Header">LOADING... </h4>',
                'footer' => '<button type="button" class="btn btn-sm btn-dark" data-dismiss="modal">Close</button>',
                'footerOptions' => [
                    'id' => 'mainModal2Footer',
                ],
                'size' => 'modal-lg',
                'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE] //para que se cierre unicamente con la x
            ]);

            ?>
            <!--contenido del modal-->
            <center id ="tagLoading">
                <?= Html::img('@web/img/loading.gif') ?>
            </center>
            <!--fin contenido modal-->
            <?php
            Modal::end();

            ?>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>


