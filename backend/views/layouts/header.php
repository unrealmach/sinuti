<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">SINUTI</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?=''// $this->render('miniComponents/header/messagesMenu.php', ['directoryAsset' => $directoryAsset]) ?>
                <?='' // $this->render('miniComponents/header/notificationsMenu.php', ['directoryAsset' => $directoryAsset]) ?>
                <?='' // $this->render('miniComponents/header/tasksMenu.php', ['directoryAsset' => $directoryAsset]) ?>
                <?= $this->render('miniComponents/header/userMenu.php', ['directoryAsset' => $directoryAsset]) ?>
<!--                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>-->
            </ul>
        </div>
        
       
    </nav>
</header>
