<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

?>
<!-- User Account: style can be found in dropdown.less -->

<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
        <?php
        //obtiene el rol del usuario que esta logeado
        $user = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
//        var_dump($user);
//        die();
        
        reset($user);  // pone el puntero en la primera posicion del array
        $first_key = key($user); //obtiene la primera key del array

        ?>
        <?php 
        $authAsiignmentModel = \backend\modules\rbac\models\AuthAssignment::find()->where(['user_id' => Yii::$app->user->id])->one();
        ?>
        <span class="hidden-xs"><?= Yii::$app->user->isGuest ? 'Invitado' : $authAsiignmentModel->item_name ?></span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                 alt="User Image"/>

            <p>
                <?= !empty(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : 'welcome' ?>
                <small>Bienvenido</small>
            </p>
        </li>
        <!-- Menu Body -->
        <!--                        <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>-->
        <!-- Menu Footer-->
        <li class="user-footer">
            <!--                            <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>-->
            <div class="pull-right">
                <?php
                if (Yii::$app->user->isGuest) {
                    echo Html::a(
                        'Ingresar', ['/site/login'], [ 'class' => 'btn btn-default btn-flat']
                    );
                } else {
                    echo Html::a(
                        'Salir ', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                    );
                }

                ?>
            </div>
        </li>
    </ul>
</li>

<!-- User Account: style can be found in dropdown.less -->