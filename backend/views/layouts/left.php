<?php
//use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
use webvimark\modules\UserManagement\UserManagementModule;

?>
<aside class="main-sidebar">

    <section class="sidebar" >

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>

            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->isGuest ? 'Invitado' : Yii::$app->user->identity->username ?></p>

                <?= Yii::$app->user->isGuest ? '' : ' <a href="#"><i class="fa fa-circle text-success"></i> Online</a>' ?>


            </div>
        </div>
        <?php
//        if (webvimark\modules\UserManagement\models\User::canRoute(['/audit'])) {
        if (Yii::$app->user->can('sysadmin') || Yii::$app->user->can('Admin') || Yii::$app->user->can('soporte')) {
            echo dmstr\widgets\Menu::widget([

                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu', 'data-widget'=>"tree"],
                'items' => [
                        ['label' => 'Sinuti', 'options' => ['class' => 'header']],
                        ['label' => '<i class="fa fa-file-code-o" style="width: 20px;"></i><span>Gii</span>',
                        'url' => ['/gii'], 'visible' => Yii::$app->user->can('sysadmin')],
                        ['label' => '<i class="fa fa-dashboard" style="width: 20px;"></i><span>Debug</span>',
                        'url' => ['/debug'], 'visible' => Yii::$app->user->can('sysadmin')],
                        ['label' => '<i class="fa fa-dashboard" style="width: 20px;"></i><span>Auditoría</span>',
                        'url' => ['/audit'], 'visible' => Yii::$app->user->can('sysadmin')],
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => '<i class="fa fa-fw fa-expeditedssl" style="width: 20px;"></i><span>Administración Sistema</span>',
//                    'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                [
                                'label' => 'Backend routes',
                                'url' => '#',
                                'visible' => Yii::$app->user->can('sysadmin') || Yii::$app->user->can('Admin') || Yii::$app->user->can('soporte'),
                                'items' => UserManagementModule::menuItems()
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-book" style="width: 20px;"></i><span>Inscripción</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-paste" style="width: 20px;"></i> Matriculas ',
                                'url' => ['/inscripcion/matricula'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-home" style="width: 20px;"></i> Salón ',
                                'url' => ['/inscripcion/salon'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-book" style="width: 20px;"></i><span>Distrito</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-paste" style="width: 20px;"></i> Distrito ',
                                'url' => ['/ubicacion/distrito'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-paste" style="width: 20px;"></i> Localidad ',
                                'url' => ['/ubicacion/localidad'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-paste" style="width: 20px;"></i> Parroquía ',
                                'url' => ['/ubicacion/parroquia'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-paste" style="width: 20px;"></i> Asignación usuario nutricionista ',
                                'url' => ['/tecnico_area/asignacionusernutricionistadistrito'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-home" style="width: 20px;"></i> Asignación usuario gastronomo ',
                                'url' => ['/tecnico_area/asignacionusergastronomodistrito'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-institution" style="width: 20px;"></i><span>Centro infantil</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-male" style="width: 20px;"></i> Asignación usuario coordinador-cibv ',
                                'url' => ['/centro_infantil/asignacionusercoordinadorcibv'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-male" style="width: 20px;"></i> Asignación usuario eduacador-cibv ',
                                'url' => ['/centro_infantil/asignacionusereducadorcibv'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-bell" style="width: 20px;"></i> Cibv ',
                                'url' => ['/centro_infantil/cibv'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-group" style="width: 20px;"></i> Sector Asignado Infantes',
                                'url' => ['/centro_infantil/sectorasignadoinfante'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-group" style="width: 20px;"></i> Comite Central ',
                                'url' => ['/centro_infantil/grupocomite'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Recursos Humanos</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-male" style="width: 20px;"></i> Representantes Cibv ',
                                'url' => ['/recursos_humanos/autoridadescibv'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Comite Padres de Familia ',
                                'url' => ['/recursos_humanos/comitepadresflia'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar-check-o" style="width: 20px;"></i> Sector Asignado Educador',
                                'url' => ['/recursos_humanos/sectorasignadoeducador'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> Coordinadores ',
                                'url' => ['/recursos_humanos/coordinadorcibv'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-hand-stop-o" style="width: 20px;"></i> Educadores ',
                                'url' => ['/recursos_humanos/educador'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-gears" style="width: 20px;"></i><span>Parametros del sistema</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-calendar-check-o" style="width: 20px;"></i> Período ',
                                'url' => ['/parametros_sistema/periodo'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Individuo</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Infantes ',
                                'url' => ['/individuo/infante'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Patologías ',
                                'url' => ['/individuo/patologia'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> Vacunas-infante ',
                                'url' => ['/individuo/registrovacunainfante'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Consumo alimenticio</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Asignación infante menú semanal',
                                'url' => ['/consumo_alimenticio/asiginfcsemanal'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Registro bitácora alimenticia',
                                'url' => ['/consumo_alimenticio/bitacoraconsumoinfantil'],
                            ],
                        ],
                    ],
//                    [ 'label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Asistencia</span>',
//                        'visible' => Yii::$app->user->isGuest == true ? false : true,
//                        'url' => ['/asistencia/asistencia'],
//                        'items' => [
////                        [ 'label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Asignación menú semanal a infantes ',
////                            'url' => ['/consumo_alimenticio/asiginfcsemanal'],
////                        ],
//                        ],
//                    ],
                    ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Métricas individuo</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Datos antropométricos ',
                                'url' => ['/metricas_individuo/datoantropometrico'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Grupo de edad ',
                                'url' => ['/metricas_individuo/grupoedad'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> Lista de Vacunas ',
                                'url' => ['/metricas_individuo/tipovacuna'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Composición nutricional</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Composición nutricional ',
                                'url' => ['/composicion_nutricional/composicionnutricional'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Composición nutricional platillo ',
                                'url' => ['/composicion_nutricional/composicionnutricionalplatillo'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Nutrientes ',
                                'url' => ['/composicion_nutricional/nutriente'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> Rango nutrientes ',
                                'url' => ['/composicion_nutricional/rangonutriente'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Preparación menús</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Tiempos de comida ',
                                'url' => ['/preparacion_carta/tiempocomida'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Tipos de preparaciones ',
                                'url' => ['/preparacion_carta/tipopreparacioncarta'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> M Menú ',
                                'url' => ['/preparacion_carta/mprepcarta'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-graduation-cap" style="width: 20px;"></i> D Menú ',
                                'url' => ['/preparacion_carta/dpreparacioncarta'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Nutrición</span>',
                        'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Categorías ',
                                'url' => ['/nutricion/categoria'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> Alimentos ',
                                'url' => ['/nutricion/alimento'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Platillos</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Grupo Platillo ',
                                'url' => ['/preparacion_platillo/grupoplatillo'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> Tipo Receta ',
                                'url' => ['/preparacion_platillo/tiporeceta'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> M Platillo ',
                                'url' => ['/preparacion_platillo/mplatillo'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> D Platillo ',
                                'url' => ['/preparacion_platillo/dplatillo'],
                            ],
                        ],
                    ],
                        ['label' => '<i class="fa fa-fw fa-user" style="width: 20px;"></i><span>Menús semanales</span>',
                       'visible' => Yii::$app->user->can('sysadmin'),
                        'url' => '#',
                        'items' => [
                                ['label' => '<i class="fa fa-fw fa-child" style="width: 20px;"></i> M menusemanal ',
                                'url' => ['/carta_semanal/mcartasemanal'],
                            ],
                                ['label' => '<i class="fa fa-fw fa-calendar" style="width: 20px;"></i> D menusemanal ',
                                'url' => ['/carta_semanal/dcartasemanal'],
                            ],
                        ],
                    ],
                ],
            ]);
        } else {
            echo "No tiene los permisos suficientes";
        }

        ?>

    </section>

</aside>
