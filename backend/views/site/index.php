<?php
/* @var $this yii\web\View */
$this->title = ' ';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Sistema Nutrimental Infantil</h1>
        <p class="lead">Bienvenido a la plataforma web, que brinda el servicio de control y seguimiento infantil.</p>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <h2>SNI</h2>
                <p>Es un servicio web orientando al control del niño sano desde  1 a 3 años que asisten a los 
                    CIBV, en el cual podras observas las curvas de crecimiento con las cuales trabaja el MSP y que 
                    a su vez sirven para prevenir la desnutrición infantil.</p>
                <p>Permite la creación interactiva de menús saludables y nutritivos para cada tiempo de comida,
                    asi los padres de familia conoceran que tipo de alimentación reciven sus hijos en los CIBV como
                    también que reacciones alimentarias estos pueden generar en los infantes.</p>
                <p>las entidades de control podrán realizar el seguimiento a través de la visualización de 
                    estadisticas poblaciones de cada centro en cuanto a peso, talla y desnutrición se refiere</p>
                <p>Los nutricionistas y gastrónomos podran ver el porcentaje de adecuación de los menús y los 
                    porcentajes de consumo diario de cada infante.</p>
            </div>
            <div class="col-lg-4">
                <h2>Procesos</h2>
                <p>Cada unos de los procesos ha sido supervisado por personal técnico en el area de
                    nutrición del MSP, personal del MIES y Area social GAD-I.</p>
            </div>
        </div>
    </div>
</div>
