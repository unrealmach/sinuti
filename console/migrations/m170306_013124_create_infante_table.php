<?php
use yii\db\Migration;

/**
 * Handles the creation for table `infante`.
 */
class m170306_013124_create_infante_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS infante_genero CASCADE ");
            $this->execute("DROP TYPE IF EXISTS infante_etnia CASCADE");
            $this->execute("DROP TYPE IF EXISTS infante_represent_parentesco CASCADE");
            $this->execute("CREATE TYPE infante_genero AS  ENUM('FEMENINO', 'MASCULINO');");
            $this->execute("CREATE TYPE infante_etnia AS  ENUM('MESTIZO', 'AFROAMERICANO', 'INDIGENA') ;");
            $this->execute("CREATE TYPE infante_represent_parentesco AS  ENUM('MAMA','PAPA','ABUELO','ABUELA','HERMANO','HERMANA','TIO','TIA','MADRASTRA','PADRASTRO','REPRESENTANTE_LEGAL') ;");
            $this->createTable('infante', [
                'infante_id' => $this->primaryKey(),
                'infante_dni' => $this->string(150)->notNull()->unique(),
                'infante_nombres' => $this->string(150)->notNull(),
                'infante_apellidos' => $this->string(50)->notNull(),
                'infante_nacionalidad' => $this->string(50)->notNull(),
                'infante_genero' => "infante_genero NOT NULL",
                'infante_etnia' => "infante_etnia NOT NULL",
                'infantes_fecha_nacimiento' => $this->date()->notNull(),
                'infante_represent_documento' => $this->string(45)->notNull(),
                'infante_represent_nombres' => $this->string(150)->notNull(),
                'infante_represent_apellidos' => $this->string(150)->notNull(),
                'infante_represent_parentesco' => "infante_represent_parentesco NOT NULL",
                'infante_direccion_domiciliaria' => $this->string(300)->notNull(),
                'infante_represent_telefono' => $this->string(15)->notNull(),
                'infante_represent_correo' => $this->string(100),
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('infante', [
                'infante_id' => $this->primaryKey(),
                'infante_dni' => $this->string(150)->notNull()->unique(),
                'infante_nombres' => $this->string(150)->notNull(),
                'infante_apellidos' => $this->string(50)->notNull(),
                'infante_nacionalidad' => $this->string(50)->notNull(),
                'infante_genero' => "ENUM('FEMENINO', 'MASCULINO') NOT NULL",
                'infante_etnia' => "ENUM('MESTIZO', 'AFROAMERICANO', 'INDIGENA')  NOT NULL",
                'infantes_fecha_nacimiento' => $this->date()->notNull(),
                'infante_represent_documento' => $this->string(45)->notNull(),
                'infante_represent_nombres' => $this->string(150)->notNull(),
                'infante_represent_apellidos' => $this->string(150)->notNull(),
                'infante_represent_parentesco' => "ENUM('MAMA','PAPA','ABUELO','ABUELA','HERMANO','HERMANA','TIO','TIA','MADRASTRA','PADRASTRO','REPRESENTANTE_LEGAL') NOT NULL",
                'infante_direccion_domiciliaria' => $this->string(300)->notNull(),
                'infante_represent_telefono' => $this->string(15)->notNull(),
                'infante_represent_correo' => $this->string(100),
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'postgres') {
            $this->execute("DROP TYPE IF EXISTS infante_genero CASCADE ");
            $this->execute("DROP TYPE IF EXISTS infante_etnia CASCADE");
            $this->execute("DROP TYPE IF EXISTS infante_represent_parentesco CASCADE");
        }

        $this->dropTable('infante');
    }
}
