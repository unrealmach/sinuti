<?php

use yii\db\Migration;

/**
 * Handles the creation for table `alimento`.
 * Has foreign keys to the tables:
 *
 * - `categoria`
 */
class m170310_014401_create_alimento_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('alimento', [
            'alimento_id' => $this->primaryKey(),
            'categoria_id' => $this->integer()->notNull(),
            'alimento_nombre' => $this->string(255)->notNull(),
            'alimento_descripcion' => $this->text()->notNull(),
        ]);

        // creates index for column `categoria_id`
        $this->createIndex(
            'idx-alimento-categoria_id',
            'alimento',
            'categoria_id'
        );

        // add foreign key for table `categoria`
        $this->addForeignKey(
            'fk-alimento-categoria_id',
            'alimento',
            'categoria_id',
            'categoria',
            'categoria_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `categoria`
        $this->dropForeignKey(
            'fk-alimento-categoria_id',
            'alimento'
        );

        // drops index for column `categoria_id`
        $this->dropIndex(
            'idx-alimento-categoria_id',
            'alimento'
        );

        $this->dropTable('alimento');
    }
}
