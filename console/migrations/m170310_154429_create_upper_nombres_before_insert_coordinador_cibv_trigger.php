<?php
use yii\db\Migration;

class m170310_154429_create_upper_nombres_before_insert_coordinador_cibv_trigger extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_coordinador_cibv_nombres_before_insert()
RETURNS trigger AS '
BEGIN
   NEW.coordinador_nombres = UPPER(NEW.coordinador_nombres);
   NEW.coordinador_apellidos = UPPER(NEW.coordinador_apellidos);
   RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_coordinador_cibv_nombres_before_insert
BEFORE INSERT ON coordinador_cibv
FOR EACH ROW
EXECUTE PROCEDURE upper_coordinador_cibv_nombres_before_insert();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_coordinador_cibv_nombres_before_insert() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_coordinador_cibv_nombres_before_insert ON coordinador_cibv CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            $createTriggerSql = <<< SQL
                CREATE TRIGGER `upper_coordinador_cibv_nombres_before_insert` BEFORE INSERT ON `coordinador_cibv` FOR EACH ROW BEGIN
SET   NEW.coordinador_nombres = UPPER(NEW.coordinador_nombres);
SET  NEW.coordinador_apellidos = UPPER(NEW.coordinador_apellidos);
END
SQL;
            $this->execute("DROP TRIGGER IF EXISTS `upper_coordinador_cibv_nombres_before_insert`");
            $this->execute($createTriggerSql);
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP  FUNCTION IF EXISTS upper_coordinador_cibv_nombres_before_insert() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_coordinador_cibv_nombres_before_insert ON coordinador_cibv CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP TRIGGER IF EXISTS `upper_coordinador_cibv_nombres_before_insert`");
        }
//        return false;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
