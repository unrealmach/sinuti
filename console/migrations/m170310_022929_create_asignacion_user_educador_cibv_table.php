<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_user_educador_cibv`.
 */
class m170310_022929_create_asignacion_user_educador_cibv_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_user_educador_cibv', [
            'asignacion_user_educador_cibv_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'sector_asignado_educador_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'observaciones' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('asignacion_user_educador_cibv');
    }
}
