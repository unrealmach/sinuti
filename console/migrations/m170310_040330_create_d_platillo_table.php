<?php

use yii\db\Migration;

/**
 * Handles the creation for table `d_platillo`.
 * Has foreign keys to the tables:
 *
 * - `alimento`
 * - `m_platillo`
 */
class m170310_040330_create_d_platillo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('d_platillo', [
            'd_platillo_id' => $this->primaryKey(),
            'alimento_id' => $this->integer()->notNull(),
            'm_platillo_id' => $this->integer()->notNull(),
            'd_platillo_cantidad_g' => $this->string(45)->notNull(),
        ]);

        // creates index for column `alimento_id`
        $this->createIndex(
            'idx-d_platillo-alimento_id',
            'd_platillo',
            'alimento_id'
        );

        // add foreign key for table `alimento`
        $this->addForeignKey(
            'fk-d_platillo-alimento_id',
            'd_platillo',
            'alimento_id',
            'alimento',
            'alimento_id',
            'CASCADE'
        );

        // creates index for column `m_platillo_id`
        $this->createIndex(
            'idx-d_platillo-m_platillo_id',
            'd_platillo',
            'm_platillo_id'
        );

        // add foreign key for table `m_platillo`
        $this->addForeignKey(
            'fk-d_platillo-m_platillo_id',
            'd_platillo',
            'm_platillo_id',
            'm_platillo',
            'm_platillo_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `alimento`
        $this->dropForeignKey(
            'fk-d_platillo-alimento_id',
            'd_platillo'
        );

        // drops index for column `alimento_id`
        $this->dropIndex(
            'idx-d_platillo-alimento_id',
            'd_platillo'
        );

        // drops foreign key for table `m_platillo`
        $this->dropForeignKey(
            'fk-d_platillo-m_platillo_id',
            'd_platillo'
        );

        // drops index for column `m_platillo_id`
        $this->dropIndex(
            'idx-d_platillo-m_platillo_id',
            'd_platillo'
        );

        $this->dropTable('d_platillo');
    }
}
