<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_gastronomo_distrito`.
 * Has foreign keys to the tables:
 *
 * - `gastronomo`
 * - `distrito`
 */
class m170310_164905_create_asignacion_gastronomo_distrito_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_gastronomo_distrito', [
            'asignacion_gastronomo_distrito_id' => $this->primaryKey(),
            'gastronomo_id' => $this->integer()->notNull(),
            'distrito_id' => $this->integer()->notNull(),
            'fecha_inicio_actividad' => $this->date()->notNull(),
            'fecha_fin_actividad' => $this->date()->defaultValue(NULL),
        ]);

        // creates index for column `gastronomo_id`
        $this->createIndex(
            'idx-asignacion_gastronomo_distrito-gastronomo_id',
            'asignacion_gastronomo_distrito',
            'gastronomo_id'
        );

        // add foreign key for table `gastronomo`
        $this->addForeignKey(
            'fk-asignacion_gastronomo_distrito-gastronomo_id',
            'asignacion_gastronomo_distrito',
            'gastronomo_id',
            'gastronomo',
            'gastronomo_id',
            'CASCADE'
        );

        // creates index for column `distrito_id`
        $this->createIndex(
            'idx-asignacion_gastronomo_distrito-distrito_id',
            'asignacion_gastronomo_distrito',
            'distrito_id'
        );

        // add foreign key for table `distrito`
        $this->addForeignKey(
            'fk-asignacion_gastronomo_distrito-distrito_id',
            'asignacion_gastronomo_distrito',
            'distrito_id',
            'distrito',
            'distrito_id',
            'CASCADE'
        );
        $this->execute("ALTER TABLE asignacion_gastronomo_distrito ALTER COLUMN fecha_fin_actividad SET DEFAULT NULL;");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `gastronomo`
        $this->dropForeignKey(
            'fk-asignacion_gastronomo_distrito-gastronomo_id',
            'asignacion_gastronomo_distrito'
        );

        // drops index for column `gastronomo_id`
        $this->dropIndex(
            'idx-asignacion_gastronomo_distrito-gastronomo_id',
            'asignacion_gastronomo_distrito'
        );

        // drops foreign key for table `distrito`
        $this->dropForeignKey(
            'fk-asignacion_gastronomo_distrito-distrito_id',
            'asignacion_gastronomo_distrito'
        );

        // drops index for column `distrito_id`
        $this->dropIndex(
            'idx-asignacion_gastronomo_distrito-distrito_id',
            'asignacion_gastronomo_distrito'
        );

        $this->dropTable('asignacion_gastronomo_distrito');
    }
}
