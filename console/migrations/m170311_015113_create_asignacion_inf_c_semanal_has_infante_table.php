<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_inf_c_semanal_has_infante`.
 * Has foreign keys to the tables:
 *
 * - `asignacion_infante_c_semanal`
 * - `infante`
 */
class m170311_015113_create_asignacion_inf_c_semanal_has_infante_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_inf_c_semanal_has_infante', [
            'asignacion_junction_id' => $this->primaryKey(),
            'asig_inf_c_sem_id' => $this->integer()->notNull(),
            'infante_id' => $this->integer()->notNull(),
            'anio' => $this->string(4)->notNull(),
            'num_semana' => $this->string(4)->notNull(),
            'fue_consumido' => $this->boolean()->notNull()->defaultValue(FALSE),
        ]);

        // creates index for column `asig_inf_c_sem_id`
        $this->createIndex(
            'idx-asignacion_inf_c_semanal_has_infante-asig_inf_c_sem_id',
            'asignacion_inf_c_semanal_has_infante',
            'asig_inf_c_sem_id'
        );

        // add foreign key for table `asignacion_infante_c_semanal`
        $this->addForeignKey(
            'fk-asignacion_inf_c_semanal_has_infante-asig_inf_c_sem_id',
            'asignacion_inf_c_semanal_has_infante',
            'asig_inf_c_sem_id',
            'asignacion_infante_c_semanal',
            'asig_inf_c_sem_id',
            'CASCADE'
        );

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-asignacion_inf_c_semanal_has_infante-infante_id',
            'asignacion_inf_c_semanal_has_infante',
            'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-asignacion_inf_c_semanal_has_infante-infante_id',
            'asignacion_inf_c_semanal_has_infante',
            'infante_id',
            'infante',
            'infante_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `asignacion_infante_c_semanal`
        $this->dropForeignKey(
            'fk-asignacion_inf_c_semanal_has_infante-asig_inf_c_sem_id',
            'asignacion_inf_c_semanal_has_infante'
        );

        // drops index for column `asig_inf_c_sem_id`
        $this->dropIndex(
            'idx-asignacion_inf_c_semanal_has_infante-asig_inf_c_sem_id',
            'asignacion_inf_c_semanal_has_infante'
        );

        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-asignacion_inf_c_semanal_has_infante-infante_id',
            'asignacion_inf_c_semanal_has_infante'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-asignacion_inf_c_semanal_has_infante-infante_id',
            'asignacion_inf_c_semanal_has_infante'
        );

        $this->dropTable('asignacion_inf_c_semanal_has_infante');
    }
}
