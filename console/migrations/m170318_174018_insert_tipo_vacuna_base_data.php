<?php

use yii\db\Migration;

class m170318_174018_insert_tipo_vacuna_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('tipo_vacuna',
            ['tipo_vacuna_id',
            'tipo_vac_enfermedad',
            'tipo_vac_nombre',
            'tipo_vac_num_dosis',
            'tipo_vac_dosis_canti',
            'tipo_vac_dosis_medida',
            'tipo_vac_via'],
            [
                ['1', 'TUBERCULOSIS', 'BCG', '1', '0.10', 'ML', 'I.D.'], ['2', 'POLIOMIELITIS',
                'ANTIPOLIO', '4', '2.00', 'GOTAS', 'V.O.'], ['3', 'DPT+HB+Hib', 'PENTAVALENTE',
                '3', '0.50', 'ML', 'IM'], ['4', 'HEPATITIS B', 'PENTAVALENTE', '4',
                '0.50', 'ML', 'IM'], ['5', 'DIARREA RORAVIRUS', 'ROTAVIRUS', '2',
                '1.00', 'ML', 'V.O.'], ['6', 'NEUMONIA', 'NEUMOCOCO', '3', '0.50',
                'ML', 'IM'], ['7', 'SRP', 'TRIPE VIRAL', '3', '0.50', 'ML', 'VS'],
            ['8', 'FIEBRE AMARILLA', 'FIEBRE AMARILLA', '1', '0.50', 'ML', 'VS']
            ]
        );
    }

    public function down()
    {
         if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table tipo_vacuna CASCADE;
SQL;
         $this->execute($truncate);}
         else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table tipo_vacuna;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}