<?php
use yii\db\Migration;

/**
 * Handles the creation for table `cibv`.
 * Has foreign keys to the tables:
 *
 * - `parroquia`
 */
class m170310_023326_create_cibv_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS cen_inf_activo CASCADE");
            $this->execute("CREATE TYPE cen_inf_activo AS  ENUM('SI', 'NO');");
            $this->createTable('cibv', [
                'cen_inf_id' => $this->primaryKey(),
                'parroquia_id' => $this->integer(),
                'cen_inf_nombre' => $this->string(150)->notNull(),
                'cen_inf_direccion' => $this->string(300)->notNull(),
                'cen_inf_telefono' => $this->string(15)->notNull(),
                'cen_inf_telefono' => $this->string(15)->notNull(),
                'cen_inf_correo' => $this->string(100)->notNull(),
                'cen_inf_activo' => "cen_inf_activo NOT NULL",
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('cibv', [
                'cen_inf_id' => $this->primaryKey(),
                'parroquia_id' => $this->integer(),
                'cen_inf_nombre' => $this->string(150)->notNull(),
                'cen_inf_direccion' => $this->string(300)->notNull(),
                'cen_inf_telefono' => $this->string(15)->notNull(),
                'cen_inf_telefono' => $this->string(15)->notNull(),
                'cen_inf_correo' => $this->string(100)->notNull(),
                'cen_inf_activo' => "ENUM('SI', 'NO') NOT NULL DEFAULT 'NO'",
            ]);
        }


        // creates index for column `parroquia_id`
        $this->createIndex(
            'idx-cibv-parroquia_id', 'cibv', 'parroquia_id'
        );

        // add foreign key for table `parroquia`
        $this->addForeignKey(
            'fk-cibv-parroquia_id', 'cibv', 'parroquia_id', 'parroquia', 'parroquia_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS cen_inf_activo CASCADE");
        } else if ($this->db->driverName === 'mysql') {
            
        }

        // drops foreign key for table `parroquia`
        $this->dropForeignKey(
            'fk-cibv-parroquia_id', 'cibv'
        );

        // drops index for column `parroquia_id`
        $this->dropIndex(
            'idx-cibv-parroquia_id', 'cibv'
        );

        $this->dropTable('cibv');
    }
}
