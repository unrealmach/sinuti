<?php
use yii\db\Migration;

/**
 * Handles the creation for table `periodo`.
 */
class m170310_034851_create_periodo_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS estado_periodo CASCADE");
            $this->execute("CREATE TYPE estado_periodo AS  ENUM('ACTIVO', 'INACTIVO');");
            $this->createTable('periodo', [
                'periodo_id' => $this->primaryKey(),
                'fecha_inicio' => $this->date()->notNull(),
                'fecha_fin' => $this->date()->notNull(),
                'estado' => "estado_periodo NOT NULL DEFAULT 'INACTIVO'",
            ]);
        } else if ($this->db->driverName === 'mysql') {

            $this->createTable('periodo', [
                'periodo_id' => $this->primaryKey(),
                'fecha_inicio' => $this->date()->notNull(),
                'fecha_fin' => $this->date()->notNull(),
                'estado' => "ENUM('ACTIVO', 'INACTIVO') NOT NULL DEFAULT 'INACTIVO'",
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS estado_periodo CASCADE");
        }
        $this->dropTable('periodo');
    }
}
