<?php
use yii\db\Migration;

/**
 * Maneja la creacion del trigger en insert para upercase nombres y apellidos
 * de infante y representante
 */
class m170306_032952_create_upper_nombres_infante_before_update_trigger extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_nombres_infante_before_update()
RETURNS trigger AS '
BEGIN
     NEW.infante_nombres := UPPER(NEW.infante_nombres);
     NEW.infante_apellidos := UPPER(NEW.infante_apellidos);
     NEW.infante_represent_apellidos := UPPER(NEW.infante_represent_apellidos);
     NEW.infante_represent_nombres := UPPER(NEW.infante_represent_nombres);
                RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_nombres_infante_before_update
BEFORE INSERT ON infante
FOR EACH ROW
EXECUTE PROCEDURE upper_nombres_infante_before_update();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_nombres_infante_before_update() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_nombres_infante_before_update ON infante CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            $createTriggerSql = <<< SQL
            CREATE TRIGGER `upper_nombres_infante_before_update` BEFORE UPDATE ON `infante` FOR EACH ROW BEGIN
SET NEW.infante_nombres = UPPER(NEW.infante_nombres);
SET NEW.infante_apellidos = UPPER(NEW.infante_apellidos);
SET NEW.infante_represent_apellidos = UPPER(NEW.infante_represent_apellidos);
SET NEW.infante_represent_nombres = UPPER(NEW.infante_represent_nombres);
END
SQL;
            $this->execute("DROP TRIGGER IF EXISTS `upper_nombres_infante_before_update`");
            $this->execute($createTriggerSql);
        }
    }

    public function down()
    {

        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP FUNCTION IF EXISTS upper_nombres_infante_before_update() CASCADE;");
            $result = $this->execute("DROP TRIGGER IF EXISTS upper_nombres_infante_before_update ON infante CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP TRIGGER IF EXISTS `upper_nombres_infante_before_update`");
        }

//        echo $result;
//        echo "m170306_032951_create_infante_trigger cannot be reverted.\n";
//        return false;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
