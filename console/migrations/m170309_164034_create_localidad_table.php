<?php

use yii\db\Migration;

/**
 * Handles the creation for table `localidad`.
 * Has foreign keys to the tables:
 *
 * - `distrito`
 */
class m170309_164034_create_localidad_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('localidad', [
            'localidad_id' => $this->primaryKey(),
            'localidad_codigo' => $this->integer()->notNull(),
            'localidad_nombre' => $this->string(100)->notNull(),
            'distrito_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `distrito_id`
        $this->createIndex(
            'idx-localidad-distrito_id',
            'localidad',
            'distrito_id'
        );

        // add foreign key for table `distrito`
        $this->addForeignKey(
            'fk-localidad-distrito_id',
            'localidad',
            'distrito_id',
            'distrito',
            'distrito_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `distrito`
        $this->dropForeignKey(
            'fk-localidad-distrito_id',
            'localidad'
        );

        // drops index for column `distrito_id`
        $this->dropIndex(
            'idx-localidad-distrito_id',
            'localidad'
        );

        $this->dropTable('localidad');
    }
}
