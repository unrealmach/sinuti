<?php
use yii\db\Migration;

class m170329_023628_insert_asignacion_user_rol_soporte extends Migration
{

    public function up()
    {
        $this->batchInsert('auth_assignment', ['item_name', 'user_id', 'created_at'
            ], [
                ['soporte', 2, '1490754843'],
            ]
        );
    }

    public function down()
    {
        $this->delete('auth_assignment',"item_name='soporte'");
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
