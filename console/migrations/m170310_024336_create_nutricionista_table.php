<?php

use yii\db\Migration;

/**
 * Handles the creation for table `nutricionista`.
 */
class m170310_024336_create_nutricionista_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nutricionista', [
            'nutricionista_id' => $this->primaryKey(),
            'nutricionista_dni' => $this->string(45)->notNull(),
            'nutricionista_nombres' => $this->string(150)->notNull(),
            'nutricionista_apellidos' => $this->string(150)->notNUll(),
            'nutricionista_telefono' => $this->string(15)->notNull(),
            'nutricionista_correo' => $this->string(150)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nutricionista');
    }
}
