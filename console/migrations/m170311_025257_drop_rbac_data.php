<?php
use yii\db\Migration;

class m170311_025257_drop_rbac_data extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child CASCADE;
SQL;
            $truncateAuth_item = <<< SQL
truncate table auth_item CASCADE;
SQL;
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group CASCADE;
SQL;
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment CASCADE;
SQL;
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log CASCADE;
SQL;
            $truncateUser = <<< SQL
truncate table "user" CASCADE;
SQL;
            $this->execute($truncateAuth_item_child);
            $this->execute($truncateAuth_item);
            $this->execute($truncateAuth_item_group);
            $this->execute($truncateAuth_assignment);
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child;
SQL;
            $truncateAuth_item = <<< SQL
truncate table auth_item;
SQL;
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group;
SQL;
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment;
SQL;
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log;
SQL;
            $truncateUser = <<< SQL
truncate table user;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncateAuth_item_child);
            $this->execute($truncateAuth_item);
            $this->execute($truncateAuth_item_group);
            $this->execute($truncateAuth_assignment);
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
            $this->execute($enable);
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child CASCADE;
SQL;
            $truncateAuth_item = <<< SQL
truncate table auth_item CASCADE;
SQL;
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group CASCADE;
SQL;
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment CASCADE;
SQL;
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log CASCADE;
SQL;
            $truncateUser = <<< SQL
truncate table "user" CASCADE;
SQL;
            $this->execute($truncateAuth_item_child);
            $this->execute($truncateAuth_item);
            $this->execute($truncateAuth_item_group);
            $this->execute($truncateAuth_assignment);
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child;
SQL;
            $truncateAuth_item = <<< SQL
truncate table auth_item;
SQL;
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group;
SQL;
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment;
SQL;
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log;
SQL;
            $truncateUser = <<< SQL
truncate table user;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncateAuth_item_child);
            $this->execute($truncateAuth_item);
            $this->execute($truncateAuth_item_group);
            $this->execute($truncateAuth_assignment);
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
