<?php

use yii\db\Migration;

/**
 * Handles the creation for table `m_carta_semanal`.
 * Has foreign keys to the tables:
 *
 * - `cibv`
 */
class m170311_011308_create_m_carta_semanal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('m_carta_semanal', [
            'm_c_sem_id' => $this->primaryKey(),
            'cen_inf_id' => $this->integer()->notNull(),
            'm_c_sem_fecha_inicio' => $this->dateTime()->notNull(),
            'm_c_sem_fecha_fin' => $this->dateTime()->notNull(),
            'm_c_sem_user_responsable' => $this->integer()->notNull(),
            'm_c_sem_num_semana' => $this->integer()->notNull(),
            'm_c_sem_num_semana' => $this->integer()->notNull(),
            'm_c_sem_capacidad_infantes' => $this->integer()->notNull(),
            'm_c_sem_observaciones' => $this->text()->notNull(),
        ]);

        // creates index for column `cen_inf_id`
        $this->createIndex(
            'idx-m_carta_semanal-cen_inf_id',
            'm_carta_semanal',
            'cen_inf_id'
        );

        // add foreign key for table `cibv`
        $this->addForeignKey(
            'fk-m_carta_semanal-cen_inf_id',
            'm_carta_semanal',
            'cen_inf_id',
            'cibv',
            'cen_inf_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `cibv`
        $this->dropForeignKey(
            'fk-m_carta_semanal-cen_inf_id',
            'm_carta_semanal'
        );

        // drops index for column `cen_inf_id`
        $this->dropIndex(
            'idx-m_carta_semanal-cen_inf_id',
            'm_carta_semanal'
        );

        $this->dropTable('m_carta_semanal');
    }
}
