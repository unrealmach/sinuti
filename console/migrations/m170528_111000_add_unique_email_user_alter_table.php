<?php
use yii\db\Migration;

class m170528_111000_add_unique_email_user_alter_table extends Migration
{

    public function up()
    {

        if ($this->db->driverName === 'mysql') {
            $alterSql = <<< SQL
ALTER TABLE user ADD UNIQUE (email);
SQL;
            $this->execute($alterSql);
        } else
        if ($this->db->driverName === 'pgsql') {
            $alterSql = <<< SQL
ALTER TABLE "user" ADD CONSTRAINT email_unique_user_index UNIQUE (email);
SQL;
            $this->execute($alterSql);
        }
    }

    public function down()
    {
        // echo "m170528_111000_add_unique_email_user_alter_table cannot be reverted.\n";

        if ($this->db->driverName === 'mysql') {
            $alterSql = <<< SQL
alter table user drop index email;
SQL;
            $this->execute($alterSql);
        } else
        if ($this->db->driverName === 'pgsql') {
            $alterSql = <<< SQL
 ALTER TABLE "user" DROP CONSTRAINT "email_unique_user_index"
SQL;
            $this->execute($alterSql);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
