<?php

use yii\db\Migration;

class m170318_191502_insert_tiempo_comida_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('tiempo_comida',
            ['tiem_com_id',
            'tiem_com_nombre',
            'tiem_com_hora_inicio',
            'tiempo_com_hora_fin',
            ],
            [
                ['2', 'DESAYUNO', '08:00:00', '08:30:00'], ['3', 'REFRIGERIO_DE_LA_MAÑANA',
                '10:00:00', '10:30:00'], ['4', 'ALMUERZO', '12:00:00', '12:30:00'],
            ['5', 'REFRIGERIO_DE_LA_TARDE', '15:00:00', '15:30:00']
            ]
        );
    }

    public function down()
    {
         if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table tiempo_comida CASCADE;
SQL;
         $this->execute($truncate);}
         else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table tiempo_comida;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}