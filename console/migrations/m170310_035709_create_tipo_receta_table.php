<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tipo_receta`.
 * Has foreign keys to the tables:
 *
 * - `grupo_platillo`
 * - `tipo_preparacion_carta`
 */
class m170310_035709_create_tipo_receta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tipo_receta', [
            'tipo_receta_id' => $this->primaryKey(),
            'grupo_platillo_id' => $this->integer(),
            'tipo_prep_carta_id' => $this->integer(),
            'tipo_receta_nombre' => $this->string(45)->notNull(),
            'tipo_receta_descripcion' => $this->string(100)->notNull(),
            'tipo_receta_cantidad_max' => $this->decimal(5,2)->notNull(),
        ]);

        // creates index for column `grupo_platillo_id`
        $this->createIndex(
            'idx-tipo_receta-grupo_platillo_id',
            'tipo_receta',
            'grupo_platillo_id'
        );

        // add foreign key for table `grupo_platillo`
        $this->addForeignKey(
            'fk-tipo_receta-grupo_platillo_id',
            'tipo_receta',
            'grupo_platillo_id',
            'grupo_platillo',
            'grupo_platillo_id',
            'CASCADE'
        );

        // creates index for column `tipo_prep_carta_id`
        $this->createIndex(
            'idx-tipo_receta-tipo_prep_carta_id',
            'tipo_receta',
            'tipo_prep_carta_id'
        );

        // add foreign key for table `tipo_preparacion_carta`
        $this->addForeignKey(
            'fk-tipo_receta-tipo_prep_carta_id',
            'tipo_receta',
            'tipo_prep_carta_id',
            'tipo_preparacion_carta',
            'tipo_prep_carta_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `grupo_platillo`
        $this->dropForeignKey(
            'fk-tipo_receta-grupo_platillo_id',
            'tipo_receta'
        );

        // drops index for column `grupo_platillo_id`
        $this->dropIndex(
            'idx-tipo_receta-grupo_platillo_id',
            'tipo_receta'
        );

        // drops foreign key for table `tipo_preparacion_carta`
        $this->dropForeignKey(
            'fk-tipo_receta-tipo_prep_carta_id',
            'tipo_receta'
        );

        // drops index for column `tipo_prep_carta_id`
        $this->dropIndex(
            'idx-tipo_receta-tipo_prep_carta_id',
            'tipo_receta'
        );

        $this->dropTable('tipo_receta');
    }
}
