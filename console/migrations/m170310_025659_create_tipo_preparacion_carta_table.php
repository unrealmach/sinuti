<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tipo_preparacion_carta`.
 */
class m170310_025659_create_tipo_preparacion_carta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tipo_preparacion_carta', [
            'tipo_prep_carta_id' => $this->primaryKey(),
            'tipo_prep_carta_nombre' => $this->string(45)->notNull(),
            'tipo_prep_carta_descripcion' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tipo_preparacion_carta');
    }
}
