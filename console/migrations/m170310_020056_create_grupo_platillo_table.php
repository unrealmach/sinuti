<?php

use yii\db\Migration;

/**
 * Handles the creation for table `grupo_platillo`.
 */
class m170310_020056_create_grupo_platillo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('grupo_platillo', [
            'grupo_platillo_id' => $this->primaryKey(),
            'grupo_platillo_nombre' => $this->string(45)->notNull(),
            'grupo_platillo_descripcion' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('grupo_platillo');
    }
}
