<?php

use yii\db\Migration;

class m170318_183429_insert_categoria_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('categoria',
            ['categoria_id',
            'categoria_nombre',
            'categoria_descripcion',
            ],
            [
                ['1', 'CEREALES Y DERIVADOS', 'CEREALES Y DERIVADOS'], ['2', 'LEGUMINOSAS Y OLEAGINOSAS',
                'LEGUMINOSAS Y OLEAGINOSAS'], ['3', 'VERDURAS, HORTALIZAS Y DERIVADOS',
                'VERDURAS, HORTALIZAS Y DERIVADOS'], ['4', 'FRUTAS Y DERIVADOS',
                'FRUTAS Y DERIVADOS'], ['5', 'HUEVOS Y DERIVADOS', 'HUEVOS Y DERIVADOS'],
            ['6', 'PESCADOS Y MARISCOS', 'PESCADOS Y MARISCOS'], ['7', 'CARNES Y DERIVADOS',
                'CARNES Y DERIVADOS'], ['8', 'LECHE Y DERIVADOS', 'LECHE Y DERIVADOS'],
            ['9', 'GRASAS Y ACEITES', 'GRASAS Y ACEITES'], ['10', 'ESPECIES', 'ESPECIES'],
            ['11', 'ALIMENTOS INDUSTRIALIZADOS Y MISCELANEOS', 'ALIMENTOS INDUSTRIALIZADOS Y MISCELANEOS'],
            ['12', 'AZUCARES Y POSTRES', 'AZUCARES Y POSTRES'], ['13', 'BEBIDAS ALCOHÓLICAS Y NO ALCOHÓLICAS',
                'BEBIDAS ALCOHÓLICAS Y NO ALCOHÓLICAS'], ['14', 'ALIMENTOS PREPARADOS [RECETAS ESTANDARIZADAS]',
                'ALIMENTOS PREPARADOS [RECETAS ESTANDARIZADAS]'], ['15', 'PREPARACIONES DE CADENAS NACIONALES Y MULTINACIONALES',
                'PREPARACIONES DE CADENAS NACIONALES Y MULTINACIONALES']
            ]
        );
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table categoria CASCADE;
SQL;
        $this->execute($truncate);}
        else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table categoria;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}