<?php

use yii\db\Migration;

class m170326_173052_create_check_cambio_correcto_procedure extends Migration {

    public function up() {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE OR REPLACE FUNCTION check_periodo() returns boolean AS $$
DECLARE
  check_cambio_correcto boolean := FALSE;
BEGIN

check_cambio_correcto:= (SELECT 
				NOW() BETWEEN concat(periodo.fecha_inicio,' ','00:00:00' )::TIMESTAMP
				AND  concat(periodo.fecha_fin,' ','23:59:59')::TIMESTAMP
			as check FROM  periodo where estado =  'ACTIVO' );

IF check_cambio_correcto = FALSE 
THEN -- si el dia actual esta fuera del rango de comparacion, ejecuta los triggers
    UPDATE  sector_asignado_educador SET  fecha_fin_actividad =  now()  where sector_asignado_educador.fecha_fin_actividad is NULL; 
    UPDATE autoridades_cibv set fecha_fin_actividad = now() where autoridades_cibv.fecha_fin_actividad is NULL; 	
    UPDATE asignacion_gastronomo_distrito set fecha_fin_actividad = now() where asignacion_gastronomo_distrito.fecha_fin_actividad is NULL;    
    UPDATE asignacion_nutricionista_distrito set fecha_fin_actividad = now() where asignacion_nutricionista_distrito.fecha_fin_actividad is NULL;
       
    UPDATE  matricula SET  matricula_estado =  'RETIRADO' WHERE  matricula.matricula_estado ='ACTIVO';
    UPDATE  periodo SET  estado =  'INACTIVO' WHERE  periodo.estado = 'ACTIVO';
END IF;

return check_cambio_correcto;
END;
$$ LANGUAGE plpgsql;
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS check_periodo() CASCADE;");
            $this->execute($createFunctionSql);
        } else if ($this->db->driverName === 'mysql') {
            $createFunctionSql = <<< SQL

CREATE PROCEDURE `check_periodo`()
    DETERMINISTIC
    COMMENT 'First SP at Expertdeveloper'
BEGIN

SET @check_cambio_correcto := 
    (SELECT IF(estado =  "ACTIVO" AND NOW() BETWEEN concat(periodo.fecha_inicio,' ','00:00:00' )
									AND concat(periodo.fecha_fin,' ','23:59:59'),1,0) as data FROM  `periodo` );
    select @check_cambio_correcto;
    
    IF @check_cambio_correcto = 0 THEN  
		
    UPDATE  sector_asignado_educador SET  sector_asignado_educador.fecha_fin_actividad =  now()  where sector_asignado_educador.fecha_fin_actividad is NULL; -- triger 2
    UPDATE autoridades_cibv set autoridades_cibv.fecha_fin_actividad = now() where autoridades_cibv.fecha_fin_actividad is NULL; -- trigger 1
	UPDATE asignacion_gastronomo_distrito set asignacion_gastronomo_distrito.fecha_fin_actividad = now() where asignacion_gastronomo_distrito.fecha_fin_actividad is NULL; -- triger 3
    UPDATE asignacion_nutricionista_distrito set asignacion_nutricionista_distrito.fecha_fin_actividad = now() where asignacion_nutricionista_distrito.fecha_fin_actividad is NULL;
       
        
	 UPDATE  `matricula` SET  `matricula_estado` =  'RETIRADO' WHERE  `matricula`.`matricula_estado` ='ACTIVO';
	 UPDATE  `periodo` SET  `estado` =  'INACTIVO' WHERE  `periodo`.`estado` = 'ACTIVO';
        
    END IF; 
END

SQL;
            $this->execute("DROP PROCEDURE IF EXISTS check_periodo;");
            $this->execute($createFunctionSql);
        }
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP  FUNCTION IF EXISTS check_periodo() CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP PROCEDURE IF EXISTS check_periodo;");
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
