<?php

use yii\db\Migration;

/**
 * Handles the creation for table `m_prep_carta`.
 * Has foreign keys to the tables:
 *
 * - `tiempo_comida`
 * - `grupo_edad`
 */
class m170310_140711_create_m_prep_carta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('m_prep_carta', [
            'm_prep_carta_id' => $this->primaryKey(),
            'm_prep_carta_nombre' => $this->string(100)->notNull(),
            'm_prep_carta_descripcion' => $this->text()->notNull(),
            'tiempo_comida_id' => $this->integer()->notNull(),
            'grupo_edad_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `tiempo_comida_id`
        $this->createIndex(
            'idx-m_prep_carta-tiempo_comida_id',
            'm_prep_carta',
            'tiempo_comida_id'
        );

        // add foreign key for table `tiempo_comida`
        $this->addForeignKey(
            'fk-m_prep_carta-tiempo_comida_id',
            'm_prep_carta',
            'tiempo_comida_id',
            'tiempo_comida',
            'tiem_com_id',
            'CASCADE'
        );

        // creates index for column `grupo_edad_id`
        $this->createIndex(
            'idx-m_prep_carta-grupo_edad_id',
            'm_prep_carta',
            'grupo_edad_id'
        );

        // add foreign key for table `grupo_edad`
        $this->addForeignKey(
            'fk-m_prep_carta-grupo_edad_id',
            'm_prep_carta',
            'grupo_edad_id',
            'grupo_edad',
            'grupo_edad_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tiempo_comida`
        $this->dropForeignKey(
            'fk-m_prep_carta-tiempo_comida_id',
            'm_prep_carta'
        );

        // drops index for column `tiempo_comida_id`
        $this->dropIndex(
            'idx-m_prep_carta-tiempo_comida_id',
            'm_prep_carta'
        );

        // drops foreign key for table `grupo_edad`
        $this->dropForeignKey(
            'fk-m_prep_carta-grupo_edad_id',
            'm_prep_carta'
        );

        // drops index for column `grupo_edad_id`
        $this->dropIndex(
            'idx-m_prep_carta-grupo_edad_id',
            'm_prep_carta'
        );

        $this->dropTable('m_prep_carta');
    }
}
