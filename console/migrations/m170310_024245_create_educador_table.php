<?php

use yii\db\Migration;

/**
 * Handles the creation for table `educador`.
 */
class m170310_024245_create_educador_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('educador', [
            'educador_id' => $this->primaryKey(),
            'educador_dni' => $this->string(45)->notNull(),
            'educador_nombres' => $this->string(150)->notNull(),
            'educador_apellidos' => $this->string(150)->notNUll(),
            'educador_telefono' => $this->string(15)->notNull(),
            'educador_correo' => $this->string(150)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('educador');
    }
}
