<?php

use yii\db\Migration;

/**
 * Handles the creation for table `autoridades_cibv`.
 * Has foreign keys to the tables:
 *
 * - `periodo`
 * - `coordinador_cibv`
 * - `cibv`
 */
class m170311_015620_create_autoridades_cibv_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('autoridades_cibv', [
            'autoridades_cibv_id' => $this->primaryKey(),
            'periodo_id' => $this->integer()->notNull(),
            'coordinador_id' => $this->integer()->notNull(),
            'cen_inf_id' => $this->integer()->notNull(),
            'fecha_inicio_actividad' => $this->date()->notNull(),
            'fecha_fin_actividad' => $this->date()->defaultValue(NULL),
        ]);

        // creates index for column `periodo_id`
        $this->createIndex(
            'idx-autoridades_cibv-periodo_id',
            'autoridades_cibv',
            'periodo_id'
        );

        // add foreign key for table `periodo`
        $this->addForeignKey(
            'fk-autoridades_cibv-periodo_id',
            'autoridades_cibv',
            'periodo_id',
            'periodo',
            'periodo_id',
            'CASCADE'
        );

        // creates index for column `coordinador_id`
        $this->createIndex(
            'idx-autoridades_cibv-coordinador_id',
            'autoridades_cibv',
            'coordinador_id'
        );

        // add foreign key for table `coordinador_cibv`
        $this->addForeignKey(
            'fk-autoridades_cibv-coordinador_id',
            'autoridades_cibv',
            'coordinador_id',
            'coordinador_cibv',
            'coordinador_id',
            'CASCADE'
        );

        // creates index for column `cen_inf_id`
        $this->createIndex(
            'idx-autoridades_cibv-cen_inf_id',
            'autoridades_cibv',
            'cen_inf_id'
        );

        // add foreign key for table `cibv`
        $this->addForeignKey(
            'fk-autoridades_cibv-cen_inf_id',
            'autoridades_cibv',
            'cen_inf_id',
            'cibv',
            'cen_inf_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `periodo`
        $this->dropForeignKey(
            'fk-autoridades_cibv-periodo_id',
            'autoridades_cibv'
        );

        // drops index for column `periodo_id`
        $this->dropIndex(
            'idx-autoridades_cibv-periodo_id',
            'autoridades_cibv'
        );

        // drops foreign key for table `coordinador_cibv`
        $this->dropForeignKey(
            'fk-autoridades_cibv-coordinador_id',
            'autoridades_cibv'
        );

        // drops index for column `coordinador_id`
        $this->dropIndex(
            'idx-autoridades_cibv-coordinador_id',
            'autoridades_cibv'
        );

        // drops foreign key for table `cibv`
        $this->dropForeignKey(
            'fk-autoridades_cibv-cen_inf_id',
            'autoridades_cibv'
        );

        // drops index for column `cen_inf_id`
        $this->dropIndex(
            'idx-autoridades_cibv-cen_inf_id',
            'autoridades_cibv'
        );

        $this->dropTable('autoridades_cibv');
    }
}
