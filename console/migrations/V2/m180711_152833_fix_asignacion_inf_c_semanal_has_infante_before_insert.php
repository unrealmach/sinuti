<?php
use yii\db\Migration;

class m180711_152833_fix_asignacion_inf_c_semanal_has_infante_before_insert extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION asignacion_inf_c_semanal_has_infante_BEFORE_INSERT()
RETURNS trigger AS '
BEGIN
     NEW.anio = extract(YEAR from NOW());
     NEW.num_semana = extract(WEEK from NOW());
     RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER asignacion_inf_c_semanal_has_infante_BEFORE_INSERT
BEFORE INSERT ON asignacion_inf_c_semanal_has_infante
FOR EACH ROW
EXECUTE PROCEDURE asignacion_inf_c_semanal_has_infante_BEFORE_INSERT();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS asignacion_inf_c_semanal_has_infante_BEFORE_INSERT() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS asignacion_inf_c_semanal_has_infante_BEFORE_INSERT ON asignacion_inf_c_semanal_has_infante CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP  FUNCTION IF EXISTS asignacion_inf_c_semanal_has_infante_BEFORE_INSERT() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS asignacion_inf_c_semanal_has_infante_BEFORE_INSERT ON asignacion_inf_c_semanal_has_infante CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP TRIGGER IF EXISTS `asignacion_inf_c_semanal_has_infante_BEFORE_INSERT`;");
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
