<?php

use yii\db\Migration;

/**
 * Class m190401_230356_create_upper_observaciones_asig_user_coordinador_cibv_before_update_trigger
 */
class m190401_230356_create_upper_observaciones_asig_user_coordinador_cibv_before_update_trigger extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_observaciones_asig_user_coordinador_cibv_before_update_trigger()
RETURNS trigger AS '
BEGIN
     NEW.observaciones := UPPER(NEW.observaciones);
       RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_observaciones_asig_user_coordinador_cibv_before_update_trigger
BEFORE UPDATE ON asignacion_user_coordinador_cibv
FOR EACH ROW
EXECUTE PROCEDURE upper_observaciones_asig_user_coordinador_cibv_before_update_trigger();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_observaciones_asig_user_coordinador_cibv_before_update_trigger() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_observaciones_asig_user_coordinador_cibv_before_update_trigger ON infante CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP FUNCTION IF EXISTS upper_observaciones_asig_user_coordinador_cibv_before_update_trigger() CASCADE;");
            $result = $this->execute("DROP TRIGGER IF EXISTS upper_observaciones_asig_user_coordinador_cibv_before_update_trigger ON infante CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
//        echo $result;
//        echo "m170306_032951_create_infante_trigger cannot be reverted.\n";
//        return false;
    }
    
}
