<?php

use yii\db\Migration;

/**
 * Class m190401_230816_create_upper_texts_asignacion_user_educador_cibv_insert_trigger
 */
class m190401_230816_create_upper_texts_asignacion_user_educador_cibv_insert_trigger extends Migration
{
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_texts_asignacion_user_educador_cibv_insert()
RETURNS trigger AS '
BEGIN
     NEW.observaciones := UPPER(NEW.observaciones);
       RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_texts_asignacion_user_educador_cibv_insert
BEFORE INSERT ON asignacion_user_educador_cibv
FOR EACH ROW
EXECUTE PROCEDURE upper_texts_asignacion_user_educador_cibv_insert();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_texts_asignacion_user_educador_cibv_insert() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_texts_asignacion_user_educador_cibv_insert ON infante CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP FUNCTION IF EXISTS upper_texts_asignacion_user_educador_cibv_insert() CASCADE;");
            $result = $this->execute("DROP TRIGGER IF EXISTS upper_texts_asignacion_user_educador_cibv_insert ON infante CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
//        echo $result;
//        echo "m170306_032951_create_infante_trigger cannot be reverted.\n";
//        return false;
    }
    
}