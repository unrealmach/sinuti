<?php

use yii\db\Migration;

/**
 * Class m190402_005050_create_upper_texts_parroquia_insert_trigger
 */
class m190402_005050_create_upper_texts_parroquia_insert_trigger extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION create_upper_texts_parroquia_insert_trigger()
RETURNS trigger AS '
BEGIN
     NEW.parroquia_nombre := UPPER(NEW.parroquia_nombre);
     
     
       RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER create_upper_texts_parroquia_insert_trigger
BEFORE INSERT ON parroquia
FOR EACH ROW
EXECUTE PROCEDURE create_upper_texts_parroquia_insert_trigger();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS create_upper_texts_parroquia_insert_trigger() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS create_upper_texts_parroquia_insert_trigger ON infante CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP FUNCTION IF EXISTS create_upper_texts_parroquia_insert_trigger() CASCADE;");
            $result = $this->execute("DROP TRIGGER IF EXISTS create_upper_texts_parroquia_insert_trigger ON infante CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            die("FALTA IMPLEMENTAR PARA MYSQL");
        }
//        echo $result;
//        echo "m170306_032951_create_infante_trigger cannot be reverted.\n";
//        return false;
    }
    
}   