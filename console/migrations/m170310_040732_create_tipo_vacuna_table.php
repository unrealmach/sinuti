<?php
use yii\db\Migration;

/**
 * Handles the creation for table `tipo_vacuna`.
 */
class m170310_040732_create_tipo_vacuna_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS  tipo_vac_dosis_medida CASCADE");
            $this->execute("DROP TYPE IF EXISTS  tipo_vac_via CASCADE");
            $this->execute("CREATE TYPE tipo_vac_dosis_medida AS  ENUM('ML', 'GOTAS');");
            $this->execute("CREATE TYPE tipo_vac_via AS  ENUM('I.D.', 'V.O.', 'IM', 'VS');");
            $this->createTable('tipo_vacuna', [
                'tipo_vacuna_id' => $this->primaryKey(),
                'tipo_vac_enfermedad' => $this->string(45)->notNull(),
                'tipo_vac_nombre' => $this->string(45)->notNull(),
                'tipo_vac_num_dosis' => $this->integer()->notNull(),
                'tipo_vac_dosis_canti' => $this->decimal(4, 2)->notNull(),
                'tipo_vac_dosis_medida' => "tipo_vac_dosis_medida NOT NULL",
                'tipo_vac_via' => "tipo_vac_via NOT NULL",
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('tipo_vacuna', [
                'tipo_vacuna_id' => $this->primaryKey(),
                'tipo_vac_enfermedad' => $this->string(45)->notNull(),
                'tipo_vac_nombre' => $this->string(45)->notNull(),
                'tipo_vac_num_dosis' => $this->integer()->notNull(),
                'tipo_vac_dosis_canti' => $this->decimal(4, 2)->notNull(),
                'tipo_vac_dosis_medida' => "ENUM('ML', 'GOTAS') NOT NULL",
                'tipo_vac_via' => "ENUM('I.D.', 'V.O.', 'IM', 'VS') NOT NULL",
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS  tipo_vac_dosis_medida CASCADE");
            $this->execute("DROP TYPE IF EXISTS tipo_vac_via CASCADE");
        }
        $this->dropTable('tipo_vacuna');
    }
}
