<?php
use yii\db\Migration;

/**
 * Handles the creation for table `comite_padres_flia`.
 * Has foreign keys to the tables:
 *
 * - `grupo_comite`
 */
class m170310_152214_create_comite_padres_flia_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS cargo_comite CASCADE");
            $this->execute("CREATE TYPE cargo_comite AS  ENUM('PRESIDENTE','VICEPRESIDENTE','TESORERO','SECRETARIO');");
            $this->createTable('comite_padres_flia', [
                'comite_id' => $this->primaryKey(),
                'comite_nombres' => $this->string(150)->notNull(),
                'comite_apellidos' => $this->string(150)->notNull(),
                'comite_dni' => $this->string(45)->notNull(),
                'comite_numero_contacto' => $this->string(15)->notNull(),
                'grupo_comite_id' => $this->integer(),
                'cargo' => "cargo_comite NOT NULL",
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('comite_padres_flia', [
                'comite_id' => $this->primaryKey(),
                'comite_nombres' => $this->string(150)->notNull(),
                'comite_apellidos' => $this->string(150)->notNull(),
                'comite_dni' => $this->string(45)->notNull(),
                'comite_numero_contacto' => $this->string(15)->notNull(),
                'grupo_comite_id' => $this->integer(),
                'cargo' => "ENUM('PRESIDENTE','VICEPRESIDENTE','TESORERO','SECRETARIO') NOT NULL",
            ]);
        }


        // creates index for column `grupo_comite_id`
        $this->createIndex(
            'idx-comite_padres_flia-grupo_comite_id', 'comite_padres_flia', 'grupo_comite_id'
        );

        // add foreign key for table `grupo_comite`
        $this->addForeignKey(
            'fk-comite_padres_flia-grupo_comite_id', 'comite_padres_flia', 'grupo_comite_id', 'grupo_comite', 'grupo_comite_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS cargo_comite CASCADE");
        }

        // drops foreign key for table `grupo_comite`
        $this->dropForeignKey(
            'fk-comite_padres_flia-grupo_comite_id', 'comite_padres_flia'
        );

        // drops index for column `grupo_comite_id`
        $this->dropIndex(
            'idx-comite_padres_flia-grupo_comite_id', 'comite_padres_flia'
        );

        $this->dropTable('comite_padres_flia');
    }
}
