<?php

use yii\db\Migration;

/**
 * Handles the creation for table `nutriente`.
 */
class m170310_024958_create_nutriente_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nutriente', [
            'nutriente_id' => $this->primaryKey(),
            'nutriente_nombre' => $this->string(45)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nutriente');
    }
}
