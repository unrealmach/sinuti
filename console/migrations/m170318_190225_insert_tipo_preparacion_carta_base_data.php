<?php

use yii\db\Migration;

class m170318_190225_insert_tipo_preparacion_carta_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('tipo_preparacion_carta',
            ['tipo_prep_carta_id',
            'tipo_prep_carta_nombre',
            'tipo_prep_carta_descripcion',
            ],
            [
                ['1', 'sopa', 'sopa'], ['2', 'acompañado', 'acompañado'], ['3', 'plato fuerte',
                'plato fuerte'], ['4', 'ensalada', 'ensalada'], ['5', 'fruta', 'fruta'],
            ['6', 'liquido', 'liquido'], ['7', 'otros', 'otros'], ['8', 'solido',
                'solido'], ['9', 'jugo', 'jugo']
        ]);
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table tipo_preparacion_carta CASCADE;
SQL;
        $this->execute($truncate);}
        else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table tipo_preparacion_carta;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}