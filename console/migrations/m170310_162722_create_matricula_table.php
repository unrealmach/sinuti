<?php
use yii\db\Migration;

/**
 * Handles the creation for table `matricula`.
 * Has foreign keys to the tables:
 *
 * - `cibv`
 * - `infante`
 * - `periodo`
 */
class m170310_162722_create_matricula_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS matricula_estado CASCADE");
            $this->execute("CREATE TYPE matricula_estado AS  ENUM('ACTIVO', 'RETIRADO');");
            $this->createTable('matricula', [
                'matricula_id' => $this->primaryKey(),
                'cen_inf_id' => $this->integer()->notNull(),
                'infante_id' => $this->integer()->notNull(),
                'periodo_id' => $this->integer()->notNull(),
                'matricula_estado' => "matricula_estado NOT NULL ",
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('matricula', [
                'matricula_id' => $this->primaryKey(),
                'cen_inf_id' => $this->integer()->notNull(),
                'infante_id' => $this->integer()->notNull(),
                'periodo_id' => $this->integer()->notNull(),
                'matricula_estado' => " ENUM('ACTIVO', 'RETIRADO') NOT NULL ",
            ]);
        }

        // creates index for column `cen_inf_id`
        $this->createIndex(
            'idx-matricula-cen_inf_id', 'matricula', 'cen_inf_id'
        );

        // add foreign key for table `cibv`
        $this->addForeignKey(
            'fk-matricula-cen_inf_id', 'matricula', 'cen_inf_id', 'cibv', 'cen_inf_id', 'CASCADE'
        );

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-matricula-infante_id', 'matricula', 'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-matricula-infante_id', 'matricula', 'infante_id', 'infante', 'infante_id', 'CASCADE'
        );

        // creates index for column `periodo_id`
        $this->createIndex(
            'idx-matricula-periodo_id', 'matricula', 'periodo_id'
        );

        // add foreign key for table `periodo`
        $this->addForeignKey(
            'fk-matricula-periodo_id', 'matricula', 'periodo_id', 'periodo', 'periodo_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS matricula_estado CASCADE");
        }
        // drops foreign key for table `cibv`
        $this->dropForeignKey(
            'fk-matricula-cen_inf_id', 'matricula'
        );

        // drops index for column `cen_inf_id`
        $this->dropIndex(
            'idx-matricula-cen_inf_id', 'matricula'
        );

        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-matricula-infante_id', 'matricula'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-matricula-infante_id', 'matricula'
        );

        // drops foreign key for table `periodo`
        $this->dropForeignKey(
            'fk-matricula-periodo_id', 'matricula'
        );

        // drops index for column `periodo_id`
        $this->dropIndex(
            'idx-matricula-periodo_id', 'matricula'
        );

        $this->dropTable('matricula');
    }
}
