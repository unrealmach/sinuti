<?php

use yii\db\Migration;

class m170318_173527_insert_localidad_base_data extends Migration {

    public function up() {
        $this->batchInsert('localidad', ['localidad_id', 'localidad_codigo', 'localidad_nombre', 'distrito_id'], [
                ['1', '1001', 'IBARRA', '1'], ['2', '1002', 'ANTONIO ANTE', '1'],
                ['3', '1003', 'COTACACHI', '1'], ['4', '1004', 'OTAVALO', '1'], ['5',
                '1005', 'PIMAMPIRO', '1'], ['6', '1006', 'URCUQUÍ', '1']
                ]
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncate = <<< SQL
truncate table localidad CASCADE;
SQL;
            $this->execute($truncate);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table localidad;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
