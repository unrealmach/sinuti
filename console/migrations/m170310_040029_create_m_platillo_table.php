<?php

use yii\db\Migration;

/**
 * Handles the creation for table `m_platillo`.
 * Has foreign keys to the tables:
 *
 * - `tipo_receta`
 */
class m170310_040029_create_m_platillo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('m_platillo', [
            'm_platillo_id' => $this->primaryKey(),
            'tipo_receta_id' => $this->integer(),
            'm_platillo_nombre' => $this->text()->notNull(),
            'm_platillo_descripcion' => $this->text()->notNull(),
            'm_platillo_cantidad_total_g' => $this->decimal(6,2)->notNull(),
        ]);

        // creates index for column `tipo_receta_id`
        $this->createIndex(
            'idx-m_platillo-tipo_receta_id',
            'm_platillo',
            'tipo_receta_id'
        );

        // add foreign key for table `tipo_receta`
        $this->addForeignKey(
            'fk-m_platillo-tipo_receta_id',
            'm_platillo',
            'tipo_receta_id',
            'tipo_receta',
            'tipo_receta_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `tipo_receta`
        $this->dropForeignKey(
            'fk-m_platillo-tipo_receta_id',
            'm_platillo'
        );

        // drops index for column `tipo_receta_id`
        $this->dropIndex(
            'idx-m_platillo-tipo_receta_id',
            'm_platillo'
        );

        $this->dropTable('m_platillo');
    }
}
