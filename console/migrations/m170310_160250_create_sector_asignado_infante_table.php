<?php

use yii\db\Migration;

/**
 * Handles the creation for table `sector_asignado_infante`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 * - `sector_asignado_educador`
 */
class m170310_160250_create_sector_asignado_infante_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sector_asignado_infante', [
            'sector_asignado_infante_id' => $this->primaryKey(),
            'infante_id' => $this->integer(),
            'sector_asignado_id' => $this->integer(),
        ]);

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-sector_asignado_infante-infante_id',
            'sector_asignado_infante',
            'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-sector_asignado_infante-infante_id',
            'sector_asignado_infante',
            'infante_id',
            'infante',
            'infante_id',
            'CASCADE'
        );

        // creates index for column `sector_asignado_id`
        $this->createIndex(
            'idx-sector_asignado_infante-sector_asignado_id',
            'sector_asignado_infante',
            'sector_asignado_id'
        );

        // add foreign key for table `sector_asignado_educador`
        $this->addForeignKey(
            'fk-sector_asignado_infante-sector_asignado_id',
            'sector_asignado_infante',
            'sector_asignado_id',
            'sector_asignado_educador',
            'sector_asignado_educador_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-sector_asignado_infante-infante_id',
            'sector_asignado_infante'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-sector_asignado_infante-infante_id',
            'sector_asignado_infante'
        );

        // drops foreign key for table `sector_asignado_educador`
        $this->dropForeignKey(
            'fk-sector_asignado_infante-sector_asignado_id',
            'sector_asignado_infante'
        );

        // drops index for column `sector_asignado_id`
        $this->dropIndex(
            'idx-sector_asignado_infante-sector_asignado_id',
            'sector_asignado_infante'
        );

        $this->dropTable('sector_asignado_infante');
    }
}
