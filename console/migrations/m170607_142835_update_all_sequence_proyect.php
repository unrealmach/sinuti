<?php
use yii\db\Migration;

class m170607_142835_update_all_sequence_proyect extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $fn_fixsequences = <<< SQL
CREATE OR REPLACE FUNCTION fn_fixsequences() RETURNS void AS
$$
DECLARE

BEGIN
 PERFORM setval('alimento_alimento_id_seq', (SELECT max(alimento_id) from alimento), true);
                
PERFORM setval('asignacion_gastronomo_distrit_asignacion_gastronomo_distrit_seq', (SELECT max(asignacion_gastronomo_distrito_id) from asignacion_gastronomo_distrito), true);
                
PERFORM setval('asignacion_inf_c_semanal_has_infante_asignacion_junction_id_seq', (SELECT max(asignacion_junction_id) from asignacion_inf_c_semanal_has_infante), true);
                
PERFORM setval('asignacion_infante_c_semanal_asig_inf_c_sem_id_seq', (SELECT max(asig_inf_c_sem_id) from asignacion_infante_c_semanal), true);
                
PERFORM setval('asignacion_nutricionista_dist_asignacion_nutricionista_dist_seq', (SELECT max(asignacion_nutricionista_distrito_id) from asignacion_nutricionista_distrito), true);
                
PERFORM setval('asignacion_user_coordinador_cibv_asignacion_user_cibv_id_seq', (SELECT max(asignacion_user_cibv_id) from asignacion_user_coordinador_cibv), true);
                
PERFORM setval('asignacion_user_educador_cibv_asignacion_user_educador_cibv_seq', (SELECT max(asignacion_user_educador_cibv_id) from asignacion_user_educador_cibv), true);
                
PERFORM setval('asignacion_user_gastronomo_di_asignacion_user_gastronomo_di_seq', (SELECT max(asignacion_user_gastronomo_distrito_id) from asignacion_user_gastronomo_distrito), true);
                
PERFORM setval('asignacion_user_nutricionista_asignacion_user_nutricionista_seq', (SELECT max(asignacion_user_nutricionista_distrito_id) from asignacion_user_nutricionista_distrito), true);
                
PERFORM setval('asistencia_asistencia_id_seq', (SELECT max(asistencia_id) from asistencia), true);
                
PERFORM setval('autoridades_cibv_autoridades_cibv_id_seq', (SELECT max(autoridades_cibv_id) from autoridades_cibv), true);
                
PERFORM setval('bitacora_consumo_infantil_b_cons_inf_id_seq', (SELECT max(b_cons_inf_id) from bitacora_consumo_infantil), true);
                
PERFORM setval('categoria_categoria_id_seq', (SELECT max(categoria_id) from categoria), true);
                
PERFORM setval('cibv_cen_inf_id_seq', (SELECT max(cen_inf_id) from cibv), true);
                
PERFORM setval('comite_padres_flia_comite_id_seq', (SELECT max(comite_id) from comite_padres_flia), true);
                
PERFORM setval('composicion_nutricional_com_nut_id_seq', (SELECT max(com_nut_id) from composicion_nutricional), true);
                
PERFORM setval('composicion_nutricional_platillo_com_nut_platillo_id_seq', (SELECT max(com_nut_platillo_id) from composicion_nutricional_platillo), true);
                
PERFORM setval('coordinador_cibv_coordinador_id_seq', (SELECT max(coordinador_id) from coordinador_cibv), true);
                
PERFORM setval('d_carta_semanal_d_c_sem_id_seq', (SELECT max(d_c_sem_id) from d_carta_semanal), true);
                
PERFORM setval('d_platillo_d_platillo_id_seq', (SELECT max(d_platillo_id) from d_platillo), true);
                
PERFORM setval('d_preparacion_carta_d_prep_carta_id_seq', (SELECT max(d_prep_carta_id) from d_preparacion_carta), true);
                
PERFORM setval('dato_antropometrico_dat_antro_id_seq', (SELECT max(dat_antro_id) from dato_antropometrico), true);
                
PERFORM setval('distrito_distrito_id_seq', (SELECT max(distrito_id) from distrito), true);
                
PERFORM setval('educador_educador_id_seq', (SELECT max(educador_id) from educador), true);
     
PERFORM setval('gastronomo_gastronomo_id_seq', (SELECT max(gastronomo_id) from gastronomo), true);
     
PERFORM setval('grupo_comite_grupo_comite_id_seq', (SELECT max(grupo_comite_id) from grupo_comite), true);
     
PERFORM setval('grupo_edad_grupo_edad_id_seq', (SELECT max(grupo_edad_id) from grupo_edad), true);
     
PERFORM setval('grupo_platillo_grupo_platillo_id_seq', (SELECT max(grupo_platillo_id) from grupo_platillo), true);
     
PERFORM setval('infante_infante_id_seq', (SELECT max(infante_id) from infante), true);
     
PERFORM setval('localidad_localidad_id_seq', (SELECT max(localidad_id) from localidad), true);
     
PERFORM setval('m_carta_semanal_m_c_sem_id_seq', (SELECT max(m_c_sem_id) from m_carta_semanal), true);
     
PERFORM setval('m_platillo_m_platillo_id_seq', (SELECT max(m_platillo_id) from m_platillo), true);
     
PERFORM setval('m_prep_carta_m_prep_carta_id_seq', (SELECT max(m_prep_carta_id) from m_prep_carta), true);
     
PERFORM setval('matricula_matricula_id_seq', (SELECT max(matricula_id) from matricula), true);
     
PERFORM setval('nutricionista_nutricionista_id_seq', (SELECT max(nutricionista_id) from nutricionista), true);
     
PERFORM setval('nutriente_nutriente_id_seq', (SELECT max(nutriente_id) from nutriente), true);
     
PERFORM setval('parroquia_parroquia_id_seq', (SELECT max(parroquia_id) from parroquia), true);
     
PERFORM setval('patologia_patologia_id_seq', (SELECT max(patologia_id) from patologia), true);
     
PERFORM setval('periodo_periodo_id_seq', (SELECT max(periodo_id) from periodo), true);
     
PERFORM setval('porcion_por_alimento_porc_alimento_id_seq', (SELECT max(porc_alimento_id) from porcion_por_alimento), true);
     
PERFORM setval('rango_nutriente_ran_nut_id_seq', (SELECT max(ran_nut_id) from rango_nutriente), true);
     
PERFORM setval('registro_vacuna_infante_reg_vac_inf_id_seq', (SELECT max(reg_vac_inf_id) from registro_vacuna_infante), true);
     
PERFORM setval('salon_salon_id_seq', (SELECT max(salon_id) from salon), true);
     
PERFORM setval('sector_asignado_educador_sector_asignado_educador_id_seq', (SELECT max(sector_asignado_educador_id) from sector_asignado_educador), true);
     
PERFORM setval('sector_asignado_infante_sector_asignado_infante_id_seq', (SELECT max(sector_asignado_infante_id) from sector_asignado_infante), true);
     
PERFORM setval('tiempo_comida_tiem_com_id_seq', (SELECT max(tiem_com_id) from tiempo_comida), true);
     
PERFORM setval('tipo_preparacion_carta_tipo_prep_carta_id_seq', (SELECT max(tipo_prep_carta_id) from tipo_preparacion_carta), true);
     
PERFORM setval('tipo_receta_tipo_receta_id_seq', (SELECT max(tipo_receta_id) from tipo_receta), true);
     
PERFORM setval('tipo_vacuna_tipo_vacuna_id_seq', (SELECT max(tipo_vacuna_id) from tipo_vacuna), true);
     
PERFORM setval('user_id_seq', (SELECT max(id) from "user"), true);
     
END;
$$
LANGUAGE 'plpgsql' VOLATILE;


     
                
SQL;
            $this->execute($fn_fixsequences);
            $this->execute("SELECT fn_fixsequences();");
        }
    }

    public function down()
    {

        return true;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
