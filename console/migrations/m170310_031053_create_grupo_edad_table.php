<?php

use yii\db\Migration;

/**
 * Handles the creation for table `grupo_edad`.
 */
class m170310_031053_create_grupo_edad_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('grupo_edad', [
            'grupo_edad_id' => $this->primaryKey(),
            'grupo_edad_descripcion' => $this->string(45)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('grupo_edad');
    }
}
