<?php

use yii\db\Migration;

/**
 * Handles the creation for table `patologia`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 */
class m170309_155054_create_patologia_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('patologia', [
            'patologia_id' => $this->primaryKey(),
            'infante_id' => $this->integer()->notNull(),
            'patologia_pregunta_1' => $this->text()->notNull(),
            'patologia_pregunta_2' => $this->text()->notNull(),
            'patologia_pregunta_3' => $this->text()->notNull(),
            'patologia_pregunta_4' => $this->text()->notNull(),
        ]);

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-patologia-infante_id',
            'patologia',
            'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-patologia-infante_id',
            'patologia',
            'infante_id',
            'infante',
            'infante_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-patologia-infante_id',
            'patologia'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-patologia-infante_id',
            'patologia'
        );

        $this->dropTable('patologia');
    }
}
