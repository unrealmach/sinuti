<?php
use yii\db\Migration;

/**
 * Handles the creation for table `d_carta_semanal`.
 * Has foreign keys to the tables:
 *
 * - `m_carta_semanal`
 * - `m_prep_carta`
 */
class m170311_012023_create_d_carta_semanal_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS d_c_sem_dia_semana CASCADE");
            $this->execute("CREATE TYPE d_c_sem_dia_semana AS  ENUM('LUNES','MARTES','MIERCOLES','JUEVES','VIERNES');");

            $this->createTable('d_carta_semanal', [
                'd_c_sem_id' => $this->primaryKey(),
                'd_c_sem_dia_semana' => "d_c_sem_dia_semana NOT NULL",
                'm_carta_semanal_id' => $this->integer()->notNull(),
                'm_prep_carta_id' => $this->integer()->notNull(),
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('d_carta_semanal', [
                'd_c_sem_id' => $this->primaryKey(),
                'd_c_sem_dia_semana' => "ENUM('LUNES','MARTES','MIERCOLES','JUEVES','VIERNES') NOT NULL",
                'm_carta_semanal_id' => $this->integer()->notNull(),
                'm_prep_carta_id' => $this->integer()->notNull(),
            ]);
        }

        // creates index for column `m_carta_semanal_id`
        $this->createIndex(
            'idx-d_carta_semanal-m_carta_semanal_id', 'd_carta_semanal', 'm_carta_semanal_id'
        );

        // add foreign key for table `m_carta_semanal`
        $this->addForeignKey(
            'fk-d_carta_semanal-m_carta_semanal_id', 'd_carta_semanal', 'm_carta_semanal_id', 'm_carta_semanal', 'm_c_sem_id', 'CASCADE'
        );

        // creates index for column `m_prep_carta_id`
        $this->createIndex(
            'idx-d_carta_semanal-m_prep_carta_id', 'd_carta_semanal', 'm_prep_carta_id'
        );

        // add foreign key for table `m_prep_carta`
        $this->addForeignKey(
            'fk-d_carta_semanal-m_prep_carta_id', 'd_carta_semanal', 'm_prep_carta_id', 'm_prep_carta', 'm_prep_carta_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS d_c_sem_dia_semana CASCADE");
        }
        // drops foreign key for table `m_carta_semanal`
        $this->dropForeignKey(
            'fk-d_carta_semanal-m_carta_semanal_id', 'd_carta_semanal'
        );

        // drops index for column `m_carta_semanal_id`
        $this->dropIndex(
            'idx-d_carta_semanal-m_carta_semanal_id', 'd_carta_semanal'
        );

        // drops foreign key for table `m_prep_carta`
        $this->dropForeignKey(
            'fk-d_carta_semanal-m_prep_carta_id', 'd_carta_semanal'
        );

        // drops index for column `m_prep_carta_id`
        $this->dropIndex(
            'idx-d_carta_semanal-m_prep_carta_id', 'd_carta_semanal'
        );

        $this->dropTable('d_carta_semanal');
    }
}
