<?php

use yii\db\Migration;

class m170318_190227_insert_tipo_receta_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('tipo_receta',
            ['tipo_receta_id',
            'grupo_platillo_id',
            'tipo_prep_carta_id',
            'tipo_receta_nombre',
            'tipo_receta_descripcion',
            'tipo_receta_cantidad_max',
            ],
            [
                ['1', '2', '1', 'cremas', 'crema de zanahoria, crema de champiñones',
                '100.00'], ['2', '2', '1', 'caldos', 'consome', '100.00'], ['3',
                '2', '1', 'SOPAS', 'sopa de quinua, sopa de arroz de cebada', '100.00'],
            ['4', '3', '2', 'carnes [Proteínas]', 'estofados, aves y mariscos', '50.00'],
            ['5', '3', '3', 'guarniciones', 'arroz, papas, fideos, mote, verde, yuca',
                '50.00'], ['6', '3', '4', 'ensaladas y menestras [frias]', 'lechuga con tomate y cebolla, encurtido',
                '30.00'], ['7', '3', '4', 'ensaladas y menestras [calientes]', 'guiso de verduras',
                '30.00'], ['8', '4', '5', 'alimentos de media mañana / media tarde',
                'ensalada de frutas [40 gr solido, 40 gr liquido]', '80.00'], ['9',
                '4', '6', 'alimentos de media mañana / media tarde', 'jugos [frutas]',
                '40.00'], ['10', '4', '8', 'alimentos de media mañana / media tarde',
                'bolon, majado', '80.00'], ['11', '4', '8', 'coladas, batidos, avenas',
                'colada de platano', '80.00'], ['12', '5', '5', 'fruta', 'manzana, peras, uvas',
                '100.00'], ['13', '5', '6', 'jugos ', 'jugo de naranjilla', '100.00'],
            ['14', '6', '7', 'POSTRES', ' galletas, pastel', '40.00']
        ]);
    }

    public function down()
    {
         if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table tipo_receta CASCADE;
SQL;
         $this->execute($truncate);}
         else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table tipo_receta;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}