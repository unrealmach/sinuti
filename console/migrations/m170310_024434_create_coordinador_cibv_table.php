<?php

use yii\db\Migration;

/**
 * Handles the creation for table `coordinador_cibv`.
 */
class m170310_024434_create_coordinador_cibv_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coordinador_cibv', [
            'coordinador_id' => $this->primaryKey(),
            'coordinador_DNI' => $this->string(45)->notNull(),
            'coordinador_nombres' => $this->string(150)->notNull(),
            'coordinador_apellidos' => $this->string(150)->notNUll(),
            'coordinador_contacto' => $this->string(15)->notNull(),
            'coordinador_correo' => $this->string(150)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('coordinador_cibv');
    }
}
