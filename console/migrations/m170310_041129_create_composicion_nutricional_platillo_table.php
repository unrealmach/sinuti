<?php
use yii\db\Migration;

/**
 * Handles the creation for table `composicion_nutricional_platillo`.
 * Has foreign keys to the tables:
 *
 * - `m_platillo`
 */
class m170310_041129_create_composicion_nutricional_platillo_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS  es_real CASCADE");
            $this->execute("CREATE TYPE es_real AS  ENUM('NO', 'SI');");

            $this->createTable('composicion_nutricional_platillo', [
                'com_nut_platillo_id' => $this->primaryKey(),
                'm_platillo_id' => $this->integer(),
                'energia_kcal' => $this->decimal(6, 2)->notNull(),
                'proteinas_g' => $this->decimal(6, 2)->notNull(),
                'grasa_total_g' => $this->decimal(6, 2)->notNull(),
                'carbohidratos_g' => $this->decimal(6, 2)->notNull(),
                'fibra_dietetica_g' => $this->decimal(6, 2)->notNull(),
                'calcio_mg' => $this->decimal(6, 2)->notNull(),
                'fosforo_mg' => $this->decimal(6, 2)->notNull(),
                'hierro_mg' => $this->decimal(6, 2)->notNull(),
                'tiamina_mg' => $this->decimal(6, 2)->notNull(),
                'riboflavina_mg' => $this->decimal(6, 2)->notNull(),
                'niacina_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_c_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_a_equiv_retinol_mcg' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_monoinsaturados_g' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_poliinsaturados_g' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_saturados_g' => $this->decimal(6, 2)->notNull(),
                'colesterol_mg' => $this->decimal(6, 2)->notNull(),
                'potasio_mg' => $this->decimal(6, 2)->notNull(),
                'sodio_mg' => $this->decimal(6, 2)->notNull(),
                'zinc_mg' => $this->decimal(6, 2)->notNull(),
                'magnesio_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_b6_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_b12_mcg' => $this->decimal(6, 2)->notNull(),
                'folato_mcg' => $this->decimal(6, 2)->notNull(),
                'es_real' => "es_real DEFAULT 'NO' "
            ]);
        } else if ($this->db->driverName === 'mysql') {

            $this->createTable('composicion_nutricional_platillo', [
                'com_nut_platillo_id' => $this->primaryKey(),
                'm_platillo_id' => $this->integer(),
                'energia_kcal' => $this->decimal(6, 2)->notNull(),
                'proteinas_g' => $this->decimal(6, 2)->notNull(),
                'grasa_total_g' => $this->decimal(6, 2)->notNull(),
                'carbohidratos_g' => $this->decimal(6, 2)->notNull(),
                'fibra_dietetica_g' => $this->decimal(6, 2)->notNull(),
                'calcio_mg' => $this->decimal(6, 2)->notNull(),
                'fosforo_mg' => $this->decimal(6, 2)->notNull(),
                'hierro_mg' => $this->decimal(6, 2)->notNull(),
                'tiamina_mg' => $this->decimal(6, 2)->notNull(),
                'riboflavina_mg' => $this->decimal(6, 2)->notNull(),
                'niacina_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_c_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_a_equiv_retinol_mcg' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_monoinsaturados_g' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_poliinsaturados_g' => $this->decimal(6, 2)->notNull(),
                'acidos_grasos_saturados_g' => $this->decimal(6, 2)->notNull(),
                'colesterol_mg' => $this->decimal(6, 2)->notNull(),
                'potasio_mg' => $this->decimal(6, 2)->notNull(),
                'sodio_mg' => $this->decimal(6, 2)->notNull(),
                'zinc_mg' => $this->decimal(6, 2)->notNull(),
                'magnesio_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_b6_mg' => $this->decimal(6, 2)->notNull(),
                'vitamina_b12_mcg' => $this->decimal(6, 2)->notNull(),
                'folato_mcg' => $this->decimal(6, 2)->notNull(),
                'es_real' => "ENUM('NO', 'SI') DEFAULT 'NO' "
            ]);
        }



        // creates index for column `m_platillo_id`
        $this->createIndex(
            'idx-composicion_nutricional_platillo-m_platillo_id', 'composicion_nutricional_platillo', 'm_platillo_id'
        );

        // add foreign key for table `m_platillo`
        $this->addForeignKey(
            'fk-composicion_nutricional_platillo-m_platillo_id', 'composicion_nutricional_platillo', 'm_platillo_id', 'm_platillo', 'm_platillo_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS  es_real CASCADE");
        }

        // drops foreign key for table `m_platillo`
        $this->dropForeignKey(
            'fk-composicion_nutricional_platillo-m_platillo_id', 'composicion_nutricional_platillo'
        );

        // drops index for column `m_platillo_id`
        $this->dropIndex(
            'idx-composicion_nutricional_platillo-m_platillo_id', 'composicion_nutricional_platillo'
        );

        $this->dropTable('composicion_nutricional_platillo');
    }
}
