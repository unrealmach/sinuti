<?php
use yii\db\Migration;

class m170310_153406_create_upper_nombres_before_update_comite_padres_flia_trigger extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_comite_padres_flia_nombres_before_update()
RETURNS trigger AS '
BEGIN
    NEW.comite_nombres = UPPER(NEW.comite_nombres);
    NEW.comite_apellidos = UPPER(NEW.comite_apellidos);
    RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_comite_padres_flia_nombres_before_update
BEFORE UPDATE ON comite_padres_flia
FOR EACH ROW
EXECUTE PROCEDURE upper_comite_padres_flia_nombres_before_update();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_comite_padres_flia_nombres_before_update() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_comite_padres_flia_nombres_before_update ON comite_padres_flia CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            $createTriggerSql = <<< SQL
            CREATE TRIGGER `upper_comite_padres_flia_nombres_before_update` BEFORE UPDATE ON `comite_padres_flia` FOR EACH ROW BEGIN
SET  NEW.comite_nombres = UPPER(NEW.comite_nombres);
SET  NEW.comite_apellidos = UPPER(NEW.comite_apellidos);
END
SQL;
            $this->execute("DROP TRIGGER IF EXISTS `upper_comite_padres_flia_nombres_before_update`;");
            $this->execute($createTriggerSql);
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP  FUNCTION IF EXISTS upper_comite_padres_flia_nombres_before_update() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_comite_padres_flia_nombres_before_update ON comite_padres_flia CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP TRIGGER IF EXISTS `upper_comite_padres_flia_nombres_before_update`;");
        }
//        return false;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
