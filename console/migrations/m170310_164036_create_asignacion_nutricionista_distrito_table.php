<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_nutricionista_distrito`.
 * Has foreign keys to the tables:
 *
 * - `nutricionista`
 * - `distrito`
 */
class m170310_164036_create_asignacion_nutricionista_distrito_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_nutricionista_distrito', [
            'asignacion_nutricionista_distrito_id' => $this->primaryKey(),
            'nutricionista_id' => $this->integer()->notNull(),
            'distrito_id' => $this->integer()->notNull(),
            'fecha_inicio_actividad' => $this->date()->notNull(),
            'fecha_fin_actividad' => $this->date()->defaultValue(NULL),
        ]);

        // creates index for column `nutricionista_id`
        $this->createIndex(
            'idx-asignacion_nutricionista_distrito-nutricionista_id',
            'asignacion_nutricionista_distrito',
            'nutricionista_id'
        );

        // add foreign key for table `nutricionista`
        $this->addForeignKey(
            'fk-asignacion_nutricionista_distrito-nutricionista_id',
            'asignacion_nutricionista_distrito',
            'nutricionista_id',
            'nutricionista',
            'nutricionista_id',
            'CASCADE'
        );

        // creates index for column `distrito_id`
        $this->createIndex(
            'idx-asignacion_nutricionista_distrito-distrito_id',
            'asignacion_nutricionista_distrito',
            'distrito_id'
        );

        // add foreign key for table `distrito`
        $this->addForeignKey(
            'fk-asignacion_nutricionista_distrito-distrito_id',
            'asignacion_nutricionista_distrito',
            'distrito_id',
            'distrito',
            'distrito_id',
            'CASCADE'
        );

        $this->execute("ALTER TABLE asignacion_nutricionista_distrito ALTER COLUMN fecha_fin_actividad SET DEFAULT NULL;");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `nutricionista`
        $this->dropForeignKey(
            'fk-asignacion_nutricionista_distrito-nutricionista_id',
            'asignacion_nutricionista_distrito'
        );

        // drops index for column `nutricionista_id`
        $this->dropIndex(
            'idx-asignacion_nutricionista_distrito-nutricionista_id',
            'asignacion_nutricionista_distrito'
        );

        // drops foreign key for table `distrito`
        $this->dropForeignKey(
            'fk-asignacion_nutricionista_distrito-distrito_id',
            'asignacion_nutricionista_distrito'
        );

        // drops index for column `distrito_id`
        $this->dropIndex(
            'idx-asignacion_nutricionista_distrito-distrito_id',
            'asignacion_nutricionista_distrito'
        );

        $this->dropTable('asignacion_nutricionista_distrito');
    }
}
