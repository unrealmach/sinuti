<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_infante_c_semanal`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 * - `m_carta_semanal`
 */
class m170311_013347_create_asignacion_infante_c_semanal_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_infante_c_semanal', [
            'asig_inf_c_sem_id' => $this->primaryKey(),
            'infante_id' => $this->integer()->notNull(),
            'm_carta_semanal_id' => $this->integer()->notNull(),
            'asi_inf_c_sem_id_observaciones' => $this->text()->notNull(),
        ]);

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-asignacion_infante_c_semanal-infante_id',
            'asignacion_infante_c_semanal',
            'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-asignacion_infante_c_semanal-infante_id',
            'asignacion_infante_c_semanal',
            'infante_id',
            'infante',
            'infante_id',
            'CASCADE'
        );

        // creates index for column `m_carta_semanal_id`
        $this->createIndex(
            'idx-asignacion_infante_c_semanal-m_carta_semanal_id',
            'asignacion_infante_c_semanal',
            'm_carta_semanal_id'
        );

        // add foreign key for table `m_carta_semanal`
        $this->addForeignKey(
            'fk-asignacion_infante_c_semanal-m_carta_semanal_id',
            'asignacion_infante_c_semanal',
            'm_carta_semanal_id',
            'm_carta_semanal',
            'm_c_sem_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-asignacion_infante_c_semanal-infante_id',
            'asignacion_infante_c_semanal'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-asignacion_infante_c_semanal-infante_id',
            'asignacion_infante_c_semanal'
        );

        // drops foreign key for table `m_carta_semanal`
        $this->dropForeignKey(
            'fk-asignacion_infante_c_semanal-m_carta_semanal_id',
            'asignacion_infante_c_semanal'
        );

        // drops index for column `m_carta_semanal_id`
        $this->dropIndex(
            'idx-asignacion_infante_c_semanal-m_carta_semanal_id',
            'asignacion_infante_c_semanal'
        );

        $this->dropTable('asignacion_infante_c_semanal');
    }
}
