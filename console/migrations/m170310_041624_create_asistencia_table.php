<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asistencia`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 */
class m170310_041624_create_asistencia_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asistencia', [
            'asistencia_id' => $this->primaryKey(),
            'asistencia_hora_ingreso' => $this->time()->notNull(),
            'asistencia_hora_salida' => $this->time(),
            'asistencia_fecha' => $this->date()->notNull(),
            'infantes_id' => $this->integer(),
        ]);

        // creates index for column `infantes_id`
        $this->createIndex(
            'idx-asistencia-infantes_id',
            'asistencia',
            'infantes_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-asistencia-infantes_id',
            'asistencia',
            'infantes_id',
            'infante',
            'infante_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-asistencia-infantes_id',
            'asistencia'
        );

        // drops index for column `infantes_id`
        $this->dropIndex(
            'idx-asistencia-infantes_id',
            'asistencia'
        );

        $this->dropTable('asistencia');
    }
}
