<?php

use yii\db\Migration;

class m170528_032309_insert_first_supervisor extends Migration {

    public function up() {
        $this->insert('user', [
            'id' => '3',
            'username' => 'supervisor1',
            'auth_key' => 'EyTn1KfB-rtMKYzOgy1R3d_vx5U5aPEF',
            'password_hash' => '$2y$13$QWh2kbIWKpK1TUz.0Ih6BOfbUjKMwpOU5M0ihHBOfMAcQ6T1xYFlC',
            'confirmation_token' => null,
            'status' => '1',
            'superadmin' => '0',
            'created_at' => '1456971730',
            'updated_at' => '1462586322',
            'registration_ip' => null,
            'bind_to_ip' => '',
            'email' => 'supervisor1_sinuti@hotmail.com',
            'email_confirmed' => '0'
        ]);
        
        $this->batchInsert('auth_assignment', ['item_name', 'user_id', 'created_at'
            ], [
                ['coordinador-gad', 3, '1490754844'],
            ]
        );
    }

    public function down() {
        
         $this->delete('auth_assignment',"user_id='3'");
        $this->delete('user',"id='3'");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
