<?php

use yii\db\Migration;

/**
 * Handles the creation for table `categoria`.
 */
class m170310_004246_create_categoria_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('categoria', [
            'categoria_id' => $this->primaryKey(),
            'categoria_nombre' => $this->string(100)->notNull(),
            'categoria_descripcion' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('categoria');
    }
}
