<?php
use yii\db\Migration;

class m170329_022952_insert_user_soporte extends Migration
{

    public function up()
    {
        
        $this->insert('user',
            [
            'id'=>'2',
            'username' => 'soporte1',
            'auth_key' => 'EyTn1KfB-rtMKYzOgy1R3d_vx5U5aPEF',
            'password_hash' => '$2y$13$x3XkGFqe3XHHzd.okVjbwe6DETVur0ErUgKOI9iUt48cpY5IFJpX.',
            'confirmation_token' => null,
            'status' => '1',
            'superadmin' => '0',
            'created_at' => '1456971730',
            'updated_at' => '1462586322',
            'registration_ip' => null,
            'bind_to_ip' => '',
            'email' => 'soporte_sinuti@hotmail.com',
            'email_confirmed' => '0'
        ]);
    }

    public function down()
    {
        $this->delete('user',"id='2'");
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
