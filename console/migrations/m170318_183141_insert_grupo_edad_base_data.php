<?php

use yii\db\Migration;

class m170318_183141_insert_grupo_edad_base_data extends Migration {

    public function up() {
        $this->batchInsert('grupo_edad', ['grupo_edad_id',
            'grupo_edad_descripcion',
                ], [
                ['1', 'Un año'], ['2', 'Un año y medio'], ['3', 'Dos años'], ['4',
                'Dos años y medio'], ['5', 'Tres años'], ['6', 'Tres años y medio'],
                ['7', 'Cuatro años'], ['8', 'Cuatro años y medio'], ['9', 'Cinco años']
                ]
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncate = <<< SQL
truncate table grupo_edad CASCADE;
SQL;
            $this->execute($truncate);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table grupo_edad;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
