<?php

use yii\db\Migration;

/**
 * Handles the creation for table `sector_asignado_educador`.
 * Has foreign keys to the tables:
 *
 * - `cibv`
 * - `periodo`
 * - `educador`
 * - `salon`
 */
class m170310_160249_create_sector_asignado_educador_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sector_asignado_educador', [
            'sector_asignado_educador_id' => $this->primaryKey(),
            'cen_inf_id' => $this->integer(),
            'periodo_id' => $this->integer(),
            'educador_id' => $this->integer(),
            'salon_id' => $this->integer(),
            'fecha_inicio_actividad' => $this->date()->notNull(),
            'fecha_fin_actividad' => $this->date()->defaultValue(NULL),
        ]);

        // creates index for column `cen_inf_id`
        $this->createIndex(
            'idx-sector_asignado_educador-cen_inf_id',
            'sector_asignado_educador',
            'cen_inf_id'
        );

        // add foreign key for table `cibv`
        $this->addForeignKey(
            'fk-sector_asignado_educador-cen_inf_id',
            'sector_asignado_educador',
            'cen_inf_id',
            'cibv',
            'cen_inf_id',
            'CASCADE'
        );

        // creates index for column `periodo_id`
        $this->createIndex(
            'idx-sector_asignado_educador-periodo_id',
            'sector_asignado_educador',
            'periodo_id'
        );

        // add foreign key for table `periodo`
        $this->addForeignKey(
            'fk-sector_asignado_educador-periodo_id',
            'sector_asignado_educador',
            'periodo_id',
            'periodo',
            'periodo_id',
            'CASCADE'
        );

        // creates index for column `educador_id`
        $this->createIndex(
            'idx-sector_asignado_educador-educador_id',
            'sector_asignado_educador',
            'educador_id'
        );

        // add foreign key for table `educador`
        $this->addForeignKey(
            'fk-sector_asignado_educador-educador_id',
            'sector_asignado_educador',
            'educador_id',
            'educador',
            'educador_id',
            'CASCADE'
        );

        // creates index for column `salon_id`
        $this->createIndex(
            'idx-sector_asignado_educador-salon_id',
            'sector_asignado_educador',
            'salon_id'
        );

        // add foreign key for table `salon`
        $this->addForeignKey(
            'fk-sector_asignado_educador-salon_id',
            'sector_asignado_educador',
            'salon_id',
            'salon',
            'salon_id',
            'CASCADE'
        );

        $this->execute("ALTER TABLE sector_asignado_educador ALTER COLUMN fecha_fin_actividad SET DEFAULT NULL;");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `cibv`
        $this->dropForeignKey(
            'fk-sector_asignado_educador-cen_inf_id',
            'sector_asignado_educador'
        );

        // drops index for column `cen_inf_id`
        $this->dropIndex(
            'idx-sector_asignado_educador-cen_inf_id',
            'sector_asignado_educador'
        );

        // drops foreign key for table `periodo`
        $this->dropForeignKey(
            'fk-sector_asignado_educador-periodo_id',
            'sector_asignado_educador'
        );

        // drops index for column `periodo_id`
        $this->dropIndex(
            'idx-sector_asignado_educador-periodo_id',
            'sector_asignado_educador'
        );

        // drops foreign key for table `educador`
        $this->dropForeignKey(
            'fk-sector_asignado_educador-educador_id',
            'sector_asignado_educador'
        );

        // drops index for column `educador_id`
        $this->dropIndex(
            'idx-sector_asignado_educador-educador_id',
            'sector_asignado_educador'
        );

        // drops foreign key for table `salon`
        $this->dropForeignKey(
            'fk-sector_asignado_educador-salon_id',
            'sector_asignado_educador'
        );

        // drops index for column `salon_id`
        $this->dropIndex(
            'idx-sector_asignado_educador-salon_id',
            'sector_asignado_educador'
        );

        $this->dropTable('sector_asignado_educador');
    }
}
