<?php

use yii\db\Migration;

class m170329_021939_insert_permisos_soporte extends Migration
{
    public function up()
    {
        $this->batchInsert('auth_item_child', ['parent','child',
            ], [
                ['soporte', 'assignRolesToUsers'], //faltan estos permisos
                ['soporte', 'bindUserToIp'],
                ['soporte', 'changeUserPassword'],
                ['soporte', 'createUsers'],
                ['soporte', 'deleteUsers'],
                ['soporte', 'editUserEmail'],
                ['soporte', 'editUsers'],
                ['soporte', 'viewRegistrationIp'],
                ['soporte', 'viewUserEmail'],
                ['soporte', 'viewUserRoles'],
                ['soporte', 'viewUsers'],
                ['soporte', 'viewVisitLog'],
                ['logout_system', '/site/logout'],
                ['logout_system', '/user-management/auth/logout'],
                ['soporte', 'logout_system'],
            ]
        );

    }

    public function down()
    {
        $this->delete('auth_item_child',"parent='logout_system'");
        $this->delete('auth_item_child',"parent='soporte'");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
