<?php
use yii\db\Migration;

class m170310_153829_create_upper_nombres_before_insert_educador_trigger extends Migration
{

    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $createFunctionSql = <<< SQL
CREATE FUNCTION upper_educador_nombres_before_insert()
RETURNS trigger AS '
BEGIN
    NEW.educador_nombres = UPPER(NEW.educador_nombres);
    NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
   RETURN NEW;
END' LANGUAGE 'plpgsql';
SQL;

            $createTriggerSql = <<< SQL

CREATE TRIGGER upper_educador_nombres_before_insert
BEFORE INSERT ON educador
FOR EACH ROW
EXECUTE PROCEDURE upper_educador_nombres_before_insert();
SQL;

            $this->execute("DROP  FUNCTION IF EXISTS upper_educador_nombres_before_insert() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_educador_nombres_before_insert ON educador CASCADE;");
            $this->execute($createFunctionSql);
            $this->execute($createTriggerSql);
        } else if ($this->db->driverName === 'mysql') {
            $createTriggerSql = <<< SQL
            CREATE TRIGGER `upper_educador_nombres_before_insert` BEFORE INSERT ON `educador` FOR EACH ROW BEGIN
SET  NEW.educador_nombres = UPPER(NEW.educador_nombres);
SET  NEW.educador_apellidos = UPPER(NEW.educador_apellidos);
END
SQL;
            $this->execute("DROP TRIGGER IF EXISTS `upper_educador_nombres_before_insert`;");
            $this->execute($createTriggerSql);
        }
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP  FUNCTION IF EXISTS upper_educador_nombres_before_insert() CASCADE;");
            $this->execute("DROP TRIGGER IF EXISTS upper_educador_nombres_before_insert ON educador CASCADE;");
        } else if ($this->db->driverName === 'mysql') {
            $this->execute("DROP TRIGGER IF EXISTS `upper_educador_nombres_before_insert`;");
        }
//        return false;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
