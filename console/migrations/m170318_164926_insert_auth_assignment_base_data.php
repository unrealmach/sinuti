<?php

use yii\db\Migration;

class m170318_164926_insert_auth_assignment_base_data extends Migration {

    public function up() {
        $this->batchInsert('auth_assignment', ['item_name', 'user_id', 'created_at'], [
                ['sysadmin', '1', '1462641876']
                ]
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment CASCADE;
SQL;
            $this->execute($truncateAuth_assignment);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateAuth_assignment = <<< SQL
truncate table auth_assignment;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncateAuth_assignment);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
