<?php
use yii\db\Migration;

/**
 * Handles the creation for table `dato_antropometrico`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 */
class m170310_171024_create_dato_antropometrico_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS dat_antro_interpretacion_peso CASCADE");
            $this->execute("DROP TYPE IF EXISTS dat_antro_interpretacion_talla CASCADE");
            $this->execute("DROP TYPE IF EXISTS dato_antro_desnutricion_talla CASCADE");
            $this->execute("DROP TYPE IF EXISTS dato_antro_desnutricion_peso CASCADE");
            $this->execute("CREATE TYPE dat_antro_interpretacion_peso AS  ENUM('BAJOPESO','RIESGOBAJOPESO','PESONORMAL','RIESGOSOBREPESO','SOBREPESO');");
            $this->execute("CREATE TYPE dat_antro_interpretacion_talla AS  ENUM('BAJATALLASEVERA','TALLABAJA','TALLANORMAL','TALLAALTA','TALLAMUYALTA');");
            $this->execute("CREATE TYPE dato_antro_desnutricion_talla AS  ENUM('DESNUTRICIONCRONICA', 'NOAPLICA');");
            $this->execute("CREATE TYPE dato_antro_desnutricion_peso AS  ENUM('DESNUTRICIONGLOBAL','DESNUTRICIONAGUDA','NOAPLICA');");
            $this->createTable('dato_antropometrico', [
                'dat_antro_id' => $this->primaryKey(),
                'infante_id' => $this->integer(),
                'dat_antro_fecha_registro' => $this->timestamp()->notNull(),
                'dat_antro_edad_meses' => $this->integer()->notNull(),
                'dat_antro_talla_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_peso_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_imc_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_interpretacion_peso' => "dat_antro_interpretacion_peso NOT NULL",
                'dat_antro_interpretacion_talla' => " dat_antro_interpretacion_talla NOT NULL",
                'dato_antro_desnutricion_talla' => "dato_antro_desnutricion_talla NOT NULL",
                'dato_antro_desnutricion_peso' => " dato_antro_desnutricion_peso NOT NULL",
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('dato_antropometrico', [
                'dat_antro_id' => $this->primaryKey(),
                'infante_id' => $this->integer(),
                'dat_antro_fecha_registro' => $this->timestamp()->notNull(),
                'dat_antro_edad_meses' => $this->integer()->notNull(),
                'dat_antro_talla_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_peso_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_imc_infante' => $this->decimal(5, 2)->notNull(),
                'dat_antro_interpretacion_peso' => "ENUM('BAJOPESO','RIESGOBAJOPESO','PESONORMAL','RIESGOSOBREPESO','SOBREPESO') NOT NULL",
                'dat_antro_interpretacion_talla' => " ENUM('BAJATALLASEVERA','TALLABAJA','TALLANORMAL','TALLAALTA','TALLAMUYALTA') NOT NULL",
                'dato_antro_desnutricion_talla' => "ENUM('DESNUTRICIONCRONICA', 'NOAPLICA') NOT NULL",
                'dato_antro_desnutricion_peso' => " ENUM('DESNUTRICIONGLOBAL','DESNUTRICIONAGUDA','NOAPLICA') NOT NULL",
            ]);
        }

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-dato_antropometrico-infante_id', 'dato_antropometrico', 'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-dato_antropometrico-infante_id', 'dato_antropometrico', 'infante_id', 'infante', 'infante_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS dat_antro_interpretacion_peso CASCADE");
            $this->execute("DROP TYPE IF EXISTS dat_antro_interpretacion_talla CASCADE");
            $this->execute("DROP TYPE IF EXISTS dato_antro_desnutricion_talla CASCADE");
            $this->execute("DROP TYPE IF EXISTS dato_antro_desnutricion_peso CASCADE");
        }
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-dato_antropometrico-infante_id', 'dato_antropometrico'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-dato_antropometrico-infante_id', 'dato_antropometrico'
        );

        $this->dropTable('dato_antropometrico');
    }
}
