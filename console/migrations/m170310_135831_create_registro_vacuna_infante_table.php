<?php

use yii\db\Migration;

/**
 * Handles the creation for table `registro_vacuna_infante`.
 * Has foreign keys to the tables:
 *
 * - `infante`
 * - `tipo_vacuna`
 */
class m170310_135831_create_registro_vacuna_infante_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('registro_vacuna_infante', [
            'reg_vac_inf_id' => $this->primaryKey(),
            'infante_id' => $this->integer()->notNull(),
            'tipo_vacuna_id' => $this->integer()->notNull(),
            'tipo_vac_fecha_aplicacion' => $this->date()->notNull(),
        ]);

        // creates index for column `infante_id`
        $this->createIndex(
            'idx-registro_vacuna_infante-infante_id',
            'registro_vacuna_infante',
            'infante_id'
        );

        // add foreign key for table `infante`
        $this->addForeignKey(
            'fk-registro_vacuna_infante-infante_id',
            'registro_vacuna_infante',
            'infante_id',
            'infante',
            'infante_id',
            'CASCADE'
        );

        // creates index for column `tipo_vacuna_id`
        $this->createIndex(
            'idx-registro_vacuna_infante-tipo_vacuna_id',
            'registro_vacuna_infante',
            'tipo_vacuna_id'
        );

        // add foreign key for table `tipo_vacuna`
        $this->addForeignKey(
            'fk-registro_vacuna_infante-tipo_vacuna_id',
            'registro_vacuna_infante',
            'tipo_vacuna_id',
            'tipo_vacuna',
            'tipo_vacuna_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `infante`
        $this->dropForeignKey(
            'fk-registro_vacuna_infante-infante_id',
            'registro_vacuna_infante'
        );

        // drops index for column `infante_id`
        $this->dropIndex(
            'idx-registro_vacuna_infante-infante_id',
            'registro_vacuna_infante'
        );

        // drops foreign key for table `tipo_vacuna`
        $this->dropForeignKey(
            'fk-registro_vacuna_infante-tipo_vacuna_id',
            'registro_vacuna_infante'
        );

        // drops index for column `tipo_vacuna_id`
        $this->dropIndex(
            'idx-registro_vacuna_infante-tipo_vacuna_id',
            'registro_vacuna_infante'
        );

        $this->dropTable('registro_vacuna_infante');
    }
}
