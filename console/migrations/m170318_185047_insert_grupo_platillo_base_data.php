<?php

use yii\db\Migration;

class m170318_185047_insert_grupo_platillo_base_data extends Migration {

    public function up() {
        $this->batchInsert('grupo_platillo', ['grupo_platillo_id',
            'grupo_platillo_nombre',
            'grupo_platillo_descripcion',
                ], [
                ['1', 'ENTRADAS', 'NO APLICA EN DIETAS INFANTILES'], ['2', 'SOPAS', 'SOPAS'], ['3', 'PLATOS FUERTES',
                'PLATOS FUERTES'], ['4', 'BREAKS', 'BREAKS'], ['5', 'FRUTAS O JUGOS',
                'frutas o jugos'], ['6', 'OTROS', 'POSTRES']
        ]);
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncate = <<< SQL
truncate table grupo_platillo CASCADE;
SQL;
            $this->execute($truncate);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table grupo_platillo;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
