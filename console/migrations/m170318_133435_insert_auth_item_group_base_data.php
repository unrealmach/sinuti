<?php

use yii\db\Migration;

class m170318_133435_insert_auth_item_group_base_data extends Migration {

    public function up() {
        $this->insert('auth_item_group', ['code' => 'userCommonPermissions',
            'name' => 'User common permission',
            'created_at' => '1456971732',
            'updated_at' => '1456971732']);

        $this->insert('auth_item_group', ['code' => 'userManagement',
            'name' => 'User management',
            'created_at' => '1456971732',
            'updated_at' => '1456971732']
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group CASCADE;
SQL;
            $this->execute($truncateAuth_item_group);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateAuth_item_group = <<< SQL
truncate table auth_item_group;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncateAuth_item_group);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
