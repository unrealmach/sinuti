<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tiempo_comida`.
 */
class m170310_025429_create_tiempo_comida_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tiempo_comida', [
            'tiem_com_id' => $this->primaryKey(),
            'tiem_com_nombre' => $this->string(45)->notNull(),
            'tiem_com_hora_inicio' => $this->time()->notNull(),
            'tiempo_com_hora_fin' => $this->time()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tiempo_comida');
    }
}
