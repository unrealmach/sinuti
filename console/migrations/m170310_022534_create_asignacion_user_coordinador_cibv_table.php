<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_user_coordinador_cibv`.
 */
class m170310_022534_create_asignacion_user_coordinador_cibv_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_user_coordinador_cibv', [
            'asignacion_user_cibv_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'autoridades_cibv_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'observaciones' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('asignacion_user_coordinador_cibv');
    }
}
