<?php

use yii\db\Migration;

class m170318_173314_insert_distrito_base_data extends Migration {

    public function up() {
        $this->batchInsert('distrito', ['distrito_id', 'distrito_codigo', 'distrito_descripcion'], [
                ['1', '10D01', 'IBARRA-PIMPAMPIRO-URCUQUÍ']
                ]
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncate = <<< SQL
truncate table distrito CASCADE;
SQL;
            $this->execute($truncate);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table distrito;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
