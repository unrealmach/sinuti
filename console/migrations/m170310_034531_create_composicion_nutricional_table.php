<?php

use yii\db\Migration;

/**
 * Handles the creation for table `composicion_nutricional`.
 * Has foreign keys to the tables:
 *
 * - `alimento`
 */
class m170310_034531_create_composicion_nutricional_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('composicion_nutricional', [
            'com_nut_id' => $this->primaryKey(),
            'alimento_id' => $this->integer(),
            'energia_kcal' => $this->decimal(6,2)->notNull(),
            'proteinas_g' => $this->decimal(6,2)->notNull(),
            'grasa_total_g' => $this->decimal(6,2)->notNull(),
            'carbohidratos_g' => $this->decimal(6,2)->notNull(),
            'fibra_dietetica_g' => $this->decimal(6,2)->notNull(),
            'calcio_mg' => $this->decimal(6,2)->notNull(),
            'fosforo_mg' => $this->decimal(6,2)->notNull(),
            'hierro_mg' => $this->decimal(6,2)->notNull(),
            'tiamina_mg' => $this->decimal(6,2)->notNull(),
            'riboflavina_mg' => $this->decimal(6,2)->notNull(),
            'niacina_mg' => $this->decimal(6,2)->notNull(),
            'vitamina_c_mg' => $this->decimal(6,2)->notNull(),
            'vitamina_a_equiv_retinol_mcg' => $this->decimal(6,2)->notNull(),
            'acidos_grasos_monoinsaturados_g' => $this->decimal(6,2)->notNull(),
            'acidos_grasos_poliinsaturados_g' => $this->decimal(6,2)->notNull(),
            'acidos_grasos_saturados_g' => $this->decimal(6,2)->notNull(),
            'colesterol_mg' => $this->decimal(6,2)->notNull(),
            'potasio_mg' => $this->decimal(6,2)->notNull(),
            'sodio_mg' => $this->decimal(6,2)->notNull(),
            'zinc_mg' => $this->decimal(6,2)->notNull(),
            'magnesio_mg' => $this->decimal(6,2)->notNull(),
            'vitamina_b6_mg' => $this->decimal(6,2)->notNull(),
            'vitamina_b12_mcg' => $this->decimal(6,2)->notNull(),
            'folato_mcg' => $this->decimal(6,2)->notNull(),
        ]);

        // creates index for column `alimento_id`
        $this->createIndex(
            'idx-composicion_nutricional-alimento_id',
            'composicion_nutricional',
            'alimento_id'
        );

        // add foreign key for table `alimento`
        $this->addForeignKey(
            'fk-composicion_nutricional-alimento_id',
            'composicion_nutricional',
            'alimento_id',
            'alimento',
            'alimento_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `alimento`
        $this->dropForeignKey(
            'fk-composicion_nutricional-alimento_id',
            'composicion_nutricional'
        );

        // drops index for column `alimento_id`
        $this->dropIndex(
            'idx-composicion_nutricional-alimento_id',
            'composicion_nutricional'
        );

        $this->dropTable('composicion_nutricional');
    }
}
