<?php
use yii\db\Migration;

class m170328_202952_insert_rol_soporte extends Migration
{

    public function up()
    {
        $this->batchInsert('auth_item', ['name', 'type', 'description', 'rule_name',
            'data', 'created_at', 'updated_at', 'group_code'
            ], [
                ['soporte', "1", 'soporte', NULL, NULL, 1490732804, 1490732804, NULL],
                ['logout_system',"2",'logout_system', NULL, NULL, 1490732804, 1490732804, NULL]
            ]
        );
    }

    public function down()
    {
        $this->delete('auth_item','name=:rol',[':rol'=>'soporte']);
        $this->delete('auth_item','name=:accion',[':accion'=>'logout_system']);
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
