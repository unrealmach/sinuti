<?php

use yii\db\Migration;

class m170318_174000_insert_parroquia_base_data extends Migration
{

    public function up()
    {
        $this->batchInsert('parroquia',
            ['parroquia_id', 'parroquia_codigo', 'parroquia_nombre', 'localidad_id'],
            [
                ['1', '100101', 'CARANQUI', '1'], ['2', '100102', 'GUAYAQUIL DE ALPACHACA',
                '1'], ['3', '100103', 'SAGRARIO', '1'], ['4', '100104', 'SAN FRANCISCO',
                '1'], ['5', '100105', 'LA DOLOROSA DEL PRIORATO', '1'], ['6', '100151',
                'AMBUQUÍ', '1'], ['7', '100152', 'ANGOCHAGUA', '1'], ['8', '100153',
                'CAROLINA', '1'], ['9', '100154', 'LA ESPERANZA', '1'], ['10', '100155',
                'LITA', '1'], ['11', '100156', 'SALINAS', '1'], ['12', '100157',
                'SAN ANTONIO', '1']
            ]
        );
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
        $truncate = <<< SQL
truncate table parroquia CASCADE;
SQL;
        $this->execute($truncate);}
        else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncate = <<< SQL
truncate table parroquia;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncate);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}