<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_user_nutricionista_distrito`.
 */
class m170310_035306_create_asignacion_user_nutricionista_distrito_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_user_nutricionista_distrito', [
            'asignacion_user_nutricionista_distrito_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'asignacion_nutricionista_distrito_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'observaciones' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('asignacion_user_nutricionista_distrito');
    }
}
