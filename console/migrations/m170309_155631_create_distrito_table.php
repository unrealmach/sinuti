<?php

use yii\db\Migration;

/**
 * Handles the creation for table `distrito`.
 */
class m170309_155631_create_distrito_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('distrito', [
            'distrito_id' => $this->primaryKey(),
            'distrito_codigo' => $this->string(10)->notNull(),
            'distrito_descripcion' => $this->string(250)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('distrito');
    }
}
