<?php

use yii\db\Migration;

/**
 * Handles the creation for table `asignacion_user_gastronomo_distrito`.
 */
class m170310_170419_create_asignacion_user_gastronomo_distrito_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('asignacion_user_gastronomo_distrito', [
            'asignacion_user_gastronomo_distrito_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'asignacion_gastronomo_distrito_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'observaciones' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('asignacion_user_gastronomo_distrito');
    }
}
