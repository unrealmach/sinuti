<?php

use yii\db\Migration;

/**
 * Handles the creation for table `rango_nutriente`.
 * Has foreign keys to the tables:
 *
 * - `nutriente`
 * - `tiempo_comida`
 * - `grupo_edad`
 */
class m170310_031705_create_rango_nutriente_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rango_nutriente', [
            'ran_nut_id' => $this->primaryKey(),
            'nutriente_id' => $this->integer()->notNull(),
            'tiempo_comida_id' => $this->integer()->notNull(),
            'grupo_edad_id' => $this->integer()->notNull(),
            'ran_nut_valor_min' => $this->decimal(10,2)->notNull()->defaultValue(0),
            'ran_nut_valor_max' => $this->decimal(10,2)->notNull()->defaultValue(0),
        ]);

        // creates index for column `nutriente_id`
        $this->createIndex(
            'idx-rango_nutriente-nutriente_id',
            'rango_nutriente',
            'nutriente_id'
        );

        // add foreign key for table `nutriente`
        $this->addForeignKey(
            'fk-rango_nutriente-nutriente_id',
            'rango_nutriente',
            'nutriente_id',
            'nutriente',
            'nutriente_id',
            'CASCADE'
        );

        // creates index for column `tiempo_comida_id`
        $this->createIndex(
            'idx-rango_nutriente-tiempo_comida_id',
            'rango_nutriente',
            'tiempo_comida_id'
        );

        // add foreign key for table `tiempo_comida`
        $this->addForeignKey(
            'fk-rango_nutriente-tiempo_comida_id',
            'rango_nutriente',
            'tiempo_comida_id',
            'tiempo_comida',
            'tiem_com_id',
            'CASCADE'
        );

        // creates index for column `grupo_edad_id`
        $this->createIndex(
            'idx-rango_nutriente-grupo_edad_id',
            'rango_nutriente',
            'grupo_edad_id'
        );

        // add foreign key for table `grupo_edad_id`
        $this->addForeignKey(
            'fk-rango_nutriente-grupo_edad_id',
            'rango_nutriente',
            'grupo_edad_id',
            'grupo_edad',
            'grupo_edad_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `nutriente`
        $this->dropForeignKey(
            'fk-rango_nutriente-nutriente_id',
            'rango_nutriente'
        );

        // drops index for column `nutriente_id`
        $this->dropIndex(
            'idx-rango_nutriente-nutriente_id',
            'rango_nutriente'
        );

//         drops foreign key for table `tiempo_comida`
        $this->dropForeignKey(
            'fk-rango_nutriente-tiempo_comida_id',
            'rango_nutriente'
        );

        // drops index for column `tiempo_comida_id`
        $this->dropIndex(
            'idx-rango_nutriente-tiempo_comida_id',
            'rango_nutriente'
        );

        // drops foreign key for table `grupo_edad`
        $this->dropForeignKey(
            'fk-rango_nutriente-grupo_edad_id',
            'rango_nutriente'
        );

        // drops index for column `grupo_edad`
        $this->dropIndex(
            'idx-rango_nutriente-grupo_edad_id',
            'rango_nutriente'
        );

        $this->dropTable('rango_nutriente');
    }
}
