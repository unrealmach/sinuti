<?php

use yii\db\Migration;

/**
 * Handles the creation for table `salon`.
 * Has foreign keys to the tables:
 *
 * - `grupo_edad`
 */
class m170310_041324_create_salon_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('salon', [
            'salon_id' => $this->primaryKey(),
            'grupo_edad_id' => $this->integer()->notNull(),
            'salon_capacidad' => $this->integer()->notNull(),
            'salon_nombre' => $this->string(100)->notNull(),
        ]);

        // creates index for column `grupo_edad_id`
        $this->createIndex(
            'idx-salon-grupo_edad_id',
            'salon',
            'grupo_edad_id'
        );

        // add foreign key for table `grupo_edad`
        $this->addForeignKey(
            'fk-salon-grupo_edad_id',
            'salon',
            'grupo_edad_id',
            'grupo_edad',
            'grupo_edad_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `grupo_edad`
        $this->dropForeignKey(
            'fk-salon-grupo_edad_id',
            'salon'
        );

        // drops index for column `grupo_edad_id`
        $this->dropIndex(
            'idx-salon-grupo_edad_id',
            'salon'
        );

        $this->dropTable('salon');
    }
}
