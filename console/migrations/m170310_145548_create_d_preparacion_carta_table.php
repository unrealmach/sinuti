<?php

use yii\db\Migration;

/**
 * Handles the creation for table `d_preparacion_carta`.
 * Has foreign keys to the tables:
 *
 * - `m_prep_carta`
 * - `tipo_preparacion`
 * - `alimento`
 * - `m_platillo`
 */
class m170310_145548_create_d_preparacion_carta_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('d_preparacion_carta', [
            'd_prep_carta_id' => $this->primaryKey(),
            'm_prep_carta_id' => $this->integer()->notNull(),
            'tipo_preparacion_id' => $this->integer()->notNull(),
            'd_prep_carta_cantidad_g' => $this->decimal(10,2)->notNull(),
            'alimento_id' => $this->integer(),
            'm_platillo_id' => $this->integer(),
        ]);

        // creates index for column `m_prep_carta_id`
        $this->createIndex(
            'idx-d_preparacion_carta-m_prep_carta_id',
            'd_preparacion_carta',
            'm_prep_carta_id'
        );

        // add foreign key for table `m_prep_carta`
        $this->addForeignKey(
            'fk-d_preparacion_carta-m_prep_carta_id',
            'd_preparacion_carta',
            'm_prep_carta_id',
            'm_prep_carta',
            'm_prep_carta_id',
            'CASCADE'
        );

        // creates index for column `tipo_preparacion_id`
        $this->createIndex(
            'idx-d_preparacion_carta-tipo_preparacion_id',
            'd_preparacion_carta',
            'tipo_preparacion_id'
        );

        // add foreign key for table `tipo_preparacion`
        $this->addForeignKey(
            'fk-d_preparacion_carta-tipo_preparacion_id',
            'd_preparacion_carta',
            'tipo_preparacion_id',
            'tipo_preparacion_carta',
            'tipo_prep_carta_id',
            'CASCADE'
        );

        // creates index for column `alimento_id`
        $this->createIndex(
            'idx-d_preparacion_carta-alimento_id',
            'd_preparacion_carta',
            'alimento_id'
        );

        // add foreign key for table `alimento`
        $this->addForeignKey(
            'fk-d_preparacion_carta-alimento_id',
            'd_preparacion_carta',
            'alimento_id',
            'alimento',
            'alimento_id',
            'CASCADE'
        );

        // creates index for column `m_platillo_id`
        $this->createIndex(
            'idx-d_preparacion_carta-m_platillo_id',
            'd_preparacion_carta',
            'm_platillo_id'
        );

        // add foreign key for table `m_platillo`
        $this->addForeignKey(
            'fk-d_preparacion_carta-m_platillo_id',
            'd_preparacion_carta',
            'm_platillo_id',
            'm_platillo',
            'm_platillo_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `m_prep_carta`
        $this->dropForeignKey(
            'fk-d_preparacion_carta-m_prep_carta_id',
            'd_preparacion_carta'
        );

        // drops index for column `m_prep_carta_id`
        $this->dropIndex(
            'idx-d_preparacion_carta-m_prep_carta_id',
            'd_preparacion_carta'
        );

        // drops foreign key for table `tipo_preparacion`
        $this->dropForeignKey(
            'fk-d_preparacion_carta-tipo_preparacion_id',
            'd_preparacion_carta'
        );

        // drops index for column `tipo_preparacion_id`
        $this->dropIndex(
            'idx-d_preparacion_carta-tipo_preparacion_id',
            'd_preparacion_carta'
        );

        // drops foreign key for table `alimento`
        $this->dropForeignKey(
            'fk-d_preparacion_carta-alimento_id',
            'd_preparacion_carta'
        );

        // drops index for column `alimento_id`
        $this->dropIndex(
            'idx-d_preparacion_carta-alimento_id',
            'd_preparacion_carta'
        );

        // drops foreign key for table `m_platillo`
        $this->dropForeignKey(
            'fk-d_preparacion_carta-m_platillo_id',
            'd_preparacion_carta'
        );

        // drops index for column `m_platillo_id`
        $this->dropIndex(
            'idx-d_preparacion_carta-m_platillo_id',
            'd_preparacion_carta'
        );

        $this->dropTable('d_preparacion_carta');
    }
}
