<?php

use yii\db\Migration;

class m170318_150802_insert_auth_item_child_base_data extends Migration {

    public function up() {
        $this->batchInsert('auth_item_child', ['parent', 'child'], [
                ['Admin', 'assignRolesToUsers'], ['Admin', 'changeOwnPassword'],
                ['Admin', 'changeUserPassword'], ['Admin', 'coordinador'], ['Admin',
                'coordinador-gad'], ['Admin', 'createUsers'], ['Admin', 'deleteUsers'],
                ['Admin', 'editUsers'], ['Admin', 'educador'], ['Admin', 'gastronomo'],
                ['Admin', 'gestion_salones'], ['Admin', 'nutricion'], ['Admin', 'permisos_auditoria'],
                ['Admin', 'viewUsers'], ['assignRolesToUsers', '/user-management/user-permission/set'],
                ['assignRolesToUsers', '/user-management/user-permission/set-roles'],
                ['assignRolesToUsers', 'viewUserRoles'], ['assignRolesToUsers', 'viewUsers'],
                ['changeOwnPassword', '/user-management/auth/change-own-password'], [
                'changeUserPassword', '/user-management/user/change-password'], [
                'changeUserPassword', 'viewUsers'], ['commonPermission', '//index'],
                ['commonPermission', '/site/error'], ['commonPermission', '/site/index'],
                ['commonPermission', '/site/login'], ['coordinador', 'gestion_asignacion_carta_semanal'],
                ['coordinador', 'gestion_asistencia'], ['coordinador', 'gestion_carta_semanal'],
                ['coordinador', 'gestion_centro_infantil'], ['coordinador', 'gestion_comite_central_padres_familia'],
                ['coordinador', 'gestion_coordinadorcibv_propia'], ['coordinador', 'gestion_educador'],
                ['coordinador', 'gestion_infante'], ['coordinador', 'gestion_matricula_infante'],
                ['coordinador', 'gestion_salones'], ['coordinador', 'gestion_sector_asignado_educador'],
                ['coordinador', 'gestion_sector_asignado_infante'], ['coordinador', 'ver_reportes_cibv'],
                ['coordinador', 'view_bitacora_consumo'], ['coordinador', 'view_medidas_antropometricas_infante'],
                ['coordinador', 'view_patologias_infantes'], ['coordinador', 'view_vacunas_infantes'],
                ['coordinador-gad', 'gestion_centro_infantil'], ['coordinador-gad', 'gestion_periodo'],
                ['coordinador-gad', 'gestion_rrhh_autoridades_cibv'], ['coordinador-gad',
                'gestion_rrhh_coordinador_cibv'], ['coordinador-gad', 'gestion_rrhh_gastronomo'],
                ['coordinador-gad', 'gestion_rrhh_nutricionista'], ['coordinador-gad',
                'gestion_sector_asignado_gastronomo'], ['coordinador-gad', 'gestion_sector_asignado_nutricionista'],
                ['coordinador-gad', 'permisos_comunes_user_registrado'], ['coordinador-gad',
                'ver_reportes_cibv'], ['coordinador-gad', 'view_asistencia_infante'],
                ['coordinador-gad', 'view_cartas'], ['coordinador-gad', 'view_consumo_infantes'],
                ['coordinador-gad', 'view_curvas_crecimiento'], ['coordinador-gad', 'view_educadores'],
                ['coordinador-gad', 'view_matriculas_infante'], ['coordinador-gad', 'view_mcartasemanal'],
                ['coordinador-gad', 'view_medidas_antropometricas_infante'], ['coordinador-gad',
                'view_mprepplatillo'], ['coordinador-gad', 'view_patologias_infantes'],
                ['coordinador-gad', 'view_reporte_crecimiento_infantil'], ['coordinador-gad',
                'view_sector_asignado_educadores'], ['coordinador-gad', 'view_sector_asignado_infantes'],
                ['coordinador-gad', 'view_vacunas_infantes'], ['createUsers', '/user-management/user/create'],
                ['createUsers', 'viewUsers'], ['deleteUsers', '/user-management/user/bulk-delete'],
                ['deleteUsers', '/user-management/user/delete'], ['deleteUsers', 'viewUsers'],
                ['editUserEmail', 'viewUserEmail'], ['editUsers', '/user-management/user/bulk-activate'],
                ['editUsers', '/user-management/user/bulk-deactivate'], ['editUsers',
                '/user-management/user/update'], ['editUsers', 'viewUsers'], ['educador',
                'gestion_educador_propia'], ['educador', 'gestion_infante'], ['educador',
                'gestion_infante_metricas'], ['educador', 'gestion_infante_vacuna'],
                ['educador', 'gestion_matricula_infante'], ['educador', 'gestion_medidas_antropometricas_infante'],
                ['educador', 'gestion_patologias_infantes'], ['educador', 'gestion_sector_asignado_infante'],
                ['educador', 'gestion_vacunas_infantes'], ['educador', 'permisos_comunes_user_registrado'],
                ['educador', 'poblar_registros_consumo_infantil'], ['educador', 'registrar_observaciones_reacciones_consumo'],
                ['educador', 'update_asistencia_diaria'], ['educador', 'view_bitacora_consumo_infantil'],
                ['gastronomo', 'gestion_carta'], ['gastronomo', 'gestion_recetas'], [
                'gastronomo', 'gestion_tipo_receta'], ['gastronomo', 'permisos_comunes_user_registrado'],
                ['gestion_alimento', '/nutricion/alimento/*'], ['gestion_asignacion_carta_semanal',
                '/consumoalimenticio/asignacioninfantecsemanal/create'], ['gestion_asignacion_carta_semanal',
                '/consumoalimenticio/asignacioninfantecsemanal/eliminar'], ['gestion_asignacion_carta_semanal',
                '/consumoalimenticio/asignacioninfantecsemanal/index'], ['gestion_asignacion_carta_semanal',
                '/consumoalimenticio/asignacioninfantecsemanal/update'], ['gestion_asignacion_carta_semanal',
                '/consumoalimenticio/asignacioninfantecsemanal/view']
                ]
        );

        $this->batchInsert('auth_item_child', ['parent', 'child'], [
                ['gestion_asignacion_carta_semanal', '/consumo_alimenticio/asigcseminfantehasinf/ajax-get-num-infantes'],
                ['gestion_asignacion_carta_semanal', '/consumo_alimenticio/asiginfcsemanal/ajax-infante-list'],
                ['gestion_asistencia', '/asistencia/asistencia/create'], ['gestion_asistencia',
                '/asistencia/asistencia/index'], ['gestion_asistencia', '/asistencia/asistencia/update'],
                ['gestion_asistencia', '/asistencia/asistencia/view'], ['gestion_carta',
                '/preparacioncarta/mprepcarta/ajax-view-all-ingredientes'], ['gestion_carta',
                '/preparacioncarta/mprepcarta/create'], ['gestion_carta', '/preparacioncarta/mprepcarta/index'],
                ['gestion_carta', '/preparacioncarta/mprepcarta/update'], ['gestion_carta',
                '/preparacioncarta/mprepcarta/view'], ['gestion_carta', '/preparacion_carta/dpreparacioncarta/create'],
                ['gestion_carta', '/preparacion_carta/dpreparacioncarta/index'], ['gestion_carta',
                '/preparacion_carta/dpreparacioncarta/update'], ['gestion_carta',
                '/preparacion_carta/dpreparacioncarta/view'], ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-alimento-list'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-alimento-seleccionado'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-cambiar-cantidad'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-eliminar-detalle-alimento'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-generar-data-maestro'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-get-alimento'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-join-tiempo-tipo'],
                ['gestion_carta', '/preparacion_carta/mprepcarta/ajax-set-tiempo-tipo'],
                ['gestion_carta_semanal', '/cartasemanal/mcartasemanal/ajax-del-carta'],
                ['gestion_carta_semanal', '/cartasemanal/mcartasemanal/create'], ['gestion_carta_semanal',
                '/cartasemanal/mcartasemanal/index'], ['gestion_carta_semanal', '/cartasemanal/mcartasemanal/make-pdf-carta-semanal'],
                ['gestion_carta_semanal', '/cartasemanal/mcartasemanal/update'], ['gestion_carta_semanal',
                '/cartasemanal/mcartasemanal/view'], ['gestion_carta_semanal', '/carta_semanal/dcartasemanal/create'],
                ['gestion_carta_semanal', '/carta_semanal/dcartasemanal/index'], ['gestion_carta_semanal',
                '/carta_semanal/dcartasemanal/update'], ['gestion_carta_semanal',
                '/carta_semanal/dcartasemanal/view'], ['gestion_carta_semanal', '/carta_semanal/mcartasemanal/ajax-d-prep-carta'],
                ['gestion_carta_semanal', '/carta_semanal/mcartasemanal/ajax-dprep-cu'],
                ['gestion_carta_semanal', '/carta_semanal/mcartasemanal/ajax-get-capacidad-carta'],
                ['gestion_carta_semanal', '/carta_semanal/mcartasemanal/ajax-num-sem-select'],
                ['gestion_carta_semanal', '/preparacioncarta/mprepcarta/ajax-view-all-ingredientes'],
                ['gestion_carta_semanal', '/preparacion_carta/mprepcarta/ajax-get-cartas-search'],
                ['gestion_centro_infantil', '/centroinfantil/cibv/create'], ['gestion_centro_infantil',
                '/centroinfantil/cibv/index'], ['gestion_centro_infantil', '/centroinfantil/cibv/update'],
                ['gestion_centro_infantil', '/centroinfantil/cibv/view'], ['gestion_comite_central_padres_familia',
                '/centroinfantil/grupocomite/create'], ['gestion_comite_central_padres_familia',
                '/centroinfantil/grupocomite/index'], ['gestion_comite_central_padres_familia',
                '/centroinfantil/grupocomite/update'], ['gestion_comite_central_padres_familia',
                '/centroinfantil/grupocomite/view'], ['gestion_comite_central_padres_familia',
                '/recursoshumanos/comitepadresflia/create'], ['gestion_comite_central_padres_familia',
                '/recursoshumanos/comitepadresflia/index'], ['gestion_comite_central_padres_familia',
                '/recursoshumanos/comitepadresflia/update'], ['gestion_comite_central_padres_familia',
                '/recursoshumanos/comitepadresflia/view'], ['gestion_coordinadorcibv_propia',
                '/recursoshumanos/coordinadorcibv/index'], ['gestion_coordinadorcibv_propia',
                '/recursoshumanos/coordinadorcibv/index-inactivos'], ['gestion_coordinadorcibv_propia',
                '/recursoshumanos/coordinadorcibv/update'], ['gestion_coordinadorcibv_propia',
                '/recursoshumanos/coordinadorcibv/view'], ['gestion_educador', '/recursoshumanos/educador/create'],
                ['gestion_educador', '/recursoshumanos/educador/index'], ['gestion_educador',
                '/recursoshumanos/educador/index-inactivos'], ['gestion_educador',
                '/recursoshumanos/educador/update'], ['gestion_educador', '/recursoshumanos/educador/view'],
                ['gestion_educador_propia', '/recursoshumanos/educador/index'], ['gestion_educador_propia',
                '/recursoshumanos/educador/update'], ['gestion_educador_propia',
                '/recursoshumanos/educador/view'], ['gestion_infante', '/individuo/infante/ajax-list-infantes'],
                ['gestion_infante', '/individuo/infante/create'], ['gestion_infante',
                '/individuo/infante/index'], ['gestion_infante', '/individuo/infante/index-inactivos'],
                ['gestion_infante', '/individuo/infante/update'], ['gestion_infante',
                '/individuo/infante/view'], ['gestion_infante_metricas', '/metricasindividuo/datoantropometrico/*'],
                ['gestion_infante_vacuna', '/individuo/registrovacunainfante/*'], ['gestion_matricula_infante',
                '/inscripcion/matricula/create'], ['gestion_matricula_infante', '/inscripcion/matricula/index'],
                ['gestion_matricula_infante', '/inscripcion/matricula/update'], ['gestion_matricula_infante',
                '/inscripcion/matricula/view'], ['gestion_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/create'], ['gestion_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/index'], ['gestion_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/update'], ['gestion_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/view'], ['gestion_parametros_sistema',
                '/parametrossistema/periodo/*'], ['gestion_patologias_infantes',
                '/individuo/patologia/create'], ['gestion_patologias_infantes', '/individuo/patologia/index'],
                ['gestion_patologias_infantes', '/individuo/patologia/update'], ['gestion_patologias_infantes',
                '/individuo/patologia/view'], ['gestion_periodo', '/parametrossistema/periodo/create'],
                ['gestion_periodo', '/parametrossistema/periodo/index'], ['gestion_periodo',
                '/parametrossistema/periodo/update'], ['gestion_periodo', '/parametrossistema/periodo/view'],
                ['gestion_recetas', '/preparacion_platillo/dplatillo/create'], ['gestion_recetas',
                '/preparacion_platillo/dplatillo/delete'], ['gestion_recetas', '/preparacion_platillo/dplatillo/index'],
                ['gestion_recetas', '/preparacion_platillo/dplatillo/update'], ['gestion_recetas',
                '/preparacion_platillo/dplatillo/view'], ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-cambiar-cantidad'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-eliminar-detalle'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-generar-data-maestro'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-get-ingrediente'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-ingrediente-list'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/ajax-ingrediente-seleccionado'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/create']
                ]
        );

        $this->batchInsert('auth_item_child', ['parent', 'child'], [
                ['gestion_recetas', '/preparacion_platillo/mplatillo/delete'], [
                'gestion_recetas', '/preparacion_platillo/mplatillo/index'], ['gestion_recetas',
                '/preparacion_platillo/mplatillo/make-pdf-orden-pedido'], ['gestion_recetas',
                '/preparacion_platillo/mplatillo/update'], ['gestion_recetas', '/preparacion_platillo/mplatillo/ver'],
                ['gestion_recetas', '/preparacion_platillo/mplatillo/view'], ['gestion_rrhh_autoridades_cibv',
                '/recursoshumanos/autoridadescibv/create'], ['gestion_rrhh_autoridades_cibv',
                '/recursoshumanos/autoridadescibv/index'], ['gestion_rrhh_autoridades_cibv',
                '/recursoshumanos/autoridadescibv/update'], ['gestion_rrhh_autoridades_cibv',
                '/recursoshumanos/autoridadescibv/view'], ['gestion_rrhh_coordinador_cibv',
                '/recursoshumanos/coordinadorcibv/create'], ['gestion_rrhh_coordinador_cibv',
                '/recursoshumanos/coordinadorcibv/index'], ['gestion_rrhh_coordinador_cibv',
                '/recursoshumanos/coordinadorcibv/update'], ['gestion_rrhh_coordinador_cibv',
                '/recursoshumanos/coordinadorcibv/view'], ['gestion_rrhh_gastronomo',
                '/recursoshumanos/gastronomo/create'], ['gestion_rrhh_gastronomo',
                '/recursoshumanos/gastronomo/index'], ['gestion_rrhh_gastronomo',
                '/recursoshumanos/gastronomo/update'], ['gestion_rrhh_gastronomo',
                '/recursoshumanos/gastronomo/view'], ['gestion_rrhh_nutricionista',
                '/recursoshumanos/nutricionista/create'], ['gestion_rrhh_nutricionista',
                '/recursoshumanos/nutricionista/index'], ['gestion_rrhh_nutricionista',
                '/recursoshumanos/nutricionista/update'], ['gestion_rrhh_nutricionista',
                '/recursoshumanos/nutricionista/view'], ['gestion_salones', '/inscripcion/salon/create'],
                ['gestion_salones', '/inscripcion/salon/index'], ['gestion_salones',
                '/inscripcion/salon/update'], ['gestion_salones', '/inscripcion/salon/view'],
                ['gestion_sector_asignado_educador', '/recursoshumanos/sectorasignadoeducador/create'],
                ['gestion_sector_asignado_educador', '/recursoshumanos/sectorasignadoeducador/index'],
                ['gestion_sector_asignado_educador', '/recursoshumanos/sectorasignadoeducador/update'],
                ['gestion_sector_asignado_educador', '/recursoshumanos/sectorasignadoeducador/view'],
                ['gestion_sector_asignado_gastronomo', '/tecnicoarea/asignaciongastronomodistrito/create'],
                ['gestion_sector_asignado_gastronomo', '/tecnicoarea/asignaciongastronomodistrito/index'],
                ['gestion_sector_asignado_gastronomo', '/tecnicoarea/asignaciongastronomodistrito/update'],
                ['gestion_sector_asignado_gastronomo', '/tecnicoarea/asignaciongastronomodistrito/view'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/*'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/create'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/delete'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/index'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/update'],
                ['gestion_sector_asignado_infante', '/centroinfantil/sectorasignadoinfante/view'],
                ['gestion_sector_asignado_nutricionista', '/tecnicoarea/asignacionnutricionistadistrito/create'],
                ['gestion_sector_asignado_nutricionista', '/tecnicoarea/asignacionnutricionistadistrito/index'],
                ['gestion_sector_asignado_nutricionista', '/tecnicoarea/asignacionnutricionistadistrito/update'],
                ['gestion_sector_asignado_nutricionista', '/tecnicoarea/asignacionnutricionistadistrito/view'],
                ['gestion_tipo_receta', '/preparacion_platillo/tiporeceta/create'], [
                'gestion_tipo_receta', '/preparacion_platillo/tiporeceta/delete'],
                ['gestion_tipo_receta', '/preparacion_platillo/tiporeceta/index'], [
                'gestion_tipo_receta', '/preparacion_platillo/tiporeceta/update'],
                ['gestion_tipo_receta', '/preparacion_platillo/tiporeceta/view'], ['gestion_tipo_vacuna',
                '/metricasindividuo/tipovacuna/create'], ['gestion_tipo_vacuna',
                '/metricasindividuo/tipovacuna/index'], ['gestion_tipo_vacuna', '/metricasindividuo/tipovacuna/update'],
                ['gestion_tipo_vacuna', '/metricasindividuo/tipovacuna/view'], ['gestion_vacunas_infantes',
                '/individuo/registrovacunainfante/create'], ['gestion_vacunas_infantes',
                '/individuo/registrovacunainfante/index'], ['gestion_vacunas_infantes',
                '/individuo/registrovacunainfante/update'], ['gestion_vacunas_infantes',
                '/individuo/registrovacunainfante/view'], ['nutricion', 'gestion_carta'],
                ['nutricion', 'gestion_carta_semanal'], ['nutricion', 'gestion_infante_metricas'],
                ['nutricion', 'gestion_infante_vacuna'], ['nutricion', 'gestion_tipo_vacuna'],
                ['nutricion', 'permisos_comunes_user_registrado'], ['nutricion', 'valorar_consumo_real_vs_ideal'],
                ['nutricion', 'view_composicion_nutricional_carta'], ['nutricion', 'view_curvas_crecimiento'],
                ['permisos_auditoria', '/audit/*'], ['permisos_auditoria', '/audit/default/*'],
                ['permisos_auditoria', '/audit/default/index'], ['permisos_auditoria',
                '/audit/entry/*'], ['permisos_auditoria', '/audit/entry/index'],
                ['permisos_auditoria', '/audit/entry/view'], ['permisos_auditoria', '/audit/error/*'],
                ['permisos_auditoria', '/audit/error/index'], ['permisos_auditoria',
                '/audit/error/view'], ['permisos_auditoria', '/audit/javascript/*'],
                ['permisos_auditoria', '/audit/javascript/index'], ['permisos_auditoria',
                '/audit/javascript/view'], ['permisos_auditoria', '/audit/js-log/*'],
                ['permisos_auditoria', '/audit/js-log/index'], ['permisos_auditoria',
                '/audit/mail/*'], ['permisos_auditoria', '/audit/mail/download'],
                ['permisos_auditoria', '/audit/mail/index'], ['permisos_auditoria', '/audit/mail/view'],
                ['permisos_auditoria', '/audit/trail/*'], ['permisos_auditoria', '/audit/trail/index'],
                ['permisos_auditoria', '/audit/trail/view'], ['permisos_comunes_user_registrado',
                '//index'], ['permisos_comunes_user_registrado', '/site/*'], ['permisos_comunes_user_registrado',
                '/site/error'], ['permisos_comunes_user_registrado', '/site/index'],
                ['permisos_comunes_user_registrado', '/site/login'], ['permisos_comunes_user_registrado',
                '/site/logout'], ['permisos_comunes_user_registrado', '/site/request-password-reset'],
                ['permisos_comunes_user_registrado', '/site/reset-password'], ['permisos_comunes_user_registrado',
                '/site/signup'], ['permisos_comunes_user_registrado', '/user-management/auth/captcha'],
                ['permisos_comunes_user_registrado', '/user-management/auth/change-own-password'],
                ['permisos_comunes_user_registrado', '/user-management/auth/confirm-email'],
                ['permisos_comunes_user_registrado', '/user-management/auth/confirm-email-receive']
                ]
        );

        $this->batchInsert('auth_item_child', ['parent', 'child'], [
                ['permisos_comunes_user_registrado', '/user-management/auth/confirm-registration-email'],
                ['permisos_comunes_user_registrado', '/user-management/auth/login'],
                ['permisos_comunes_user_registrado', '/user-management/auth/logout'],
                ['permisos_comunes_user_registrado', '/user-management/auth/password-recovery'],
                ['permisos_comunes_user_registrado', '/user-management/auth/password-recovery-receive'],
                ['permisos_comunes_user_registrado', 'changeOwnPassword'], ['poblar_registros_consumo_infantil',
                '/consumoalimenticio/bitacoraconsumoinfantil/create'], ['poblar_registros_consumo_infantil',
                '/consumoalimenticio/bitacoraconsumoinfantil/index'], ['registrar_observaciones_reacciones_consumo',
                '/consumoalimenticio/bitacoraconsumoinfantil/index'], ['registrar_observaciones_reacciones_consumo',
                '/consumoalimenticio/bitacoraconsumoinfantil/update'], ['sysadmin',
                'Admin'], ['sysadmin', 'assignRolesToUsers'], ['sysadmin', 'bindUserToIp'],
                ['sysadmin', 'changeOwnPassword'], ['sysadmin', 'changeUserPassword'],
                ['sysadmin', 'coordinador'], ['sysadmin', 'coordinador-gad'], ['sysadmin',
                'createUsers'], ['sysadmin', 'deleteUsers'], ['sysadmin', 'editUserEmail'],
                ['sysadmin', 'editUsers'], ['sysadmin', 'educador'], ['sysadmin', 'gastronomo'],
                ['sysadmin', 'nutricion'], ['sysadmin', 'viewRegistrationIp'], ['sysadmin',
                'viewUserEmail'], ['sysadmin', 'viewUserRoles'], ['sysadmin', 'viewUsers'],
                ['update_asistencia_diaria', '/asistencia/asistencia/index'], ['update_asistencia_diaria',
                '/asistencia/asistencia/update'], ['valorar_consumo_real_vs_ideal',
                '/consumoalimenticio/bitacoraconsumoinfantil/ajaxcalcrep'], ['valorar_consumo_real_vs_ideal',
                '/consumoalimenticio/bitacoraconsumoinfantil/index'], ['valorar_consumo_real_vs_ideal',
                '/consumoalimenticio/bitacoraconsumoinfantil/report-consumo-ideal'],
                ['valorar_consumo_real_vs_ideal', '/consumoalimenticio/bitacoraconsumoinfantil/update'],
                ['valorar_consumo_real_vs_ideal', '/consumoalimenticio/bitacoraconsumoinfantil/view'],
                ['valorar_consumo_real_vs_ideal', '/individuo/infante/ajax-list-infantes'],
                ['ver_reportes_cibv', '/centroinfantil/cibv/report-estado-nutricional'],
                ['viewUsers', '/user-management/user/grid-page-size'], ['viewUsers',
                '/user-management/user/index'], ['viewUsers', '/user-management/user/view'],
                ['viewVisitLog', '/user-management/user-visit-log/grid-page-size'], [
                'viewVisitLog', '/user-management/user-visit-log/index'], ['viewVisitLog',
                '/user-management/user-visit-log/view'], ['view_asistencia_infante',
                '/asistencia/asistencia/index'], ['view_asistencia_infante', '/asistencia/asistencia/view'],
                ['view_bitacora_consumo', '/consumoalimenticio/bitacoraconsumoinfantil/ajaxcalcrep'],
                ['view_bitacora_consumo', '/consumoalimenticio/bitacoraconsumoinfantil/index'],
                ['view_bitacora_consumo', '/consumoalimenticio/bitacoraconsumoinfantil/index-inactivos'],
                ['view_bitacora_consumo', '/consumoalimenticio/bitacoraconsumoinfantil/report-consumo-ideal'],
                ['view_bitacora_consumo', '/consumoalimenticio/bitacoraconsumoinfantil/view'],
                ['view_bitacora_consumo_infantil', '/consumoalimenticio/bitacoraconsumoinfantil/index'],
                ['view_bitacora_consumo_infantil', '/consumoalimenticio/bitacoraconsumoinfantil/view'],
                ['view_cartas', '/preparacioncarta/mprepcarta/ajax-view-all-ingredientes'],
                ['view_cartas', '/preparacioncarta/mprepcarta/index'], ['view_cartas',
                '/preparacioncarta/mprepcarta/view'], ['view_cartas', '/preparacion_carta/dpreparacioncarta/index'],
                ['view_cartas', '/preparacion_carta/dpreparacioncarta/view'], ['view_consumo_infantes',
                '/consumoalimenticio/bitacoraconsumoinfantil/ajaxcalcrep'], ['view_consumo_infantes',
                '/consumoalimenticio/bitacoraconsumoinfantil/index'], ['view_consumo_infantes',
                '/consumoalimenticio/bitacoraconsumoinfantil/index-inactivos'], [
                'view_consumo_infantes', '/consumoalimenticio/bitacoraconsumoinfantil/report-consumo-ideal'],
                ['view_consumo_infantes', '/consumoalimenticio/bitacoraconsumoinfantil/view'],
                ['view_consumo_infantes', '/individuo/infante/ajax-list-infantes'], [
                'view_curvas_crecimiento', '/individuo/infante/index'], ['view_curvas_crecimiento',
                '/individuo/infante/index-inactivos'], ['view_curvas_crecimiento',
                '/individuo/infante/view'], ['view_educadores', '/recursoshumanos/educador/index'],
                ['view_educadores', '/recursoshumanos/educador/index-inactivos'], ['view_educadores',
                '/recursoshumanos/educador/view'], ['view_matriculas_infante', '/inscripcion/matricula/index'],
                ['view_matriculas_infante', '/inscripcion/matricula/view'], ['view_mcartasemanal',
                '/cartasemanal/mcartasemanal/index'], ['view_mcartasemanal', '/cartasemanal/mcartasemanal/make-pdf-carta-semanal'],
                ['view_mcartasemanal', '/cartasemanal/mcartasemanal/view'], ['view_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/index'], ['view_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/index-inactivos'], ['view_medidas_antropometricas_infante',
                '/metricasindividuo/datoantropometrico/view'], ['view_mprepplatillo',
                '/preparacion_platillo/mplatillo/*'], ['view_mprepplatillo', '/preparacion_platillo/mplatillo/index'],
                ['view_mprepplatillo', '/preparacion_platillo/mplatillo/make-pdf-orden-pedido'],
                ['view_mprepplatillo', '/preparacion_platillo/mplatillo/view'], ['view_patologias_infantes',
                '/individuo/patologia/index'], ['view_patologias_infantes', '/individuo/patologia/view'],
                ['view_sector_asignado_educadores', '/recursoshumanos/sectorasignadoeducador/index'],
                ['view_sector_asignado_educadores', '/recursoshumanos/sectorasignadoeducador/view'],
                ['view_sector_asignado_infantes', '/centroinfantil/sectorasignadoinfante/index'],
                ['view_sector_asignado_infantes', '/centroinfantil/sectorasignadoinfante/view'],
                ['view_vacunas_infantes', '/individuo/registrovacunainfante/index'],
                ['view_vacunas_infantes', '/individuo/registrovacunainfante/index-inactivos'],
                ['view_vacunas_infantes', '/individuo/registrovacunainfante/view']
                ]
        );
    }

    public function down() {
        if ($this->db->driverName === 'pgsql') {
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child CASCADE;
SQL;
            $this->execute($truncateAuth_item_child);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateAuth_item_child = <<< SQL
truncate table auth_item_child;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;
            $this->execute($disable);
            $this->execute($truncateAuth_item_child);
            $this->execute($enable);
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
