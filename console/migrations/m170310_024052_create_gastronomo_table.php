<?php

use yii\db\Migration;

/**
 * Handles the creation for table `gastronomo`.
 */
class m170310_024052_create_gastronomo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('gastronomo', [
            'gastronomo_id' => $this->primaryKey(),
            'gastronomo_dni' => $this->string(45)->notNull(),
            'gastronomo_nombres' => $this->string(150)->notNull(),
            'gastronomo_apellidos' => $this->string(150)->notNUll(),
            'gastronomo_telefono' => $this->string(15)->notNull(),
            'gastronomo_correo' => $this->string(150)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('gastronomo');
    }
}
