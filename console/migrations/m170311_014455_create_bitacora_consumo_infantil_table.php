<?php
use yii\db\Migration;

/**
 * Handles the creation for table `bitacora_consumo_infantil`.
 * Has foreign keys to the tables:
 *
 * - `asignacion_infante_c_semanal`
 */
class m170311_014455_create_bitacora_consumo_infantil_table extends Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS b_cons_inf_tiempo_comida CASCADE");
            $this->execute("CREATE TYPE b_cons_inf_tiempo_comida AS  ENUM('DESAYUNO','REFRIGERIO_DE_LA_MAÑANA','ALMUERZO','REFRIGERIO_DE_LA_TARDE');");

            $this->createTable('bitacora_consumo_infantil', [
                'b_cons_inf_id' => $this->primaryKey(),
                'asig_inf_c_sem_id' => $this->integer()->notNull(),
                'b_cons_inf_porcentaje_consumo' => $this->decimal(3, 2)->notNull(),
                'm_prep_carta_id' => $this->integer()->notNull(),
                'b_cons_inf_tiempo_comida' => "b_cons_inf_tiempo_comida NOT NULL",
                'b_cons_inf_observaciones' => $this->string(255),
                'b_cons_inf_fecha_consumo' => $this->date()->notNull(),
            ]);
        } else if ($this->db->driverName === 'mysql') {
            $this->createTable('bitacora_consumo_infantil', [
                'b_cons_inf_id' => $this->primaryKey(),
                'asig_inf_c_sem_id' => $this->integer()->notNull(),
                'b_cons_inf_porcentaje_consumo' => $this->decimal(3, 2)->notNull(),
                'm_prep_carta_id' => $this->integer()->notNull(),
                'b_cons_inf_tiempo_comida' => "ENUM('DESAYUNO','REFRIGERIO_DE_LA_MAÑANA','ALMUERZO','REFRIGERIO_DE_LA_TARDE') NOT NULL",
                'b_cons_inf_observaciones' => $this->string(255),
                'b_cons_inf_fecha_consumo' => $this->date()->notNull(),
            ]);
        }

        // creates index for column `asig_inf_c_sem_id`
        $this->createIndex(
            'idx-bitacora_consumo_infantil-asig_inf_c_sem_id', 'bitacora_consumo_infantil', 'asig_inf_c_sem_id'
        );

        // add foreign key for table `asignacion_infante_c_semanal`
        $this->addForeignKey(
            'fk-bitacora_consumo_infantil-asig_inf_c_sem_id', 'bitacora_consumo_infantil', 'asig_inf_c_sem_id', 'asignacion_infante_c_semanal', 'asig_inf_c_sem_id', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $this->execute("DROP TYPE IF EXISTS b_cons_inf_tiempo_comida CASCADE");
        }
        // drops foreign key for table `asignacion_infante_c_semanal`
        $this->dropForeignKey(
            'fk-bitacora_consumo_infantil-asig_inf_c_sem_id', 'bitacora_consumo_infantil'
        );

        // drops index for column `asig_inf_c_sem_id`
        $this->dropIndex(
            'idx-bitacora_consumo_infantil-asig_inf_c_sem_id', 'bitacora_consumo_infantil'
        );

        $this->dropTable('bitacora_consumo_infantil');
    }
}
