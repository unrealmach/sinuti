<?php

use yii\db\Migration;

/**
 * Handles the creation for table `grupo_comite`.
 * Has foreign keys to the tables:
 *
 * - `periodo`
 * - `cibv`
 */
class m170310_151013_create_grupo_comite_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('grupo_comite', [
            'grupo_comite_id' => $this->primaryKey(),
            'fecha_inicio_actividad' => $this->date()->notNull(),
            'fecha_fin_actividad' => $this->date(),
            'periodo_id' => $this->integer(),
            'cen_inf_id' => $this->integer(),
        ]);

        // creates index for column `periodo_id`
        $this->createIndex(
            'idx-grupo_comite-periodo_id',
            'grupo_comite',
            'periodo_id'
        );

        // add foreign key for table `periodo`
        $this->addForeignKey(
            'fk-grupo_comite-periodo_id',
            'grupo_comite',
            'periodo_id',
            'periodo',
            'periodo_id',
            'CASCADE'
        );

        // creates index for column `cen_inf_id`
        $this->createIndex(
            'idx-grupo_comite-cen_inf_id',
            'grupo_comite',
            'cen_inf_id'
        );

        // add foreign key for table `cibv`
        $this->addForeignKey(
            'fk-grupo_comite-cen_inf_id',
            'grupo_comite',
            'cen_inf_id',
            'cibv',
            'cen_inf_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `periodo`
        $this->dropForeignKey(
            'fk-grupo_comite-periodo_id',
            'grupo_comite'
        );

        // drops index for column `periodo_id`
        $this->dropIndex(
            'idx-grupo_comite-periodo_id',
            'grupo_comite'
        );

        // drops foreign key for table `cibv`
        $this->dropForeignKey(
            'fk-grupo_comite-cen_inf_id',
            'grupo_comite'
        );

        // drops index for column `cen_inf_id`
        $this->dropIndex(
            'idx-grupo_comite-cen_inf_id',
            'grupo_comite'
        );

        $this->dropTable('grupo_comite');
    }
}
