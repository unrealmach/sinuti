<?php

use yii\db\Migration;

/**
 * Handles the creation for table `parroquia`.
 * Has foreign keys to the tables:
 *
 * - `localidad`
 */
class m170309_182432_create_parroquia_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('parroquia', [
            'parroquia_id' => $this->primaryKey(),
            'parroquia_codigo' => $this->integer()->notNull(),
            'parroquia_nombre' => $this->string(100)->notNull(),
            'localidad_id' => $this->integer(),
        ]);

        // creates index for column `localidad_id`
        $this->createIndex(
            'idx-parroquia-localidad_id',
            'parroquia',
            'localidad_id'
        );

        // add foreign key for table `localidad`
        $this->addForeignKey(
            'fk-parroquia-localidad_id',
            'parroquia',
            'localidad_id',
            'localidad',
            'localidad_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `localidad`
        $this->dropForeignKey(
            'fk-parroquia-localidad_id',
            'parroquia'
        );

        // drops index for column `localidad_id`
        $this->dropIndex(
            'idx-parroquia-localidad_id',
            'parroquia'
        );

        $this->dropTable('parroquia');
    }
}
