<?php

use yii\db\Migration;

/**
 * Handles the creation for table `porcion_por_alimento`.
 * Has foreign keys to the tables:
 *
 * - `alimento`
 */
class m170310_030641_create_porcion_por_alimento_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('porcion_por_alimento', [
            'porc_alimento_id' => $this->primaryKey(),
            'alimento_id' => $this->integer(),
            'porc_alimento_cantidad' => $this->decimal('10,2')->notNull()->defaultValue(0),
            'porc_alimento_medida_cacera' => $this->string(45)->notNull(),
        ]);

        // creates index for column `alimento_id`
        $this->createIndex(
            'idx-porcion_por_alimento-alimento_id',
            'porcion_por_alimento',
            'alimento_id'
        );

        // add foreign key for table `alimento`
        $this->addForeignKey(
            'fk-porcion_por_alimento-alimento_id',
            'porcion_por_alimento',
            'alimento_id',
            'alimento',
            'alimento_id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `alimento`
        $this->dropForeignKey(
            'fk-porcion_por_alimento-alimento_id',
            'porcion_por_alimento'
        );

        // drops index for column `alimento_id`
        $this->dropIndex(
            'idx-porcion_por_alimento-alimento_id',
            'porcion_por_alimento'
        );

        $this->dropTable('porcion_por_alimento');
    }
}
