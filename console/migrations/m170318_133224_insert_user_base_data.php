<?php
use yii\db\Migration;

class m170318_133224_insert_user_base_data extends Migration
{

    public function up()
    {
        $this->insert('user', [
            'id' => '1',
            'username' => 'superadmin',
            'auth_key' => 'p64yTbZBMStJ7YcZjWxIQVEEkbccr9LN',
            'password_hash' => '$2y$13$xwwx85ZN2yOgtcuF8/gqjux./fHMjjfTHIgj6XfICs0oUNpbk/jQq',
            'confirmation_token' => null,
            'status' => '1',
            'superadmin' => '1',
            'created_at' => '1456971730',
            'updated_at' => '1462586322',
            'registration_ip' => null,
            'bind_to_ip' => '',
            'email' => 'unrealmach2@hotmail.com',
            'email_confirmed' => '0'
        ]);
    }

    public function down()
    {
        if ($this->db->driverName === 'pgsql') {
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log CASCADE;
SQL;
            

            $truncateUser = <<< SQL
truncate table "user" CASCADE;
SQL;
            
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
        } else if ($this->db->driverName === 'mysql') {
            $disable = <<< SQL
SET FOREIGN_KEY_CHECKS = 0;
SQL;
            $truncateUser_visit_log = <<< SQL
truncate table user_visit_log ;
SQL;
            $truncateUser = <<< SQL
truncate table user ;
SQL;
            $enable = <<< SQL
SET FOREIGN_KEY_CHECKS = 1;
SQL;

            $this->execute($disable);
            $this->execute($truncateUser_visit_log);
            $this->execute($truncateUser);
            $this->execute($enable);
        }
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
